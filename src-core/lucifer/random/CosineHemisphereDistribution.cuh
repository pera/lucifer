#ifndef COSINEHEMISPHEREDISTRIBUTION_CUH_
#define COSINEHEMISPHEREDISTRIBUTION_CUH_

#include "ConcentricDiskDistribution.cuh"
#include "lucifer/math/Math.cuh"

namespace lucifer {

/*!
 * \brief
 * Distributes points along the positive z axis hemisphere proportionally to
 * the cosine of the angle with z axis.
 *
 * \author
 * Bruno Pera.
 */
class CosineHemisphereDistribution {
private:
	__host__ __device__
	static float v2NormE2(const float x[2]) {
		return x[0] * x[0] + x[1] * x[1];
	}

	__host__ __device__
	static float v3NormE2(const float x[3]) {
		return x[0] * x[0] + x[1] * x[1] + x[2] * x[2];
	}

public:
	__host__ __device__
	CosineHemisphereDistribution() = delete;

	/*!
	 * \brief
	 * Maps a pair o uniformly distributed numbers in the interval [0, 1] into
	 * points on the positive z-axis hemisphere of the sphere of radius 1
	 * centered at the origin proportionally to the cosine of the angle with
	 * the z axis.
	 *
	 * \param[in] random A pair of numbers in [0, 1].
	 * \param[out] sample The resulting x, y and z coordinates of a point on the
	 * positive z-axis of the canonical sphere's surface. As the given pair
	 * `random` uniformly varies between 0 and 1 the resulting `sample` will
	 * vary on the hemisphere's surface proportionally to the cosine of the
	 * angle.
	 * \param[out] The PDF with respect to the surface area measure.
	 */
	__host__ __device__
	static void sample(const float random[2], float sample[3], float* pdf) {
		ConcentricDiskDistribution::sample(random, sample, pdf);
		sample[2] = sqrt(max(0.f, 1.f - v2NormE2(sample)));
		if (pdf != nullptr) *pdf = sample[2] / (PI<float>() * v3NormE2(sample));
	}

	/*!
	 * \brief
	 * Returns the PDF of sampling a point on the positive z-axis of the
	 * canonical sphere's surface with respect to the surface area measure.
	 *
	 * \param[in] sample The point on the positive z-axis canonical sphere's
	 * surface. The point is supposed to be on the canonical sphere surface but
	 * not necessarily at the positive z-axis hemisphere.
	 *
	 * \return The PDF of sampling the given point with respect to the surface
	 * area measure.
	 */
	__host__ __device__
	static float pdf(const float sample[3]) {
	    return sample[2] < 0.f ? 0.f :  sample[2] / (PI<float>() * v3NormE2(sample));
	}
};

}

#endif /* COSINEHEMISPHEREDISTRIBUTION_CUH_ */

#ifndef UNIFORMHEMISPHEREDISTRIBUTION_CUH_
#define UNIFORMHEMISPHEREDISTRIBUTION_CUH_

#include "lucifer/math/Math.cuh"

namespace lucifer {

/*!
 * \brief
 * A uniform distribution of points along positve z axis hemisphere of the
 * canonical sphere.
 *
 * \author
 * Bruno Pera.
 */
class UniformHemisphereDistribution {
public:
	static constexpr float PDF = 1.f / (2.f * PI<float>());

public:
	__host__ __device__
	UniformHemisphereDistribution() = delete;

	/*!
	 * \brief
	 * Maps a pair o uniformly distributed numbers in the interval [0, 1] into
	 * points on the positive z-axis hemisphere of the sphere of radius 1
	 * centered at the origin.
	 *
	 * \param[in] random A pair of numbers in [0, 1].
	 * \param[out] sample The resulting x, y and z coordinates of a point on the
	 * positive z-axis of the canonical sphere's surface. As the given pair
	 * `random` uniformly varies between 0 and 1 the resulting `sample` will
	 * uniformly vary on the hemisphere's surface.
	 * \param[out] The PDF with respect to the surface area measure.
	 */
	__host__ __device__
	static void sample(const float random[2], float sample[3], float* pdf) {
		float t = random[1];
	    float r = sqrt(max(0.f, 1.f - (t * t)));
	    float phi = 2.f * PI<float>() * random[0];
	    sample[0] = r * cos(phi);
	    sample[1] = r * sin(phi);
	    sample[2] = t;
	    if (pdf != nullptr) *pdf = PDF;
	}

	/*!
	 * \brief
	 * Returns the PDF of uniformly sampling a point on the positive z-axis of
	 * the canonical sphere's surface with respect to the surface area measure.
	 *
	 * \param[in] sample The point on the positive z-axis canonical sphere's
	 * surface. The point is supposed to be on the canonical sphere surface but
	 * not necessarily at the positive z-axis hemisphere.
	 *
	 * \return The PDF of sampling the given point with respect to the surface
	 * area measure.
	 */
	__host__ __device__
	static float pdf(const float sample[3]) {
		return sample[2] < 0.f ? 0.f : PDF;
	}
};

}

#endif /* UNIFORMHEMISPHEREDISTRIBUTION_CUH_ */

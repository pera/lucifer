#ifndef PIECEWISELINEARDISTRIBUTION1D_CUH_
#define PIECEWISELINEARDISTRIBUTION1D_CUH_

#include <cassert>
#include <cstdint>
#include <iostream>
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/memory/Polymorphic.cuh"

namespace lucifer {

/*!
 * \brief
 * One dimensional piecewise linear distribution.
 *
 * \author
 * Bruno Pera.
 */
class PiecewiseLinearDistribution1D: public Polymorphic {
public:
	using index_t = uint16_t;
	using array_t = Array<float, index_t>;

private:
	RangeF range;
	float xstep;
	array_t pdf_;
	array_t cdf_;
	array_t del_;

private:
	__host__  __device__
	static float solveQuadratic(const float a, const float b, const float c) {
		float d = b * b - 4 * a * c;
		assert(d >= 0.f);
		if (d == 0.f) return -.5f * b / a;
		return c / ((b > 0.f) ? -0.5f * (b + sqrt(d)) : -0.5f * (b - sqrt(d)));
	}

	__host__ __device__
	float quantile(const index_t i, const float p) const {
		return solveQuadratic(.5f * del_[i], pdf_[i], cdf_[i] - p) + range.lower + i * xstep;
	}

public:
	__host__
	PiecewiseLinearDistribution1D(MemoryPool& pool, const RangeF& support, const array_t& values):
	range(support), xstep(support.length() / (values.size() - 1)), pdf_(pool, values.size() - 1), cdf_(pool, values.size()), del_(pool, values.size() - 1) {
#ifdef __LUCIFER_DEVELOPMENT__
		assert(values.size() >= 2);
		for (auto v : values) assert(v >= 0.f);
#endif
		auto n = pdf_.size();
		auto step = support.length() / n;
		// compute function integral
		cdf_[0] = 0.f;
		for (index_t i = 1; i < cdf_.size(); ++i) {
			cdf_[i] = cdf_[i - 1] + xstep * .5f * (values[i - 1] + values[i]);
		}
		// computes pdf and derivatives values
		for (index_t i = 0; i < pdf_.size(); ++i) {
			pdf_[i] = values[i] / cdf_[n];
			del_[i] = (values[i + 1] - values[i]) / (cdf_[n] * step);
		}
		// normalize cdf to 1
		for (index_t i = 1; i < cdf_.size() - 1; i++) {
			cdf_[i] /= cdf_[n];
		}
		cdf_[n] = 1.f;
	}

	/*!
	 * \brief
	 * Creates a new
	 * <a href="https://en.wikipedia.org/wiki/Piecewise_linear_function">
	 * one dimensional piecewise linear distribution
	 * </a>.
	 *
	 * \param[in] support The distribution's support.
	 * \param[in] values The array of values on equally spaced positions on the
	 * distribution's support. These values define a piecewise linear function
	 * that will be scaled so that it's integral over the support is 1 so that
	 * it can be used as PDF. The array must have at least two elements and
	 * every element must be non negative otherwise the behaviour is
	 * undefined.
	 */
	__host__
	static Pointer<PiecewiseLinearDistribution1D> build(MemoryPool& pool, const RangeF& support, const array_t& values) {
		return Pointer<PiecewiseLinearDistribution1D>::make(pool, pool, support, values);
	}

	/*!
	 * \brief
	 * Returns the distribution's support.
	 *
	 * \return The distribution's support.
	 */
	__host__ __device__
	const RangeF& support() const {
		return range;
	}

	/*!
	 * \brief
	 * Computes the PDF at a given point.
	 *
	 * \param x The point in the distribution's domain.
	 * \return The PDF at the given point.
	 */
	__host__ __device__
	float pdf(const float x) const {
		if (x < range.lower || x > range.upper) return 0.f;
		index_t i = clamp<index_t>((((x - range.lower) * pdf_.size()) / range.length()), 0, pdf_.size() - 1);
		return pdf_[i] + del_[i] * (x - (range.lower + i * xstep));
	}

	/*!
	 * \brief
	 * Computes the CDF at a given point.
	 *
	 * \param x The point in the distribution's domain.
	 * \return The CDF at the given point.
	 */
	__host__ __device__
	float cdf(const float x) const {
		if (x <= range.lower) return 0.f;
		if (x >= range.upper) return 1.f;
		index_t i = clamp<index_t>((((x - range.lower) * pdf_.size()) / range.length()), 0, pdf_.size() - 1);
		float dx = x - (range.lower + i * xstep);
		return cdf_[i] + dx * (pdf_[i] + .5f * dx * del_[i]);
	}

	/*!
	 * \brief
	 * Computes the quantile (CDF inverse).
	 *
	 * \param p The cumulative density value.
	 * \return The quantile at `p`.
	 */
	__host__ __device__
	float quantile(const float p) const {
		uint16_t l = 0;
		uint16_t u = cdf_.size() - 1;
		while(true) {
			if (u < l) return p < 0.f ? 0.f : 1.f;
			uint16_t m = (l + u) / 2;
			if (p >= cdf_[m] and p <= cdf_[m + 1]) return quantile(m, p);
			if (p <  cdf_[m]) u = m - 1;
			else l = m + 1;
		}
	}

	/*!
	 * \brief
	 * Samples a number from this distribution.
	 *
	 * \param[in] rnd A uniformly distributed random number in [0, 1].
	 * \param[out] pdf If not null holds the pdf of the sampled value.
	 * \return The sampled value.
	 */
	__host__ __device__
	float sample(const float rnd, float* pdf = nullptr) const {
		float smp = quantile(clamp(rnd, 0.f, 1.0f));
		if (pdf != nullptr) *pdf = this->pdf(smp);
		return smp;
	}
};

}

#endif /* PIECEWISELINEARDISTRIBUTION1D_CUH_ */

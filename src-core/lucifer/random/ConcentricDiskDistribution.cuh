#ifndef CONCENTRICDISKDISTRIBUTION_CUH_
#define CONCENTRICDISKDISTRIBUTION_CUH_

#include "lucifer/math/Math.cuh"

namespace lucifer {

/*!
 * \brief
 * Uniform distribution of points along the unit disk at the origin.
 *
 * \author
 * Bruno Pera.
 */
class ConcentricDiskDistribution {
public:
	__host__ __device__
	ConcentricDiskDistribution() = delete;

	/*!
	 * \brief
	 * Maps a pair o uniformly distributed numbers in the interval [0, 1] into
	 * points on the unit disk centered at the origin.
	 *
	 * \param[in] random A pair of numbers in [0, 1].
	 * \param[out] sample The resulting x, y coordinates of a point on the unit
	 * circel centered at the origin. As the given pair `random` uniformly
	 * varies between 0 and 1 the resulting `sample` will uniformly vary on the
	 * the disk.
	 * \param[out] The PDF with respect to the surface area measure.
	 */
	__host__ __device__
	static void sample(const float random[2], float sample[2], float* pdf) {
		float r;
		float t;
		if (pdf != nullptr) *pdf = INV_PI<float>();
		// Map uniform random numbers to [-1,1]^2
		const float sx = (2.f * random[0]) - 1.f;
		const float sy = (2.f * random[1]) - 1.f;
		// Handle degeneracy at the origin
		if ((sx == 0.0f) && (sy == 0.0f)) {
			sample[0] = 0.0f;
			sample[1] = 0.0f;
			return;
		}
		// Map square to (r, theta)
		if (sx >= -sy) {
			if (sx > sy) {
				// Handle first region of disk
				r = sx;
				if (sy > 0.0f) {
					t = sy / r;
				} else {
					t = 8.f + (sy / r);
				}
			} else {
				// Handle second region of disk
				r = sy;
				t = 2.f - (sx / r);
			}
		} else {
			if (sx <= sy) {
				// Handle third region of disk
				r = -sx;
				t = 4.f - (sy / r);
			} else {
				// Handle fourth region of disk
				r = -sy;
				t = 6.f + (sx / r);
			}
		}
		t *= PI<float>() / 4.f;
		if (t < 0.f) {
			t += 2.f * PI<float>();
		}
		// ok
		sample[0] = r * cos(t);
		sample[1] = r * sin(t);
	}

	/*!
	 * \brief
	 * Returns the PDF of uniformly sampling a point on the unit disk
	 * centered at the origin with respect to the area measure.
	 *
	 * \param[in] sample The point on the canonical sphere's surface.
	 *
	 * \return The PDF of sampling the given point with respect to the surface
	 * area measure or zero if the given point is outside the disk.
	 */
	__host__ __device__
	static float pdf(const float sample[2]) {
		return (sample[0] * sample[0] + sample[1] * sample[1] > 1.f) ? 0.f : INV_PI<float>();
	}
};

}

#endif /* CONCENTRICDISKDISTRIBUTION_CUH_ */

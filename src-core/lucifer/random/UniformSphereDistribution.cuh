#ifndef UNIFORMSPHEREDISTRIBUTION_CUH_
#define UNIFORMSPHEREDISTRIBUTION_CUH_

#include "lucifer/math/Math.cuh"

namespace lucifer {

/*!
 * \brief
 * A uniform distribution of points along the canonical sphere's surface.
 *
 * \author
 * Bruno Pera.
 */
class UniformSphereDistribution {
public:
	static constexpr float PDF = 1.f / (4.f * PI<float>());

public:
	__host__ __device__
	UniformSphereDistribution() = delete;

	/*!
	 * \brief
	 * Maps a pair o uniformly distributed numbers in the interval [0, 1] into
	 * points on the surface of the sphere of radius 1 centered at the origin.
	 *
	 * \param[in] random A pair of numbers in [0, 1].
	 * \param[out] sample The resulting x, y and z coordinates of a point on
	 * the canonical sphere's surface. As the given pair `random` uniformly
	 * varies between 0 and 1 the resulting `sample` will uniformly vary on
	 * canonical's sphere suface.
	 * \param[out] The PDF with respect to the surface area measure.
	 */
	__host__ __device__
	static void sample(const float random[2], float sample[3], float* pdf) {
	    float t = 1.f - (2.f * random[1]);
	    float r = sqrt(max(0.f, 1.f - (t * t)));
	    float phi = 2.f * PI<float>() * random[0];
	    sample[0] = r * cos(phi);
	    sample[1] = r * sin(phi);
	    sample[2] = t;
	    if (pdf != nullptr) *pdf = PDF;
	}

	/*!
	 * \brief
	 * The inverse of the `sample` method. Maps a uniformly distributed point
	 * on the the canonical sphere's surface into a pair of uniformly
	 * distributed numbers in [0, 1].
	 *
	 * \param[in] sampled The x, y, z components of a point on the canonical
	 * sphere's surface. The point is supposed to lie on the sphere's surface
	 * otherwise the result is undefined.
	 * \param[out] random The resulting uniformly distributed pair of numbers
	 * between 0 and 1.
	 */
	__host__ __device__
	static void random(const float sample[3], float random[2]) {
		random[1] = (1.f - sample[2]) / 2.f;
		random[0] = atan2(sample[1], sample[0]) / (2.f * PI<float>());
		if (random[0] < 0.f) random[0] += 1.f;
	}

	/*!
	 * \brief
	 * Returns the PDF of uniformly sampling a point on the canonical sphere's
	 * surface with respect to the surface area measure. As the point is
	 * supposed to be on the surface and this is a uniform distribution the
	 * constant value `1.f / (4.f * M_PI)` is always returned.
	 *
	 * \param[in] sample The point on the canonical sphere's surface.
	 *
	 * \return The PDF of sampling the given point with respect to the surface
	 * area measure.
	 */
	__host__ __device__
	static float pdf(const float sample[3]) {
		return PDF;
	}
};

}

#endif /* UNIFORMSPHEREDISTRIBUTION_CUH_ */

#ifndef EXPONENTIALDISTRIBUTION_CUH_
#define EXPONENTIALDISTRIBUTION_CUH_

namespace lucifer {

/*!
 * \brief
 * <a href="https://en.wikipedia.org/wiki/Exponential_distribution">
 * Exponential Distribution.
 * </a>
 *
 * \author
 * Bruno Pera.
 */
class ExponentialDistribution {
public:
	__host__ __device__
	ExponentialDistribution() = delete;

	/*!
	 * \brief
	 * Maps a uniformly distributed random number in [0, 1] to a random number
	 * distributed according to the exponential distribution.
	 *
	 * \param[in] lambda The exponential distribution rate parameter.
	 * \param[in] rnd A uniformly distributed number in [0, 1].
	 * \param[out] pdf If not null holds the PDF of sampling a the resulting
	 * number from the corresponding exponential distribution.
	 *
	 * \return The number distributed according to the exponental distribution.
	 */
	__host__ __device__
	static float sample(const float lambda, const float rnd, float* pdf = nullptr) {
		float value = -logf(1.f - rnd) / lambda;
		if (pdf != nullptr) *pdf = lambda * expf(-lambda * value);
		return value;
	}

	/*!
	 * \brief
	 * Returns the PDF of sampling a value from the exponential distribution.
	 *
	 * \param[in] lambda The exponential distribution rate parameter.
	 * \param[in] value The sampled value.
	 *
	 * \return The PDF of sampling a value from the exponential distribution.
	 */
	__host__ __device__
	static float pdf(const float lambda, const float value) {
		return lambda * expf(-lambda * value);
	}

	/*!
	 * \brief
	 * Return the CDF of the exponential distribition at a given point.
	 *
	 * \param[in] lambda The exponential distribution rate parameter.
	 * \param[in] value The sampled value.
	 *
	 * \return The CDF of the exponential distribition at a given point.
	 */
	__host__ __device__
	static float cdf(const float lambda, const float value) {
		return 1 - expf(-lambda * value);
	}
};

}

#endif /* EXPONENTIALDISTRIBUTION_CUH_ */

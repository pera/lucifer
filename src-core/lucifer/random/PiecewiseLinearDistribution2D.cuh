#ifndef PIECEWISELINEARDISTRIBUTION2D_CUH_
#define PIECEWISELINEARDISTRIBUTION2D_CUH_

#include "PiecewiseLinearDistribution1D.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/memory/Polymorphic.cuh"

namespace lucifer {

/*!
 * \brief
 * Two dimensional piecewise linear distribution.
 *
 * \author
 * Bruno Pera.
 */
class PiecewiseLinearDistribution2D: public Polymorphic {
public:
	using idx1d_t = PiecewiseLinearDistribution1D::index_t;
	using idx2d_t = uint32_t;
	using array_t = Array<float, idx2d_t>;
	friend class TypedPool<PiecewiseLinearDistribution2D>;

private:
	float vstep;
	Pointer<PiecewiseLinearDistribution1D> marginal;
	Array<Pointer<PiecewiseLinearDistribution1D>, uint16_t> conditionals;

private:
	__host__ __device__
	const RangeF& v_range() const {
		return marginal->support();
	}

	__host__ __device__
	const RangeF& u_range() const {
		return conditionals[0]->support();
	}

	__host__
	PiecewiseLinearDistribution2D(MemoryPool& pool, const RangeF& u, const RangeF& v, const idx1d_t nu, const idx1d_t nv, const array_t& values):
	vstep(v.length() / (nv - 1)), conditionals(pool, nv) {
		idx2d_t k = 0;
		PiecewiseLinearDistribution1D::array_t m(pool, nv);
		PiecewiseLinearDistribution1D::array_t c(pool, nu);
		for (idx1d_t i = 0; i < nv; ++i) {
			m[i] = 0.f;
			for (idx1d_t j = 0; j < nu; ++j, ++k) {
				m[i] += values[k];
				c[j]  = values[k];
			}
			conditionals[i] = PiecewiseLinearDistribution1D::build(pool, u, c);
		}
		marginal = PiecewiseLinearDistribution1D::build(pool, v, m);
	}

public:
	__host__
	static Pointer<PiecewiseLinearDistribution2D> build(MemoryPool& pool, const RangeF& u, const RangeF& v, const idx1d_t nu, const idx1d_t nv, const array_t& values) {
		return Pointer<PiecewiseLinearDistribution2D>::make(pool, pool, u, v, nu, nv, values);
	}
	/*!
	 * \brief
	 * Creates a new
	 * <a href="https://en.wikipedia.org/wiki/Piecewise_linear_function">
	 * two dimensaional piecewise linear distribution
	 * </a>.
	 *
	 * \param[in] u The 1st dimension support.
	 * \param[in] v The 2nd dimension support.
	 * \param[in] nu The number of samples in u. Must be at least 2.
	 * \param[in] nv The number of samples in v. Must be at least 2.
	 * \param[in] values The array of nu x nv values on equally spaced
	 * positions on the distribution's support. These values define a piecewise
	 * linear function that will be scaled so that it's integral over the
	 * support is 1 so that it can be used as PDF. Every element must be non
	 * negative otherwise the behaviour is undefined.
	 */

	/*!
	 * \brief
	 * Computes the PDF at a given point.
	 *
	 * \param sample The point in the distribution's domain.
	 * \return The PDF at the given point.
	 */
	__host__ __device__
	float pdf(const float sample[2]) const {
		if (!u_range().contains(sample[0]) or !v_range().contains(sample[1])) return 0.f;
		idx1d_t j = min((sample[1] - v_range().lower) / vstep, conditionals.size() - 2);
		float w = (sample[1] - (v_range().lower + j * vstep)) / vstep;
		return marginal->pdf(sample[1]) * lerp(conditionals[j]->pdf(sample[0]), conditionals[j + 1]->pdf(sample[0]), w);
	}

	/*!
	 * \brief
	 * Samples a position on the distribution's support.
	 *
	 * \param[in] random A pair of randomly distributed numbers in [0, 1].
	 * \param[out] sampled The sampled position on the support.
	 * \param[out] pdf If not null holds the PDF at the sampled position.
	 */
	__host__ __device__
	void sample(const float random[2], float sample[2], float* pdf = nullptr) const {
		sample[1] = marginal->sample(random[1]);
		idx1d_t j = min((sample[1] - v_range().lower) / vstep, conditionals.size() - 2);
		float w = (sample[1] - (v_range().lower + j * vstep)) / vstep;
		sample[0] =
				random[0] > w ?
				conditionals[j]->sample((random[0] - w) / (1.f - w)) :
				conditionals[j]->sample(random[0] / w);
		if (pdf != nullptr) {
			*pdf = marginal->pdf(sample[1]) * lerp(conditionals[j]->pdf(sample[0]), conditionals[j + 1]->pdf(sample[0]), w);
		}
	}
};

}

#endif /* PIECEWISELINEARDISTRIBUTION2D_CUH_ */

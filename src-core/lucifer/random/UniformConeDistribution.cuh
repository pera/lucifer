#ifndef UNIFORMCONEDISTRIBUTION_CUH_
#define UNIFORMCONEDISTRIBUTION_CUH_

#include "lucifer/math/Math.cuh"

namespace lucifer {

/*!
 * \brief
 * A uniform distribution of points over a spherical cap of the unit sphere
 * centered at the origin. The cap is defined by the cosine of the max angle
 * with the positive z axis.
 *
 * \author
 * Bruno Pera.
 */
class UniformConeDistribution {
public:
	__host__ __device__
	UniformConeDistribution() = delete;

	/*!
	 * \brief
	 * Maps a pair o uniformly distributed numbers in the interval [0, 1] onto
	 * the surface of a spherical cap of the unit sphere centered at the
	 * origin.
	 *
	 * \param[in] random A pair of numbers in [0, 1].
	 * \param[in] cosThetaMax Cosine of the max angle with the positive z axis.
	 * \param[out The resulting (x, y, z) coordinates of a point on the
	 * spherical cap of the canonical sphere. As the given pair `random`
	 * uniformly varies between 0 and 1 the resulting `sample` will uniformly
	 *  vary on cap's suface.
	 */
	__host__ __device__
	static void sample(const float random[2], const float cosThetaMax, float sample[3], float* pdf) {
	    float cosTheta = (1.f - random[0]) + random[0] * cosThetaMax;
	    float sinTheta = sqrt(1.f - cosTheta * cosTheta);
	    float phi = random[1] * 2 * PI<float>();
	    sample[0] = cos(phi) * sinTheta;
	    sample[1] = sin(phi) * sinTheta;
	    sample[2] = cosTheta;
	    if (pdf != nullptr) *pdf = 1.f / (2.f * PI<float>() * (1.f - cosThetaMax));
	}

	/*!
	 * \brief
	 * Returns the PDF of uniformly sampling a point on the the spherical cap
	 * of the canonical sphere with respect to the surface area measure.
	 *
	 * * <a href=
	 * "https://en.wikipedia.org/wiki/Spherical_cap#Volume_and_surface_area">
	 * Spherical cap surface area.
	 * </a>
	 *
	 * \param[in] sample The point on the canonical cap's surface. The point is
	 * supposed to be in the canonical sphere's surface but not necessarily on
	 * the cap.
	 * \param[in] cosThetaMax Cosine of the max angle with the positive z axis.
	 *
	 * \return The PDF of sampling the given point with respect to the surface
	 * area measure.
	 */
	__host__ __device__
	static float pdf(const float sample[3], const float cosThetaMax) {
		return sample[2] <= cosThetaMax ? 0.f : 1.f / (2.f * PI<float>() * (1.f - cosThetaMax));
	}
};

}

#endif /* UNIFORMCONEDISTRIBUTION_CUH_ */

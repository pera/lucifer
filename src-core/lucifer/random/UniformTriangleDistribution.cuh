#ifndef UNIFORMTRIANGLEDISTRIBUTION_CUH_
#define UNIFORMTRIANGLEDISTRIBUTION_CUH_

#include <cassert>
#include "lucifer/math/geometry/CommonTypes.cuh"

namespace lucifer {

/*!
 * \brief
 * Uniform distributions of points along the surface of a 3D triangle.
 *
 * \author
 * Bruno Pera.
 */
class UniformTriangleDistribution {
private:
	__host__ __device__
	static Point2F uvCoordinates(const Vectr3F& dp, const Vectr3F& u, const Vectr3F& v, const Norml3F& n, Point3F::index_t axis0, Point3F::index_t axis1) {
		auto dt =   v[axis1] *  u[axis0] - v[axis0] *  u[axis1];
		auto b1 = ( v[axis1] * dp[axis0] - v[axis0] * dp[axis1]) / dt;
		auto b2 = (-u[axis1] * dp[axis0] + u[axis0] * dp[axis1]) / dt;
		return Point2F(1.f - (b1 + b2), b1);
	}

	__host__ __device__
	static Point2F uvCoordinates(const Vectr3F& dp, const Vectr3F& u, const Vectr3F& v, const Norml3F& n) {
		switch (n.maxAbsAxis()) {
			case 0:
				return uvCoordinates(dp, u, v, n, 1, 2);
			case 1:
				return uvCoordinates(dp, u, v, n, 2, 0);
			case 2:
				return uvCoordinates(dp, u, v, n, 0, 1);
		}
		assert(false);
		return Point2F(0.f, 0.f);
	}

public:
	__host__ __device__
	UniformTriangleDistribution() = delete;

	/*!
	 * \brief
	 * Maps a pair o uniformly distributed numbers in the interval [0, 1] into
	 * points on the surface of a 3D triangle defined by a vertex and two
	 * edges.
	 *
	 * \param[in] p A vertex of the triangle.
	 * \param[in] u The 1st edge starting at `p`.
	 * \param[in] v The 2nd edge starting at `p`.
	 * \param[in] random A pair of numbers in [0, 1].
	 * \param[out] sample The resulting (x, y, z) coordinates of the point on
	 * the surface of the triangle. As the given pair `random` uniformly varies
	 * between 0 and 1 the resulting `sample` will uniformly vary on triangle's
	 * suface.
	 * \param[out] b1 If not null holds the 1st barycentric coordinate of the
	 * mapped point.
	 * \param[out] b2 If not null holds the 2nd barycentric coordinate of the
	 * mapped point.
	 */
	__host__ __device__
	static void sample(const Point3F& p, const Vectr3F& u, const Vectr3F& v, const float random[2], float sample[3], float* b1 = nullptr, float* b2 = nullptr) {
	    float s = sqrt(random[0]);
	    float l1 = 1.f - s;
	    float l2 = s * (1.f - random[1]);
	    float l3 = s * (0.f + random[1]);
	    sample[0] = (l1 * p[0]) + (l2 * (p[0] + u[0])) + (l3 * (p[0] + v[0]));
	    sample[1] = (l1 * p[1]) + (l2 * (p[1] + u[1])) + (l3 * (p[1] + v[1]));
	    sample[2] = (l1 * p[2]) + (l2 * (p[2] + u[2])) + (l3 * (p[2] + v[2]));
	    if (b1 != nullptr) *b1 = l1;
	    if (b2 != nullptr) *b2 = l2;
	}

	/*!
	 * \brief
	 * Maps a pair o uniformly distributed numbers in the interval [0, 1] into
	 * points on the surface of a 3D triangle defined by three vertexes.
	 *
	 * \param[in] p1 The 1st vertex.
	 * \param[in] p2 The 2nd vertex.
	 * \param[in] p3 The 3rd vertex.
	 * \param[in] random A pair of numbers in [0, 1].
	 * \param[out] sample The resulting (x, y, z) coordinates of the point on
	 * the surface of the triangle. As the given pair `random` uniformly varies
	 * between 0 and 1 the resulting `sample` will uniformly vary on triangle's
	 * suface.
	 * \param[out] b1 If not null holds the 1st barycentric coordinate of the
	 * mapped point.
	 * \param[out] b2 If not null holds the 2nd barycentric coordinate of the
	 * mapped point.
	 */
	__host__ __device__
	static void sample(const Point3F& p1, const Point3F& p2, const Point3F& p3, const float random[2], float sample[3], float* b1 = nullptr, float* b2 = nullptr) {
	    float s = sqrt(random[0]);
	    float l1 = 1.f - s;
	    float l2 = s * (1.f - random[1]);
	    float l3 = s * (0.f + random[1]);
	    sample[0] = l1 * p1[0] + l2 * p2[0] + l3 * p3[0];
	    sample[1] = l1 * p1[1] + l2 * p2[1] + l3 * p3[1];
	    sample[2] = l1 * p1[2] + l2 * p2[2] + l3 * p3[2];
	    if (b1 != nullptr) *b1 = l1;
	    if (b2 != nullptr) *b2 = l2;
	}

	/*!
	 * \brief
	 * The inverse of the `sample` method. Maps a uniformly distributed point
	 * on the the triangle's surface into a pair of uniformly distributed
	 * numbers in [0, 1]. Is the exact inverse of the corresponding `sample`
	 * method.
	 *
	 * \param[in] p A vertex of the triangle.
	 * \param[in] u The 1st edge starting at `p`.
	 * \param[in] v The 2nd edge starting at `p`.
	 * \param[in] n The pre computed triangle unit-length normal vector.
	 * \param[in] s The point on the triangle's surface. The point is supposed
	 * to lie on the triangle's surface otherwise the result is undefined.
	 * \param[out] rnd The resulting uniformly distributed pair of numbers
	 * between 0 and 1.
	 */
	__host__ __device__
	static void random(const Point3F& p, const Vectr3F& u, const Vectr3F& v, const Norml3F& n, const Point3F& s, float rnd[2]) {
		Point2F uv = uvCoordinates(s - p, u, v, n);
		rnd[0] = (1.f - uv[0]) * (1.f - uv[0]);
		rnd[1] = 1.f - uv[1] / (1.f - uv[0]);
	}

	/*!
	 * \brief
	 * The inverse of the `sample` method. Maps a uniformly distributed point
	 * on the the triangle's surface into a pair of uniformly distributed
	 * numbers in [0, 1]. Is the exact inverse of the corresponding `sample`
	 * method.
	 *
	 * \param[in] p1 The 1st vertex.
	 * \param[in] p2 The 2nd vertex.
	 * \param[in] p3 The 3rd vertex.
	 * \param[in] n The pre computed triangle unit-length normal vector.
	 * \param[in] s The point on the triangle's surface. The point is supposed
	 * to lie on the triangle's surface otherwise the result is undefined.
	 * \param[out] rnd The resulting uniformly distributed pair of numbers
	 * between 0 and 1.
	 */
	__host__ __device__
	static void random(const Point3F& p1, const Point3F& p2, const Point3F& p3, const Norml3F& n, const Point3F& s, float rnd[2]) {
		random(p1, p2 - p1, p3 - p1, n, s, rnd);
	}

	__host__ __device__
	static Point2F barycentricCoordinates(const Point3F& p, const Vectr3F& u, const Vectr3F& v, const Norml3F& n, const Point3F& s) {
		return uvCoordinates(s - p, u, v, n);
	}
};

}

#endif /* UNIFORMTRIANGLEDISTRIBUTION_CUH_ */

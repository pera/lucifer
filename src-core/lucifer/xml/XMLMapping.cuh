#ifndef XMLMAPPING_CUH_
#define XMLMAPPING_CUH_

#include "Library.cuh"
#include "lucifer/math/geometry/PlannarUVMapping.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class XMLMapping {
public:
	__host__
	XMLMapping() = delete;

	__host__
	static Pointer<const UVMapping> load(MemoryPool& pool, const luc::plannar_uv_mapping& mapping) {
		return Pointer<const UVMapping>(PlanarUVMapping::build(pool, PlanarUVMapping::Axes(mapping.axes()[0])));
	}

	__host__
	static Pointer<const UVMapping> load(MemoryPool& pool, const luc::uv_mapper& mapper) {
		return load(pool, mapper.plannar());
	}
};

}

#endif /* XMLMAPPING_CUH_ */

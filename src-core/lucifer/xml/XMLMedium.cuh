#ifndef XMLMEDIUM_CUH_
#define XMLMEDIUM_CUH_

#include "XMLShadingNode.cuh"
#include "lucifer/medium/OpacityNode.cuh"
#include "lucifer/medium/HeterogeneousMedium.cuh"
#include "lucifer/medium/HomogeneousMedium.cuh"
#include "lucifer/medium/HenyeyGreensteinPhaseFunction.cuh"
#include "lucifer/medium/IsotropicPhaseFunction.cuh"
#include "lucifer/medium/MixedPhaseFunction.cuh"
#include "lucifer/medium/RayleighPhaseFunction.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/xml/generated/lucifer.hxx"

namespace lucifer {

class XMLMedium {
private:
	__host__
	XMLMedium() {
	}

public:
	static Pointer<const OpacityNode> loadAtt(MemoryPool& pool, luc::shading_float_node& opacity, Library* library) {
		return Pointer<const OpacityNode>(AttenuationNode::build(pool, XMLShadingNode::load(pool, opacity, library)));
	}

	static Pointer<const OpacityNode> loadExt(MemoryPool& pool, luc::shading_float_node& opacity, Library* library) {
		return Pointer<const OpacityNode>(ExtinctionNode::build(pool, XMLShadingNode::load(pool, opacity, library)));
	}

	static Pointer<const OpacityNode> load(MemoryPool& pool, luc::opacity& opacity, Library* library) {
		if (opacity.attenuation().present()) {
			return loadAtt(pool, opacity.attenuation().get(), library);
		}
		if (opacity.extinction().present()) {
			return loadExt(pool, opacity.extinction().get(), library);
		}
		return Pointer<const OpacityNode>();
	}

	__host__
	static Pointer<const PhaseFunction> load(MemoryPool& pool, luc::isotropic_phase_function& phase) {
		return Pointer<const PhaseFunction>(IsotropicPhaseFunction::build(pool));
	}

	__host__
	static Pointer<const PhaseFunction> load(MemoryPool& pool, luc::rayleigh_phase_function& phase) {
		return Pointer<const PhaseFunction>(RayleighPhaseFunction::build(pool));
	}

	__host__
	static Pointer<const PhaseFunction> load(MemoryPool& pool, luc::henyey_greenstein_phase_function& phase, Library* library) {
		auto g = XMLShadingNode::load(pool, phase.asymmetry(), library);
		return Pointer<const PhaseFunction>(HenyeyGreensteinPhaseFunction::build(pool, g));
	}

	__host__
	static Pointer<const PhaseFunction> load(MemoryPool& pool, luc::mixed_phase_function& phase, Library* library) {
		using index_t = MixedPhaseFunction::index_t;
		index_t n = phase.mix_element().size();
		MixedPhaseFunction::array_f w(pool, n);
		MixedPhaseFunction::array_p p(pool, n);
		for (index_t i = 0; i < n; i++) {
			w[i] = phase.mix_element()[i].weight();
			p[i] = load(pool, phase.mix_element()[i].phase_function(), library);
		}
		return Pointer<const PhaseFunction>(MixedPhaseFunction::build(pool, w, p));
	}

	__host__
	static Pointer<const PhaseFunction> load(MemoryPool& pool, luc::phase_function& phase, Library* library) {
		Pointer<const PhaseFunction> result;
		if (phase.isotropic().present()) {
			result = load(pool, phase.isotropic().get());
		}
		if (phase.rayleigh().present()) {
			result = load(pool, phase.rayleigh().get());
		}
		if (phase.henyey_greenstein().present()) {
			result = load(pool, phase.henyey_greenstein().get(), library);
		}
		if (phase.mixed().present()) {
			result = load(pool, phase.mixed().get(), library);
		}
		if (phase.reference().present()) {
			auto pair = library->phaseFunctionMap.find(phase.reference().get().referenced_id());
			if (pair == library->phaseFunctionMap.end()) {
				std::cerr << "could not find phase function: " << phase.reference().get().referenced_id() << std::endl;
			} else {
				result = pair->second;
			}
		}
		if (!result.isNull()) {
			if (phase.id().present()) {
				library->phaseFunctionMap[phase.id().get()] = result;
			}
		}
		return result;
	}

	__host__
	static Pointer<const Medium> load(MemoryPool& pool, luc::homogeneous_medium& medium, Library* library) {
		Pointer<const OpacityNode> opacity;
		if (medium.opacity().present()) {
			opacity = load(pool, medium.opacity().get(), library);
		} else {
			opacity = Pointer<const OpacityNode>(
				AttenuationNode::build(
					pool,
					Pointer<const ShadingNode<float>>(ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.f))
				)
			);
		}
		Pointer<const ShadingNode<float>> scattering;
		if (medium.scattering().present()) {
			scattering = XMLShadingNode::load(pool, medium.scattering().get(), library);
		} else {
			scattering = Pointer<const ShadingNode<float>>(ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.f));
		}
		Pointer<const ShadingNode<float>> refractiveIndex;
		if (medium.refractive_index().present()) {
			refractiveIndex = XMLShadingNode::load(pool, medium.refractive_index().get(), library);
		} else {
			refractiveIndex = Pointer<const ShadingNode<float>>(ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f));
		}
		Pointer<const PhaseFunction> phaseFunction;
		if (medium.phase_function().present()) {
			phaseFunction = load(pool, medium.phase_function().get(), library);
		} else {
			phaseFunction = Pointer<const PhaseFunction>(IsotropicPhaseFunction::build(pool));
		}
		bool volumetric = medium.volumetric();
		uint8_t priority = medium.priority();
		return Pointer<const Medium>(
			HomogeneousMedium::build(
				pool,
				opacity,
				scattering,
				refractiveIndex,
				phaseFunction,
				volumetric,
				priority
			)
		);
	}

	__host__
	static Pointer<const Medium> load(MemoryPool& pool, luc::heterogeneous_medium& medium, Library* library) {
		Pointer<const OpacityNode> opacity;
		if (medium.opacity().present()) {
			opacity = load(pool, medium.opacity().get(), library);
		} else {
			opacity = Pointer<const OpacityNode>(
				AttenuationNode::build(
					pool,
					Pointer<const ShadingNode<float>>(ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.f))
				)
			);
		}
		Pointer<const ShadingNode<float>> scattering;
		if (medium.scattering().present()) {
			scattering = XMLShadingNode::load(pool, medium.scattering().get(), library);
		} else {
			scattering = Pointer<const ShadingNode<float>>(
				ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.f)
			);
		}
		Pointer<const ShadingNode<float>> refractiveIndex;
		if (medium.refractive_index().present()) {
			refractiveIndex = XMLShadingNode::load(pool, medium.refractive_index().get(), library);
		} else {
			refractiveIndex = Pointer<const ShadingNode<float>>(
				ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f)
			);
		}
		Pointer<const PhaseFunction> phaseFunction;
		if (medium.phase_function().present()) {
			phaseFunction = load(pool, medium.phase_function().get(), library);
		} else {
			phaseFunction = Pointer<const PhaseFunction>(IsotropicPhaseFunction::build(pool));
		}
		bool volumetric = medium.volumetric();
		uint8_t priority = medium.priority();
		return Pointer<const Medium>(
			HeterogeneousMedium::build(
				pool,
				opacity,
				scattering,
				refractiveIndex,
				phaseFunction,
				volumetric,
				priority
			)
		);
	}

	__host__
	static Pointer<const Medium> load(MemoryPool& pool, luc::medium& medium, Library* library) {
		Pointer<const Medium> result;
		if (medium.homogeneous().present()) {
			result = load(pool, medium.homogeneous().get(), library);
		}
		if (medium.heterogeneous().present()) {
			result = load(pool, medium.heterogeneous().get(), library);
		}
		if (medium.reference().present()) {
			auto pair = library->mediumMap.find(medium.reference().get().referenced_id());
			if (pair == library->mediumMap.end()) {
				std::cerr << "could not find medium: " << medium.reference().get().referenced_id() << std::endl;
			} else {
				result = pair->second;
			}
		}
		if (!result.isNull()) {
			if (medium.id().present()) {
				library->mediumMap[medium.id().get()] = result;
			}
		}
		return result;
	}

};

}

#endif /* XMLMEDIUM_CUH_ */

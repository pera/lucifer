#ifndef XMLSCATTERING_CUH_
#define XMLSCATTERING_CUH_

#include <tuple>
#include "XMLShadingNode.cuh"
#include "lucifer/bsdf/BSDF.cuh"
#include "lucifer/bsdf/InvisibleBTDF.cuh"
#include "lucifer/bsdf/LambertianBRDF.cuh"
#include "lucifer/bsdf/LambertianBTDF.cuh"
#include "lucifer/bsdf/MicrofacetBRDF.cuh"
#include "lucifer/bsdf/MicrofacetBTDF.cuh"
#include "lucifer/bsdf/MicrofacetBSDF.cuh"
#include "lucifer/bsdf/OrenNayarBRDF.cuh"
#include "lucifer/bsdf/SpecularBRDF.cuh"
#include "lucifer/bsdf/SpecularBSDF.cuh"
#include "lucifer/bsdf/SpecularBTDF.cuh"
#include "lucifer/bsdf/SubstrateBRDF.cuh"
#include "lucifer/bsdf/microfacet/SmithTrowbridgeReitzModel.cuh"
#include "lucifer/bsdf/multiple-scattering/ConductorMicrosurfaceBRDF.cuh"
#include "lucifer/bsdf/multiple-scattering/DielectricMicrosurfaceBSDF.cuh"
#include "lucifer/bsdf/multiple-scattering/DiffuseMicrosurfaceBRDF.cuh"
#include "lucifer/bsdf/normal-map/DoubleScatterNormalMapBRDF.cuh"
#include "lucifer/bsdf/normal-map/MicrosurfaceNormalMapBRDF.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/xml/generated/lucifer.hxx"

namespace lucifer {

class XMLScattering {
private:
	__host__
	XMLScattering() {
	}

public:
	__host__
	static std::pair<Pointer<const ShadingNode<float>>, Pointer<const ShadingNode<float>>> load(MemoryPool& pool, luc::roughness& roughness, Library* library) {
		if (roughness.isotropic().present()) {
			auto r = XMLShadingNode::load(pool, roughness.isotropic().get(), library);
			return std::pair<Pointer<const ShadingNode<float>>, Pointer<const ShadingNode<float>>>(r, r);
		}
		auto u = XMLShadingNode::load(pool, roughness.anisotropic_u().get(), library);
		auto v = XMLShadingNode::load(pool, roughness.anisotropic_v().get(), library);
		return std::pair<Pointer<const ShadingNode<float>>, Pointer<const ShadingNode<float>>>(u, v);
	}

	__host__
	static Pointer<const BSDF> load(MemoryPool& pool, luc::invisible& invisible, Library* library) {
		Pointer<BSDF> bsdf = BSDF::build(pool, 1);
		(*bsdf)[0] = Pointer<const BxDF>(InvisibleBTDF::build(pool));
		return Pointer<const BSDF>(bsdf);
	}

	__host__
	static Pointer<const BSDF> load(MemoryPool& pool, luc::matte& matte, Library* library) {
		Pointer<BSDF> bsdf = BSDF::build(pool, 1);
		auto reflectance = XMLShadingNode::load(pool, matte.reflectance(), library);
		if (matte.sigma().present()) {
			auto sigma = XMLShadingNode::load(pool, matte.sigma().get(), library);
			(*bsdf)[0] = Pointer<const BxDF>(OrenNayarBRDF::build(pool, reflectance, sigma));
		} else {
			(*bsdf)[0] = Pointer<const BxDF>(LambertianBRDF::build(pool, reflectance));
		}
		return Pointer<const BSDF>(bsdf);
	}

	__host__
	static Pointer<const BSDF> load(MemoryPool& pool, luc::plastic& plastic, Library* library) {
		Pointer<BSDF> bsdf = BSDF::build(pool, 2);
		auto diffuse = XMLShadingNode::load(pool, plastic.diffuse(), library);
		if (plastic.sigma().present()) {
			auto sigma = XMLShadingNode::load(pool, plastic.sigma().get(), library);
			(*bsdf)[0] = Pointer<const BxDF>(OrenNayarBRDF::build(pool, diffuse, sigma));
		} else {
			(*bsdf)[0] = Pointer<const BxDF>(LambertianBRDF::build(pool, diffuse));
		}
		auto specular = XMLShadingNode::load(pool, plastic.specular(), library);
		if (!plastic.roughness().present()) {
			(*bsdf)[1] = Pointer<const BxDF>(SpecularBRDF::build(pool, specular, true));
		}
		else {
			auto roughness = load(pool, plastic.roughness().get(), library);
			auto distribution = Pointer<const MicrofacetModel>(SmithTrowbridgeReitzModel::build(pool, roughness.first, roughness.second));
			(*bsdf)[1] = Pointer<const BxDF>(MicrofacetBRDF::build(pool, specular, distribution, true));
		}
		return Pointer<const BSDF>(bsdf);
	}

	__host__
	static Pointer<const BSDF> load(MemoryPool& pool, luc::metal& metal, Library* library) {
		Pointer<BSDF> bsdf = BSDF::build(pool, 1);
		auto reflection = Pointer<const ShadingNode<float>>(ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f));
		if (!metal.roughness().present()) {
			(*bsdf)[0] = Pointer<const BxDF>(SpecularBRDF::build(pool, reflection, true));
		} else {
			auto roughness = load(pool, metal.roughness().get(), library);
			auto distribution = Pointer<const MicrofacetModel>(SmithTrowbridgeReitzModel::build(pool, roughness.first, roughness.second));
			(*bsdf)[0] = Pointer<const BxDF>(MicrofacetBRDF::build(pool, reflection, distribution, true));
		}
		return Pointer<const BSDF>(bsdf);
	}

	__host__
	static Pointer<const BSDF> load(MemoryPool& pool, luc::substrate& substrate, Library* library) {
		Pointer<BSDF> bsdf = BSDF::build(pool, 1);
		auto diffuse = XMLShadingNode::load(pool, substrate.diffuse(), library);
		auto specular = XMLShadingNode::load(pool, substrate.specular(), library);
		auto roughness = load(pool, substrate.roughness(), library);
		auto distribution = Pointer<const MicrofacetModel>(SmithTrowbridgeReitzModel::build(pool, roughness.first, roughness.second));
		(*bsdf)[0] = Pointer<const BxDF>(SubstrateBRDF::build(pool, diffuse, specular, distribution));
		return Pointer<const BSDF>(bsdf);
	}

	__host__
	static Pointer<const BSDF> load(MemoryPool& pool, luc::glass& glass, Library* library) {
		Pointer<BSDF> bsdf = BSDF::build(pool, 1);
		auto reflectance = XMLShadingNode::load(pool, glass.reflectance(), library);
		auto transmittance = XMLShadingNode::load(pool, glass.transmittance(), library);
		if (!glass.roughness().present()) {
			(*bsdf)[0] = Pointer<const BxDF>(SpecularBSDF::build(pool, reflectance, transmittance));
		} else {
			auto roughness = load(pool, glass.roughness().get(), library);
			auto distribution = Pointer<const MicrofacetModel>(SmithTrowbridgeReitzModel::build(pool, roughness.first, roughness.second));
			(*bsdf)[0] = Pointer<const BxDF>(MicrofacetBSDF::build(pool, reflectance, transmittance, distribution));
		}
		return Pointer<const BSDF>(bsdf);
	}

	__host__
	static Pointer<const BxDF> load(MemoryPool& pool, luc::lambertian_brdf& lambertian, Library* library) {
		auto reflectance = XMLShadingNode::load(pool, lambertian.reflectance(), library);
		return Pointer<const BxDF>(LambertianBRDF::build(pool, reflectance));
	}

	__host__
	static Pointer<const BxDF> load(MemoryPool& pool, luc::lambertian_btdf& lambertian, Library* library) {
		auto transmittance = XMLShadingNode::load(pool, lambertian.transmittance(), library);
		return Pointer<const BxDF>(LambertianBTDF::build(pool, transmittance));
	}

	__host__
	static Pointer<const BxDF> load(MemoryPool& pool, luc::oren_nayar_brdf& oren_nayar, Library* library) {
		auto reflectance = XMLShadingNode::load(pool, oren_nayar.reflectance(), library);
		auto sigma = XMLShadingNode::load(pool, oren_nayar.sigma(), library);
		return Pointer<const BxDF>(OrenNayarBRDF::build(pool, reflectance, sigma));
	}

	__host__
	static Pointer<const BxDF> load(MemoryPool& pool, luc::specular_brdf& brdf, Library* library) {
		auto reflectance = XMLShadingNode::load(pool, brdf.reflectance(), library);
		return Pointer<const BxDF>(SpecularBRDF::build(pool, reflectance, brdf.fresnel()));
	}

	__host__
	static Pointer<const BxDF> load(MemoryPool& pool, luc::specular_btdf& btdf, Library* library) {
		auto transmittance = XMLShadingNode::load(pool, btdf.transmittance(), library);
		return Pointer<const BxDF>(SpecularBTDF::build(pool, transmittance, btdf.fresnel()));
	}

	__host__
	static Pointer<const BxDF> load(MemoryPool& pool, luc::specular_bsdf& bsdf, Library* library) {
		auto reflectance = XMLShadingNode::load(pool, bsdf.reflectance(), library);
		auto transmittance = XMLShadingNode::load(pool, bsdf.transmittance(), library);
		return Pointer<const BxDF>(SpecularBSDF::build(pool, reflectance, transmittance));
	}

	__host__
	static Pointer<const MicrofacetModel> load(MemoryPool& pool, luc::trowbridge_reitz_distribution& distribution, Library* library) {
		auto roughness = load(pool, distribution.roughness(), library);
		return Pointer<const MicrofacetModel>(SmithTrowbridgeReitzModel::build(pool, roughness.first, roughness.second));
	}

	__host__
	static Pointer<const BxDF> load(MemoryPool& pool, luc::microfacet_brdf& brdf, Library* library) {
		auto reflectance = XMLShadingNode::load(pool, brdf.reflectance(), library);
		auto distribution = load(pool, brdf.distribution(), library);
		return Pointer<const BxDF>(MicrofacetBRDF::build(pool, reflectance, distribution, brdf.fresnel()));
	}

	__host__
	static Pointer<const BxDF> load(MemoryPool& pool, luc::microfacet_btdf& btdf, Library* library) {
		auto transmittance = XMLShadingNode::load(pool, btdf.transmittance(), library);
		auto distribution = load(pool, btdf.distribution(), library);
		return Pointer<const BxDF>(MicrofacetBTDF::build(pool, transmittance, distribution, btdf.fresnel()));
	}

	__host__
	static Pointer<const BxDF> load(MemoryPool& pool, luc::microfacet_bsdf& bsdf, Library* library) {
		auto reflectance = XMLShadingNode::load(pool, bsdf.reflectance(), library);
		auto transmittance = XMLShadingNode::load(pool, bsdf.transmittance(), library);
		auto distribution = load(pool, bsdf.distribution(), library);
		return Pointer<const BxDF>(MicrofacetBSDF::build(pool, reflectance, transmittance, distribution));
	}

	__host__
	static Pointer<const BxDF> load(MemoryPool& pool, luc::substrate_brdf& brdf, Library* library) {
		auto diffuse = XMLShadingNode::load(pool, brdf.diffuse(), library);
		auto specular = XMLShadingNode::load(pool, brdf.specular(), library);
		auto distribution = load(pool, brdf.distribution(), library);
		return Pointer<const BxDF>(SubstrateBRDF::build(pool, diffuse, specular, distribution));
	}

	__host__
	static Pointer<const BxDF> load(MemoryPool& pool, luc::diffuse_microsurface_brdf& brdf, Library* library) {
		auto reflectance = XMLShadingNode::load(pool, brdf.reflectance(), library);
		auto roughnessX = XMLShadingNode::load(pool, brdf.roughness_x(), library);
		auto roughnessY = XMLShadingNode::load(pool, brdf.roughness_y(), library);
		auto scattering = brdf.max_scattering_order();
		return Pointer<const BxDF>(DiffuseMicrosurfaceBRDF::build(pool, reflectance, roughnessX, roughnessY, scattering));
	}

	__host__
	static Pointer<const BxDF> load(MemoryPool& pool, luc::conductor_microsurface_brdf& brdf, Library* library) {
		auto reflectance = XMLShadingNode::load(pool, brdf.reflectance(), library);
		auto roughnessX = XMLShadingNode::load(pool, brdf.roughness_x(), library);
		auto roughnessY = XMLShadingNode::load(pool, brdf.roughness_y(), library);
		auto fresnel = brdf.fresnel();
		auto uniformPDF = brdf.uniform_pdf();
		auto scattering = brdf.max_scattering_order();
		return Pointer<const BxDF>(ConductorMicrosurfaceBRDF::build(pool, reflectance, roughnessX, roughnessY, fresnel, uniformPDF, scattering));
	}

	__host__
	static Pointer<const BxDF> load(MemoryPool& pool, luc::dielectric_microsurface_bsdf& brdf, Library* library) {
		auto reflectance = XMLShadingNode::load(pool, brdf.reflectance(), library);
		auto transmittance = XMLShadingNode::load(pool, brdf.transmittance(), library);
		auto roughnessX = XMLShadingNode::load(pool, brdf.roughness_x(), library);
		auto roughnessY = XMLShadingNode::load(pool, brdf.roughness_y(), library);
		auto uniformPDF = brdf.uniform_pdf();
		auto scattering = brdf.max_scattering_order();
		return Pointer<const BxDF>(DielectricMicrosurfaceBSDF::build(pool, reflectance, transmittance, roughnessX, roughnessY, uniformPDF, scattering));
	}

	__host__
	static Pointer<const BxDF> load(MemoryPool& pool, luc::multiple_scatter_normal_map_bsdf& bsdf, Library* library) {
		auto bxdf = load(pool, bsdf.bsdf(), library);
		auto map = XMLShadingNode::load(pool, bsdf.normal_map(), library);
		auto uniformPDF = bsdf.uniform_pdf();
		auto scattering = bsdf.max_scattering_order();
		return MicrosurfaceNormalMapBRDF::build(pool, bxdf, map, uniformPDF, scattering);
	}

	__host__
	static Pointer<const BxDF> load(MemoryPool& pool, luc::double_scatter_normal_map_bsdf& brdf, Library* library) {
		auto bxdf = load(pool, brdf.bsdf(), library);
		auto map = XMLShadingNode::load(pool, brdf.normal_map(), library);
		return DoubleScatterNormalMapBRDF::build(pool, bxdf, map);
	}

	__host__
	static Pointer<const BxDF> load(MemoryPool& pool, luc::bsdf& bsdf, Library* library) {
		if (bsdf.lambertian_brdf().present()) {
			return load(pool, bsdf.lambertian_brdf().get(), library);
		}
		if (bsdf.diffuse_microsurface_brdf().present()) {
			return load(pool, bsdf.diffuse_microsurface_brdf().get(), library);
		}
		if (bsdf.conductor_microsurface_brdf().present()) {
			return load(pool, bsdf.conductor_microsurface_brdf().get(), library);
		}
		if (bsdf.dielectric_microsurface_bsdf().present()) {
			return load(pool, bsdf.dielectric_microsurface_bsdf().get(), library);
		}
		if (bsdf.multiple_scatter_normal_map_bsdf().present()) {
			return load(pool, bsdf.multiple_scatter_normal_map_bsdf().get(), library);
		}
		if (bsdf.double_scatter_normal_map_bsdf().present()) {
			return load(pool, bsdf.double_scatter_normal_map_bsdf().get(), library);
		}
		return Pointer<const BxDF>{};
	}

	__host__
	static Pointer<const BSDF> load(MemoryPool& pool, luc::raw_scattering& raw, Library* library) {
		size_t n = 0;
		n += raw.lambertian_brdf().size();
		n += raw.lambertian_btdf().size();
		n += raw.oren_nayar_brdf().size();
		n += raw.specular_brdf().size();
		n += raw.specular_btdf().size();
		n += raw.specular_bsdf().size();
		n += raw.microfacet_brdf().size();
		n += raw.microfacet_btdf().size();
		n += raw.microfacet_bsdf().size();
		n += raw.substrate_brdf().size();
		n += raw.diffuse_microsurface_brdf().size();
		n += raw.conductor_microsurface_brdf().size();
		n += raw.dielectric_microsurface_bsdf().size();
		n += raw.multiple_scatter_normal_map_bsdf().size();
		n += raw.double_scatter_normal_map_bsdf().size();
		Pointer<BSDF> bsdf = BSDF::build(pool, n);
		size_t i = 0;
		for (auto it = raw.lambertian_brdf().begin(); it != raw.lambertian_brdf().end(); it++) {
			(*bsdf)[i++] = load(pool, *it, library);
		}
		for (auto it = raw.lambertian_btdf().begin(); it != raw.lambertian_btdf().end(); it++) {
			(*bsdf)[i++] = load(pool, *it, library);
		}
		for (auto it = raw.oren_nayar_brdf().begin(); it != raw.oren_nayar_brdf().end(); it++) {
			(*bsdf)[i++] = load(pool, *it, library);
		}
		for (auto it = raw.specular_brdf().begin(); it != raw.specular_brdf().end(); it++) {
			(*bsdf)[i++] = load(pool, *it, library);
		}
		for (auto it = raw.specular_btdf().begin(); it != raw.specular_btdf().end(); it++) {
			(*bsdf)[i++] = load(pool, *it, library);
		}
		for (auto it = raw.specular_bsdf().begin(); it != raw.specular_bsdf().end(); it++) {
			(*bsdf)[i++] = load(pool, *it, library);
		}
		for (auto it = raw.microfacet_brdf().begin(); it != raw.microfacet_brdf().end(); it++) {
			(*bsdf)[i++] = load(pool, *it, library);
		}
		for (auto it = raw.microfacet_btdf().begin(); it != raw.microfacet_btdf().end(); it++) {
			(*bsdf)[i++] = load(pool, *it, library);
		}
		for (auto it = raw.microfacet_bsdf().begin(); it != raw.microfacet_bsdf().end(); it++) {
			(*bsdf)[i++] = load(pool, *it, library);
		}
		for (auto it = raw.substrate_brdf().begin(); it != raw.substrate_brdf().end(); it++) {
			(*bsdf)[i++] = load(pool, *it, library);
		}
		for (auto it = raw.diffuse_microsurface_brdf().begin(); it != raw.diffuse_microsurface_brdf().end(); it++) {
			(*bsdf)[i++] = load(pool, *it, library);
		}
		for (auto it = raw.conductor_microsurface_brdf().begin(); it != raw.conductor_microsurface_brdf().end(); it++) {
			(*bsdf)[i++] = load(pool, *it, library);
		}
		for (auto it = raw.dielectric_microsurface_bsdf().begin(); it != raw.dielectric_microsurface_bsdf().end(); it++) {
			(*bsdf)[i++] = load(pool, *it, library);
		}
		for (auto it = raw.multiple_scatter_normal_map_bsdf().begin(); it != raw.multiple_scatter_normal_map_bsdf().end(); it++) {
			(*bsdf)[i++] = load(pool, *it, library);
		}
		for (auto it = raw.double_scatter_normal_map_bsdf().begin(); it != raw.double_scatter_normal_map_bsdf().end(); it++) {
			(*bsdf)[i++] = load(pool, *it, library);
		}
		return Pointer<const BSDF>(bsdf);
	}

	__host__
	static Pointer<const BSDF> load(MemoryPool& pool, luc::scattering& scattering, Library* library) {
		Pointer<const BSDF> result;
		if (scattering.invisible().present()) {
			result = load(pool, scattering.invisible().get(), library);
		}
		if (scattering.matte().present()) {
			result = load(pool, scattering.matte().get(), library);
		}
		if (scattering.plastic().present()) {
			result = load(pool, scattering.plastic().get(), library);
		}
		if (scattering.metal().present()) {
			result = load(pool, scattering.metal().get(), library);
		}
		if (scattering.substrate().present()) {
			result = load(pool, scattering.substrate().get(), library);
		}
		if (scattering.glass().present()) {
			result = load(pool, scattering.glass().get(), library);
		}
		if (scattering.raw().present()) {
			result = load(pool, scattering.raw().get(), library);
		}
		if (scattering.reference().present()) {
			auto pair = library->bsdfMap.find(scattering.reference().get().referenced_id());
			if (pair == library->bsdfMap.end()) {
				std::cerr << "could not find scattering pattern: " << scattering.reference().get().referenced_id() << std::endl;
			} else {
				result = pair->second;
			}
		}
		if (!result.isNull()) {
			if (scattering.id().present()) {
				library->bsdfMap[scattering.id().get()] = result;
			}
		}
		return result;
	}
};

}

#endif /* XMLSCATTERING_CUH_ */

#ifndef XMLRENDERER_CUH_
#define XMLRENDERER_CUH_

#include "lucifer/renderer/CPUMultiThreadBidirectionalTracer.cuh"
#include "lucifer/renderer/CPUReferencePathTracer.cuh"
#include "lucifer/renderer/CPUSingleThreadBidirectionalTracer.cuh"
#include "lucifer/renderer/CPUSingleThreadPathTracer.cuh"
#include "lucifer/renderer/CPUTiledPathTracer.cuh"
#include "lucifer/renderer/GPUPathTracer.cuh"
#include "lucifer/random/CorrelatedMultijitteredRNG.cuh"
#include "lucifer/random/CPUBasicRNG.cuh"
#include "lucifer/random/GPUBasicRNG.cuh"
#include "lucifer/xml/generated/lucifer.hxx"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class XMLRenderer {
private:
	__host__
	XMLRenderer() {
	}

public:
	__host__
	static Pointer<CPURNGFactory> load(MemoryPool& pool, luc::random_rng& rng) {
		return Pointer<CPURNGFactory>::make<CPUBasicRNGFactory<>>(pool);
	}

	__host__
	static Pointer<CPURNGFactory> load(MemoryPool& pool, luc::correlated_multi_jittered_rng& rng) {
		return Pointer<CPURNGFactory>::make<CPUCorrelatedMultiJitteredRNGFactory>(pool, rng.samples());
	}

	__host__
	static Pointer<CPURNGFactory> load(MemoryPool& pool, luc::rng& rng) {
		if (rng.random().present()) {
			return load(pool, rng.random().get());
		}
		if (rng.correlated_multi_jittered().present()) {
			return load(pool, rng.correlated_multi_jittered().get());
		}
		return Pointer<CPURNGFactory>();
	}

	__host__
	static Pointer<GPURNGFactory> load(MemoryPool& pool, luc::gpu_random_rng& rng) {
		return Pointer<GPURNGFactory>::make<GPUBasicRNGFactory>(pool);
	}

	__host__
	static Pointer<GPURNGFactory> load(MemoryPool& pool, luc::gpu_correlated_multi_jittered_rng& rng) {
		return Pointer<GPURNGFactory>::make<GPUCorrelatedMultiJitteredRNGFactory>(pool, rng.samples());
	}

	__host__
	static Pointer<GPURNGFactory> load(MemoryPool& pool, luc::gpu_rng& rng) {
		if (rng.random().present()) {
			return load(pool, rng.random().get());
		}
		if (rng.correlated_multi_jittered().present()) {
			return load(pool, rng.correlated_multi_jittered().get());
		}
		return Pointer<GPURNGFactory>();
	}

	__host__
	static Pointer<Renderer> load(MemoryPool& pool, luc::cpu_reference_path_tracer& renderer) {
		auto rngFactory = load(pool, renderer.rng());
		auto minDepth = renderer.min_depth();
		auto maxDepth = renderer.max_depth();
		auto spp = renderer.samples_per_pixel();
		auto tracker = Pointer<ProgressTracker<uint64_t>>::make<BasicProgressTracker<uint64_t>>(pool);
		return Pointer<Renderer>::make<CPUReferencePathTracer>(pool, rngFactory, minDepth, maxDepth, spp, tracker);
	}

	__host__
	static Pointer<Renderer> load(MemoryPool& pool, luc::cpu_single_thread_path_tracer& renderer) {
		auto rngFactory = load(pool, renderer.rng());
		auto minPathLength = renderer.min_path_length();
		auto maxPathLength = renderer.max_path_length();
		auto spp = renderer.samples_per_pixel();
		auto tracker = Pointer<ProgressTracker<uint64_t>>::make<BasicProgressTracker<uint64_t>>(pool);
		return Pointer<Renderer>::make<CPUSingleThreadPathTracer>(pool, rngFactory, minPathLength, maxPathLength, spp, tracker);
	}

	__host__
	static Pointer<Renderer> load(MemoryPool& pool, luc::cpu_tiled_path_tracer& renderer) {
		auto spp = renderer.samples_per_pixel();
		auto minPathLength = renderer.min_path_length();
		auto maxPathLength = renderer.max_path_length();
		auto threads = renderer.threads();
		auto timeBudget = renderer.time_budget();
		auto tileSize = TileSize(renderer.tile_width(), renderer.tile_height());
		auto rngFactory = load(pool, renderer.rng());
		auto tracker = Pointer<ProgressTracker<uint64_t>>::make<SynchronizedProgressTracker<uint64_t>>(
			pool,
			Pointer<ProgressTracker<uint64_t>>::make<BasicProgressTracker<uint64_t>>(pool)
		);
		auto time = renderer.fixed_time();
		return Pointer<Renderer>::make<CPUTiledPathTracer>(pool, spp, minPathLength, maxPathLength, threads, timeBudget, tileSize, rngFactory, tracker, time);
	}

	__host__
	static Pointer<Renderer> load(MemoryPool& pool, luc::cpu_single_thread_bidirectional_tracer& renderer) {
		auto rngFactory = load(pool, renderer.rng());
		auto minCamPathLen = renderer.min_camera_path_length();
		auto maxCamPathLen = renderer.max_camera_path_length();
		auto minLgtPathLen = renderer.min_light_path_length();
		auto maxLgtPathLen = renderer.max_light_path_length();
		auto spp = renderer.samples_per_pixel();
		auto mis = renderer.mis();
		auto resample = renderer.resample_direct();
		auto tracker = Pointer<ProgressTracker<uint64_t>>::make<BasicProgressTracker<uint64_t>>(pool);
		return Pointer<Renderer>::make<CPUSingleThreadBidirectionalTracer>(pool, rngFactory, minCamPathLen, maxCamPathLen, minLgtPathLen, maxLgtPathLen, spp, mis, resample, tracker);
	}

	__host__
	static Pointer<Renderer> load(MemoryPool& pool, luc::cpu_multi_thread_bidirectional_tracer& renderer) {
		auto rngFactory = load(pool, renderer.rng());
		auto minCamPathLen = renderer.min_camera_path_length();
		auto maxCamPathLen = renderer.max_camera_path_length();
		auto minLgtPathLen = renderer.min_light_path_length();
		auto maxLgtPathLen = renderer.max_light_path_length();
		auto spp = renderer.samples_per_pixel();
		auto mis = renderer.mis();
		auto resample = renderer.resample_direct();
		auto threads = renderer.threads();
		auto tracker = Pointer<ProgressTracker<uint64_t>>::make<SynchronizedProgressTracker<uint64_t>>(
			pool,
			Pointer<ProgressTracker<uint64_t>>::make<BasicProgressTracker<uint64_t>>(pool)
		);
		return Pointer<Renderer>::make<CPUMultiThreadBidirectionalTracer>(pool, rngFactory, minCamPathLen, maxCamPathLen, minLgtPathLen, maxLgtPathLen, spp, mis, resample, threads, tracker);
	}

	__host__
	static Pointer<Renderer> load(MemoryPool& pool, luc::gpu_path_tracer& renderer) {
		auto spp = renderer.samples_per_pixel();
		auto minPathLength = renderer.min_path_length();
		auto maxPathLength = renderer.max_path_length();
		auto tileWidth = renderer.tile_width();
		auto tileHeight = renderer.tile_height();
		auto tracker = Pointer<ProgressTracker<uint64_t>>::make<BasicProgressTracker<uint64_t>>(pool);
		return Pointer<Renderer>::make<GPUPathTracer>(pool, spp, minPathLength, maxPathLength, tileWidth, tileHeight, load(pool, renderer.rng()), tracker);
	}

	__host__
	static Pointer<Renderer> load(MemoryPool& pool, luc::renderer& renderer) {
		if (renderer.cpu_reference_path_tracer().present()) {
			return load(pool, renderer.cpu_reference_path_tracer().get());
		}
		if (renderer.cpu_single_thread_path_tracer().present()) {
			return load(pool, renderer.cpu_single_thread_path_tracer().get());
		}
		if (renderer.cpu_tiled_path_tracer().present()) {
			return load(pool, renderer.cpu_tiled_path_tracer().get());
		}
		if (renderer.cpu_single_thread_bidirectional_tracer().present()) {
			return load(pool, renderer.cpu_single_thread_bidirectional_tracer().get());
		}
		if (renderer.cpu_multi_thread_bidirectional_tracer().present()) {
			return load(pool, renderer.cpu_multi_thread_bidirectional_tracer().get());
		}
		if (renderer.gpu_path_tracer().present()) {
			return load(pool, renderer.gpu_path_tracer().get());
		}
		return Pointer<Renderer>();
	}
};

}

#endif /* XMLRENDERER_CUH_ */

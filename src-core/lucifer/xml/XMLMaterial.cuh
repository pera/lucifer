#ifndef XMLMATERIAL_CUH_
#define XMLMATERIAL_CUH_

#include "Library.cuh"
#include "XMLAnimatedTransformation.cuh"
#include "XMLEmission.cuh"
#include "XMLGeometry.cuh"
#include "XMLMedium.cuh"
#include "XMLScattering.cuh"
#include "lucifer/material/AnimatedMaterial.cuh"
#include "lucifer/material/BaseMaterial.cuh"
#include "lucifer/material/TransformedMaterial.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/xml/generated/lucifer.hxx"

namespace lucifer {

class XMLMaterial {
public:
	__host__
	XMLMaterial() = delete;

	__host__
	static Pointer<const Material> load(MemoryPool& pool, luc::base_material& material, Library* library) {
		Pointer<const UEDF> uedf;
		if (material.emission().present()) {
			uedf = XMLEmission::load(pool, material.emission().get(), library);
		}
		Pointer<const BSDF> bsdf;
		if (material.scattering().present()) {
			bsdf = XMLScattering::load(pool, material.scattering().get(), library);
		}
		Pointer<const Medium> medium;
		if (material.medium().present()) {
			medium = XMLMedium::load(pool, material.medium().get(), library);
		}
		return Pointer<const Material>(BaseMaterial::build(pool, uedf, bsdf, medium));
	}

	__host__
	static Pointer<const Material> load(MemoryPool& pool, luc::transformed_material& material, Library* library) {
		auto transformation  = XMLGeometry::load(material.transformation());
		auto transformed = load(pool, material.material(), library);
		return Pointer<const Material>(TransformedMaterial::build(pool, transformation, transformed));
	}

	__host__
	static Pointer<const Material> load(MemoryPool& pool, luc::animated_material& material, Library* library) {
		auto transformation  = XMLTemporalTransformation::load(pool, material.animated_transformation());
		auto transformed = load(pool, material.material(), library);
		return Pointer<const Material>(AnimatedMaterial::build(pool, transformation, transformed));
	}

	__host__
	static Pointer<const Material> load(MemoryPool& pool, luc::material& material, Library* library) {
		Pointer<const Material> result;
		if (material.base().present()) {
			result = load(pool, material.base().get(), library);
		}
		if (material.transformed().present()) {
			result = load(pool, material.transformed().get(), library);
		}
		if (material.animated().present()) {
			result = load(pool, material.animated().get(), library);
		}
		if (material.reference().present()) {
			result = library->materialMap[material.reference().get().referenced_id()];
		}
		if (!result.isNull() && material.id().present()) {
			library->materialMap[material.id().get()] = result;
		}
		return result;
	}

};

}

#endif /* XMLMATERIAL_CUH_ */

#ifndef XMLLIGHTDISTRIBUTION_CUH_
#define XMLLIGHTDISTRIBUTION_CUH_

#include "Library.cuh"
#include "lucifer/light/AreaLightDistribution.cuh"
#include "lucifer/light/PowerLightDistribution.cuh"
#include "lucifer/light/SpatialLightDistribution.cuh"
#include "lucifer/light/UnweightedLightDistribution.cuh"
#include "lucifer/random/CorrelatedMultijitteredRNG.cuh"
#include "lucifer/memory/MemoryPool.cuh"

namespace lucifer {

class XMLLightDistribution {
public:
	__host__
	XMLLightDistribution() = delete;

	__host__
	static Pointer<const LightDistribution> load(MemoryPool& pool, std::vector<Pointer<const GeometricPrimitive>>& lights, luc::unweighted_light_distribution& xml) {
		return Pointer<const LightDistribution>(UnweightedLightDistribution::build(pool, lights));
	}

	__host__
	static Pointer<const LightDistribution> load(MemoryPool& pool, std::vector<Pointer<const GeometricPrimitive>>& lights, luc::area_light_distribution& xml) {
		Pointer<RNG> rng = CPUCorrelatedMultiJitteredRNGFactory(xml.samples()).build(pool, 1);
		return Pointer<const LightDistribution>(AreaLightDistribution::build(pool, lights, xml.samples(), rng.get()));
	}

	__host__
	static Pointer<const LightDistribution> load(MemoryPool& pool, std::vector<Pointer<const GeometricPrimitive>>& lights, luc::power_light_distribution& xml, Library* library) {
		CPUCorrelatedMultiJitteredRNGFactory factory(xml.samples());
		Pointer<RNG> lRNG = factory.build(pool, 1);
		Pointer<RNG> tRNG = factory.build(pool, 1);
		Pointer<RNG> gRNG = factory.build(pool, 2);
		return Pointer<const LightDistribution>(PowerLightDistribution::build(pool, lights, xml.samples(), lRNG.get(), tRNG.get(), gRNG.get(), library->xyzColorSpace.get()));
	}

	__host__
	static Pointer<const LightDistribution> load(MemoryPool& pool, std::vector<Pointer<const GeometricPrimitive>>& lights, luc::spatial_light_distribution& xml, Library* library) {
		return Pointer<const LightDistribution>(SpatialLightDistribution::build(pool, lights, xml.samples(), library->xyzColorSpace.get()));
	}

	__host__
	static Pointer<const LightDistribution> load(MemoryPool& pool, std::vector<Pointer<const GeometricPrimitive>>& lights) {
		Pointer<RNG> rng =  CPUCorrelatedMultiJitteredRNGFactory(256).build(pool, 1);
		return Pointer<const LightDistribution>(AreaLightDistribution::build(pool, lights, 256, rng.get()));
	}

	__host__
	static Pointer<const LightDistribution> load(MemoryPool& pool, std::vector<Pointer<const GeometricPrimitive>>& lights, luc::scene::light_distribution_optional& xml, Library* library) {
		if (xml.present()) {
			if (xml.get().unweighted_distribution().present()) {
				return load(pool, lights, xml.get().unweighted_distribution().get());
			}
			if (xml.get().area_distribution().present()) {
				return load(pool, lights, xml.get().area_distribution().get());
			}
			if (xml.get().power_distribution().present()) {
				return load(pool, lights, xml.get().power_distribution().get(), library);
			}
			if (xml.get().spatial_distribution().present()) {
				return load(pool, lights, xml.get().spatial_distribution().get(), library);
			}
		}
		return load(pool, lights);
	}
};

}

#endif /* XMLLIGHTDISTRIBUTION_CUH_ */

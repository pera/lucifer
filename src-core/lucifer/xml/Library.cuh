#ifndef LIBRARY_CUH_
#define LIBRARY_CUH_

#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "lucifer/uedf/UEDF.cuh"
#include "lucifer/bsdf/BSDF.cuh"
#include "lucifer/color/Spectrum.cuh"
#include "lucifer/color/RGBColorSpace.cuh"
#include "lucifer/image/HDRImage.cuh"
#include "lucifer/primitive/GeometricPrimitive.cuh"
#include "lucifer/primitive/Primitive.cuh"
#include "lucifer/shape/Shape.cuh"
#include "lucifer/material/Material.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/medium/PhaseFunction.cuh"
#include "lucifer/medium/Medium.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"

namespace lucifer {

class Library {
private:
	using string_set = std::unordered_set<std::string>;

	template<typename V>
	using string_map = std::unordered_map<std::string, V>;

public:
	const Pointer<const XYZColorSpace> xyzColorSpace;
	string_set includedFiles;
	string_map<Pointer<const RGBColorSpace>> colorSpaceMap;
	string_map<Pointer<const HDRImage<float, 3>>> imageMap;
	string_map<Pointer<const ShadingNode<bool>>> shadingBoolNodeMap;
	string_map<Pointer<const ShadingNode<float>>> shadingFloatNodeMap;
	string_map<Pointer<const ShadingNode<Color3F>>> shadingColorNodeMap;
	string_map<Pointer<const ShadingNode<Point3F>>> shadingPoint3FNodeMap;
	string_map<Pointer<const ShadingNode<Norml3F>>> shadingNorml3FNodeMap;
	string_map<Pointer<const ShadingNode<Vectr3F>>> shadingVectr3FNodeMap;
	string_map<Pointer<const ShadingNode<Point2F>>> shadingPoint2FNodeMap;
	string_map<Pointer<const Shape>> shapeMap;
	string_map<Pointer<const UEDF>> uedfMap;
	string_map<Pointer<const BSDF>> bsdfMap;
	string_map<Pointer<const PhaseFunction>> phaseFunctionMap;
	string_map<Pointer<const Medium>> mediumMap;
	string_map<Pointer<const Material>> materialMap;
	string_map<std::vector<Pointer<const Primitive>>> flatPrimitiveMap;
	string_map<std::pair<std::vector<Pointer<const GeometricPrimitive>>, std::vector<Pointer<const GeometricPrimitive>>>> primitiveMap;

	__host__
	Library(const Pointer<const XYZColorSpace>& xyzColorSpace):
	xyzColorSpace(xyzColorSpace) {
	}

	__host__
	Pointer<const Material> material(const std::string& name) {
		auto pair = materialMap.find(name);
		if (pair == materialMap.end()) {
			std::cerr << "could not find material: " << name << std::endl;
			return Pointer<const Material>();
		}
		return pair->second;
	}
};

}

#endif /* LIBRARY_CUH_ */

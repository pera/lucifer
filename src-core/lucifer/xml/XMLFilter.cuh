#ifndef XMLFILTER_CUH_
#define XMLFILTER_CUH_

#include "lucifer/filter/BoxFilter1D.cuh"
#include "lucifer/filter/CartesianFilter2D.cuh"
#include "lucifer/filter/GaussianFilter1D.cuh"
#include "lucifer/filter/LanczosFilter1D.cuh"
#include "lucifer/filter/MitchellNetravaliFilter1D.cuh"
#include "lucifer/filter/SampledFilter2D.cuh"
#include "lucifer/filter/TriangleFilter1D.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/xml/generated/lucifer.hxx"

namespace lucifer {

class XMLFilter {
private:
	__host__
	XMLFilter() {
	}

public:
	__host__
	static Pointer<const Filter1D> load(MemoryPool& pool, const luc::box_filter_1d& filter) {
		return Pointer<const Filter1D>(BoxFilter1D::build(pool, filter.radius()));
	}

	__host__
	static Pointer<const Filter1D> load(MemoryPool& pool, const luc::triangle_filter_1d& filter) {
		return Pointer<const Filter1D>(TriangleFilter1D::build(pool, filter.radius()));
	}

	__host__
	static Pointer<const Filter1D> load(MemoryPool& pool, const luc::gaussian_filter_1d& filter) {
		return Pointer<const Filter1D>(GaussianFilter1D::build(pool, filter.radius(), filter.standard_deviation()));
	}

	__host__
	static Pointer<const Filter1D> load(MemoryPool& pool, const luc::lanczos_filter_1d& filter) {
		return Pointer<const Filter1D>(LanczosFilter1D::build(pool, (uint8_t)filter.radius()));
	}

	__host__
	static Pointer<const Filter1D> load(MemoryPool& pool, const luc::mitchell_netravali_filter_1d& filter) {
		return Pointer<const Filter1D>(MitchellNetravaliFilter1D::build(pool, filter.b(), filter.c()));
	}

	__host__
	static Pointer<const Filter1D> load(MemoryPool& pool, const luc::filter_1d& filter) {
		if (filter.box().present()) {
			return load(pool, filter.box().get());
		}
		if (filter.triangle().present()) {
			return load(pool, filter.triangle().get());
		}
		if (filter.gaussian().present()) {
			return load(pool, filter.gaussian().get());
		}
		if (filter.lanczos().present()) {
			return load(pool, filter.lanczos().get());
		}
		if (filter.mitchell_netravali().present()) {
			return load(pool, filter.mitchell_netravali().get());
		}
		return Pointer<const Filter1D>();
	}

	__host__
	static Pointer<const Filter2D> load(MemoryPool& pool, const luc::cartesian_filter_2d& filter) {
		return Pointer<const Filter2D>(
			CartesianFilter2D::build(
				pool,
				load(pool, filter.x_filter()),
				load(pool, filter.y_filter())
			)
		);
	}

	__host__
	static Pointer<const Filter2D> load(MemoryPool& pool, const luc::sampled_filter_2d& filter) {
		return Pointer<const Filter2D>(
			SampledFilter2D::build(
				pool,
				filter.x_samples(),
				filter.y_samples(),
				load(pool, filter.inner())
			)
		);
	}

	__host__
	static Pointer<const Filter2D> load(MemoryPool& pool, const luc::filter_2d& filter) {
		if (filter.cartesian().present()) {
			return load(pool, filter.cartesian().get());
		}
		if (filter.sampled().present()) {
			return load(pool, filter.sampled().get());
		}
		return Pointer<const Filter2D>();
	}

};

}

#endif /* XMLFILTER_CUH_ */

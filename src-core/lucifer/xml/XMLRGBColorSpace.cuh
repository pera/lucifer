#ifndef XMLRGBCOLORSPACE_CUH_
#define XMLRGBCOLORSPACE_CUH_

#include "Library.cuh"
#include "lucifer/color/RGBColorSpace.cuh"
#include "lucifer/xml/generated/lucifer.hxx"
#include "lucifer/memory/MemoryPool.cuh"

namespace lucifer {

class XMLRGBColorSpace {
private:
	__host__
	XMLRGBColorSpace() {
	}

public:
	__host__
	static Pointer<const Gamma> load(MemoryPool& pool, const luc::no_gamma& gamma) {
		return Pointer<const Gamma>(NoGamma::build(pool));
	}

	__host__
	static Pointer<const Gamma> load(MemoryPool& pool, const luc::simplified_gamma& gamma) {
		return Pointer<const Gamma>(SimplifiedGamma::build(pool, gamma.value()));
	}

	__host__
	static Pointer<const Gamma> load(MemoryPool& pool, const luc::detailed_gamma& gamma) {
		return Pointer<const Gamma>(DetailedGamma::build(pool, gamma.offset(), gamma.gamma(), gamma.transition(), gamma.slope()));
	}

	__host__
	static Pointer<const Gamma> load(MemoryPool& pool, const luc::gamma& gamma) {
		if (gamma.no_gamma().present()) {
			return load(pool, gamma.no_gamma().get());
		}
		if (gamma.simplified().present()) {
			return load(pool, gamma.simplified().get());
		}
		if (gamma.detailed().present()) {
			return load(pool, gamma.detailed().get());
		}
		return Pointer<const Gamma>();
	}

	__host__
	static Pointer<const RGBColorSpace> load(MemoryPool& pool, const luc::rgb_cs_def& cs, Library* library) {
		const Point2F r {cs.r().x(), cs.r().y()};
		const Point2F g {cs.g().x(), cs.g().y()};
		const Point2F b {cs.b().x(), cs.b().y()};
		const Point3F s {cs.w_src().x(), cs.w_src().y(), cs.w_src().z()};
		const Point3F d {cs.w_dst().x(), cs.w_dst().y(), cs.w_dst().z()};
		return Pointer<const RGBColorSpace>(RGBColorSpace::build(pool, r, g, b, s, d, load(pool, cs.gamma()), library->xyzColorSpace));
	}

	__host__
	static Pointer<const RGBColorSpace> load(MemoryPool& pool, const luc::rgb_color_space& cs, Library* library) {
		Pointer<const RGBColorSpace> result = Pointer<const RGBColorSpace>();
		if (cs.define().present()) {
			result = load(pool, cs.define().get(), library);
		}
		if (cs.reference().present()) {
			auto pair = library->colorSpaceMap.find(cs.reference().get().referenced_id());
			if (pair == library->colorSpaceMap.end()) {
				std::cerr << "could not find rgb color space: " << cs.reference().get().referenced_id() << std::endl;
			} else {
				result = pair->second;
			}
		}
		if (!result.isNull()) {
			if (cs.id().present()) {
				library->colorSpaceMap[cs.id().get()] = result;
			}
		}
		return result;
	}
};

}

#endif /* XMLRGBCOLORSPACE_CUH_ */

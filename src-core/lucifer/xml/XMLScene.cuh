#ifndef XMLSCENE_CUH_
#define XMLSCENE_CUH_

#include <memory>
#include <vector>
#include "Library.cuh"
#include "XMLCamera.cuh"
#include "XMLInput.cuh"
#include "XMLLibrary.cuh"
#include "XMLLightDistribution.cuh"
#include "XMLPrimitive.cuh"
#include "XMLToneMapper.cuh"
#include "XMLRenderer.cuh"
#include "XMLRGBColorSpace.cuh"
#include "lucifer/primitive/PrimitiveSet.cuh"
#include "lucifer/renderer/Scene.cuh"
#include "lucifer/renderer/Viewport.cuh"
#include "lucifer/xml/generated/lucifer.hxx"
#include "lucifer/memory/ManagedAllocator.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/util/Context.cuh"

namespace lucifer {

class XMLScene {
public:
	__host__
	XMLScene() = delete;

	__host__
	static XMLInput* load(std::unique_ptr<luc::scene>& xmlScene) {
		auto renderer = XMLRenderer::load(Context::managedMemoryPool(), xmlScene->renderer());
		auto library = XMLLibrary::load(Context::managedMemoryPool(), xmlScene->library());
		auto camera = XMLCamera::load(Context::managedMemoryPool(), xmlScene->camera(), library.get());

		std::vector<Pointer<const GeometricPrimitive>> plist;
		std::vector<Pointer<const GeometricPrimitive>> llist;
		for (auto& p : xmlScene->primitive()) {
			std::vector<Pointer<const GeometricPrimitive>> tmpplist;
			std::vector<Pointer<const GeometricPrimitive>> tmpllist;
			XMLPrimitive::load(Context::managedMemoryPool(), p, tmpplist, tmpllist, library.get());
			plist.insert(std::end(plist), std::begin(tmpplist), std::end(tmpplist));
			llist.insert(std::end(llist), std::begin(tmpllist), std::end(tmpllist));
		}
		plist.insert(std::end(plist), std::begin(llist), std::end(llist));

		GroupMode mode = GroupModeBuilder::buildFrom(xmlScene->group_mode());
		auto distribution = XMLLightDistribution::load(Context::managedMemoryPool(), llist, xmlScene->light_distribution(), library.get());
		Pointer<const Primitive> primitive = XMLPrimitive::group(Context::managedMemoryPool(), plist, mode);


		Viewport viewport {
			xmlScene->viewport().present() ? XMLGeometry::load(xmlScene->viewport().get().lower()) : Point2F{0.f, 0.f},
			xmlScene->viewport().present() ? XMLGeometry::load(xmlScene->viewport().get().upper()) : Point2F{camera->sensor()->width(), camera->sensor()->height()}
		};

		auto scene = Pointer<Scene>::make(Context::managedMemoryPool(), camera, viewport, primitive, distribution);

		XMLPreview preview;
		if (xmlScene->preview().present()) {
			preview.toneMapper = XMLToneMapper::load(Context::managedMemoryPool(), xmlScene->preview().get().tone_mapping());
			preview.colorSpace = XMLRGBColorSpace::load(Context::managedMemoryPool(), xmlScene->preview().get().color_space(), library.get());
			preview.autoUpdate = xmlScene->preview().get().auto_update();
		}
		XMLInput* result = new XMLInput(renderer, scene, preview, xmlScene->renderer().progressive());
		for (int i = 0; i < xmlScene->output().size(); i++) {
			XMLOutput output;
			output.toneMapper = XMLToneMapper::load(Context::managedMemoryPool(), xmlScene->output()[i].tone_mapping());
			output.filename = xmlScene->output()[i].file_name();
			result->output.push_back(output);
		}
		return result;
	}
};

}

#endif /* XMLSCENE_CUH_ */

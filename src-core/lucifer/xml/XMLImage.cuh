#ifndef XMLIMAGE_CUH_
#define XMLIMAGE_CUH_

#include <iostream>
#include "Library.cuh"
#include "XMLRGBColorSpace.cuh"
#include "lucifer/image/HDRImage.cuh"
#include "lucifer/image/HDRImageIO.cuh"
#include "lucifer/memory/MemoryPool.cuh"

namespace lucifer {

class XMLImage {
private:
	__host__
	XMLImage() {
	}

public:
	__host__
	static Pointer<const HDRImage<float, 3>> load(MemoryPool& pool, const luc::hdr_image& image, Library* library) {
		Pointer<const HDRImage<float, 3>> result;
		auto pair = library->imageMap.find(image.file());
		if (pair == library->imageMap.end()) {
			std::ifstream in(image.file());
			if (!in) {
				std::cerr << "could not open file " << image.file() << std::endl;
				return result;
			}
			auto temp = HDRImageIO::readPFM(pool, in);
			if (image.normalize()) {
				temp->normalize();
			}
			result = temp;
			library->imageMap[image.file()] = result;
		} else {
			result = pair->second;
		}
		return result;
	}
};

}

#endif /* XMLIMAGE_CUH_ */

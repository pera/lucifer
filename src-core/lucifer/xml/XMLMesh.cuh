#ifndef XMLMESH_CUH_
#define XMLMESH_CUH_

#include <memory>
#include <string>
#include <vector>
#include "Library.cuh"
#include "ObjMeshLoader.cuh"
#include "XMLGeometry.cuh"
#include "XMLShadingNode.cuh"
#include "lucifer/shape/TriangleMesh.cuh"
#include "lucifer/memory/MemoryPool.cuh"

namespace lucifer {

class XMLMeshData {
public:
	Data vertexes;
	std::vector<Mesh> meshes;
	std::vector<std::string> materials;

	__host__
	void transform(const Transformation3F& transformation) {
		for (auto& v : vertexes.vrtx) v = transformation * v;
		for (auto& n : vertexes.nrml) n = transformation * n;
		for (auto& t : vertexes.tngt) t = transformation * t;
	}
};

class XMLMesh {
public:
	__host__
	XMLMesh() = delete;

	__host__
	static std::unique_ptr<XMLMeshData> load(luc::xml_mesh& xml) {
		// creates input data structures
		auto data = std::unique_ptr<XMLMeshData>(new XMLMeshData);
		data->meshes.emplace_back();
		auto& mesh = data->meshes.back();
		// load vertex data
		for (auto& vrtx : xml.vrtx()) {
			data->vertexes.vrtx.emplace_back(vrtx.x(), vrtx.y(), vrtx.z());
		}
		for (auto& nrml : xml.nrml()) {
			data->vertexes.nrml.emplace_back(nrml.x(), nrml.y(), nrml.z());
		}
		for (auto& tngt : xml.tngt()) {
			data->vertexes.tngt.emplace_back(tngt.x(), tngt.y(), tngt.z());
		}
		for (auto& txtr : xml.txtr()) {
			data->vertexes.txtr.emplace_back(txtr.x(), txtr.y());
		}
		// load face data
		for (auto& vrtxFace : xml.vrtx_face()) {
			mesh.vrtx.emplace_back(vrtxFace[0], vrtxFace[1], vrtxFace[2]);
		}
		for (auto& nrmlFace : xml.nrml_face()) {
			mesh.nrml.emplace_back(nrmlFace[0], nrmlFace[1], nrmlFace[2]);
		}
		for (auto& tngtFace : xml.tngt_face()) {
			mesh.nrml.emplace_back(tngtFace[0], tngtFace[1], tngtFace[2]);
		}
		return data;
	}

	__host__
	static std::unique_ptr<XMLMeshData> load(luc::obj_mesh& xml) {
		auto data = std::unique_ptr<XMLMeshData>(new XMLMeshData);
		if (!ObjMeshLoader::load(xml.file_name(), data->vertexes, data->meshes, data->materials)) {
			return std::unique_ptr<XMLMeshData>(nullptr);
		}
		return data;
	}

	__host__
	static std::unique_ptr<XMLMeshData> load(MemoryPool& pool, luc::transformed_mesh& xml, Library* library) {
		auto transformation = XMLGeometry::load(xml.transformation());
		auto data = load(pool, xml.mesh(), library);
		data->transform(transformation);
		return data;
	}

	__host__
	static std::unique_ptr<XMLMeshData> load(MemoryPool& pool, luc::smoothed_mesh& xml, Library* library) {
		auto data = load(pool, xml.mesh(), library);
		if (xml.estimate_normals ()) data->vertexes.estimateNrmls(data->meshes);
		if (xml.estimate_tangents()) data->vertexes.estimateTngts(data->meshes);
		return data;
	}

	__host__
	static std::unique_ptr<XMLMeshData> load(MemoryPool& pool, luc::sharpened_mesh& xml, Library* library) {
		auto data = load(pool, xml.mesh(), library);
		if (xml.drop_normals ()) data->vertexes.dropNrmls(data->meshes);
		if (xml.drop_tangents()) data->vertexes.dropTngts(data->meshes);
		return data;
	}

	__host__
	static std::unique_ptr<XMLMeshData> load(MemoryPool& pool, luc::displaced_mesh& xml, Library* library) {
		auto map = XMLShadingNode::load(pool, xml.displacement_map(), library);
		auto data = load(pool, xml.mesh(), library);
		data->vertexes.displace(map, data->meshes);
		return data;
	}

	__host__
	static std::unique_ptr<XMLMeshData> load(MemoryPool& pool, luc::raw_mesh& xml, Library* library) {
		if (xml.xml().present()) {
			return load(xml.xml().get());
		}
		if (xml.obj().present()) {
			return load(xml.obj().get());
		}
		if (xml.transformed().present()) {
			return load(pool, xml.transformed().get(), library);
		}
		if (xml.smoothed().present()) {
			return load(pool, xml.smoothed().get(), library);
		}
		if (xml.sharpened().present()) {
			return load(pool, xml.sharpened().get(), library);
		}
		if (xml.displaced().present()) {
			return load(pool, xml.displaced().get(), library);
		}
		return std::unique_ptr<XMLMeshData>(nullptr);
	}
};

}

#endif /* XMLMESH_CUH_ */

#ifndef OBJMESHLOADER_CUH_
#define OBJMESHLOADER_CUH_

#include <cassert>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "lucifer/shape/TriangleMesh.cuh"

namespace lucifer {

class ObjMeshLoader {
public:
	__host__
	ObjMeshLoader() = delete;

	__host__
	static void toNextLine(std::ifstream& input, bool print = false) {
		int c;
		do {
			c = input.get();
			if (print) {
				std::cout << c;
			}
		} while(c != '\n' && c != '\r' && c != EOF);
	}

	__host__
	static void discardWhiteSpaces(std::ifstream& input)  {
		int c;
		while(true) {
			c = input.peek();
			if (c == ' ') {
				input.get();
			} else {
				break;
			}
		}
	}

	__host__
	static bool load(std::string filename, Data& data, std::vector<Mesh>& meshes, std::vector<std::string>& materials) {
		// clear data
		data.clear();
		meshes.clear();
		materials.clear();
		// open input file
		std::ifstream input;
		input.open(filename);
		if (!input) {
			std::cerr << "could not open file: " << filename << std::endl;
			return false;
		}
		// temporary face data
		std::vector<Index3::index_t> vrtxFace;
		std::vector<Index3::index_t> nrmlFace;
		std::vector<Index3::index_t> txtrFace;
		// current mesh
		std::string material = "default";
		meshes.emplace_back();
		// read input file
		std::string token;
		while (!input.eof()) {
			input >> token;
			if (token == "#") {
				// comment
			} else if (token == "o") {
				if (meshes.back().vrtx.size() > 0) {
					materials.push_back(material);
					meshes.emplace_back();
				}
			} else if (token == "g") {
				if (meshes.back().vrtx.size() > 0) {
					materials.push_back(material);
					meshes.emplace_back();
				}
			} else if (token == "usemtl") {
				if (meshes.back().vrtx.size() > 0) {
					materials.push_back(material);
					meshes.emplace_back();
				}
				input >> material;
			} else if (token ==  "v") {
				// vertex
				float x, y, z, w = 1.f;
				input >> x;
				input >> y;
				input >> z;
				discardWhiteSpaces(input);
				if (input.peek() != '\n' && input.peek() != '\r') {
					input >> w;
				}
				try {
					data.vrtx.push_back(Point3F(x / w, y / w, z / w));
				} catch (std::bad_alloc& e) {
					std::cerr << "not enough memory for obj vrtx vertex: " << e.what() << std::endl;
					return false;
				}
			} else if (token == "vt") {
				// texture
				float u, v, w = 1.f;
				input >> u;
				input >> v;
				discardWhiteSpaces(input);
				if (input.peek() != '\n' && input.peek() != '\r') {
					input >> w;
				}
				try {
					data.txtr.push_back(Point2F(u, v));
				} catch (std::bad_alloc& e) {
					std::cerr << "not enough memory for obj txtr vertex: " << e.what() << std::endl;
					return false;
				}
			} else if (token == "vn") {
				// normal
				float x, y, z, w = 1.f;
				input >> x;
				input >> y;
				input >> z;
				discardWhiteSpaces(input);
				if (input.peek() != '\n' && input.peek() != '\r') {
					input >> w;
				}
				try {
					data.nrml.push_back(Norml3F(x / w, y / w, z / w));
				} catch (std::bad_alloc& e) {
					std::cerr << "not enough memory for obj nrml vertex: " << e.what() << std::endl;
					return false;
				}
			} else if (token ==  "f") {
				// face
				vrtxFace.clear();
				nrmlFace.clear();
				txtrFace.clear();
				int idx;
				do {
					// vertex index
					input >> idx;
					if (idx > 0) {
						idx = idx - 1;
					} else {
						idx = data.vrtx.size() + idx;
					}
					if (idx < 0 || idx >= data.vrtx.size()) {
						std::cerr << "bad vrtx vertex index: " << idx << ", vrtx count:" << data.vrtx.size() << std::endl;
					}
					vrtxFace.push_back(idx);
					if (input.peek() == '/') {
						input.get(); // discards '/'
						if (input.peek() != '/') {
							// texture index
							input >> idx;
							if (idx > 0) {
								idx = idx - 1;
							} else {
								idx = data.txtr.size() + idx;
							}
							if (idx < 0 || idx >= data.txtr.size()) {
								std::cerr << "bad txtr vertex index: " << idx << ", txtr count:" << data.txtr.size() << std::endl;
							}
							txtrFace.push_back(idx);
						}
						if (input.peek() == '/') {
							// normal index
							input.get(); // discards '/'
							input >> idx;
							if (idx > 0) {
								idx = idx - 1;
							} else {
								idx = data.nrml.size() + idx;
							}
							if (idx < 0 || idx >= data.nrml.size()) {
								std::cerr << "bad nrml vertex index: " << idx << ", nrml count:" << data.nrml.size() << std::endl;
							}
							nrmlFace.push_back(idx);
						}
					}
					discardWhiteSpaces(input);
				} while (input.peek() != '\n' && input.peek() != '\r' && !input.eof());
				Mesh& mesh = meshes.back();
				for (int i = 2; i < vrtxFace.size(); i++) {
					try {
						mesh.vrtx.emplace_back(vrtxFace[0], vrtxFace[i - 1], vrtxFace[i]);
					} catch (std::bad_alloc& e) {
						std::cerr << "not enough memory for obj mesh vrtx face: " << e.what() << std::endl;
						return false;
					}
				}
				for (int i = 2; i < txtrFace.size(); i++) {
					try {
						mesh.txtr.emplace_back(txtrFace[0], txtrFace[i - 1], txtrFace[i]);
					} catch (std::bad_alloc& e) {
						std::cerr << "not enough memory for obj mesh txtr face: " << e.what() << std::endl;
						return false;
					}
				}
				for (int i = 2; i < nrmlFace.size(); i++) {
					try {
						mesh.nrml.emplace_back(nrmlFace[0], nrmlFace[i - 1], nrmlFace[i]);
					} catch (std::bad_alloc& e) {
						std::cerr << "not enough memory for obj mesh nface: " << e.what() << std::endl;
						return false;
					}
				}
			}
			toNextLine(input);
		}
		materials.push_back(material);
		assert(meshes.size() == materials.size());
		return true;
	}
};

}

#endif /* OBJMESHLOADER_CUH_ */

#ifndef XMLCAMERA_CUH_
#define XMLCAMERA_CUH_

#include "Library.cuh"
#include "XMLFilter.cuh"
#include "XMLGeometry.cuh"
#include "XMLAnimatedTransformation.cuh"
#include "lucifer/camera/AnimatedCamera.cuh"
#include "lucifer/camera/PerspectiveCamera.cuh"
#include "lucifer/camera/TransformedCamera.cuh"
#include "lucifer/color/XYZColorSpace.cuh"
#include "lucifer/image/HDRImage.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/xml/generated/lucifer.hxx"

namespace lucifer {

class XMLCamera {
private:
	__host__
	XMLCamera() {
	}

public:
	__host__
	static Pointer<Camera> load(MemoryPool& pool, luc::perspective_camera& camera, Library* library) {
		Pointer<const Filter2D> filter;
		if (camera.sensor().filter().present()) {
			filter = XMLFilter::load(pool, camera.sensor().filter().get());
		}
		auto raster = Pointer<HDRImage<float, 3>>::make(pool, pool, camera.sensor().width(), camera.sensor().height());
		auto sensor = Pointer<Sensor>::make(pool, raster, library->xyzColorSpace, filter);
		float fovY = camera.fov_y();
		if (camera.unit() == luc::angle_unit::degrees) {
			fovY = toRadians(fovY);
		}
		float lensRadius = 0;
		float focalDistance = 1;
		if (camera.thin_lens().present()) {
			lensRadius = camera.thin_lens().get().radius();
			focalDistance = camera.thin_lens().get().focal_distance();
		}
		return Pointer<Camera>(
			PerspectiveCamera::build(
				pool,
				fovY,
				lensRadius,
				focalDistance,
				sensor
			)
		);
	}

	__host__
	static Pointer<Camera> load(MemoryPool& pool, luc::transformed_camera& transformedCamera, Library* library) {
		auto t = XMLGeometry::load(transformedCamera.transformation());
		auto camera = load(pool, transformedCamera.camera(), library);
		return Pointer<Camera>(TransformedCamera::build(pool, t, camera));
	}

	__host__
	static Pointer<Camera> load(MemoryPool& pool, luc::animated_camera& animatedCamera, Library* library) {
		auto transformation = XMLTemporalTransformation::load(pool, animatedCamera.animated_transformation());
		auto camera = load(pool, animatedCamera.camera(), library);
		return Pointer<Camera>(AnimatedCamera::build(pool, transformation, camera));
	}

	__host__
	static Pointer<Camera> load(MemoryPool& pool, luc::scene::camera_type& camera, Library* library) {
		if (camera.perspective().present()) {
			return load(pool, camera.perspective().get(), library);
		}
		if (camera.transformed().present()) {
			return load(pool, camera.transformed().get(), library);
		}
		if (camera.animated().present()) {
			return load(pool, camera.animated().get(), library);
		}
		return Pointer<Camera>();
	}

};

}


#endif /* XMLCAMERA_CUH_ */

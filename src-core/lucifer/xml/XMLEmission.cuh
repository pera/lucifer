#ifndef XMLEMISSION_CUH_
#define XMLEMISSION_CUH_

#include "Library.cuh"
#include "XMLShadingNode.cuh"
#include "lucifer/node/ConstantNode.cuh"
#include "lucifer/node/math/MultiplicationNode.cuh"
#include "lucifer/uedf/DeltaDirectionalUEDC.cuh"
#include "lucifer/uedf/FalloffHemisphericalUEDC.cuh"
#include "lucifer/uedf/IESProfileUEDC.cuh"
#include "lucifer/uedf/IsotropicHemisphericalUEDC.cuh"
#include "lucifer/xml/generated/lucifer.hxx"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class XMLEmission {
private:
	__host__
	XMLEmission() {
	}

	__host__
	static Pointer<const ShadingNode<float>> toRadians(MemoryPool& pool, const Pointer<const ShadingNode<float>>& degrees) {
		return Pointer<const ShadingNode<float>>(
			MultiplicationNode<float, float, LUCIFER_SHADING_INPUT>::build(
				pool,
				degrees,
				Pointer<const ShadingNode<float>>(ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 180.f))
			)
		);
	}

public:
	__host__
	static Pointer<const UEDC> load(MemoryPool& pool, luc::isotropic_hemispherical_uedf& uedf, Library* library) {
		auto power = XMLShadingNode::load(pool, uedf.power(), library);
		return Pointer<const UEDC>(IsotropicHemisphericalUEDC::build(pool, power));
	}

	__host__
	static Pointer<const UEDC> load(MemoryPool& pool, luc::falloff_hemispherical_uedf& uedf, Library* library) {
		auto power = XMLShadingNode::load(pool, uedf.power(), library);
		auto thetaMin = XMLShadingNode::load(pool, uedf.theta_min(), library);
		auto thetaMax = XMLShadingNode::load(pool, uedf.theta_max(), library);
		if (uedf.unit() == luc::angle_unit::degrees) {
			thetaMin = toRadians(pool, thetaMin);
			thetaMax = toRadians(pool, thetaMax);
		}
		return Pointer<const UEDC>(FalloffHemisphericalUEDC::build(pool, power, thetaMin, thetaMax));
	}

	__host__
	static Pointer<const UEDC> load(MemoryPool& pool, luc::delta_uedf& uedf, Library* library) {
		auto power = XMLShadingNode::load(pool, uedf.power(), library);
		return Pointer<const UEDC>(DeltaDirectionalUEDC::build(pool, power));
	}

	__host__
	static Pointer<const UEDC> load(MemoryPool& pool, luc::ies_profile_uedf& uedf, Library* library) {
		auto power = XMLShadingNode::load(pool, uedf.power(), library);
		return Pointer<const UEDC>(IESProfileUEDC::build(pool, power, uedf.file_name()));
	}

	__host__
	static Pointer<const UEDF> load(MemoryPool& pool, luc::emission& emission, Library* library) {
		// number of components
		int n = 0;
		n += emission.isotropic_hemispherical().size();
		n += emission.falloff_hemispherical().size();
		n += emission.delta().size();
		n += emission.ies_profile().size();
		if (n == 0) {
			return Pointer<const UEDF>();
		}
		// load each component
		Pointer<const UEDF> result;
		if (emission.reference().present()) {
			auto pair = library->uedfMap.find(emission.reference().get().referenced_id());
			if (pair == library->uedfMap.end()) {
				std::cerr << "could not find emission pattern: " << emission.reference().get().referenced_id() << std::endl;
			} else {
				result = pair->second;
			}
		} else {
			auto temp = UEDF::build(pool, n);
			int i = 0;
			for (int j = 0; j < emission.isotropic_hemispherical().size(); i++, j++) {
				(*temp)[i] = load(pool, emission.isotropic_hemispherical()[j], library);
			}
			for (int j = 0; j < emission.falloff_hemispherical().size(); i++, j++) {
				(*temp)[i] = load(pool, emission.falloff_hemispherical()[j], library);
			}
			for (int j = 0; j < emission.delta().size(); i++, j++) {
				(*temp)[i] = load(pool, emission.delta()[j], library);
			}
			for (int j = 0; j < emission.ies_profile().size(); i++, j++) {
				(*temp)[i] = load(pool, emission.ies_profile()[j], library);
			}
			result = Pointer<const UEDF>(temp);
		}
		if (!result.isNull()) {
			if (emission.id().present()) {
				library->uedfMap[emission.id().get()] = result;
			}
		}
		return result;
	}

};

}

#endif /* XMLEMISSION_CUH_ */

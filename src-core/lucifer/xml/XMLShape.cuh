#ifndef XMLSHAPE_CUH_
#define XMLSHAPE_CUH_

#include <string>
#include <vector>

#include "Library.cuh"
#include "ObjMeshLoader.cuh"
#include "XMLGeometry.cuh"
#include "XMLShadingNode.cuh"
#include "XMLAnimatedTransformation.cuh"
#include "XMLMesh.cuh"
#include "lucifer/math/geometry/Box.cuh"
#include "lucifer/shape/AnimatedShape.cuh"
#include "lucifer/shape/BVHShape.cuh"
#include "lucifer/shape/BumpMappedShape.cuh"
#include "lucifer/shape/InvertedShape.cuh"
#include "lucifer/shape/NormalMappedShape.cuh"
#include "lucifer/shape/ShapeSet.cuh"
#include "lucifer/shape/Sphere.cuh"
#include "lucifer/shape/STBVHShape.cuh"
#include "lucifer/shape/TransformedShape.cuh"
#include "lucifer/shape/Triangle.cuh"
#include "lucifer/shape/TriangleMesh.cuh"
#include "lucifer/xml/generated/lucifer.hxx"
#include "lucifer/memory/MemoryPool.cuh"

namespace lucifer {

class XMLShape {
public:
	__host__
	XMLShape() = delete;

	__host__
	static Pointer<const Shape> load(MemoryPool& pool, luc::sphere& sphere) {
		return Pointer<const Shape>(
			Sphere::build(
				pool,
				XMLGeometry::load(sphere.center()),
				sphere.radius()
			)
		);
	}

	__host__
	static Pointer<const Shape> load(MemoryPool& pool, luc::triangle& triangle) {
		return Pointer<const Shape>(
			Triangle::build(
				pool,
				XMLGeometry::load(triangle.point1()),
				XMLGeometry::load(triangle.point2()),
				XMLGeometry::load(triangle.point3())
			)
		);
	}

	__host__
	static Pointer<const Shape> load(MemoryPool& pool, luc::box& box) {
		//build vertex data
		Data data;
		Box b(XMLGeometry::load(box.a()), XMLGeometry::load(box.b()));
		Transformation3F transformation = XMLGeometry::load(box.transformation());
		data.vrtx.emplace_back(transformation * Point3F(b.lower()[0], b.lower()[1], b.lower()[2]));
		data.vrtx.emplace_back(transformation * Point3F(b.upper()[0], b.lower()[1], b.lower()[2]));
		data.vrtx.emplace_back(transformation * Point3F(b.upper()[0], b.upper()[1], b.lower()[2]));
		data.vrtx.emplace_back(transformation * Point3F(b.lower()[0], b.upper()[1], b.lower()[2]));
		data.vrtx.emplace_back(transformation * Point3F(b.lower()[0], b.upper()[1], b.upper()[2]));
		data.vrtx.emplace_back(transformation * Point3F(b.upper()[0], b.upper()[1], b.upper()[2]));
		data.vrtx.emplace_back(transformation * Point3F(b.upper()[0], b.lower()[1], b.upper()[2]));
		data.vrtx.emplace_back(transformation * Point3F(b.lower()[0], b.lower()[1], b.upper()[2]));
		// build face data
		std::vector<Mesh> meshes;
		meshes.emplace_back();
		auto& mesh = meshes.back();
		mesh.vrtx.emplace_back(0, 2, 1);
		mesh.vrtx.emplace_back(0, 3, 2);
		mesh.vrtx.emplace_back(2, 3, 4);
		mesh.vrtx.emplace_back(2, 4, 5);
		mesh.vrtx.emplace_back(1, 2, 5);
		mesh.vrtx.emplace_back(1, 5, 6);
		mesh.vrtx.emplace_back(0, 7, 4);
		mesh.vrtx.emplace_back(0, 4, 3);
		mesh.vrtx.emplace_back(5, 4, 7);
		mesh.vrtx.emplace_back(5, 7, 6);
		mesh.vrtx.emplace_back(0, 6, 7);
		mesh.vrtx.emplace_back(0, 1, 6);
		// create triangles
		auto meshVrtxData = Pointer<const MeshVrtxData>(MeshVrtxData::build(pool, data));
		auto meshFaceData = Pointer<const MeshFaceData>(MeshFaceData::build(pool, meshVrtxData, mesh));
		std::vector<Pointer<const Shape>> triangles;
		buildTriangleMesh(pool, meshFaceData, triangles);
		// create accelaration structure
		auto mode = GroupModeBuilder::buildFrom(box.mode());
		if (mode == GroupMode::SET) {
			return Pointer<const Shape>(
				ShapeSet::build(
					pool,
					ShapeSet::array_t(pool, triangles.size(), triangles.data())
				)
			);
		}
		if (mode == GroupMode::BVH) {
			return Pointer<const Shape>(
				BVHShape::build(
					pool,
					BVHShape::array_t(pool, triangles.size(), triangles.data())
				)
			);
		}
		return Pointer<const Shape>(
			STBVHShape::build(
				pool,
				STBVHShape::array_t(pool, triangles.size(), triangles.data())
			)
		);
	}

	__host__
	static Pointer<const Shape> load(MemoryPool& pool, luc::mesh_shape& xml, Library* library) {
		// create triangles
		auto data = XMLMesh::load(pool, xml.data(), library);
		auto meshVrtxData = Pointer<const MeshVrtxData>(MeshVrtxData::build(pool, data->vertexes));
		Array<Pointer<const MeshFaceData>> meshFaceData(pool, data->meshes.size());
		for (Index3::index_t i = 0; i < data->meshes.size(); ++i) {
			if (data->meshes[i].vrtx.size() > 0) {
				meshFaceData[i] = Pointer<const MeshFaceData>(MeshFaceData::build(pool, meshVrtxData, data->meshes[i]));
			}
		}
		std::vector<Pointer<const Shape>> triangles;
		buildTriangleMesh(pool, meshFaceData, triangles);
		// create accelaration structure
		auto mode = GroupModeBuilder::buildFrom(xml.mode());
		if (mode == GroupMode::SET) {
			return Pointer<const Shape>(
				ShapeSet::build(
					pool,
					ShapeSet::array_t(pool, triangles.size(), triangles.data())
				)
			);
		}
		if (mode == GroupMode::BVH) {
			return Pointer<const Shape>(
				BVHShape::build(
					pool,
					BVHShape::array_t(pool, triangles.size(), triangles.data())
				)
			);
		}
		return Pointer<const Shape>(
			STBVHShape::build(
				pool,
				STBVHShape::array_t(pool, triangles.size(), triangles.data())
			)
		);
	}

	__host__
	static Pointer<const Shape> load(MemoryPool& pool, luc::bump_mapped_shape& bump, Library* library) {
		auto map = XMLShadingNode::load(pool, bump.bump_map(), library);
		auto shape = load(pool, bump.shape(), library);
		return Pointer<const Shape>(
			BumpMappedShape::build(
				pool,
				map,
				shape,
				bump.delta()
			)
		);
	}

	__host__
	static Pointer<const Shape> load(MemoryPool& pool, luc::normal_mapped_shape& normal, Library* library) {
		auto map = XMLShadingNode::load(pool, normal.normal_map(), library);
		auto factor = XMLShadingNode::load(pool, normal.strength(), library);
		auto shape = load(pool, normal.shape(), library);
		return Pointer<const Shape>(
			NormalMappedShape::build(
				pool,
				map,
				factor,
				shape
			)
		);
	}

	__host__
	static Pointer<const Shape> load(MemoryPool& pool, luc::shape_set& shapeSet, Library* library) {
		auto n = shapeSet.shape().size();
		GroupMode mode = GroupModeBuilder::buildFrom(shapeSet.mode());
		if (mode == GroupMode::SET) {
			ShapeSet::array_t array(pool, n);
			for (ShapeSet::index_t i = 0; i < n; i++) {
				array[i] = load(pool, shapeSet.shape()[i], library);
			}
			return Pointer<const Shape>(ShapeSet::build(pool, array));
		} else if (mode == GroupMode::BVH) {
			BVHShape::array_t array(pool, n);
			for (BVHShape::index_t i = 0; i < n; i++) {
				array[i] = load(pool, shapeSet.shape()[i], library);
			}
			return Pointer<const Shape>(BVHShape::build(pool, array));
		} else {
			STBVHShape::array_t array(pool, n);
			for (STBVHShape::index_t i = 0; i < n; i++) {
				array[i] = load(pool, shapeSet.shape()[i], library);
			}
			return Pointer<const Shape>(STBVHShape::build(pool, array));
		}
	}

	__host__
	static Pointer<const Shape> load(MemoryPool& pool, luc::inverted_shape& shape, Library* library) {
		return Pointer<const Shape>(InvertedShape::build(pool,load(pool, shape.shape(), library)));
	}

	__host__
	static Pointer<const Shape> load(MemoryPool& pool, luc::transformed_shape& transformedShape, Library* library) {
		auto transformation = XMLGeometry::load(transformedShape.transformation());
		auto shape = load(pool, transformedShape.shape(), library);
		return Pointer<const Shape>(TransformedShape::build(pool, transformation, shape));
	}

	__host__
	static Pointer<const Shape> load(MemoryPool& pool, luc::animated_shape& animatedShape, Library* library) {
		auto transformation = XMLTemporalTransformation::load(pool, animatedShape.animated_transformation());
		auto shape = load(pool, animatedShape.shape(), library);
		return Pointer<const Shape>(AnimatedShape::build(pool, transformation, shape));
	}

	__host__
	static Pointer<const Shape> load(MemoryPool& pool, luc::shape& shape, Library* library) {
		Pointer<const Shape> result;
		if (shape.sphere().present()) {
			result = load(pool, shape.sphere().get());
		}
		if (shape.triangle().present()) {
			result = load(pool, shape.triangle().get());
		}
		if (shape.box().present()) {
			result = load(pool, shape.box().get());
		}
		if (shape.mesh().present()) {
			result = load(pool, shape.mesh().get(), library);
		}
		if (shape.set().present()) {
			result = load(pool, shape.set().get(), library);
		}
		if (shape.inverted().present()) {
			result = load(pool, shape.inverted().get(), library);
		}
		if (shape.transformed().present()) {
			result = load(pool, shape.transformed().get(), library);
		}
		if (shape.animated().present()) {
			result = load(pool, shape.animated().get(), library);
		}
		if (shape.bump_mapped().present()) {
			result = load(pool, shape.bump_mapped().get(), library);
		}
		if (shape.normal_mapped().present()) {
			result = load(pool, shape.normal_mapped().get(), library);
		}
		if (shape.reference().present()) {
			result = library->shapeMap[shape.reference().get().referenced_id()];
		}
		if (!result.isNull()) {
			if (shape.id().present()) {
				library->shapeMap[shape.id().get()] = result;
			}
		}
		return result;
	}
};

}

#endif /* XMLSHAPE_CUH_ */

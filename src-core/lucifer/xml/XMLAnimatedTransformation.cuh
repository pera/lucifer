#ifndef XMLANIMATEDTRANSFORMATION_CUH_
#define XMLANIMATEDTRANSFORMATION_CUH_

#include "XMLGeometry.cuh"
#include "lucifer/math/geometry/KeyFrameTransformation.cuh"
#include "lucifer/math/geometry/StaticTransformation.cuh"
#include "lucifer/xml/generated/lucifer.hxx"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class XMLTemporalTransformation {
public:
	__host__
	XMLTemporalTransformation() = delete;

	__host__
	static Pointer<const AnimatedTransformation> load(MemoryPool& pool, luc::static_transformation3& transformation) {
		return Pointer<const AnimatedTransformation>(
			StaticTransformation::build(
				pool,
				XMLGeometry::load(transformation.transformation())
			)
		);
	}

	__host__
	static Pointer<const AnimatedTransformation> load(MemoryPool& pool, luc::key_frame_transformation3& transformation) {
		using KeyFrame = KeyFrameTransformation::KeyFrame;
		KeyFrameTransformation::array_t keyFrames(pool, transformation.frame().size());
		for (uint8_t k = 0; k < transformation.frame().size(); ++k) {
			keyFrames[k] = KeyFrame(transformation.frame()[k].time(), XMLGeometry::load(transformation.frame()[k].transformation()));
		}
		return Pointer<const AnimatedTransformation>(KeyFrameTransformation::build(pool, keyFrames));
	}

	__host__
	static Pointer<const AnimatedTransformation> load(MemoryPool& pool, luc::animated_transformation3& transformation) {
		if (transformation.static_().present()) {
			return load(pool, transformation.static_().get());
		}
		if (transformation.key_frame().present()) {
			return load(pool, transformation.key_frame().get());
		}
		return Pointer<const AnimatedTransformation>();
	}
};

}

#endif /* XMLANIMATEDTRANSFORMATION_CUH_ */

#ifndef XMLINPUT_CUH_
#define XMLINPUT_CUH_

#include <memory>
#include <vector>
#include "lucifer/color/RGBColorSpace.cuh"
#include "lucifer/image/ToneMapper.cuh"
#include "lucifer/renderer/Scene.cuh"
#include "lucifer/renderer/Renderer.cuh"
#include "lucifer/memory/MemoryPool.cuh"

namespace lucifer {

class XMLPreview {
public:
	bool autoUpdate;
	Pointer<const ToneMapper> toneMapper;
	Pointer<const RGBColorSpace> colorSpace;
};

class XMLOutput {
public:
	Pointer<const ToneMapper> toneMapper;
	std::string filename;
};

class XMLInput {
public:
	using OutputList = std::vector<XMLOutput>;
	Pointer<Renderer> renderer;
	Pointer<Scene> scene;
	XMLPreview preview;
	OutputList output;
	bool progressive;

public:
	__host__
	XMLInput(Pointer<Renderer>& renderer, Pointer<Scene>& scene, XMLPreview& preview, const bool progressive):
	renderer(renderer), scene(scene), preview(preview), progressive(progressive) {
	}
};

}

#endif /* XMLINPUT_CUH_ */

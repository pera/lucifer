#ifndef XMLTONEMAPPER_CUH_
#define XMLTONEMAPPER_CUH_

#include "lucifer/image/LinearToneMapper.cuh"
#include "lucifer/image/ReinhardToneMapper.cuh"
#include "lucifer/xml/generated/lucifer.hxx"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class XMLToneMapper {
private:
	__host__
	XMLToneMapper() {
	}

public:
	__host__
	static Pointer<const ToneMapper> load(MemoryPool& pool, luc::linear_tone_mapping& mapper) {
		return Pointer<const ToneMapper>::make<LinearToneMapper>(pool);
	}

	__host__
	static Pointer<const ToneMapper> load(MemoryPool& pool, luc::reinhard_tone_mapping& mapper) {
		return Pointer<const ToneMapper>::make<ReinhardToneMapper>(pool, mapper.key());
	}

	__host__
	static Pointer<const ToneMapper> load(MemoryPool& pool, luc::tone_mapping& mapper) {
		if (mapper.linear().present()) {
			return load(pool, mapper.linear().get());
		}
		if (mapper.reinhard().present()) {
			return load(pool, mapper.reinhard().get());
		}
		return Pointer<const ToneMapper>();
	}
};

}

#endif /* XMLTONEMAPPER_CUH_ */

#ifndef XMLLIBRARY_CUH_
#define XMLLIBRARY_CUH_

#include <iostream>
#include <memory>
#include <tuple>
#include <vector>

#include "Library.cuh"
#include "XMLEmission.cuh"
#include "XMLImage.cuh"
#include "XMLPrimitive.cuh"
#include "XMLMaterial.cuh"
#include "XMLMedium.cuh"
#include "XMLRGBColorSpace.cuh"
#include "XMLScattering.cuh"
#include "XMLShadingNode.cuh"
#include "XMLShape.cuh"
#include "lucifer/xml/generated/lucifer.hxx"
#include "lucifer/memory/MemoryPool.cuh"

namespace lucifer {

class XMLLibrary {
public:
	__host__
	XMLLibrary() = delete;

	__host__
	static void load(MemoryPool& pool, luc::library& xml, Library* library) {
		auto includeSequence = xml.include();
		for (auto inner = includeSequence.begin(); inner != includeSequence.end(); inner++) {
			try {
				std::string path = getFullPath(inner->c_str());
				if (library->includedFiles.find(path) == library->includedFiles.end()) {
					std::unique_ptr<luc::library> xmlInner(luc::library_(inner->c_str()));
					load(pool, *xmlInner, library);
					library->includedFiles.emplace(path);
				}
			} catch(const xml_schema::exception& e) {
				std::cerr << e << std::endl;
			}
		}
		auto rgbSequence = xml.rgb_color_space();
		for (auto rgb = rgbSequence.begin(); rgb != rgbSequence.end(); rgb++) {
			XMLRGBColorSpace::load(pool, *rgb, library);
		}
		auto imageSequence = xml.hdr_image();
		for (auto image: imageSequence) {
			XMLImage::load(pool, image, library);
		}
		auto shadingNodeSequence = xml.shading_node();
		for (auto shadingNode = shadingNodeSequence.begin(); shadingNode != shadingNodeSequence.end(); shadingNode++) {
			XMLShadingNode::load(pool, *shadingNode, library);
		}
		auto shapeSequence = xml.shape();
		for (auto shape = shapeSequence.begin(); shape != shapeSequence.end(); shape++) {
			XMLShape::load(pool, *shape, library);
		}
		auto emissionSequence = xml.emission();
		for (auto emission = emissionSequence.begin(); emission != emissionSequence.end(); emission++) {
			XMLEmission::load(pool, *emission, library);
		}
		auto scatteringSequence = xml.scattering();
		for (auto scattering = scatteringSequence.begin(); scattering != scatteringSequence.end(); scattering++) {
			XMLScattering::load(pool, *scattering, library);
		}
		auto mediumSequence = xml.medium();
		for (auto medium  = mediumSequence.begin(); medium != mediumSequence.end(); medium++) {
			XMLMedium::load(pool, *medium, library);
		}
		auto materialSequence = xml.material();
		for (auto material = materialSequence.begin(); material != materialSequence.end(); material++) {
			XMLMaterial::load(pool, *material, library);
		}
		auto primitiveSequence = xml.primitive();
		for (auto primitive = primitiveSequence.begin(); primitive != primitiveSequence.end(); primitive++) {
			std::vector<Pointer<const GeometricPrimitive>> plist;
			std::vector<Pointer<const GeometricPrimitive>> llist;
			XMLPrimitive::load(pool, *primitive, plist, llist, library);
		}
	}

	__host__
	static std::unique_ptr<Library> load(MemoryPool& pool, luc::scene::library_optional& library) {
		auto colorSpace = Pointer<const XYZColorSpace>::make<XYZColorSpace>(pool, pool);
		Library* result = new Library(colorSpace);
		if (library.present()) {
			load(pool, library.get(), result);
		}
		return std::unique_ptr<Library>(result);
	}
};

}

#endif /* XMLLIBRARY_CUH_ */

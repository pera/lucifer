#ifndef XMLGEOMETRY_CUH_
#define XMLGEOMETRY_CUH_

#include "lucifer/math/Util.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/xml/generated/lucifer.hxx"

namespace lucifer {

class XMLGeometry {
private:
	__host__
	XMLGeometry() {
	}

public:
	__host__
	static Point2F load(const luc::point2& v) {
		return Point2F(v.x(), v.y());
	}

	__host__
	static Vectr2F load(const luc::vector2& v) {
		return Vectr2F(v.x(), v.y());
	}

	__host__
	static Point3F load(const luc::point3& v) {
		return Point3F(v.x(), v.y(), v.z());
	}

	__host__
	static Vectr3F load(const luc::vector3& v) {
		return Vectr3F(v.x(), v.y(), v.z());
	}

	__host__
	static Transformation3F load(const luc::transformation3& t) {
		if (t.translate().present()) {
			Vectr3F vec = load(t.translate().get());
			return Transformation3F::translation(vec);
		}
		if (t.scale().present()) {
			Vectr3F vec = load(t.scale().get());
			return Transformation3F::scaling(vec);
		}
		if (t.rotate_x().present()) {
			float angle = t.rotate_x().get().angle();
			if (t.rotate_x().get().unit() == luc::angle_unit::degrees) {
				angle = toRadians(angle);
			}
			return Transformation3F::rotation<1, 2>(angle);
		}
		if (t.rotate_y().present()) {
			float angle = t.rotate_y().get().angle();
			if (t.rotate_y().get().unit() == luc::angle_unit::degrees) {
				angle = toRadians(angle);
			}
			return Transformation3F::rotation<2, 0>(angle);
		}
		if (t.rotate_z().present()) {
			float angle = t.rotate_z().get().angle();
			if (t.rotate_z().get().unit() == luc::angle_unit::degrees) {
				angle = toRadians(angle);
			}
			return Transformation3F::rotation<0, 1>(angle);
		}
		if (t.look_at().present()) {
			Point3F from = load(t.look_at().get().from());
			Point3F   to = load(t.look_at().get().to()  );
			Vectr3F   up = load(t.look_at().get().up()  );
			return Transformation3F::lookAt(from, to, up);
		}
		return Transformation3F::identity();
	}

	__host__
	static Transformation3F load(const ::xsd::cxx::tree::sequence<luc::transformation3>& seq) {
		Transformation3F result = Transformation3F::identity();
		for (auto t = seq.begin(); t != seq.end(); t++) {
			result *= load(*t);
		}
		return result;
	}

	__host__
	static Affine2F load(const luc::transformation2& t) {
		if (t.translate().present()) {
			Vectr2F v = load(t.scale().get());
			return Affine2F::translation(-v);
		}
		if (t.scale().present()) {
			Vectr2F v = load(t.scale().get());
			return Affine2F::scaling(Vectr2F { 1.f / v[0], 1.f / v[1] });
		}
		if (t.rotate().present()) {
			float angle = t.rotate().get().angle();
			if (t.rotate().get().unit() == luc::angle_unit::degrees) {
				angle = toRadians(angle);
			}
			return Affine2F::rotation(0, 1, -angle);
		}
		return Affine2F::identity();
	}

	__host__
	static Affine2F load(const ::xsd::cxx::tree::sequence<luc::transformation2>& seq) {
		Affine2F result = Affine2F::identity();
		for (auto t = seq.begin(); t != seq.end(); t++) {
			result *= load(*t);
		}
		return result;
	}
};

}

#endif /* XMLGEOMETRY_CUH_ */

#ifndef XMLSHADINGNODE_CUH_
#define XMLSHADINGNODE_CUH_

#include <cassert>
#include <random>
#include "XMLFilter.cuh"
#include "XMLGeometry.cuh"
#include "XMLImage.cuh"
#include "XMLMapping.cuh"
#include "XMLRGBColorSpace.cuh"
#include "lucifer/node/ConstantNode.cuh"
#include "lucifer/node/ConverterNode.cuh"
#include "lucifer/node/MixNode.cuh"
#include "lucifer/node/boolean/AndNode.cuh"
#include "lucifer/node/boolean/IfElseNode.cuh"
#include "lucifer/node/boolean/GreaterThanNode.cuh"
#include "lucifer/node/boolean/LessThanNode.cuh"
#include "lucifer/node/boolean/NotNode.cuh"
#include "lucifer/node/boolean/OrNode.cuh"
#include "lucifer/node/color/ColorSpaceNode.cuh"
#include "lucifer/node/color/ImageNode.cuh"
#include "lucifer/node/color/LuminanceNode.cuh"
#include "lucifer/node/math/PiecewiseLinearFunction1DNode.cuh"
#include "lucifer/node/math/PiecewiseLinearIrregular1DNode.cuh"
#include "lucifer/node/math/NegationNode.cuh"
#include "lucifer/node/math/AbsoluteNode.cuh"
#include "lucifer/node/math/InversionNode.cuh"
#include "lucifer/node/math/LogarithmNode.cuh"
#include "lucifer/node/math/ExponentialNode.cuh"
#include "lucifer/node/math/SquareRootNode.cuh"
#include "lucifer/node/math/SineNode.cuh"
#include "lucifer/node/math/CosineNode.cuh"
#include "lucifer/node/math/TangentNode.cuh"
#include "lucifer/node/math/AdditionNode.cuh"
#include "lucifer/node/math/SubtractionNode.cuh"
#include "lucifer/node/math/MultiplicationNode.cuh"
#include "lucifer/node/math/DivisionNode.cuh"
#include "lucifer/node/math/MinimumNode.cuh"
#include "lucifer/node/math/MaximumNode.cuh"
#include "lucifer/node/shading/NormalNode.cuh"
#include "lucifer/node/shading/PositionNode.cuh"
#include "lucifer/node/shading/WavelengthNode.cuh"
#include "lucifer/node/shading/TangentVectorNode.cuh"
#include "lucifer/node/shading/TimeNode.cuh"
#include "lucifer/node/shading/UVNode.cuh"
#include "lucifer/node/spectrum/BlackbodyNode.cuh"
#include "lucifer/node/spectrum/RayleighSpectrumNode.cuh"
#include "lucifer/node/spectrum/RGBSpectrumNode.cuh"
#include "lucifer/node/spectrum/SellmeierNode.cuh"
#include "lucifer/node/field/Checker2DNode.cuh"
#include "lucifer/node/field/Checker3DNode.cuh"
#include "lucifer/node/field/PerlinNoise2DNode.cuh"
#include "lucifer/node/field/PerlinNoise3DNode.cuh"
#include "lucifer/node/field/RegularGrid2DNode.cuh"
#include "lucifer/node/field/RegularGrid3DNode.cuh"
#include "lucifer/node/field/TriplanarNode.cuh"
#include "lucifer/node/field/VoronoiNoise2DNode.cuh"
#include "lucifer/node/field/VoronoiNoise3DNode.cuh"
#include "lucifer/node/geometry/DotProductNode.cuh"
#include "lucifer/node/geometry/PairNode.cuh"
#include "lucifer/node/geometry/Transform3DNode.cuh"
#include "lucifer/node/geometry/Transform2DNode.cuh"
#include "lucifer/node/geometry/TripletNode.cuh"
#include "lucifer/node/geometry/TupleElementNode.cuh"
#include "lucifer/node/geometry/UVMappingNode.cuh"
#include "lucifer/xml/Library.cuh"
#include "lucifer/memory/MemoryPool.cuh"

namespace lucifer {

class XMLShadingNode {
public:
	__host__
	XMLShadingNode() = delete;

	/**************************************************************************
	 * shading boolean nodes
	 **************************************************************************/
	__host__
	static Pointer<const ShadingNode<bool>> load(MemoryPool& pool, const luc::shading_constant_boolean_node& node) {
		return Pointer<const ShadingNode<bool>>(ConstantNode<bool, LUCIFER_SHADING_INPUT>::build(pool, node.value()));
	}

	__host__
	static Pointer<const ShadingNode<bool>> load(MemoryPool& pool, const luc::shading_not_boolean_node& node, Library* library) {
		auto arg = load(pool, node.argument(), library);
		return Pointer<const ShadingNode<bool>>(NotNode<LUCIFER_SHADING_INPUT>::build(pool, arg));
	}

	__host__
	static Pointer<const ShadingNode<bool>> load(MemoryPool& pool, const luc::shading_and_boolean_node& node, Library* library) {
		auto lhs = load(pool, node.lhs(), library);
		auto rhs = load(pool, node.rhs(), library);
		return Pointer<const ShadingNode<bool>>(AndNode<LUCIFER_SHADING_INPUT>::build(pool, lhs, rhs));
	}

	__host__
	static Pointer<const ShadingNode<bool>> load(MemoryPool& pool, const luc::shading_or_boolean_node& node, Library* library) {
		auto lhs = load(pool, node.lhs(), library);
		auto rhs = load(pool, node.rhs(), library);
		return Pointer<const ShadingNode<bool>>(OrNode<LUCIFER_SHADING_INPUT>::build(pool, lhs, rhs));
	}

	__host__
	static Pointer<const ShadingNode<bool>> load(MemoryPool& pool, const luc::shading_less_than_boolean_node& node, Library* library) {
		auto lhs = load(pool, node.lhs(), library);
		auto rhs = load(pool, node.rhs(), library);
		return Pointer<const ShadingNode<bool>>(LessThanNode<LUCIFER_SHADING_INPUT>::build(pool, lhs, rhs));
	}

	__host__
	static Pointer<const ShadingNode<bool>> load(MemoryPool& pool, const luc::shading_greater_than_boolean_node& node, Library* library) {
		auto lhs = load(pool, node.lhs(), library);
		auto rhs = load(pool, node.rhs(), library);
		return Pointer<const ShadingNode<bool>>(GreaterThanNode<LUCIFER_SHADING_INPUT>::build(pool, lhs, rhs));
	}

	__host__
	static Pointer<const ShadingNode<bool>> load(MemoryPool& pool, const luc::shading_boolean_node& node, Library* library) {
		Pointer<const ShadingNode<bool>> result;
		if (node.constant().present()) {
			result = load(pool, node.constant().get());
		}
		if (node.not_().present()) {
			result = load(pool, node.not_().get(), library);
		}
		if (node.and_().present()) {
			result = load(pool, node.and_().get(), library);
		}
		if (node.or_().present()) {
			result = load(pool, node.or_().get(), library);
		}
		if (node.less_than().present()) {
			result = load(pool, node.less_than().get(), library);
		}
		if (node.greater_than().present()) {
			result = load(pool, node.greater_than().get(), library);
		}
		if (node.reference().present()) {
			result = library->shadingBoolNodeMap[node.reference().get().referenced_id()];
		}
		if (!result.isNull()) {
			if (node.id().present()) {
				library->shadingBoolNodeMap[node.id().get()] = result;
			}
		}
		return result;
	}

	/**************************************************************************
	 * shading float nodes
	 **************************************************************************/
	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_wavelength_node& node) {
		return Pointer<const ShadingNode<float>>(WavelengthNode::build(pool));
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_time_node& node) {
		return Pointer<const ShadingNode<float>>(TimeNode::build(pool));
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_constant_float_node& node) {
		return Pointer<const ShadingNode<float>>(ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, node.value()));
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_regular_piecewise_linear_float_node& node, Library* library) {
		using index_t = PiecewiseLinearFunction1DNode<float, LUCIFER_SHADING_INPUT>::index_t;
		using array_t = PiecewiseLinearFunction1DNode<float, LUCIFER_SHADING_INPUT>::array_t;
		index_t size = node.values().size();
		array_t values(pool, size);
		for (index_t i = 0; i < size; ++i) values[i] = node.values()[i];
		return Pointer<const ShadingNode<float>>(
			PiecewiseLinearFunction1DNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.argument(), library),
				node.min_argument(),
				node.max_argument(),
				values
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_irregular_piecewise_linear_float_node& node, Library* library) {
		using index_t = PiecewiseLinearIrregular1DNode<float, LUCIFER_SHADING_INPUT>::index_t;
		using x_arr_t = PiecewiseLinearIrregular1DNode<float, LUCIFER_SHADING_INPUT>::x_arr_t;
		using y_arr_t = PiecewiseLinearIrregular1DNode<float, LUCIFER_SHADING_INPUT>::y_arr_t;
		index_t size = node.arguments().size();
		x_arr_t arguments(pool, size);
		y_arr_t values(pool, size);
		for (index_t i = 0; i < size; ++i) {
			arguments[i] = node.arguments()[i];
			values[i] = node.values()[i];
		}
		return Pointer<const ShadingNode<float>>(
			PiecewiseLinearIrregular1DNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.argument(), library),
				arguments,
				values
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_luminance_node& node, Library* library) {
		Pointer<const RGBColorSpace> space;
		if (node.color_space().present()) space = XMLRGBColorSpace::load(pool, node.color_space().get(), library);
		return Pointer<const ShadingNode<float>>(
			LuminanceNode<LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.color(), library),
				space
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_blackbody_spectrum_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			BlackbodyNode<LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.wavelength(), library),
				load(pool, node.temperature(), library),
				node.normalize(),
				node.minWavelength(),
				node.maxWavelength()
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_sellmeier_spectrum_node& node, Library* library) {
		auto b = node.b();
		auto c = node.c();
		return Pointer<const ShadingNode<float>>(
			SellmeierNode<LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.wavelength(), library),
				b[0], b[1], b[2],
				c[0], c[1], c[2]
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_rayleigh_spectrum_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			RayleighSpectrumNode<LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.wavelength(), library),
				load(pool, node.height(), library),
				load(pool, node.density(), library),
				load(pool, node.ior(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_rgb_spectrum_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			RGBSpectrumNode<LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.rgb(), library),
				load(pool, node.wavelength(), library),
				XMLRGBColorSpace::load(pool, node.color_space(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_tuple_axis_node& node, Library* library) {
		if (node.point2().present()) {
			assert(node.axis() < 2);
			return Pointer<const ShadingNode<float>>(
				TupleElementNode<Point2F, LUCIFER_SHADING_INPUT>::build(
					pool,
					node.axis(),
					load(pool, node.point2().get(),
					library)
				)
			);
		}
		if (node.point3().present()) {
			assert(node.axis() < 3);
			return Pointer<const ShadingNode<float>>(
				TupleElementNode<Point3F, LUCIFER_SHADING_INPUT>::build(
					pool,
					node.axis(),
					load(pool, node.point3().get(),
					library)
				)
			);
		}
		if (node.vector().present()) {
			assert(node.axis() < 3);
			return Pointer<const ShadingNode<float>>(
				TupleElementNode<Vectr3F, LUCIFER_SHADING_INPUT>::build(
					pool,
					node.axis(),
					load(pool, node.vector().get(),
					library)
				)
			);
		}
		if (node.normal().present()) {
			assert(node.axis() < 3);
			return Pointer<const ShadingNode<float>>(
				TupleElementNode<Norml3F, LUCIFER_SHADING_INPUT>::build(
					pool,
					node.axis(),
					load(pool, node.normal().get(),
					library)
				)
			);
		}
		if (node.color().present()) {
			assert(node.axis() < 3);
			return Pointer<const ShadingNode<float>>(
				TupleElementNode<Color3F, LUCIFER_SHADING_INPUT>::build(
					pool,
					node.axis(),
					load(pool, node.color().get(),
					library))
			);
		}
		assert(false);
		return Pointer<const ShadingNode<float>>();
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_checker_float_2d_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			Checker2DNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.node_a(), library),
				load(pool, node.node_b(), library),
				load(pool, node.point (), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_checker_float_3d_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			Checker3DNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.node_a(), library),
				load(pool, node.node_b(), library),
				load(pool, node.point(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_value_noise_2d_node& node, Library* library) {
		RegularGrid2DNode<float, LUCIFER_SHADING_INPUT>::idx2d_t nx = node.nx();
		RegularGrid2DNode<float, LUCIFER_SHADING_INPUT>::idx2d_t ny = node.ny();
		RegularGrid2DNode<float, LUCIFER_SHADING_INPUT>::array_t values(pool, nx * ny);
		std::default_random_engine engine(node.seed());
		std::uniform_real_distribution<float> uniform;
		auto lower = node.min_value();
		auto upper = node.max_value();
		for (auto& value : values) value = lower + uniform(engine) * (upper - lower);
		return Pointer<const ShadingNode<float>>(
			RegularGrid2DNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.point(), library),
				nx, ny, values,
				interpolationMode(node.interpolation().c_str())
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_value_noise_3d_node& node, Library* library) {
		RegularGrid3DNode<float, LUCIFER_SHADING_INPUT>::idx3d_t nx = node.nx();
		RegularGrid3DNode<float, LUCIFER_SHADING_INPUT>::idx3d_t ny = node.ny();
		RegularGrid3DNode<float, LUCIFER_SHADING_INPUT>::idx3d_t nz = node.nz();
		RegularGrid3DNode<float, LUCIFER_SHADING_INPUT>::array_t values(pool, nx * ny * nz);
		std::default_random_engine engine(node.seed());
		std::uniform_real_distribution<float> uniform;
		auto lower = node.min_value();
		auto upper = node.max_value();
		for (auto& value : values) value = lower + uniform(engine) * (upper - lower);
		return Pointer<const ShadingNode<float>>(
			RegularGrid3DNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.point(), library),
				nx, ny, nz, values,
				interpolationMode(node.interpolation().c_str())
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_perlin_noise_2d_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			PerlinNoise2DNode<LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.point(), library),
				node.min_value(), node.max_value(),
				node.nx(), node.ny(), node.seed()
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_perlin_noise_3d_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			PerlinNoise3DNode<LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.point(), library),
				node.min_value(), node.max_value(),
				node.nx(), node.ny(), node.nz(), node.seed()
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_voronoi_noise_2d_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			VoronoiNoise2DNode<LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.point(), library),
				node.min_value(), node.max_value(),
				node.nx(), node.ny(), node.jitter(), node.seed()
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_voronoi_noise_3d_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			VoronoiNoise3DNode<LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.point(), library),
				node.min_value(), node.max_value(),
				node.nx(), node.ny(), node.nz(),
				node.jitter(), node.seed()
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_mix_float_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			MixNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.node_a(), library),
				load(pool, node.node_b(), library),
				load(pool, node.weight(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_negation_float_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			NegationNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.argument(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_absolute_float_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			AbsoluteNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.argument(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_inversion_float_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			InversionNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.argument(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_logarithm_float_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			LogarithmNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.argument(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_exponential_float_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			ExponentialNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.argument(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_square_root_float_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			SquareRootNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.argument(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_sine_float_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			SineNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.argument(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_cosine_float_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			CosineNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.argument(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_tangent_float_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			TangentNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.argument(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_addition_float_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			AdditionNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.lhs(), library),
				load(pool, node.rhs(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_subtraction_float_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			SubtractionNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.lhs(), library),
				load(pool, node.rhs(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_multiplication_float_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			MultiplicationNode<float, float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.lhs(), library),
				load(pool, node.rhs(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_division_float_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			DivisionNode<float, float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.lhs(), library),
				load(pool, node.rhs(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_minimum_float_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			MinimumNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.lhs(), library),
				load(pool, node.rhs(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_maximum_float_node& node, Library* library) {
		return Pointer<const ShadingNode<float>>(
			MaximumNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.lhs(), library),
				load(pool, node.rhs(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_sample_wavelength_float_node& node, Library* library) {
		auto minWLen = node.min_argument();
		auto maxWLen = node.max_argument();
		auto samples = node.samples();
		PiecewiseLinearFunction1DNode<float, LUCIFER_SHADING_INPUT>::array_t values(pool, samples);
		Pointer<const ShadingNode<float>> inner = load(pool, node.node(), library);
		Point3F p; Vectr3F t; Norml3F n; Point2F u;
		for (int i = 0; i < samples; i++) {
			values[i] = (*inner)(p, t, n, u, minWLen + i * (maxWLen - minWLen) / (samples - 1.f), 0.f);
		}
		return Pointer<const ShadingNode<float>>(
			PiecewiseLinearFunction1DNode<float, LUCIFER_SHADING_INPUT>::build(
				pool,
				Pointer<const ShadingNode<float>>(WavelengthNode::build(pool)),
				minWLen, maxWLen, values
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_triplanar_float_node& node, Library* library) {
		auto n = load(pool, node.normal(), library);
		auto x = load(pool, node.x(), library);
		auto y = load(pool, node.y(), library);
		auto z = load(pool, node.z(), library);
		auto e = load(pool, node.e(), library);
		return Pointer<const ShadingNode<float>>(
			TriplanarNode<float, LUCIFER_SHADING_INPUT>::build(pool, n, x, y, z, e)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_dot_product_float_node& node, Library* library) {
		auto lhs = load(pool, node.lhs(), library);
		auto rhs = load(pool, node.rhs(), library);
		return Pointer<const ShadingNode<float>>(
			DotProductNode<Vectr3F, LUCIFER_SHADING_INPUT>::build(pool, lhs, rhs)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_if_else_float_node& node, Library* library) {
		auto condition = load(pool, node.condition(), library);
		auto ifTrue = load(pool, node.then(), library);
		auto ifFalse = load(pool, node.else_(), library);
		return Pointer<const ShadingNode<float>>(
			IfElseNode<float, LUCIFER_SHADING_INPUT>::build(pool, condition, ifTrue, ifFalse)
		);
	}

	__host__
	static Pointer<const ShadingNode<float>> load(MemoryPool& pool, const luc::shading_float_node& node, Library* library) {
		Pointer<const ShadingNode<float>> result;
		if (node.wavelength().present()) {
			result = load(pool, node.wavelength().get());
		}
		if (node.time().present()) {
			result = load(pool, node.time().get());
		}
		if (node.constant().present()) {
			result = load(pool, node.constant().get());
		}
		if (node.regular_piecewise_linear().present()) {
			result = load(pool, node.regular_piecewise_linear().get(), library);
		}
		if (node.irregular_piecewise_linear().present()) {
			result = load(pool, node.irregular_piecewise_linear().get(), library);
		}
		if (node.luminance().present()) {
			result = load(pool, node.luminance().get(), library);
		}
		if (node.blackbody_spectrum().present()) {
			result = load(pool, node.blackbody_spectrum().get(), library);
		}
		if (node.sellmeier_spectrum().present()) {
			result = load(pool, node.sellmeier_spectrum().get(), library);
		}
		if (node.rayleigh_spectrum().present()) {
			result = load(pool, node.rayleigh_spectrum().get(), library);
		}
		if (node.rgb_spectrum().present()) {
			result = load(pool, node.rgb_spectrum().get(), library);
		}
		if (node.axis().present()) {
			result = load(pool, node.axis().get(), library);
		}
		if (node.checker_2d().present()) {
			result = load(pool, node.checker_2d().get(), library);
		}
		if (node.checker_3d().present()) {
			result = load(pool, node.checker_3d().get(), library);
		}
		if (node.value_noise_2d().present()) {
			result = load(pool, node.value_noise_2d().get(), library);
		}
		if (node.value_noise_3d().present()) {
			result = load(pool, node.value_noise_3d().get(), library);
		}
		if (node.perlin_2d().present()) {
			result = load(pool, node.perlin_2d().get(), library);
		}
		if (node.perlin_3d().present()) {
			result = load(pool, node.perlin_3d().get(), library);
		}
		if (node.voronoi_2d().present()) {
			result = load(pool, node.voronoi_2d().get(), library);
		}
		if (node.voronoi_3d().present()) {
			result = load(pool, node.voronoi_3d().get(), library);
		}
		if (node.mix().present()) {
			result = load(pool, node.mix().get(), library);
		}
		if (node.neg().present()) {
			result = load(pool, node.neg().get(), library);
		}
		if (node.abs().present()) {
			result = load(pool, node.abs().get(), library);
		}
		if (node.inv().present()) {
			result = load(pool, node.inv().get(), library);
		}
		if (node.log().present()) {
			result = load(pool, node.log().get(), library);
		}
		if (node.exp().present()) {
			result = load(pool, node.exp().get(), library);
		}
		if (node.sin().present()) {
			result = load(pool, node.sin().get(), library);
		}
		if (node.cos().present()) {
			result = load(pool, node.cos().get(), library);
		}
		if (node.tan().present()) {
			result = load(pool, node.tan().get(), library);
		}
		if (node.sqrt().present()) {
			result = load(pool, node.sqrt().get(), library);
		}
		if (node.add().present()) {
			result = load(pool, node.add().get(), library);
		}
		if (node.sub().present()) {
			result = load(pool, node.sub().get(), library);
		}
		if (node.mul().present()) {
			result = load(pool, node.mul().get(), library);
		}
		if (node.div().present()) {
			result = load(pool, node.div().get(), library);
		}
		if (node.min().present()) {
			result = load(pool, node.min().get(), library);
		}
		if (node.max().present()) {
			result = load(pool, node.max().get(), library);
		}
		if (node.sample_wavelength().present()) {
			result = load(pool, node.sample_wavelength().get(), library);
		}
		if (node.triplanar().present()) {
			result = load(pool, node.triplanar().get(), library);
		}
		if (node.dot_product().present()) {
			result = load(pool, node.dot_product().get(), library);
		}
		if (node.if_().present()) {
			result = load(pool, node.if_().get(), library);
		}
		if (node.reference().present()) {
			result = library->shadingFloatNodeMap[node.reference().get().referenced_id()];
		}
		if (!result.isNull()) {
			if (node.id().present()) {
				library->shadingFloatNodeMap[node.id().get()] = result;
			}
		}
		return result;
	}

	/**************************************************************************
	 * shading point3 nodes
	 **************************************************************************/
	__host__
	static Pointer<const ShadingNode<Point3F>> load(MemoryPool& pool, const luc::shading_position_node& node) {
		return Pointer<const ShadingNode<Point3F>>(PositionNode::build(pool));
	}

	__host__
	static Pointer<const ShadingNode<Point3F>> load(MemoryPool& pool, const luc::shading_constant_point3_node& node) {
		return Pointer<const ShadingNode<Point3F>>(
			ConstantNode<Point3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				Point3F(node.x(), node.y(), node.z())
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Point3F>> load(MemoryPool& pool, const luc::shading_assemble_point3_node& node, Library* library) {
		return Pointer<const ShadingNode<Point3F>>(
			TripletNode<Point3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.x(), library),
				load(pool, node.y(), library),
				load(pool, node.z(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Point3F>> load(MemoryPool& pool, const luc::shading_convert_to_point3_node& node, Library* library) {
		if (node.color().present()) {
			return Pointer<const ShadingNode<Point3F>>(
				ConverterNode<Point3F,  Color3F, LUCIFER_SHADING_INPUT>::build(
					pool,
					load(pool, node.color().get(),  library)
				)
			);
		}
		if (node.vector().present()) {
			return Pointer<const ShadingNode<Point3F>>(
				ConverterNode<Point3F, Vectr3F, LUCIFER_SHADING_INPUT>::build(
					pool,
					load(pool, node.vector().get(), library)
				)
			);
		}
		if (node.normal().present()) {
			return Pointer<const ShadingNode<Point3F>>(
				ConverterNode<Point3F, Norml3F, LUCIFER_SHADING_INPUT>::build(
					pool,
					load(pool, node.normal().get(), library)
				)
			);
		}
		assert(false);
		return Pointer<const ShadingNode<Point3F>>();
	}

	__host__
	static Pointer<const ShadingNode<Point3F>> load(MemoryPool& pool, const luc::shading_transformed_point3_node& node, Library* library) {
		return Pointer<const ShadingNode<Point3F>>(
			Transform3DNode<Point3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.point(), library),
				XMLGeometry::load(node.transformation())
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Point3F>> load(MemoryPool& pool, const luc::shading_if_else_point3_node& node, Library* library) {
		auto condition = load(pool, node.condition(), library);
		auto ifTrue = load(pool, node.then(), library);
		auto ifFalse = load(pool, node.else_(), library);
		return Pointer<const ShadingNode<Point3F>>(
			IfElseNode<Point3F, LUCIFER_SHADING_INPUT>::build(pool, condition, ifTrue, ifFalse)
		);
	}

	__host__
	static Pointer<const ShadingNode<Point3F>> load(MemoryPool& pool, const luc::shading_point3_node& node, Library* library) {
		Pointer<const ShadingNode<Point3F>> result;
		if (node.position().present()) {
			result = load(pool, node.position().get());
		}
		if (node.constant().present()) {
			result = load(pool, node.constant().get());
		}
		if (node.assemble().present()) {
			result = load(pool, node.assemble().get(), library);
		}
		if (node.convert().present()) {
			result = load(pool, node.convert().get(), library);
		}
		if (node.transformed().present()) {
			result = load(pool, node.transformed().get(), library);
		}
		if (node.if_().present()) {
			result = load(pool, node.if_().get(), library);
		}
		if (node.reference().present()) {
			result = library->shadingPoint3FNodeMap[node.reference().get().referenced_id()];
		}
		if (!result.isNull()) {
			if (node.id().present()) {
				library->shadingPoint3FNodeMap[node.id().get()] = result;
			}
		}
		return result;
	}

	/**************************************************************************
	 * shading norml3 nodes
	 **************************************************************************/
	__host__
	static Pointer<const ShadingNode<Norml3F>> load(MemoryPool& pool, const luc::shading_normal_vector_node& node) {
		return Pointer<const ShadingNode<Norml3F>>(NormalNode::build(pool));
	}

	__host__
	static Pointer<const ShadingNode<Norml3F>> load(MemoryPool& pool, const luc::shading_constant_normal_node& node) {
		return Pointer<const ShadingNode<Norml3F>>(
			ConstantNode<Norml3F, LUCIFER_SHADING_INPUT>::build(pool, Norml3F(node.x(), node.y(), node.z()))
		);
	}

	__host__
	static Pointer<const ShadingNode<Norml3F>> load(MemoryPool& pool, const luc::shading_assemble_normal_node& node, Library* library) {
		return Pointer<const ShadingNode<Norml3F>>(
			TripletNode<Norml3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.x(), library),
				load(pool, node.y(), library),
				load(pool, node.z(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Norml3F>> load(MemoryPool& pool, const luc::shading_convert_to_normal_node& node, Library* library) {
		if (node.color().present()) {
			return Pointer<const ShadingNode<Norml3F>>(
				ConverterNode<Norml3F,  Color3F, LUCIFER_SHADING_INPUT>::build(
					pool,
					load(pool, node.color().get(),  library)
				)
			);
		}
		if (node.point3().present()) {
			return Pointer<const ShadingNode<Norml3F>>(
				ConverterNode<Norml3F, Point3F, LUCIFER_SHADING_INPUT>::build(
					pool,
					load(pool, node.point3().get(), library)
				)
			);
		}
		if (node.vector().present()) {
			return Pointer<const ShadingNode<Norml3F>>(
				ConverterNode<Norml3F, Vectr3F, LUCIFER_SHADING_INPUT>::build(
					pool,
					load(pool, node.vector().get(), library))
			);
		}
		assert(false);
		return Pointer<const ShadingNode<Norml3F>>();
	}

	__host__
	static Pointer<const ShadingNode<Norml3F>> load(MemoryPool& pool, const luc::shading_transformed_normal_node& node, Library* library) {
		return Pointer<const ShadingNode<Norml3F>>(
			Transform3DNode<Norml3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.normal(), library),
				XMLGeometry::load(node.transformation())
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Norml3F>> load(MemoryPool& pool, const luc::shading_if_else_normal_node& node, Library* library) {
		auto condition = load(pool, node.condition(), library);
		auto ifTrue = load(pool, node.then(), library);
		auto ifFalse = load(pool, node.else_(), library);
		return Pointer<const ShadingNode<Norml3F>>(
			IfElseNode<Norml3F, LUCIFER_SHADING_INPUT>::build(pool, condition, ifTrue, ifFalse)
		);
	}

	__host__
	static Pointer<const ShadingNode<Norml3F>> load(MemoryPool& pool, const luc::shading_normal_node& node, Library* library) {
		Pointer<const ShadingNode<Norml3F>> result;
		if (node.normal().present()) {
			result = load(pool, node.normal().get());
		}
		if (node.constant().present()) {
			result = load(pool, node.constant().get());
		}
		if (node.assemble().present()) {
			result = load(pool, node.assemble().get(), library);
		}
		if (node.convert().present()) {
			result = load(pool, node.convert().get(), library);
		}
		if (node.transformed().present()) {
			result = load(pool, node.transformed().get(), library);
		}
		if (node.if_().present()) {
			result = load(pool, node.if_().get(), library);
		}
		if (node.reference().present()) {
			result = library->shadingNorml3FNodeMap[node.reference().get().referenced_id()];
		}
		if (!result.isNull()) {
			if (node.id().present()) {
				library->shadingNorml3FNodeMap[node.id().get()] = result;
			}
		}
		return result;
	}

	/**************************************************************************
	 * shading vectr3 nodes
	 **************************************************************************/
	__host__
	static Pointer<const ShadingNode<Vectr3F>> load(MemoryPool& pool, const luc::shading_tangent_vector_node& node) {
		return Pointer<const ShadingNode<Vectr3F>>(TangentVectorNode::build(pool));
	}

	__host__
	static Pointer<const ShadingNode<Vectr3F>> load(MemoryPool& pool, const luc::shading_constant_vector_node& node) {
		return Pointer<const ShadingNode<Vectr3F>>(
			ConstantNode<Vectr3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				Vectr3F(node.x(), node.y(), node.z())
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Vectr3F>> load(MemoryPool& pool, const luc::shading_assemble_vector_node& node, Library* library) {
		return Pointer<const ShadingNode<Vectr3F>>(TripletNode<Vectr3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.x(), library),
				load(pool, node.y(), library),
				load(pool, node.z(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Vectr3F>> load(MemoryPool& pool, const luc::shading_convert_to_vector_node& node, Library* library) {
		if (node.color().present()) {
			return Pointer<const ShadingNode<Vectr3F>>(
				ConverterNode<Vectr3F,  Color3F, LUCIFER_SHADING_INPUT>::build(
					pool,
					load(pool, node.color().get(),  library)
				)
			);
		}
		if (node.point3().present()) {
			return Pointer<const ShadingNode<Vectr3F>>(
				ConverterNode<Vectr3F, Point3F, LUCIFER_SHADING_INPUT>::build(
					pool,
					load(pool, node.point3().get(), library)
				)
			);
		}
		if (node.normal().present()) {
			return Pointer<const ShadingNode<Vectr3F>>(
				ConverterNode<Vectr3F, Norml3F, LUCIFER_SHADING_INPUT>::build(
					pool,
					load(pool, node.normal().get(), library)
				)
			);
		}
		assert(false);
		return Pointer<const ShadingNode<Vectr3F>>();
	}

	__host__
	static Pointer<const ShadingNode<Vectr3F>> load(MemoryPool& pool, const luc::shading_transformed_vector_node& node, Library* library) {
		return Pointer<const ShadingNode<Vectr3F>>(
			Transform3DNode<Vectr3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.vector(), library),
				XMLGeometry::load(node.transformation())
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Vectr3F>> load(MemoryPool& pool, const luc::shading_if_else_vector_node& node, Library* library) {
		auto condition = load(pool, node.condition(), library);
		auto ifTrue = load(pool, node.then(), library);
		auto ifFalse = load(pool, node.else_(), library);
		return Pointer<const ShadingNode<Vectr3F>>(
			IfElseNode<Vectr3F, LUCIFER_SHADING_INPUT>::build(pool, condition, ifTrue, ifFalse)
		);
	}

	__host__
	static Pointer<const ShadingNode<Vectr3F>> load(MemoryPool& pool, const luc::shading_vector_node& node, Library* library) {
		Pointer<const ShadingNode<Vectr3F>> result;
		if (node.tangent().present()) {
			result = load(pool, node.tangent().get());
		}
		if (node.constant().present()) {
			result = load(pool, node.constant().get());
		}
		if (node.assemble().present()) {
			result = load(pool, node.assemble().get(), library);
		}
		if (node.convert().present()) {
			result = load(pool, node.convert().get(), library);
		}
		if (node.transformed().present()) {
			result = load(pool, node.transformed().get(), library);
		}
		if (node.if_().present()) {
			result = load(pool, node.if_().get(), library);
		}
		if (node.reference().present()) {
			result = library->shadingVectr3FNodeMap[node.reference().get().referenced_id()];
		}
		if (!result.isNull()) {
			if (node.id().present()) {
				library->shadingVectr3FNodeMap[node.id().get()] = result;
			}
		}
		return result;
	}

	/**************************************************************************
	 * shading point2 nodes
	 **************************************************************************/
	__host__
	static Pointer<const ShadingNode<Point2F>> load(MemoryPool& pool, const luc::shading_uv_node& node) {
		return Pointer<const ShadingNode<Point2F>>(UVNode::build(pool));
	}

	__host__
	static Pointer<const ShadingNode<Point2F>> load(MemoryPool& pool, const luc::shading_constant_point2_node& node) {
		return Pointer<const ShadingNode<Point2F>>(
			ConstantNode<Point2F, LUCIFER_SHADING_INPUT>::build(
				pool,
				Point2F(node.u(), node.v())
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Point2F>> load(MemoryPool& pool, const luc::shading_assemble_point2_node& node, Library* library) {
		return Pointer<const ShadingNode<Point2F>>(
			PairNode<Point2F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.x(), library),
				load(pool, node.y(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Point2F>> load(MemoryPool& pool, const luc::shading_transformed_point2_node& node, Library* library) {
		return Pointer<const ShadingNode<Point2F>>(
			Transform2DNode<Point2F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.uv(), library),
				XMLGeometry::load(node.transformation())
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Point2F>> load(MemoryPool& pool, const luc::shading_uv_mapped_point2_node& node, Library* library) {
		return Pointer<const ShadingNode<Point2F>>(
			UVMappingNode<LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.point3(), library),
				XMLMapping::load(pool, node.mapping())
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Point2F>> load(MemoryPool& pool, const luc::shading_if_else_point2_node& node, Library* library) {
		auto condition = load(pool, node.condition(), library);
		auto ifTrue = load(pool, node.then(), library);
		auto ifFalse = load(pool, node.else_(), library);
		return Pointer<const ShadingNode<Point2F>>(
			IfElseNode<Point2F, LUCIFER_SHADING_INPUT>::build(pool, condition, ifTrue, ifFalse)
		);
	}

	__host__
	static Pointer<const ShadingNode<Point2F>> load(MemoryPool& pool, const luc::shading_point2_node& node, Library* library) {
		Pointer<const ShadingNode<Point2F>> result;
		if (node.uv().present()) {
			result = load(pool, node.uv().get());
		}
		if (node.constant().present()) {
			result = load(pool, node.constant().get());
		}
		if (node.assemble().present()) {
			result = load(pool, node.assemble().get(), library);
		}
		if (node.transformed().present()) {
			result = load(pool, node.transformed().get(), library);
		}
		if (node.mapped().present()) {
			result = load(pool, node.mapped().get(), library);
		}
		if (node.if_().present()) {
			result = load(pool, node.if_().get(), library);
		}
		if (node.reference().present()) {
			result = library->shadingPoint2FNodeMap[node.reference().get().referenced_id()];
		}
		if (!result.isNull()) {
			if (node.id().present()) {
				library->shadingPoint2FNodeMap[node.id().get()] = result;
			}
		}
		return result;
	}

	/**************************************************************************
	 * shading color nodes
	 **************************************************************************/
	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_constant_color_node& node) {
		return Pointer<const ShadingNode<Color3F>>(
			ConstantNode<Color3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				Color3F(node.x(), node.y(), node.z())
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_assemble_color_node& node, Library* library) {
		return Pointer<const ShadingNode<Color3F>>(
			TripletNode<Color3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.x(), library),
				load(pool, node.y(), library),
				load(pool, node.z(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_convert_to_color_node& node, Library* library) {
		if (node.point3().present()) {
			return Pointer<const ShadingNode<Color3F>>(
				ConverterNode<Color3F, Point3F, LUCIFER_SHADING_INPUT>::build(
					pool,
					load(pool, node.point3().get(), library)
				)
			);
		}
		if (node.normal().present()) {
			return Pointer<const ShadingNode<Color3F>>(
				ConverterNode<Color3F, Norml3F, LUCIFER_SHADING_INPUT>::build(
					pool,
					load(pool, node.normal().get(), library)
				)
			);
		}
		if (node.vector().present()) {
			return Pointer<const ShadingNode<Color3F>>(
				ConverterNode<Color3F, Vectr3F, LUCIFER_SHADING_INPUT>::build(
					pool,
					load(pool, node.vector().get(), library)
				)
			);
		}
		assert(false);
		return Pointer<const ShadingNode<Color3F>>();
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_color_space_node& node, Library* library) {
		Pointer<const RGBColorSpace>  in;
		Pointer<const RGBColorSpace> out;
		if (node.input_color_space().present())   in = XMLRGBColorSpace::load(pool, node.input_color_space().get(),  library);
		if (node.output_color_space().present()) out = XMLRGBColorSpace::load(pool, node.output_color_space().get(), library);
		return Pointer<const ShadingNode<Color3F>>(
			ColorSpaceNode<LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.color(), library),
				in, out
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_mix_color_node& node, Library* library) {
		return Pointer<const ShadingNode<Color3F>>(
			MixNode<Color3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.node_a(), library),
				load(pool, node.node_b(), library),
				load(pool, node.weight(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_checker_color_2d_node& node, Library* library) {
		return Pointer<const ShadingNode<Color3F>>(
			Checker2DNode<Color3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.node_a(), library),
				load(pool, node.node_b(), library),
				load(pool, node.point(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_checker_color_3d_node& node, Library* library) {
		return Pointer<const ShadingNode<Color3F>>(
			Checker3DNode<Color3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.node_a(), library),
				load(pool, node.node_b(), library),
				load(pool, node.point(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_image_color_node& node, Library* library) {
		Pointer<const Filter2D> filter;
		if (node.filter().present()) filter = XMLFilter::load(pool, node.filter().get());
		return Pointer<const ShadingNode<Color3F>>(
			ImageNode<LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.pixel(), library),
				XMLImage::load(pool, node.image(), library),
				filter, node.wrap()
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_negation_color_node& node, Library* library) {
		return Pointer<const ShadingNode<Color3F>>(
			NegationNode<Color3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.argument(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_absolute_color_node& node, Library* library) {
		return Pointer<const ShadingNode<Color3F>>(
			AbsoluteNode<Color3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.argument(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_inversion_color_node& node, Library* library) {
		return Pointer<const ShadingNode<Color3F>>(
			InversionNode<Color3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.argument(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_addition_color_node& node, Library* library) {
		return Pointer<const ShadingNode<Color3F>>(
			AdditionNode<Color3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.lhs(), library),
				load(pool, node.rhs(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_subtraction_color_node& node, Library* library) {
		return Pointer<const ShadingNode<Color3F>>(
			SubtractionNode<Color3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.lhs(), library),
				load(pool, node.rhs(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_multiplication_color_node& node, Library* library) {
		return Pointer<const ShadingNode<Color3F>>(
			MultiplicationNode<Color3F, float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.lhs(), library),
				load(pool, node.rhs(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_division_color_node& node, Library* library) {
		return Pointer<const ShadingNode<Color3F>>(
			DivisionNode<Color3F, float, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.lhs(), library),
				load(pool, node.rhs(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_minimum_color_node& node, Library* library) {
		return Pointer<const ShadingNode<Color3F>>(
			MinimumNode<Color3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.lhs(), library),
				load(pool, node.rhs(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_maximum_color_node& node, Library* library) {
		return Pointer<const ShadingNode<Color3F>>(
			MaximumNode<Color3F, LUCIFER_SHADING_INPUT>::build(
				pool,
				load(pool, node.lhs(), library),
				load(pool, node.rhs(), library)
			)
		);
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_triplanar_color_node& node, Library* library) {
		auto n = load(pool, node.normal(), library);
		auto x = load(pool, node.x(), library);
		auto y = load(pool, node.y(), library);
		auto z = load(pool, node.z(), library);
		auto e = load(pool, node.e(), library);
		return Pointer<const ShadingNode<Color3F>>(
			TriplanarNode<Color3F, LUCIFER_SHADING_INPUT>::build(pool, n, x, y, z, e)
		);
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_if_else_color_node& node, Library* library) {
		auto condition = load(pool, node.condition(), library);
		auto ifTrue = load(pool, node.then(), library);
		auto ifFalse = load(pool, node.else_(), library);
		return Pointer<const ShadingNode<Color3F>>(
			IfElseNode<Color3F, LUCIFER_SHADING_INPUT>::build(pool, condition, ifTrue, ifFalse)
		);
	}

	__host__
	static Pointer<const ShadingNode<Color3F>> load(MemoryPool& pool, const luc::shading_color_node& node, Library* library) {
		Pointer<const ShadingNode<Color3F>> result;
		if (node.constant().present()) {
			result = load(pool, node.constant().get());
		}
		if (node.assemble().present()) {
			result = load(pool, node.assemble().get(), library);
		}
		if (node.convert().present()) {
			result = load(pool, node.convert().get(), library);
		}
		if (node.color_space().present()) {
			result = load(pool, node.color_space().get(), library);
		}
		if (node.mix().present()) {
			result = load(pool, node.mix().get(), library);
		}
		if (node.checker_2d().present()) {
			result = load(pool, node.checker_2d().get(), library);
		}
		if (node.checker_3d().present()) {
			result = load(pool, node.checker_3d().get(), library);
		}
		if (node.image().present()) {
			result = load(pool, node.image().get(), library);
		}
		if (node.neg().present()) {
			result = load(pool, node.neg().get(), library);
		}
		if (node.abs().present()) {
			result = load(pool, node.abs().get(), library);
		}
		if (node.inv().present()) {
			result = load(pool, node.inv().get(), library);
		}
		if (node.add().present()) {
			result = load(pool, node.add().get(), library);
		}
		if (node.sub().present()) {
			result = load(pool, node.sub().get(), library);
		}
		if (node.mul().present()) {
			result = load(pool, node.mul().get(), library);
		}
		if (node.div().present()) {
			result = load(pool, node.div().get(), library);
		}
		if (node.min().present()) {
			result = load(pool, node.min().get(), library);
		}
		if (node.max().present()) {
			result = load(pool, node.max().get(), library);
		}
		if (node.triplanar().present()) {
			result = load(pool, node.triplanar().get(), library);
		}
		if (node.if_().present()) {
			result = load(pool, node.if_().get(), library);
		}
		if (node.reference().present()) {
			result = library->shadingColorNodeMap[node.reference().get().referenced_id()];
		}
		if (!result.isNull()) {
			if (node.id().present()) {
				library->shadingColorNodeMap[node.id().get()] = result;
			}
		}
		return result;
	}

	/**************************************************************************
	 * shading nodes
	 **************************************************************************/
	static void load(MemoryPool& pool, const luc::shading_node& node, Library* library) {
		if (node.float_().present()) {
			load(pool, node.float_().get(), library);
		}
		if (node.color().present()) {
			load(pool, node.color().get(), library);
		}
		if (node.point3().present()) {
			load(pool, node.point3().get(), library);
		}
		if (node.vector().present()) {
			load(pool, node.vector().get(), library);
		}
		if (node.normal().present()) {
			load(pool, node.normal().get(), library);
		}
		if (node.point2().present()) {
			load(pool, node.point2().get(), library);
		}
	}
};

}

#endif /* XMLSHADINGNODE_CUH_ */

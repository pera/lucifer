#ifndef XMLPRIMITIVE_CUH_
#define XMLPRIMITIVE_CUH_

#include "Library.cuh"
#include "XMLAnimatedTransformation.cuh"
#include "XMLGeometry.cuh"
#include "XMLMaterial.cuh"
#include "XMLMesh.cuh"
#include "XMLShape.cuh"
#include "lucifer/primitive/BVHPrimitive.cuh"
#include "lucifer/math/geometry/GroupMode.cuh"
#include "lucifer/primitive/GeometricPrimitive.cuh"
#include "lucifer/primitive/PrimitiveSet.cuh"
#include "lucifer/primitive/STBVHPrimitive.cuh"
#include "lucifer/system/Environment.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/xml/generated/lucifer.hxx"

namespace lucifer {

class XMLPrimitive {
public:
	XMLPrimitive() = delete;

	__host__
	static Pointer<const Primitive> group(MemoryPool& pool, std::vector<Pointer<const GeometricPrimitive>>& primitives, GroupMode mode) {
		if (primitives.size() == 0) {
			return Pointer<const Primitive>();
		}
		if (primitives.size() == 1) {
			return Pointer<const Primitive>(primitives[0]);
		}
		if (mode == GroupMode::SET) {
			return Pointer<const Primitive>(
				PrimitiveSet::build(
					pool,
					PrimitiveSet::array_t(pool, primitives.size(), primitives.data())
				)
			);
		}
		if (mode == GroupMode::BVH) {
			return Pointer<const Primitive>(
				BVHPrimitive::build(
					pool,
					BVHPrimitive::array_t(pool, primitives.size(), primitives.data())
				)
			);
		}
		return Pointer<const Primitive>(
			STBVHPrimitive::build(
				pool,
				STBVHPrimitive::array_t(pool, primitives.size(), primitives.data())
			)
		);
	}

	__host__
	static void load(
		MemoryPool& pool,
		luc::geometric_primitive& xml,
		std::vector<Pointer<const GeometricPrimitive>>& plist,
		std::vector<Pointer<const GeometricPrimitive>>& llist,
		Library* library)
	{
		// load material and shape
		auto material = XMLMaterial::load(pool, xml.material(), library);
		require(!material.isNull());
		auto shape = XMLShape::load(pool, xml.shape(), library);
		require(!shape.isNull());
		// decompose shape if material is emissive
		std::vector<Pointer<const Shape>> shapes;
		if (material->isEmissive()) shape->decompose(pool, shapes, shape);
		else shapes.push_back(shape);
		// instance a primitive for each shape
		const bool histogram = xml.histogram().present();
		const short nu = histogram ? xml.histogram().get().nu() : 0;
		const short nv = histogram ? xml.histogram().get().nv() : 0;
		const short nl = histogram ? xml.histogram().get().nl() : 0;
		if (material->isEmissive())
			for (auto s : shapes)
				llist.push_back(
					Pointer<const GeometricPrimitive>(
						GeometricPrimitive::build(
							pool, s,
							material,
							histogram,
							nu, nv, nl,
							library->xyzColorSpace
						)
					)
				);
		else
			for (auto s : shapes)
				plist.push_back(
					Pointer<const GeometricPrimitive>(
						GeometricPrimitive::build(
							pool, s,
							material,
							histogram,
							nu, nv, nl,
							library->xyzColorSpace
						)
					)
				);
	}

	__host__
	static void load(
		MemoryPool& pool,
		luc::primitive_set& xml,
		std::vector<Pointer<const GeometricPrimitive>>& plist,
		std::vector<Pointer<const GeometricPrimitive>>& llist,
		Library* library)
	{
		for (auto& p : xml.primitive()) load(pool, p, plist, llist, library);
	}

	__host__
	static void load(
		MemoryPool& pool,
		luc::mesh_primitive& xml,
		std::vector<Pointer<const GeometricPrimitive>>& plist,
		std::vector<Pointer<const GeometricPrimitive>>& llist,
		Library* library)
	{
		// create primitives
		auto data = XMLMesh::load(pool, xml.data(), library);
		auto mode = GroupModeBuilder::buildFrom(xml.mode());
		auto meshVrtxData = Pointer<const MeshVrtxData>(MeshVrtxData::build(pool, data->vertexes));
		for (Index3::index_t i = 0; i < data->meshes.size(); i++) {
			Mesh& mesh = data->meshes[i];
			if (mesh.vrtx.size() > 0) {
				auto meshFaceData = Pointer<const MeshFaceData>(MeshFaceData::build(pool, meshVrtxData, mesh));
				std::vector<Pointer<const Shape>> triangles;
				buildTriangleMesh(pool, meshFaceData, triangles);
				auto material = library->material(data->materials[i]);

				std::vector<Pointer<const GeometricPrimitive>> tmpplist;
				std::vector<Pointer<const GeometricPrimitive>> tmpllist;
				if (material->isEmissive()) {
					for (auto& triangle : triangles)
						tmpllist.push_back(Pointer<const GeometricPrimitive>(GeometricPrimitive::build(pool, triangle, material)));
				} else {
					if (mode == GroupMode::SET)
						// plain geometry set
						tmpplist.push_back(
							Pointer<const GeometricPrimitive>(
								GeometricPrimitive::build(
									pool,
									Pointer<const Shape>(ShapeSet::build(pool, ShapeSet::array_t(pool, triangles.size(), triangles.data()))),
									material
								)
							)
						);
					else
						// geometry bvh
						tmpplist.push_back(
							Pointer<const GeometricPrimitive>(
								GeometricPrimitive::build(
									pool,
									Pointer<const Shape>(BVHShape::build(pool, BVHShape::array_t(pool, triangles.size(), triangles.data()))),
									material
								)
							)
						);
				}
				// add tmp lists to final list
				for (auto& p : tmpplist) plist.push_back(p);
				for (auto& l : tmpllist) llist.push_back(l);
			}
		}
	}

	__host__
	static void load(
		MemoryPool& pool,
		luc::transformed_primitive& xml,
		std::vector<Pointer<const GeometricPrimitive>>& plist,
		std::vector<Pointer<const GeometricPrimitive>>& llist,
		Library* library)
	{
		std::vector<Pointer<const GeometricPrimitive>> tmpplist;
		std::vector<Pointer<const GeometricPrimitive>> tmpllist;
		auto transformation = XMLGeometry::load(xml.transformation());
		load(pool, xml.primitive(), tmpplist, tmpllist, library);
		for (auto& p : tmpplist)
			plist.push_back(p->transform(pool, transformation).dynamicCast<const GeometricPrimitive>());
		for (auto& l : tmpllist)
			llist.push_back(l->transform(pool, transformation).dynamicCast<const GeometricPrimitive>());
	}

	__host__
	static void load(
		MemoryPool& pool,
		luc::animated_primitive& xml,
		std::vector<Pointer<const GeometricPrimitive>>& plist,
		std::vector<Pointer<const GeometricPrimitive>>& llist,
		Library* library)
	{
		std::vector<Pointer<const GeometricPrimitive>> tmpplist;
		std::vector<Pointer<const GeometricPrimitive>> tmpllist;
		auto transformation = XMLTemporalTransformation::load(pool, xml.animated_transformation());
		load(pool, xml.primitive(), tmpplist, tmpllist, library);
		for (auto& p : tmpplist)
			plist.push_back(p->transform(pool, transformation).dynamicCast<const GeometricPrimitive>());
		for (auto& l : tmpllist)
			llist.push_back(l->transform(pool, transformation).dynamicCast<const GeometricPrimitive>());
	}

	__host__
	static void load(
		MemoryPool& pool,
		luc::primitive& xml,
		std::vector<Pointer<const GeometricPrimitive>>& plist,
		std::vector<Pointer<const GeometricPrimitive>>& llist,
		Library* library)
	{
		if (xml.geometric().present()) load(pool, xml.geometric().get(), plist, llist, library);
		if (xml.set().present()) load(pool, xml.set().get(), plist, llist, library);
		if (xml.mesh().present()) load(pool, xml.mesh().get(), plist, llist, library);
		if (xml.transformed().present()) load(pool, xml.transformed().get(), plist, llist, library);
		if (xml.animated().present()) load(pool, xml.animated().get(), plist, llist, library);
		if (xml.reference().present()) {
			auto entry = library->primitiveMap.find(xml.reference().get().referenced_id());
			plist.insert(std::end(plist), std::begin(entry->second.first ), std::end(entry->second.first ));
			llist.insert(std::end(llist), std::begin(entry->second.second), std::end(entry->second.second));
		}
		if (xml.id().present()) {
			library->primitiveMap[xml.id().get()] =
					std::pair<std::vector<Pointer<const GeometricPrimitive>>, std::vector<Pointer<const GeometricPrimitive>>>(plist, llist);
		}
	}
};

}

#endif /* XMLPRIMITIVE_CUH_ */

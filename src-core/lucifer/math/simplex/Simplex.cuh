#ifndef SIMPLEX_CUH_
#define SIMPLEX_CUH_

namespace lucifer {

/*
 * Based on "LINEAR PROGRAMMING, A Concise Introcution"
 * by Thomas S. Ferguson
 */
class Simplex {
private:
	int r;
	int c;
	double* t;

private:
	static constexpr double EPS = 1E-8;

	__host__
	int infeasiblePivotCol() const {
		int k = 0;
		for (int i = 1; i <= r; i++) {
			if ((*this)[i][c+1] <= -EPS) {
				k = i;
				break;
			}
		}
		if (k == 0) {
			return 0; // procede to phase II
		}
		double minValue = -EPS;
		int pc = -2; // return code -2 means infeasible problem
		for (int j = 1; j <= c; j++) {
			if ((*this)[k][j] < minValue) {
				pc = j;
				minValue = (*this)[k][j];
			}
		}
		return pc;
	}

	__host__
	int infeasiblePivotRow(int j) const {
		int k = 0;
		for (int i = 1; i <= r; i++) {
			if ((*this)[i][c + 1] <= -EPS) {
				k = i;
				break;
			}
		}
		double pr = k;
		double minRatio = (*this)[k][c + 1] / (*this)[k][j];
		for (int i = 1; i <= r; i++) {
			if ((*this)[i][j] > +EPS && (*this)[i][c + 1] > +EPS) {
				double ratio = (*this)[i][c + 1] / (*this)[i][j];
				if (ratio < minRatio) {
					pr = i;
					minRatio = ratio;
				}
			}
		}
		return pr;
	}

	__host__
	int feasiblePivotCol(bool first) const {
		double minValue = first ? -EPS : +EPS;
		int pc = 0; // return code 0 means optimal solution
		for (int j = 1; j <= c; j++) {
			if ((*this)[r + 1][j] < minValue) {
				pc = j;
				minValue = (*this)[r + 1][j];
			}
		}
		return pc;
	}

	__host__
	int feasiblePivotRow(int j) {
		int pr = -1; // return code -1 means unbounded feasible
		double minRatio = +INFINITY;
		for (int i = 1; i <= r; i++) {
			if ((*this)[i][j] > +EPS) {
				double ratio = (*this)[i][c + 1] / (*this)[i][j];
				if (ratio < minRatio) {
					pr = i;
					minRatio = ratio;
				}
			}
		}
		return pr;
	}

	__host__
	void swap(int row, int col) {
		double temp = (*this)[row][0];
		(*this)[row][0] = (*this)[0][col];
		(*this)[0][col] = temp;
	}

	__host__
	void pivot(int row, int col) {
		for (int i = 1; i <= r + 1; i++) {
			for (int j = 1; j <= c + 1; j++) {
				if (i != row && j != col) {
					(*this)[i][j] -= (*this)[i][col] * (*this)[row][j] / (*this)[row][col];
				}
			}
		}
		for (int j = 1; j <= c + 1; j++) {
			if (j != col) {
				(*this)[row][j] /= +(*this)[row][col];
			}
		}
		for (int i = 1; i <= r + 1; i++) {
			if (i != row) {
				(*this)[i][col] /= -(*this)[row][col];
			}
		}
		(*this)[row][col] = 1. / (*this)[row][col];
		swap(row, col);
	}

public:
	__host__
	Simplex(const int rows, const int cols) :
	r(rows), c(cols), t(new double[(r + 2) * (c + 2)]) {
		memset(t, 0, (r + 2) * (c + 2) * sizeof(double));
		for (int i = 1; i <= r; i++) {
			(*this)[i][0] = -i;
		}
		for (int j = 1; j <= c; j++) {
			(*this)[0][j] = +j;
		}
	}

	__host__
	const double* operator[](int row) const {
		return t + row * (c + 2);
	}

	__host__
	double* operator[](int row) {
		return t + row * (c + 2);
	}

	__host__
	bool maximize() {
		// phase I
		while(true) {
			int col = infeasiblePivotCol();
			if (col == 0) {
				break;
			}
			if (col <  0) {
				return false;
			}
			int row = infeasiblePivotRow(col);
			if (row <= 0) {
				return false;
			}
			pivot(row, col);
		}
		// phase II
		while(true) {
			int col = feasiblePivotCol(true);
			if (col == 0) {
				return true;
			}
			int row = feasiblePivotRow(col);
			if (row <= 0) {
				return false;
			}
			pivot(row, col);
		}
	}

	template<typename T> __host__
	void maximumSolution(T* s_) {
		memset(s_, 0, c * sizeof(T));
		for (int i = 1; i <= r; i++) {
			if ((*this)[i][0] > 0.) {
				int idx = ((int)(*this)[i][0]) - 1;
				s_[idx] = (T)(*this)[i][c + 1];
			}
		}
	}

	__host__
	double maximumValue() {
		return (*this)[r + 1][c + 1];
	}

	__host__
	void print() {
		for (int i = 0; i <= r + 1; i++) {
			for (int j = 0; j <= c + 1; j++) {
				printf("%g\t", (*this)[i][j]);
			}
			printf("\n");
		}
	}

};

}

#endif /* SIMPLEX_CUH_ */

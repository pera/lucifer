#ifndef GROUPMODE_CUH_
#define GROUPMODE_CUH_

#include <string>

namespace lucifer {

enum class GroupMode {
	SET,
	BVH,
	STBVH
};

class GroupModeBuilder {
public:
	__host__
	GroupModeBuilder() = delete;

	static GroupMode buildFrom(const std::string& name) {
		if (name == "set") {
			return GroupMode::SET;
		}
		if (name == "bvh") {
			return GroupMode::BVH;
		}
		return GroupMode::STBVH;
	}
};

}

#endif /* GROUPMODE_CUH_ */

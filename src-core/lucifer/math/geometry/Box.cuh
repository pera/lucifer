#ifndef BOX_CUH_
#define BOX_CUH_

#include <cassert>
#include "Ray.cuh"

namespace lucifer {

class Box {
private:
	Point3F min_;
	Point3F max_;
	bool empty_;

public:
	using dim_t = uint8_t;

public:
	__host__ __device__
	Box() :
	min_(0.f, 0.f, 0.f), max_(0.f, 0.f, 0.f), empty_(true) {
	}

	__host__ __device__
	Box(const Point3F& p):
	min_(p), max_(p), empty_(false) {
	}

	__host__ __device__
	Box(const Point3F& a, const Point3F& b): empty_(false) {
		min_ = Point3F(min(a[0], b[0]), min(a[1], b[1]), min(a[2], b[2]));
		max_ = Point3F(max(a[0], b[0]), max(a[1], b[1]), max(a[2], b[2]));
	}

	__host__ __device__
	Box(const Point3R& a, const Point3R& b): empty_(false) {
		min_ = Point3F(min(a[0].lower, b[0].lower), min(a[1].lower, b[1].lower), min(a[2].lower, b[2].lower));
		max_ = Point3F(max(a[0].upper, b[0].upper), max(a[1].upper, b[1].upper), max(a[2].upper, b[2].upper));
	}

	__host__ __device__
	bool empty() const {
		return empty_;
	}

	__host__ __device__
	bool contains(const Point3F& p) const {
		return !empty_ and
				min_[0] <= p[0] and min_[1] <= p[1] and min_[2] <= p[2] and
				max_[0] >= p[0] and max_[1] >= p[1] and max_[2] >= p[2];
	}

	__host__ __device__
	const Point3F& lower() const {
		assert(!empty_);
		return min_;
	}

	__host__ __device__
	Point3F& lower() {
		assert(!empty_);
		return min_;
	}

	__host__ __device__
	const Point3F& upper() const {
		assert(!empty_);
		return max_;
	}

	__host__ __device__
	Point3F& upper() {
		assert(!empty_);
		return max_;
	}

	__host__ __device__
	void set(const Point3F& p) {
		min_ = max_ = p;
		empty_ = false;
	}

	__host__ __device__
	Vectr3F length() const {
		assert(!empty_);
		return max_ - min_;
	}

	__host__ __device__
	float length(dim_t axis) const {
		assert(!empty_);
		return max_[axis] - min_[axis];
	}

	__host__ __device__
	float maxLength() const {
		assert(!empty_);
		float maxLength = -1.f;
		for (dim_t i = 0; i < 3; ++i) {
			float length = max_[i] - min_[i];
			if (length > maxLength) {
				maxLength = length;
			}
		}
		return maxLength;
	}

	__host__ __device__
	dim_t maxLengthAxis() const {
		assert(!empty_);
		int8_t axis = -1;
		float maxLength = -1.f;
		for (int8_t i = 0; i < 3; ++i) {
			float length = max_[i] - min_[i];
			if (length > maxLength) {
				axis = i;
				maxLength = length;
			}
		}
		return axis;
	}

	__host__  __device__
	Point3F centroid() const {
		assert(!empty_);
		return Point3F(max_[0] + min_[0], max_[1] + min_[1], max_[2] + min_[2]) * .5f;
	}

	__host__ __device__
	float area() const {
		if (empty_) { return 0.f; }
		const Vectr3F l = length();
		return 2.f * ((l[0] * l[1]) + (l[1] * l[2]) + (l[2] * l[0]));
	}

	__host__ __device__
	float volume() const {
		if (empty_) { return 0.f; }
		const Vectr3F l = length();
		return l[0] * l[1] * l[2];
	}

	__host__ __device__
	Box operator+(const Point3F& p) const {
		if (empty_) { return Box(p); }
		const Point3F umin(min(min_[0], p[0]), min(min_[1], p[1]), min(min_[2], p[2]));
		const Point3F umax(max(max_[0], p[0]), max(max_[1], p[1]), max(max_[2], p[2]));
		return Box(umin, umax);
	}

	__host__ __device__
	Box& operator+=(const Point3F& p) {
		if (empty_) {
			min_ = max_ = p;
			empty_ = false;
		} else {
			min_ = Point3F(min(min_[0], p[0]), min(min_[1], p[1]), min(min_[2], p[2]));
			max_ = Point3F(max(max_[0], p[0]), max(max_[1], p[1]), max(max_[2], p[2]));
		}
		return *this;
	}

	__host__ __device__
	Box& operator+=(const Point3R& p) {
		operator+=(Point3F(p[0].lower, p[1].lower, p[2].lower));
		operator+=(Point3F(p[0].upper, p[1].upper, p[2].upper));
		return *this;
	}

	__host__ __device__
	Box operator+(const Box& that) const {
		if (empty_) { return that; }
		if (that.empty_) { return *this; }
		const Point3F umin(min(min_[0], that.min_[0]), min(min_[1], that.min_[1]), min(min_[2], that.min_[2]));
		const Point3F umax(max(max_[0], that.max_[0]), max(max_[1], that.max_[1]), max(max_[2], that.max_[2]));
		return Box(umin, umax);
	}

	__host__ __device__
	Box& operator+=(const Box& that) {
		if (!that.empty_) {
			if (empty_) {
				min_ = that.min_;
				max_ = that.max_;
				empty_ = false;
			} else {
				min_ = Point3F(min(min_[0], that.min_[0]), min(min_[1], that.min_[1]), min(min_[2], that.min_[2]));
				max_ = Point3F(max(max_[0], that.max_[0]), max(max_[1], that.max_[1]), max(max_[2], that.max_[2]));
			}
		}
		return *this;
	}

	__host__ __device__
	bool intersection(const Ray& ray) const {
		if (empty_) {
			return false;
		}
		float t0 = ray.r.lower;
		float t1 = ray.r.upper;
		for (dim_t i = 0; i < 3; ++i) {
		  float tNr = (min_[i] - ray.o[i]) / ray.d[i];
		  float tFr = (max_[i] - ray.o[i]) / ray.d[i];
		  if (tNr > tFr) {
			float temp = tNr;
			tNr = tFr;
			tFr = temp;
		  }
		  t0 = tNr > t0 ? tNr : t0;
		  t1 = tFr < t1 ? tFr : t1;
		  if (t0 > t1) {
			return false;
		  }
		}
		return true;
	}

	__host__ __device__
	Box intersection(const Box& that) const {
		if (empty_ or that.empty_) { return Box(); }
		const Point3F umin(max(min_[0], that.min_[0]), max(min_[1], that.min_[1]), max(min_[2], that.min_[2]));
		const Point3F umax(min(max_[0], that.max_[0]), min(max_[1], that.max_[1]), min(max_[2], that.max_[2]));
		if (umin[0] > umax[0] || umin[1] > umax[1] || umin[2] > umax[2]) { return Box(); }
		return Box(umin, umax);
	}

};

template<typename E> __host__ __device__
inline Box operator*(const Transform<E, 3>& t, const Box& b) {
	Box result(t * b.lower(), t * b.upper());
	result += t * Point3F(b.upper()[0], b.lower()[1], b.lower()[2]);
	result += t * Point3F(b.lower()[0], b.upper()[1], b.lower()[2]);
	result += t * Point3F(b.lower()[0], b.lower()[1], b.upper()[2]);
	result += t * Point3F(b.lower()[0], b.upper()[1], b.upper()[2]);
	result += t * Point3F(b.upper()[0], b.upper()[1], b.lower()[2]);
	result += t * Point3F(b.upper()[0], b.lower()[1], b.upper()[2]);
	return result;
}

template<typename E> __host__ __device__
inline Box operator*(const Transformation<E, 3>& t, const Box& b) {
	return t.direct() * b;
}

}

#endif /* BOX_CUH_ */

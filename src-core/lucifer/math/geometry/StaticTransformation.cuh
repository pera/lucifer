#ifndef STATICTRANSFORMATION_CUH_
#define STATICTRANSFORMATION_CUH_

#include "AnimatedTransformation.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class StaticTransformation: public AnimatedTransformation {
private:
	friend class TypedPool<StaticTransformation>;
	Transformation3F transformation;

	__host__ __device__
	static Transformation3F scalar_(const AnimatedTransformation* trans, const float t) {
		return static_cast<const StaticTransformation*>(trans)->transformation;
	}

	__host__ __device__
	static Transformation3R ranged_(const AnimatedTransformation* trans, const RangeF t) {
		return Transformation3R{static_cast<const StaticTransformation*>(trans)->transformation};
	}

	__host__
	StaticTransformation(
		const Transformation3F& transformation,
		const scalar_eval_t& scalar,
		const ranged_eval_t& ranged
	): AnimatedTransformation(scalar, ranged), transformation(transformation) {
	}

public:
	__host__
	static Pointer<StaticTransformation> build(MemoryPool& pool, const Transformation3F& transformation) {
		return Pointer<StaticTransformation>::make(
			pool, transformation,
			scalar_eval_t(virtualize(scalar_)),
			ranged_eval_t(virtualize(ranged_))
		);
	}

	__host__
	bool hasUnitScale(const float error = 1E-4f) const override {
		auto factor = scale(transformation);
		return abs(1.f - factor[0]) < error and abs(1.f - factor[1]) < error and abs(1.f - factor[2]) < error;
	}

	__host__
	bool hasUniformScale(const float error = 1E-4f) const override {
		auto factor = scale(transformation);
		return abs(factor[0] - factor[1]) < error and abs(factor[1] - factor[2]) < error;
	}

	__host__
	bool hasConstantScale(const float error = 1E-4f) const override {
		return true;
	}
};

}

#endif /* STATICTRANSFORMATION_CUH_ */

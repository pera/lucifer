#ifndef UVMAPPING_CUH_
#define UVMAPPING_CUH_

#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/memory/Virtual.cuh"

namespace lucifer {

class UVMapping: public Polymorphic {
public:
	using scalar_eval_t = Virtual<Point2F, const UVMapping*, const Point3F&>;
	using ranged_eval_t = Virtual<Point2R, const UVMapping*, const Point3R&>;

private:
	const scalar_eval_t scalar_;
	const ranged_eval_t ranged_;

public:
	__host__
	UVMapping(const scalar_eval_t& scalar_, const ranged_eval_t& ranged_):
	scalar_(scalar_), ranged_(ranged_) {
	}

	__host__ __device__
	Point2F operator()(const Point3F& p) const {
		return scalar_(this, p);
	}

	__host__ __device__
	Point2R operator()(const Point3R& p) const {
		return ranged_(this, p);
	}
};

}

#endif /* UVMAPPING_CUH_ */

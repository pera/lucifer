#ifndef ANIMATEDTRANSFORMATION_CUH_
#define ANIMATEDTRANSFORMATION_CUH_

#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/memory/Virtual.cuh"

namespace lucifer {

class AnimatedTransformation: public Polymorphic {
public:
	using scalar_eval_t = Virtual<Transformation3F, const AnimatedTransformation*, const  float>;
	using ranged_eval_t = Virtual<Transformation3R, const AnimatedTransformation*, const RangeF>;

private:
	const scalar_eval_t scalar_;
	const ranged_eval_t ranged_;

public:
	__host__
	AnimatedTransformation(
		const scalar_eval_t& scalar_,
		const ranged_eval_t& ranged_
	):
		scalar_(scalar_),
		ranged_(ranged_) {
	}

	__host__
	virtual bool hasUnitScale(const float error = 1E-4f) const  = 0;

	__host__
	virtual bool hasUniformScale(const float error = 1E-4f) const = 0;

	__host__
	virtual bool hasConstantScale(const float error = 1E-4f) const = 0;

	__host__ __device__
	Transformation3F atTime(const float  t) const {
		return scalar_(this, t);
	}
	__host__ __device__
	Transformation3R atTime(const RangeF t) const {
		return ranged_(this, t);
	}

	__host__
	virtual ~AnimatedTransformation() {
	}
};

}

#endif /* ANIMATEDTRANSFORMATION_CUH_ */

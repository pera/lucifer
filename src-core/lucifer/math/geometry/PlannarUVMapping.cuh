#ifndef PLANNARUVMAPPING_CUH_
#define PLANNARUVMAPPING_CUH_

#include <cassert>
#include "UVMapping.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class PlanarUVMapping: public UVMapping {
public:
	enum Axes {
		XY = 'x',
		YZ = 'y',
		ZX = 'z'
	};

private:
	const Axes axes;

	__host__ __device__
	static Point2F scalar_(const UVMapping* mapping, const Point3F& p) {
		switch (static_cast<const PlanarUVMapping*>(mapping)->axes) {
			case XY:
				return Point2F(p[0], p[1]);
			case YZ:
				return Point2F(p[1], p[2]);
			case ZX:
				return Point2F(p[2], p[0]);
			default:
				assert(false);
				return Point2F(0.f, 0.f);
		}
	}

	__host__ __device__
	static Point2R ranged_(const UVMapping* mapping, const Point3R& p) {
		switch (static_cast<const PlanarUVMapping*>(mapping)->axes) {
			case XY:
				return Point2R(p[0], p[1]);
			case YZ:
				return Point2R(p[1], p[2]);
			case ZX:
				return Point2R(p[2], p[0]);
			default:
				assert(false);
				return Point2R(0.f, 0.f);
		}
	}

	__host__
	PlanarUVMapping(const Axes axes, const scalar_eval_t& scalar, const ranged_eval_t& ranged):
	UVMapping(scalar, ranged), axes(axes) {
	}

public:
	friend class TypedPool<PlanarUVMapping>;

	__host__
	static Pointer<PlanarUVMapping> build(MemoryPool& pool, const Axes axes) {
		return Pointer<PlanarUVMapping>::make(
			pool, axes,
			scalar_eval_t(virtualize(scalar_)),
			ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* PLANNARUVMAPPING_CUH_ */

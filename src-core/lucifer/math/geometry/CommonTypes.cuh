#ifndef COMMONTYPES_CUH_
#define COMMONTYPES_CUH_

#include "lucifer/math/Affine.cuh"
#include "lucifer/math/Complex.cuh"
#include "lucifer/math/Matrix.cuh"
#include "lucifer/math/Quaternion.cuh"
#include "lucifer/math/Range.cuh"
#include "lucifer/math/Transformation.cuh"
#include "lucifer/math/Tuple.cuh"

namespace lucifer {

using RangeF = Range<float>;
using RangeB = Range< bool>;

using ComplexF = Complex< float>;
using ComplexR = Complex<RangeF>;

using QuaternionF = Quaternion< float>;
using QuaternionR = Quaternion<RangeF>;

using Point2F = Tuple< float, TType::POINT, 2>;
using Point2R = Tuple<RangeF, TType::POINT, 2>;

using Vectr2F = Tuple< float, TType::VECTR, 2>;
using Vectr2R = Tuple<RangeF, TType::VECTR, 2>;

using Point3F = Tuple< float, TType::POINT, 3>;
using Point3R = Tuple<RangeF, TType::POINT, 3>;

using Vectr3F = Tuple< float, TType::VECTR, 3>;
using Vectr3R = Tuple<RangeF, TType::VECTR, 3>;
using Vectr4F = Tuple< float, TType::VECTR, 4>;

using Norml3F = Tuple< float, TType::NORML, 3>;
using Norml3R = Tuple<RangeF, TType::NORML, 3>;

using Color3F = Tuple< float, TType::COLOR, 3>;
using Color3R = Tuple<RangeF, TType::COLOR, 3>;

using Matrix3x3F = Matrix< float, 3, 3>;
using Matrix3x3R = Matrix<RangeF, 3, 3>;

using Matrix4x4F = Matrix< float, 4, 4>;
using Matrix4x4R = Matrix<RangeF, 4, 4>;

using Affine2F = Affine< float, 2>;
using Affine2R = Affine<RangeF, 2>;

using Affine3F = Affine< float, 3>;
using Affine3R = Affine<RangeF, 3>;

using Transform3F = Transform< float, 3>;
using Transform3R = Transform<RangeF, 3>;

using Transformation3F = Transformation< float, 3>;
using Transformation3R = Transformation<RangeF, 3>;
}

#endif /* COMMONTYPES_CUH_ */

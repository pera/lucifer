#ifndef GEOMETRY_CUH_
#define GEOMETRY_CUH_

#include "lucifer/math/geometry/CommonTypes.cuh"

namespace lucifer {

class Geometry {
public:
	Point2F uv;
	Point3F gp;
	Vectr3F gu;
	Norml3F gn;
	Vectr3F su;
	Norml3F sn;

public:
	__host__ __device__
	Geometry():
	uv(0.f, 0.f), gp(0.f, 0.f, 0.f), gu(0.f, 0.f, 0.f), gn(0.f, 0.f, 0.f), su(0.f, 0.f, 0.f), sn(0.f, 0.f, 0.f) {
	}

	__host__ __device__
	Geometry(const Point3F& gp, const Vectr3F& gu, const Norml3F& gn):
	uv(0.f, 0.f), gp(gp), gu(gu), gn(gn), su(gu), sn(gn) {
	}

	__host__ __device__
	Geometry(const Point3F& gp, const Vectr3F& gu, const Norml3F& gn, const Vectr3F& su, const Norml3F& sn):
	uv(0.f, 0.f), gp(gp), gu(gu), gn(gn), su(su), sn(sn) {
	}

	__host__ __device__
	Geometry(const Point3F& gp, const Vectr3F& gu, const Norml3F& gn, const Vectr3F& su, const Norml3F& sn, const Point2F& uv):
	uv(uv), gp(gp), gu(gu), gn(gn), su(su), sn(sn) {
	}

	__host__ __device__
	void apply(const Transform3F& t) {
		gp =  t * gp;
		gu = (t * gu).normalize();
		gn = (t * gn).normalize();
		su = (t * su).normalize();
		sn = (t * sn).normalize();
	}

	__host__ __device__
	void apply(const Transformation3F& t) {
		apply(t.direct());
	}
};

__host__ __device__
inline Geometry operator*(const Transform3F& t, const Geometry& g) {
	return Geometry(
		 t * g.gp,
		(t * g.gu).normalize(),
		(t * g.gn).normalize(),
		(t * g.su).normalize(),
		(t * g.sn).normalize(),
		     g.uv
	);
}

__host__ __device__
inline Geometry operator*(const Transformation3F& t, const Geometry& g) {
	return operator*(t.direct(), g);
}

}

#endif /* GEOMETRY_CUH_ */

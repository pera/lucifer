#ifndef KEYFRAMETRANSFORMATION_CUH_
#define KEYFRAMETRANSFORMATION_CUH_

#include <cassert>
#include <cstdint>
#include "AnimatedTransformation.cuh"
#include "lucifer/system/Environment.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/math/Decomposition.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class KeyFrameTransformation: public AnimatedTransformation {
private:
	friend class TypedPool<KeyFrameTransformation>;
	using QuaterF = Quaternion<float>;
	using Matrx3F = Matrix3x3F;

public:
	class KeyFrame {
	public:
		friend class TypedPool<KeyFrame>;
		float time;
		Decomposition<float> dcmp;

	public:
		__host__
		KeyFrame(): time{0.f}, dcmp{Vectr3F{0.f, 0.f, 0.f}, QuaterF{1.f}, Matrx3F::identity()} {
		}

		__host__
		KeyFrame(float time, const Transformation3F& transformation):
		time(time), dcmp(transformation.direct().dir) {
		}
	};

public:
	using index_t = uint8_t;
	using array_t = Array<KeyFrame, index_t>;

private:
	const array_t keyFrames;

private:
	__host__ __device__
	Affine3F at(const float time) const {
		if (time <= keyFrames[0].time) {
			return keyFrames[0].dcmp.recompose();
		}
		auto n = keyFrames.size();
		if (time >= keyFrames[n - 1].time) {
			return keyFrames[n - 1].dcmp.recompose();
		}
		index_t k = 1;
		do {
			if (k == n  or time <= keyFrames[k].time) {
				float w = (time - keyFrames[k - 1].time) / (keyFrames[k].time - keyFrames[k - 1].time);
				return interpolate(keyFrames[k - 1].dcmp, keyFrames[k].dcmp, w).recompose();
			}
			++k;
		} while (true);
	}

	__host__ __device__
	static void combine(Affine3R& a, const Affine3F& b) {
		for (uint8_t r = 0; r < Affine3R::R; ++r) {
			for (uint8_t c = 0; c < Affine3R::C; ++c) {
				a[r][c] = hull(a[r][c], b[r][c]);
			}
		}
	}

	__host__ __device__
	static Transformation3F scalar_(const AnimatedTransformation* trans, const float t) {
		auto this_ = static_cast<const KeyFrameTransformation*>(trans);
		return Transformation3F(this_->at(t));
	}

	__host__ __device__
	static Transformation3R ranged_(const AnimatedTransformation* trans, const RangeF t) {
		auto this_ = static_cast<const KeyFrameTransformation*>(trans);
		auto matrix = Affine3R{this_->at(t.lower)};
		combine(matrix, this_->at(t.upper));
		for (index_t k = 0; k < this_->keyFrames.size(); ++k) {
			if (t.contains(this_->keyFrames[k].time)) {
				combine(matrix, this_->at(this_->keyFrames[k].time));
			}
		}
		return Transformation3R(matrix);
	}

	__host__
	KeyFrameTransformation(
		const array_t& keyFrames,
		const scalar_eval_t& scalar,
		const ranged_eval_t& ranged
	): AnimatedTransformation(scalar, ranged), keyFrames(keyFrames) {
		assert(keyFrames.size() > 0);
		for (index_t k = 1; k < keyFrames.size(); k++) {
			require(keyFrames[k - 1].time < keyFrames[k].time);
		}
	}

public:
	__host__
	static Pointer<KeyFrameTransformation> build(MemoryPool& pool, const array_t& keyFrames) {
		return Pointer<KeyFrameTransformation>::make(
			pool, keyFrames,
			scalar_eval_t(virtualize(scalar_)),
			ranged_eval_t(virtualize(ranged_))
		);
	}

	__host__
	bool hasUnitScale(const float error = 1E-4f) const override {
		for (auto& frame : keyFrames) {
			auto factor = frame.dcmp.scaleSquared();
			if (abs(1.f - factor[0]) > error or abs(1.f - factor[1]) > error or abs(1.f - factor[2]) > error) {
				return false;
			}
		}
		return true;
	}

	__host__
	bool hasUniformScale(const float error = 1E-4f) const override {
		for (auto& frame : keyFrames) {
			auto factor = frame.dcmp.scaleSquared();
			if (abs(factor[0] - factor[1]) > error or abs(factor[1] - factor[2]) > error) {
				return false;
			}
		}
		return true;
	}

	__host__
	bool hasConstantScale(const float error = 1E-4f) const override {
		Vectr3F min_(+INFINITY, +INFINITY, +INFINITY);
		Vectr3F max_(-INFINITY, -INFINITY, -INFINITY);
		for (auto& frame : keyFrames) {
			auto factor = frame.dcmp.scaleSquared();
			min_[0] = min(min_[0], factor[0]); min_[1] = min(min_[1], factor[1]); min_[2] = min(min_[2], factor[2]);
			max_[0] = max(max_[0], factor[0]); max_[1] = max(max_[1], factor[1]); max_[2] = max(max_[2], factor[2]);
		}
		return abs(max_[0] - min_[0]) <= error and abs(max_[1] - min_[1]) <= error and abs(max_[2] - min_[2]) <= error;
	}
};

}

#endif /* KEYFRAMETRANSFORMATION_CUH_ */

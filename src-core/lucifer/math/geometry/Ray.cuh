#ifndef RAY_CUH_
#define RAY_CUH_

#include "lucifer/math/geometry/CommonTypes.cuh"

namespace lucifer {

class Ray {
public:
	Point3F o;
	Vectr3F d;
	RangeF r;
	float l;
	float t;

public:
	__host__ __device__
	Ray():
	o(0.f, 0.f, 0.f), d(0.f, 0.f, 0.f), r(0.f, + INFINITY), l(0.f), t(0.f) {
	}

	__host__ __device__
	Ray(const Point3F& origin, const Vectr3F& direction, const RangeF& range, const float l, const float t):
	o(origin), d(direction), r(range), l(l), t(t) {
	}

	__host__ __device__
	Point3F operator()(const float distance)  const {
		return o + d * distance;
	}

	__host__ __device__
	Point3R operator()(const RangeF& distance) const {
		const Point3F pl = o + d * distance.lower;
		const Point3F pu = o + d * distance.upper;
		return Point3R(RangeF(pl[0], pu[0]), RangeF(pl[1], pu[1]), RangeF(pl[2], pu[2]));
	}

	__host__ __device__
	void apply(const Transform3F& transform) {
		o = transform * o;
		d = transform * d;
	}

	__host__ __device__
	void apply(const Transformation3F& transformation) {
		apply(transformation.direct());
	}

};

__host__ __device__
inline Ray operator*(const Transform3F& t, const Ray& ray) {
	return Ray(t * ray.o, t * ray.d, ray.r, ray.l, ray.t);
}

__host__ __device__
inline Ray operator*(const Transformation3F& t, const Ray& ray) {
	return operator*(t.direct(), ray);
}

}

#endif /* RAY_CUH_ */

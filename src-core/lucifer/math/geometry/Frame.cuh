#ifndef FRAME_CUH_
#define FRAME_CUH_

#include "lucifer/math/Util.cuh"
#include "CommonTypes.cuh"

namespace lucifer {

class Frame {

public:
	Vectr3F u;
	Vectr3F v;
	Vectr3F w;

	__host__ __device__
	Frame(const Vectr3F& u, const Vectr3F& v, const Vectr3F& w): u{u}, v{v}, w{w} {
	}

	__host__ __device__
	Frame(const Norml3F& n) {
		w = Vectr3F{n};
		orthonormalBasis(w, &u, &v);
	}

	__host__ __device__
	Vectr3F toLocal(const Vectr3F& x) const {
		return Vectr3F {dot(x, u), dot(x, v), dot(x, w)};
	}

	__host__ __device__
	Vectr3F toWorld(const Vectr3F& x) const {
		return Vectr3F {
		    (u[0] * x[0]) + (v[0] * x[1]) + (w[0] * x[2]),
		    (u[1] * x[0]) + (v[1] * x[1]) + (w[1] * x[2]),
		    (u[2] * x[0]) + (v[2] * x[1]) + (w[2] * x[2])
		};
	}
};

}

#endif /* FRAME_CUH_ */

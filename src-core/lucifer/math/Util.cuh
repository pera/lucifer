#ifndef UTIL_CUH_
#define UTIL_CUH_

#include "lucifer/math/Math.cuh"
#include "lucifer/math/function/Function.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"

namespace lucifer {

enum class Smooth {
	NONE,
	LINEAR,
	CUBIC,
	QUINTIC
};

__host__
inline Smooth interpolationMode(const char* mode) {
	if (strcmp(mode, "linear") == 0) {
		return Smooth::LINEAR;
	}
	if (strcmp(mode, "cubic") == 0) {
		return Smooth::CUBIC;
	}
	if (strcmp(mode, "quintic") == 0) {
		return Smooth::QUINTIC;
	}
	return Smooth::NONE;
}

template<typename T> __host__ __device__
T smooth3(const T t) {
	return t * t * (3 - 2 * t);
}

template<typename T> __host__ __device__
T smooth5(const T t) {
	return t * t * t * (t * (t * 6 - 15) + 10);
}

template<typename T> __host__ __device__
void smooth2D(T& dx, T& dy, const Smooth smooth) {
	switch (smooth) {
		case Smooth::CUBIC:
			dx = smooth3(dx);
			dy = smooth3(dy);
			break;
		case Smooth::QUINTIC:
			dx = smooth5(dx);
			dy = smooth5(dy);
			break;
		default:
			break;
	}
}

template<typename T> __host__ __device__
void smooth3D(T& dx, T& dy, T& dz, const Smooth smooth) {
	switch (smooth) {
		case Smooth::CUBIC:
			dx = smooth3(dx);
			dy = smooth3(dy);
			dz = smooth3(dz);
			break;
		case Smooth::QUINTIC:
			dx = smooth5(dx);
			dy = smooth5(dy);
			dz = smooth5(dz);
			break;
		default:
			break;
	}
}

__host__ __device__
inline bool isValid(const float val) {
	return !isnan(val) && !isinf(val) && val > 0.f;
}

__host__ __device__
inline bool isInvalid(const float val) {
	return isnan(val) || isinf(val) || val <= 0.f;
}

template<typename T> __host__ __device__
T toRadians(const T degrees) {
	return degrees * PI<T>() / 180;
}

__host__  __device__
inline bool solveQuadratic(const float a, const float b, const float c, float &x0, float &x1) {
	const float d = b * b - 4 * a * c;
	if (d < 0.f) return false;
	else if (d == 0.f) {
		x0 = x1 = -0.5f * b / a;
		return true;
	}
	float q = (b > 0.f) ? -0.5f * (b + sqrt(d)) : -0.5f * (b - sqrt(d));
	x0 = q / a;
	x1 = c / q;
	if (x0 > x1) interchange(x0, x1);
	return true;
}

__host__ __device__
inline float integrate(const Function1D& f, const Function1D& g, const float& lower, const float& upper, size_t n) {
	const float step = (upper - lower) / (n - 1);
	float integral = 0.f;
	for (size_t i = 0; i < n; i++) {
		const float x = lower + i * step;
		integral += f(x) * g(x);
	}
	return integral * (upper - lower) / n;
}

template<typename A, typename B> __host__ __device__
float absDot(const A& a, const B& b) {
	return abs(dot(a, b));
}

__host__ __device__
inline float toSolidAnglePdf(const Point3F& from, const Point3F& sample, const Norml3F& normal, const float areaPdf) {
	const auto w = from - sample;
	const auto distanceE2 = w.lengthSquared();
	const auto solidAnglePdf = areaPdf * distanceE2 * sqrt(distanceE2) / abs(dot(normal, w));
    return isinf(solidAnglePdf) || isnan(solidAnglePdf)? 0.f : solidAnglePdf;
}

__host__ __device__
inline float toAreaPdf(const Point3F& from, const Point3F& sample, const Norml3F& normal, const float solidAnglePdf) {
	const auto w = from - sample;
	const auto distanceE2 = w.lengthSquared();
	const auto areaPdf = solidAnglePdf * abs(dot(normal, w)) / (distanceE2 * sqrt(distanceE2));
    return isinf(areaPdf) || isnan(areaPdf)? 0.f : areaPdf;
}

template<typename T> __host__ __device__
inline float cosTheta(const T& w) {
	return w[2];
}

template<typename T> __host__ __device__
inline float cosE2Theta(const T& w) {
	return w[2] * w[2];
}

template<typename T> __host__ __device__
inline float absCosTheta(const T& w) {
	return abs(w[2]);
}

template<typename T> __host__ __device__
inline float sinE2Theta(const T& w) {
	return max(0.f, 1.f - cosE2Theta(w));
}

template<typename T> __host__ __device__
inline float sinTheta(const T& w) {
	return sqrtf(sinE2Theta(w));
}

template<typename T> __host__ __device__
inline float tanTheta(const T& w) {
	return sinTheta(w) / cosTheta(w);
}

template<typename T> __host__ __device__
inline float tanE2Theta(const T& w) {
	return sinE2Theta(w) / cosE2Theta(w);
}

template<typename T> __host__ __device__
inline float cosPhi(const T& w) {
	const float sin_theta = sinTheta(w);
	return (sin_theta == 0.f) ? 1.f : clamp(w[0] / sin_theta, -1.f, 1.f);
}

template<typename T> __host__ __device__
inline float cosE2Phi(const T& w) {
	const float cos_phi = cosPhi(w);
	return cos_phi * cos_phi;
}

template<typename T> __host__ __device__
inline float sinPhi(const T& w) {
	const float sin_theta = sinTheta(w);
	return (sin_theta == 0.f) ? 0.f : clamp(w[1] / sin_theta, -1.f, 1.f);
}

template<typename T> __host__ __device__
inline float sinE2Phi(const T& w) {
	const float sin_phi = sinPhi(w);
	return sin_phi * sin_phi;
}

template<typename T1, typename T2> __host__ __device__
inline float cosDeltaPhi(const T1& w1, const T2& w2) {
	return clamp((w1[0] * w2[0] + w1[1] * w2[1]) / sqrtf((w1[0] * w1[0] + w1[1] * w1[1]) * (w2[0] * w2[0] + w2[1] * w2[1])), -1.f, 1.f);
}

template<typename A, typename B, typename R> __host__ __device__
R& cross(const A& a, const B& b, R& r) {
	assert(&r != &a);
	assert(&r != &b);
    r[0] = (a[1] * b[2]) - (a[2] * b[1]);
    r[1] = (a[2] * b[0]) - (a[0] * b[2]);
    r[2] = (a[0] * b[1]) - (a[1] * b[0]);
	return r;
}

template<typename A, typename B, typename R> __host__ __device__
R cross(const A& a, const B& b) {
	return R(
		(a[1] * b[2]) - (a[2] * b[1]),
		(a[2] * b[0]) - (a[0] * b[2]),
		(a[0] * b[1]) - (a[1] * b[0])
	);
}

__host__ __device__ inline
void toShadingCoordinates(const Vectr3F& g_u, const Vectr3F& g_v, const Norml3F& g_n, const Vectr3F& w, Vectr3F& l) {
	assert(&l != &g_u);
	assert(&l != &g_v);
	assert(&l != &w);
	l[0] = dot(w, g_u);
	l[1] = dot(w, g_v);
	l[2] = dot(w, g_n);
}

__host__ __device__ inline
void toWorldCoordinates(const Vectr3F& g_u, const Vectr3F& g_v, const Norml3F& g_n, const Vectr3F& l, Vectr3F& w) {
	assert(&w != &g_u);
	assert(&w != &g_v);
	assert(&w != &l);
    w[0] = (g_u[0] * l[0]) + (g_v[0] * l[1]) + (g_n[0] * l[2]);
    w[1] = (g_u[1] * l[0]) + (g_v[1] * l[1]) + (g_n[1] * l[2]);
    w[2] = (g_u[2] * l[0]) + (g_v[2] * l[1]) + (g_n[2] * l[2]);
}

template<typename V> __host__ __device__
void orthonormalBasis(const V& v1, V* v2, V* v3	) {
	if (abs(v1[0]) > abs(v1[1])) {
		*v2 = V(-v1[2], 0.f, v1[0]) / sqrt(v1[0] * v1[0] + v1[2] * v1[2]);
	} else {
		*v2 = V(0.f, v1[2], -v1[1]) / sqrt(v1[1] * v1[1] + v1[2] * v1[2]);
	}
	*v3 = cross<V, V, V>(v1, *v2);
}

template<typename T> __host__ __device__
T sphericalDirection(float sinTheta, float cosTheta, float phi) {
    return T(sinTheta * cos(phi), sinTheta * sin(phi), cosTheta);
}

template<typename T> __host__ __device__
T sphericalDirection(float sinTheta, float cosTheta, float phi, const T& x, const T& y, const T& z) {
    return x * (sinTheta * cos(phi)) + y * (sinTheta * sin(phi)) + z * cosTheta;
}

template<typename A, typename B> __host__ __device__
inline bool sameHemisphere(const A& a, const B& b) {
	return a[2] * b[2] >= 0.f;
}

template<typename N, typename A, typename B> __host__ __device__
inline bool sameHemisphere(const N& n, const A& a, const B& b) {
	return dot(n, a) * dot(n, b) >= 0.f;
}

template<typename T> __host__ __device__
T reflect(const T &w, const T &n) {
    return n * (2.f * dot(w, n)) - w;
}

template<typename V> __host__ __device__
bool refract(const V& wi, const float eta, V* wo) {
	const float cosThetaI = wi[2];
	const float discriminant = 1.f - eta * eta * (1.f - cosThetaI * cosThetaI);
	// total internal reflection
	if (discriminant < 0.f) {
		return false;
	}
	// refraction
	*wo = -wi * eta;
	if (cosThetaI >= 0.f) {
		(*wo)[2] += eta * +cosThetaI - sqrt(discriminant);
	} else {
		(*wo)[2] -= eta * -cosThetaI - sqrt(discriminant);
	}
	return true;
}

template<typename V> __host__ __device__
bool refract(const V& wi, const V& n, float eta, V* wt) {
    // Snell's law
    const float  cosThetaI = dot(n, wi);
    const float sin2ThetaI = max(0.f, 1.f - cosThetaI * cosThetaI);
    const float sin2ThetaT = eta * eta * sin2ThetaI;
	// total internal reflection
    if (sin2ThetaT >= 1.f) {
    	return false;
    }
    // refraction
    const float cosThetaT = sqrt(1.f - sin2ThetaT);
    *wt = (-wi) * eta + n * (eta * cosThetaI - cosThetaT);
    return true;
}

template<typename V> __host__ __device__
auto triangleArea(const V& a, const V& b, const V& c) {
	const auto sa = (b - a).length();
	const auto sb = (c - b).length();
	const auto sc = (a - c).length();
	const auto s  = (sa + sb + sc) / 2;
	return sqrt(max(0, s * (s - sa) * (s - sb) * (s - sc)));
}

template<typename T> __host__ __device__
auto balanceHeuristic(const T pdf1, const T pdf2) {
	return pdf1 / (pdf1 + pdf2);
}

template<typename T> __host__ __device__
auto powerHeuristic(const T pdf1, const T pdf2) {
	const auto pdf1E2 = pdf1 * pdf1;
	return pdf1E2 / (pdf1E2 + pdf2 * pdf2);
}

}

#endif /* UTIL_CUH_ */

#ifndef SPHERICALGEOMETRY_CUH_
#define SPHERICALGEOMETRY_CUH_

#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/Point3.cuh"
#include "lucifer/math/geometry/Vectr3.cuh"

namespace lucifer {

//constexpr float SOLID_ANGLE_MIN = 0.12990381f; // 2% of max solid angle
constexpr float SOLID_ANGLE_MIN = 0.f;

/*!
 * \brief
 * Canonical spherical projection.
 *
 * Projects the the given point `x` onto the surface the unit sphere centered
 * at `c`. The resulting point is given in a reference frame where projection
 * sphere is centered at the origin.
 *
 * \param[in] c The center of the projection sphere.
 * \param[in] x The point to be projected onto the unit sphere.
 *
 * \return A unit vector from `c` in the direction of `x`.
 */
__host__ __device__
inline Vectr3F sphericalProjection(const Point3F& c, const Point3F& x) {
	return (x - c).normalize();
}

/*!
 * \brief
 * Computes the area of spherical triangle given it's vertexes.
 *
 * \param[in] A The 1st vertex, length must equal 1.f.
 * \param[in] B The 2nd vertex, length must equal 1.f.
 * \param[in] C The 3rd vertex, length must equal 1.f.
 * \param[out] theta_a The internal angle at the first vertex in radians.
 *
 * \return The spherical triangle area.
 */
__host__ __device__
inline bool sphericalArea(const Vectr3F& A, const Vectr3F& B, const Vectr3F& C, float* area, float* theta_a) {
	// compute sine and cosine of triangle sides
	const float cosa = B.dot(C);
	const float cosb = C.dot(A);
	const float cosc = A.dot(B);
	const float sina = sqrtf(fmax(0.f, 1.f - cosa * cosa));
	const float sinb = sqrtf(fmax(0.f, 1.f - cosb * cosb));
	const float sinc = sqrtf(fmax(0.f, 1.f - cosc * cosc));
	// validade inner angles
	*theta_a = acosf(clamp((cosa - cosb * cosc) / (sinb * sinc), -1.f, +1.f));
	if (isInvalid(*theta_a)) return false;
	const float theta_b = acosf(clamp((cosb - cosc * cosa) / (sinc * sina), -1.f, +1.f));
	if (isInvalid( theta_b)) return false;
	const float theta_c = acosf(clamp((cosc - cosa * cosb) / (sina * sinb), -1.f, +1.f));
	if (isInvalid( theta_c)) return false;
	// compute area from inner angles
	*area = fmax( 0.f, *theta_a + theta_b + theta_c - PI<float>());
	return isValid(*area) and *area > SOLID_ANGLE_MIN;
//	const auto A_ = B.cross(C).normalize();
//	const auto B_ = C.cross(A).normalize();
//	const auto C_ = A.cross(B).normalize();
//	*theta_a = acosf(clamp(dot(B_, C_), -1.f, +1.f));
//	return fmax(0.f, *theta_a + acosf(clamp(dot(C_, A_), -1.f, +1.f)) + acosf(clamp(dot(A_, B_), -1.f, +1.f)) - PI<float>());
}

__host__ __device__
inline bool sphericalArea(const Point3F& O, const Point3F& A_, const Point3F& B_, const Point3F& C_, float* area) {
	// project vertexes onto sphere centered at O
	const Vectr3F A = sphericalProjection(O, A_);
	if (hasnan(A) or hasinf(A)) return false;
	const Vectr3F B = sphericalProjection(O, B_);
	if (hasnan(B) or hasinf(B)) return false;
	const Vectr3F C = sphericalProjection(O, C_);
	if (hasnan(C) or hasinf(C)) return false;
//	// compute planes normal vectors
//	const auto nA = B.cross(C).normalize();
//	const auto nB = C.cross(A).normalize();
//	const auto nC = A.cross(B).normalize();
//	// compute spherical triangle area from angles
//	*area = fmax(
//		0.f,
//		acosf(clamp(dot(nB, nC), -1.f, +1.f)) +
//		acosf(clamp(dot(nC, nA), -1.f, +1.f)) +
//		acosf(clamp(dot(nA, nB), -1.f, +1.f)) -
//		PI<float>()
//	);
	// compute sine and cosine of triangle sides
	const float cosa = B.dot(C);
	const float cosb = C.dot(A);
	const float cosc = A.dot(B);
	const float sina = sqrtf(fmax(0.f, 1.f - cosa * cosa));
	const float sinb = sqrtf(fmax(0.f, 1.f - cosb * cosb));
	const float sinc = sqrtf(fmax(0.f, 1.f - cosc * cosc));
	// validade inner angles
	const float theta_a = acosf(clamp((cosa - cosb * cosc) / (sinb * sinc), -1.f, +1.f));
	if (isInvalid(theta_a)) return false;
	const float theta_b = acosf(clamp((cosb - cosc * cosa) / (sinc * sina), -1.f, +1.f));
	if (isInvalid(theta_b)) return false;
	const float theta_c = acosf(clamp((cosc - cosa * cosb) / (sina * sinb), -1.f, +1.f));
	if (isInvalid(theta_c)) return false;
	// compute spherical triangle area from inner angles
	*area = fmax( 0.f, theta_a + theta_b + theta_c - PI<float>());
	return isValid(*area) and *area > SOLID_ANGLE_MIN;
}

/*!
 * \brief
 * Auxiliary function defined by James Arvo for
 * "Uniformly samples a point on the surface of a spherical triangle".
 */
template<typename X, typename Y> __host__ __device__
inline Vectr3F aux(const X& x, const Y& y) {
	return (x - y * dot(x, y)).normalize();
}

__host__ __device__
inline bool sphericalSample(const Point3F& O, const Point3F& A_, const Point3F& B_, const Point3F& C_, const float rnd[2], Vectr3F* S, float* pdf) {
	// project vertexes onto sphere centered at O
	const Vectr3F A = sphericalProjection(O, A_);
	if (hasnan(A) or hasinf(A)) return false;
	const Vectr3F B = sphericalProjection(O, B_);
	if (hasnan(B) or hasinf(B)) return false;
	const Vectr3F C = sphericalProjection(O, C_);
	if (hasnan(C) or hasinf(C)) return false;
	// compute spherical area and angle at first vertex
	float area, theta_a;
	if (!sphericalArea(A, B, C, &area, &theta_a)) return false;
	*pdf = 1.f / area;
	// sample a point over the spherical triangle surface
	const float s = sinf(area * rnd[0] - theta_a);
	const float t = sqrtf(fmax(0.f, 1.f - s * s));
	const float sin_theta_a = sinf(theta_a);
	const float cos_theta_a = sqrtf(fmax(0.f, 1.f - sin_theta_a * sin_theta_a));
	const float u = t - cos_theta_a;
	const float v = s + sin_theta_a * A.dot(B);
	float q;
	q = ((v * t - u * s) * cos_theta_a - v) / ((v * s + u * t) * sin_theta_a);
	const Point3F D = A * q + aux(C, A) * sqrtf(fmax(0.f, 1.f - q * q));
	q = 1.f - rnd[1] * (1.f - dot(D, B));
	*S = B * q + aux(D, B) * sqrtf(fmax(0.f, 1 - q * q));
	// OK
	return true;
}

/*!
 * \brief
 * Projects a point on the canonical unit sphere back to the original triangle's plane.
 *
 * \param[in] o The center of the projection sphere.
 * \param[in] p The point on the original triangle's plane.
 * \param[in] n The normal vector the original triangle's plane.
 * \param[in] S the point on the canonical unit sphere.
 *
 * \return The point `S` projected back onto the original triangle's plane.
 */
__host__ __device__
inline Point3F planarProjection(const Point3F& o, const Point3F& P, const Vectr3F& n, const Vectr3F& S) {
	return o + S * ((dot(n, P) - dot(n, o)) / dot(n, S));
}

}

#endif /* SPHERICALGEOMETRY_CUH_ */

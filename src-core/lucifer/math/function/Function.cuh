#ifndef FUNCTION_CUH_
#define FUNCTION_CUH_

#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/memory/Virtual.cuh"

namespace lucifer {

template<typename... T> class Function: public Polymorphic {
public:
	using scalar_eval_t = Virtual<float, const Function<T...>*, T...>;
	using ranged_eval_t = Virtual<RangeF, const Function<T...>*, T..., T...>;

private:
	const scalar_eval_t scalar_;
	const ranged_eval_t range_;

public:
	__host__
	Function(
		const scalar_eval_t& scalar_,
		const ranged_eval_t& range_):
	scalar_(scalar_), range_(range_) {
	}

	__host__ __device__
	float operator()(T... x) const {
		return scalar_(this, x...);
	}

	__host__ __device__
	RangeF evaluate(T... l, T... u) const {
		return range_(this, l..., u...);
	}
};

using Function1D = Function<float>;

}

#endif /* FUNCTION_CUH_ */

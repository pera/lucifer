#ifndef REGULARGRID1D_CUH_
#define REGULARGRID1D_CUH_

#include <cstdint>
#include <iostream>
#include "Function.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/Util.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class RegularGrid1D: public Function1D {
public:
	using dim_t = uint16_t;
	using array = Array<float, dim_t>;

private:
	const RangeF param;
	const RangeF value;
	const float delta;
	const array samples;

	__host__
	static RangeF range(const array& values) {
		float min = +INFINITY;
		float max = -INFINITY;
		for (dim_t i = 0; i < values.size(); ++i) {
			if (min > values[i]) min = values[i];
			if (max < values[i]) max = values[i];
		}
		return RangeF(min, max);
	}

	__host__
	static array sample(MemoryPool& pool, const float l, const float u, const dim_t n, const Function1D* f) {
		array samples(pool, n);
		float delta = (u - l) / (n - 1);
		for (dim_t i = 0; i < n; ++i) {
			samples[i] = (*f)(l + i * delta);
		}
		return samples;
	}

	__host__ __device__
	static float scalar_(const Function<float>* this_, float x)  {
		auto self = static_cast<const RegularGrid1D*>(this_);
	    auto i = (int32_t) ((x - self->param.lower) / self->delta);
	    return lerp(
			self->samples[clamp<dim_t>(i + 0, 0, self->samples.size() - 1)],
			self->samples[clamp<dim_t>(i + 1, 0, self->samples.size() - 1)],
			(x - (self->param.lower + i * self->delta)) / self->delta
		);
	}

	__host__ __device__
	static RangeF range_(const Function<float>* this_, float lower, float upper)  {
		return static_cast<const RegularGrid1D*>(this_)->value;
	}

	__host__
	RegularGrid1D(
		const float l,
		const float u,
		const array& values,
		const scalar_eval_t& scalar_,
		const ranged_eval_t& range_
	): Function1D(scalar_, range_), param(l, u), value(range(values)), delta((u - l) / (values.size() - 1.f)), samples(values) {
	}

public:
	friend class TypedPool<RegularGrid1D>;

	__host__
	static Pointer<RegularGrid1D> build(MemoryPool& pool, const float l, const float u, const array& values) {
		return Pointer<RegularGrid1D>::make(
			pool, l, u, values,
			scalar_eval_t(virtualize(scalar_)),
			ranged_eval_t( range_, [] __device__ () {return (void*)range_ ;})
		);
	}

	__host__
	static Pointer<RegularGrid1D> build(MemoryPool& pool, const float l, const float u, const dim_t n, const Function1D* f) {
		return Pointer<RegularGrid1D>::make(
			pool, l, u, sample(pool, l, u, n, f),
			scalar_eval_t(virtualize(scalar_)),
			ranged_eval_t( range_, [] __device__ () {return (void*)range_ ;})
		);
	}

	__host__ __device__
	inline float operator[](const dim_t i) const {
		return samples[i];
	}
};

}

#endif /* REGULARGRID1D_CUH_ */

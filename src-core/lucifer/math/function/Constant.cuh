#ifndef CONSTANT_CUH_
#define CONSTANT_CUH_

#include "Function.cuh"
#include "lucifer/memory/MemoryPool.cuh"

namespace lucifer {

template<typename ... T> class Constant: public Function<T...> {
private:
	const float value;

	__host__
	Constant(
		const float value,
		const typename Function<T...>::scalar_eval_t& scalar,
		const typename Function<T...>::ranged_eval_t& range
	): Function<T...>(scalar, range), value(value) {
	}

	__host__ __device__
	static float scalar_(const Function<T...>* this_, T... args)  {
		return static_cast<const Constant<T...>*>(this_)->value;
	}

	__host__ __device__
	static Range<float> range_(const Function<T...>* this_, T... lower, T... upper)  {
		return Range<float>{static_cast<const Constant<T...>*>(this_)->value};
	}

public:
	friend class TypedPool<Constant<T...>>;

	__host__
	static Constant<T...>* build(MemoryPool& pool, const float value) {
	return pool.create<Constant<T...>>(
		value,
		typename Function<T...>::scalar_eval_t(virtualize(scalar_)),
		typename Function<T...>::ranged_eval_t( range_, [] __device__ () {return (void*)range_ ;})
	);
}
};

}

#endif /* CONSTANT_CUH_ */


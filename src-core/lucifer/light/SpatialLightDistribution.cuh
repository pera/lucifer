#ifndef SPATIALLIGHTDISTRIBUTION_CUH_
#define SPATIALLIGHTDISTRIBUTION_CUH_

#include <cassert>
#include "LightDistribution.cuh"
#include "lucifer/random/CorrelatedMultijitteredRNG.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/util/ZOrder.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/memory/Polymorphic.cuh"

namespace lucifer {

class SpatialLightDistribution: public LightDistribution {
public:
	class Node: public Polymorphic {
	public:
		friend class TypedPool<Node>;

	public:
		const Box box;
		const float power;
		const Pointer<const GeometricPrimitive> light;
		const Pointer<const Node> child0;
		const Pointer<const Node> child1;
		Pointer<const Node> parent;

	private:
		__host__
		static float estimateWeight(Pointer<const GeometricPrimitive>& primitive, const uint16_t samples, RNG* lRNG, RNG* tRNG, RNG* gRNG, const XYZColorSpace* colorSpace) {
			float l, lPDF, lRND;
			float t, tPDF, tRND;
			float gRND[2];
			Geometry g;
			Color3F color;
			float power = 0.f;
			for (uint16_t i = 0; i < samples; i++) {
				// sample wavelength
				lRNG->next(&lRND);
				l = colorSpace->xyzHistogram()->sample(lRND, &lPDF);
				// sample time
				tRNG->next(&tRND);
				tPDF = 1.f;
				// sample geometry
				gRNG->next(gRND);
				primitive->getShape()->sampleGeometry(tRND, gRND, &g);
				// emitted XYZ power
				colorSpace->XYZ(l, primitive->getMaterial()->emission0(g, l, t) / (lPDF * tPDF), color);
				power += (color[0] + color[1] + color[2]);
			}
			return power * primitive->getShape()->area(t) / samples;
		}

	public:
		__host__
		Node(Pointer<const GeometricPrimitive>& light, const uint16_t samples, RNG* lRNG, RNG* tRNG, RNG* gRNG, const XYZColorSpace* colorSpace):
		box(light->boundary(RangeF(0.f, 1.f))), power(estimateWeight(light, samples, lRNG, tRNG, gRNG, colorSpace)), light(light), child0(), child1(), parent() {
		}

		__host__
		Node(MemoryPool& pool, Pointer<Node>& child0, Pointer<Node>& child1):
		box(child0->box + child1->box), power(child0->power + child1->power), light(), child0(child0), child1(child1), parent() {
		}

		__host__ __device__
		float meanDistanceE2(const Point3F& ref) const {
			auto pmin = box.lower() - ref;
			auto pmax = box.upper() - ref;
			return (pmin[2] * pmin[2] + pmax[2] * pmin[2] + pmax[2] * pmax[2] +
					pmin[1] * pmin[1] + pmax[1] * pmin[1] + pmax[1] * pmax[1] +
					pmin[0] * pmin[0] + pmax[0] * pmin[0] + pmax[0] * pmax[0]) / 3.f;
		}

		__host__ __device__
		float meanWeight(const Point3F& ref) const {
			return power / meanDistanceE2(ref);
		}
	};

private:
	using index_t = uint32_t;
	using array_t = Array<Pointer<Node>, index_t>;

private:
	array_t index;
	Pointer<Node> root;

private:
	__host__
	Pointer<Node> build(
		MemoryPool& pool,
		index_t begin,
		index_t end,
		std::vector<Pointer<const GeometricPrimitive>>& lights,
		index_t& idx,
		const uint16_t samples,
		RNG* lRNG,
		RNG* tRNG,
		RNG* gRNG,
		const XYZColorSpace* colorSpace)
	{
		if (end == begin) {
			// no light sources, return null
			return Pointer<Node>();
		}
		if (end - begin == 1) {
			// one light source, build leaf node
			lights[begin]->id(idx);
			return index[idx++] = Pointer<Node>::make(pool, lights[begin], samples, lRNG, tRNG, gRNG, colorSpace);
		}
		// recurse
		index_t middle = (begin + end) / 2;
		Pointer<Node> child0 = build(pool, begin, middle, lights, idx, samples, lRNG, tRNG, gRNG, colorSpace);
		Pointer<Node> child1 = build(pool, middle,   end, lights, idx, samples, lRNG, tRNG, gRNG, colorSpace);
		auto node = Pointer<Node>::make(pool, pool, child0, child1);
		child0->parent = node;
		child1->parent = node;
		return node;
	}

	__host__ __device__
	const Node* select(const Point3F& ref, float rnd[2], float* prb) const {
		*prb = 1.f;
		const Node* node = root.get();
		while (node != nullptr && node->light.isNull()) {
			assert(!node->child0.isNull());
			assert(!node->child1.isNull());
			float w = node->child0->meanWeight(ref);
			w = w / (w + node->child1->meanWeight(ref));
			if (rnd[0] <= w) {
				rnd[0]  = (rnd[0] - 0.f) / (w - 0.f);
				assert(rnd[0] >= 0.f and rnd[0] <= 1.f);
				node = node->child0.get();
				*prb *= (w - 0.f);
			} else {
				rnd[0]  = (rnd[0] -   w) / (1.f - w);
				assert(rnd[0] >= 0.f and rnd[0] <= 1.f);
				node = node->child1.get();
				*prb *= (1.f - w);
			}
		}
		return node;
	}

	__host__ __device__
	static const SpatialLightDistribution* cast(const LightDistribution* this_) {
		return static_cast<const SpatialLightDistribution*>(this_);
	}

	__host__ __device__
	static float sample_le_(const LightDistribution* this_, const Point3F& ref, const float wlen, const float time, float rnd[2], Geometry* geometry, float* pdf) {
		float prb;
		const Node* node = cast(this_)->select(ref, rnd, &prb);
		assert(!node->light.isNull());
		const float le = node->light->sampleLe(ref, wlen, time, rnd, geometry, pdf);
		*pdf *= prb;
		return le;
	}

	__host__ __device__
	static float sample_le_pdf_(const LightDistribution* this_, const Point3F& ref, const float wlen, const float time, const Primitive::Intersection& isect) {
		const Node* node = cast(this_)->index[isect.primitive->id()].get();
		assert(!node->light.isNull());
		float prb = 1.f;
		while (!node->parent.isNull()) {
			const Node* s = (node == node->parent->child0.get()) ? node->parent->child1.get() : node->parent->child0.get();
			float w = node->meanWeight(ref);
			w = w / (w + s->meanWeight(ref));
			node = node->parent.get();
			prb *= w;
		}
		return isect.primitive->sampleLePDF(ref, wlen, time, isect.geometry) * prb;
	}

	__host__ __device__
	static void sample_solid_angle_(const LightDistribution* this_, const Point3F& ref, const float wlen, const float time, float rnd[2], Geometry* geometry, float* pdf, const GeometricPrimitive** sampled) {
		assert(false);
	}

	__host__ __device__
	static void sample_area_(const LightDistribution* this_, const float wlen, const float time, float rnd[2], Geometry* geometry, float* pdf, const GeometricPrimitive** sampled) {
		assert(false);
	}

	__host__ __device__
	static float sample_area_pdf_(const LightDistribution* this_, const float wlen, const float time, const Point3F& geometry, const GeometricPrimitive* sampled) {
		assert(false);
		return 0.f;
	}

	__host__
	SpatialLightDistribution(
		MemoryPool& pool,
		std::vector<Pointer<const GeometricPrimitive>>& lights,
		const uint16_t samples,
		const XYZColorSpace* colorSpace,
		const sample_le_t& sample_le_,
		const sample_le_pdf_t& sample_le_pdf_,
		const sample_solid_angle_t& sample_solid_angle_,
		const sample_area_t& sample_area_,
		const sample_area_pdf_t& sample_area_pdf_
	):
		LightDistribution(sample_le_, sample_le_pdf_, sample_solid_angle_, sample_area_, sample_area_pdf_),
		index(pool, lights.size()), root()
	{
		// sort light sources using z order of centroids
		Box box;
		for (auto& l : lights) box += l->boundary(RangeF(0.f, 1.f));
		ZOrderComparator zorder(box.lower(), box.upper());
		std::sort(lights.begin(),lights.end(),
			[&zorder](Pointer<const GeometricPrimitive>& a, Pointer<const GeometricPrimitive>& b) {
				return zorder(a->boundary(RangeF(0.f, 1.f)).centroid(), b->boundary(RangeF(0.f, 1.f)).centroid());
			}
		);
		// build light source tree and define primitives indexes
		index_t idx = 0;
		CPUCorrelatedMultiJitteredRNGFactory factory(samples);
		Pointer<RNG> lRNG = factory.build(pool, 1);
		Pointer<RNG> tRNG = factory.build(pool, 1);
		Pointer<RNG> gRNG = factory.build(pool, 2);
		root = build(pool, 0, lights.size(), lights, idx, samples, lRNG.get(), tRNG.get(), gRNG.get(), colorSpace);
	}

public:
	friend class TypedPool<SpatialLightDistribution>;

	__host__
	static Pointer<SpatialLightDistribution> build(
		MemoryPool& pool,
		std::vector<Pointer<const GeometricPrimitive>>& lights,
		const uint16_t samples,
		const XYZColorSpace* colorSpace)
	{
		return Pointer<SpatialLightDistribution>::make(
			pool, pool, lights, samples, colorSpace,
			sample_le_t(virtualize(sample_le_)),
			sample_le_pdf_t(virtualize(sample_le_pdf_)),
			sample_solid_angle_t(virtualize(sample_solid_angle_)),
			sample_area_t(virtualize(sample_area_)),
			sample_area_pdf_t(virtualize(sample_area_pdf_))
		);
	}
};

}

#endif /* SPATIALLIGHTDISTRIBUTION_CUH_ */

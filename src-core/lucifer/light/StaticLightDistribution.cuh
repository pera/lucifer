#ifndef STATICLIGHTDISTRIBUTION_CUH_
#define STATICLIGHTDISTRIBUTION_CUH_

#include <cassert>
#include <cstdint>
#include <type_traits>
#include "LightDistribution.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/memory/Polymorphic.cuh"

namespace lucifer {
/*!
 * \brief
 * A light distribution with static weight for each primitive light source.
 *
 * \author
 * Bruno Pera.
 */
template<typename this_t>
class StaticLightDistribution: public LightDistribution {
public:
	class Element: public Polymorphic {
	public:
		Pointer<const GeometricPrimitive> primitive;
		float weightValue;
		float weightTotal;

	public:
		__host__
		Element(Pointer<const GeometricPrimitive>& primitive, const float value) :
		primitive(primitive), weightValue(value), weightTotal(0.f) {
		}
	};

	using index_t = uint32_t;
	using array_t = Array<Pointer<Element>, index_t>;

private:
	 array_t elements;

	__host__ __device__
	float child0TotalWeight(index_t idx) const {
		index_t childIdx = 2 * idx + 1;
		return childIdx < elements.size() ? elements[childIdx]->weightTotal : 0.f;
	}

	__host__ __device__
	float child1TotalWeight(index_t idx) const {
		index_t childIdx = 2 * idx + 2;
		return childIdx < elements.size() ? elements[childIdx]->weightTotal : 0.f;
	}

	__host__ __device__
	float accumulateWeights(index_t i) {
		return i >= elements.size() ? 0.f : elements[i]->weightTotal = elements[i]->weightValue + accumulateWeights(2 * i + 1) + accumulateWeights(2 * i + 2);
	}

	__host__ __device__
	index_t select(float rnd[2]) const {
		index_t i = 0;
		while (i < elements.size()) {
			float a	= elements[i]->weightValue;
			float b = a + child0TotalWeight(i);
			float c = b + child1TotalWeight(i);
			a /= c;
			b /= c;
			if (rnd[0] <= a) {
				rnd[0] /= a;
				return i;
			}
			if (rnd[0] <= b) {
				rnd[0] = (rnd[0] - a) / (b - a);
				i = 2 * i + 1;
			} else {
				rnd[0] = (rnd[0] - b) / (1 - b);
				i = 2 * i + 2;
			}
		}
		assert(false);
		return 0;
	}

	__host__ __device__
	static const StaticLightDistribution<this_t>* cast(const LightDistribution* this_) {
		return static_cast<const StaticLightDistribution<this_t>*>(this_);
	}

protected:
	__host__ __device__
	static float sample_le_(const LightDistribution* this_, const Point3F& ref, const float wlen, const float time, float rnd[2], Geometry* geometry, float* pdf) {
		index_t i = cast(this_)->select(rnd);
		float le = cast(this_)->elements[i]->primitive->sampleLe(ref, wlen, time, rnd, geometry, pdf);
		*pdf = cast(this_)->elements[i]->weightValue * (*pdf) / cast(this_)->elements[0]->weightTotal;
		return le;
	}

	__host__ __device__
	static float sample_le_pdf_(const LightDistribution* this_, const Point3F& ref, const float wlen, const float time, const Primitive::Intersection& isect) {
		assert(isect.primitive != nullptr);
		return cast(this_)->elements[isect.primitive->id()]->weightValue * isect.primitive->sampleLePDF(ref, wlen, time, isect.geometry) / cast(this_)->elements[0]->weightTotal;
	}

	__host__ __device__
	static void sample_solid_angle_(const LightDistribution* this_, const Point3F& ref, const float wlen, const float time, float rnd[2], Geometry* geometry, float* pdf, const GeometricPrimitive** sampled) {
		index_t i = cast(this_)->select(rnd);
		*sampled = cast(this_)->elements[i]->primitive.get();
		*pdf = cast(this_)->elements[i]->weightValue * (*pdf) / cast(this_)->elements[0]->weightTotal;
	}

	__host__ __device__
	static void sample_area_(const LightDistribution* this_, const float wlen, const float time, float rnd[2], Geometry* geometry, float* pdf, const GeometricPrimitive** sampled) {
		index_t i = cast(this_)->select(rnd);
		cast(this_)->elements[i]->primitive->sampleLeGeo(wlen, time, rnd, geometry, pdf);
		*sampled = cast(this_)->elements[i]->primitive.get();
		*pdf = cast(this_)->elements[i]->weightValue * (*pdf) / cast(this_)->elements[0]->weightTotal;
	}

	__host__ __device__
	static float sample_area_pdf_(const LightDistribution* this_, const float wlen, const float time, const Point3F& geometry, const GeometricPrimitive* sampled) {
		assert(sampled != nullptr);
		return cast(this_)->elements[sampled->id()]->weightValue * sampled->sampleLeGeoPDF(wlen, time, geometry) / cast(this_)->elements[0]->weightTotal;
	}

	template<typename functor_t> __host__
	StaticLightDistribution(
		MemoryPool& pool,
		std::vector<Pointer<const GeometricPrimitive>>& lights,
		functor_t weight,
		const sample_le_t& sample_le_,
		const sample_le_pdf_t& sample_le_pdf_,
		const sample_solid_angle_t& sample_solid_angle_,
		const sample_area_t& sample_area_,
		const sample_area_pdf_t& sample_area_pdf_
	):
		LightDistribution(sample_le_, sample_le_pdf_, sample_solid_angle_, sample_area_, sample_area_pdf_),
		elements(pool, lights.size())
	{
		for (index_t i = 0; i < elements.size(); ++i) {
			lights[i]->id(i);
			elements[i] = Pointer<Element>::make(pool, lights[i], weight(lights[i].get()));
		}
		accumulateWeights(0);
	}
};

}

#endif /* STATICLIGHTDISTRIBUTION_CUH_ */

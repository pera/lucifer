#ifndef POWERLIGHTDISTRIBUTION_CUH_
#define POWERLIGHTDISTRIBUTION_CUH_

#include "StaticLightDistribution.cuh"
#include "lucifer/color/XYZColorSpace.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * Samples light sources according to the average emitted power.
 */
class PowerLightDistribution: public StaticLightDistribution<PowerLightDistribution> {
private:
	__host__
	static float estimateWeight(const GeometricPrimitive* primitive, const uint16_t samples, RNG* lRNG, RNG* tRNG, RNG* gRNG, const XYZColorSpace* colorSpace) {
		float l, lPDF, lRND;
		float t, tPDF, tRND;
		float gRND[2];
		Geometry g;
		Color3F color;
		float power = 0.f;
		for (uint16_t i = 0; i < samples; i++) {
			// sample wavelength
			lRNG->next(&lRND);
			l = colorSpace->xyzHistogram()->sample(lRND, &lPDF);
			// sample time
			tRNG->next(&tRND);
			tPDF = 1.f;
			// sample geometry
			gRNG->next(gRND);
			primitive->getShape()->sampleGeometry(tRND, gRND, &g);
			// emitted XYZ power
			colorSpace->XYZ(l, primitive->getMaterial()->emission0(g, l, t) / (lPDF * tPDF), color);
			power += (color[0] + color[1] + color[2]) * primitive->getShape()->area(t);
		}
		return power / samples;
	}

	__host__
	PowerLightDistribution(
		MemoryPool& pool,
		std::vector<Pointer<const GeometricPrimitive>>& lights,
		const uint samples,
		RNG* lRNG,
		RNG* tRNG,
		RNG* gRNG,
		const XYZColorSpace* colorSpace,
		const sample_le_t& sample_le_,
		const sample_le_pdf_t& sample_le_pdf_,
		const sample_solid_angle_t& sample_solid_angle_,
		const sample_area_t& sample_area_,
		const sample_area_pdf_t& sample_area_pdf_
	):
		StaticLightDistribution(
			pool,
			lights,
			[&](const GeometricPrimitive* p) { return estimateWeight(p, samples, lRNG, tRNG, gRNG, colorSpace); },
			sample_le_,
			sample_le_pdf_,
			sample_solid_angle_,
			sample_area_,
			sample_area_pdf_) {
	}

public:
	friend class TypedPool<PowerLightDistribution>;
	/*!
	 * \brief
	 * Creates a new power light distribution.
	 *
	 * \param[in] lights Set of all light-emitting geometric primitives.
	 * \param[in] samples Number of time samples used to estimate the emitted power of each light source.
	 * \param[in] lRNG one dimensional random number generator used to sample light wavelength.
	 * \param[in] tRNG One dimensional random number generator used to sample time.
	 * \param[in] gRNG One dimensional random number generator used to sample surface geometry.
	 * \param[in] colorSpace XYZ colorspace.
	 */
	__host__
	static Pointer<PowerLightDistribution> build(
		MemoryPool& pool,
		std::vector<Pointer<const GeometricPrimitive>>& lights,
		const uint samples,
		RNG* lRNG,
		RNG* tRNG,
		RNG* gRNG,
		const XYZColorSpace* colorSpace)
	{
		return Pointer<PowerLightDistribution>::make(
			pool, pool, lights, samples, lRNG, tRNG, gRNG, colorSpace,
			sample_le_t(virtualize(sample_le_)),
			sample_le_pdf_t(virtualize(sample_le_pdf_)),
			sample_solid_angle_t(virtualize(sample_solid_angle_)),
			sample_area_t(virtualize(sample_area_)),
			sample_area_pdf_t(virtualize(sample_area_pdf_))
		);
	}
};

}

#endif /* POWERLIGHTDISTRIBUTION_CUH_ */

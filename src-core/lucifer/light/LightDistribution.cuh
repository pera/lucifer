#ifndef LIGHTDISTRIBUTION_CUH_
#define LIGHTDISTRIBUTION_CUH_

#include "lucifer/primitive/Primitive.cuh"
#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/memory/Virtual.cuh"

namespace lucifer {

/*!
 * \brief
 * Distribution used sample the light sources.
 *
 * \author
 * Bruno Pera.
 */
class LightDistribution: public Polymorphic {
public:
	/**************************************************************************
	 * cuda virtual interface
	 *************************************************************************/
	using sample_le_t = Virtual<
		float,							// return type
		const LightDistribution*,		// this
		const Point3F&,		 			// reference
		const float,					// wavelentgh
		const float,					// time
		float[2],						// random
		Geometry*,						// geometry
		float*							// pdf
	>;

	using sample_le_pdf_t = Virtual<
		float,							// return type
		const LightDistribution*,		// this
		const Point3F&, 				// reference
		const float,					// wvelength
		const float,					// time
		const Primitive::Intersection&	// intersection
	>;

	using sample_solid_angle_t = Virtual<
		void, 							// return type
		const LightDistribution*,		// this
		const Point3F&,					// reference
		const float,					// wavelength
		const float,					// time
		float[2],						// random
		Geometry*,						// geometry
		float*,							// pdf
		const GeometricPrimitive**		// sampled
	>;

	using sample_area_t = Virtual<
		void,							// return type
		const LightDistribution*,		// this
		const float,					// wavelength
		const float,					// time
		float[2],						// random
		Geometry*,						// geometry
		float*,							// pdf
		const GeometricPrimitive**		// sampled
	>;

	using sample_area_pdf_t = Virtual<
		float, 							// return type
		const LightDistribution*,		// this
		const float,	 				// wavelength
		const float,					// time
		const Point3F&,					// geometry
		const GeometricPrimitive*		// sampled
	>;

private:
	const sample_le_t sample_le_;
	const sample_le_pdf_t sample_le_pdf_;
	const sample_solid_angle_t sample_solid_angle_;
	const sample_area_t sample_area_;
	const sample_area_pdf_t sample_area_pdf_;

protected:
	__host__
	LightDistribution(
		const sample_le_t& sample_le_,
		const sample_le_pdf_t& sample_le_pdf_,
		const sample_solid_angle_t& sample_solid_angle_,
		const sample_area_t& sample_area_,
		const sample_area_pdf_t& sample_area_pdf_

	):
		sample_le_(sample_le_),
		sample_le_pdf_(sample_le_pdf_),
		sample_solid_angle_(sample_solid_angle_),
		sample_area_(sample_area_),
		sample_area_pdf_(sample_area_pdf_) {
	}

public:
	/*!
	 * \brief
	 * Samples a position on the light source surface and computes the spectral
	 * emitted power from the sampled point towards the given reference point.
	 *
	 * \param[in] ref Reference point being illuminated.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time Then normalized time in [0, 1].
	 * \param[in] rnd A triplet of randomly generated numbers in [0, 1].
	 * \param[out] geometry The sampled geometry on the light source surface.
	 * \param[out] pdf The PDF according to the solid angle measure.
	 *
	 * \return Emitted spectral power towards the reference point.
	 */
	__host__ __device__
	float sampleLe(const Point3F& ref, const float wlen, const float time, float rnd[2], Geometry* geometry, float* pdf) const {
		return sample_le_(this, ref, wlen, time, rnd, geometry, pdf);
	}

	/*!
	 * \brief
	 * Computes the PDF of sampling a position on the light source surface
	 * according to the solid angle measured from given reference point.
	 *
	 * \param[in] ref Reference point being illuminated.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time Then normalized time in [0, 1].
	 * \param[in] isect Interaction from where light is emitted.
	 *
	 * \return The PDF of sampling the intersection point.
	 */
	__host__ __device__
	float sampleLePDF(const Point3F& ref, const float wlen, const float time, const Primitive::Intersection& isect) const {
		return sample_le_pdf_(this, ref, wlen, time, isect);
	}

	/**
	 * \brief
	 * Samples a position on the light source surface
	 *
	 * \param[in] ref Reference point being illuminated.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time Then normalized time in [0, 1].
	 * \param[in] rnd A triplet of randomly generated numbers in [0, 1].
	 * \param[out] geometry The sampled geometry on the light source surface.
	 * \param[out] pdf The PDF according to the solid angle measure.
	 * \param[out] sampled The geometric primitive actually sampled.
	 */
	__host__ __device__
	void sampleGeometry(const Point3F& ref, const float wlen, const float time, float rnd[2], Geometry* geometry, float* pdf, const GeometricPrimitive** sampled) const {
		sample_solid_angle_(this, ref, wlen, time, rnd, geometry, pdf, sampled);
	}

	/**
	 * \brief
	 * Samples a position on the light source surface
	 *
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time Then normalized time in [0, 1].
	 * \param[in] rnd A triplet of randomly generated numbers in [0, 1].
	 * \param[out] geometry The sampled geometry on the light source surface.
	 * \param[out] pdf PDF with according to the surface area measure.
	 * \param[out] sampled The geometric primitive actually sampled.
	 */
	__host__ __device__
	void sampleGeometry(const float wlen, const float time, float rnd[2], Geometry* geometry, float* pdf, const GeometricPrimitive** sampled) const {
		sample_area_(this, wlen, time, rnd, geometry, pdf, sampled);
	}

	/*!
	 * \brief
	 * Computes the PDF of sampling a position on the light source surface
	 * according to the area measure.
	 *
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time Then normalized time in [0, 1].
	 * \param[in] geometry The geometry on the light source surface.
	 * \param[out] sampled The basic primitive that was actually sampled.
	 *
	 * \return The PDF according to the surface area measure.
	 */
	__host__ __device__
	float sampleGeometryPDF(const float wlen, const float time, const Point3F& geometry, const GeometricPrimitive* sampled) const {
		return sample_area_pdf_(this, wlen, time, geometry, sampled);
	}
};

}

#endif /* LIGHTDISTRIBUTION_CUH_ */

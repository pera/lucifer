/*
 * UnweightedLightDistribution.cuh
 *
 *  Created on: 26/12/2018
 *      Author: touch
 */

#ifndef UNWEIGHTEDLIGHTDISTRIBUTION_CUH_
#define UNWEIGHTEDLIGHTDISTRIBUTION_CUH_

#include <cstdint>
#include <vector>
#include "LightDistribution.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class UnweightedLightDistribution: public LightDistribution {
private:
	using index_t = uint32_t;
	Array<Pointer<const GeometricPrimitive>, index_t> lights;

	__host__ __device__
	index_t select(float rnd[2]) const {
		index_t i = rnd[0] * lights.size();
		if (i >= lights.size()) i = lights.size()-1;
		rnd[0] = clamp(lights.size() * (rnd[0] - ((float) i) / lights.size()), 0.f, 1.f);
		return i;
	}

	__host__ __device__
	static const UnweightedLightDistribution* cast(const LightDistribution* this_) {
		return static_cast<const UnweightedLightDistribution*>(this_);
	}

	__host__ __device__
	static float sample_le_(const LightDistribution* this_, const Point3F& ref, const float wlen, const float time, float rnd[2], Geometry* geometry, float* pdf) {
		const float le = cast(this_)->lights[cast(this_)->select(rnd)]->sampleLe(ref, wlen, time, rnd, geometry, pdf);
		*pdf /= cast(this_)->lights.size();
		return le;
	}

	__host__ __device__
	static float sample_le_pdf_(const LightDistribution* this_, const Point3F& ref, const float wlen, const float time, const Primitive::Intersection& isect) {
		return isect.primitive->sampleLePDF(ref, wlen, time, isect.geometry) / cast(this_)->lights.size();
	}

	__host__ __device__
	static void sample_solid_angle_(const LightDistribution* this_, const Point3F& ref, const float wlen, const float time, float rnd[2], Geometry* geometry, float* pdf, const GeometricPrimitive** sampled) {
		index_t i = cast(this_)->select(rnd);
		cast(this_)->lights[i]->sampleLeGeo(ref, wlen, time, rnd, geometry, pdf);
		*pdf / cast(this_)->lights.size();
		*sampled = cast(this_)->lights[i].get();
	}

	__host__ __device__
	static void sample_area_(const LightDistribution* this_, const float wlen, const float time, float rnd[2], Geometry* geometry, float* pdf, const GeometricPrimitive** sampled) {
		index_t i = cast(this_)->select(rnd);
		cast(this_)->lights[i]->sampleLeGeo(wlen, time, rnd, geometry, pdf);
		*pdf / cast(this_)->lights.size();
		*sampled = cast(this_)->lights[i].get();
	}

	__host__ __device__
	static float sample_area_pdf_(const LightDistribution* this_, const float wlen, const float time, const Point3F& geometry, const GeometricPrimitive* sampled) {
		return sampled->sampleLeGeoPDF(wlen, time, geometry) / cast(this_)->lights.size();
	}

	__host__
	UnweightedLightDistribution(
		MemoryPool& pool,
		std::vector<Pointer<const GeometricPrimitive>>& luminaries,
		const sample_le_t& sample_le_,
		const sample_le_pdf_t& sample_le_pdf_,
		const sample_solid_angle_t& sample_solid_angle_,
		const sample_area_t& sample_area_,
		const sample_area_pdf_t& sample_area_pdf_
	):
		LightDistribution(sample_le_, sample_le_pdf_, sample_solid_angle_, sample_area_, sample_area_pdf_),
		lights(pool, luminaries.size(), luminaries.data()) {
	}

public:
	friend class TypedPool<UnweightedLightDistribution>;

	__host__
	static Pointer<UnweightedLightDistribution> build(
		MemoryPool& pool,
		std::vector<Pointer<const GeometricPrimitive>>& luminaries)
	{
		return Pointer<UnweightedLightDistribution>::make(
			pool, pool, luminaries,
			sample_le_t(virtualize(sample_le_)),
			sample_le_pdf_t(virtualize(sample_le_pdf_)),
			sample_solid_angle_t(virtualize(sample_solid_angle_)),
			sample_area_t(virtualize(sample_area_)),
			sample_area_pdf_t(virtualize(sample_area_pdf_))
		);
	}
};

}

#endif /* UNWEIGHTEDLIGHTDISTRIBUTION_CUH_ */

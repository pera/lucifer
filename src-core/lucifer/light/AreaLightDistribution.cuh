#ifndef AREALIGHTDISTRIBUTION_CUH_
#define AREALIGHTDISTRIBUTION_CUH_

#include "StaticLightDistribution.cuh"
#include "lucifer/random/RNG.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * Samples light sources according to the average area.
 */
class AreaLightDistribution: public StaticLightDistribution<AreaLightDistribution> {
private:
	__host__
	static float estimateWeight(const GeometricPrimitive* primitive, const uint16_t samples, RNG* rng1) {
		float time;
		float area = 0.f;
		for (uint16_t i = 0; i < samples; ++i) {
			rng1->next(&time);
			area += primitive->area(time);
		}
		return area / samples;
	}

	__host__
	AreaLightDistribution(
		MemoryPool& pool,
		std::vector<Pointer<const GeometricPrimitive>>& lights,
		const uint samples,
		RNG* rng1,
		const sample_le_t& sample_le_,
		const sample_le_pdf_t& sample_le_pdf_,
		const sample_solid_angle_t& sample_solid_angle_,
		const sample_area_t& sample_area_,
		const sample_area_pdf_t& sample_area_pdf_
	):
		StaticLightDistribution(
			pool,
			lights,
			[&](const GeometricPrimitive* p) { return estimateWeight(p, samples, rng1); },
			sample_le_,
			sample_le_pdf_,
			sample_solid_angle_,
			sample_area_,
			sample_area_pdf_) {
	}

public:
	friend class TypedPool<AreaLightDistribution>;
	/*!
	 * \brief
	 * Creates a new area light distribution.
	 *
	 * \param[in] lights Set of all light-emitting geometric primitives.
	 * \param[in] samples Number o time samples used to estimate the area of each light source.
	 * \param[in] rng1 One dimensional random number generator used to sample time.
	 */
	__host__
	static Pointer<AreaLightDistribution> build(MemoryPool& pool, std::vector<Pointer<const GeometricPrimitive>>& lights, const uint samples, RNG* rng1) {
		return Pointer<AreaLightDistribution>::make(
			pool, pool, lights, samples, rng1,
			sample_le_t(virtualize(sample_le_)),
			sample_le_pdf_t(virtualize(sample_le_pdf_)),
			sample_solid_angle_t(virtualize(sample_solid_angle_)),
			sample_area_t(virtualize(sample_area_)),
			sample_area_pdf_t(virtualize(sample_area_pdf_))
		);
	}
};

}

#endif /* AREALIGHTDISTRIBUTION_CUH_ */

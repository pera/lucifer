#ifndef MATERIAL_CUH_
#define MATERIAL_CUH_

#include "lucifer/uedf/UEDF.cuh"
#include "lucifer/bsdf/BSDF.cuh"
#include "lucifer/math/geometry/AnimatedTransformation.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/medium/Medium.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/memory/Virtual.cuh"

namespace lucifer {

/*!
 * \brief
 *
 * Material interface.
 *
 * The Material interface is the entry point to the shading sub system. The
 * implementations must provide methods that define the way light
 * interacts with a Primitive, including emission, scattering at the surface
 * and scattering within the medium.
 *
 * \author
 * Bruno Pera.
 */
class Material: public Polymorphic {
public:
	/**************************************************************************
	 * uedf cuda virtual interface
	 *************************************************************************/
	using uedf_is_emissive_t = Virtual<
		bool,					// return type
		const Material*			// this
	>;

	using uedf_emission0_t = Virtual<
		float, 					// return type
		const Material*, 		// this
		const Geometry&, 		// geometry
		const float, 			// wavelength
		const float				// time
	>;

	using uedf_emission1_t = Virtual<
		float, 					// return type
		const Material*, 		// this
		const Geometry&, 		// geometry
		const float, 			// wavelength
		const float, 			// time
		const Vectr3F&			// direction
	>;

	using uedf_emission_t = Virtual<
		float, 					// return type
		const Material*, 		// this
		const Geometry&, 		// geometry
		const float, 			// wavelength
		const float, 			// time
		const Vectr3F&			// direction
	>;

	using uedf_sample_we_t = Virtual<
		float, 					// return type
		const Material*, 		// this
		const Geometry&, 		// geometry
		const float, 			// wavelength
		const float, 			// time
		float[2], 				// random
		Vectr3F*, 				// direction
		float*, 				// sampling pdf
		const UEDC**			// sampled uedc
	>;

	using uedf_sample_we_pdf_t = Virtual<
		float, 					// return type
		const Material*, 		// this
		const Geometry&, 		// geometry
		const float, 			// wavelength
		const float, 			// time
		const Vectr3F&			// direction
	>;

	/**************************************************************************
	 * bsdf cuda virtual interface
	 *************************************************************************/
	using bsdf_is_interacting_t = Virtual<
		bool, 					// return type
		const Material*			// this
	>;

	using bsdf_matches_type_t = Virtual<
		bool, 					// return type
		const Material*, 		// this
		BxDF::Type				// type
	>;

	using bsdf_evaluate_t = Virtual<
		float, 					// return type
		const Material*, 		// this
		const Geometry&, 		// geometry
		const float, 			// wavelength
		const float, 			// time
		const Vectr3F&, 		// wi
		const Vectr3F&, 		// wo
		const ComplexF&, 		// ior1
		const ComplexF&, 		// ior2
		const BxDF::Mode&, 		// transport mode
		RNG*,					// random number generator
		const BxDF::Type		// restrictions
	>;

	using bsdf_sample_wi_t = Virtual<
		float, 					// return type
		const Material*, 		// this
		const Geometry&, 		// geometry
		const float, 			// wavelength
		const float, 			// time
		const Vectr3F&, 		// wo
		const ComplexF&,		// ior1
		const ComplexF&, 		// ior2
		const BxDF::Mode&, 		// transport mode
		const BxDF::Type, 		// restrictions
		float[2], 				// random
		Vectr3F*, 				// wi
		float*, 				// pdf
		RNG*,					// random number generator
		const BxDF**			// sampled bxdf
	>;

	using bsdf_sample_wi_pdf_t = Virtual<
		float, 					// return type
		const Material*,		// this
		const Geometry&, 		// geometry
		const float,			// wavelength
		const float, 			// time
		const Vectr3F&,			// wo
		const Vectr3F&,			// wi
		const ComplexF&,		// ior1
		const ComplexF&,		// ior2
		const BxDF::Mode&,		// transport mode
		RNG*,					// random number generator
		const BxDF::Type,		// restrictions
		const BxDF*				// sampled bxdf
	>;

	/**************************************************************************
	 * medium cuda virtual interface
	 *************************************************************************/
	using medium_get_priority_t = Virtual<
		Medium::priority_t, 	// return type
		const Material*			// this
	>;

	using medium_is_volumetric_t = Virtual<
		bool, 					// return type
		const Material*			// this
	>;

	using medium_refractive_index_t = Virtual<
		ComplexF,				// return type
		const Material*, 		// this
		const Point3F&, 		// position
		const Point2F&, 		// uv coordinates
		const float,	 		// wavelength
		const float				// time

	>;

	using medium_transmittance_t = Virtual<
		float, 					// return type
		const Material*,		// this
		const Ray&, 			// ray
		RNG*					// random source
	>;


	using medium_sample_t = Virtual<
		bool,					// return type
		const Material*, 		// this
		const Ray&,				// ray
		RNG*,					// random source
		Point3F*,				// sampled position
		float*					// beta
	>;

	using medium_evaluate_phase_t = Virtual<
		float,					// return type
		const Material*,		// this
		const Point3F&,			// position
		const float,			// wavelength
		const float,			// time
		const Vectr3F&, 		// wi
		const Vectr3F&			// wo
	>;

	using medium_sample_wi_t = Virtual<
		float,					// return type
		const Material*, 		// this
		const Point3F&, 		// position
		const float,			// wavelength
		const float,			// time
		const Vectr3F&,			// wo
		float[2],				// random
		Vectr3F*, 				// wi
		float*					// pdf
	>;

	using medium_sample_wi_pdf_t = Virtual<
		float,					// return type
		const Material*,		// this
		const Point3F&, 		// position
		const float, 			// wavelength
		const float, 			// time
		const Vectr3F&, 		// wo
		const Vectr3F&			// wi
	>;

protected:
	const uedf_is_emissive_t uedf_is_emissive_;
	const uedf_emission0_t uedf_emission0_;
	const uedf_emission1_t uedf_emission1_;
	const uedf_emission_t uedf_emission_;
	const uedf_sample_we_t uedf_sample_we_;
	const uedf_sample_we_pdf_t uedf_sample_we_pdf_;
	const bsdf_is_interacting_t bsdf_is_interacting_;
	const bsdf_matches_type_t bsdf_matches_type_;
	const bsdf_evaluate_t bsdf_evaluate_;
	const bsdf_sample_wi_t bsdf_sample_wi_;
	const bsdf_sample_wi_pdf_t bsdf_sample_wi_pdf_;
	const medium_get_priority_t medium_get_priority_;
	const medium_is_volumetric_t medium_is_volumetric_;
	const medium_refractive_index_t medium_refractive_index_;
	const medium_transmittance_t medium_transmittance_;
	const medium_sample_t medium_sample_;
	const medium_evaluate_phase_t medium_evaluate_phase_;
	const medium_sample_wi_t medium_sample_wi_;
	const medium_sample_wi_pdf_t medium_sample_wi_pdf_;

	__host__
	Material(
		const uedf_is_emissive_t& uedf_is_emissive_,
		const uedf_emission0_t& uedf_emission0_,
		const uedf_emission1_t& uedf_emission1_,
		const uedf_emission_t& uedf_emission_,
		const uedf_sample_we_t& uedf_sample_we_,
		const uedf_sample_we_pdf_t& uedf_sample_we_pdf_,
		const bsdf_is_interacting_t& bsdf_is_interacting_,
		const bsdf_matches_type_t& bsdf_matches_type_,
		const bsdf_evaluate_t& bsdf_evaluate_,
		const bsdf_sample_wi_t& bsdf_sample_wi_,
		const bsdf_sample_wi_pdf_t& bsdf_sample_wi_pdf_,
		const medium_get_priority_t& medium_get_priority_,
		const medium_is_volumetric_t& medium_is_volumetric_,
		const medium_refractive_index_t& medium_refractive_index_,
		const medium_transmittance_t& medium_transmittance_,
		const medium_sample_t& medium_sample_,
		const medium_evaluate_phase_t& medium_evaluate_phase_,
		const medium_sample_wi_t& medium_sample_wi_,
		const medium_sample_wi_pdf_t& medium_sample_wi_pdf_
	):
		uedf_is_emissive_(uedf_is_emissive_),
		uedf_emission0_(uedf_emission0_),
		uedf_emission1_(uedf_emission1_),
		uedf_emission_(uedf_emission_),
		uedf_sample_we_(uedf_sample_we_),
		uedf_sample_we_pdf_(uedf_sample_we_pdf_),
		bsdf_is_interacting_(bsdf_is_interacting_),
		bsdf_matches_type_(bsdf_matches_type_),
		bsdf_evaluate_(bsdf_evaluate_),
		bsdf_sample_wi_(bsdf_sample_wi_),
		bsdf_sample_wi_pdf_(bsdf_sample_wi_pdf_),
		medium_get_priority_(medium_get_priority_),
		medium_is_volumetric_(medium_is_volumetric_),
		medium_refractive_index_(medium_refractive_index_),
		medium_transmittance_(medium_transmittance_),
		medium_sample_(medium_sample_),
		medium_evaluate_phase_(medium_evaluate_phase_),
		medium_sample_wi_(medium_sample_wi_),
		medium_sample_wi_pdf_(medium_sample_wi_pdf_) {
	}

public:
	/*!
	 * \brief
	 * Creates a new transformed instance of this material.
	 *
	 * \param[in] transformation The transformation.
	 * \param[in] self A pointer to this material.
	 * \return The transformed material instance.
	 */
	__host__
	 virtual Pointer<const Material> transform(MemoryPool& pool, const Transformation3F& transformation, const Pointer<const Material>& self) const = 0;

	/*!
	 * \brief
	 * Creates a new animated instance of this material.
	 *
	 * \param[in] transformation The animated transformation.
	 * \return The animated material instance.
	 */
	__host__
	virtual Pointer<const Material> transform(MemoryPool& pool, const Pointer<const AnimatedTransformation>& transformation, const Pointer<const Material>& self) const = 0;

	__host__
	virtual ~Material() {
	}

	/**************************************************
	 * UEDF interface
	 **************************************************/
	/*!
	 * \brief
	 * Returns if this material emits light.
	 *
	 * \return If this material emits light.
	 */
	__host__ __device__
	bool isEmissive() const {
		return uedf_is_emissive_(this);
	}

	/*!
	 * \brief
	 * Computes the total emitted spectral power from a position on the
	 * surface.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 *
	 * \return The total emitted spectral power at a position on the surface.
	 */
	__host__ __device__
	float emission0(const Geometry& g, const float wlen, const float time) const {
		return uedf_emission0_(this, g, wlen, time);
	}

	/*!
	 * \brief
	 * Computes the directional differential of the emitted spectral power from
	 * a position on the surface.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \paran[in] w The normalized direction vector in material coordinates.
	 *
	 * \return The directional differential of the emitted spectral power
	 */
	__host__ __device__
	float emission1(const Geometry& g, const float wlen, const float time, const Vectr3F& w) const {
		return uedf_emission1_(this, g, wlen, time, w);
	}

	/*!
	 * \brief
	 * Computes the directional spectral emission from a position on the
	 * surface.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \paran[in] w The normalized direction vector in material coordinates.
	 *
	 * \return The directional spectral emission from a position on the
	 * surface.
	 */
	__host__ __device__
	float emission(const Geometry& g, const float wlen, const float time, const Vectr3F& w) const {
		return uedf_emission_(this, g, wlen, time, w);
	}

	/*!
	 * \brief
	 * Samples an emission direction from a position on the surface and
	 * computes the corresponding PDF with respect to the solid angle measure.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] rnd A pair of randomly generated numbers in [0, 1].
	 * \param[out] w The sampled normalized direction in material coordinates.
	 * \param[out] pdf The PDF with respect to the solid angle measure.
	 * \param[out] sampled The actually sampled UEDC.
	 *
	 * \return The directional differential of spectral emitted power.
	 */
	__host__ __device__
	float sampleWe(const Geometry& g, const float wlen, const float time, float rnd[2], Vectr3F* w, float* pdf, const UEDC** sampled = nullptr) const {
		return uedf_sample_we_(this, g, wlen, time, rnd, w, pdf, sampled);
	}

	/*!
	 * \brief
	 * Computes the PDF of sampling an emission direction from a point on the
	 * surface with respect to the solid angle measure.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[int] time The normalized time in [0, 1].
	 * \param[in] w The normalized emission direction in material coordinates.
	 *
	 * \return The PDF with respect to the solid angle measure.
	 */
	__host__ __device__
	float sampleWePDF(const Geometry& g, const float wlen, const float time, const Vectr3F& w) const {
		return uedf_sample_we_pdf_(this, g, wlen, time, w);
	}

	/**************************************************
	 * BSDF interface
	 **************************************************/
	/*!
	 * \brief
	 * Returns if the material's BSDF interacts with light.
	 *
	 * \return If the material's BSDF interacts with light.
	 */
	__host__ __device__
	bool hasOpticallyInteractingBSDF() const {
		return bsdf_is_interacting_(this);
	}

	/*!
	 * \brief
	 * Return if the material's BSDF matches the type.
	 *
	 * \return If the material's BSDF matches the type.
	 */
	__host__ __device__
	bool matchesBSDF(BxDF::Type t) const {
		return bsdf_matches_type_(this, t);
	}

	/*!
	 * \brief
	 * Evalues the material's BSDF.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wi The normalized incoming direction.
	 * \param[in] wo The normalized outgoing direction.
	 * \param[in] ior1 The refractive index on the same side of the outgoing
	 * direction.
	 * \param[in] ior2 The refractive index on the opposite side of the
	 * outgoing direction.
	 * \param[in] mode The transport mode, RADIANCE or IMPORTANCE.
	 * \param[in] restrictions The restrictions on the BxDF types to be
	 * considered.
	 *
	 * \return The material's BSDF value.
	 */
	__host__ __device__
	float evaluateBSDF(const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const BxDF::Mode& mode, RNG* rng, const BxDF::Type restrictions = BxDF::Type(0)) const {
		return bsdf_evaluate_(this, g, wlen, time, wi, wo, ior1, ior2, mode, rng, restrictions);
	}

	/*!
	 * \brief
	 * Samples an incoming direction from the material's BSDF.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wo The normalized outgoing direction.
	 * \param[in] ior1 The refractive index on the same side of the outgoing
	 * direction.
	 * \param[in] ior2 The refractive index on the opposite side of the
	 * outgoing direction.
	 * \param[in] mode The transport mode.
	 * \param[in] restrictions The restrictions on the BxDF types to be
	 * considered.
	 * \param[in] rnd Two uniformly distributed random numbers in [0, 1].
	 * \param[out] wi The sampled normalized incoming direction.
	 * \param[out] pdf The PDF of sampling `wi` with respect to the solid angle
	 * measure from the geometry's normal vector.
	 * \param[out] sampled If not null holds the actually sampled BxDF.
	 *
	 * \return The material's BSDF value.
	 */
	__host__ __device__
	float sampleBSDFWi(const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const BxDF::Mode& mode, const BxDF::Type restrictions, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng, const BxDF** sampled = nullptr) const {
		return bsdf_sample_wi_(this, g, wlen, time, wo, ior1, ior2, mode, restrictions, rnd, wi, pdf, rng, sampled);
	}

	/*!
	 * \brief
	 * Computes the PDF of sampling the incoming direction `wi` from the
	 * material's BSDF given the outgoing direction `wo` with respect to the
	 * solid angle measue from the geometry's normal vector.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wo The normalized outgoing direction.
	 * \param[in] wi The normalized incoming direction.
	 * \param[in] ior1 The refractive index on the same side of the outgoing
	 * direction.
	 * \param[in] ior2 The refractive index on the opposite side of the
	 * outgoing direction.
	 * \param[in] mode The transport mode.
	 * \param[in] restrictions The restrictions on the BxDF types to be
	 * considered.
	 * \param[out] sampled The actually sampled BxDF.
	 *
	 * \return The PDF of sampling `wi` from the material's BSDF with respect
	 * to the solid angle measure from the geometry's normal vector.
	 */
	__host__ __device__
	float sampleBSDFWiPDF(const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const BxDF::Mode& mode, RNG* rng, const BxDF::Type restrictions = BxDF::Type(0), const BxDF* sampled = nullptr) const {
		return bsdf_sample_wi_pdf_(this, g, wlen, time, wo, wi, ior1, ior2, mode, rng, restrictions, sampled);
	}

	/**************************************************
	 * Medium interface
	 **************************************************/
	/*!
	 * \brief
	 * Returns the medium's priority.
	 *
	 * The priority is used to decide wich medium should be considered when
	 * traversing a volume with different overlapping media. The details can be
	 * found in "Simple Nested Dielectrics in Ray Traced Images" by Charles M.
	 * Schmidt and Brian Budge.
	 *
	 * \return The priority.
	 */
	__host__  __device__
	Medium::priority_t prioriry() const {
		return medium_get_priority_(this);
	}

	/*!
	 * \brief
	 * Returns if the material's medium is actually enclosed by a volume.
	 *
	 * Some BSDF depends on the complex refractive index defined by the
	 * material's medium, for this reason when using these BSDFs it is
	 * necessary to specify a medium. When the corresponding primitive's shape
	 * is not actually volumetric (like a triangle or any open shape) the
	 * medium should be tagged as "non-volumetric" so that the rays are
	 * never considered to be inside the medium.
	 *
	 * \return If this medium is actually enclosed by a volume.
	 */
	__host__ __device__
	bool hasVolumetricMedium() const {
		return medium_is_volumetric_(this);
	}

	/*!
	 * \brief
	 * Computes the complex refractive index at a position within the medium.
	 *
	 * \param[in] p The position in material's coordinate.
	 * \param[in] uv The uv coordinates (when on the surface).
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 *
	 * \return The complex refractive index.
	 */
	__host__  __device__
	 ComplexF refractiveIndex(const Point3F& p, const Point2F& uv, const float wlen, const float time) const {
		return medium_refractive_index_(this, p, uv, wlen, time);
	}

	/*!
	 * \brief
	 * Estimates the transmittance along the ray's extent.
	 *
	 * \param[in] ray The ray in material's coordinates.
	 * \param[in] rng1D One dimensional uniform random number generator.
	 *
	 * \return The fraction of light that will be transmitted through the
	 * traversed medium.
	 */
	__host__ __device__
	float transmittance(const Ray& ray, RNG* rng1D) const {
		return medium_transmittance_(this, ray, rng1D);
	}

	/*!
	 * \brief
	 * Randomly samples a interaction position within then medium along
	 * the ray's extent.
	 *
	 * \param[in] Ray The ray in material's coordinates.
	 * \param[in] rng1D One dimensional uniform random number generator.
	 * \param[out] p The sampled position within the medium.
	 * \param[out] beta The transmittance from the ray's origin to the sampled
	 * position divided by the sampling pdf.
	 *
	 * \return If a point was sampled within the medium. Returning false means
	 * that an interaction did not occur inside the medium and the position `p`
	 * is undefined.
	 */
	__host__ __device__
	bool sampleMedium(const Ray& ray, RNG* rng1D, Point3F* p, float* beta) const {
		return medium_sample_(this, ray, rng1D, p, beta);
	}

	/*!
	 * \brief
	 * Evaluates the material's phase function.
	 *
	 * \param[in] p The position within the medium in material's coordinates.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wi The normalized light incoming direction.
	 * \param[in] wo The normalized light outgoing direction.
	 *
	 * \return The value of the material's phase function.
	 */
	__host__ __device__
	float evaluatePhase(const Point3F& p, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo) const {
		return medium_evaluate_phase_(this, p, wlen, time, wi, wo);
	}

	/*!
	 * \brief
	 * Samples an incoming light direction from the material's phase function.
	 *
	 * \param[in] p The position within the medium.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wo The normalized light outgoing direction.
	 * \param[in] rnd A pair of uniformly sampled numbers in [0, 1].
	 * \param[out] wi The sampled normalized light incoming direction.
	 * \param[out] pdf The PDF of sampling `wi` with respect to the solid
	 * angle measure.
	 *
	 * \return The material's phase function value.
	 */
	__host__ __device__
	float samplePhaseWi(const Point3F& p, const float wlen, const float time, const Vectr3F& wo, float rnd[2], Vectr3F* wi, float* pdf) const {
		return medium_sample_wi_(this, p, wlen, time, wo, rnd, wi, pdf);
	}

	/*!
	 * \brief
	 * Computes the PDF of sampling an incoming direction from the phase function.
	 *
	 * \param[in] p The position within the medium in material's coordinates.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wo The normalized light outgoing direction.
	 * \param[in] wi The normalized light incoming direction.
	 *
	 * \return The PDF with respect to the solid angle measure.
	 */
	__host__ __device__
	float samplePhaseWiPDF(const Point3F& p, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi) const {
		return medium_sample_wi_pdf_(this, p, wlen, time, wo, wi);
	}
};

}

#endif /* MATERIAL_CUH_ */

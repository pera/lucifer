#include "AnimatedMaterial.cuh"
#include "TransformedMaterial.cuh"

namespace lucifer {

__host__
Pointer<const Material> TransformedMaterial::transform(MemoryPool& pool, const Transformation3F& transformation, const Pointer<const Material>& self) const {
	assert(this == self.get());
	return Pointer<const Material>(TransformedMaterial::build(pool, transformation * materialToWorld, material));
}

__host__
Pointer<const Material> TransformedMaterial::transform(MemoryPool& pool, const Pointer<const AnimatedTransformation>& transformation, const Pointer<const Material>& self) const {
	assert(this == self.get());
	return Pointer<const Material>(AnimatedMaterial::build(pool, transformation, self));
}

}

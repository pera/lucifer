#ifndef INTERIORLISTUTILS_CUH_
#define INTERIORLISTUTILS_CUH_

#include "Material.cuh"
#include "lucifer/data/List.cuh"

namespace lucifer {

using InteriorList = List<const Material*, 4>;

template<typename T=void> __host__ __device__
const Material* highestPriority(const InteriorList& list) {
	const Material* material = nullptr;
	for (uint8_t i = 0; i < list.size(); i++) {
		if (material == nullptr || material->prioriry() < list[i]->prioriry()) {
			material = list[i];
		}
	}
	return material;
}

template<typename T=void> __host__ __device__
const Material* highestPriority(const InteriorList& list, const uint8_t limit) {
	const Material* material = nullptr;
	for (uint8_t i = 0; i < list.size(); i++) {
		if (list[i]->prioriry() < limit && (material == nullptr || material->prioriry() < list[i]->prioriry())) {
			material = list[i];
		}
	}
	return material;
}

}

#endif /* INTERIORLISTUTILS_CUH_ */

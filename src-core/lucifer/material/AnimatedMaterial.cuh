#ifndef ANIMATEDMATERIAL_CUH_
#define ANIMATEDMATERIAL_CUH_

#include "Material.cuh"
#include "TransformedMaterial.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/math/geometry/AnimatedTransformation.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * Animated Material
 *
 * \author
 * Bruno Pera.
 */
class AnimatedMaterial: public Material {
private:
	friend class TypedPool<AnimatedMaterial>;
	const Pointer<const AnimatedTransformation> materialToWorld;
	const Pointer<const Material> material;

	__host__ __device__
	static const AnimatedMaterial* cast(const Material* material) {
		return static_cast<const AnimatedMaterial*>(material);
	}

	__host__ __device__
	static bool uedf_is_emissive_(const Material* this_) {
		return cast(this_)->material->isEmissive();
	}

	__host__ __device__
	static float uedf_emission0_(const Material* this_, const Geometry& g, const float wlen, const float time) {
		auto materialToWorld = cast(this_)->materialToWorld->atTime(time);
		return cast(this_)->material->emission0(materialToWorld.inverse() * g, wlen, time);
	}

	__host__ __device__
	static float uedf_emission1_(const Material* this_, const Geometry& g, const float wlen, const float time, const Vectr3F& w) {
		auto materialToWorld = cast(this_)->materialToWorld->atTime(time);
		auto worldToMaterial = materialToWorld.inverse();
		return cast(this_)->material->emission1(worldToMaterial * g, wlen, time, worldToMaterial * w);
	}

	__host__ __device__
	static float uedf_emission_(const Material* this_, const Geometry& g, const float wlen, const float time, const Vectr3F& w) {
		auto materialToWorld = cast(this_)->materialToWorld->atTime(time);
		auto worldToMaterial = materialToWorld.inverse();
		return cast(this_)->material->emission (worldToMaterial * g, wlen, time, worldToMaterial * w);
	}

	__host__ __device__
	static float uedf_sample_we_(const Material* this_, const Geometry& g, const float wlen, const float time, float rnd[2], Vectr3F* w, float* pdf, const UEDC** sampled) {
		// transform input parameters to local coordinates
		auto materialToWorld = cast(this_)->materialToWorld->atTime(time);
		auto worldToMaterial = materialToWorld.inverse();
		auto value = cast(this_)->material->sampleWe(worldToMaterial * g, wlen, time, rnd, w, pdf);
		// transform output parameters to world coordinates
		*w = materialToWorld * (*w);
		// ok
		return value;
	}

	__host__ __device__
	static float uedf_sample_we_pdf_(const Material* this_, const Geometry& g, const float wlen, const float time, const Vectr3F& w) {
		auto materialToWorld = cast(this_)->materialToWorld->atTime(time);
		auto worldToMaterial = materialToWorld.inverse();
		return cast(this_)->material->sampleWePDF(worldToMaterial * g, wlen, time, worldToMaterial * w);
	}

	__host__ __device__
	static bool bsdf_is_interacting_(const Material* this_) {
		return cast(this_)->material->hasOpticallyInteractingBSDF();
	}

	__host__ __device__
	static bool bsdf_matches_type_(const Material* this_, BxDF::Type t) {
		return cast(this_)->material->matchesBSDF(t);
	}

	__host__ __device__
	static float bsdf_evaluate_(const Material* this_, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const BxDF::Mode& mode, RNG* rng, const BxDF::Type restrictions) {
		auto materialToWorld = cast(this_)->materialToWorld->atTime(time);
		auto worldToMaterial = materialToWorld.inverse();
		Geometry localG = worldToMaterial *  g;
		Vectr3F localWi = worldToMaterial * wi;
		Vectr3F localWo = worldToMaterial * wo;
		return cast(this_)->material->evaluateBSDF(localG, wlen, time, localWi, localWo, ior1, ior2, mode, rng, restrictions);
	}

	__host__ __device__
	static float bsdf_sample_wi_(const Material* this_, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const BxDF::Mode& mode, const BxDF::Type restrictions, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng, const BxDF** sampled) {
		// transform input parameters to local coordinates
		auto materialToWorld = cast(this_)->materialToWorld->atTime(time);
		auto worldToMaterial = materialToWorld.inverse();
		Geometry localG = worldToMaterial *  g;
		Vectr3F localWo = worldToMaterial * wo;
		float bsdf = cast(this_)->material->sampleBSDFWi(localG, wlen, time, localWo, ior1, ior2, mode, restrictions, rnd, wi, pdf, rng, sampled);
		// transform output parameters to world coordintes
		*wi = materialToWorld * (*wi);
		// ok
		return bsdf;
	}

	__host__ __device__
	static float bsdf_sample_wi_pdf_(const Material* this_, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const BxDF::Mode& mode, RNG* rng, const BxDF::Type restrictions, const BxDF* sampled) {
		auto materialToWorld = cast(this_)->materialToWorld->atTime(time);
		auto worldToMaterial = materialToWorld.inverse();
		Geometry localG = worldToMaterial *  g;
		Vectr3F localWi = worldToMaterial * wi;
		Vectr3F localWo = worldToMaterial * wo;
		return cast(this_)->material->sampleBSDFWiPDF(localG, wlen, time, localWo, localWi, ior1, ior2, mode, rng, restrictions, sampled);
	}

	__host__ __device__
	static Medium::priority_t medium_get_priority_(const Material* this_) {
		return cast(this_)->material->prioriry();
	}

	__host__ __device__
	static bool medium_is_volumetric_(const Material* this_) {
		return cast(this_)->material->hasVolumetricMedium();
	}

	__host__ __device__
	static ComplexF medium_refractive_index_(const Material* this_, const Point3F& p, const Point2F& uv, const float wlen, const float time) {
		auto materialToWorld = cast(this_)->materialToWorld->atTime(time);
		auto worldToMaterial = materialToWorld.inverse();
		return cast(this_)->material->refractiveIndex(worldToMaterial * p, uv, wlen, time);
	}

	__host__ __device__
	static float medium_transmittance_(const Material* this_, const Ray& ray, RNG* rng1D) {
		auto materialToWorld = cast(this_)->materialToWorld->atTime(ray.t);
		auto worldToMaterial = materialToWorld.inverse();
		return cast(this_)->material->transmittance(worldToMaterial * ray, rng1D);
	}

	__host__ __device__
	static bool medium_sample_(const Material* this_, const Ray& ray, RNG* rng1D, Point3F* p, float* beta) {
		auto materialToWorld = cast(this_)->materialToWorld->atTime(ray.t);
		auto worldToMaterial = materialToWorld.inverse();
		bool sampled = cast(this_)->material->sampleMedium(worldToMaterial * ray, rng1D, p, beta);
		if (sampled) {
			*p = materialToWorld * (*p);
		}
		return sampled;
	}

	__host__ __device__
	static float medium_evaluate_phase_(const Material* this_, const Point3F& p, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo) {
		auto materialToWorld = cast(this_)->materialToWorld->atTime(time);
		auto worldToMaterial = materialToWorld.inverse();
		return cast(this_)->material->evaluatePhase(worldToMaterial * p, wlen, time, worldToMaterial * wi, worldToMaterial * wo);
	}

	__host__ __device__
	static float medium_sample_wi_(const Material* this_, const Point3F& p, const float wlen, const float time, const Vectr3F& wo, float rnd[2], Vectr3F* wi, float* pdf) {
		auto materialToWorld = cast(this_)->materialToWorld->atTime(time);
		auto worldToMaterial = materialToWorld.inverse();
		float value = cast(this_)->material->samplePhaseWi(worldToMaterial * p, wlen, time, worldToMaterial * wo, rnd, wi, pdf);
		if (*pdf > 0.f) {
			(*wi) = materialToWorld * (*wi);
		}
		return value;
	}

	__host__ __device__
	static float medium_sample_wi_pdf_(const Material* this_, const Point3F& p, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi) {
		auto materialToWorld = cast(this_)->materialToWorld->atTime(time);
		auto worldToMaterial = materialToWorld.inverse();
		return cast(this_)->material->samplePhaseWiPDF(worldToMaterial * p, wlen, time, worldToMaterial * wo, worldToMaterial * wi);
	}

	__host__
	AnimatedMaterial(
		const Pointer<const AnimatedTransformation>& materialToWorld,
		const Pointer<const Material>& material,
		const uedf_is_emissive_t& uedf_is_emissive_,
		const uedf_emission0_t& uedf_emission0_,
		const uedf_emission1_t& uedf_emission1_,
		const uedf_emission_t& uedf_emission_,
		const uedf_sample_we_t& uedf_sample_we_,
		const uedf_sample_we_pdf_t& uedf_sample_we_pdf_,
		const bsdf_is_interacting_t& bsdf_is_interacting_,
		const bsdf_matches_type_t& bsdf_matches_type_,
		const bsdf_evaluate_t& bsdf_evaluate_,
		const bsdf_sample_wi_t& bsdf_sample_wi_,
		const bsdf_sample_wi_pdf_t& bsdf_sample_wi_pdf_,
		const medium_get_priority_t& medium_get_priority_,
		const medium_is_volumetric_t& medium_is_volumetric_,
		const medium_refractive_index_t& medium_refractive_index_,
		const medium_transmittance_t& medium_transmittance_,
		const medium_sample_t& medium_sample_,
		const medium_evaluate_phase_t& medium_evaluate_phase_,
		const medium_sample_wi_t& medium_sample_wi_,
		const medium_sample_wi_pdf_t& medium_sample_wi_pdf_
	):
		Material(
			uedf_is_emissive_,
			uedf_emission0_,
			uedf_emission1_,
			uedf_emission_,
			uedf_sample_we_,
			uedf_sample_we_pdf_,
			bsdf_is_interacting_,
			bsdf_matches_type_,
			bsdf_evaluate_,
			bsdf_sample_wi_,
			bsdf_sample_wi_pdf_,
			medium_get_priority_,
			medium_is_volumetric_,
			medium_refractive_index_,
			medium_transmittance_,
			medium_sample_,
			medium_evaluate_phase_,
			medium_sample_wi_,
			medium_sample_wi_pdf_
		),
		materialToWorld(materialToWorld),
		material(material) {
	}

public:
	friend class TypedPool<AnimatedMaterial>;

	/*!
	 * Creates a new animated material.
	 *
	 * \param[in] materialToWorld The material to world transformation.
	 * \param[in] material The original material to be animated.
	 */
	__host__
	static Pointer<AnimatedMaterial> build(
		MemoryPool& pool,
		const Pointer<const AnimatedTransformation>& materialToWorld,
		const Pointer<const Material>& material)
	{
		return Pointer<AnimatedMaterial>::make(
			pool, materialToWorld, material,
			uedf_is_emissive_t(virtualize(uedf_is_emissive_)),
			uedf_emission0_t(uedf_emission0_, [] __device__ () {return (void*)uedf_emission0_;}),
			uedf_emission1_t(virtualize(uedf_emission1_)),
			uedf_emission_t(virtualize(uedf_emission_)),
			uedf_sample_we_t(virtualize(uedf_sample_we_)),
			uedf_sample_we_pdf_t(virtualize(uedf_sample_we_pdf_)),
			bsdf_is_interacting_t(virtualize(bsdf_is_interacting_)),
			bsdf_matches_type_t(virtualize(bsdf_matches_type_)),
			bsdf_evaluate_t(virtualize(bsdf_evaluate_)),
			bsdf_sample_wi_t(virtualize(bsdf_sample_wi_)),
			bsdf_sample_wi_pdf_t(virtualize(bsdf_sample_wi_pdf_)),
			medium_get_priority_t(virtualize(medium_get_priority_)),
			medium_is_volumetric_t(virtualize(medium_is_volumetric_)),
			medium_refractive_index_t(virtualize(medium_refractive_index_)),
			medium_transmittance_t(virtualize(medium_transmittance_)),
			medium_sample_t(virtualize(medium_sample_)),
			medium_evaluate_phase_t(virtualize(medium_evaluate_phase_)),
			medium_sample_wi_t(virtualize(medium_sample_wi_)),
			medium_sample_wi_pdf_t(virtualize(medium_sample_wi_pdf_))
		);
	}

	__host__
	Pointer<const Material> transform(MemoryPool& pool, const Transformation3F& transformation, const Pointer<const Material>& self) const override;

	__host__
	Pointer<const Material> transform(MemoryPool& pool, const Pointer<const AnimatedTransformation>& transformation, const Pointer<const Material>& self) const override;
};

}

#endif /* ANIMATEDMATERIAL_CUH_ */

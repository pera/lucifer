#ifndef TRANSFORMEDMATERIAL_CUH_
#define TRANSFORMEDMATERIAL_CUH_

#include "Material.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * Transformed material.
 *
 * \author
 * Bruno Pera.
 */
class TransformedMaterial: public Material {
private:
	friend class TypedPool<TransformedMaterial>;
	const Transformation3F materialToWorld;
	const Pointer<const Material> material;

	__host__ __device__
	static const TransformedMaterial* cast(const Material* material) {
		return static_cast<const TransformedMaterial*>(material);
	}

	__host__ __device__
	static bool uedf_is_emissive_(const Material* this_) {
		return cast(this_)->material->isEmissive();
	}

	__host__ __device__
	static float uedf_emission0_(const Material* this_, const Geometry& g, const float wlen, const float time) {
		return cast(this_)->material->emission0(cast(this_)->materialToWorld.inverse() * g, wlen, time);
	}

	__host__ __device__
	static float uedf_emission1_(const Material* this_, const Geometry& g, const float wlen, const float time, const Vectr3F& w) {
		return cast(this_)->material->emission1(cast(this_)->materialToWorld.inverse() * g, wlen, time, cast(this_)->materialToWorld.inverse() * w);
	}

	__host__ __device__
	static float uedf_emission_(const Material* this_, const Geometry& g, const float wlen, const float time, const Vectr3F& w) {
		return cast(this_)->material->emission (cast(this_)->materialToWorld.inverse() * g, wlen, time, cast(this_)->materialToWorld.inverse() * w);
	}

	__host__ __device__
	static float uedf_sample_we_(const Material* this_, const Geometry& g, const float wlen, const float time, float rnd[2], Vectr3F* w, float* pdf, const UEDC** sampled) {
		float value = cast(this_)->material->sampleWe(cast(this_)->materialToWorld.inverse() * g, wlen, time, rnd, w, pdf);
		*w = cast(this_)->materialToWorld * (*w);
		return value;
	}

	__host__ __device__
	static float uedf_sample_we_pdf_(const Material* this_, const Geometry& g, const float wlen, const float time, const Vectr3F& w) {
		return cast(this_)->material->sampleWePDF(cast(this_)->materialToWorld.inverse() * g, wlen, time, cast(this_)->materialToWorld.inverse() * w);
	}

	__host__ __device__
	static bool bsdf_is_interacting_(const Material* this_) {
		return cast(this_)->material->hasOpticallyInteractingBSDF();
	}

	__host__ __device__
	static bool bsdf_matches_type_(const Material* this_, BxDF::Type t) {
		return cast(this_)->material->matchesBSDF(t);
	}

	__host__ __device__
	static float bsdf_evaluate_(const Material* this_, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const BxDF::Mode& mode, RNG* rng, const BxDF::Type restrictions) {
		Geometry localG = cast(this_)->materialToWorld.inverse() *  g;
		Vectr3F localWi = cast(this_)->materialToWorld.inverse() * wi;
		Vectr3F localWo = cast(this_)->materialToWorld.inverse() * wo;
		return cast(this_)->material->evaluateBSDF(localG, wlen, time, localWi, localWo, ior1, ior2, mode, rng, restrictions);
	}

	__host__ __device__
	static float bsdf_sample_wi_(const Material* this_, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const BxDF::Mode& mode, const BxDF::Type restrictions, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng, const BxDF** sampled) {
		Geometry localG = cast(this_)->materialToWorld.inverse() *  g;
		Vectr3F localWo = cast(this_)->materialToWorld.inverse() * wo;
		float bsdf = cast(this_)->material->sampleBSDFWi(localG, wlen, time, localWo, ior1, ior2, mode, restrictions, rnd, wi, pdf, rng, sampled);
		*wi = cast(this_)->materialToWorld * (*wi);
		return bsdf;
	}

	__host__ __device__
	static float bsdf_sample_wi_pdf_(const Material* this_, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const BxDF::Mode& mode, RNG* rng, const BxDF::Type restrictions, const BxDF* sampled) {
		Geometry localG = cast(this_)->materialToWorld.inverse() *  g;
		Vectr3F localWi = cast(this_)->materialToWorld.inverse() * wi;
		Vectr3F localWo = cast(this_)->materialToWorld.inverse() * wo;
		return cast(this_)->material->sampleBSDFWiPDF(localG, wlen, time, localWo, localWi, ior1, ior2, mode, rng, restrictions, sampled);
	}

	__host__ __device__
	static Medium::priority_t medium_get_priority_(const Material* this_) {
		return cast(this_)->material->prioriry();
	}

	__host__ __device__
	static bool medium_is_volumetric_(const Material* this_) {
		return cast(this_)->material->hasVolumetricMedium();
	}

	__host__ __device__
	static ComplexF medium_refractive_index_(const Material* this_, const Point3F& p, const Point2F& uv, const float wlen, const float time) {
		return cast(this_)->material->refractiveIndex(cast(this_)->materialToWorld.inverse() * p, uv, wlen, time);
	}

	__host__ __device__
	static float medium_transmittance_(const Material* this_, const Ray& ray, RNG* rng1D) {
		return cast(this_)->material->transmittance(cast(this_)->materialToWorld.inverse() * ray, rng1D);
	}

	__host__ __device__
	static bool medium_sample_(const Material* this_, const Ray& ray, RNG* rng1D, Point3F* p, float* beta) {
		bool sampled = cast(this_)->material->sampleMedium(cast(this_)->materialToWorld.inverse() * ray, rng1D, p, beta);
		if (sampled) *p = cast(this_)->materialToWorld * (*p);
		return sampled;
	}

	__host__ __device__
	static float medium_evaluate_phase_(const Material* this_, const Point3F& p, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo) {
		return cast(this_)->material->evaluatePhase(cast(this_)->materialToWorld.inverse() * p, wlen, time, cast(this_)->materialToWorld.inverse() * wi, cast(this_)->materialToWorld.inverse() * wo);
	}

	__host__ __device__
	static float medium_sample_wi_(const Material* this_, const Point3F& p, const float wlen, const float time, const Vectr3F& wo, float rnd[2], Vectr3F* wi, float* pdf) {
		float value = cast(this_)->material->samplePhaseWi(cast(this_)->materialToWorld.inverse() * p, wlen, time, cast(this_)->materialToWorld.inverse() * wo, rnd, wi, pdf);
		if (*pdf > 0.f) (*wi) = cast(this_)->materialToWorld * (*wi);
		return value;
	}

	__host__ __device__
	static float medium_sample_wi_pdf_(const Material* this_, const Point3F& p, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi) {
		return cast(this_)->material->samplePhaseWiPDF(cast(this_)->materialToWorld.inverse() * p, wlen, time, cast(this_)->materialToWorld.inverse() * wo, cast(this_)->materialToWorld.inverse() * wi);
	}

	__host__
	TransformedMaterial(
		const Transformation3F& materialToWorld,
		const Pointer<const Material>& material,
		const uedf_is_emissive_t& uedf_is_emissive_,
		const uedf_emission0_t& uedf_emission0_,
		const uedf_emission1_t& uedf_emission1_,
		const uedf_emission_t& uedf_emission_,
		const uedf_sample_we_t& uedf_sample_we_,
		const uedf_sample_we_pdf_t& uedf_sample_we_pdf_,
		const bsdf_is_interacting_t& bsdf_is_interacting_,
		const bsdf_matches_type_t& bsdf_matches_type_,
		const bsdf_evaluate_t& bsdf_evaluate_,
		const bsdf_sample_wi_t& bsdf_sample_wi_,
		const bsdf_sample_wi_pdf_t& bsdf_sample_wi_pdf_,
		const medium_get_priority_t& medium_get_priority_,
		const medium_is_volumetric_t& medium_is_volumetric_,
		const medium_refractive_index_t& medium_refractive_index_,
		const medium_transmittance_t& medium_transmittance_,
		const medium_sample_t& medium_sample_,
		const medium_evaluate_phase_t& medium_evaluate_phase_,
		const medium_sample_wi_t& medium_sample_wi_,
		const medium_sample_wi_pdf_t& medium_sample_wi_pdf_
	):
		Material(
			uedf_is_emissive_,
			uedf_emission0_,
			uedf_emission1_,
			uedf_emission_,
			uedf_sample_we_,
			uedf_sample_we_pdf_,
			bsdf_is_interacting_,
			bsdf_matches_type_,
			bsdf_evaluate_,
			bsdf_sample_wi_,
			bsdf_sample_wi_pdf_,
			medium_get_priority_,
			medium_is_volumetric_,
			medium_refractive_index_,
			medium_transmittance_,
			medium_sample_,
			medium_evaluate_phase_,
			medium_sample_wi_,
			medium_sample_wi_pdf_
		),
		materialToWorld(materialToWorld),
		material(material) {
	}

public:
	friend class TypedPool<TransformedMaterial>;

	/*!
	 * \brief
	 * Creates a new transformed material.
	 *
	 * \param[in] materialToWorld The material to world transformtion.
	 * \param[in] material The original material to be transformed.
	 */
	__host__
	static Pointer<TransformedMaterial> build(MemoryPool& pool, const Transformation3F& materialToWorld, const Pointer<const Material>& material) {
		return Pointer<TransformedMaterial>::make(
			pool, materialToWorld, material,
			uedf_is_emissive_t(virtualize(uedf_is_emissive_)),
			uedf_emission0_t(uedf_emission0_, [] __device__ () {return (void*)uedf_emission0_;}),
			uedf_emission1_t(virtualize(uedf_emission1_)),
			uedf_emission_t(virtualize(uedf_emission_)),
			uedf_sample_we_t(virtualize(uedf_sample_we_)),
			uedf_sample_we_pdf_t(virtualize(uedf_sample_we_pdf_)),
			bsdf_is_interacting_t(virtualize(bsdf_is_interacting_)),
			bsdf_matches_type_t(virtualize(bsdf_matches_type_)),
			bsdf_evaluate_t(virtualize(bsdf_evaluate_)),
			bsdf_sample_wi_t(virtualize(bsdf_sample_wi_)),
			bsdf_sample_wi_pdf_t(virtualize(bsdf_sample_wi_pdf_)),
			medium_get_priority_t(virtualize(medium_get_priority_)),
			medium_is_volumetric_t(virtualize(medium_is_volumetric_)),
			medium_refractive_index_t(virtualize(medium_refractive_index_)),
			medium_transmittance_t(virtualize(medium_transmittance_)),
			medium_sample_t(virtualize(medium_sample_)),
			medium_evaluate_phase_t(virtualize(medium_evaluate_phase_)),
			medium_sample_wi_t(virtualize(medium_sample_wi_)),
			medium_sample_wi_pdf_t(virtualize(medium_sample_wi_pdf_))
		);
	}

	__host__
	Pointer<const Material> transform(MemoryPool& pool, const Transformation3F& transformation, const Pointer<const Material>& self) const override;

	__host__
	Pointer<const Material> transform(MemoryPool& pool, const Pointer<const AnimatedTransformation>& transformation, const Pointer<const Material>& self) const override;

};

}

#endif /* TRANSFORMEDMATERIAL_CUH_ */

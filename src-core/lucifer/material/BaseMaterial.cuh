#ifndef BASEMATERIAL_CUH_
#define BASEMATERIAL_CUH_

#include "Material.cuh"
#include "AnimatedMaterial.cuh"
#include "TransformedMaterial.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * Base Material.
 *
 * The base material type defined directly from the light-interacting
 * entities such as UEDF, BSDF and Medium.
 *
 * \author
 * Bruno Pera.
 */
class BaseMaterial: public Material {
private:
	const Pointer<const UEDF> uedf;
	const Pointer<const BSDF> bsdf;
	const Pointer<const Medium> medium;

	__host__ __device__
	static const BaseMaterial* cast(const Material* material) {
		return static_cast<const BaseMaterial*>(material);
	}

	__host__ __device__
	static bool uedf_is_emissive_(const Material* material) {
		return !cast(material)->uedf.isNull();
	}

	__host__ __device__
	static float uedf_emission0_(const Material* material, const Geometry& g, const float wlen, const float time) {
		return cast(material)->uedf.isNull() ? 0.f : cast(material)->uedf->emission0(g, wlen, time);
	}

	__host__ __device__
	static float uedf_emission1_(const Material* material, const Geometry& g, const float wlen, const float time, const Vectr3F& w) {
		return cast(material)->uedf.isNull() ? 0.f : cast(material)->uedf->emission1(g, wlen, time, w);
	}

	__host__ __device__
	static float uedf_emission_(const Material* material, const Geometry& g, const float wlen, const float time, const Vectr3F& w) {
		return cast(material)->emission0(g, wlen, time) * cast(material)->emission1(g, wlen, time, w);
	}

	__host__ __device__
	static float uedf_sample_we_(const Material* material, const Geometry& g, const float wlen, const float time, float rnd[2], Vectr3F* w, float* pdf, const UEDC** sampled) {
		if (!cast(material)->uedf.isNull()) {
			return cast(material)->uedf->sampleWe(g, wlen, time, rnd, w, pdf, sampled);
		}
		if (pdf != nullptr) {
			*pdf = 0.f;
		}
		return 0.f;
	}

	__host__ __device__
	static float uedf_sample_we_pdf_(const Material* material, const Geometry& g, const float wlen, const float time, const Vectr3F& w) {
		return cast(material)->uedf.isNull() ? 0.f : cast(material)->uedf->sampleWePDF(g, wlen, time, w);
	}

	__host__ __device__
	static bool bsdf_is_interacting_(const Material* material) {
		return !cast(material)->bsdf.isNull() && cast(material)->bsdf->isOpticallyInteracting();
	}

	__host__ __device__
	static bool bsdf_matches_type_(const Material* material, BxDF::Type t) {
		return cast(material)->bsdf.isNull() ? false : cast(material)->bsdf->matches(t);
	}

	__host__ __device__
	static float bsdf_evaluate_(const Material* material, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const BxDF::Mode& mode, RNG* rng, const BxDF::Type restrictions) {
		return cast(material)->bsdf.isNull() ? 0.f : cast(material)->bsdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, mode, rng, restrictions);
	}

	__host__ __device__
	static float bsdf_sample_wi_(const Material* material, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const BxDF::Mode& mode, const BxDF::Type restrictions, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng, const BxDF** sampled) {
		*pdf = 0.f;
		return cast(material)->bsdf.isNull() ? 0.f : cast(material)->bsdf->sampleWi(g, wlen, time, wo, ior1, ior2, mode, restrictions, rnd, wi, pdf, rng, sampled);
	}

	__host__ __device__
	static float bsdf_sample_wi_pdf_(const Material* material, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const BxDF::Mode& mode, RNG* rng, const BxDF::Type restrictions, const BxDF* sampled) {
		return cast(material)->bsdf.isNull() ? 0.f : cast(material)->bsdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, mode, rng, restrictions, sampled);
	}

	__host__ __device__
	static Medium::priority_t medium_get_priority_(const Material* material) {
		return cast(material)->medium.isNull() ? 0 : cast(material)->medium->priority();
	}

	__host__ __device__
	static bool medium_is_volumetric_(const Material* material) {
		return !cast(material)->medium.isNull() && cast(material)->medium->isVolumetric();
	}

	__host__ __device__
	static ComplexF medium_refractive_index_(const Material* material, const Point3F& p, const Point2F& uv, const float wlen, const float time) {
		return cast(material)->medium.isNull() ? ComplexF{1.f} : cast(material)->medium->refractiveIndex(p, uv, wlen, time);
	}

	__host__ __device__
	static float medium_transmittance_(const Material* material, const Ray& ray, RNG* rng1D) {
		return cast(material)->medium.isNull() ? 1.f : cast(material)->medium->transmittance(ray, rng1D);
	}

	__host__ __device__
	static bool medium_sample_(const Material* material, const Ray& ray, RNG* rng1D, Point3F* p, float* beta) {
		if (cast(material)->medium.isNull()) {
			*beta = 1.f;
			return false;
		}
		return cast(material)->medium->sample(ray, rng1D, p, beta);
	}

	__host__ __device__
	static float medium_evaluate_phase_(const Material* material, const Point3F& p, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo) {
		return cast(material)->medium.isNull() ? 0.f : cast(material)->medium->evaluatePhase(p, wlen, time, wi, wo);
	}

	__host__ __device__
	static float medium_sample_wi_(const Material* material, const Point3F& p, const float wlen, const float time, const Vectr3F& wo, float rnd[2], Vectr3F* wi, float* pdf) {
		if (cast(material)->medium.isNull()) {
			*pdf = 0.f;
			return 0.f;
		}
		return cast(material)->medium->sampleWi(p, wlen, time, wo, rnd, wi, pdf);
	}

	__host__ __device__
	static float medium_sample_wi_pdf_(const Material* material, const Point3F& p, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi) {
		return cast(material)->medium.isNull() ? 0.f : cast(material)->medium->sampleWiPDF(p, wlen, time, wo, wi);
	}

	__host__
	BaseMaterial(
		const Pointer<const UEDF>& uedf,
		const Pointer<const BSDF>& bsdf,
		const Pointer<const Medium>& medium,
		const uedf_is_emissive_t& uedf_is_emissive_,
		const uedf_emission0_t& uedf_emission0_,
		const uedf_emission1_t& uedf_emission1_,
		const uedf_emission_t& uedf_emission_,
		const uedf_sample_we_t& uedf_sample_we_,
		const uedf_sample_we_pdf_t& uedf_sample_we_pdf_,
		const bsdf_is_interacting_t& bsdf_is_interacting_,
		const bsdf_matches_type_t& bsdf_matches_type_,
		const bsdf_evaluate_t& bsdf_evaluate_,
		const bsdf_sample_wi_t& bsdf_sample_wi_,
		const bsdf_sample_wi_pdf_t& bsdf_sample_wi_pdf_,
		const medium_get_priority_t& medium_get_priority_,
		const medium_is_volumetric_t& medium_is_volumetric_,
		const medium_refractive_index_t& medium_refractive_index_,
		const medium_transmittance_t& medium_transmittance_,
		const medium_sample_t& medium_sample_,
		const medium_evaluate_phase_t& medium_evaluate_phase_,
		const medium_sample_wi_t& medium_sample_wi_,
		const medium_sample_wi_pdf_t& medium_sample_wi_pdf_
	):
		Material(
			uedf_is_emissive_,
			uedf_emission0_,
			uedf_emission1_,
			uedf_emission_,
			uedf_sample_we_,
			uedf_sample_we_pdf_,
			bsdf_is_interacting_,
			bsdf_matches_type_,
			bsdf_evaluate_,
			bsdf_sample_wi_,
			bsdf_sample_wi_pdf_,
			medium_get_priority_,
			medium_is_volumetric_,
			medium_refractive_index_,
			medium_transmittance_,
			medium_sample_,
			medium_evaluate_phase_,
			medium_sample_wi_,
			medium_sample_wi_pdf_
		),
		uedf(uedf),
		bsdf(bsdf),
		medium(medium) {
	}

public:
	friend class TypedPool<BaseMaterial>;

	/*!
	 * \brief
	 * Creates na new BaseMaterial.
	 *
	 * \param[in] uedf The surface light emission distribution function.
	 * \param[in] bsdf The bidirectional scattering distribution function.
	 * \param[in] medium The participating medium.
	 */
	__host__
	static Pointer<BaseMaterial> build(MemoryPool& pool, const Pointer<const UEDF>& uedf, const Pointer<const BSDF>& bsdf, const Pointer<const Medium>& medium) {
		return Pointer<BaseMaterial>::make(
			pool, uedf, bsdf, medium,
			uedf_is_emissive_t(virtualize(uedf_is_emissive_)),
			uedf_emission0_t(uedf_emission0_, [] __device__ () {return (void*)uedf_emission0_;}),
			uedf_emission1_t(virtualize(uedf_emission1_)),
			uedf_emission_t(virtualize(uedf_emission_)),
			uedf_sample_we_t(virtualize(uedf_sample_we_)),
			uedf_sample_we_pdf_t(virtualize(uedf_sample_we_pdf_)),
			bsdf_is_interacting_t(virtualize(bsdf_is_interacting_)),
			bsdf_matches_type_t(virtualize(bsdf_matches_type_)),
			bsdf_evaluate_t(virtualize(bsdf_evaluate_)),
			bsdf_sample_wi_t(virtualize(bsdf_sample_wi_)),
			bsdf_sample_wi_pdf_t(virtualize(bsdf_sample_wi_pdf_)),
			medium_get_priority_t(virtualize(medium_get_priority_)),
			medium_is_volumetric_t(virtualize(medium_is_volumetric_)),
			medium_refractive_index_t(virtualize(medium_refractive_index_)),
			medium_transmittance_t(virtualize(medium_transmittance_)),
			medium_sample_t(virtualize(medium_sample_)),
			medium_evaluate_phase_t(virtualize(medium_evaluate_phase_)),
			medium_sample_wi_t(virtualize(medium_sample_wi_)),
			medium_sample_wi_pdf_t(virtualize(medium_sample_wi_pdf_))
		);
	}

	__host__
	Pointer<const Material> transform(MemoryPool& pool, const Transformation3F& transformation, const Pointer<const Material>& self) const override {
		assert(this == self.get());
		return Pointer<const Material>(TransformedMaterial::build(pool, transformation, self));
	}

	__host__
	Pointer<const Material> transform(MemoryPool& pool, const Pointer<const AnimatedTransformation>& transformation, const Pointer<const Material>& self) const override {
		assert(this == self.get());
		return Pointer<const Material>(AnimatedMaterial::build(pool, transformation, self));
	}
};

}

#endif /* BASEMATERIAL_CUH_ */

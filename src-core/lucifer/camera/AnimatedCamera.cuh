#ifndef ANIMATEDCAMERA_CUH_
#define ANIMATEDCAMERA_CUH_

#include <iostream>
#include "Camera.cuh"
#include "lucifer/math/geometry/AnimatedTransformation.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * A Camera transformed by an AnimatedTransformation.
 *
 * \author
 * Bruno Pera.
 */
class AnimatedCamera: public Camera {
private:
	friend class TypedPool<AnimatedCamera>;
	const Pointer<const AnimatedTransformation> cameraToWorld_;
	Pointer<Camera> camera_;

	__host__ __device__
	static Sensor* get_sensor_(Camera* camera) {
		return static_cast<AnimatedCamera*>(camera)->camera_->sensor();
	}

	__host__ __device__
	static Ray create_ray_(const Camera* camera, const float x, const float y, const float u, const float v, const float l, const float t, Norml3F* n) {
		auto this_ = static_cast<const AnimatedCamera*>(camera);
		Ray r = this_->camera_->ray(x, y, u, v, l, t, n);
		Transformation3F transformation = this_->cameraToWorld_->atTime(t);
		r.apply(transformation);
		if (n != nullptr) {
			*n = transformation * (*n);
		}
		return r;
	}

	__host__ __device__
	static bool raster_ray_(const Camera* camera, const Ray& ray, float* x, float* y) {
		auto this_ = static_cast<const AnimatedCamera*>(camera);
		auto cameraToWorld = this_->cameraToWorld_->atTime(ray.t);
		return this_->camera_->raster(cameraToWorld.inverse() * ray, x, y);
	}

	__host__ __device__
	static float importance0_(const Camera* camera, const Point3F& p, const float time) {
		auto this_ = static_cast<const AnimatedCamera*>(camera);
		auto cameraToWorld = this_->cameraToWorld_->atTime(time);
		return this_->camera_->importance0(cameraToWorld.inverse() * p, time);
	}

	__host__ __device__
	static float smp_geo_pdf_(const Camera* camera, const Point3F& p, const float time) {
		auto this_ = static_cast<const AnimatedCamera*>(camera);
		auto cameraToWorld = this_->cameraToWorld_->atTime(time);
		return this_->camera_->sampleGeoPDF(cameraToWorld.inverse() * p, time);
	}

	__host__ __device__
	static float importance1_(const Camera* camera, const Ray& ray) {
		auto this_ = static_cast<const AnimatedCamera*>(camera);
		auto cameraToWorld = this_->cameraToWorld_->atTime(ray.t);
		return this_->camera_->importance1(cameraToWorld.inverse() * ray);
	}
	__host__ __device__
	static float smp_dir_pdf_(const Camera* camera, const Ray& ray) {
		auto this_ = static_cast<const AnimatedCamera*>(camera);
		auto cameraToWorld = this_->cameraToWorld_->atTime(ray.t);
		return this_->camera_->sampleDirPDF(cameraToWorld.inverse() * ray);
	}

	__host__
	AnimatedCamera(
		const Pointer<const AnimatedTransformation>& cameraToWorld,
		const Pointer<Camera>& camera,
		const sensor_t& get_sensor_,
		const genray_t& create_ray_,
		const raster_t& raster_ray_,
		const importance0_t& importance0_,
		const smp_geo_pfd_t& smp_geo_pdf_,
		const importance1_t& importance1_,
		const smp_dir_pfd_t& smp_dir_pdf_
	):
		Camera(get_sensor_, create_ray_, raster_ray_, importance0_, smp_geo_pdf_, importance1_, smp_dir_pdf_),
		cameraToWorld_(cameraToWorld),
		camera_(camera)
	{
		if (!cameraToWorld->hasUnitScale()) {
			std::cerr << "camera transformation should have unit scaling factors" << std::endl;
		}
	}

public:
	/*!
	 * \brief
	 * Creates a new AnimatedCamera.
	 *
	 * \param[in] cameraToWorld The shape-to-world AnimatedTransformation. The
	 * transformation must have unit scaling factors over the entire normalized
	 * time range otherwise the result is undefined.
	 * \param[in] camera The Camera to be animated.
	 */
	__host__
	static Pointer<AnimatedCamera> build(MemoryPool& pool, const Pointer<const AnimatedTransformation>& cameraToWorld, const Pointer<Camera>& camera) {
		return Pointer<AnimatedCamera>::make(
			pool, cameraToWorld, camera,
			sensor_t(virtualize(get_sensor_)),
			genray_t(virtualize(create_ray_)),
			raster_t(virtualize(raster_ray_)),
			importance0_t(importance0_, [] __device__ () {return (void*)importance0_;}),
			smp_geo_pfd_t(virtualize(smp_geo_pdf_)),
			importance1_t(virtualize(importance1_)),
			smp_dir_pfd_t(virtualize(smp_dir_pdf_))
		);
	}
};

}

#endif /* ANIMATEDCAMERA_CUH_ */

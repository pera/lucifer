#ifndef CAMERA_CUH_
#define CAMERA_CUH_

#include "Sensor.cuh"
#include "lucifer/math/geometry/Ray.cuh"
#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/memory/Virtual.cuh"

namespace lucifer {

/*!
 * \brief
 * The basic interface of a camera.
 *
 * A camera's main responsability is generating rays to start the path tracing
 * process. For bidirectional methods a camera must also be able to compute the
 * probabilities of generating a ray and to compute the emitted importance.
 *
 * \author
 * Bruno Pera.
 */
class Camera: public Polymorphic {
public:
	using sensor_t = Virtual<Sensor*, Camera*>;
	using genray_t = Virtual<Ray, const Camera*, const float, const float, const float, const float, const float, const float, Norml3F*>;
	using raster_t = Virtual<bool, const Camera*, const Ray&, float*, float*>;
	using importance0_t = Virtual<float, const Camera*, const Point3F&, const float>;
	using smp_geo_pfd_t = Virtual<float, const Camera*, const Point3F&, const float>;
	using importance1_t = Virtual<float, const Camera*, const Ray&>;
	using smp_dir_pfd_t = Virtual<float, const Camera*, const Ray&>;

private:
	sensor_t get_sensor_;
	genray_t create_ray_;
	raster_t raster_ray_;
	importance0_t importance0_;
	smp_geo_pfd_t smp_geo_pdf_;
	importance1_t importance1_;
	smp_dir_pfd_t smp_dir_pdf_;

public:
	__host__
	Camera(
		const sensor_t& get_sensor_,
		const genray_t& create_ray_,
		const raster_t& raster_ray_,
		const importance0_t& importance0_,
		const smp_geo_pfd_t& smp_geo_pdf_,
		const importance1_t& importance1_,
		const smp_dir_pfd_t& smp_dir_pdf_
	):
		get_sensor_(get_sensor_),
		create_ray_(create_ray_),
		raster_ray_(raster_ray_),
		importance0_(importance0_),
		smp_geo_pdf_(smp_geo_pdf_),
		importance1_(importance1_),
		smp_dir_pdf_(smp_dir_pdf_) {
	}

	/*!
	 * \brief
	 * Returns the camera sensor.
	 */
	__host__ __device__
	Sensor* sensor() {
		return get_sensor_(this);
	}

	/*!
	 * \brief
	 * Generates a ray from the given parameters.
	 *
	 * \param[in] x The x coordinate in image space.
	 * \param[in] y The y coordinate in image space.
	 * \param[in] u A random number in [0, 1] to sample a position on the lens.
	 * \param[in] v A random number in [0, 1] to sample a position on the lens.
	 * \param[in] l The light wavelength in meters.
	 * \param[in] t The normalized time in [0, 1].
	 * \param[out] n The normal vector at the sampled position on the lens.
	 */
	__host__ __device__
	Ray ray(const float x, const float y, const float u, const float v, const float l, const float t, Norml3F* n = nullptr) const {
		return create_ray_(this, x, y, u, v, l, t, n);
	}

	/*!
	 * \brief
	 * Computes the x and y image coordinates from a given ray.
	 *
	 * \param[in] ray The ray.
	 * \param[out] The x coordinate in image space.
	 * \param[out] The y coordinate in image space.
	 *
	 * \return true if the ray maps to a valid position in image space.
	 */
	__host__ __device__
	bool raster(const Ray& ray, float* x, float* y) const {
		return raster_ray_(this, ray, x, y);
	}

	/*!
	 * \brief
	 * Computes the total emitted importance from a point on the camera lens.
	 *
	 * \param[in] p The position on the camera lens.
	 * \param[in] time The normalized time in [0, 1].
	 *
	 * \return The total emitted importance from the point p.
	 */
	__host__ __device__
	float importance0(const Point3F& p, const float time) const {
		return importance0_(this, p, time);
	}

	/*!
	 * \brief
	 * Computes the PDF of sampling a position on the camera lens.
	 *
	 * \param[in] p The position on the camera lens.
	 * \param[in] time The normalized time in [0, 1].
	 *
	 * \return The PDF of sampling p according to the lens area measure.
	 */
	__host__ __device__
	float sampleGeoPDF(const Point3F& p, const float time) const {
		return smp_geo_pdf_(this, p, time);
	}

	/*!
	 * \brief
	 * Computes the directional differencial importace of a sampled ray.
	 *
	 * \param[in] ray The sampled ray. The ray's origin is supposed to be on
	 * the camera lens and the ray direction is used to compute the directional
	 * differencial.
	 *
	 * \return The directional differencial importace.
	 */
	__host__ __device__
	float importance1(const Ray& ray) const {
		return importance1_(this, ray);
	}

	/*!
	 * \brief
	 * Computes the PDF of sampling the given ray's direction.
	 *
	 * \param[in] ray The sampled ray. The ray's origin is supposed to be on
	 * the camera lens.
	 *
	 * \return The PDF of sampling the ray's direction with respect to the
	 * solid angle measure from the ray's origin.
	 */
	__host__ __device__
	float sampleDirPDF(const Ray& ray) const {
		return smp_dir_pdf_(this, ray);
	}
};

}

#endif /* CAMERA_CUH_ */

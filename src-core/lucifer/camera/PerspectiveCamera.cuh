#ifndef PERSPECTIVECAMERA_CUH_
#define PERSPECTIVECAMERA_CUH_

#include "Camera.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/random/ConcentricDiskDistribution.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * A basic circular thin lens perspective camera model.
 *
 * \author
 * Bruno Pera.
 */
class PerspectiveCamera: public Camera {
private:
	friend class TypedPool<PerspectiveCamera>;
	const float h;
	const float w;
	const float r;
	const float d;
	Pointer<Sensor> sensor_;

	__host__ __device__
	inline float lensArea() const {
		return r > 0.f ? (2.f / PI<float>()) * r * r : 1.f;
	}

	__host__ __device__
	static Sensor* get_sensor_(Camera* camera) {
		return static_cast<PerspectiveCamera*>(camera)->sensor_.get();
	}

	__host__ __device__
	static Ray create_ray_(const Camera* camera, const float x, const float y, const float u, const float v, const float l, const float t, Norml3F* n) {
		auto this_ = static_cast<const PerspectiveCamera*>(camera);
	    float dx = ((x * this_->w) / this_->sensor_->width()) - (this_->w / 2.f);
	    float dy = (this_->h / 2.f) - ((y * this_->h) / this_->sensor_->height());
	    Ray ray(Point3F(0.f, 0.f, 0.f), Vectr3F(dx, dy, 1).normalize(), RangeF{0.f, POSITIVE_INFINITY<float>()}, l, t);
	    if (this_->r > 0.f) {
			float uv[2] = { u, v };
	        ConcentricDiskDistribution::sample(uv, uv, nullptr);
	        Point3F pFocus = ray(this_->d / ray.d[2]);
	        ray.o = Point3F(uv[0] * this_->r, uv[1] * this_->r, 0.f);
	        ray.d = (pFocus - ray.o).normalize();
	    }
		if (n != nullptr) *n = Norml3F(0.f, 0.f, 1.f);
	    return ray;
	}

	__host__ __device__
	static bool raster_ray_(const Camera* camera, const Ray& ray, float* x, float* y) {
		auto this_ = static_cast<const PerspectiveCamera*>(camera);
		if (ray.d[2] < 0.f) {
			return false;
		}
		float dx, dy;
		if (this_->r > 0.f) {
			float dist = (this_->d - ray.o[2]) / ray.d[2];
			dx = (ray.o[0] + dist * ray.d[0]) / this_->d;
			dy = (ray.o[1] + dist * ray.d[1]) / this_->d;
		} else {
			dx = ray.d[0] / ray.d[2];
			dy = ray.d[1] / ray.d[2];
		}
		*x = this_->sensor_->width () * (dx + (this_->w / 2.f)) / this_->w;
		*y = this_->sensor_->height() * ((this_->h / 2.f) - dy) / this_->h;
		return *x >= 0.f && *x <= this_->sensor_->width() && *y >= 0.f && *y <= this_->sensor_->height();
	}

	__host__ __device__
	static float importance0_(const Camera* camera, const Point3F& p, const float time) {
		return 1.f;
	}

	__host__ __device__
	static float smp_geo_pdf_(const Camera* camera, const Point3F& p, const float time) {
		return 1.f / static_cast<const PerspectiveCamera*>(camera)->lensArea();
	}

	__host__ __device__
	static float importance1_(const Camera* camera, const Ray& ray) {
		auto this_ = static_cast<const PerspectiveCamera*>(camera);
		float x, y;
		if (!this_->raster(ray, &x, &y)) return 0.f;
		return 1.f / (this_->w * this_->h * ray.d[2] * ray.d[2] * ray.d[2]);
	}

	__host__ __device__
	static float smp_dir_pdf_(const Camera* camera, const Ray& ray) {
		auto this_ = static_cast<const PerspectiveCamera*>(camera);
		float x, y;
		if (!this_->raster(ray, &x, &y)) return 0.f;
		return 1.f / (this_->w * this_->h * ray.d[2] * ray.d[2] * ray.d[2]);
	}

	__host__
	PerspectiveCamera(
		const float fovy,
		const float lensRadius,
		const float focalDistance,
		const Pointer<Sensor>& sensor,
		const sensor_t& get_sensor_,
		const genray_t& create_ray_,
		const raster_t& raster_ray_,
		const importance0_t& importance0_,
		const smp_geo_pfd_t& smp_geo_pdf_,
		const importance1_t& importance1_,
		const smp_dir_pfd_t& smp_dir_pdf_
	):
		Camera(get_sensor_, create_ray_, raster_ray_, importance0_, smp_geo_pdf_, importance1_, smp_dir_pdf_),
		h(2.f * tanf(fovy / 2.f)), w((h * sensor->width()) / sensor->height()), r(lensRadius), d(focalDistance), sensor_(sensor) {
	}

public:
	/*!
	 * \brief
	 * Creates a new perspective camera.
	 *
	 * \param[in] fovy The vertival field of view in radians.
	 * \param[in] lensRadius The lens radius in meters, 0.f for a pinhole camera.
	 * \param[in] focalDistance The focal distance in meters.
	 * \param[in] sensor The camera sensor.
	 */
	__host__
	static Pointer<PerspectiveCamera> build(MemoryPool& pool, const float fovy, const float lensRadius, const float focalDistance, const Pointer<Sensor>& sensor) {
		return Pointer<PerspectiveCamera>::make(
			pool, fovy, lensRadius, focalDistance, sensor,
			sensor_t(virtualize(get_sensor_)),
			genray_t(virtualize(create_ray_)),
			raster_t(virtualize(raster_ray_)),
			importance0_t(importance0_, [] __device__ () {return (void*)importance0_;}),
			smp_geo_pfd_t(virtualize(smp_geo_pdf_)),
			importance1_t(virtualize(importance1_)),
			smp_dir_pfd_t(virtualize(smp_dir_pdf_))
		);
	}
};

}

#endif /* PERSPECTIVECAMERA_CUH_ */

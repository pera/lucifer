#ifndef TRANSFORMEDCAMERA_CUH_
#define TRANSFORMEDCAMERA_CUH_

#include <iostream>
#include "Camera.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * A transformed camera.
 *
 * \author
 * Bruno Pera.
 */
class TransformedCamera: public Camera {
private:
	friend class TypedPool<TransformedCamera>;
	const Transformation3F  cameraToWorld_;
	Pointer<Camera> camera_;

	__host__ __device__
	static Sensor* get_sensor_(Camera* camera) {
		return static_cast<TransformedCamera*>(camera)->camera_->sensor();
	}

	__host__ __device__
	static Ray create_ray_(const Camera* camera, const float x, const float y, const float u, const float v, const float l, const float t, Norml3F* n) {
		auto this_ = static_cast<const TransformedCamera*>(camera);
		Ray r = this_->camera_->ray(x, y, u, v, l, t, n);
		r.apply(this_->cameraToWorld_);
		if (n != nullptr) {
			*n = this_->cameraToWorld_ * (*n);
		}
		return r;
	}

	__host__ __device__
	static bool raster_ray_(const Camera* camera, const Ray& ray, float* x, float* y) {
		auto this_ = static_cast<const TransformedCamera*>(camera);
		return this_->camera_->raster(this_->cameraToWorld_.inverse() * ray, x, y);
	}

	__host__ __device__
	static float importance0_(const Camera* camera, const Point3F& p, const float time) {
		auto this_ = static_cast<const TransformedCamera*>(camera);
		return this_->camera_->importance0(this_->cameraToWorld_.inverse() * p, time);
	}

	__host__ __device__
	static float smp_geo_pdf_(const Camera* camera, const Point3F& p, const float time) {
		auto this_ = static_cast<const TransformedCamera*>(camera);
		return this_->camera_->sampleGeoPDF(this_->cameraToWorld_.inverse() * p, time);
	}

	__host__ __device__
	static float importance1_(const Camera* camera, const Ray& ray) {
		auto this_ = static_cast<const TransformedCamera*>(camera);
		return this_->camera_->importance1(this_->cameraToWorld_.inverse() * ray);
	}
	__host__ __device__
	static float smp_dir_pdf_(const Camera* camera, const Ray& ray) {
		auto this_ = static_cast<const TransformedCamera*>(camera);
		return this_->camera_->sampleDirPDF(this_->cameraToWorld_.inverse() * ray);
	}

	__host__
	TransformedCamera(
		const Transformation3F& cameraToWorld,
		const Pointer<Camera>& camera,
		const sensor_t& get_sensor_,
		const genray_t& create_ray_,
		const raster_t& raster_ray_,
		const importance0_t& importance0_,
		const smp_geo_pfd_t& smp_geo_pdf_,
		const importance1_t& importance1_,
		const smp_dir_pfd_t& smp_dir_pdf_
	):
		Camera(get_sensor_, create_ray_, raster_ray_, importance0_, smp_geo_pdf_, importance1_, smp_dir_pdf_),
		cameraToWorld_(cameraToWorld),
		camera_(camera)
	{
		auto factor = scale(cameraToWorld);
		if (abs(1.f - factor[0]) > 1E-4f or abs(1.f - factor[1]) > 1E-4f or abs(1.f - factor[2]) > 1E-4f) {
			std::cerr << "camera transformation should have unit scaling factors" << std::endl;
		}
	}

public:
	/*!
	 * \brief
	 * Creates a new TransformedCamera.
	 *
	 * \param[in] cameraToWorld The camera to world transformation. Must have
	 * unit scaling factors otherwise the result is undefined.
	 * \param[in] camera The original camera to be transformed.
	 */
	__host__
	static Pointer<TransformedCamera> build(MemoryPool& pool, const Transformation3F& cameraToWorld, const Pointer<Camera>& camera) {
		return Pointer<TransformedCamera>::make(
			pool, cameraToWorld, camera,
			sensor_t(virtualize(get_sensor_)),
			genray_t(virtualize(create_ray_)),
			raster_t(virtualize(raster_ray_)),
			importance0_t(importance0_, [] __device__ () {return (void*)importance0_;}),
			smp_geo_pfd_t(virtualize(smp_geo_pdf_)),
			importance1_t(virtualize(importance1_)),
			smp_dir_pfd_t(virtualize(smp_dir_pdf_))
		);
	}
};

}

#endif /* TRANSFORMEDCAMERA_CUH_ */

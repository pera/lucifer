#ifndef SENSOR_CUH_
#define SENSOR_CUH_

#include <cassert>
#include "lucifer/color/XYZColorSpace.cuh"
#include "lucifer/filter/Filter2D.cuh"
#include "lucifer/image/HDRImage.cuh"
#include "lucifer/memory/Polymorphic.cuh"

namespace lucifer {

class Sensor: public Polymorphic {
private:
	Pointer<HDRImage<float, 3>> raster_;
	const Pointer<const XYZColorSpace> colorspace_;
	const Pointer<const Filter2D> filter_;
	int xLower, xUpper;
	int yLower, yUpper;

public:
	__host__
	Sensor(Pointer<HDRImage<float, 3>>& raster, const Pointer<const XYZColorSpace>& xyz, const Pointer<const Filter2D>& filter):
	raster_(raster), colorspace_(xyz), filter_(filter) {
		if (!filter.isNull()) {
			RangeF xRange = filter->xSupport();
			RangeF yRange = filter->ySupport();
			xLower = (int) ( ceil(xRange.lower - 0.5f));
			xUpper = (int) (floor(xRange.upper + 0.5f));
			yLower = (int) ( ceil(yRange.lower - 0.5f));
			yUpper = (int) (floor(yRange.upper + 0.5f));
		}
	}

	__host__  __device__
	Pointer<HDRImage<float, 3>>& raster() {
		return raster_;
	}

	__host__ __device__
	const Pointer<const XYZColorSpace>& colorspace() const {
		return colorspace_;
	}

	__host__ __device__
	const Pointer<const Filter2D>& filter() const {
		return filter_;
	}

	__host__ __device__
	size_t width() const {
		return raster_->width();
	}

	__host__ __device__
	size_t height() const {
		return raster_->height();
	}

	__host__ __device__
	void clear() {
		for (int y = 0; y < height(); y++) {
			for (int x = 0; x < width(); x++) {
				(*raster_)(y, x) = Color3F(0.f, 0.f, 0.f);
			}
		}
	}

	__host__ __device__
	void addSample(const float x, const float y, const float l, const float val, const float pdf) {
		if (x < 0.f || x >= width() || y < 0.f || y >= height() || pdf <= 0.f || val <= 0.f) {
			return;
		}
		if (isinf(val) || isinf(pdf) || isnan(val) || isnan(pdf) ) {
			return;
		}
		int xi = (int) x;
		int yi = (int) y;
		Color3F xyz;
		colorspace_->XYZ(l, val / pdf, xyz);
		assert(!isinf(xyz));
		assert(!isnan(xyz));
		uint h = raster_->height();
		uint w = raster_->width();
		if (filter_.isNull()) {
			assert(yi >= 0 && yi < h);
			assert(xi >= 0 && xi < w);
			(*raster_)(yi, xi) += xyz;
		} else {
			for (int dy = yLower; dy <= yUpper; dy++) {
				if (yi + dy < 0 || yi + dy >= h) {
					continue;
				}
				for (int dx = xLower; dx <= xUpper; dx++) {
					if (xi + dx < 0 || xi + dx >= w) {
						continue;
					}
					float weight = (*filter_)(x - (xi + dx), y - (yi + dy));
					assert(!isinf(weight));
					assert(!isnan(weight));
					(*raster_)(yi + dy, xi + dx) += xyz * weight;
				}
			}
		}
	}

	__device__
	void atomicAdd(const float x, const float y, const float l, const float val, const float pdf) {
		if (x < 0.f || x >= width() || y < 0.f || y >= height() || pdf <= 0.f || val <= 0.f) {
			return;
		}
		if (isinf(val) || isinf(pdf) || isnan(val) || isnan(pdf) ) {
			return;
		}
		int xi = (int) x;
		int yi = (int) y;
		Color3F xyz;
		colorspace_->XYZ(l, val / pdf, xyz);
		assert(!isinf(xyz));
		assert(!isnan(xyz));
		uint h = raster_->height();
		uint w = raster_->width();
		if (filter_.isNull()) {
			assert(yi >= 0 && yi < h);
			assert(xi >= 0 && xi < w);
			(*raster_)(yi, xi).atomAdd(xyz);
		} else {
			for (int dy = yLower; dy <= yUpper; dy++) {
				if (yi + dy < 0 || yi + dy >= h) {
					continue;
				}
				for (int dx = xLower; dx <= xUpper; dx++) {
					if (xi + dx < 0 || xi + dx >= w) {
						continue;
					}
					float weight = (*filter_)(x - (xi + dx), y - (yi + dy));
					assert(!isinf(weight));
					assert(!isnan(weight));
					(*raster_)(yi + dy, xi + dx).atomAdd(xyz * weight);
				}
			}
		}
	}

	__host__ __device__
	void addWeightedSample(const uint32_t x, const uint32_t y, const float l, const float val, const float pdf, const float wgt) {
		uint h = height();
		uint w = width();
		if (x >= w || y >= h || pdf <= 0.f || val <= 0.f) {
			return;
		}
		if (isinf(val) || isinf(pdf) || isinf(wgt) || isnan(val) || isnan(pdf) || isnan(wgt)) {
			return;
		}
		Color3F xyz;
		colorspace_->XYZ(l, val / pdf, xyz);
		assert(y < h);
		assert(x < w);
		(*raster_)(y, x) += xyz * wgt;
	}

	__host__
	static Pointer<Sensor> build(MemoryPool& pool, Pointer<HDRImage<float, 3>>& raster, const Pointer<const XYZColorSpace>& xyz, const Pointer<const Filter2D>& filter) {
		return Pointer<Sensor>::make(pool, raster, xyz, filter);
	}
};

}

#endif /* SENSOR_CUH_ */

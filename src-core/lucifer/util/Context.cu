#include "Context.cuh"

#include "lucifer/memory/CPUAllocator.cuh"
#include "lucifer/memory/ManagedAllocator.cuh"

namespace lucifer {

MemoryPool* Context::cpuOnlyMemoryPool_{nullptr};
MemoryPool* Context::managedMemoryPool_{nullptr};


} /* namespace lucifer */

#ifndef CONTEXT_CUH_
#define CONTEXT_CUH_

#include "lucifer/memory/CPUAllocator.cuh"
#include "lucifer/memory/ManagedAllocator.cuh"
#include "lucifer/memory/MemoryPool.cuh"

namespace lucifer {

class Context {
private:
	static MemoryPool* cpuOnlyMemoryPool_;
	static MemoryPool* managedMemoryPool_;

public:
	__host__
	Context() = delete;

	__host__ static
	void initialize() {
		assert(cpuOnlyMemoryPool_ == nullptr);
		assert(managedMemoryPool_ == nullptr);
		cpuOnlyMemoryPool_ = new MemoryPool{32, new CPUAllocator{}};
		managedMemoryPool_ = new MemoryPool{32, new ManagedAllocator{}};
	}

	__host__ static
	void shutdown() {
		assert(cpuOnlyMemoryPool_ != nullptr);
		assert(managedMemoryPool_ != nullptr);
		delete cpuOnlyMemoryPool_;
		delete managedMemoryPool_;
		cpuOnlyMemoryPool_ = nullptr;
		managedMemoryPool_ = nullptr;
	}

	__host__ static
	MemoryPool& cpuOnlyMemoryPool() {
		assert(cpuOnlyMemoryPool_ != nullptr);
		return *cpuOnlyMemoryPool_;
	}

	__host__ static
	MemoryPool& managedMemoryPool() {
		assert(managedMemoryPool_ != nullptr);
		return *managedMemoryPool_;
	}
};

} /* namespace lucifer */

#endif /* CONTEXT_CUH_ */

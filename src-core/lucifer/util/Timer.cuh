#ifndef TIMER_CUH_
#define TIMER_CUH_

#include <chrono>

namespace lucifer {

class Timer {
public:
	__host__ Timer() : beg_(clock_::now()) { }
	__host__ void reset() {
		beg_ = clock_::now();
	}
	__host__ double elapsed() const {
		return std::chrono::duration_cast<second_>(clock_::now() - beg_).count();
	}
private:
	typedef std::chrono::high_resolution_clock clock_;
	typedef std::chrono::duration<double, std::ratio<1> > second_;
	std::chrono::time_point<clock_> beg_;
};

}

#endif /* TIMER_CUH_ */

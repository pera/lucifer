#ifndef THRUSTDEVICECACHEDALLOCATOR_CUH_
#define THRUSTDEVICECACHEDALLOCATOR_CUH_

#include <thrust/system/cuda/memory.h>
#include <cstdlib>
#include <map>

namespace lucifer {

class ThrustDeviceCachedAllocator {
private:
    using free_blocks_type = std::multimap<std::ptrdiff_t, char*>;
    using allocated_blocks_type = std::map<char*, std::ptrdiff_t>;
    free_blocks_type free_blocks;
    allocated_blocks_type allocated_blocks;

private:
    __host__
    free_blocks_type::iterator find_first(const std::ptrdiff_t num_bytes) {
    	for (auto it = free_blocks.begin(); it != free_blocks.end(); ++it) {
    		if (it->first >= num_bytes) {
    			return it;
    		}
    	}
    	return free_blocks.end();
    }

public:
	using value_type = char;

	__host__
    value_type* allocate(const std::ptrdiff_t num_bytes) {
	  value_type* result = 0;
	  free_blocks_type::iterator free_block = find_first(num_bytes);
	  if(free_block != free_blocks.end()) {
		result = free_block->second;
		free_blocks.erase(free_block);
	  }
	  else {
		try {
		  result = thrust::cuda::malloc<value_type>(num_bytes).get();
		} catch(std::runtime_error &e) {
		  throw;
		}
	  }
	  allocated_blocks.insert(std::make_pair(result, num_bytes));
	  return result;
	}

	__host__
    void deallocate(char *ptr, const size_t n) {
      allocated_blocks_type::iterator iter = allocated_blocks.find(ptr);
      std::ptrdiff_t num_bytes = iter->second;
      allocated_blocks.erase(iter);
      free_blocks.insert(std::make_pair(num_bytes, ptr));
    }

    __host__
    ~ThrustDeviceCachedAllocator() {
		for(free_blocks_type::iterator i = free_blocks.begin(); i != free_blocks.end(); ++i) {
			thrust::cuda::free(thrust::cuda::pointer<char>(i->second));
		}
		for(allocated_blocks_type::iterator i = allocated_blocks.begin(); i != allocated_blocks.end(); ++i) {
			thrust::cuda::free(thrust::cuda::pointer<char>(i->first));
		}
    }
};

}

#endif /* THRUSTDEVICECACHEDALLOCATOR_CUH_ */

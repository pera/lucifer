#ifndef ZORDER_CUH_
#define ZORDER_CUH_

#include <cstdint>
#include "lucifer/math/geometry/CommonTypes.cuh"

namespace lucifer {

__host__
inline bool less_msb(const int32_t x, const int32_t y) {
	return x < y and x < (x ^ y);
}

template<int N> __host__
inline bool cmp_zorder(const int32_t a[N], const int32_t b[N]) {
	int32_t j = 0;
    int32_t x = 0;
    for (int32_t k = 0; k < N; ++k) {
    	int32_t y = a[k] ^ b[k];
		if (less_msb(x, y)) {
			j = k;
			x = y;
		}
    }
    return a[j] <= b[j];
}

class ZOrderComparator {
private:
	const Point3F min;
	const Point3F max;

public:
	ZOrderComparator(const Point3F& min, const Point3F& max):
	min(min - Vectr3F(0.1f, 0.1f, 0.1f)), max(max + Vectr3F(0.1f, 0.1f, 0.1f)) {
	}

	bool operator()(const Point3F& a, const Point3F& b) const {
		int32_t x[] = {
			(int32_t)(INT32_MAX * (a[0] - min[0]) / (max[0] - min[0])),
			(int32_t)(INT32_MAX * (a[1] - min[1]) / (max[1] - min[1])),
			(int32_t)(INT32_MAX * (a[2] - min[2]) / (max[2] - min[2]))
		};
		int32_t y[] = {
			(int32_t)(INT32_MAX * (b[0] - min[0]) / (max[0] - min[0])),
			(int32_t)(INT32_MAX * (b[1] - min[1]) / (max[1] - min[1])),
			(int32_t)(INT32_MAX * (b[2] - min[2]) / (max[2] - min[2]))
		};
		return cmp_zorder<3>(x, y);
	}
};

}

#endif /* ZORDER_CUH_ */

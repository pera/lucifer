#ifndef PROGRESS_CUH_
#define PROGRESS_CUH_

#include <mutex>
#include <thread>
#include <iostream>
#include <iomanip>

#include "Timer.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename T> class ProgressTracker {
public:
	__host__
	virtual void initialize(const T& workload) = 0;

	__host__
	virtual void log(const T& work) = 0;

	__host__
	virtual const T workDone() = 0;

	__host__
	virtual double percentDone() = 0;

	__host__
	virtual double elapsedTime() = 0;

	__host__
	virtual bool isTerminated() = 0;

	__host__
	virtual double terminate() = 0;

	__host__
	virtual ~ProgressTracker() {
	}
};

template<typename T> class BasicProgressTracker: public ProgressTracker<T> {
private:
	T workload_;
	T workDone_;
	bool terminated;
	Timer timer;
public:
	__host__
	virtual void initialize(const T& workload) {
		workload_ = workload;
		workDone_ = 0;
		terminated = false;
		timer.reset();
	}

	__host__
	virtual void log(const T& work) {
		workDone_ += work;
	}

	__host__
	virtual const T workDone() {
		return workDone_;
	}

	__host__
	virtual double percentDone() {
		double result = (double) ((double) (workDone_) / workload_);
		return isnan(result) ? 0 : result;
	}

	__host__
	virtual double elapsedTime() {
		return timer.elapsed();
	}

	__host__
	virtual bool isTerminated() {
		return terminated;
	}

	__host__
	virtual double terminate() {
		terminated = true;
		return timer.elapsed();
	}

	__host__
	virtual ~BasicProgressTracker() {
	}
};

template<typename T> class SynchronizedProgressTracker: public ProgressTracker<T> {
private:
	Pointer<ProgressTracker<T>> tracker;
	std::mutex mutex;

public:
	__host__
	SynchronizedProgressTracker(Pointer<ProgressTracker<T>>&  tracker): tracker(tracker) {
	}

	__host__
	SynchronizedProgressTracker(Pointer<ProgressTracker<T>>&& tracker): tracker(tracker) {
	}

	__host__
	virtual void initialize(const T& workload) {
		std::unique_lock<std::mutex> lock(mutex);
		tracker->initialize(workload);
	}

	__host__
	virtual void log(const T& work) {
		std::unique_lock<std::mutex> lock(mutex);
		tracker->log(work);
	}

	__host__
	virtual const T workDone() {
		std::unique_lock<std::mutex> lock(mutex);
		return tracker->workDone();
	}

	__host__
	virtual double percentDone() {
		std::unique_lock<std::mutex> lock(mutex);
		return tracker->percentDone();
	}

	__host__
	virtual double elapsedTime() {
		std::unique_lock<std::mutex> lock(mutex);
		return tracker->elapsedTime();
	}

	__host__
	virtual bool isTerminated() {
		std::unique_lock<std::mutex> lock(mutex);
		return tracker->isTerminated();
	}

	__host__
	virtual double terminate() {
		std::unique_lock<std::mutex> lock(mutex);
		return tracker->terminate();
	}

	__host__
	virtual ~SynchronizedProgressTracker() {
	}
};


template<typename T=void> __host__
void printProgress(double percent, uint8_t width) {
	std::cout << "[";
	int pos = width * percent;
	for (int i = 0; i < width; ++i) {
		if (i < pos) std::cout << "=";
		else if (i == pos) std::cout << ">";
		else std::cout << " ";
	}
	std::cout << "] ";
    std::cout << std::internal << std::setfill(' ') << std::setw(6);
    std::cout << std::fixed << std::setprecision(2);
    std::cout << (percent * 100) << "%\r";
    std::cout.flush();
}

template<typename T> __host__
void trackProgress(ProgressTracker<T>* progress, uint8_t width) {
	while (!progress->isTerminated()) {
		printProgress(progress->percentDone(), width);
	    std::this_thread::sleep_for(std::chrono::seconds(1));
	}
	printProgress(progress->percentDone(), width);
	std::cout << std::endl;
}

}

#endif /* PROGRESS_CUH_ */

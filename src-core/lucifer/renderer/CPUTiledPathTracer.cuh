#ifndef CPUTILEDPATHTRACER_CUH_
#define CPUTILEDPATHTRACER_CUH_

#include <atomic>
#include <cassert>
#include <future>
#include <mutex>
#include <vector>
#include "Renderer.cuh"
#include "lucifer/image/TiledBuffer.cuh"
#include "lucifer/material/InteriorListUtils.cuh"
#include "lucifer/memory/CPUAllocator.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/random/CPUBasicRNG.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/util/Context.cuh"
#include "lucifer/util/Timer.cuh"

namespace lucifer {

class TileSize {
public:
	const size_t w;
	const size_t h;
	__host__
	TileSize(const size_t w, const size_t h):
	w(w), h(h) {
	}
};

class Workload {
private:
	const size_t n;
	const size_t s;
	std::atomic<size_t> i;

public:
	__host__
	Workload(const TiledBuffer& tiles, size_t spp):
	n(tiles.size() * spp), s(tiles.size()), i(0) {
	}

	__host__
	bool next(size_t* tile) {
		*tile = i++;
		if (*tile >= n) {
			return false;
		}
		*tile = *tile - (*tile / s) * s;
		return true;
	}
};

class CPUTiledPathTracer: public Renderer {
private:
	class Task {
	private:
		std::mutex mutex;
		bool finished;
		Pointer<TiledBuffer> tiles_;
		Pointer<HDRImage<float, 3>> output_;
		size_t timeBudget;
		Timer timer;

	public:
		__host__
		Task(Pointer<TiledBuffer>&& tiles, Pointer<HDRImage<float, 3>>& output, size_t timeBudget):
		finished(false), tiles_(tiles), output_(output), timeBudget(timeBudget) {
		}

		__host__
		inline bool isFinished() {
			std::unique_lock<std::mutex> lock(mutex);
			return finished;
		}

		__host__
		inline TiledBuffer* tiles() {
			return tiles_.get();
		}

		__host__
		void update() {
			std::unique_lock<std::mutex> lock(mutex);
			if (!finished) {
				tiles_->reconstruct(output_.get());
			}
		}

		__host__
		void updateAndFinish() {
			std::unique_lock<std::mutex> lock(mutex);
			if (!finished) {
				tiles_->reconstruct(output_.get());
				finished = true;
			}
		}

		__host__
		bool updateAndFinishIfOutOfTime() {
			if (timeBudget > 0 and timer.elapsed() > timeBudget) {
				updateAndFinish();
				return true;
			}
			return false;
		}

		__host__
		void preview(HDRImage<float, 3>* image) {
			*image = *output_;
		}

		__host__
		void resetTimer() {
			timer.reset();
		}
	};

	class Path {
	public:
		Ray ray;
		float x;
		float y;
		float val;
		float trs;
		float pdf;
		bool specular;
		bool finished;
		int16_t depth;
		InteriorList media;
	};

private:
	const size_t spp;
	const size_t minPathLength;
	const size_t maxPathLength;
	const size_t threads;
	const size_t timeBudget;
	const TileSize tileSize;
	Pointer<CPURNGFactory> rngFactory;
	Pointer<ProgressTracker<uint64_t>> progress;
	Pointer<Task> task;
	const float time;

	__host__
	void init(Path& path, size_t r, size_t c, Scene* scene, RNG* camRNG) const {
		float rnd[6];
		camRNG->next(rnd);
		if (time >= 0.f and time <= 1.f) {
			rnd[5] = time;
		}
		path.x = c + rnd[0];
		path.y = r + rnd[1];
		path.val = 0.f;
		path.trs = 1.f;
		path.specular = false;
		path.finished = false;
		rnd[4] = scene->camera()->sensor()->colorspace()->xyzHistogram()->sample(rnd[4], &path.pdf);
		path.ray = scene->camera()->ray(path.x, path.y, rnd[2], rnd[3], rnd[4], rnd[5]);
		path.depth = 0;
		scene->initInteriorList(path.ray, path.media);
	}

	__host__
	void bounce(Scene* scene, RNG* rng1, RNG* rng2, Sensor* sensor, Path& path) {
		Primitive::Intersection it, lgtIt;
		InteriorList lgtList;
		Vectr3F wi, wo = -path.ray.d;
		float emit, bsdf, phase, pdf, beta, weight, rnd[2];
		// scene interaction
		if (!scene->interaction(path.ray, path.media, &it, &beta, rng1)) {
			path.finished = true;
			return;
		}
		path.trs *= beta;
		if (isInvalid(path.trs)) {
			path.finished = true;
			return;
		}
		if (it.onSurface()) {
			// surface emission
			if (path.depth == 0 || path.specular) {
				path.val += path.trs * it.emission();
			}
			if (it.matchesBSDF(BxDF::Type::DIFFUSE)) {
				// direct lighting from light sources
				rng2->next(rnd);
				emit = scene->sampleLe(it.geometry.gp, path.ray.l, path.ray.t, rnd, &lgtIt.geometry, &pdf);
				if (emit > 0 && pdf > 0) {
					Ray shadow = path.ray;
					lgtList = path.media;
					Scene::scatter(shadow, lgtList, it, lgtIt.geometry.gp);
					wi = shadow.d;
					if (!scene->intersection(shadow, lgtList, rng1, &beta, &lgtIt)) {
						bsdf = it.evaluateBSDF(wi, path.media, BxDF::Mode::RADIANCE, rng1, BxDF::Type::DIFFUSE);
						if (bsdf > 0) {
							weight = balanceHeuristic(pdf, it.sampleBSDFWiPDF(wi, path.media, BxDF::Mode::RADIANCE, rng1, BxDF::Type::DIFFUSE));
							if (isValid(weight) && isValid(beta)) {
								path.val += path.trs * beta * emit * absDot(it.geometry.sn, wi) * bsdf * weight / pdf;
							}
						}
					}
				}
				// direct lighting from bsdf
				rng2->next(rnd);
				bsdf = it.sampleBSDFWi(path.media, BxDF::Mode::RADIANCE, BxDF::Type::DIFFUSE, rnd, &wi, &pdf, rng1);
				if (bsdf > 0 && pdf > 0) {
					Ray shadow = path.ray;
					lgtList = path.media;
					Scene::scatter(shadow, lgtList, it, wi);
					if (scene->intersection(shadow, lgtList, rng1, &beta, &lgtIt)) {
						emit = lgtIt.emission();
						if (emit > 0) {
							// weight = balanceHeuristic(pdf, scene->sampleLePDF(it.geometry.gp, path.ray.l, path.ray.t, lgtIt));
							weight = balanceHeuristic(it.sampleBSDFWiPDF(wi, path.media, BxDF::Mode::RADIANCE, rng1, BxDF::Type::DIFFUSE), scene->sampleLePDF(it.geometry.gp, path.ray.l, path.ray.t, lgtIt));
							if (isValid(weight) && isValid(beta)) {
								path.val += path.trs * beta * emit * absDot(it.geometry.sn, wi) * bsdf * weight / pdf;
							}
						}
					}
				}
			}
			// possibly terminate path
			if (++path.depth >= maxPathLength) {
				path.finished = true;
				return;
			}
			// sample next direction according to bsdf
			BxDF const* sampled = nullptr;
			rng2->next(rnd);
			bsdf = it.sampleBSDFWi(path.media, BxDF::Mode::RADIANCE, BxDF::Type(0), rnd, &wi, &pdf, rng1, &sampled);
			path.trs *= absDot(it.geometry.sn, wi) * bsdf / pdf;
			Scene::scatter(path.ray, path.media, it, wi);
			path.specular = sampled != nullptr && sampled->matches(BxDF::Type::SPECULAR);
		} else {
			const Material* medium = highestPriority(path.media);
			// direct lighting from light sources
			rng2->next(rnd);
			emit = scene->sampleLe(it.geometry.gp, path.ray.l, path.ray.t, rnd, &lgtIt.geometry, &pdf);
			if (isValid(emit) && isValid(pdf)) {
				Ray shadow;
				shadow.l = path.ray.l;
				lgtList = path.media;
				Scene::scatter(shadow, it.geometry.gp, lgtIt.geometry.gp);
				wi = shadow.d;
				if (!scene->intersection(shadow, lgtList, rng1, &beta, &lgtIt)) {
					phase = medium->evaluatePhase(it.geometry.gp, path.ray.l, path.ray.t, wi, wo);
					if (isValid(phase)) {
						weight = balanceHeuristic(pdf, medium->samplePhaseWiPDF(it.geometry.gp, path.ray.l, path.ray.t, wo, wi));
						if (isValid(weight) && isValid(beta)) {
							path.val += path.trs * beta * emit * phase * weight / pdf;
						}
					}
				}
			}
			// direct lighting from phase function
			rng2->next(rnd);
			phase = medium->samplePhaseWi(it.geometry.gp, path.ray.l, path.ray.t, wo, rnd, &wi, &pdf);
			if (isValid(phase) && isValid(pdf)) {
				Ray shadow;
				shadow.l = path.ray.l;
				lgtList = path.media;
				Scene::scatter(shadow, it.geometry.gp, wi);
				if (scene->intersection(shadow, lgtList, rng1, &beta, &lgtIt)) {
					emit = lgtIt.emission();
					if (isValid(emit)) {
						weight = balanceHeuristic(pdf, scene->sampleLePDF(it.geometry.gp, path.ray.l, path.ray.t, lgtIt));
						if (isValid(weight) && isValid(beta)) {
							path.val += path.trs * beta * emit * phase * weight / pdf;
						}
					}
				}
			}
			// possibly terminate path
			if (++path.depth >= maxPathLength) {
				path.finished = true;
				return;
			}
			// sample next direction according to phase function
			rng2->next(rnd);
			phase = medium->samplePhaseWi(it.geometry.gp, path.ray.l, path.ray.t, wo, rnd, &wi, &pdf);
			path.trs *= phase / pdf;
			Scene::scatter(path.ray, it.geometry.gp, wi);
			path.specular = false;
		}
		if (isInvalid(path.trs)) {
			path.finished = true;
			return;
		}
		// russian roulete
		if (path.depth >= minPathLength) {
			rng1->next(rnd);
			float threshold = clamp(path.trs, 0.01f, 0.99f);
			if (rnd[0] > threshold) {
				path.finished = true;
				return;
			}
			path.trs /= threshold;
		}
	}

	__host__
	void doRender(Scene* scene, Task* task, Workload& workload, RNG* camRNG, RNG* rng1, RNG* rng2, const bool progressive) {
		Sensor* sensor = scene->camera()->sensor();
		Path path;
		size_t t;
		if (progressive) {
			while(workload.next(&t)) {
				TiledBuffer::Tile& tile = (*task->tiles())[t];
				for (size_t r = tile.rowMin(); r < tile.rowMax(); ++r) {
					for (size_t c = tile.colMin(); c < tile.colMax(); ++c) {
						init(path, r, c, scene, camRNG);
						while (!path.finished) bounce(scene, rng1, rng2, sensor, path);
						tile.addSample(path.y, path.x, path.ray.l, path.val, path.pdf);
					}
				}
				progress->log(tile.size());
				if (task->updateAndFinishIfOutOfTime()) {
					break;
				}
			}
		} else {
			while(workload.next(&t)) {
				TiledBuffer::Tile& tile = (*task->tiles())[t];
				for (size_t r = tile.rowMin(); r < tile.rowMax(); ++r) {
					for (size_t c = tile.colMin(); c < tile.colMax(); ++c) {
						for (size_t s = 0; s < spp; ++s) {
							init(path, r, c, scene, camRNG);
							while (!path.finished) bounce(scene, rng1, rng2, sensor, path);
							tile.addSample(path.y, path.x, path.ray.l, path.val, path.pdf);
						}
						progress->log(spp);
						if (task->updateAndFinishIfOutOfTime()) {
							break;
						}
					}
				}
			}
		}
	}

public:
	__host__
	CPUTiledPathTracer(
		const size_t spp,
		const size_t minPathLength,
		const size_t maxPathLength,
		const size_t threads,
		const size_t timeBudget,
		const TileSize tileSize,
		Pointer<CPURNGFactory>& rngFactory,
		Pointer<ProgressTracker<uint64_t>>& tracker,
		const float time
	):
		spp(spp),
		minPathLength(minPathLength),
		maxPathLength(maxPathLength),
		threads(threads),
		timeBudget(timeBudget),
		tileSize(tileSize),
		rngFactory(rngFactory),
		progress(tracker),
		task(),
		time(time) {
	}

	__host__
	void render(Scene* scene, const bool progressive) override {
		// initialize
		Sensor* sensor = scene->camera()->sensor();
		sensor->clear();
		size_t w = sensor->width();
		size_t h = sensor->height();
		size_t n = w * h;
		progress->initialize(n * spp);

		// build tiles
		assert(task.isNull());
		task = Pointer<Task>::make(Context::cpuOnlyMemoryPool(), Pointer<TiledBuffer>::make(Context::cpuOnlyMemoryPool(), w, h, tileSize.w, tileSize.h, sensor->filter(), sensor->colorspace()), sensor->raster(), timeBudget);
		Workload workload(*(task->tiles()), progressive ? spp : 1);

		// random number generators
		size_t nthreads = threads == 0 ? std::thread::hardware_concurrency() : threads;
		CPUBasicRNGFactory<> uncorrelatedFactory;
		Array<Pointer<RNG>> camRNG(Context::cpuOnlyMemoryPool(), nthreads);
		Array<Pointer<RNG>> rng1(Context::cpuOnlyMemoryPool(), nthreads);
		Array<Pointer<RNG>> rng2(Context::cpuOnlyMemoryPool(), nthreads);
		for (size_t k = 0; k < nthreads; k++) {
			camRNG[k] = progressive ?
					uncorrelatedFactory.build(Context::cpuOnlyMemoryPool(), 6) :
					rngFactory->build(Context::cpuOnlyMemoryPool(), 6);
			rng1[k] = uncorrelatedFactory.build(Context::cpuOnlyMemoryPool(), 1);
			rng2[k] = uncorrelatedFactory.build(Context::cpuOnlyMemoryPool(), 2);
		}

		// render
		task->resetTimer();
		Array<std::future<void>> results(Context::cpuOnlyMemoryPool(), nthreads);
		for (size_t t = 0; t < nthreads; t++) {
			results[t] = std::async(
				std::launch::async,
				&CPUTiledPathTracer::doRender,
				this,
				scene,
				task.get(),
				std::ref(workload),
				camRNG[t].get(),
				rng1[t].get(),
				rng2[t].get(),
				progressive
			);
		}
		for (uint32_t t = 0; t < nthreads; t++) {
			results[t].wait();
		}
		// done
		task->updateAndFinish();
		progress->terminate();
	}

	__host__
	ProgressTracker<uint64_t>* tracker() {
		return progress.get();
	}

	__host__
	void updateSensor() override {
		if (!task.isNull()) {
			task->update();
		}
	}

	__host__
	void preview(HDRImage<float, 3>* image) override {
		if (!task.isNull()) {
			updateSensor();
			task->preview(image);
		}
	}
};

}

#endif /* CPUTILEDPATHTRACER_CUH_ */

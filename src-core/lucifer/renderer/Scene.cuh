#ifndef SCENE_CUH_
#define SCENE_CUH_

#include "Viewport.cuh"
#include "lucifer/camera/Camera.cuh"
#include "lucifer/light/LightDistribution.cuh"
#include "lucifer/material/InteriorListUtils.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/primitive/Primitive.cuh"

namespace lucifer {

/*!
 * \brief
 * The main node of the scene graph.
 */
class Scene: public Polymorphic {
private:
	Pointer<Camera> camera_;
	const Viewport viewport_;
	const Pointer<const Primitive> primitive_;
	const Pointer<const LightDistribution> distribution_;

public:

	__host__ __device__
	static void adjustInteriorList(InteriorList& list, const Primitive::Intersection& at, const Vectr3F& to) {
		if ((at.primitive == nullptr || at.material()->hasVolumetricMedium()) && !sameHemisphere(at.geometry.gn, at.direction, to)) {
			// crossing
			if (dot(at.geometry.gn, to) > 0.f) {
				// leaving
				if (!list.isEmpty()) list.remove(at.material());
			} else {
				// entering
				if (!list.isFull()) list.add(at.material());
			}
		}
	}

public:
	/*!
	 * \brief
	 * Creates a new scene object.
	 *
	 * \param[in] camera Camera used to generated rays.
	 * \param[in] primitive Compositive primitive.
	 * \param[in] distribution Light distribution used to sample light sources.
	 */
	__host__
	Scene(Pointer<Camera>& camera, const Viewport& viewport, const Pointer<const Primitive>& primitive, const Pointer<const LightDistribution>& distribution):
	camera_(camera), viewport_(viewport), primitive_(primitive), distribution_(distribution) {
	}

	__host__ __device__
	Camera* camera() {
		return camera_.get();
	}

	__host__ __device__
	const Camera* camera() const {
		return camera_.get();
	}

	__host__ __device__
	const Viewport& viewport() const {
		return viewport_;
	}

	__host__ __device__
	const Pointer<const Primitive>& primitive() const {
		return primitive_;
	}

	__host__ __device__
	const Pointer<const LightDistribution>& distribution() const {
		return distribution_;
	}

	__host__ __device__
	void initInteriorList(const Point3F& o, const Vectr3F& d, float t, InteriorList& list) const {
		InteriorList control;
		list.clear();
		Ray r(o, d, RangeF(1E-3, +INFINITY), 0.f, t);
		Primitive::Intersection it;
		while (primitive_->intersection(r, &it)) {
			const Material* material = it.material();
			if (material->hasVolumetricMedium()) {
				if (dot(it.geometry.gn, r.d) > 0.f) {
					// leaving
					if (!control.contains(material) && !list.isFull()) {
						list.add(material);
					}
					if (!control.isEmpty()) {
						control.remove(material);
					}
				} else {
					// entering
					if (!control.isFull()) {
						control.add(material);
					}
				}
			}
			r.r.lower = r.r.upper + 1E-3;
			r.r.upper = +INFINITY;
		}
	}

	__host__ __device__
	inline void initInteriorList(const Ray& ray, InteriorList& list) const {
		initInteriorList(ray.o, ray.d, ray.t, list);
	}

	__host__ __device__
	static void scatter(Ray& ray, const Point3F& at, const Vectr3F& to) {
		ray.o = at;
		ray.d = to;
		ray.r.lower = 0.f;
		ray.r.upper = HUGE_VALF;
	}

	__host__ __device__
	static void scatter(Ray& ray, const Point3F& at, const Point3F& to) {
		ray.o = at;
		ray.d = to - at;
		float length = ray.d.length();
		ray.d /= length;
		ray.r.lower = 0.f;
		ray.r.upper = length - 1E-3;
	}

	__host__ __device__
	static void scatter(Ray& ray, InteriorList& list, const Primitive::Intersection& at, const Vectr3F& to) {
		ray.o = at.geometry.gp;
		ray.d = to;
		ray.r.lower = 1E-3;
		ray.r.upper = HUGE_VALF;
		adjustInteriorList(list, at, ray.d);
	}

	__host__ __device__
	static void scatter(Ray& ray, InteriorList& list, const Primitive::Intersection& at, const Point3F& to) {
		ray.o = at.geometry.gp;
		ray.d = to - at.geometry.gp;
		float length = ray.d.length();
		ray.d /= length;
		ray.r.lower = 1E-3;
		ray.r.upper = length - 1E-3;
		adjustInteriorList(list, at, ray.d);
	}

	__host__ __device__
	bool intersection(Ray& ray, InteriorList& list, RNG* rng1D, float* transmittance, Primitive::Intersection* it) const {
		*transmittance = 1.f;
		while(true) {
			float rmax = ray.r.upper;
			bool found = primitive_->intersection(ray, it);
			const Material* medium = highestPriority(list);
			uint8_t priority = medium == nullptr ? 0 : medium->prioriry();
			*transmittance *= (medium == nullptr) ? 1.f : medium->transmittance(ray, rng1D);
			if (!found) {
				return false;
			}
			if ((it->hasOpticallyInteractingBSDF() || it->isEmissive()) && (!it->material()->hasVolumetricMedium() || it->material()->prioriry() >= priority)) {
				return true;
			}
			if (it->material()->hasVolumetricMedium()) {
				if (dot(it->geometry.gn, ray.d) >= 0) {
					// leaving
					if (!list.isEmpty()) list.remove(it->material());
				} else {
					// entering
					if (!list.isFull()) list.add(it->material());
				}
			}
			ray.r.lower = ray.r.upper + 1E-3;
			ray.r.upper = rmax;
		}
	}

	__host__ __device__
	inline float sampleLe(const Point3F& ref, const float l, const float time, float rnd[2], Geometry* geometry, float* pdf) const {
		return distribution_->sampleLe(ref, l, time, rnd, geometry, pdf);
	}

	__host__ __device__
	inline float sampleLePDF(const Point3F& ref, const float l, const float time, const Primitive::Intersection& intersection) const {
		return distribution_->sampleLePDF(ref, l, time, intersection);
	}

	__host__ __device__
	inline void sampleLeGeo(const Point3F& ref, const float l, const float time, float rnd[2], Geometry* geometry, const GeometricPrimitive** primitive, float* pdf) const {
		distribution_->sampleGeometry(ref, l, time, rnd, geometry, pdf, primitive);
	}

//	__host__ __device__
//	inline float sampleLeGeoPDF(const Point3F& ref, const float l, const float time, const Point3F& geometry, const GeometricPrimitive* primitive) const {
//		// TODO:: sample according to reference point
//		return distribution_->sampleGeometryPDF(l, time, geometry, primitive);
//	}

	__host__ __device__
	inline void sampleLeGeo(const float l, const float time, float rnd[3], Geometry* geometry, const GeometricPrimitive** primitive, float* pdf) const {
		distribution_->sampleGeometry(l, time, rnd, geometry, pdf, primitive);
	}

	__host__ __device__
	inline float sampleLeGeoPDF(const float l, const float time, const Point3F& geometry, const GeometricPrimitive* primitive) const {
		return distribution_->sampleGeometryPDF(l, time, geometry, primitive);
	}

	__host__ __device__
	bool interaction(Ray& ray, InteriorList& list, Primitive::Intersection* it, float* transmittance, RNG* rng1D ) const {
		float tr = *transmittance = 1.f;
		Point3F pt;
		while(true) {
			tr = 1.f;
			// intesect
			float rmax = ray.r.upper;
			if (!primitive_->intersection(ray, it)) {
				return false;
			}
			// sample current medium
			const Material* medium = highestPriority(list);
			uint8_t priority = medium == nullptr ? 0 : medium->prioriry();
			bool sampledMedium = medium != nullptr && medium->sampleMedium(ray, rng1D, &pt, &tr);
			*transmittance *= tr;
			if (sampledMedium) {
				// medium interaction
				it->geometry.gp = pt;
				it->primitive = nullptr;
				return true;
			} else {
				if ((it->hasOpticallyInteractingBSDF() || it->isEmissive()) && (!it->material()->hasVolumetricMedium() || it->material()->prioriry() >= priority)) {
					// true surface interaction
					return true;
				}
				// false surface interaction, respawn ray and continue
				if (it->material()->hasVolumetricMedium()) {
					if (dot(it->geometry.gn, ray.d) >= 0) {
						// leaving
						if (!list.isEmpty()) list.remove(it->material());
					} else {
						// entering
						if (!list.isFull()) list.add(it->material());
					}
				}
				ray.r.lower = ray.r.upper + 1E-3;
				ray.r.upper = rmax;
			}
		}
	}
};

}

#endif /* SCENE_CUH_ */

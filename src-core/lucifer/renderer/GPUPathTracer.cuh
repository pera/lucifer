#ifndef GPUPATHTRACER_CUH_
#define GPUPATHTRACER_CUH_

#include <cassert>
#include <vector>
#include <thrust/device_vector.h>
#include <thrust/partition.h>
#include <thrust/functional.h>
#include <thrust/system/cuda/execution_policy.h>

#include "Renderer.cuh"
#include "lucifer/material/InteriorListUtils.cuh"
#include "lucifer/random/GPUBasicRNG.cuh"
#include "lucifer/system/CudaEnvironment.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/util/Context.cuh"
#include "lucifer/util/ThrustDeviceCachedAllocator.cuh"


namespace lucifer {

namespace gpupt {

enum class PathState {
	GENERATE,
	INTERSECT,
	SHADE_SURFACE,
	SHADE_MEDIUM,
	FILTER,
	FINISHED
};

class Path {
public:
	// path id
	uint32_t col;
	uint32_t row;
	RNG* rng1;
	RNG* rng2;
	RNG* rng6;
	// render data
	Ray ray;
	float x;
	float y;
	float val;
	float trs;
	float pdf;
	bool specular;
	bool finished;
	int16_t depth;
	Primitive::Intersection it;
	InteriorList media;
};

class Tile {
public:
	int32_t cMin;
	int32_t rMin;
	int32_t cMax;
	int32_t rMax;

public:
	__host__ __device__
	Tile(const int32_t cMin, const int32_t rMin, const int32_t cMax, const int32_t rMax):
	cMin(cMin), rMin(rMin), cMax(cMax), rMax(rMax) {
		assert(cMin < cMax);
		assert(rMin < rMax);
	}

	__host__ __device__
	inline uint32_t rows() const {
		return rMax - rMin;
	}

	__host__ __device__
	inline uint32_t cols() const {
		return cMax - cMin;
	}

	__host__ __device__
	inline uint64_t size() const {
		return static_cast<uint64_t>(rows()) * static_cast<uint64_t>(cols());
	}
};

template<typename T = void> __global__
void clearSensorKernel(Sensor* sensor, size_t n) {
	auto i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < n) {
		auto x = i % sensor->width();
		auto y = i / sensor->width();
		(*sensor->raster())(y, x) = Color3F(0.f, 0.f, 0.f);
	}
}

template<typename T = void>__global__
void initPathsKernel(Path** pathPtr, Path* path, PathState* state, Pointer<RNG>* rng1, Pointer<RNG>* rng2, Pointer<RNG>* rng6, const Tile tile) {
	auto i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < tile.size()) {
		pathPtr[i] = &path[i];
		path[i].row = tile.rMin + i / tile.cols();
		path[i].col = tile.cMin + i % tile.cols();
		path[i].rng1 = rng1[i].get();
		path[i].rng2 = rng2[i].get();
		path[i].rng6 = rng6[i].get();
		state[i] = PathState::GENERATE;
	}
}

template<typename T = void> __device__
void generateRay(Path* path, PathState* state, Scene* scene) {
	float rnd[6];
	path->rng6->next(rnd);
	path->x = path->col + rnd[0];
	path->y = path->row + rnd[1];
	path->val = 0.f;
	path->trs = 1.f;
	path->specular = false;
	path->finished = false;
	path->it.primitive = nullptr;
	path->depth = 0;
	rnd[4] = scene->camera()->sensor()->colorspace()->xyzHistogram()->sample(rnd[4], &path->pdf);
	path->ray = scene->camera()->ray(path->x, path->y, rnd[2], rnd[3], rnd[4], rnd[5]);
	scene->initInteriorList(path->ray, path->media);
	*state = PathState::INTERSECT;
}

template<typename T = void> __global__
void generateRaysKernel(Path** path, PathState* state, size_t n, Scene* scene) {
	auto i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < n) {
		assert(state[i] == PathState::GENERATE);
		generateRay(path[i], &state[i], scene);
	}
}

template<typename T = void> __device__
void intersectScene(Path* path, PathState* state, Scene* scene) {
	float beta;
	if (!scene->interaction(path->ray, path->media, &path->it, &beta, path->rng1)) {
		*state = PathState::FILTER;
		return;
	}
	path->trs *= beta;
	if (isInvalid(path->trs)) {
		*state = PathState::FILTER;
		return;
	}
	*state = path->it.onSurface() ? PathState::SHADE_SURFACE : PathState::SHADE_MEDIUM;
}

template<typename T = void> __global__
void intersectSceneKernel(Path** path, PathState* state, size_t n, Scene* scene) {
	auto i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < n) {
		assert(state[i] == PathState::INTERSECT);
		intersectScene(path[i], &state[i], scene);
	}
}
template<typename T = void> __device__
void shadeSurface(Path* path, PathState* state, Scene* scene, uint16_t minPathLength, uint16_t maxPathLength) {
	BxDF const* sampled;
	float emit, pdf, beta, bsdf, weight, rnd[2];
	Vectr3F wi;
	Primitive::Intersection lgtIt;
	InteriorList lgtList;
	Ray shadow;
	// surface emission
	if (path->depth == 0 || path->specular) {
		path->val += path->trs * path->it.emission();
	}
	if (path->it.matchesBSDF(BxDF::Type::DIFFUSE)) {
		// direct lighting from light sources
		path->rng2->next(rnd);
		emit = scene->sampleLe(path->it.geometry.gp, path->ray.l, path->ray.t, rnd, &lgtIt.geometry, &pdf);
		if (emit > 0 && pdf > 0) {
			shadow = path->ray;
			lgtList = path->media;
			Scene::scatter(shadow, lgtList, path->it, lgtIt.geometry.gp);
			wi = shadow.d;
			if (!scene->intersection(shadow, lgtList, path->rng1, &beta, &lgtIt)) {
				bsdf = path->it.evaluateBSDF(wi, path->media, BxDF::Mode::RADIANCE, path->rng1, BxDF::Type::DIFFUSE);
				if (bsdf > 0) {
					weight = balanceHeuristic(pdf, path->it.sampleBSDFWiPDF(wi, path->media, BxDF::Mode::RADIANCE, path->rng1, BxDF::Type::DIFFUSE));
					if (isValid(weight) && isValid(beta)) {
						path->val += path->trs * beta * emit * absDot(path->it.geometry.sn, wi) * bsdf * weight / pdf;
					}
				}
			}
		}
		// direct lighting from bsdf
		path->rng2->next(rnd);
		bsdf = path->it.sampleBSDFWi(path->media, BxDF::Mode::RADIANCE, BxDF::Type::DIFFUSE, rnd, &wi, &pdf, path->rng1);
		if (bsdf > 0 && pdf > 0) {
			shadow = path->ray;
			lgtList = path->media;
			Scene::scatter(shadow, lgtList, path->it, wi);
			if (scene->intersection(shadow, lgtList, path->rng1, &beta, &lgtIt)) {
				emit = lgtIt.emission();
				if (emit > 0) {
					weight = balanceHeuristic(path->it.sampleBSDFWiPDF(wi, path->media, BxDF::Mode::RADIANCE, path->rng1, BxDF::Type::DIFFUSE), scene->sampleLePDF(path->it.geometry.gp, path->ray.l, path->ray.t, lgtIt));
					if (isValid(weight) && isValid(beta)) {
						path->val += path->trs * beta * emit * absDot(path->it.geometry.sn, wi) * bsdf * weight / pdf;
					}
				}
			}
		}
	}
	// possibly terminate path
	if (++(path->depth) >= maxPathLength) {
		*state = PathState::FILTER;
		return;
	}
	// sample next direction according to bsdf
	sampled = nullptr;
	path->rng2->next(rnd);
	bsdf = path->it.sampleBSDFWi(path->media, BxDF::Mode::RADIANCE, BxDF::Type(0), rnd, &wi, &pdf, path->rng1, &sampled);
	path->trs *= absDot(path->it.geometry.sn, wi) * bsdf / pdf;
	Scene::scatter(path->ray, path->media, path->it, wi);
	path->specular = sampled != nullptr && sampled->matches(BxDF::Type::SPECULAR);
	if (isInvalid(path->trs)) {
		*state = PathState::FILTER;
		return;
	}
	// russian roulete
	if (path->depth >= minPathLength) {
		path->rng1->next(rnd);
		float threshold = clamp(path->trs, 0.01f, 0.99f);
		if (rnd[0] > threshold) {
			*state = PathState::FILTER;
			return;
		}
		path->trs /= threshold;
	}
	*state = PathState::INTERSECT;
}

template<typename T = void> __global__
void shadeSurfaceKernel(Path** path, PathState* state, size_t n, Scene* scene, uint16_t minPathLength, uint16_t maxPathLength) {
	auto i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < n) {
		assert(state[i] == PathState::SHADE_SURFACE);
		shadeSurface(path[i], &state[i], scene, minPathLength, maxPathLength);
	}
}

template<typename T = void> __device__
void shadeMedium(Path* path, PathState* state, Scene* scene, uint16_t minPathLength, uint16_t maxPathLength) {
	// local variables
	Vectr3F wi, wo = -path->ray.d;
	float rnd[2], emit, pdf, beta, weight, phase;
	Primitive::Intersection lgtIt;
	InteriorList lgtList;
	const Material* medium = highestPriority(path->media);
	// direct lighting from light sources
	path->rng2->next(rnd);
	emit = scene->sampleLe(path->it.geometry.gp, path->ray.l, path->ray.t, rnd, &lgtIt.geometry, &pdf);
	if (isValid(emit) && isValid(pdf)) {
		Ray shadow;
		shadow.l = path->ray.l;
		lgtList = path->media;
		Scene::scatter(shadow, path->it.geometry.gp, lgtIt.geometry.gp);
		wi = shadow.d;
		if (!scene->intersection(shadow, lgtList, path->rng1, &beta, &lgtIt)) {
			phase = medium->evaluatePhase(path->it.geometry.gp, path->ray.l, path->ray.t, wi, wo);
			if (isValid(phase)) {
				weight = balanceHeuristic(pdf, medium->samplePhaseWiPDF(path->it.geometry.gp, path->ray.l, path->ray.t, wo, wi));
				if (isValid(weight) && isValid(beta)) {
					path->val += path->trs * beta * emit * phase * weight / pdf;
				}
			}
		}
	}
	// direct lighting from phase function
	path->rng2->next(rnd);
	phase = medium->samplePhaseWi(path->it.geometry.gp, path->ray.l, path->ray.t, wo, rnd, &wi, &pdf);
	if (isValid(phase) && isValid(pdf)) {
		Ray shadow;
		shadow.l = path->ray.l;
		lgtList = path->media;
		Scene::scatter(shadow, path->it.geometry.gp, wi);
		if (scene->intersection(shadow, lgtList, path->rng1, &beta, &lgtIt)) {
			emit = lgtIt.emission();
			if (isValid(emit)) {
				weight = balanceHeuristic(pdf, scene->sampleLePDF(path->it.geometry.gp, path->ray.l, path->ray.t, lgtIt));
				if (isValid(weight) && isValid(beta)) {
					path->val += path->trs * beta * emit * phase * weight / pdf;
				}
			}
		}
	}
	// possibly terminate path
	if (++(path->depth) >= maxPathLength) {
		*state = PathState::FILTER;
		return;
	}
	// sample next direction according to phase function
	path->rng2->next(rnd);
	phase = medium->samplePhaseWi(path->it.geometry.gp, path->ray.l, path->ray.t, wo, rnd, &wi, &pdf);
	path->trs *= phase / pdf;
	Scene::scatter(path->ray, path->it.geometry.gp, wi);
	path->specular = false;
	if (isInvalid(path->trs)) {
		*state = PathState::FILTER;
		return;
	}
	// russian roulete
	if (path->depth >= minPathLength) {
		path->rng1->next(rnd);
		float threshold = clamp(path->trs, 0.01f, 0.99f);
		if (rnd[0] > threshold) {
			*state = PathState::FILTER;
			return;
		}
		path->trs /= threshold;
	}
	*state = PathState::INTERSECT;
}

template<typename T = void> __global__
void shadeMediumKernel(Path** path, PathState* state, size_t n, Scene* scene, uint16_t minPathLength, uint16_t maxPathLength) {
	auto i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < n) {
		assert(state[i] == PathState::SHADE_MEDIUM);
		shadeMedium(path[i], &state[i], scene, minPathLength, maxPathLength);
	}
}

template<typename T = void> __device__
void filterImage(Path* path, PathState* state, Scene* scene) {
	scene->camera()->sensor()->atomicAdd(path->x, path->y, path->ray.l, path->val, path->pdf);
}

template<typename T = void> __global__
void filterImageKernel(Path** path, PathState* state, size_t n, Scene* scene) {
	auto i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < n) {
		assert(state[i] == PathState::FILTER);
		filterImage(path[i], &state[i], scene);
	}
}

template<typename T = void> __global__
void previewKernel(const HDRImage<float, 3>* src, HDRImage<float, 3>* dst) {
//	src->copyTo(dst);
	*dst = *src;
}

}

class GPUPathTracer: public Renderer {
private:
	class Streams {
	public:
		cudaStream_t partition;
		cudaStream_t generate;
		cudaStream_t intersect;
		cudaStream_t shadeSurface;
		cudaStream_t shadeMedium;
		cudaStream_t filter;

	public:
		__host__
		Streams() {
			CUDA_CHECK(cudaStreamCreate(&partition));
			CUDA_CHECK(cudaStreamCreate(&generate));
			CUDA_CHECK(cudaStreamCreate(&intersect));
			CUDA_CHECK(cudaStreamCreate(&shadeSurface));
			CUDA_CHECK(cudaStreamCreate(&shadeMedium));
			CUDA_CHECK(cudaStreamCreate(&filter));
		}

		__host__
		~Streams() {
			CUDA_CHECK(cudaStreamDestroy(partition));
			CUDA_CHECK(cudaStreamDestroy(generate));
			CUDA_CHECK(cudaStreamDestroy(intersect));
			CUDA_CHECK(cudaStreamDestroy(shadeSurface));
			CUDA_CHECK(cudaStreamDestroy(shadeMedium));
			CUDA_CHECK(cudaStreamDestroy(filter));
		}
	};

	class Tiles {
	private:
		std::vector<gpupt::Tile> tiles;

	public:
		__host__
		Tiles(const uint32_t wImg, const uint32_t hImg, uint32_t wTile, uint32_t hTile) {
			// build tiles
			wTile = min(wTile, wImg);
			hTile = min(hTile, hImg);
			const int32_t rw = wImg % wTile;
			const int32_t rh = hImg % hTile;
			const int32_t cols = wImg / wTile + (rw > 0 ? 1 : 0);
			const int32_t rows = hImg / hTile + (rh > 0 ? 1 : 0);
			for (int32_t r = 0, i = 0; r < rows; ++r) {
				int32_t hMin = r * hTile;
				int32_t hMax = hMin + (r == rows - 1 && rh > 0 ? rh : hTile);
				for (int32_t c = 0; c < cols; ++c, ++i) {
					int32_t wMin = c * wTile;
					int32_t wMax = wMin + (c == cols - 1 && rw > 0 ? rw : wTile);
					tiles.emplace_back(wMin, hMin, wMax, hMax);
				}
			}
		}

		inline size_t size() const {
			return tiles.size();
		}

		inline const gpupt::Tile& operator[](const size_t i) const {
			return tiles[i];
		}
	};

private:
	uint32_t spp;
	uint16_t minPathLength;
	uint16_t maxPathLength;
	uint32_t tileWidth;
	uint32_t tileHeight;
	Pointer<GPURNGFactory> rngFactory;
	Pointer<ProgressTracker<uint64_t>> progress;
	Pointer<HDRImage<float, 3>> output;

private:
	static constexpr size_t THREADS_PER_BLOCK = 64;
	static constexpr float THRESHOLD = 1.f / 5.f;

private:
	static void clearSensor(Sensor* sensor) {
		auto n = sensor->width() * sensor->height();
		auto blocks = (n + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
		gpupt::clearSensorKernel<<<blocks, THREADS_PER_BLOCK>>>(sensor, n);
		CUDA_CHECK(cudaPeekAtLastError());
	}

	void renderTile(ThrustDeviceCachedAllocator& allocator, Streams& streams, thrust::device_vector<gpupt::Path*>& pathPtr, gpupt::Path* path, thrust::device_vector<gpupt::PathState>& state, Pointer<RNG>* rng1, Pointer<RNG>* rng2, Pointer<RNG>* rng6, const gpupt::Tile& tile, Scene* scene) {
		// initialize
		auto blocks = (tile.size() + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
		gpupt::initPathsKernel<<<blocks, THREADS_PER_BLOCK, 0, streams.generate>>>(thrust::raw_pointer_cast(&pathPtr[0]), path, thrust::raw_pointer_cast(&state[0]), rng1, rng2, rng6, tile);
		blocks = (tile.size() + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
		gpupt::generateRaysKernel<<<blocks, THREADS_PER_BLOCK, 0, streams.generate>>>(thrust::raw_pointer_cast(&pathPtr[0]), thrust::raw_pointer_cast(&state[0]), tile.size(), scene);
		CUDA_CHECK(cudaStreamSynchronize(streams.generate));
		// bounce
		auto begin = state.begin();
		auto end   = begin + tile.size();
		auto policy = thrust::cuda::par(allocator).on(streams.partition);
		do {
			// sort and partition paths
			thrust::sort_by_key(policy, begin, end, pathPtr.begin());
			auto fst = begin;
			auto intBegin = thrust::find(policy, fst, end, gpupt::PathState::INTERSECT);
			if (intBegin < end) fst = intBegin;
			auto surBegin = thrust::find(policy, fst, end, gpupt::PathState::SHADE_SURFACE);
			if (surBegin < end) fst = surBegin;
			auto medBegin = thrust::find(policy, fst, end, gpupt::PathState::SHADE_MEDIUM);
			if (medBegin < end) fst = medBegin;
			end = thrust::find(policy, fst, end, gpupt::PathState::FILTER);
			CUDA_CHECK(cudaStreamSynchronize(streams.partition));
			// intersect paths
			auto last = min(min(surBegin, medBegin), end);
			if (intBegin < last) {
				auto first = intBegin - begin;
				auto count = last - intBegin;
				assert(count > 0);
				blocks = (count + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
				gpupt::intersectSceneKernel<<<blocks, THREADS_PER_BLOCK, 0, streams.intersect>>>(thrust::raw_pointer_cast(&pathPtr[first]), thrust::raw_pointer_cast(&state[first]), count, scene);
			}
			// shade surface
			last = min(medBegin, end);
			if (surBegin < last) {
				auto first = surBegin - begin;
				auto count = last - surBegin;
				assert(count > 0);
				blocks = (count + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
				gpupt::shadeSurfaceKernel<<<blocks, THREADS_PER_BLOCK, 0, streams.shadeSurface>>>(thrust::raw_pointer_cast(&pathPtr[first]), thrust::raw_pointer_cast(&state[first]), count, scene, minPathLength, maxPathLength);
			}
			// shade medium
			last = end;
			if (medBegin < last) {
				auto first = medBegin - begin;
				auto count = last - medBegin;
				assert(count > 0);
				blocks = (count + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
				gpupt::shadeMediumKernel<<<blocks, THREADS_PER_BLOCK, 0, streams.shadeMedium>>>(thrust::raw_pointer_cast(&pathPtr[first]), thrust::raw_pointer_cast(&state[first]), count, scene, minPathLength, maxPathLength);
			}
			CUDA_CHECK(cudaDeviceSynchronize());

		} while(end != begin);
		// filter
		blocks = (tile.size() + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
		gpupt::filterImageKernel<<<blocks, THREADS_PER_BLOCK, 0, streams.filter>>>(thrust::raw_pointer_cast(&pathPtr[0]), thrust::raw_pointer_cast(&state[0]), tile.size(), scene);
		CUDA_CHECK(cudaStreamSynchronize(streams.filter));
		progress->log(tile.size());
	}

public:
	__host__
	GPUPathTracer(const uint32_t spp, const uint16_t minPathLength, const uint16_t maxPathLength, const uint32_t tileWidth, const uint32_t tileHeight, const Pointer<GPURNGFactory>& rngFactory, const Pointer<ProgressTracker<uint64_t>>& tracker):
	spp(spp), minPathLength(minPathLength), maxPathLength(maxPathLength), tileWidth(tileWidth), tileHeight(tileHeight), rngFactory(rngFactory), progress(tracker) {
		assert(spp > 0);
		assert(minPathLength > 0);
		assert(maxPathLength >= minPathLength);
	}

	__host__
	void render(Scene* scene, const bool progressive) override {
		if (!CudaEnvironment::HAS_UNIFIED_MEMORY) {
			std::cerr << "no unified memory capable cuda device found" << std::endl;
			return;
		}
		// initialize progress tracker
		output = scene->camera()->sensor()->raster();
		auto w = scene->camera()->sensor()->width();
		auto h = scene->camera()->sensor()->height();
		auto imageSize =  w * h;
		uint64_t samples = ((uint64_t)spp) * imageSize;
		progress->initialize(samples);

		// alloc path data
		const Tiles tiles(w, h, (tileWidth == 0 ? w : tileWidth), (tileHeight == 0 ? h : tileHeight));
		thrust::device_vector<gpupt::Path> path(tiles[0].size());
		thrust::device_vector<gpupt::Path*> pathPtr(tiles[0].size());
		thrust::device_vector<gpupt::PathState> state(tiles[0].size());

		// alloc RNG data
		GPUBasicRNGFactory uncorrelatedFactory;
		Array<Pointer<RNG>> rng6 = progressive ? uncorrelatedFactory.build(Context::managedMemoryPool(), 6, tiles[0].size()) : rngFactory->build(Context::managedMemoryPool(), 6, tiles[0].size());
		Array<Pointer<RNG>> rng1 = uncorrelatedFactory.build(Context::managedMemoryPool(), 1, tiles[0].size());
		Array<Pointer<RNG>> rng2 = uncorrelatedFactory.build(Context::managedMemoryPool(), 2, tiles[0].size());

		// render
		Streams streams;
		ThrustDeviceCachedAllocator allocator;
		clearSensor(scene->camera()->sensor());
		if (progressive) {
			for (uint16_t s = 0; s < spp; ++s) {
				for (size_t t = 0; t < tiles.size(); ++t) {
					renderTile(allocator, streams, pathPtr, thrust::raw_pointer_cast(&path[0]), state, rng1.begin(), rng2.begin(), rng6.begin(), tiles[t], scene);
				}
			}
		} else {
			for (size_t t = 0; t < tiles.size(); ++t) {
				for (uint16_t s = 0; s < spp; ++s) {
					renderTile(allocator, streams, pathPtr, thrust::raw_pointer_cast(&path[0]), state, rng1.begin(), rng2.begin(), rng6.begin(), tiles[t], scene);
				}
			}
		}

		// copy image from device to host memory
		CUDA_CHECK(cudaDeviceSynchronize());
		progress->terminate();
	}

	__host__
	ProgressTracker<uint64_t>* tracker() {
		return progress.get();
	}

	__host__
	void preview(HDRImage<float, 3>* image) {
		if (!output.isNull()) {
			CUDA_CHECK(cudaDeviceSynchronize());
			*image = *output;
		}
	}
};

}

#endif /* GPUPATHTRACER_CUH_ */

#ifndef CPUSINGLETHREADPATHTRACER_CUH_
#define CPUSINGLETHREADPATHTRACER_CUH_

#include "Renderer.cuh"
#include "lucifer/material/InteriorListUtils.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/random/CPUBasicRNG.cuh"

namespace lucifer {

class CPUSingleThreadPathTracer: public Renderer {
private:
	Pointer<CPURNGFactory> factory;
	uint32_t minPathLength;
	uint32_t maxPathLength;
	uint32_t spp;
	Pointer<HDRImage<float, 3>> output;
	Pointer<ProgressTracker<uint64_t>> progress;

	class Path {
	public:
		Ray ray;
		float x;
		float y;
		float val;
		float trs;
		float pdf;
		bool specular;
		bool finished;
		int16_t depth;
		InteriorList media;
	};

	__host__
	void clearImage(HDRImage<float, 3>* img) {
		for (uint32_t y = 0; y < img->height(); y++) {
			for (uint32_t x = 0; x < img->width(); x++) {
				(*img)(y, x) = Color3F(0, 0, 0);
			}
		}
	}

	__host__
	void init(Path& path, size_t r, size_t c, Scene* scene, RNG* camRNG) const {
		float rnd[6];
		camRNG->next(rnd);
		path.x = c + rnd[0];
		path.y = r + rnd[1];
		path.val = 0.f;
		path.trs = 1.f;
		path.specular = false;
		path.finished = false;
		rnd[4] = scene->camera()->sensor()->colorspace()->xyzHistogram()->sample(rnd[4], &path.pdf);
		path.ray = scene->camera()->ray(path.x, path.y, rnd[2], rnd[3], rnd[4], rnd[5]);
		path.depth = 0;
		scene->initInteriorList(path.ray, path.media);
	}

	__host__
	void bounce(Scene* scene, RNG* rng1, RNG* rng2, Sensor* sensor, Path& path) {
		Primitive::Intersection it, lgtIt;
		InteriorList lgtList;
		Vectr3F wi, wo = -path.ray.d;
		float emit, bsdf, phase, pdf, beta, weight, rnd[2];
		// scene interaction
		if (!scene->interaction(path.ray, path.media, &it, &beta, rng1)) {
			path.finished = true;
			return;
		}
		path.trs *= beta;
		if (isInvalid(path.trs)) {
			path.finished = true;
			return;
		}
		if (it.onSurface()) {
			// surface emission
			if (path.depth == 0 || path.specular) {
				path.val += path.trs * it.emission();
			}
			if (it.matchesBSDF(BxDF::Type::DIFFUSE)) {
				// direct lighting from light sources
				rng2->next(rnd);
				emit = scene->sampleLe(it.geometry.gp, path.ray.l, path.ray.t, rnd, &lgtIt.geometry, &pdf);
				if (emit > 0 && pdf > 0) {
					Ray shadow = path.ray;
					lgtList = path.media;
					Scene::scatter(shadow, lgtList, it, lgtIt.geometry.gp);
					wi = shadow.d;
					if (!scene->intersection(shadow, lgtList, rng1, &beta, &lgtIt)) {
						bsdf = it.evaluateBSDF(wi, path.media, BxDF::Mode::RADIANCE, rng1, BxDF::Type::DIFFUSE);
						if (bsdf > 0) {
							weight = 1.f;//balanceHeuristic(pdf, it.sampleBSDFWiPDF(wi, path.media, BxDF::Mode::RADIANCE, rng1, BxDF::Type::DIFFUSE));
							if (isValid(weight) && isValid(beta)) {
								path.val += path.trs * beta * emit * absDot(it.geometry.sn, wi) * bsdf * weight / pdf;
							}
						}
					}
				}
//				// direct lighting from bsdf
//				rng2->next(rnd);
//				bsdf = it.sampleBSDFWi(path.media, BxDF::Mode::RADIANCE, BxDF::Type::DIFFUSE, rnd, &wi, &pdf, rng1);
//				if (bsdf > 0 && pdf > 0) {
//					Ray shadow = path.ray;
//					lgtList = path.media;
//					Scene::scatter(shadow, lgtList, it, wi);
//					if (scene->intersection(shadow, lgtList, rng1, &beta, &lgtIt)) {
//						emit = lgtIt.emission();
//						if (emit > 0) {
//							weight = balanceHeuristic(it.sampleBSDFWiPDF(wi, path.media, BxDF::Mode::RADIANCE, rng1, BxDF::Type::DIFFUSE), scene->sampleLePDF(it.geometry.gp, path.ray.l, path.ray.t, lgtIt));
//							if (isValid(weight) && isValid(beta)) {
//								path.val += path.trs * beta * emit * absDot(it.geometry.sn, wi) * bsdf * weight / pdf;
//							}
//						}
//					}
//				}
			}
			// possibly terminate path
			if (++path.depth >= maxPathLength) {
				path.finished = true;
				return;
			}
			// sample next direction according to bsdf
			BxDF const* sampled = nullptr;
			rng2->next(rnd);
			bsdf = it.sampleBSDFWi(path.media, BxDF::Mode::RADIANCE, BxDF::Type(0), rnd, &wi, &pdf, rng1, &sampled);
			path.trs *= absDot(it.geometry.sn, wi) * bsdf / pdf;
			Scene::scatter(path.ray, path.media, it, wi);
			path.specular = sampled != nullptr && sampled->matches(BxDF::Type::SPECULAR);
		} else {
			const Material* medium = highestPriority(path.media);
			// direct lighting from light sources
			rng2->next(rnd);
			emit = scene->sampleLe(it.geometry.gp, path.ray.l, path.ray.t, rnd, &lgtIt.geometry, &pdf);
			if (isValid(emit) && isValid(pdf)) {
				Ray shadow;
				shadow.l = path.ray.l;
				lgtList = path.media;
				Scene::scatter(shadow, it.geometry.gp, lgtIt.geometry.gp);
				wi = shadow.d;
				if (!scene->intersection(shadow, lgtList, rng1, &beta, &lgtIt)) {
					phase = medium->evaluatePhase(it.geometry.gp, path.ray.l, path.ray.t, wi, wo);
					if (isValid(phase)) {
						weight = balanceHeuristic(pdf, medium->samplePhaseWiPDF(it.geometry.gp, path.ray.l, path.ray.t, wo, wi));
						if (isValid(weight) && isValid(beta)) {
							path.val += path.trs * beta * emit * phase * weight / pdf;
						}
					}
				}
			}
			// direct lighting from phase function
			rng2->next(rnd);
			phase = medium->samplePhaseWi(it.geometry.gp, path.ray.l, path.ray.t, wo, rnd, &wi, &pdf);
			if (isValid(phase) && isValid(pdf)) {
				Ray shadow;
				shadow.l = path.ray.l;
				lgtList = path.media;
				Scene::scatter(shadow, it.geometry.gp, wi);
				if (scene->intersection(shadow, lgtList, rng1, &beta, &lgtIt)) {
					emit = lgtIt.emission();
					if (isValid(emit)) {
						weight = balanceHeuristic(pdf, scene->sampleLePDF(it.geometry.gp, path.ray.l, path.ray.t, lgtIt));
						if (isValid(weight) && isValid(beta)) {
							path.val += path.trs * beta * emit * phase * weight / pdf;
						}
					}
				}
			}
			// possibly terminate path
			if (++path.depth >= maxPathLength) {
				path.finished = true;
				return;
			}
			// sample next direction according to phase function
			rng2->next(rnd);
			phase = medium->samplePhaseWi(it.geometry.gp, path.ray.l, path.ray.t, wo, rnd, &wi, &pdf);
			path.trs *= phase / pdf;
			Scene::scatter(path.ray, it.geometry.gp, wi);
			path.specular = false;
		}
		if (isInvalid(path.trs)) {
			path.finished = true;
			return;
		}
		// russian roulete
		if (path.depth >= minPathLength) {
			rng1->next(rnd);
			float threshold = clamp(path.trs, 0.01f, 0.99f);
			if (rnd[0] > threshold) {
				path.finished = true;
				return;
			}
			path.trs /= threshold;
		}
	}

public:
	__host__
	CPUSingleThreadPathTracer(
		Pointer<CPURNGFactory>& factory,
		uint32_t minPathLen, uint32_t maxPathLen, uint32_t spp,
		Pointer<ProgressTracker<uint64_t>>& progress
	): factory(factory), minPathLength(minPathLen), maxPathLength(maxPathLen), spp(spp), progress(progress) {
	}

	__host__
	virtual void render(Scene* scene, const bool progressive) override {
		// local memory pool
		MemoryPool memPool{32, new CPUAllocator{}};
		// image size
		Sensor* sensor = scene->camera()->sensor();
		uint32_t w = (uint32_t) sensor->width();
		uint32_t h = (uint32_t) sensor->height();
		size_t imgSize = w * h;
		// random number generators
		CPUBasicRNGFactory<> uncorrelatedFactory;
		Pointer<RNG> camRNG = progressive ? uncorrelatedFactory.build(memPool, 6) : factory->build(memPool, 6);
		Pointer<RNG> rng1 = uncorrelatedFactory.build(memPool, 1);
		Pointer<RNG> rng2 = uncorrelatedFactory.build(memPool, 3);
		// initialize
		sensor->clear();
		output = sensor->raster();
		progress->initialize(imgSize * spp);
		// render
		Path path;
		InteriorList media;
		if (progressive) {
			for (uint32_t s = 0; s < spp; s++) {
				for (uint32_t y = 0; y < h; y++) {
					for (uint32_t x = 0; x < w; x++) {
						if (x == 255 and y == 19) {
							printf("bingo\n");
						}
						init(path, y, x, scene, camRNG.get());
						while (!path.finished) bounce(scene, rng1.get(), rng2.get(), sensor, path);
						sensor->addSample(path.x, path.y, path.ray.l, path.val, path.pdf);
						progress->log(1);
					}
				}
			}
		} else {
			for (uint32_t y = 0; y < h; y++) {
				for (uint32_t x = 0; x < w; x++) {
					for (uint32_t s = 0; s < spp; s++) {
						init(path, y, x, scene, camRNG.get());
						while (!path.finished) bounce(scene, rng1.get(), rng2.get(), sensor, path);
						sensor->addSample(path.x, path.y, path.ray.l, path.val, path.pdf);
						progress->log(1);
					}
				}
			}
		}
		// done rendering
		progress->terminate();
	}

	__host__
	ProgressTracker<uint64_t>* tracker() override {
		return progress.get();
	}

	__host__
	void preview(HDRImage<float, 3>* image) override {
		if (!output.isNull()) {
			*image = *output;
		}
	}
};

}

#endif /* CPUSINGLETHREADPATHTRACER_CUH_ */

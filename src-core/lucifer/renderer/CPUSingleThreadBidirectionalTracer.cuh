#ifndef CPUSINGLETHREADBIDIRECTIONALTRACER_CUH_
#define CPUSINGLETHREADBIDIRECTIONALTRACER_CUH_

#include "Renderer.cuh"
#include "lucifer/image/HDRImage.cuh"
#include "lucifer/material/InteriorListUtils.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/memory/CPUAllocator.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/random/CPUBasicRNG.cuh"
#include "lucifer/statistics/Statistics.cuh"

namespace lucifer {

class CPUSingleThreadBidirectionalTracer: public Renderer {
private:
	Pointer<CPURNGFactory> factory;
	uint32_t minCamPathLen;
	uint32_t maxCamPathLen;
	uint32_t minLgtPathLen;
	uint32_t maxLgtPathLen;
	uint32_t spp;
	bool mis;
	bool resample;
	Pointer<HDRImage<float, 3>> output;
	Pointer<ProgressTracker<uint64_t>> progress;

	typedef struct PathVertex {
	public:
		Primitive::Intersection it;
		float val = 0.f;
		float lgt2CamPDF = 0.f;
		float cam2LgtPDF = 0.f;
		BxDF const* sampled = nullptr;
		UEDC const* uedc = nullptr;
		bool onCamera = false;
		InteriorList list;

	public:
		__host__
		inline bool onSurface() const {
			return onCamera || it.primitive != nullptr;
		}

		__host__
		inline bool isDelta(const Scene* scene) const {
			return (uedc != nullptr && uedc->isDelta()) || (sampled != nullptr && sampled->matches(BxDF::Type::SPECULAR));
		}

		__host__
		inline bool isLight() const {
			return !onCamera && it.primitive != nullptr && it.isEmissive();
		}

		__host__
		inline float absDotSN(const Vectr3F& w) const {
			return onSurface() ? absDot(it.geometry.sn, w) : 1.f;
		}

		__host__
		inline void adjustInteriorList(InteriorList& list, const Vectr3F& w) const {
			if (onSurface()) Scene::adjustInteriorList(list, it, w);
		}

		__host__
		inline void scatter(Ray& ray, InteriorList& list, const Point3F& p) const {
			if (onSurface()) {
				Scene::scatter(ray, list, it, p);
			} else {
				Scene::scatter(ray, it.geometry.gp, p);
			}
		}

		__host__
		inline void scatter(Ray& ray, InteriorList& list, const Vectr3F& w) const {
			if (onSurface()) {
				Scene::scatter(ray, list, it, w);
			} else {
				Scene::scatter(ray, it.geometry.gp, w);
			}
		}

		__host__
		inline float sampleWi(float rnd[2], const BxDF::Mode& mode, Vectr3F* wi, float* pdf) {
			if (onSurface()) {
				return it.sampleBSDFWi(list, mode, BxDF::Type(0), rnd, wi, pdf, nullptr, &sampled);
			} else {
				return highestPriority(list)->samplePhaseWi(it.geometry.gp, it.wlen, it.time, it.direction, rnd, wi, pdf);
			}
		}

		__host__
		inline float sampleWiPDF(const BxDF::Mode& mode, const Vectr3F& wi, const InteriorList& list) const {
			if (onSurface()) {
				return it.sampleBSDFWiPDF(it.direction, wi, list, mode, nullptr, BxDF::Type(0), sampled);
			} else {
				return highestPriority(list)->samplePhaseWiPDF(it.geometry.gp, it.wlen, it.time, it.direction, wi);
			}
		}

		__host__
		inline float sampleWiPDF(const BxDF::Mode& mode, const Vectr3F& wo, const Vectr3F& wi, const InteriorList& list) const {
			if (onSurface()) {
				return it.sampleBSDFWiPDF(wo, wi, list, mode, nullptr, BxDF::Type(0), sampled);
			} else {
				return highestPriority(list)->samplePhaseWiPDF(it.geometry.gp, it.wlen, it.time, wo, wi);
			}
		}

		__host__
		inline float evaluate(const Vectr3F& wi, const BxDF::Mode& mode) const {
			if (onSurface()) {
				return it.evaluateBSDF(wi, list, BxDF::Mode::RADIANCE, nullptr);
			} else {
				return highestPriority(list)->evaluatePhase(it.geometry.gp, it.wlen, it.time, wi, it.direction);
			}
		}

	} PathVertex;

	typedef struct Path {
		float imgX;
		float imgY;
		float wLen;
		float lPDF;
		float time;
		std::vector<PathVertex> cam;
		std::vector<PathVertex> lgt;

		__host__ __device__
		size_t count() const {
			return max(0, cam.size() - 1) + max(0, lgt.size() - 1) + cam.size() * lgt.size();
		}

	} Path;

	typedef struct Stats {
		Statistics<float> len;
		Statistics<float> val;

		__host__
		void update(const Path& path, const float value) {
			len.add(path.count());
			val.add(value);
		}

	} Stats;

	__host__
	static float toAreaPDF(const PathVertex& from, const PathVertex& sample, const float solidAnglePDF) {
		Vectr3F w = from.it.geometry.gp - sample.it.geometry.gp;
		float distanceE2 = w.lengthSquared();
		float areaPDF = solidAnglePDF / (distanceE2 * sqrt(distanceE2));
		if (sample.onSurface()) {
			areaPDF *= absDot(sample.it.geometry.sn, w);
		}
		return isInvalid(areaPDF) ? 0.f : areaPDF;
	}

	__host__
	void generateCamPath(RNG* camRNG, RNG* rng2, RNG* rng1, uint32_t x, uint32_t y, Scene* scene, Path& path) {
		path.cam.clear();
		uint32_t maxPathLen = mis ? maxCamPathLen + maxLgtPathLen : maxCamPathLen;
		if (maxPathLen == 0) {
			return;
		}
		// initialize
		float camRND[6], tr;
		PathVertex vertex;
		vertex.onCamera = true;
		camRNG->next(camRND);
		path.imgX = x + camRND[0];
		path.imgY = y + camRND[1];
		path.time = camRND[5];
		path.wLen = scene->camera()->sensor()->colorspace()->xyzHistogram()->sample(camRND[4], &path.lPDF);
		Ray ray = scene->camera()->ray(path.imgX, path.imgY, camRND[2], camRND[3], path.wLen, path.time, &vertex.it.geometry.gn);
		scene->initInteriorList(ray, vertex.list);
		vertex.it.geometry.sn = vertex.it.geometry.gn;
		vertex.it.geometry.gp = ray.o;
		vertex.val = scene->camera()->importance0(ray.o, path.time);
		vertex.cam2LgtPDF = scene->camera()->sampleGeoPDF(ray.o, path.time);
		path.cam.push_back(vertex);
		// iterate
		Vectr3F wi;
		float surRND[3];
		for (int i = 1; i < maxPathLen; i++) {
			// sample next direction
			PathVertex& previous = path.cam[i - 1];
			PathVertex  vertex;
			if (i == 1) {
				// next direct from camera lens
				wi = ray.d;
				vertex.val = scene->camera()->importance1(ray);
				vertex.cam2LgtPDF = scene->camera()->sampleDirPDF(ray);
			} else {
				// next direction from previous vertex BSDF
				rng2->next(surRND);
				vertex.val = previous.sampleWi(surRND, BxDF::Mode::RADIANCE, &wi, &vertex.cam2LgtPDF);
			}
			// russian roulette
			float rusRND;
			if (i >= minCamPathLen) {
				rng1->next(&rusRND);
				float threshold = vertex.val * previous.absDotSN(wi) / vertex.cam2LgtPDF;
				threshold = clamp(threshold, 0.01f, 0.99f);
				if (rusRND > threshold || isInvalid(threshold)) {
					break;
				}
				vertex.val /= threshold;
			}
			vertex.val *= previous.val * previous.absDotSN(wi) / vertex.cam2LgtPDF;
			if (!isValid(vertex.val) || !isValid(vertex.cam2LgtPDF)) {
				break;
			}
			// generate next vertex
			tr = 1.f;
			vertex.list = previous.list;
			previous.scatter(ray, vertex.list, wi);
			if (!scene->interaction(ray, vertex.list, &vertex.it, &tr, rng1)) {
				break;
			}
			if (isInvalid(tr)) {
				break;
			}
			vertex.val *= tr;
			vertex.cam2LgtPDF  = toAreaPDF(previous, vertex, vertex.cam2LgtPDF);
			if (isInvalid(vertex.cam2LgtPDF)) {
				break;
			}
			path.cam.push_back(vertex);
		}
	}

	__host__
	void generateLgtPath(RNG* lgtRNG, RNG* rng2, RNG* rng1, Scene* scene, Path& path) {
		path.lgt.clear();
		uint32_t maxPathLen = mis ? maxCamPathLen + maxLgtPathLen : maxLgtPathLen;
		if (maxLgtPathLen + maxCamPathLen == 0) {
			return;
		}
		// initialize
		float lgtRND[3];
		PathVertex vertex;
		lgtRNG->next(lgtRND);
		scene->sampleLeGeo(path.wLen, path.time, lgtRND, &vertex.it.geometry, &vertex.it.primitive, &vertex.lgt2CamPDF);
		vertex.it.wlen = path.wLen;
		vertex.val = vertex.it.primitive->getMaterial()->emission0(vertex.it.geometry, path.wLen, path.time) / vertex.lgt2CamPDF;
		if (!isValid(vertex.val) || !isValid(vertex.lgt2CamPDF)) {
			return;
		}
		scene->initInteriorList(vertex.it.geometry.gp, Vectr3F{vertex.it.geometry.gn}, path.time, vertex.list);
		path.lgt.push_back(vertex);
		// iterate
		Ray ray;
		Vectr3F wo;
		float tr;//, surRND[3];
		for (int i = 1; i < maxPathLen; i++) {
			// sample next direction
			PathVertex& previous = path.lgt[i - 1];
			PathVertex  vertex;
			vertex.it.wlen = path.wLen;
			rng2->next(lgtRND);
			if (i == 1) {
				// from uedf
				vertex.val = previous.it.primitive->sampleLeDir(previous.it.geometry, path.wLen, path.time, lgtRND, &wo, &vertex.lgt2CamPDF, &previous.uedc);
			} else {
				// from bsdf
				vertex.val = previous.sampleWi(lgtRND, BxDF::Mode::IMPORTANCE, &wo, &vertex.lgt2CamPDF);
			}
			// russian roulette
			float rusRND;
			if (i >= minLgtPathLen) {
				rng1->next(&rusRND);
				float threshold = vertex.val * previous.absDotSN(wo) / vertex.lgt2CamPDF;
				threshold = clamp(threshold, 0.01f, 0.99f);
				if (rusRND > threshold || isInvalid(threshold)) {
					break;
				}
				vertex.val /= threshold;
			}
			vertex.val *= previous.val * previous.absDotSN(wo) / vertex.lgt2CamPDF;
			if (!isValid(vertex.val) || !isValid(vertex.lgt2CamPDF)) {
				break;
			}
			// generate next vertex
			ray.l = path.wLen;
			ray.t = path.time;
			vertex.list = previous.list;
			previous.scatter(ray, vertex.list, wo);
			if (!scene->interaction(ray, vertex.list, &vertex.it, &tr, rng1)) {
				break;
			}
			if (isInvalid(tr)) {
				break;
			}
			vertex.val *= tr;
			vertex.lgt2CamPDF  = toAreaPDF(previous, vertex, vertex.lgt2CamPDF);
			if (isInvalid(vertex.lgt2CamPDF)) {
				break;
			}
			path.lgt.push_back(vertex);
		}
	}

	__host__
	void computeCamPathBackPDF(Path& path, const Scene* scene, InteriorList media) {
		for (uint32_t i = 2; i < path.cam.size(); i++) {
			PathVertex& v0 = path.cam[i - 2];
			const PathVertex& v1 = path.cam[i - 1];
			const PathVertex& v2 = path.cam[i - 0];
			media = v1.list;
			v1.adjustInteriorList(media, -v2.it.direction);
			v0.lgt2CamPDF = v1.sampleWiPDF(BxDF::Mode::IMPORTANCE, -v2.it.direction, v1.it.direction, media);
			v0.lgt2CamPDF = toAreaPDF(v1, v0, v0.lgt2CamPDF);
			if (isInvalid(v0.lgt2CamPDF)) {
				v0.lgt2CamPDF = 0.f;
			}
		}
	}

	__host__
	void computeLgtPathBackPDF(Path& path, const Scene* scene, InteriorList media) {
		for (uint32_t i = 2; i < path.lgt.size(); i++) {
			PathVertex& v0 = path.lgt[i - 2];
			const PathVertex& v1 = path.lgt[i - 1];
			const PathVertex& v2 = path.lgt[i - 0];
			media = v1.list;
			v1.adjustInteriorList(media, -v2.it.direction);
			v0.cam2LgtPDF = v1.sampleWiPDF(BxDF::Mode::RADIANCE, -v2.it.direction, v1.it.direction, media);
			v0.cam2LgtPDF = toAreaPDF(v1, v0, v0.cam2LgtPDF);
			if (isInvalid(v0.cam2LgtPDF)) {
				v0.cam2LgtPDF = 0.f;
			}
		}
	}

	__host__
	float balance(const Scene* scene, InteriorList& media, const Path& path, uint32_t nCam, uint32_t nLgt, const PathVertex* sampled) {
		if (!mis) {
			return 1.f / (min(nCam, maxCamPathLen) + min(nLgt, maxLgtPathLen));
		}
		float sum = 0.f;
		// camera sub path probabilities
		const PathVertex* camVertex = &path.cam[nCam - 1];
		float l2cConnPDF = 0.f, l2cBackPDF = 0.f;
		if (nLgt == 0) {
			// probability of sampling camera vertex from light source
			if (camVertex->isLight()) {
				l2cConnPDF = scene->sampleLeGeoPDF(path.wLen, path.time, camVertex->it.geometry.gp, camVertex->it.primitive);
				if(nCam > 1) {
					const PathVertex* preVertex = &path.cam[nCam - 2];
					l2cBackPDF = camVertex->it.primitive->sampleLeDirPDF(camVertex->it.geometry, path.wLen, path.time, camVertex->it.direction);
					l2cBackPDF = toAreaPDF(*camVertex, *preVertex, l2cBackPDF);
				}
			}
		} else {
			const PathVertex* lgtVertex = sampled == nullptr ? &path.lgt[nLgt - 1] : sampled;
			Vectr3F wo = (camVertex->it.geometry.gp - lgtVertex->it.geometry.gp).normalize();
			if (nLgt == 1) {
				// probability of sampling camera vertex from light vertex UEDF
				l2cConnPDF = lgtVertex->it.primitive->sampleLeDirPDF(lgtVertex->it.geometry, path.wLen, path.time, wo);
				l2cConnPDF = toAreaPDF(*lgtVertex, *camVertex, l2cConnPDF);
			} else {
				media = lgtVertex->list;
				l2cConnPDF = lgtVertex->sampleWiPDF(BxDF::Mode::IMPORTANCE, wo, media);
				l2cConnPDF = toAreaPDF(*lgtVertex, *camVertex, l2cConnPDF);
			}
			if (nCam > 1) {
				media = camVertex->list;
				const PathVertex* preVertex = &path.cam[nCam - 2];
				camVertex->adjustInteriorList(media, -wo);
				l2cBackPDF = camVertex->sampleWiPDF(BxDF::Mode::IMPORTANCE, -wo, camVertex->it.direction, media);
				l2cBackPDF = toAreaPDF(*camVertex, *preVertex, l2cBackPDF);
			}
		}
		float p = 1.f;
		for (int i = nCam - 1; i > 0; i--) {
			if (i == nCam - 1) {
				p *= l2cConnPDF / path.cam[i].cam2LgtPDF;
			} else if (i == nCam - 2) {
				p *= l2cBackPDF / path.cam[i].cam2LgtPDF;
			} else {
				p *= path.cam[i].lgt2CamPDF / path.cam[i].cam2LgtPDF;
			}
			if (!isValid(p)) {
				break;
			}
			if ((i == nCam - 1 || !path.cam[i].isDelta(scene)) && !path.cam[i - 1].isDelta(scene)) {
				sum += p;
			}
		}
		// light sub path probabilities
		if (nLgt > 0) {
			const PathVertex* lgtVertex = sampled == nullptr ? &path.lgt[nLgt - 1] : sampled;
			float c2lConnPDF = 0.f, c2lBackPDF = 0.f;
			Vectr3F wi = (lgtVertex->it.geometry.gp - camVertex->it.geometry.gp).normalize();
			media = camVertex->list;
			if (nCam == 1) {
				c2lConnPDF = scene->camera()->sampleDirPDF(Ray(camVertex->it.geometry.gp, wi, RangeF{0.f, +INFINITY}, path.wLen, path.time));
			} else {
				c2lConnPDF = camVertex->sampleWiPDF(BxDF::Mode::RADIANCE, wi, media);
			}
			c2lConnPDF = toAreaPDF(*camVertex, *lgtVertex, c2lConnPDF);
			if (nLgt > 1) {
				media = lgtVertex->list;
				const PathVertex* preVertex = &path.lgt[nLgt - 2];
				lgtVertex->adjustInteriorList(media, -wi);
				c2lBackPDF = lgtVertex->sampleWiPDF(BxDF::Mode::RADIANCE, -wi, lgtVertex->it.direction, media);
				c2lBackPDF = toAreaPDF(*lgtVertex, *preVertex, c2lBackPDF);
			}
			float p = 1.f;
			for (int j = nLgt - 1; j >= 0; j--) {
				if (j == nLgt - 1) {
					p *= c2lConnPDF / (sampled == nullptr ? &path.lgt[j] : sampled)->lgt2CamPDF;
				} else if (j == nLgt - 2) {
					p *= c2lBackPDF / path.lgt[j].lgt2CamPDF;
				} else {
					p *= path.lgt[j].cam2LgtPDF / path.lgt[j].lgt2CamPDF;
				}
				if (!isValid(p)) {
					break;
				}
				if ((j == nLgt -1 || !path.lgt[j].isDelta(scene)) && (j == 0 || !path.lgt[j - 1].isDelta(scene))) {
					sum += p;
				}
			}
		}
		// done
		return 1.f / (1.f + sum);
	}

	__host__
	void bounce(RNG* camRNG, RNG* lgtRNG, RNG* rng1, RNG* rng2, Path& path, Scene* scene, uint32_t x, uint32_t y, InteriorList& media, Array<Stats>& statistics) {
		// generate sub paths
		generateCamPath(camRNG, rng2, rng1, x, y, scene, path);
		if (path.cam.empty()) {
			return;
		}
		generateLgtPath(lgtRNG, rng2, rng1, scene, path);
		// compute path back PDFs
		if (mis) {
			computeCamPathBackPDF(path, scene, media);
			computeLgtPathBackPDF(path, scene, media);
		}
		// compute multi path contribution
		Ray shadow;
		shadow.l = path.wLen;
		shadow.t = path.time;
		Primitive::Intersection lgtIt;
		float tr, val = 0.f, statVal = 0.f;
		Stats& stats = statistics[y * scene->camera()->sensor()->width() + x];
		float delta = sqrt(stats.val.var() / stats.len.avg());
		for (uint32_t i = 1; i <= min(path.cam.size(), maxCamPathLen); i++) {
			for (uint32_t j = 0; j <= min(path.lgt.size(), maxLgtPathLen); j++) {
				if (j == 0) {
					// treat camera sub path as a complete path
					const PathVertex& camVertex = path.cam[i - 1];
					if (camVertex.isLight()) {
						float pathVal = camVertex.val * camVertex.it.emission();
						float pathWgt = balance(scene, media, path, i, j, nullptr);
						if (isValid(pathVal) && isValid(pathWgt)) val += pathWgt * pathVal;
					}
				} else {
					// connects camera sub path to light sub path
					PathVertex sampled;
					const PathVertex* camVertex = &path.cam[i - 1];
					const PathVertex* lgtVertex = nullptr;
					if (resample && j == 1) {
						// resample direct lighting
						float rnd[2];
						rng2->next(rnd);
						scene->sampleLeGeo(camVertex->it.geometry.gp, path.wLen, path.time, rnd, &sampled.it.geometry, &sampled.it.primitive, &sampled.lgt2CamPDF);
						sampled.it.wlen = path.wLen;
						sampled.val = sampled.it.primitive->getMaterial()->emission0(sampled.it.geometry, path.wLen, path.time) / sampled.lgt2CamPDF;
						if (!isValid(sampled.val) || !isValid(sampled.lgt2CamPDF)) {
							continue;
						}
						scene->initInteriorList(sampled.it.geometry.gp, Vectr3F{sampled.it.geometry.gn}, path.time, sampled.list);
						lgtVertex = &sampled;
					} else {
						lgtVertex = &path.lgt[j - 1];
					}
					// compute geometric term
					float g = 1.f;
					media = camVertex->list;
					camVertex->scatter(shadow, media, lgtVertex->it.geometry.gp);
					g *= camVertex->absDotSN(shadow.d);
					g *= lgtVertex->absDotSN(shadow.d);
					g /= (camVertex->it.geometry.gp - lgtVertex->it.geometry.gp).lengthSquared();
					if (!isValid(g)) {
						continue;
					}
					// compute contribution;
					float pathVal = camVertex->val * g * lgtVertex->val;
					pathVal *=  i == 1 ?
							scene->camera()->sampleDirPDF(shadow):
							camVertex->evaluate(shadow.d, BxDF::Mode::RADIANCE);
					if (lgtVertex->onSurface()) {
						pathVal *= j == 1 ?
							lgtVertex->it.material()->emission1(lgtVertex->it.geometry, path.wLen, path.time, -shadow.d):
							lgtVertex->it.evaluateBSDF(-shadow.d, lgtVertex->list, BxDF::Mode::IMPORTANCE, nullptr);
					} else {
						const Material* medium = highestPriority(lgtVertex->list);
						pathVal *= medium->evaluatePhase(lgtVertex->it.geometry.gp, path.wLen, path.time, -shadow.d, lgtVertex->it.direction);
					}
					if (!isValid(pathVal)) {
						continue;
					}
					float pathWgt = balance(scene, media, path, i, j, (resample && j == 1) ? &sampled : nullptr);
					if (!isValid(pathWgt)) {
						continue;
					}
					// russian roulette connection
					float rnd, threshold = (pathWgt * pathVal) / delta;
					threshold = isnan(threshold) ? 1.f : clamp(threshold, 0.f, 1.f);
					rng1->next(&rnd);
					if (rnd > threshold) {
						continue;
					}
					// trace shadow ray
					if (scene->intersection(shadow, media, rng1, &tr, &lgtIt)) {
						continue;
					}
					if (i == 1) {
						// splat value on film
						float x, y;
						if (scene->camera()->raster(shadow, &x, &y)) {
							scene->camera()->sensor()->addSample(x, y, path.wLen, (tr * pathWgt * pathVal) / threshold, path.lPDF);
						}
					} else {
						// accumulate value
						val += (tr * pathWgt * pathVal) / threshold;
						statVal +=  (pathWgt * pathVal) / threshold;
					}
				}
			}
		}
		stats.update(path, statVal);
		scene->camera()->sensor()->addSample(path.imgX, path.imgY, path.wLen, val, path.lPDF);
	}

public:
	__host__
	CPUSingleThreadBidirectionalTracer(
		Pointer<CPURNGFactory>& factory,
		uint32_t minCamPathLen,
		uint32_t maxCamPathLen,
		uint32_t minLgtPathLen,
		uint32_t maxLgtPathLen,
		uint32_t spp,
		bool mis,
		bool resample,
		Pointer<ProgressTracker<uint64_t>>& progress
	):
		factory(factory),
		minCamPathLen(minCamPathLen),
		maxCamPathLen(maxCamPathLen),
		minLgtPathLen(minLgtPathLen),
		maxLgtPathLen(maxLgtPathLen),
		spp(spp),
		mis(mis),
		resample(resample),
		progress(progress) {
	}

	__host__
	void render(Scene* scene, const bool progressive) override {
		// local memory pool
		MemoryPool memPool{32, new CPUAllocator{}};
		// image size
		Sensor* sensor = scene->camera()->sensor();
		uint32_t w = (uint32_t) sensor->width();
		uint32_t h = (uint32_t) sensor->height();
		size_t imgSize = w * h;
		// random number generators
		CPUBasicRNGFactory<> uncorrelatedFactory;
		Pointer<RNG> camRNG = progressive ? uncorrelatedFactory.build(memPool, 6) : factory->build(memPool, 6);
		Pointer<RNG> lgtRNG = progressive ? uncorrelatedFactory.build(memPool, 2) : factory->build(memPool, 2);
		Pointer<RNG> rng1 = uncorrelatedFactory.build(memPool, 1);
		Pointer<RNG> rng2 = uncorrelatedFactory.build(memPool, 2);
		// initialize
		sensor->clear();
		Array<Stats> statistics(memPool, imgSize);
		output = sensor->raster();
		progress->initialize(imgSize * spp);
		// render
		Path path;
		InteriorList media;
		if (progressive) {
			for (uint32_t s = 0; s < spp; s++) {
				for (uint32_t y = 0; y < h; y++) {
					for (uint32_t x = 0; x < w; x++) {
						bounce(camRNG.get(), lgtRNG.get(), rng1.get(), rng2.get(), path, scene, x, y, media, statistics);
						progress->log(1);
					}
				}
			}
		} else {
			for (uint32_t y = 0; y < h; y++) {
				for (uint32_t x = 0; x < w; x++) {
					for (uint32_t s = 0; s < spp; s++) {
						bounce(camRNG.get(), lgtRNG.get(), rng1.get(), rng2.get(), path, scene, x, y, media, statistics);
						progress->log(1);
					}
				}
			}
		}
		// done rendering
		progress->terminate();
	}

	__host__
	ProgressTracker<uint64_t>* tracker() override {
		return progress.get();
	}

	__host__
	void preview(HDRImage<float, 3>* image) override {
		if (!output.isNull()) {
			*image = *output;
		}
	}
};

}

#endif /* CPUSINGLETHREADBIDIRECTIONALTRACER_CUH_ */

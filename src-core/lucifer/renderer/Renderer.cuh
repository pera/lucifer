#ifndef RENDERER_CUH_
#define RENDERER_CUH_

#include "Scene.cuh"
#include "lucifer/image/HDRImage.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/util/Progress.cuh"

namespace lucifer {

class Renderer {
public:
	__host__
	virtual void render(Scene* scene, const bool progressive) = 0;

	__host__
	virtual ProgressTracker<uint64_t>* tracker() = 0;

	__host__
	virtual void updateSensor() {
	}

	__host__
	virtual void preview(HDRImage<float, 3>* image) {
	}

	__host__
	virtual ~Renderer() {
	}
};

}

#endif /* RENDERER_CUH_ */

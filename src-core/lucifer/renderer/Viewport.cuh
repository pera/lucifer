#ifndef VIEWPORT_CUH_
#define VIEWPORT_CUH_

#include "lucifer/math/geometry/CommonTypes.cuh"

namespace lucifer {

class Viewport {
public:
	Point2F lower;
	Point2F upper;

	__host__
	Viewport(
		const Point2F lower = Point2F{},
		const Point2F upper = Point2F{}
	):
		lower(lower),
		upper(upper)
	{ }
};

}

#endif /* VIEWPORT_CUH_ */

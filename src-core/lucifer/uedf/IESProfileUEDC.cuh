#ifndef IESPROFILEUEDC_CUH_
#define IESPROFILEUEDC_CUH_

#include <cassert>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "UEDC.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/Util.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"
#include "lucifer/random/PiecewiseLinearDistribution2D.cuh"
#include "lucifer/random/UniformSphereDistribution.cuh"
#include "lucifer/random/CPUBasicRNG.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

#define IESNA_1995 "IESNA:LM-63-1995"
#define IESNA_2002 "IESNA:LM-63-2002"
#define IESNA_TILT "TILT=NONE"
#define IESNA_TYPEC 1
#define IESNA_TYPEB 2
#define IESNA_TYPEA 3
#define IESNA_FEET 1
#define IESNA_METERS 2

namespace lucifer {

/*
 * Minimum IES profile data.
 */
class IESProfileData {
public:
	int type;
	float factor;
	std::vector<float> vAngles;
	std::vector<float> hAngles;
	std::vector<float> cValues;
};

/**
 * versions: LM-63-1995, LM-63-2002
 * tilt: no tilt only
 * type: C (1) only
 */
class IESProfileParser {
private:
	__host__
	static std::istream& safeGetline(std::istream& is, std::string& t) {
	    t.clear();
	    std::istream::sentry se(is, true);
	    std::streambuf* sb = is.rdbuf();
	    for(;;) {
	        int c = sb->sbumpc();
	        switch (c) {
	        case '\n':
	            return is;
	        case '\r':
	            if(sb->sgetc() == '\n')
	                sb->sbumpc();
	            return is;
	        case std::streambuf::traits_type::eof():
	            if(t.empty())
	                is.setstate(std::ios::eofbit);
	            return is;
	        default:
	            t += (char)c;
	        }
	    }
	}

public:
	__host__
	static bool parse(const std::string& filename, IESProfileData& data, bool print = false) {
		// open input file
		std::ifstream input;
		input.open(filename);
		if (!input) {
			std::cerr << "could not open file: " << filename << std::endl;
			return false;
		}
		if (print) std::cout << "reading IES file: " << filename << std::endl;
		// file version
		std::string line;
		safeGetline(input, line);
		if (line == IESNA_1995) {
			if (print) std::cout << "version: 1995" << std::endl;
		} else if (line == IESNA_2002) {
			if (print) std::cout << "version: 2002" << std::endl;
		} else {
			std::cerr << "unsupported IES file version: " << line << std::endl;
			return false;
		}
		// keywords
		safeGetline(input, line);
		while(line.at(0) == '[') {
			if (print) std::cout << "keyword: " << line << std::endl;
			safeGetline(input, line);
		}
		// tilt
		if (line != IESNA_TILT) {
			std::cerr << "unsupported IES tilt: " << line << std::endl;
			return false;
		}
		int n;
		float x;
		// number of lamps
		input >> n;
		if (print) std::cout << "lamps: " << n << std::endl;
		// lumens per lamp
		input >> x;
		if (print) std::cout << "lumens per lamp: " << x << std::endl;
		// candela multiplier
		input >> x;
		if (print) std::cout << "candela multiplier: " << x << std::endl;
		data.factor = x;
		// number of vertical angles
		int nv;
		input >> nv;
		if (print) std::cout << "vertical angles: " << nv << std::endl;
		// number of horizontal angles
		int nh;
		input >> nh;
		if (print) std::cout << "horizontal angles: " << nh << std::endl;
		// photometric type
		input >> n;
		switch (n) {
			case IESNA_TYPEA:
				std::cerr << "unsupported photometric type: A" << n << std::endl;
				return false;
			case IESNA_TYPEB:
				std::cerr << "unsupported photometric type: B" << n << std::endl;
				return false;
			case IESNA_TYPEC:
				if (print) std::cout << "photometric type: C" << std::endl;
				break;
			default:
				std::cerr << "unsupported photometric type: " << n << std::endl;
				return false;
		}
		data.type = n;
		// units
		input >> n;
		switch (n) {
			case IESNA_FEET:
				if (print) std::cout << "units: feet" << std::endl;
				break;
			case IESNA_METERS:
				if (print) std::cout << "units: meters" << std::endl;
				break;
			default:
				std::cerr << "unsupported unit: " << n << std::endl;
				return false;
		}
		// width
		input >> x;
		if (print) std::cout << "width: " << x << std::endl;
		// length
		input >> x;
		if (print) std::cout << "length: " << x << std::endl;
		// height
		input >> x;
		if (print) std::cout << "height: " << x << std::endl;
		// balast factor
		input >> x;
		if (print) std::cout << "ballast factor: " << x << std::endl;
		data.factor *= x;
		// future use
		input >> x;
		if (print) std::cout << "future use: " << x << std::endl;
		// input watts
		input >> x;
		if (print) std::cout << "input watts: " << x << std::endl;
		// vertical angles
		data.vAngles.clear();
		if (print) std::cout << "list of vertical angles:\t";
		for (int i = 0; i < nv; i++) {
			input >> x;
			if (print) std::cout << x << "\t";
			data.vAngles.push_back(x);
		}
		if (data.vAngles.size() < 2) {
			std::cerr << "necessary at least 2 vertical angles, found: " << data.vAngles.size() << std::endl;
			return false;
		}
		if (print) std::cout << std::endl;
		// horizontal angles
		data.hAngles.clear();
		if (print) std::cout << "list of horizontal angles:\t";
		for (int i = 0; i < nh; i++) {
			input >> x;
			if (print) std::cout << x << "\t";
			data.hAngles.push_back(x);
		}
		if (data.hAngles.size() < 1) {
			std::cerr << "necessary at least 1 horizontal angle, found: " << data.hAngles.size() << std::endl;
			return false;
		}
		if (print) std::cout << std::endl;
		// candela values;
		data.cValues.clear();
		if (print) std::cout << "candela values:" << std::endl;
		for (int i = 0; i < nh; i++) {
			for (int j = 0; j < nv; j++) {
				input >> x;
				if (print) std::cout << x << "\t";
				data.cValues.push_back(x);
			}
			if (print) std::cout << std::endl;
		}
		// ok
		return true;
	}
};

/*!
 * \brief
 * A photometric light-emission pattern defined by an IES profile.
 *
 * The IESNA LM-63 photometric data file is an ASCII text file commonly used by
 * North American lighting fixture manufacturers to distribute photometric
 * information about their products.
 *
 * <a href="http://lumen.iee.put.poznan.pl/kw/iesna.txt">
 * Parsing The IESNA LM-63 Photometric Data File
 * </a>
 *
 * The current implementation supports only type C files with no tilt.
 *
 * \author
 * Bruno Pera
 */
class IESProfileUEDC: public UEDC {
private:
	using index_t = uint16_t;
	using arr16_t = Array<float, uint16_t>;
	using arr32_t = Array<float, uint32_t>;
	friend class TypedPool<IESProfileUEDC>;

private:
	const Pointer<const ShadingNode<float>> power;
	arr16_t vAngles;
	arr16_t hAngles;
	arr32_t cValues;
	Pointer<PiecewiseLinearDistribution2D> histogram;

	__host__ __device__
	static index_t binarySearch(const arr16_t& list, const float at, const index_t l, const index_t u) {
		if ((u - l) < 2) {
			return l;
		}
		const index_t m = (l + u) / 2;
		if (at < list[m]) {
			return binarySearch(list, at, l, m);
		}
		return binarySearch(list, at, m, u);
	}

	__host__ __device__
	uint32_t index(const index_t v, const index_t h) const {
		return ((uint32_t) h) * vAngles.size() + ((uint32_t) v);
	}

	__host__ __device__
	float value(const int v, const int h) const {
		return cValues[index(v, h)];
	}

	__host__ __device__
	float emission1(const Vectr3F& lw) const {
		// find   vertical angle and index
		float theta = acos(lw[2]);
		if (theta < vAngles[0] || theta > vAngles[vAngles.size() - 1]) {
			return 0.f;
		}
		index_t v = binarySearch(vAngles, theta, 0, vAngles.size() - 1);
		assert(v < vAngles.size() - 1);
		// find horizontal angle and index
		index_t h = 0;
		float phi = 0.f;
		if (hAngles.size() > 1) {
			phi = atan2(lw[1], lw[0]);
			if (phi < 0.f) phi += 2 * M_PI;
			phi = fmod(phi, hAngles[hAngles.size() - 1]);
			h = binarySearch(hAngles, phi, 0, hAngles.size() - 1);
			assert(h < hAngles.size() - 1 || (h == 0 && hAngles.size() == 1));
		}
		// interpolate
		float dv = smooth3((theta - vAngles[v]) / (vAngles[v + 1] - vAngles[v]));
		if (hAngles.size() == 1) {
			return lerp(value(v, h), value(v + 1, h), dv);
		}
		float dh = smooth3((  phi - hAngles[h]) / (hAngles[h + 1] - hAngles[h]));
		return
			lerp(
				lerp(value(v + 0, h + 0), value(v + 1, h + 0), dv),
				lerp(value(v + 0, h + 1), value(v + 1, h + 1), dv),
				dh
			);
	}

	__host__ __device__
	static constexpr const IESProfileUEDC* cast(const UEDC* uedc) {
		return static_cast<const IESProfileUEDC*>(uedc);
	}

	__host__ __device__
	static bool delta_dist_(const UEDC* uedc) {
		return false;
	}

	__host__ __device__
	static float emission_0_(const UEDC* uedc, const Geometry& g, const float wlen, const float time) {
		return (*cast(uedc)->power)(g.gp, g.su, g.sn, g.uv, wlen, time);
	}

	__host__ __device__
	static float emission_1_(const UEDC* uedc, const Geometry& g, const float wlen, const float time, const Vectr3F& w) {
		Vectr3F lw;
		Vectr3F g_sv = cross<Norml3F, Vectr3F, Vectr3F>(g.sn, g.su).normalize();
		toShadingCoordinates(g.su, g_sv, g.sn, w, lw);
		return cast(uedc)->emission1(lw);
	}

	__host__ __device__
	static float sample_dir_(const UEDC* uedc, const Geometry& g, const float wlen, const float time, float rnd[2], Vectr3F* w, float* pdf) {
		// sample theta and phi
		cast(uedc)->histogram->sample(rnd, rnd, pdf);
		float cosTheta = 1.f - 2.0f * rnd[0];
		float sinTheta = sqrt(clamp(1.f - cosTheta * cosTheta, 0.f, 1.f));
		float phi = 2.f * M_PI * rnd[1];
		*pdf /= 4.f * M_PI;
		// compute direction
		Vectr3F lw(sinTheta * cos(phi), sinTheta * sin(phi), cosTheta);
		Vectr3F g_sv = cross<Norml3F, Vectr3F, Vectr3F>(g.sn, g.su).normalize();
		toWorldCoordinates(g.su, g_sv, g.sn, lw, *w);
		// compute emission
		return cast(uedc)->emission1(lw);
	}

	__host__ __device__
	static float sample_pdf_(const UEDC* uedc, const Geometry& g, const float wlen, const float time, const Vectr3F& w) {
		// to local shading coordiantes
		Vectr3F lw;
		Vectr3F g_sv = cross<Norml3F, Vectr3F, Vectr3F>(g.sn, g.su).normalize();
		toShadingCoordinates(g.su, g_sv, g.sn, w, lw);
		// compute pdf
		float rnd[2];
		rnd[0] = 0.5 * (1.f - lw[2]);
		rnd[1] = atan2(lw[1], lw[0]) / (2.f * M_PI);
		if (rnd[1] < 0) rnd[1] += 1.f;
		float pdf = cast(uedc)->histogram->pdf(rnd) / (4.f * M_PI);
		if (isnan(pdf) || isinf(pdf)) {
			return 0.f;
		}
	    return pdf;
	}

	__host__
	IESProfileUEDC(
		MemoryPool& pool,
		const Pointer<const ShadingNode<float>>& power,
		const IESProfileData& data,
		const delta_dist_t& delta_dist_,
		const emission_0_t& emission_0_,
		const emission_1_t& emission_1_,
		const sample_dir_t& sample_dir_,
		const sample_pdf_t& sample_pdf_
	):
		UEDC(delta_dist_, emission_0_, emission_1_, sample_dir_, sample_pdf_),
		power(power),
		vAngles(pool, data.vAngles.size()),
		hAngles(pool, data.hAngles.size()),
		cValues(pool, data.cValues.size()),
		histogram() {
		// copy data
		for (uint16_t i = 0; i < vAngles.size(); i++) {
			vAngles[i] = toRadians(data.vAngles[i]);
		}
		for (uint16_t i = 0; i < hAngles.size(); i++) {
			hAngles[i] = toRadians(data.hAngles[i]);
		}
		for (uint32_t i = 0; i < cValues.size(); i++) {
			cValues[i] = data.cValues[i] * data.factor;
		}
		// normalize directional differential of emitted power
		Vectr3F w;
		constexpr uint32_t n = 100;
		float rnd[2], pdf, sum = 0.f;
		Pointer<RNG> rng = CPUBasicRNGFactory<>().build(pool, 1);
		for (uint8_t i = 0; i < n; i++) {
			rng->next(&rnd[0]);
			rnd[0] = (i + rnd[0]) / n;
			for (uint8_t j = 0; j < n; j++) {
				rng->next(&rnd[1]);
				rnd[1] = (j + rnd[1]) / n;
				UniformSphereDistribution::sample(rnd, &w[0], &pdf);
				sum += emission1(w) / pdf;
			}
		}
		sum /= (n * n);
		assert(sum > 0.f);
		for (uint16_t i = 0; i < cValues.size(); i++) {
			cValues[i] /= sum;
		}
		// build histogram distribution
		PiecewiseLinearDistribution2D::idx2d_t vbins = 4 * vAngles.size();
		PiecewiseLinearDistribution2D::idx2d_t hbins = 4 * hAngles.size();
		PiecewiseLinearDistribution2D::array_t hstValues(pool, vbins * hbins);
		for (PiecewiseLinearDistribution2D::idx2d_t h = 0, k = 0; h < hbins; ++h) {
			float phi = (2.f * M_PI * h) / (hbins - 1.f);
			for (PiecewiseLinearDistribution2D::idx2d_t v = 0; v < vbins; ++v, ++k) {
				float cosTheta = 1.f - 2.f * v / (vbins - 1.f);
				float sinTheta = sqrt(clamp(1.f - cosTheta * cosTheta, 0.f, 1.f));
				w = Vectr3F(sinTheta * cos(phi), sinTheta * sin(phi), cosTheta);
				hstValues[k] = emission1(w) * abs(w[2]);
			}
		}
		histogram = PiecewiseLinearDistribution2D::build(pool, RangeF(0.f, 1.f), RangeF(0.f, 1.f), vbins, hbins, hstValues);
	}

public:
	friend class TypedPool<IESProfileUEDC>;

	/*!
	 * \brief
	 * Creates a new IESProfileUEDC from the given IESProfileData.
	 *
	 * \param[in] power The total emitted spectral power at each point.
	 * \param[in] data Describes the directional distrbution of emitted light.
	 */
	__host__
	static Pointer<IESProfileUEDC> build(MemoryPool& pool, const Pointer<const ShadingNode<float>>& power, const IESProfileData& data) {
		return Pointer<IESProfileUEDC>::make(
			pool, pool, power, data,
			delta_dist_t(virtualize(delta_dist_)),
			emission_0_t(emission_0_, [] __device__ () {return (void*)emission_0_;}),
			emission_1_t(virtualize(emission_1_)),
			sample_dir_t(virtualize(sample_dir_)),
			sample_pdf_t(virtualize(sample_pdf_))
		);
	}

	/*!
	 * \brief
	 * Parses an IESNA LM-63 photometric data file and creates a new UEDC.
	 *
	 * \param[in] power The total emitted spectral power at each point.
	 * \param[in] file The input file name.
	 *
	 * \return An IESProfileUEDC.
	 */
	__host__
	static Pointer<UEDC> build(MemoryPool& pool, const Pointer<const ShadingNode<float>>& power, const std::string& file) {
		IESProfileData data;
		if (!IESProfileParser::parse(file, data)) {
			return Pointer<UEDC>();
		}
		return build(pool, power, data);
	}

};

}

#endif /* IESPROFILEUEDC_CUH_ */

#ifndef UEDF_CUH_
#define UEDF_CUH_

#include <cassert>
#include <cstdint>
#include "UEDC.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/memory/Polymorphic.cuh"

namespace lucifer {

/*!
 * \brief
 * Unidirectional Emission Distribution Function.
 *
 * An UEDF describes how a surface emits light. An UEDF must be able to compute
 * the total emitted power at a given position on the surface, the directional
 * differencial of emitted power in a given direction and must also be able to
 * sample an emission direction and compute corresponding PDF with respect to
 * the solid angle measure.
 *
 * \author
 * Bruno Pera
 */
class UEDF: public Polymorphic {
public:
	using index_t = uint8_t;
	using array_t = Array<Pointer<const UEDC>, index_t>;

private:
	array_t uedcs;

	__host__
	UEDF(MemoryPool& pool, const index_t n):
	uedcs(pool, n) {
	}

public:
	friend class TypedPool<UEDF>;

	/*!
	 * \brief
	 * Creates a new UEDF.
	 *
	 * \param[in] n The number of components.
	 */
	__host__
	static Pointer<UEDF> build(MemoryPool& pool, const index_t n) {
		return Pointer<UEDF>::make(pool, pool, n);
	}

	/*!
	 * \brief
	 * Returns a const pointer to the i-th component UEDC.
	 *
	 * \param[in] i The UEDC index.
	 *
	 * \return A const pointer to the i-th component UEDC.
	 */
	__host__ __device__
	const Pointer<const UEDC>& operator[](index_t i) const {
		assert(i < uedcs.size());
		return uedcs[i];
	}

	/*!
	 * \brief
	 * Returns a pointer to the i-th component UEDC.
	 *
	 * \param[in] i The UEDC index.
	 *
	 * \return A pointer to the i-th component UEDC.
	 */
	__host__ __device__
	Pointer<const UEDC>& operator[](index_t i) {
		assert(i < uedcs.size());
		return uedcs[i];
	}

	/*!
	 * \brief
	 * Computes the total emitted spectral power at a position on the surface.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 *
	 * \return The total emitted spectral power at a position on the surface.
	 */
	__host__ __device__
	float emission0(const Geometry& g, const float wlen, const float time) const {
		float ems = 0.f;
		for (auto& uedc : uedcs) {
			ems += uedc->emission0(g, wlen, time);
		}
		return ems;
	}

	/*!
	 * \brief
	 * Computes the directional differential of spectral emitted power at a
	 * position no the surface.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \paran[in] w The normalized direction vector in material coordinates.
	 *
	 * \return The directional differential of spectral emitted power
	 */
	__host__ __device__
	float emission1(const Geometry& g, const float wlen, const float time, const Vectr3F& w) const {
		float tmp0;
		float ems0 = 0.f;
		float ems1 = 0.f;
		for (auto& uedc : uedcs) {
			tmp0  = uedc->emission0(g, wlen, time);
			ems0 += tmp0;
			ems1 += tmp0 * uedc->emission1(g, wlen, time, w);
		}
		return ems0 <= 0.f ? ems1 : ems1 / ems0;
	}

	/*!
	 * \brief
	 * Samples an emission direction from a position on the surface and
	 * computes the corresponding PDF with respect to the solid angle measure.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] rnd A pair of randomly generated numbers in [0, 1].
	 * \param[out] w The sampled normalized direction in material coordinates.
	 * \param[out] pdf The PDF with respect to the solid angle measure.
	 * \param[out] sampled The actually sampled UEDC.
	 *
	 * \return The directional differential of spectral emitted power.
	 */
	__host__ __device__
	float sampleWe(const Geometry& g, const float wlen, const float time, float rnd[2], Vectr3F* w, float* pdf, const UEDC** sampled ) const {
		index_t i = min((rnd[0] * uedcs.size()), uedcs.size() - 1);
		float a = ((float) i + 0) / uedcs.size();
		float b = ((float) i + 1) / uedcs.size();
		rnd[0] = clamp(a + (rnd[0] - a) / (b - a), 0.f, 1.f);
		float tmp0;
		float ems0 = uedcs[i]->emission0(g, wlen, time);
		float ems1 = ems0 * uedcs[i]->sampleWe(g, wlen, time, rnd, w, pdf);
		for (int j = 0; j < uedcs.size(); j++) {
			if (j != i) {
				 tmp0  = uedcs[j]->emission0(g, wlen, time);
				*pdf  += uedcs[j]->sampleWePDF(g, wlen, time, *w);
				 ems0 += tmp0;
				 ems1 += tmp0 * uedcs[j]->emission1(g, wlen, time, *w);
			}
		}
		*pdf /= uedcs.size();
		if (sampled != nullptr) {
			*sampled = *pdf <= 0.f ? nullptr : uedcs[i].get();
		}
		return ems0 <= 0.f ? ems1 : ems1 / ems0;
	}

	/*!
	 * \brief
	 * Computes the PDF of sampling an emission direction from a point on the
	 * surface with respect to the solid angle measure.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] w The normalized emission direction in material coordinates.
	 *
	 * \return The PDF with respect to the solid angle measure.
	 */
	__host__ __device__
	float sampleWePDF(const Geometry& g, const float wlen, const float time, const Vectr3F& w) const {
		float pdf = 0.f;
		for (index_t i = 0; i < uedcs.size(); ++i) {
			pdf += uedcs[i]->sampleWePDF(g, wlen, time, w);
		}
		return pdf / uedcs.size();
	}
};

}

#endif /* UEDF_CUH_ */

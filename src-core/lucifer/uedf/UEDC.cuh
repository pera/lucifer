#ifndef UEDC_CUH_
#define UEDC_CUH_

#include "lucifer/math/geometry/Geometry.cuh"
#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/memory/Virtual.cuh"

namespace lucifer {

/*!
 * \brief
 * Unidirectional Emission Distribution Component.
 *
 * An UEDC describes a simple light emission distribution pattern. The complete
 * emission patter of a surface, the UEDF, is just a sum of simple UEDC
 * components. An UEDC must be able to compute the total emitted power at a
 * given position on the surface, the directional differencial of emitted power
 * in a given direction and must also be able to sample an emission direction
 * and compute corresponding PDF with respect to the solid angle measure.
 *
 * \author
 * Bruno Pera.
 */
class UEDC: public Polymorphic {
public:
	using delta_dist_t = Virtual<bool, const UEDC*>;
	using emission_0_t = Virtual<float, const UEDC*, const Geometry&, const float, const float>;
	using emission_1_t = Virtual<float, const UEDC*, const Geometry&, const float, const float, const Vectr3F&>;
	using sample_dir_t = Virtual<float, const UEDC*, const Geometry&, const float, const float, float[2], Vectr3F*, float*>;
	using sample_pdf_t = Virtual<float, const UEDC*, const Geometry&, const float, const float, const Vectr3F&>;

private:
	const delta_dist_t delta_dist_;
	const emission_0_t emission_0_;
	const emission_1_t emission_1_;
	const sample_dir_t sample_dir_;
	const sample_pdf_t sample_pdf_;

protected:
	__host__
	UEDC(
		const delta_dist_t& delta_dist_,
		const emission_0_t& emission_0_,
		const emission_1_t& emission_1_,
		const sample_dir_t& sample_dir_,
		const sample_pdf_t& sample_pdf_
	):
		delta_dist_(delta_dist_),
		emission_0_(emission_0_),
		emission_1_(emission_1_),
		sample_dir_(sample_dir_),
		sample_pdf_(sample_pdf_) {
	}

public:
	/*!
	 * \brief
	 * Returns if this UEDC represents a delta distribution, in other words, if
	 * this UEDC emits light in a single direction or on a one dimensional set
	 * of directions.
	 *
	 * \return If this UEDC represents a delta distribution.
	 */
	__host__ __device__
	bool isDelta() const {
		return delta_dist_(this);
	}

	/*!
	 * \brief
	 * Computes the total emitted spectral power at a position on the surface.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 *
	 * \return The total emitted spectral power at a position on the surface.
	 */
	__host__ __device__
	float emission0(const Geometry& g, const float wlen, const float time) const {
		return emission_0_(this, g, wlen, time);
	}

	/*!
	 * \brief
	 * Computes the directional differential of spectral emitted power at a
	 * position no the surface.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \paran[in] w The normalized direction vector in material coordinates.
	 *
	 * \return The directional differential of spectral emitted power
	 */
	__host__ __device__
	float emission1(const Geometry& g, const float wlen, const float time, const Vectr3F& w) const {
		return emission_1_(this, g, wlen, time, w);
	}

	/*!
	 * \brief
	 * Samples an emission direction from a position on the surface and
	 * computes the corresponding PDF with respect to the solid angle measure.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] rnd A pair of randomly generated numbers in [0, 1].
	 * \param[out] w The sampled normalized direction in material coordinates.
	 * \param[out] pdf The PDF with respect to the solid angle measure.
	 * \param[out] sampled The actually sampled UEDC.
	 *
	 * \return The directional differential of spectral emitted power.
	 */
	__host__ __device__
	float sampleWe(const Geometry& g, const float wlen, const float time, float rnd[2], Vectr3F* w, float* pdf) const {
		return sample_dir_(this, g, wlen, time, rnd, w, pdf);
	}

	/*!
	 * \brief
	 * Computes the PDF of sampling an emission direction from a point on the
	 * surface with respect to the solid angle measure.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] w The normalized emission direction in material coordinates.
	 *
	 * \return The PDF with respect to the solid angle measure.
	 */
	__host__ __device__
	float sampleWePDF(const Geometry& g, const float wlen, const float time, const Vectr3F& w) const {
		return sample_pdf_(this, g, wlen, time, w);
	}
};

}

#endif /* UEDC_CUH_ */

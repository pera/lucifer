#ifndef DELTADIRECTIONALUEDC_CUH_
#define DELTADIRECTIONALUEDC_CUH_

#include "UEDC.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * An UEDC that emits light only in the normal vector's direction.
 *
 * \author
 * Bruno Pera.
 */
class DeltaDirectionalUEDC: public UEDC {
private:
	const Pointer<const ShadingNode<float>> power;

	__host__ __device__
	static constexpr const DeltaDirectionalUEDC* cast(const UEDC* uedc) {
		return static_cast<const DeltaDirectionalUEDC*>(uedc);
	}

	__host__ __device__
	static bool delta_dist_(const UEDC* uedc) {
		return true;
	}

	__host__ __device__
	static float emission_0_(const UEDC* uedc, const Geometry& g, const float wlen, const float time) {
		return (*cast(uedc)->power)(g.gp, g.su, g.sn, g.uv, wlen, time);
	}

	__host__ __device__
	static float emission_1_(const UEDC* uedc, const Geometry& g, const float wlen, const float time, const Vectr3F& w) {
		return 0.f;
	}

	__host__ __device__
	static float sample_dir_(const UEDC* uedc, const Geometry& g, const float wlen, const float time, float rnd[2], Vectr3F* w, float* pdf) {
		*w = Vectr3F(g.sn[0], g.sn[1], g.sn[2]);
		*pdf = 1.f;
		return 1.f;
	}

	__host__ __device__
	static float sample_pdf_(const UEDC* uedc, const Geometry& g, const float wlen, const float time, const Vectr3F& w) {
		return 0.f;
	}

	__host__
	DeltaDirectionalUEDC(
		const Pointer<const ShadingNode<float>>& power,
		const delta_dist_t& delta_dist_,
		const emission_0_t& emission_0_,
		const emission_1_t& emission_1_,
		const sample_dir_t& sample_dir_,
		const sample_pdf_t& sample_pdf_
	):
		UEDC(delta_dist_, emission_0_, emission_1_, sample_dir_, sample_pdf_), power(power) {
	}

public:
	friend class TypedPool<DeltaDirectionalUEDC>;

	/*!
	 * \brief
	 * Creates a new DeltaDirectionalUEDC.
	 *
	 * \param[in] power The field defining the total emitted power.
	 */
	__host__
	static Pointer<DeltaDirectionalUEDC> build(MemoryPool& pool, const Pointer<const ShadingNode<float>>& power) {
		return Pointer<DeltaDirectionalUEDC>::make(
			pool, power,
			delta_dist_t(virtualize(delta_dist_)),
			emission_0_t(emission_0_, [] __device__ () {return (void*)emission_0_;}),
			emission_1_t(virtualize(emission_1_)),
			sample_dir_t(virtualize(sample_dir_)),
			sample_pdf_t(virtualize(sample_pdf_))
		);
	}
};

}

#endif /* DELTADIRECTIONALUEDC_CUH_ */

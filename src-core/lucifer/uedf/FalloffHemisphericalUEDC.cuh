#ifndef FALLOFFHEMISPHERICALUEDC_CUH_
#define FALLOFFHEMISPHERICALUEDC_CUH_

#include "UEDC.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"
#include "lucifer/random/UniformConeDistribution.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * An UEDC with a linear falloff of emitted power.
 *
 * A FalloffHemisphericalUEDC emits light with full power for angle ranging
 * from 0 to thetaMin and decreases linear from thetaMin to thetaMax. The total
 * emitted power, thetaMin and thetaMax angles can vary with position and light
 * wavelength and are defined using instances of SpectralField.
 *
 * \author
 * Bruno Pera.
 */
class FalloffHemisphericalUEDC: public UEDC {
private:
	const Pointer<const ShadingNode<float>> power;
	const Pointer<const ShadingNode<float>> thetaMin;
	const Pointer<const ShadingNode<float>> thetaMax;

	__host__ __device__
	static constexpr const FalloffHemisphericalUEDC* cast(const UEDC* uedc) {
		return static_cast<const FalloffHemisphericalUEDC*>(uedc);
	}

	__host__ __device__
	static bool delta_dist_(const UEDC* uedc) {
		return false;
	}

	__host__ __device__
	static float emission_0_(const UEDC* uedc, const Geometry& g, const float wlen, const float time) {
		return (*cast(uedc)->power)(g.gp, g.su, g.sn, g.uv, wlen, time);
	}

	__host__ __device__
	static float emission_1_(const UEDC* uedc, const Geometry& g, const float wlen, const float time, const Vectr3F& w) {
		float cosHalfTheta = dot(g.sn, w);
		float cosHalfThetaMax = clamp(cos(0.5f * (*cast(uedc)->thetaMax)(g.gp, g.su, g.sn, g.uv, wlen, time)), 0.f, 1.f);
		if (cosHalfTheta < cosHalfThetaMax) {
			return 0.f;
		}
		float cosHalfThetaMin = clamp(cos(0.5f * (*cast(uedc)->thetaMin)(g.gp, g.su, g.sn, g.uv, wlen, time)), 0.f, 1.f);
		float valMax = 1.f / (M_PI * (2.f - cosHalfThetaMax - cosHalfThetaMin));
		if (cosHalfTheta < cosHalfThetaMin) {
			return valMax * (cosHalfTheta - cosHalfThetaMax) / (cosHalfThetaMin - cosHalfThetaMax);
		}
		return valMax;
	}

	__host__ __device__
	static float sample_dir_(const UEDC* uedc, const Geometry& g, const float wlen, const float time, float rnd[2], Vectr3F* w, float* pdf) {
		// sample direction
		Vectr3F lw;
		float cosHalfThetaMax = clamp(cos(0.5f * (*cast(uedc)->thetaMax)(g.gp, g.su, g.sn, g.uv, wlen, time)), 0.f, 1.f);
		UniformConeDistribution::sample(rnd, cosHalfThetaMax, &(lw[0]), pdf);
		Vectr3F g_sv = cross<Norml3F, Vectr3F, Vectr3F>(g.sn, g.su).normalize();
		toWorldCoordinates(g.su, g_sv, g.sn, lw, *w);
		// compute emission1
		float cosHalfThetaMin = clamp(cos(0.5f * (*cast(uedc)->thetaMin)(g.gp, g.su, g.sn, g.uv, wlen, time)), 0.f, 1.f);
		float valMax = 1.f / (M_PI * (2.f - cosHalfThetaMax - cosHalfThetaMin));
		if (cosTheta(lw) < cosHalfThetaMin) {
			return valMax * (cosTheta(lw) - cosHalfThetaMax) / (cosHalfThetaMin - cosHalfThetaMax);
		}
		return valMax;
	}

	__host__ __device__
	static float sample_pdf_(const UEDC* uedc, const Geometry& g, const float wlen, const float time, const Vectr3F& w) {
		Vectr3F lw;
		float cosHalfThetaMax = clamp(cos(0.5f * (*cast(uedc)->thetaMax)(g.gp, g.su, g.sn, g.uv, wlen, time)), 0.f, 1.f);
		Vectr3F g_sv = cross<Norml3F, Vectr3F, Vectr3F>(g.sn, g.su).normalize();
		toShadingCoordinates(g.su, g_sv, g.sn, w, lw);
		return UniformConeDistribution::pdf(&(lw[0]), cosHalfThetaMax);
	}

	__host__
	FalloffHemisphericalUEDC(
		const Pointer<const ShadingNode<float>>& power,
		const Pointer<const ShadingNode<float>>& thetaMin,
		const Pointer<const ShadingNode<float>>& thetaMax,
		const delta_dist_t& delta_dist_,
		const emission_0_t& emission_0_,
		const emission_1_t& emission_1_,
		const sample_dir_t& sample_dir_,
		const sample_pdf_t& sample_pdf_
	):
		UEDC(delta_dist_, emission_0_, emission_1_, sample_dir_, sample_pdf_),
		power(power), thetaMin(thetaMin), thetaMax(thetaMax) {
	}

public:
	friend class TypedPool<FalloffHemisphericalUEDC>;

	/*!
	 * \brief
	 * Creates a new FalloffHemisphericalUEDC.
	 *
	 * \param[in] power The total emitted spectral power at each point.
	 * \param[in] thetaMin The cone angle where the linear falloff starts.
	 * Must be in the range [0, PI] and <= thetaMax.
	 * \param[in] thetaMax The cone angle where the linear falloff ends.
	 * Must be in the range [0, PI] and >= thetaMin.
	 */
	__host__
	static Pointer<FalloffHemisphericalUEDC> build(
		MemoryPool& pool,
		const Pointer<const ShadingNode<float>>& power,
		const Pointer<const ShadingNode<float>>& thetaMin,
		const Pointer<const ShadingNode<float>>& thetaMax)
	{
		return Pointer<FalloffHemisphericalUEDC>::make(
			pool, power, thetaMin, thetaMax,
			delta_dist_t(virtualize(delta_dist_)),
			emission_0_t(emission_0_, [] __device__ () {return (void*)emission_0_;}),
			emission_1_t(virtualize(emission_1_)),
			sample_dir_t(virtualize(sample_dir_)),
			sample_pdf_t(virtualize(sample_pdf_))
		);
	}
};

}

#endif /* FALLOFFHEMISPHERICALUEDF_CUH_ */

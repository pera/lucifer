#ifndef ISOTROPICHEMISPHERICALUEDC_CUH_
#define ISOTROPICHEMISPHERICALUEDC_CUH_

#include "UEDC.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/Util.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"
#include "lucifer/random/CosineHemisphereDistribution.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * An isotropic hemispherical UEDC.
 *
 * An UEDC that emits light uniformly distributed on the hemisphere around the
 * surface normal vector. The total emitted spectral power is controlled by a
 * SpectralField.
 *
 * \author
 * Bruno Pera.
 */
class IsotropicHemisphericalUEDC: public UEDC {
private:
	const Pointer<const ShadingNode<float>> power;

	__host__ __device__
	static constexpr const IsotropicHemisphericalUEDC* cast(const UEDC* uedc) {
		return static_cast<const IsotropicHemisphericalUEDC*>(uedc);
	}

	__host__ __device__
	static bool delta_dist_(const UEDC* uedc) {
		return false;
	}

	__host__ __device__
	static float emission_0_(const UEDC* uedc, const Geometry& g, const float wlen, const float time) {
		return (*cast(uedc)->power)(g.gp, g.su, g.sn, g.uv, wlen, time);
	}

	__host__ __device__
	static float emission_1_(const UEDC* uedc, const Geometry& g, const float wlen, const float time, const Vectr3F& w) {
		return dot(g.sn, w) < 0.f ? 0.f : 1.f / (2.f * M_PI);
	}

	__host__ __device__
	static float sample_dir_(const UEDC* uedc, const Geometry& g, const float wlen, const float time, float rnd[2], Vectr3F* w, float* pdf) {
		Vectr3F lw;
		CosineHemisphereDistribution::sample(rnd, &(lw[0]), pdf);
		Vectr3F g_sv = cross<Norml3F, Vectr3F, Vectr3F>(g.sn, g.su).normalize();
		toWorldCoordinates(g.su, g_sv, g.sn, lw, *w);
		return 1.f / (2.f * M_PI);
	}

	__host__ __device__
	static float sample_pdf_(const UEDC* uedc, const Geometry& g, const float wlen, const float time, const Vectr3F& w) {
		if (dot(g.sn, w) <= 0.f) return 0.f;
		Vectr3F lw, g_sv = cross<Norml3F, Vectr3F, Vectr3F>(g.sn, g.su).normalize();
		toShadingCoordinates(g.su, g_sv, g.sn, w, lw);
		return CosineHemisphereDistribution::pdf(&(lw[0]));
	}

	__host__
	IsotropicHemisphericalUEDC(
		const Pointer<const ShadingNode<float>>& power,
		const delta_dist_t& delta_dist_,
		const emission_0_t& emission_0_,
		const emission_1_t& emission_1_,
		const sample_dir_t& sample_dir_,
		const sample_pdf_t& sample_pdf_
	):
		UEDC(delta_dist_, emission_0_, emission_1_, sample_dir_, sample_pdf_), power(power) {
	}

public:
	friend class TypedPool<IsotropicHemisphericalUEDC>;

	/*!
	 * \brief
	 * Creates a new IsotropicHemisphericalUEDC.
	 *
	 * \param[in] power The field that defines the total emitted power.
	 */
	__host__
	static Pointer<IsotropicHemisphericalUEDC> build(MemoryPool& pool, const Pointer<const ShadingNode<float>>& power) {
		return Pointer<IsotropicHemisphericalUEDC>::make(
			pool, power,
			delta_dist_t(virtualize(delta_dist_)),
			emission_0_t(emission_0_, [] __device__ () {return (void*)emission_0_;}),
			emission_1_t(virtualize(emission_1_)),
			sample_dir_t(virtualize(sample_dir_)),
			sample_pdf_t(virtualize(sample_pdf_))
		);
	}
};

}

#endif /* ISOTROPICHEMISPHERICALUEDF_CUH_ */

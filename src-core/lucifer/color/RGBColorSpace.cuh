#ifndef RGBCOLORSPACE_CUH_
#define RGBCOLORSPACE_CUH_

#include "Gamma.cuh"
#include "XYZColorSpace.cuh"
#include "lucifer/math/function/RegularGrid1D.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/math/simplex/Simplex.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/memory/Polymorphic.cuh"

namespace lucifer {

#define BASIS_SIZE 7
#define BASIS_R 0
#define BASIS_G 1
#define BASIS_B 2
#define BASIS_C 3
#define BASIS_M 4
#define BASIS_Y 5
#define BASIS_W 6

class RGBColorSpace: public Polymorphic {
private:
	Matrix3x3F toXYZ_;
	Matrix3x3F toRGB_;
	const Pointer<const Gamma> gamma_;
	Pointer<Spectrum> basis[BASIS_SIZE];

private:
	__host__
	inline static Matrix3x3F bradfordDir() {
		return Matrix3x3F(
				 0.8951000,  0.2664000, -0.1614000,
				-0.7502000,  1.7135000,  0.0367000,
				 0.0389000, -0.0685000,  1.0296000
		 );
	}

	__host__
	inline static Matrix3x3F bradfordInv() {
		return Matrix3x3F(
				 0.9869929, -0.1470543,  0.1599627,
				 0.4323053,  0.5183603,  0.0492912,
				-0.0085287,  0.0400428,  0.9684867
		);
	}

	__host__ __device__
	inline static RangeF& clampR(RangeF&& range, const float min, const float max) {
		if (range.lower < min) {
			range.lower = min;
		}
		if (range.upper > max) {
			range.upper = max;
		}
		return range;
	}

	__host__
	void buildReconstructionBasis(MemoryPool& pool, const Pointer<const XYZColorSpace>& xyzCS) {
		// sample RGB matching functions
		RegularGrid1D::dim_t n = xyzCS->samples();
		RangeF support = xyzCS->support();
		RegularGrid1D::array fr(pool, n);
		RegularGrid1D::array fg(pool, n);
		RegularGrid1D::array fb(pool, n);
		float factor = support.length() / n;
		for (RegularGrid1D::dim_t i = 0; i < n; i++) {
			float l = support.lower + (i * support.length()) / (n - 1);
			Color3F xyz(
				(*xyzCS->xMatchingFunction())(l),
				(*xyzCS->yMatchingFunction())(l),
				(*xyzCS->zMatchingFunction())(l)
			);
			Color3F rgb = toRGB_ * xyz;
			fr[i] = rgb[0] * factor;
			fg[i] = rgb[1] * factor;
			fb[i] = rgb[2] * factor;
		}
		// basic colors to be reconstructed
		Color3F rgbTargets[BASIS_SIZE] = {
				Color3F(1, 0, 0), // R
				Color3F(0, 1, 0), // G
				Color3F(0, 0, 1), // B
				Color3F(0, 1, 1), // C
				Color3F(1, 0, 1), // M
				Color3F(1, 1, 0), // Y
				Color3F(1, 1, 1), // W
		};
		RegularGrid1D::array s0(pool, n + 3);
		for (uint8_t c = 0; c < 7; c++) {
			// build simplex tableau
			Simplex t(n + 6, n + 3);
			// spectrum <= 1
			for (RegularGrid1D::dim_t i = 0; i < n; i++) {
				t[i + 1][i + 1] = t[i + 1][n + 4] = 1;
			}
			// error bounds
			for (RegularGrid1D::dim_t j = 0; j < n; j++) {
				// r error bounds
				t[n + 1][j + 1] = +fr[j]; t[n + 1][n + 1] = -1; t[n + 1][n + 4] = +rgbTargets[c][0];
				t[n + 2][j + 1] = -fr[j]; t[n + 2][n + 1] = -1; t[n + 2][n + 4] = -rgbTargets[c][0];
				// g error bounds
				t[n + 3][j + 1] = +fg[j]; t[n + 3][n + 2] = -1; t[n + 3][n + 4] = +rgbTargets[c][1];
				t[n + 4][j + 1] = -fg[j]; t[n + 4][n + 2] = -1; t[n + 4][n + 4] = -rgbTargets[c][1];
				// b error bounds
				t[n + 5][j + 1] = +fb[j]; t[n + 5][n + 3] = -1; t[n + 5][n + 4] = +rgbTargets[c][2];
				t[n + 6][j + 1] = -fb[j]; t[n + 6][n + 3] = -1; t[n + 6][n + 4] = -rgbTargets[c][2];
			}
			// goal: minimize sum of absolute error
			t[n + 7][n + 1] = t[n + 7][n + 2] = t[n + 7][n + 3] = 1;
			// solve
			if (!t.maximize()) {
				printf("could not build rgb reconstruction spectra\n");
				return;
			}
			t.maximumSolution(s0.begin());
			basis[c] = Pointer<Spectrum>(RegularGrid1D::build(pool, support.lower, support.upper, s0));
		}
	}

public:
	__host__
	RGBColorSpace(MemoryPool& pool, const Point2F& r, const Point2F& g, const Point2F& b, const Point3F& ws, const Point3F& wd, const Pointer<const Gamma>& gamma, const Pointer<const XYZColorSpace>& xyzCS):
	gamma_(gamma) {
		const float xr = r[0] / r[1]; const float yr = 1.f; const float zr = (1 - r[0] - r[1]) / r[1];
		const float xg = g[0] / g[1]; const float yg = 1.f; const float zg = (1 - g[0] - g[1]) / g[1];
		const float xb = b[0] / b[1]; const float yb = 1.f; const float zb = (1 - b[0] - b[1]) / b[1];

		toXYZ_[0][0] = xr; toXYZ_[0][1] = xg; toXYZ_[0][2] = xb;
		toXYZ_[1][0] = yr; toXYZ_[1][1] = yg; toXYZ_[1][2] = yb;
		toXYZ_[2][0] = zr; toXYZ_[2][1] = zg; toXYZ_[2][2] = zb;
		Point3F s = toXYZ_.inverse() * ws;

		toXYZ_[0][0] = s[0] * xr; toXYZ_[0][1] = s[1] * xg; toXYZ_[0][2] = s[2] * xb;
		toXYZ_[1][0] = s[0] * yr; toXYZ_[1][1] = s[1] * yg; toXYZ_[1][2] = s[2] * yb;
		toXYZ_[2][0] = s[0] * zr; toXYZ_[2][1] = s[1] * zg; toXYZ_[2][2] = s[2] * zb;
		toRGB_ = toXYZ_.inverse();

		Matrix3x3F bradDir = bradfordDir();
		Matrix3x3F bradInv = bradfordInv();
		Point3F src = bradDir * ws;
		Point3F dst = bradDir * wd;
		Matrix3x3F adapt;
		adapt[0][0] = dst[0] / src[0];
		adapt[1][1] = dst[1] / src[1];
		adapt[2][2] = dst[2] / src[2];
		adapt = bradInv * adapt * bradDir;

		toXYZ_ = adapt * toXYZ_;
		toRGB_ = toRGB_ * adapt.inverse();

		buildReconstructionBasis(pool, xyzCS);
	}

	__host__
	static Pointer<RGBColorSpace> build(MemoryPool& pool, const Point2F& r, const Point2F& g, const Point2F& b, const Point3F& ws, const Point3F& wd, const Pointer<const Gamma>& gamma, const Pointer<const XYZColorSpace>& xyzCS) {
		return Pointer<RGBColorSpace>::make(pool, pool, r, g, b, ws, wd, gamma, xyzCS);
	}

	__host__ __device__
	const Pointer<const Gamma>& gamma() const {
		return gamma_;
	}

	template<typename C> __host__ __device__
	C& toXYZ(const C& rgb, C& xyz) const {
		C tmp = rgb;
		gamma_->decode(tmp);
		xyz = toXYZ_ * tmp;
		return xyz;
	}

	template<typename C> __host__ __device__
	C toXYZ(const C& rgb) const {
		C xyz;
		return toXYZ(rgb, xyz);
	}

	template<typename C> __host__ __device__
	auto luminance(const C& rgb) const -> decltype(rgb[1]){
		return toXYZ(rgb)[1];
	}

	template<typename C> __host__ __device__
	C& toRGB(const C& xyz, C& rgb) const {
		rgb = toRGB_ * xyz;
		gamma_->encode(rgb);
		return rgb;
	}

	template<typename C> __host__ __device__
	C  toRGB(const C& xyz) const {
		C rgb;
		return toRGB(xyz, rgb);
	}

	__host__ __device__
	float spectrum(const Color3F& rgb, const float wavelength, const float min = 0.f, const float max = 1.f) const {
		Color3F dec = rgb;
		gamma_->decode(dec);
		if (dec[0] <= dec[1] && dec[1] <= dec[2]) {
			// white, cyan, blue
			float w_ = (dec[0] - 0.00f) * (*basis[BASIS_W])(wavelength);
			float c_ = (dec[1] - dec[0]) * (*basis[BASIS_C])(wavelength);
			float b_ = (dec[2] - dec[1]) * (*basis[BASIS_B])(wavelength);
			return clamp(w_ + c_ + b_, min, max);
		}
		if (dec[0] <= dec[2] && dec[2] <= dec[1]) {
			// white, cyan, green
			float w_ = (dec[0] - 0.00f) * (*basis[BASIS_W])(wavelength);
			float c_ = (dec[2] - dec[0]) * (*basis[BASIS_C])(wavelength);
			float g_ = (dec[1] - dec[2]) * (*basis[BASIS_G])(wavelength);
			return clamp(w_ + c_ + g_, min, max);
		}
		if (dec[1] <= dec[0] && dec[0] <= dec[2]) {
			// white, magenta, blue
			float w_ = (dec[1] - 0.00f) * (*basis[BASIS_W])(wavelength);
			float m_ = (dec[0] - dec[1]) * (*basis[BASIS_M])(wavelength);
			float b_ = (dec[2] - dec[0]) * (*basis[BASIS_B])(wavelength);
			return clamp(w_ + m_ + b_, min, max);
		}
		if (dec[1] <= dec[2] && dec[2] <= dec[0]) {
			// white, magenta, red
			float w_ = (dec[1] - 0.00f) * (*basis[BASIS_W])(wavelength);
			float m_ = (dec[2] - dec[1]) * (*basis[BASIS_M])(wavelength);
			float r_ = (dec[0] - dec[2]) * (*basis[BASIS_R])(wavelength);
			return clamp(w_ + m_ + r_, min, max);
		}
		if (dec[2] <= dec[0] && dec[0] <= dec[1]) {
			// white, yellow, green
			float w_ = (dec[2] - 0.00f) * (*basis[BASIS_W])(wavelength);
			float y_ = (dec[0] - dec[2]) * (*basis[BASIS_Y])(wavelength);
			float g_ = (dec[1] - dec[0]) * (*basis[BASIS_G])(wavelength);
			return clamp(w_ + y_ + g_, min, max);
		}
		if (dec[2] <= dec[1] && dec[1] <= dec[0]) {
			// white, yellow, red
			float w_ = (dec[2] - 0.00f) * (*basis[BASIS_W])(wavelength);
			float y_ = (dec[1] - dec[2]) * (*basis[BASIS_Y])(wavelength);
			float r_ = (dec[0] - dec[1]) * (*basis[BASIS_R])(wavelength);
			return clamp(w_ + y_ + r_, min, max);
		}
		return min;
	}

	__host__ __device__
	RangeF spectrum(const Color3F& rgb, const RangeF& wavelength, const float min = 0.f, const float max = 1.f) const {
		Color3F dec = rgb;
		gamma_->decode(dec);
		if (dec[0] <= dec[1] && dec[1] <= dec[2]) {
			// white, cyan, blue
			RangeF w_ = basis[BASIS_W]->evaluate(wavelength.lower, wavelength.upper) * (dec[0] - 0.00f);
			RangeF c_ = basis[BASIS_C]->evaluate(wavelength.lower, wavelength.upper) * (dec[1] - dec[0]);
			RangeF b_ = basis[BASIS_B]->evaluate(wavelength.lower, wavelength.upper) * (dec[2] - dec[1]);
			return clampR(w_ + c_ + b_, min, max);
		}
		if (dec[0] <= dec[2] && dec[2] <= dec[1]) {
			// white, cyan, green
			RangeF w_ = basis[BASIS_W]->evaluate(wavelength.lower, wavelength.upper) * (dec[0] - 0.00f);
			RangeF c_ = basis[BASIS_C]->evaluate(wavelength.lower, wavelength.upper) * (dec[2] - dec[0]);
			RangeF g_ = basis[BASIS_G]->evaluate(wavelength.lower, wavelength.upper) * (dec[1] - dec[2]);
			return clampR(w_ + c_ + g_, min, max);
		}
		if (dec[1] <= dec[0] && dec[0] <= dec[2]) {
			// white, magenta, blue
			RangeF w_ = basis[BASIS_W]->evaluate(wavelength.lower, wavelength.upper) * (dec[1] - 0.00f);
			RangeF m_ = basis[BASIS_M]->evaluate(wavelength.lower, wavelength.upper) * (dec[0] - dec[1]);
			RangeF b_ = basis[BASIS_B]->evaluate(wavelength.lower, wavelength.upper) * (dec[2] - dec[0]);
			return clampR(w_ + m_ + b_, min, max);
		}
		if (dec[1] <= dec[2] && dec[2] <= dec[0]) {
			// white, magenta, red
			RangeF w_ = basis[BASIS_W]->evaluate(wavelength.lower, wavelength.upper) * (dec[1] - 0.00f);
			RangeF m_ = basis[BASIS_M]->evaluate(wavelength.lower, wavelength.upper) * (dec[2] - dec[1]);
			RangeF r_ = basis[BASIS_R]->evaluate(wavelength.lower, wavelength.upper) * (dec[0] - dec[2]);
			return clampR(w_ + m_ + r_, min, max);
		}
		if (dec[2] <= dec[0] && dec[0] <= dec[1]) {
			// white, yellow, green
			RangeF w_ = basis[BASIS_W]->evaluate(wavelength.lower, wavelength.upper) * (dec[2] - 0.00f);
			RangeF y_ = basis[BASIS_Y]->evaluate(wavelength.lower, wavelength.upper) * (dec[0] - dec[2]);
			RangeF g_ = basis[BASIS_G]->evaluate(wavelength.lower, wavelength.upper) * (dec[1] - dec[0]);
			return clampR(w_ + y_ + g_, min, max);
		}
		if (dec[2] <= dec[1] && dec[1] <= dec[0]) {
			// white, yellow, red
			RangeF w_ = basis[BASIS_W]->evaluate(wavelength.lower, wavelength.upper) * (dec[2] - 0.00f);
			RangeF y_ = basis[BASIS_Y]->evaluate(wavelength.lower, wavelength.upper) * (dec[1] - dec[2]);
			RangeF r_ = basis[BASIS_R]->evaluate(wavelength.lower, wavelength.upper) * (dec[0] - dec[1]);
			return clampR(w_ + y_ + r_, min, max);
		}
		return RangeF{min, max};
	}
};

}

#endif /* RGBCOLORSPACE_CUH_ */

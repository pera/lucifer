#ifndef XYZCOLORSPACE_CUH_
#define XYZCOLORSPACE_CUH_

#include "Spectrum.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/Util.cuh"
#include "lucifer/math/function/RegularGrid1D.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/random/PiecewiseLinearDistribution1D.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/memory/Polymorphic.cuh"

#define CIE_5NM_LOWER 360E-9
#define CIE_5NM_UPPER 830E-9
#define CIE_5NM_LENGTH 95

namespace lucifer {

class XYZColorSpace: public Polymorphic {
private:
	float  lower_;
	float  upper_;
	int  samples_;
	const Pointer<const Spectrum> x;
	const Pointer<const Spectrum> y;
	const Pointer<const Spectrum> z;
	const Pointer<const PiecewiseLinearDistribution1D> histogram;

	__host__
	Pointer<const PiecewiseLinearDistribution1D> buildHistogram(MemoryPool& pool) {
		PiecewiseLinearDistribution1D::array_t data(pool, samples_);
		float step = (upper_ - lower_) / (samples_ - 1);
		for (int i = 0; i < samples_; i++) {
			float l = lower_ + i * step;
			data[i] = (*x)(l) + (*y)(l) + (*z)(l);
		}
		return Pointer<const PiecewiseLinearDistribution1D>(PiecewiseLinearDistribution1D::build(pool, RangeF(lower_, upper_), data));
	}

	__host__
	static Pointer<const Spectrum> cie_x_5nm(MemoryPool& pool) {
		auto values = RegularGrid1D::array{pool, {
				0.000129900000f, 0.000232100000f, 0.000414900000f, 0.000741600000f, 0.001368000000f, 0.002236000000f, 0.004243000000f,
				0.007650000000f, 0.014310000000f, 0.023190000000f, 0.043510000000f, 0.077630000000f, 0.134380000000f, 0.214770000000f,
				0.283900000000f, 0.328500000000f, 0.348280000000f, 0.348060000000f, 0.336200000000f, 0.318700000000f, 0.290800000000f,
				0.251100000000f, 0.195360000000f, 0.142100000000f, 0.095640000000f, 0.057950010000f, 0.032010000000f, 0.014700000000f,
				0.004900000000f, 0.002400000000f, 0.009300000000f, 0.029100000000f, 0.063270000000f, 0.109600000000f, 0.165500000000f,
				0.225749900000f, 0.290400000000f, 0.359700000000f, 0.433449900000f, 0.512050100000f, 0.594500000000f, 0.678400000000f,
				0.762100000000f, 0.842500000000f, 0.916300000000f, 0.978600000000f, 1.026300000000f, 1.056700000000f, 1.062200000000f,
				1.045600000000f, 1.002600000000f, 0.938400000000f, 0.854449900000f, 0.751400000000f, 0.642400000000f, 0.541900000000f,
				0.447900000000f, 0.360800000000f, 0.283500000000f, 0.218700000000f, 0.164900000000f, 0.121200000000f, 0.087400000000f,
				0.063600000000f, 0.046770000000f, 0.032900000000f, 0.022700000000f, 0.015840000000f, 0.011359160000f, 0.008110916000f,
				0.005790346000f, 0.004109457000f, 0.002899327000f, 0.002049190000f, 0.001439971000f, 0.000999949300f, 0.000690078600f,
				0.000476021300f, 0.000332301100f, 0.000234826100f, 0.000166150500f, 0.000117413000f, 0.000083075270f, 0.000058706520f,
				0.000041509940f, 0.000029353260f, 0.000020673830f, 0.000014559770f, 0.000010253980f, 0.000007221456f, 0.000005085868f,
				0.000003581652f, 0.000002522525f, 0.000001776509f, 0.000001251141f, }};
		float factor = 0.f;
		for (int i = 0; i < CIE_5NM_LENGTH; i++) {
			factor += values[i];
		}
		factor *= (CIE_5NM_UPPER - CIE_5NM_LOWER) / CIE_5NM_LENGTH;
		for (int i = 0; i < CIE_5NM_LENGTH; i++) {
			values[i] /= factor;
		}
		return Pointer<const Spectrum>(RegularGrid1D::build(pool, CIE_5NM_LOWER, CIE_5NM_UPPER, values));
	}

	__host__
	static Pointer<const Spectrum> cie_y_5nm(MemoryPool& pool) {
		auto values = RegularGrid1D::array{pool, {
				0.000003917000f, 0.000006965000f, 0.000012390000f, 0.000022020000f, 0.000039000000f, 0.000064000000f, 0.000120000000f,
				0.000217000000f, 0.000396000000f, 0.000640000000f, 0.001210000000f, 0.002180000000f, 0.004000000000f, 0.007300000000f,
				0.011600000000f, 0.016840000000f, 0.023000000000f, 0.029800000000f, 0.038000000000f, 0.048000000000f, 0.060000000000f,
				0.073900000000f, 0.090980000000f, 0.112600000000f, 0.139020000000f, 0.169300000000f, 0.208020000000f, 0.258600000000f,
				0.323000000000f, 0.407300000000f, 0.503000000000f, 0.608200000000f, 0.710000000000f, 0.793200000000f, 0.862000000000f,
				0.914850100000f, 0.954000000000f, 0.980300000000f, 0.994950100000f, 1.000000000000f, 0.995000000000f, 0.978600000000f,
				0.952000000000f, 0.915400000000f, 0.870000000000f, 0.816300000000f, 0.757000000000f, 0.694900000000f, 0.631000000000f,
				0.566800000000f, 0.503000000000f, 0.441200000000f, 0.381000000000f, 0.321000000000f, 0.265000000000f, 0.217000000000f,
				0.175000000000f, 0.138200000000f, 0.107000000000f, 0.081600000000f, 0.061000000000f, 0.044580000000f, 0.032000000000f,
				0.023200000000f, 0.017000000000f, 0.011920000000f, 0.008210000000f, 0.005723000000f, 0.004102000000f, 0.002929000000f,
				0.002091000000f, 0.001484000000f, 0.001047000000f, 0.000740000000f, 0.000520000000f, 0.000361100000f, 0.000249200000f,
				0.000171900000f, 0.000120000000f, 0.000084800000f, 0.000060000000f, 0.000042400000f, 0.000030000000f, 0.000021200000f,
				0.000014990000f, 0.000010600000f, 0.000007465700f, 0.000005257800f, 0.000003702900f, 0.000002607800f, 0.000001836600f,
				0.000001293400f, 0.000000910930f, 0.000000641530f, 0.000000451810f, }};
		float factor = 0.f;
		for (int i = 0; i < CIE_5NM_LENGTH; i++) {
			factor += values[i];
		}
		factor *= (CIE_5NM_UPPER - CIE_5NM_LOWER) / CIE_5NM_LENGTH;
		for (int i = 0; i < CIE_5NM_LENGTH; i++) {
			values[i] /= factor;
		}
		return Pointer<const Spectrum>(RegularGrid1D::build(pool, CIE_5NM_LOWER, CIE_5NM_UPPER, values));
	}

	__host__
	static Pointer<const Spectrum> cie_z_5nm(MemoryPool& pool) {
		auto values = RegularGrid1D::array{pool, {
				0.000606100000f, 0.001086000000f, 0.001946000000f, 0.003486000000f, 0.006450001000f, 0.010549990000f, 0.020050010000f,
				0.036210000000f, 0.067850010000f, 0.110200000000f, 0.207400000000f, 0.371300000000f, 0.645600000000f, 1.039050100000f,
				1.385600000000f, 1.622960000000f, 1.747060000000f, 1.782600000000f, 1.772110000000f, 1.744100000000f, 1.669200000000f,
				1.528100000000f, 1.287640000000f, 1.041900000000f, 0.812950100000f, 0.616200000000f, 0.465180000000f, 0.353300000000f,
				0.272000000000f, 0.212300000000f, 0.158200000000f, 0.111700000000f, 0.078249990000f, 0.057250010000f, 0.042160000000f,
				0.029840000000f, 0.020300000000f, 0.013400000000f, 0.008749999000f, 0.005749999000f, 0.003900000000f, 0.002749999000f,
				0.002100000000f, 0.001800000000f, 0.001650001000f, 0.001400000000f, 0.001100000000f, 0.001000000000f, 0.000800000000f,
				0.000600000000f, 0.000340000000f, 0.000240000000f, 0.000190000000f, 0.000100000000f, 0.000049999990f, 0.000030000000f,
				0.000020000000f, 0.000010000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f,
				0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f,
				0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f,
				0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f,
				0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f,
				0.000000000000f, 0.000000000000f, 0.000000000000f, 0.000000000000f, }};
		float factor = 0.f;
		for (int i = 0; i < CIE_5NM_LENGTH; i++) {
			factor += values[i];
		}
		factor *= (CIE_5NM_UPPER - CIE_5NM_LOWER) / CIE_5NM_LENGTH;
		for (int i = 0; i < CIE_5NM_LENGTH; i++) {
			values[i] /= factor;
		}
		return Pointer<const Spectrum>(RegularGrid1D::build(pool, CIE_5NM_LOWER, CIE_5NM_UPPER, values));
	}

	__host__
	XYZColorSpace(MemoryPool& pool):
	lower_(CIE_5NM_LOWER), upper_(CIE_5NM_UPPER), samples_(CIE_5NM_LENGTH), x(cie_x_5nm(pool)), y(cie_y_5nm(pool)), z(cie_z_5nm(pool)), histogram(buildHistogram(pool)) {
	}

public:
	friend class TypedPool<XYZColorSpace>;

	__host__
	static Pointer<XYZColorSpace> build(MemoryPool& pool) {
		return Pointer<XYZColorSpace>::make(pool, pool);
	}

	__host__ __device__
	const RangeF& support() const  {
		return histogram->support();
	}

	__host__ __device__
	int samples() const {
		return samples_;
	}

	__host__ __device__
	const Pointer<const Spectrum>& xMatchingFunction() const {
		return x;
	}

	__host__ __device__
	const Pointer<const Spectrum>& yMatchingFunction() const {
		return y;
	}

	__host__ __device__
	const Pointer<const Spectrum>& zMatchingFunction() const {
		return z;
	}

	__host__ __device__
	const Pointer<const PiecewiseLinearDistribution1D>& xyzHistogram() const {
		return histogram;
	}

	__host__ __device__
	float Y(const Spectrum& spectrum) const {
		return integrate(*y, spectrum, lower_, upper_, samples_);
	}

	__host__ __device__
	float Y(const float wavelength, const float value) const {
		return value * (*y)(wavelength);
	}

	__host__ __device__
	Color3F& XYZ(const Spectrum& spectrum, Color3F& color) const {
		color[0] = integrate(*x, spectrum, lower_, upper_, samples_);
		color[1] = integrate(*y, spectrum, lower_, upper_, samples_);
		color[2] = integrate(*z, spectrum, lower_, upper_, samples_);
		return color;
	}

	__host__ __device__
	Color3F& XYZ(const float wavelength, const float value, Color3F& color) const {
		color[0] = (*x)(wavelength) * value;
		color[1] = (*y)(wavelength) * value;
		color[2] = (*z)(wavelength) * value;
		return color;
	}

	__host__ __device__
	Color3F XYZ(const float wavelength, const float value) const {
		Color3F result;
		return XYZ(wavelength, value, result);
	}
};

}

#endif /* XYZCOLORSPACE_CUH_ */

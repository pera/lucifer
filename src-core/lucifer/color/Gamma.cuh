#ifndef GAMMA_CUH_
#define GAMMA_CUH_

#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/memory/Virtual.cuh"

namespace lucifer {

class Gamma: public Polymorphic {
private:
	const Virtual<void, const Gamma*, Color3F&> encode_;
	const Virtual<void, const Gamma*, Color3F&> decode_;

public:
	__host__
	Gamma(
		const Virtual<void, const Gamma*, Color3F&> encode_,
		const Virtual<void, const Gamma*, Color3F&> decode_):
	encode_(encode_), decode_(decode_) {
	}

	__host__ __device__
	void encode(Color3F& val) const {
		encode_(this, val);
	}

	__host__ __device__
	void decode(Color3F& val) const {
		decode_(this, val);
	}

	__host__ __device__
	void encode(Range<Color3F>& val) const;

	__host__ __device__
	void decode(Range<Color3F>& val) const;

	__host__ __device__
	void encode(Color3R& val) const;

	__host__ __device__
	void decode(Color3R& val) const;
};

class NoGamma: public Gamma {
private:
	friend class TypedPool<NoGamma>;
	__host__ __device__
	static void encode_(const Gamma* this_, Color3F& val) {
	}

	__host__ __device__
	static void decode_(const Gamma* this_, Color3F& val) {
	}

	__host__
	NoGamma(
		const Virtual<void, const Gamma*, Color3F&>& encode,
		const Virtual<void, const Gamma*, Color3F&>& decode
	): Gamma(encode, decode) {
	}

public:
	__host__
	static Pointer<NoGamma> build(MemoryPool& pool) {
		return Pointer<NoGamma>::make(
			pool,
			Virtual<void, const Gamma*, Color3F&>(virtualize(encode_)),
			Virtual<void, const Gamma*, Color3F&>(virtualize(decode_))
		);
	}

};

class SimplifiedGamma: public Gamma {
private:
	friend class TypedPool<SimplifiedGamma>;
	float gamma;
	float invGamma;

	__host__ __device__
	static void encode_(const Gamma* this_, Color3F& val);

	__host__ __device__
	static void decode_(const Gamma* this_, Color3F& val);

	__host__
	SimplifiedGamma(
		float gamma,
		const Virtual<void, const Gamma*, Color3F&>& encode,
		const Virtual<void, const Gamma*, Color3F&>& decode
	);

public:
	__host__
	static Pointer<SimplifiedGamma> build(MemoryPool& pool, const float gamma);
};

class DetailedGamma: public Gamma {
private:
	friend class TypedPool<DetailedGamma>;
	float offset;
	float gamma;
	float invGamma;
	float transition;
	float slope;

	__host__ __device__
	void encode(float& val) const;

	__host__ __device__
	void decode(float& val) const;

	__host__ __device__
	static void encode_(const Gamma* this_, Color3F& val);

	__host__ __device__
	static void decode_(const Gamma* this_, Color3F& val);

	__host__
	DetailedGamma(
		float offset, float gamma, float transition, float slope,
		const Virtual<void, const Gamma*, Color3F&>& encode,
		const Virtual<void, const Gamma*, Color3F&>& decode
	);

public:
	__host__
	static Pointer<DetailedGamma> build(MemoryPool& pool, float offset, float gamma, float transition, float slope);
};

}

#endif /* GAMMA_CUH_ */

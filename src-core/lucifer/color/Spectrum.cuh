#ifndef SPECTRUM_CUH_
#define SPECTRUM_CUH_

#include "lucifer/math/function/Function.cuh"

namespace lucifer {

using Spectrum = Function<float>;

}

#endif /* SPECTRUM_CUH_ */

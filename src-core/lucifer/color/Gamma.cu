#include "Gamma.cuh"

namespace lucifer {

/******************************************************************************
* GAMMA
******************************************************************************/
__host__ __device__
void Gamma::encode(Range<Color3F>& val) const {
	encode(val.lower);
	encode(val.upper);
}

__host__ __device__
void Gamma::decode(Range<Color3F>& val) const {
	decode(val.lower);
	decode(val.upper);
}

__host__ __device__
void Gamma::encode(Color3R& val) const {
	Range<Color3F> tmp(Color3F(val[0].lower, val[1].lower, val[2].lower), Color3F(val[0].upper, val[1].upper, val[2].upper));
	encode(tmp);
	val = Color3R(Range<float>(tmp.lower[0], tmp.upper[0]), Range<float>(tmp.lower[1], tmp.upper[1]), Range<float>(tmp.lower[2], tmp.upper[2]));
}

__host__ __device__
void Gamma::decode(Color3R& val) const {
	Range<Color3F> tmp(Color3F(val[0].lower, val[1].lower, val[2].lower), Color3F(val[0].upper, val[1].upper, val[2].upper));
	decode(tmp);
	val = Color3R(Range<float>(tmp.lower[0], tmp.upper[0]), Range<float>(tmp.lower[1], tmp.upper[1]), Range<float>(tmp.lower[2], tmp.upper[2]));
}

/******************************************************************************
* SIMPLIFIED GAMMA
******************************************************************************/
__host__ __device__
void SimplifiedGamma::encode_(const Gamma* this_, Color3F& val) {
	auto g = static_cast<const SimplifiedGamma*>(this_);
	if (val[0] > 0.f) val[0] = powf(val[0], g->invGamma);
	if (val[1] > 0.f) val[1] = powf(val[1], g->invGamma);
	if (val[2] > 0.f) val[2] = powf(val[2], g->invGamma);
}

__host__ __device__
void SimplifiedGamma::decode_(const Gamma* this_, Color3F& val) {
	auto g = static_cast<const SimplifiedGamma*>(this_);
	if (val[0] > 0.f) val[0] = powf(val[0], g->gamma);
	if (val[1] > 0.f) val[1] = powf(val[1], g->gamma);
	if (val[2] > 0.f) val[2] = powf(val[2], g->gamma);
}

__host__
SimplifiedGamma::SimplifiedGamma(
	float gamma,
	const Virtual<void, const Gamma*, Color3F&>& encode,
	const Virtual<void, const Gamma*, Color3F&>& decode
): Gamma(encode, decode), gamma(gamma), invGamma(1.f / gamma) {
}

__host__
Pointer<SimplifiedGamma> SimplifiedGamma::build(MemoryPool& pool, const float gamma) {
	return Pointer<SimplifiedGamma>::make(
		pool, gamma,
		Virtual<void, const Gamma*, Color3F&>(virtualize(encode_)),
		Virtual<void, const Gamma*, Color3F&>(virtualize(decode_))
	);
}

/******************************************************************************
* DETAILED GAMMA
******************************************************************************/
__host__ __device__
void DetailedGamma::encode(float& val) const {
	if ((val >= 0) && (val <= transition)) val = val * slope;
	else val = ((1 + offset) * powf(val, invGamma)) - offset;
}

__host__ __device__
void DetailedGamma::decode(float& val) const {
	if ((val >= 0) && (val <= (slope * transition))) val = val / slope;
	else val = powf((val + offset) / (1 + offset), gamma);
}

__host__ __device__
void DetailedGamma::encode_(const Gamma* this_, Color3F& val) {
	auto g = static_cast<const DetailedGamma*>(this_);
	g->encode(val[0]);
	g->encode(val[1]);
	g->encode(val[2]);
}

__host__ __device__
void DetailedGamma::decode_(const Gamma* this_, Color3F& val) {
	auto g = static_cast<const DetailedGamma*>(this_);
	g->decode(val[0]);
	g->decode(val[1]);
	g->decode(val[2]);
}

__host__
DetailedGamma::DetailedGamma(
	float offset, float gamma, float transition, float slope,
	const Virtual<void, const Gamma*, Color3F&>& encode,
	const Virtual<void, const Gamma*, Color3F&>& decode
): Gamma(encode, decode), offset(offset), gamma(gamma), invGamma(1.f / gamma), transition(transition), slope(slope) {
}

__host__
Pointer<DetailedGamma> DetailedGamma::build(MemoryPool& pool, float offset, float gamma, float transition, float slope) {
	return Pointer<DetailedGamma>::make(
		pool, offset, gamma, transition, slope,
		Virtual<void, const Gamma*, Color3F&>(virtualize(encode_)),
		Virtual<void, const Gamma*, Color3F&>(virtualize(decode_))
	);
}

} // namespace lucifer

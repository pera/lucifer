#ifndef RAYLEIGHPHASEFUNCTION_CUH_
#define RAYLEIGHPHASEFUNCTION_CUH_

#include "PhaseFunction.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/random/UniformSphereDistribution.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * Rayleight Phase Function.
 *
 * The phase function associated with the Rayleigh scattering process that
 * describes the scattering of light or other electromagnetic radiation by
 * particles much smaller than the wavelength of the radiation.
 *
 * \author
 * Bruno Pera.
 */
class RayleighPhaseFunction: public PhaseFunction {
private:
	static constexpr float FACTOR = 3.f / (16.f * PI<float>());

private:
	__host__ __device__
	static float evaluate(float cosTheta) {
		cosTheta = clamp(cosTheta, -1.f, +1.f);
		return FACTOR * (1.f + cosTheta * cosTheta);
	}

	__host__ __device__
	static float evl_(const PhaseFunction* phase, const Point3F& p, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo) {
		return evaluate(-dot( wi, wo));
	}

	__host__ __device__
	static float smp_(const PhaseFunction* phase, const Point3F& p, const float wlen, const float time, const Vectr3F& wo, float rnd[2], Vectr3F* wi, float* pdf) {
		UniformSphereDistribution::sample(rnd, &((*wi)[0]), pdf);
		return evaluate(-dot(*wi, wo));
	}

	__host__ __device__
	static float pdf_(const PhaseFunction* phase, const Point3F& p, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi) {
		return UniformSphereDistribution::PDF;
	}

	__host__
	RayleighPhaseFunction(const evl_t& evl_, const smp_t& smp_, const pdf_t& pdf_):
	PhaseFunction(evl_, smp_, pdf_) {
	}

public:
	friend class TypedPool<RayleighPhaseFunction>;

	__host__
	static Pointer<RayleighPhaseFunction> build(MemoryPool& pool) {
		return Pointer<RayleighPhaseFunction>::make(
			pool,
			evl_t(virtualize(evl_)),
			smp_t(virtualize(smp_)),
			pdf_t(virtualize(pdf_))
		);
	}
};

}

#endif /* RAYLEIGHPHASEFUNCTION_CUH_ */

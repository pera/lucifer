#ifndef HENYEYGREENSTEINPHASEFUNCTION_CUH_
#define HENYEYGREENSTEINPHASEFUNCTION_CUH_

#include "PhaseFunction.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/random/UniformSphereDistribution.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * Henyey-Greenstein Phase Function.
 *
 * The
 * <href a="http://www.oceanopticsbook.info/view/scattering/the_henyeygreenstein_phase_function">
 * Henyey-Greenstein phase function</a> is a simple analytical model that can
 * describe forward and backward scattering using a single asymmetry parameter.
 *
 * \author
 * Bruno Pera.
 */
class HenyeyGreensteinPhaseFunction: public PhaseFunction {
private:
	const Pointer<const ShadingNode<float>> g;

private:
	__host__ __device__
	static constexpr Vectr3F T() {
		return Vectr3F(0.f, 0.f, 0.f);
	}

	__host__ __device__
	static constexpr Norml3F N() {
		return Norml3F(0.f, 0.f, 0.f);
	}

	__host__ __device__
	static constexpr Point2F U() {
		return Point2F(0.f, 0.f);
	}

	__host__ __device__
	static float evaluate(float g_, const float cosTheta) {
		g_ = clamp(g_, -1.f, +1.f);
		float term = 1.f + g_ * g_ - 2.f * g_ * cosTheta;
		return (1.f - g_ * g_) / (4.f * PI<float>() * term * sqrt(term));
	}

	__host__ __device__
	static float evl_(const PhaseFunction* phase, const Point3F& p, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo) {
		auto this_ = static_cast<const HenyeyGreensteinPhaseFunction*>(phase);
		return evaluate((*this_->g)(p, T(), N(), U(), wlen, time), -dot( wi, wo));
	}

	__host__ __device__
	static float smp_(const PhaseFunction* phase, const Point3F& p, const float wlen, const float time, const Vectr3F& wo, float rnd[2], Vectr3F* wi, float* pdf) {
		auto this_ = static_cast<const HenyeyGreensteinPhaseFunction*>(phase);
		float cosTheta;
		float g_ = (*this_->g)(p, T(), N(), U(), wlen, time);
		if (abs(g_) < 1E-3) {
			cosTheta = 1.f - 2.f * rnd[0];
		} else{
			float term = (1.f - g_ * g_) / (1.f - g_ + 2.f * g_ * rnd[0]);
			cosTheta = (1.f + g_ * g_ - term * term) / (2.f * g_);
		}
		float sinTheta = sqrt(clamp(1.f - cosTheta * cosTheta, 0.f, 1.f));
		float phi = 2.f * PI<float>() * rnd[1];
		Vectr3F v1, v2;
		orthonormalBasis(wo, &v1, &v2);
		*wi = sphericalDirection(sinTheta, cosTheta, phi, v1, v2, -wo);
		*pdf = evaluate(g_, cosTheta);
		return *pdf;
	}

	__host__ __device__
	static float pdf_(const PhaseFunction* phase, const Point3F& p, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi) {
		auto this_ = static_cast<const HenyeyGreensteinPhaseFunction*>(phase);
		return evaluate((*this_->g)(p, T(), N(), U(), wlen, time), -dot(wi, wo));
	}

	__host__
	HenyeyGreensteinPhaseFunction(const Pointer<const ShadingNode<float>>& asymmetry, const evl_t& evl_, const smp_t& smp_, const pdf_t& pdf_):
	PhaseFunction(evl_, smp_, pdf_), g(asymmetry) {
	}

public:
	friend class TypedPool<HenyeyGreensteinPhaseFunction>;

	/*!
	 * \brief
	 * Creates a new HenyeyGreensteinPhaseFunction.
	 *
	 * \param[in] asymmetry The spectral asymmetry parameter that controls the
	 * relative amounts of forward and backward scattering. Must be between -1
	 * (complety back scattering) and +1 (complete forward scattering),
	 * otherwise the behaviour is undefined.
	 */
	__host__
	static Pointer<HenyeyGreensteinPhaseFunction> build(MemoryPool& pool, const Pointer<const ShadingNode<float>>& asymmetry) {
		return Pointer<HenyeyGreensteinPhaseFunction>::make(
			pool, asymmetry,
			evl_t(virtualize(evl_)),
			smp_t(virtualize(smp_)),
			pdf_t(virtualize(pdf_))
		);
	}
};

}

#endif /* HENYEYGREENSTEINPHASEFUNCTION_CUH_ */

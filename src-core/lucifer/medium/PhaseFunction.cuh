#ifndef PHASEFUNCTION_CUH_
#define PHASEFUNCTION_CUH_

#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/memory/Virtual.cuh"

namespace lucifer {

/*!
 * \brief
 * Phase Function interface.
 *
 * Describes the angular distribution of light scattering within the medium.
 *
 * A physically correct phase function must:
 * * Respect the Helmholtz’s law of reciprocity: p(wi, wo) = p(wo, wi).
 * * Be normalized: For any fixed incoming direction wi, the integral over the
 * entire sphere of outgoing directions wo is 1.
 *
 * \author
 * Bruno Pera.
 */
class PhaseFunction: public Polymorphic {
public:
	using evl_t = Virtual<float, const PhaseFunction*, const Point3F&, const float, const float, const Vectr3F&, const Vectr3F&>;
	using pdf_t = Virtual<float, const PhaseFunction*, const Point3F&, const float, const float, const Vectr3F&, const Vectr3F&>;
	using smp_t = Virtual<float, const PhaseFunction*, const Point3F&, const float, const float, const Vectr3F&, float[2], Vectr3F*, float*>;

private:
	const evl_t evl_;
	const smp_t smp_;
	const pdf_t pdf_;

public:
	__host__
	PhaseFunction(const evl_t& evl_, const smp_t& smp_, const pdf_t& pdf_):
	evl_(evl_), smp_(smp_), pdf_(pdf_) {
	}

	/*!
	 * \brief
	 * Evaluates the phase function.
	 *
	 * \param[in] p The position within the medium in material's coordinates.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wi The normalized light incoming direction.
	 * \param[in] wo The normalized light outgoing direction.
	 *
	 * \return The value of the phase function.
	 */
	__host__ __device__
	float operator()(const Point3F& p, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo) const {
		return evl_(this, p, wlen, time, wi, wo);
	}

	/*!
	 * \brief
	 * Samples an incoming light direction given an outgoing direction.
	 *
	 * \param[in] p The position within the medium.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wo The normalized light outgoing direction.
	 * \param[in] rnd A pair of uniformly sampled numbers in [0, 1].
	 * \param[out] wi The sampled normalized light incoming direction.
	 * \param[out] pdf The PDF of sampling `wi` with respect to the solid
	 * angle measure.
	 *
	 * \return The phase function value at the sampled direction.
	 */
	__host__ __device__
	float sampleWi(const Point3F& p, const float wlen, const float time, const Vectr3F& wo, float rnd[2], Vectr3F* wi, float* pdf) const {
		return smp_(this, p, wlen, time, wo, rnd, wi, pdf);
	}

	/*!
	 * \brief
	 * Computes the PDF of sampling an incoming direction given an outgoing direction.
	 *
	 * \param[in] p The position within the medium in material's coordinates.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wo The normalized light outgoing direction.
	 * \param[in] wi The normalized light incoming direction.
	 *
	 * \return The PDF with respect to the solid angle measure.
	 */
	__host__ __device__
	float sampleWiPdf(const Point3F& p, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi) const {
		return pdf_(this, p, wlen, time, wo, wi);
	}
};

}

#endif /* PHASEFUNCTION_CUH_ */

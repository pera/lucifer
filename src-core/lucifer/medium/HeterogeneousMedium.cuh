#ifndef HETEROGENEOUSMEDIUM_CUH_
#define HETEROGENEOUSMEDIUM_CUH_

#include "Medium.cuh"
#include "OpacityNode.cuh"
#include "PhaseFunction.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * A light-interacting medium that has spatially varying attributes.
 *
 * \author
 * Bruno Pera.
 */
class HeterogeneousMedium: public Medium {
private:
	const Pointer<const OpacityNode> opacity_;
	const Pointer<const ShadingNode<float>> scattering_;
	const Pointer<const ShadingNode<float>> refractiveIndex_;
	const Pointer<const PhaseFunction> phaseFunction_;

private:
	static constexpr float __TR_THRESHOLD__ = .1f;
	static constexpr   int __TR_MAX_DEPTH__ = 256;

	__host__ __device__
	static constexpr Vectr3F T() {
		return Vectr3F(0.f, 0.f, 0.f);
	}

	__host__ __device__
	static constexpr Norml3F N() {
		return Norml3F(0.f, 0.f, 0.f);
	}

	__host__ __device__
	static constexpr Point2F U() {
		return Point2F(0.f, 0.f);
	}

	__host__ __device__
	static const HeterogeneousMedium* cast(const Medium* medium) {
		return static_cast<const HeterogeneousMedium*>(medium);
	}

	__host__ __device__
	float maxSigmaT(const Ray& ray) const {
		const Point3R p = ray(ray.r);
		const float sigma_a = opacity_->attenuation(p, Vectr3R(), Norml3R(), Point2R(), RangeF(ray.l), RangeF(ray.t)).upper;
		const float sigma_s = (*scattering_)(p, Vectr3R(), Norml3R(), Point2R(), RangeF(ray.l), RangeF(ray.t)).upper;
		return sigma_a + sigma_s;
	}

	__host__ __device__
	static ComplexF refract_index_(const Medium* medium, const Point3F& p, const Point2F& uv, const float wlen, const float time) {
		return ComplexF((*cast(medium)->refractiveIndex_)(p, T(), N(), uv, wlen, time), cast(medium)->opacity_->extinction(p, T(), N(), uv, wlen, time));

	}

	__host__ __device__
	static float transmittance_(const Medium* medium, const Ray& ray, RNG* rng1D) {
		float sigma_t_max = cast(medium)->maxSigmaT(ray);
		if (isinf(ray.r.upper)) {
			return sigma_t_max > 0.f ? 0.f: 1.0f;
		}
		float rnd, tr = 1.f;
		float dist = ray.r.lower;
		if (sigma_t_max > 0.f) {
			uint16_t n = 0;
			while (true) {
				rng1D->next(&rnd);
				dist -= log(1 - rnd) / sigma_t_max;
				if (dist > ray.r.upper) {
					break;
				}
				const Point3F p = ray(dist);
				const float sigma_a = max(cast(medium)->opacity_->attenuation(p, T(), N(), U(), ray.l, ray.t), 0.f);
				const float sigma_s = max((*cast(medium)->scattering_)(p, T(), N(), U(), ray.l, ray.t), 0.f);
				const float sigma_t = sigma_s + sigma_a;
				tr *= 1 - max(0.f, sigma_t / sigma_t_max);
				// russian roulette
				if (n > __TR_MAX_DEPTH__ || tr < __TR_THRESHOLD__) {
					rng1D->next(&rnd);
					const float q = max(.05f, 1 - tr);
					if (rnd < q) {
						return 0.f;
					}
					tr /= 1 - q;
				}
				n++;
			}
		}
		return tr;
	}

	__host__ __device__
	static bool sample_medium_(const Medium* medium, const Ray& ray, RNG* rng1D, Point3F* p, float* beta) {
		// normalize ray
		const float rlen = ray.d.length();
		const float dmax = ray.r.upper * rlen;
		const Vectr3F rd = ray.d / rlen;
		// sigma_t max
		const float sigma_t_max = cast(medium)->maxSigmaT(ray);
		float rnd;
		if (sigma_t_max >= 0.f) {
			float dist = ray.r.lower * rlen;
			while(true) {
				// update potential interaction point
				rng1D->next(&rnd);
				dist -= log(1 - rnd) / sigma_t_max;
				if (dist > dmax) {
					break;
				}
				*p = ray.o + rd * dist;
				// interaction at point p?
				const float sigma_a = max(cast(medium)->opacity_->attenuation(*p, T(), N(), U(), ray.l, ray.t), 0.f);
				const float sigma_s = max((*cast(medium)->scattering_)(*p, T(), N(), U(), ray.l, ray.t), 0.f);
				const float sigma_t = sigma_s + sigma_a;
				rng1D->next(&rnd);
				if (rnd < sigma_t / sigma_t_max) {
					// sampled medium
					rng1D->next(&rnd);
					*beta = sigma_s / sigma_t;
					return true;
				}
			}
		}
		// sampled surface
		rng1D->next(&rnd);
		*beta = 1.f;
		return false;
	}

	__host__ __device__
	static float evaluatephase_(const Medium* medium, const Point3F& p, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo) {
		return (*cast(medium)->phaseFunction_)(p, wlen, time, wi, wo);
	}

	__host__ __device__
	static float sample_wi_vec_(const Medium* medium, const Point3F& p, const float wlen, const float time, const Vectr3F& wo, float rnd[2], Vectr3F* wi, float* pdf) {
		return cast(medium)->phaseFunction_->sampleWi(p, wlen, time, wo, rnd, wi, pdf);
	}

	__host__ __device__
	static float sample_wi_pdf_(const Medium* medium, const Point3F& p, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi) {
		return cast(medium)->phaseFunction_->sampleWiPdf(p, wlen, time, wo, wi);
	}

	__host__
	HeterogeneousMedium(
		const Pointer<const OpacityNode>& attenuation,
		const Pointer<const ShadingNode<float>>& scattering,
		const Pointer<const ShadingNode<float>>& refractiveIndex,
		const Pointer<const PhaseFunction>& phaseFunction,
		const bool volumetric,
		const uint8_t priority,
		const refract_index_t& refract_index_,
		const transmittance_t& transmittance_,
		const sample_medium_t& sample_medium_,
		const evaluatephase_t& evaluatephase_,
		const sample_wi_vec_t& sample_wi_vec_,
		const sample_wi_pdf_t& sample_wi_pdf_
	):
		Medium(
				refract_index_,
				transmittance_,
				sample_medium_,
				evaluatephase_,
				sample_wi_vec_,
				sample_wi_pdf_,
				priority,
				volumetric
		),
		opacity_(attenuation),
		scattering_(scattering),
		refractiveIndex_(refractiveIndex),
		phaseFunction_(phaseFunction) {
	}

public:
	friend class TypedPool<HeterogeneousMedium>;

	/*!
	 * \brief
	 * Creates a new HeterogeneousMedium
	 *
	 * \param[in] attenuation The spectral attenuation coefficient field.
	 * \param[in] scattering The spectral scattering coefficient field.
	 * \param[in] refractiveIndex The spectral refractive index field.
	 * \param[in] phaseFunction The PhaseFunction.
	 * \param[in] volumetric If the medium is actually enclosed by a volume.
	 * \param[in] priority The priority used to decide among overlapping media.
	 */
	__host__
	static Pointer<HeterogeneousMedium> build(
		MemoryPool& pool,
		const Pointer<const OpacityNode>& attenuation,
		const Pointer<const ShadingNode<float>>& scattering,
		const Pointer<const ShadingNode<float>>& refractiveIndex,
		const Pointer<const PhaseFunction>& phaseFunction,
		const bool volumetric,
		const uint8_t priority) {
		return Pointer<HeterogeneousMedium>::make(
			pool, attenuation, scattering, refractiveIndex, phaseFunction, volumetric, priority,
			refract_index_t(virtualize(refract_index_)),
			transmittance_t(virtualize(transmittance_)),
			sample_medium_t(virtualize(sample_medium_)),
			evaluatephase_t(virtualize(evaluatephase_)),
			sample_wi_vec_t(virtualize(sample_wi_vec_)),
			sample_wi_pdf_t(virtualize(sample_wi_pdf_))
		);
	}
};

}

#endif /* HETEROGENEOUSMEDIUM_CUH_ */

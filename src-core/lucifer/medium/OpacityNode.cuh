#ifndef OPACITYNODE_CUH_
#define OPACITYNODE_CUH_

#include "lucifer/node/shading/ShadingNode.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/memory/Virtual.cuh"

namespace lucifer {

/******************************************************************************
 * OpacityNode Inteface
 *****************************************************************************/
class OpacityNode: public Polymorphic {
public:
	using scalar_eval_t = Virtual<float, const OpacityNode*, const Point3F&, const Vectr3F&, const Norml3F&, const Point2F&, const float&, const float&>;
	using ranged_eval_t = Virtual<RangeF, const OpacityNode*, const Point3R&, const Vectr3R&, const Norml3R&, const Point2R&, const RangeF&, const RangeF&>;

private:
	scalar_eval_t scalar_att_;
	ranged_eval_t ranged_att_;
	scalar_eval_t scalar_ext_;
	ranged_eval_t ranged_ext_;

public:
	__host__
	OpacityNode(const scalar_eval_t& scalar_att_, const ranged_eval_t& ranged_att_, const scalar_eval_t& scalar_ext_, const ranged_eval_t& ranged_ext_):
	scalar_att_(scalar_att_), ranged_att_(ranged_att_), scalar_ext_(scalar_ext_), ranged_ext_(ranged_ext_) {
	}

	__host__ __device__
	float attenuation(const Point3F& p, const Vectr3F& t, const Norml3F& n, const Point2F& u, const float& wlen, const float& time) const {
		return scalar_att_(this, p, t, n, u, wlen, time);
	}

	__host__ __device__
	RangeF attenuation(const Point3R& p, const Vectr3R& t, const Norml3R& n, const Point2R& u, const RangeF& wlen, const RangeF& time) const {
		return ranged_att_(this, p, t, n, u, wlen, time);
	}

	__host__ __device__
	float  extinction(const Point3F& p, const Vectr3F& t, const Norml3F& n, const Point2F& u, const float& wlen, const float& time) const {
		return scalar_ext_(this, p, t, n, u, wlen, time);
	}

	__host__ __device__
	RangeF  extinction(const Point3R& p, const Vectr3R& t, const Norml3R& n, const Point2R& u, const RangeF& wlen, const RangeF& time) const {
		return ranged_ext_(this, p, t, n, u, wlen, time);
	}
};

/******************************************************************************
 * Attenuation Node Implementation
 *****************************************************************************/
class AttenuationNode: public OpacityNode {
private:
	const Pointer<const ShadingNode<float>> value;

	__host__ __device__
	static float scalar_att_(const OpacityNode* node, const Point3F& p, const Vectr3F& t, const Norml3F& n, const Point2F& u, const float& wlen, const float& time) {
		return (*static_cast<const AttenuationNode*>(node)->value)(p, t, n, u, wlen, time);
	}

	__host__ __device__
	static RangeF ranged_att_(const OpacityNode* node, const Point3R& p, const Vectr3R& t, const Norml3R& n, const Point2R& u, const RangeF& wlen, const RangeF& time) {
		return (*static_cast<const AttenuationNode*>(node)->value)(p, t, n, u, wlen, time);
	}

	__host__ __device__
	static float scalar_ext_(const OpacityNode* node, const Point3F& p, const Vectr3F& t, const Norml3F& n, const Point2F& u, const float& wlen, const float& time) {
		return (*static_cast<const AttenuationNode*>(node)->value)(p, t, n, u, wlen, time) * wlen / (4.f * PI<float>());
	}

	__host__ __device__
	static RangeF ranged_ext_(const OpacityNode* node, const Point3R& p, const Vectr3R& t, const Norml3R& n, const Point2R& u, const RangeF& wlen, const RangeF& time) {
		return (*static_cast<const AttenuationNode*>(node)->value)(p, t, n, u, wlen, time) * wlen / (4.f * PI<float>());
	}

	__host__
	AttenuationNode(
		const Pointer<const ShadingNode<float>>& value,
		const scalar_eval_t& scalar_att_,
		const ranged_eval_t& ranged_att_,
		const scalar_eval_t& scalar_ext_,
		const ranged_eval_t& ranged_ext_
	): OpacityNode(scalar_att_, ranged_att_, scalar_ext_, ranged_ext_), value (value) {
	}

public:
	friend class TypedPool<AttenuationNode>;

	__host__
	static Pointer<AttenuationNode> build(MemoryPool& pool, const Pointer<const ShadingNode<float>>& value) {
		return Pointer<AttenuationNode>::make(
			pool, value,
			scalar_eval_t(virtualize(scalar_att_)),
			ranged_eval_t(virtualize(ranged_att_)),
			scalar_eval_t(virtualize(scalar_ext_)),
			ranged_eval_t(virtualize(ranged_ext_))
		);
	}
};

/******************************************************************************
 * Extinction Node Implementation
 *****************************************************************************/
class ExtinctionNode: public OpacityNode {
private:
	const Pointer<const ShadingNode<float>> value;

	__host__ __device__
	static float scalar_att_(const OpacityNode* node, const Point3F& p, const Vectr3F& t, const Norml3F& n, const Point2F& u, const float& wlen, const float& time) {
		return (*static_cast<const ExtinctionNode*>(node)->value)(p, t, n, u, wlen, time) * 4.f * PI<float>() / wlen;
	}

	__host__ __device__
	static RangeF ranged_att_(const OpacityNode* node, const Point3R& p, const Vectr3R& t, const Norml3R& n, const Point2R& u, const RangeF& wlen, const RangeF& time) {
		return (*static_cast<const ExtinctionNode*>(node)->value)(p, t, n, u, wlen, time) * 4.f * PI<float>() / wlen;
	}

	__host__ __device__
	static float scalar_ext_(const OpacityNode* node, const Point3F& p, const Vectr3F& t, const Norml3F& n, const Point2F& u, const float& wlen, const float& time) {
		return (*static_cast<const ExtinctionNode*>(node)->value)(p, t, n, u, wlen, time);
	}

	__host__ __device__
	static RangeF ranged_ext_(const OpacityNode* node, const Point3R& p, const Vectr3R& t, const Norml3R& n, const Point2R& u, const RangeF& wlen, const RangeF& time) {
		return (*static_cast<const ExtinctionNode*>(node)->value)(p, t, n, u, wlen, time);
	}

	__host__
	ExtinctionNode(
		const Pointer<const ShadingNode<float>>& value,
		const scalar_eval_t& scalar_att_,
		const ranged_eval_t& ranged_att_,
		const scalar_eval_t& scalar_ext_,
		const ranged_eval_t& ranged_ext_
	): OpacityNode(scalar_att_, ranged_att_, scalar_ext_, ranged_ext_), value (value) {
	}

public:
	friend class TypedPool<ExtinctionNode>;

	__host__
	static Pointer<ExtinctionNode> build(MemoryPool& pool, const Pointer<const ShadingNode<float>>& value) {
		return Pointer<ExtinctionNode>::make(
			pool, value,
			scalar_eval_t(virtualize(scalar_att_)),
			ranged_eval_t(virtualize(ranged_att_)),
			scalar_eval_t(virtualize(scalar_ext_)),
			ranged_eval_t(virtualize(ranged_ext_))
		);
	}
};

}

#endif /* OPACITYNODE_CUH_ */

#ifndef HOMOGENEOUSMEDIUM_CUH_
#define HOMOGENEOUSMEDIUM_CUH_

#include "Medium.cuh"
#include "OpacityNode.cuh"
#include "PhaseFunction.cuh"
#include "lucifer/random/ExponentialDistribution.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"


namespace lucifer {

/*!
 * \brief
 * A light-interacting medium that has spatially constant attributes.
 *
 * \author
 * Bruno Pera.
 */
class HomogeneousMedium: public Medium {
private:
	const Pointer<const OpacityNode> opacity_;
	const Pointer<const ShadingNode<float>> scattering_;
	const Pointer<const ShadingNode<float>> refractiveIndex_;
	const Pointer<const PhaseFunction> phaseFunction_;

	__host__ __device__
	static constexpr Vectr3F T() {
		return Vectr3F(0.f, 0.f, 0.f);
	}

	__host__ __device__
	static constexpr Norml3F N() {
		return Norml3F(0.f, 0.f, 0.f);
	}

	__host__ __device__
	static constexpr Point2F U() {
		return Point2F(0.f, 0.f);
	}

	__host__ __device__
	static const HomogeneousMedium* cast(const Medium* medium) {
		return static_cast<const HomogeneousMedium*>(medium);
	}

	__host__ __device__
	static ComplexF refract_index_(const Medium* medium, const Point3F& p, const Point2F& uv, const float wlen, const float time) {
		return ComplexF((*cast(medium)->refractiveIndex_)(p, T(), N(), uv, wlen, time), cast(medium)->opacity_->extinction(p, T(), N(), uv, wlen, time));
	}

	__host__ __device__
	static float transmittance_(const Medium* medium, const Ray& ray, RNG* rng1D) {
		const float sigma_t = max(
			cast(medium)->opacity_->attenuation(ray.o, T(), N(), U(), ray.l, ray.t) + (*cast(medium)->scattering_)(ray.o, T(), N(), U(), ray.l, ray.t),
			0.f
		);
		if (sigma_t <= 0.f) return 1.f;
		if (isinf(ray.r.length())) return 0.f;
		return exp(-sigma_t * ray.r.length() * ray.d.length());
	}

	__host__ __device__
	static bool sample_medium_(const Medium* medium, const Ray& ray, RNG* rng1D, Point3F* p, float* beta) {
		// scattering and attenuation coefficients
		const float sigma_s = max((*cast(medium)->scattering_)(ray.o, T(), N(), U(), ray.l, ray.t), 0.f);
		const float sigma_t = sigma_s + max(cast(medium)->opacity_->attenuation(ray.o, T(), N(), U(), ray.l, ray.t), 0.f);
		// normalize ray
		const float rlen = ray.d.length();
		const float dmax = ray.r.length() * rlen;
		if (sigma_s <= 0.f) {
			*beta = isinf(dmax) ? (sigma_t <= 0.f ? 1.f : 0.f) : exp(-sigma_t * dmax);
			return false;
		}
		// sample distance along ray
		float rnd;
		rng1D->next(&rnd);
		const float dist = ExponentialDistribution::sample(sigma_s, rnd);
		if (dist < dmax) {
			// sampled inside medium
			*p = ray.o + ray.d * ((ray.r.lower + dist) / rlen);
			*beta = sigma_s * exp(-sigma_t * dist) / ExponentialDistribution::pdf(sigma_s, dist);
			return true;
		}
		// sampled ouside medium
		*beta = exp(-sigma_t * dmax) / (1.f - ExponentialDistribution::cdf(sigma_s, dmax));
		return false;
	}

	__host__ __device__
	static float evaluatephase_(const Medium* medium, const Point3F& p, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo) {
		return (*cast(medium)->phaseFunction_)(p, wlen, time, wi, wo);
	}

	__host__ __device__
	static float sample_wi_vec_(const Medium* medium, const Point3F& p, const float wlen, const float time, const Vectr3F& wo, float rnd[2], Vectr3F* wi, float* pdf) {
		return cast(medium)->phaseFunction_->sampleWi(p, wlen, time, wo, rnd, wi, pdf);
	}

	__host__ __device__
	static float sample_wi_pdf_(const Medium* medium, const Point3F& p, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi) {
		return cast(medium)->phaseFunction_->sampleWiPdf(p, wlen, time, wo, wi);
	}

	__host__
	HomogeneousMedium(
		const Pointer<const OpacityNode>& attenuation,
		const Pointer<const ShadingNode<float>>& scattering,
		const Pointer<const ShadingNode<float>>& refractiveIndex,
		const Pointer<const PhaseFunction>& phaseFunction,
		const bool volumetric,
		const uint8_t priority,
		const refract_index_t& refract_index_,
		const transmittance_t& transmittance_,
		const sample_medium_t& sample_medium_,
		const evaluatephase_t& evaluatephase_,
		const sample_wi_vec_t& sample_wi_vec_,
		const sample_wi_pdf_t& sample_wi_pdf_
	):
		Medium(
				refract_index_,
				transmittance_,
				sample_medium_,
				evaluatephase_,
				sample_wi_vec_,
				sample_wi_pdf_,
				priority,
				volumetric
		),
		opacity_(attenuation),
		scattering_(scattering),
		refractiveIndex_(refractiveIndex),
		phaseFunction_(phaseFunction) {
	}

public:
	friend class TypedPool<HomogeneousMedium>;

	/*!
	 * \brief
	 * Creates a new HomogeneousMedium.
	 *
	 * \param[in] attenuation The spectral attenuation coefficient.
	 * \param[in] scattering The spectral scattering coefficient.
	 * \param[in] refractiveIndex The spectral refractive index.
	 * \param[in] phaseFunction The PhaseFunction.
	 * \param[in] volumetric If the medium is actually enclosed by a volume.
	 * \param[in] priority The priority used to decide among overlapping media.
	 */
	__host__
	static Pointer<HomogeneousMedium> build(
		MemoryPool& pool,
		const Pointer<const OpacityNode>& attenuation,
		const Pointer<const ShadingNode<float>>& scattering,
		const Pointer<const ShadingNode<float>>& refractiveIndex,
		const Pointer<const PhaseFunction>& phaseFunction,
		const bool volumetric,
		const uint8_t priority) {
		return Pointer<HomogeneousMedium>::make(
			pool, attenuation, scattering, refractiveIndex, phaseFunction, volumetric, priority,
			refract_index_t(virtualize(refract_index_)),
			transmittance_t(virtualize(transmittance_)),
			sample_medium_t(virtualize(sample_medium_)),
			evaluatephase_t(virtualize(evaluatephase_)),
			sample_wi_vec_t(virtualize(sample_wi_vec_)),
			sample_wi_pdf_t(virtualize(sample_wi_pdf_))
		);
	}
};

}

#endif /* HOMOGENEOUSMEDIUM_CUH_ */

/*
 * MixedPhaseFunction.cuh
 *
 *  Created on: 20/08/2017
 *      Author: pera
 */

#ifndef MIXEDPHASEFUNCTION_CUH_
#define MIXEDPHASEFUNCTION_CUH_

#include <cstdint>
#include "PhaseFunction.cuh"
#include "lucifer/system/Environment.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * A convex combination of phase functions.
 *
 * \author
 * Bruno Pera.
 */
class MixedPhaseFunction: public PhaseFunction {
public:
	using index_t = uint8_t;
	using array_f = Array<float, index_t>;
	using array_p = Array<Pointer<const PhaseFunction>, index_t>;

private:
	array_f wgt;
	array_f acc;
	array_p phase;

	__host__ __device__
	static float evl_(const PhaseFunction* pf, const Point3F& p, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo) {
		auto this_ = static_cast<const MixedPhaseFunction*>(pf);
		float val = 0.f;
		for (index_t i = 0; i < this_->phase.size(); i++) {
			val += this_->wgt[i] * (*this_->phase[i])(p, wlen, time, wi, wo);
		}
		return val;

	}

	__host__ __device__
	static float smp_(const PhaseFunction* pf, const Point3F& p, const float wlen, const float time, const Vectr3F& wo, float rnd[2], Vectr3F* wi, float* pdf) {
		auto this_ = static_cast<const MixedPhaseFunction*>(pf);
		// sample random phase function
		index_t i = min((index_t) (rnd[0] * this_->phase.size()), (index_t)(this_->phase.size() - 1));
		float a = i == 0 ? 0 : this_->acc[i - 1], b = this_->acc[i];
		rnd[0] = clamp<float>((rnd[0] - a) / (b - a), 0.f, 1.f);
		float val = this_->phase[i]->sampleWi(p, wlen, time, wo, rnd, wi, pdf);
		 val *= this_->wgt[i];
		*pdf *= this_->wgt[i];
		// compute value and pdf contribution from other phase functions
		for (index_t j = 0; j < this_->phase.size(); j++) {
			if (j != i) {
				 val += this_->wgt[j] * (*this_->phase[j])(p, wlen, time, *wi, wo);
				*pdf += this_->wgt[j] * this_->phase[j]->sampleWiPdf(p, wlen, time, wo, *wi);
			}
		}
		return val;
	}

	__host__ __device__
	static float pdf_(const PhaseFunction* pf, const Point3F& p, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi) {
		auto this_ = static_cast<const MixedPhaseFunction*>(pf);
		float pdf = 0.f;
		for (index_t i = 0; i < this_->phase.size(); i++) {
			pdf += this_->wgt[i] * this_->phase[i]->sampleWiPdf(p, wlen, time, wi, wo);
		}
		return pdf;
	}

	__host__
	MixedPhaseFunction(MemoryPool& pool, const array_f& w, const array_p& p, const evl_t& evl_, const smp_t& smp_, const pdf_t& pdf_):
	PhaseFunction(evl_, smp_, pdf_), wgt(pool, w.size()), acc(pool, w.size()), phase(pool, p.size()) {
		require(w.size() == p.size());
		float wSum = 0.f;
		index_t n = w.size();
		for (index_t i = 0; i < n; i++) {
			wSum += w[i];
			phase[i] = p[i];
		}
		wgt[0] = acc[0] = w[0] / wSum;
		for (index_t i = 1; i < n; i++) {
			wgt[i] = w[i] / wSum;
			acc[i] += wgt[i];
		}
	}

public:
	friend class TypedPool<MixedPhaseFunction>;

	/*!
	 * \brief
	 * Creates a new MixedPhaseFunction.
	 *
	 * \param[in] w The phase functions' weight.
	 * \param[in] p The phase functions.
	 */
	__host__
	static Pointer<MixedPhaseFunction> build(MemoryPool& pool, const array_f& w, const array_p& p) {
		return Pointer<MixedPhaseFunction>::make(
			pool, pool, w, p,
			evl_t(virtualize(evl_)),
			smp_t(virtualize(smp_)),
			pdf_t(virtualize(pdf_))
		);
	}
};

}

#endif /* MIXEDPHASEFUNCTION_CUH_ */

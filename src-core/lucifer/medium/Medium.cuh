#ifndef MEDIUM_CUH_
#define MEDIUM_CUH_

#include <cstdint>
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/math/geometry/Ray.cuh"
#include "lucifer/random/RNG.cuh"
#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/memory/Virtual.cuh"

namespace lucifer {

/*!
 * \brief
 * Participating Medium
 *
 * Medium is the basic interface used to describe the way light interacts
 * within the volume of a Primitive. It is used to model volumetric effects like
 * fog and clouds and also subsurface scattering in materials like milk, marble
 * and skin, for example.
 *
 * The medium must be able to compute the transmittance along a ray and, for
 * each point within the volume, provide the
 * <a href="https://en.wikipedia.org/wiki/Refractive_index#Complex_refractive_index">
 * complex refractive index</a> and all PhaseFunction related methods.
 *
 * \author
 * Bruno Pera.
 */
class Medium: public Polymorphic {
public:
	using priority_t = uint8_t;
	using refract_index_t = Virtual<ComplexF, const Medium*, const Point3F&, const Point2F&, const float, const float>;
	using transmittance_t = Virtual<float, const Medium*, const Ray&, RNG*>;
	using sample_medium_t = Virtual<bool, const Medium*, const Ray&, RNG*, Point3F*, float*>;
	using evaluatephase_t = Virtual<float, const Medium*, const Point3F&, const float, const float, const Vectr3F&, const Vectr3F&>;
	using sample_wi_vec_t = Virtual<float, const Medium*, const Point3F&, const float, const float, const Vectr3F&, float[2], Vectr3F*, float*>;
	using sample_wi_pdf_t = Virtual<float, const Medium*, const Point3F&, const float, const float, const Vectr3F&, const Vectr3F&>;

private:
	const refract_index_t refract_index_;
	const transmittance_t transmittance_;
	const sample_medium_t sample_medium_;
	const evaluatephase_t evaluatephase_;
	const sample_wi_vec_t sample_wi_vec_;
	const sample_wi_pdf_t sample_wi_pdf_;

protected:
	const priority_t priority_;
	const bool volumetric_;

	__host__
	Medium(
		const refract_index_t& refract_index_,
		const transmittance_t& transmittance_,
		const sample_medium_t& sample_medium_,
		const evaluatephase_t& evaluatephase_,
		const sample_wi_vec_t& sample_wi_vec_,
		const sample_wi_pdf_t& sample_wi_pdf_,
		const priority_t priority,
		const bool volumetric

	):
		refract_index_(refract_index_),
		transmittance_(transmittance_),
		sample_medium_(sample_medium_),
		evaluatephase_(evaluatephase_),
		sample_wi_vec_(sample_wi_vec_),
		sample_wi_pdf_(sample_wi_pdf_),
		priority_(priority),
		volumetric_(volumetric) {
	}

public:
	/*!
	 * \brief
	 * Returns the priority of this medium.
	 *
	 * The priority is used to decide wich medium should be considered when
	 * traversing a volume with different overlapping media. The details can be
	 * found in "Simple Nested Dielectrics in Ray Traced Images" by Charles M.
	 * Schmidt and Brian Budge.
	 *
	 * \return The priority.
	 */
	__host__ __device__
	priority_t priority() const {
		return priority_;
	}

	/*!
	 * \brief rng1D
	 * Returns if this medium is actually enclosed by a volume.
	 *
	 * Some BSDF depends on the complex refractive index defined by the
	 * material's medium, for this reason when using these BSDFs it is
	 * necessary to specify a medium. When the corresponding primitive's shape
	 * is not actually volumetric (like a triangle or any open surface) the
	 * medium should be tagged as "non-volumetric" so that the rays are
	 * never considered to be inside the medium.
	 *
	 * \return If this medium is actually enclosed by a volume.
	 */
	__host__ __device__
	bool isVolumetric() const {
		return volumetric_;
	}

	/*!
	 * \brief
	 * Computes the complex refractive index at a position within the medium.
	 *
	 * \param[in] p The position in material's coordinate.
	 * \param[in] uv The uv coordinates (when on the surface).
	 * \param[in] wlen The light wavelength in meters. p
	 * \param[in] time The normalized time in [0, 1].
	 *
	 * \return The complex refractive index.
	 */
	__host__ __device__
	ComplexF refractiveIndex(const Point3F& p, const Point2F& uv, const float wlen, const float time) const {
		return refract_index_(this, p, uv, wlen, time);
	}

	/*!
	 * \brief
	 * Estimates the transmittance along the ray's extent.
	 *
	 * \param[in] ray The ray in material's coordinates.
	 * \param[in] rng1D One dimensional uniform random number generator.
	 *
	 * \return The fraction of light that will be transmitted.
	 */
	__host__ __device__
	float transmittance(const Ray& ray, RNG* rng1D) const {
		return transmittance_(this, ray, rng1D);
	}

	/*!
	 * \brief
	 * Randomly samples a interaction position within then medium along
	 * the ray's extent.
	 *
	 * \param[in] Ray The ray in material's coordinates.
	 * \param[in] rng1D One dimensional uniform random number generator.
	 * \param[out] p The sampled position within the medium.
	 * \param[out] beta The transmittance from the ray's origin to the sampled
	 * position divided by the sampling pdf.
	 *
	 * \return If a point was sampled within the medium. Returning false means
	 * that an interaction did not occur inside the medium a pnd the position `p`
	 * is undefined.
	 */
	__host__ __device__
	bool sample(const Ray& ray, RNG* rng1D, Point3F* p, float* beta) const {
		return sample_medium_(this, ray, rng1D, p, beta);
	}

	/*!
	 * \brief
	 * Evaluates the phase function.
	 *
	 * \param[in] p The position within the medium in material's coordinates.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wi The normalized light incoming direction.
	 * \param[in] wo The normalized light outgoing direction.
	 *
	 * \return The value of the phase function.
	 */
	__host__ __device__
	float evaluatePhase(const Point3F& p, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo) const {
		return evaluatephase_(this, p, wlen, time, wi, wo);
	}

	/*!
	 * \brief
	 * Samples an incoming light direction from the phase function.
	 *
	 * \param[in] p The position within the medium.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wo The normalized light outgoing direction.
	 * \param[in] rnd A pair of uniformly sampled numbers in [0, 1].
	 * \param[out] wi The sampled normalized light incoming direction.
	 * \param[out] pdf The PDF of sampling `wi` with respect to the solid
	 * angle measure.
	 *
	 * \return The phase function value at the sampled direction.
	 */
	__host__ __device__
	float sampleWi(const Point3F& p, const float wlen, const float time, const Vectr3F& wo, float rnd[2], Vectr3F* wi, float* pdf) const {
		return sample_wi_vec_(this, p, wlen, time, wo, rnd, wi, pdf);
	}

	/*!
	 * \brief
	 * Computes the PDF of sampling an incoming direction from the phase function.
	 * p
	 * \param[in] p The position within the medium in material's coordinates.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wo The normalized light outgoing direction.
	 * \param[in] wi The normalized light incoming direction.
	 *
	 * \return The PDF with respect to the solid angle measure.
	 */
	__host__ __device__
	float sampleWiPDF(const Point3F& p, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi) const {
		return sample_wi_pdf_(this, p, wlen, time, wo, wi);
	}
};

}

#endif /* MEDIUM_CUH_ */

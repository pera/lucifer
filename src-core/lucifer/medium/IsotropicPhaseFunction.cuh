#ifndef ISOTROPICPHASEFUNCTION_CUH_
#define ISOTROPICPHASEFUNCTION_CUH_

#include "PhaseFunction.cuh"
#include "lucifer/random/UniformSphereDistribution.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * Isotropic Phase Function
 *
 * A phase function that scatters light uniformly in all directions.
 *
 * \author
 */
class IsotropicPhaseFunction: public PhaseFunction {
private:
	static constexpr float VALUE = 1.f / (4.f * PI<float>());

	__host__ __device__
	static float evl_(const PhaseFunction* phase, const Point3F& p, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo) {
		return VALUE;
	}

	__host__ __device__
	static float smp_(const PhaseFunction* phase, const Point3F& p, const float wlen, const float time, const Vectr3F& wo, float rnd[2], Vectr3F* wi, float* pdf) {
		UniformSphereDistribution::sample(rnd, &((*wi)[0]), pdf);
		return VALUE;
	}

	__host__ __device__
	static float pdf_(const PhaseFunction* phase, const Point3F& p, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi) {
		return UniformSphereDistribution::PDF;
	}

	__host__
	IsotropicPhaseFunction(const evl_t& evl_, const smp_t& smp_, const pdf_t& pdf_):
	PhaseFunction(evl_, smp_, pdf_) {
	}

public:
	friend class TypedPool<IsotropicPhaseFunction>;

	__host__
	static Pointer<IsotropicPhaseFunction> build(MemoryPool& pool) {
		return Pointer<IsotropicPhaseFunction>::make(
			pool,
			evl_t(virtualize(evl_)),
			smp_t(virtualize(smp_)),
			pdf_t(virtualize(pdf_))
		);
	}
};

}

#endif /* ISOTROPICPHASEFUNCTION_CUH_ */

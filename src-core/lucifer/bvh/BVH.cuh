#ifndef BVH_CUH_
#define BVH_CUH_

#include <cassert>
#include <cstdint>
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/Box.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/math/geometry/GroupMode.cuh"
#include "lucifer/math/geometry/AnimatedTransformation.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Polymorphic.cuh"

namespace lucifer {

class BVHBucket {
public:
	static constexpr int N_BUCKETS = 16;

	__host__
	static uint8_t index(float pos, float lower, float length) {
		return clamp<uint8_t>((uint8_t) ((N_BUCKETS * (pos - lower)) / length), 0, N_BUCKETS - 1);
	}

public:
	Box boundary;
	float cost;

public:
	__host__
	BVHBucket(): cost(0.f), boundary() {
	}
};

template<typename E> class BVHElement {
public:
	Pointer<E> element;
	float cost;
	Box boundary;
	Point3F centroid; 
	uint8_t bucket;	

public:
	__host__
	static size_t partition(Array<BVHElement>& elements, size_t min, size_t max, uint8_t split) {
		int64_t l = min - 1;
		int64_t r = max + 1;
		while (true) {
			while ((l < r) && elements[++l].bucket <= split) { }
			while ((r > l) && elements[--r].bucket >  split) { }
			if (l >= r) { break; }
			interchange(elements[l], elements[r]);
		}
		return l;
	}

public:
	__host__
	BVHElement():
	element{}, cost{0.f}, bucket{0} {
	}
	__host__
	BVHElement(const Pointer<E>& element):
	element{element}, cost{element->cost()}, boundary{element->boundary(RangeF(0.f, 1.f))}, centroid{boundary.centroid()}, bucket{0} {
	}
};

template<typename T, typename E = T> class BVHElementBuilder {
public:
	__host__
	virtual Pointer<T> build(MemoryPool& pool, const Array<BVHElement<E>>& elements, size_t min, size_t max) const = 0;

	__host__
	virtual ~BVHElementBuilder() {
	}
};


template<typename T> class BVHNode: public Polymorphic {
private:
	Box boundary_;
	uint8_t axis_;
	Pointer<T> element_;
	Pointer<BVHNode> child0_;
	Pointer<BVHNode> child1_;
	float area_;

private:
	static constexpr uint8_t MAX_DEPTH = 63;

	__host__
	static Box boundaryOf(const Pointer<BVHNode>& a, const Pointer<BVHNode>& b) {
		assert(!a.isNull());
		assert(!b.isNull());
		return a->boundary() + b->boundary();
	}

	__host__
	static float areaOf(const Pointer<BVHNode>& a, const Pointer<BVHNode>& b) {
		assert(!a.isNull());
		assert(!b.isNull());
		const float areaA = a->area();
		const float areaB = b->area();
		return (areaA < 0.f or areaB < 0.f) ? -1.f : areaA + areaB;
	}

private:
	__host__
	float innerCost() const {
		if (isLeaf()) return element_->cost();
		return 2.f + (child0_->innerCost() * child0_->boundary_.area() + child1_->innerCost() * child1_->boundary_.area()) / boundary_.area();
	}

	__host__ __device__
	const BVHNode* nearChild(const Ray& ray) const {
		assert(!isLeaf());
		return ray.d[axis_] >= 0.f ? child0_.get(): child1_.get();
	}

	__host__ __device__
	const BVHNode*  farChild(const Ray& ray) const {
		assert(!isLeaf());
		return ray.d[axis_] >= 0.f ? child1_.get(): child0_.get();
	}

	__host__ __device__
	bool overlaps(const Ray& ray) const {
		return boundary_.intersection(ray);
	}

	template<typename E = T> __host__
	static Pointer<BVHNode> build(MemoryPool& pool, Array<BVHElement<const E>>& elements, size_t min, size_t max, const BVHElementBuilder<const T, const E>& builder, uint8_t depth) {
		int64_t n = max - min;
		if (n <= 0) {
			return Pointer<BVHNode>();
		}
		if (n == 1) {
			return Pointer<BVHNode>::make(pool, pool, builder.build(pool, elements, min, max));
		}
		// compute boundary and centroid extent
		Box centroid;
		Box boundary;
		for (auto e = min; e < max; e++) {
			centroid += elements[e].centroid;
			boundary += elements[e].boundary;
		}
		// find leaf cost;
		float leafCost = 0.f;
		for (auto e = min; e < max; e++) leafCost += elements[e].cost;
		// find minimum cost axis and split position
		uint8_t axis, split;
		float splitCost = +INFINITY;
		for (uint8_t a = 0; a < 3; a++) {
			// initialize buckets
			BVHBucket buckets[BVHBucket::N_BUCKETS];
			for (size_t e = min; e < max; e++) {
				elements[e].bucket = BVHBucket::index(elements[e].centroid[a], centroid.lower()[a], centroid.length(a));
				buckets[elements[e].bucket].cost += elements[e].cost;
				buckets[elements[e].bucket].boundary += elements[e].boundary;
			}
			// find minimum SAH cost split position
			for (uint8_t s = 0; s < (BVHBucket::N_BUCKETS - 1); s++) {
				Box b0;
				float c0 = 0;
				for (uint8_t b = 0; b <= s; b++) {
					b0 += buckets[b].boundary;
					c0 += buckets[b].cost;
				}
				Box b1;
				float c1 = 0;
				for (uint8_t b = s + 1; b < BVHBucket::N_BUCKETS; b++) {
					b1 += buckets[b].boundary;
					c1 += buckets[b].cost;
				}
				float c = 1.f + ((c0 * b0.area() + c1 * b1.area()) / boundary.area());
				if (c < splitCost) {
					splitCost = c;
					split = s;
					axis = a;
				}
			}
		}
		if (leafCost <= splitCost || depth == MAX_DEPTH) {
			return Pointer<BVHNode<T>>::make(pool, pool, builder.build(pool, elements, min, max));
		}
		// partition elements at minimum cost split position
		for (size_t e = min; e < max; e++) {
			elements[e].bucket = BVHBucket::index(elements[e].centroid[axis], centroid.lower()[axis], centroid.length(axis));
		}
		size_t mid = BVHElement<const E>::partition(elements, min, max - 1, split);
		if ((mid <= min) || (mid >= max)) {
			return Pointer<BVHNode>::make(pool, pool, builder.build(pool, elements, min, max));
		}
		Pointer<BVHNode> child0 = build(pool, elements, min, mid, builder, depth + 1);
		Pointer<BVHNode> child1 = build(pool, elements, mid, max, builder, depth + 1);
		// done
		return Pointer<BVHNode>::make(pool, pool, child0, child1, axis);
	}

public:
	__host__
	explicit BVHNode(MemoryPool& pool, const Pointer<const T>& element):
	element_(element), boundary_(element->boundary(RangeF(0.f, 1.f))), axis_(0), child0_(), child1_(), area_(element->area()){
	}

	__host__
	explicit BVHNode(MemoryPool& pool, const Pointer<BVHNode<const T>>& child0, const Pointer<BVHNode<const T>>& child1, uint8_t axis):
	element_(), boundary_(boundaryOf(child0, child1)), axis_(axis), child0_(child0), child1_(child1), area_(areaOf(child0, child1)) {
	}

	__host__ __device__
	bool isLeaf() const {
		return !element_.isNull();
	}

	__host__ __device__
	Pointer<T>& element() {
		return element_;
	}

	__host__ __device__
	const Pointer<const T>& element() const {
		return element_;
	}

	__host__
	float cost() const {
		return 1.f + innerCost();
	}

	__host__ __device__
	float area() const {
		return area_;
	}

	__host__ __device__
	float area(const float time) const {
		return area_ >= 0.f ? area_ : (isLeaf() ? element_->area(time) : child0_->area(time) + child1_->area(time));
	}

	__host__
	Box boundary() const {
		return boundary_;
	}

	__host__
	Box boundary(const RangeF& time) const {
		return isLeaf() ? element_->boundary(time) : child0_->boundary(time) + child1_->boundary(time);
	}

	__host__ __device__
	const BVHNode* child0() const {
		return child0_.get();
	}

	__host__ __device__
	const BVHNode* child1() const {
		return child1_.get();
	}

	__host__ __device__
	BVHNode* child0() {
		return child0_.get();
	}

	__host__ __device__
	BVHNode* child1() {
		return child1_.get();
	}

	__host__
	Pointer<BVHNode> transform(MemoryPool& pool, const Transformation3F& transformation) const {
		if (isLeaf()) {
			return Pointer<BVHNode>::make(pool, pool, element_->transform(pool, transformation));
		}
		return Pointer<BVHNode>::make(pool, pool, child0_->transform(pool, transformation), child1_->transform(pool, transformation), axis_);
	}

	__host__
	Pointer<BVHNode> transform(MemoryPool& pool, const Pointer<const AnimatedTransformation>& transformation) const {
		if (isLeaf()) {
			return Pointer<BVHNode>::make(pool, pool, element_->transform(pool, transformation));
		}
		return Pointer<BVHNode>::make(pool, pool, child0_->transform(pool, transformation), child1_->transform(pool, transformation), axis_);
	}

	__host__
	uint16_t depth() const {
		if (isLeaf()) {
			return 0;
		}
		return max(child0_->depth(), child1_->depth()) + 1;
	}

	__host__
	uint32_t leaves() const {
		if (isLeaf()) {
			return 1;
		}
		return child0_->leaves() + child1_->leaves();
	}

	template<typename I> __host__ __device__
	bool intersection(Ray& ray, I* isect) const {
		using NodePtr = const BVHNode*;
		// test root node
		if (!isLeaf() and !overlaps(ray)) {
			return false;
		}
		// initialize stack
		NodePtr stack[MAX_DEPTH + 1];
		NodePtr* stackPtr = stack;
		*stackPtr++ = nullptr;
		// while stack not empty
		bool found = false;
		for (NodePtr node = this; node != nullptr;) {
			if (node->isLeaf()) {
				found |= node->element_->intersection(ray, isect);
				node = *--stackPtr;
			}
			else {
				NodePtr childL = node->nearChild(ray);
				NodePtr childR = node-> farChild(ray);
				bool traverseL = childL->overlaps(ray);
				bool traverseR = childR->overlaps(ray);
				if (!traverseL && !traverseR) {
					node = *--stackPtr;
				} else {
					node = (traverseL) ? childL : childR;
					if (traverseL && traverseR) {
						*stackPtr++ = childR;
					}
				}
			}
		}
		return found;
	}

	template<typename index_t, typename E = T> __host__
	static Pointer<BVHNode> build(MemoryPool& pool, const Array<Pointer<const E>, index_t>& elements, const BVHElementBuilder<const T, const E>&& builder) {
		Array<BVHElement<const E>> bvhElements(pool, elements.size());
		for (size_t i = 0; i < elements.size(); i++) {
			BVHElement<const E> e(elements[i]);
			bvhElements[i] = e;
		}
		Pointer<BVHNode> result = build(pool, bvhElements, 0, elements.size(),  builder, 0);
#ifdef __LUCIFER_DEVELOPMENT__
		printf("bvh leaves: %u\tdepth: %u\tcost: %f\n", result->leaves(), result->depth(), result->cost());
#endif
		return result;
	}
};

}

#endif /* BVH_CUH_ */

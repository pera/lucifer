#ifndef STBVH_CUH_
#define STBVH_CUH_

#include <cassert>
#include <cstdint>
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/Box.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/memory/Polymorphic.cuh"

namespace lucifer {

class STBox {
public:
	RangeF time;
	Box box;

public:
	STBox(const RangeF& time, const Box& box):
	time(time), box(box) {
	}
};

template<typename E> class STBVHElement {
public:
	Pointer<E> element;
	float cost;
	Box boundary;
	Point3F centroid;
	uint8_t bucket;

public:
	__host__
	static size_t partition(Array<STBVHElement<E>>& elements, size_t min, size_t max, uint8_t split) {
		int64_t l = min - 1;
		int64_t r = max + 1;
		while (true) {
			while ((l < r) && elements[++l].bucket <= split) { }
			while ((r > l) && elements[--r].bucket >  split) { }
			if (l >= r) { break; }
			interchange(elements[l], elements[r]);
		}
		return l;
	}

public:
	__host__
	STBVHElement():
	element(Pointer<E>()), cost(0.f), bucket(0) {
	}

	__host__
	STBVHElement(const Pointer<E>& element):
	element(element), cost(element->cost()), bucket(0) {
	}

	__host__
	void update(const RangeF& time) {
		boundary = element->boundary(time);
		centroid = boundary.centroid();
	}

	__host__
	Box boundaryAtTime(const RangeF& time) const {
		return element->boundary(time);
	}
};

class STBVHBucket {
public:
	static constexpr int S_BUCKETS = 16;

public:
	__host__
	static uint8_t index(float pos, float lower, float length) {
		return clamp<uint8_t>((uint8_t) ((S_BUCKETS * (pos - lower)) / length), 0, S_BUCKETS - 1);
	}

public:
	Box boundary;
	float cost;

public:
	__host__
	STBVHBucket(): cost(0.f), boundary() {
	}
};

template<typename T, typename E = T> class STBVHElementBuilder {
public:
	__host__
	virtual Pointer<T> build(MemoryPool& pool, const Array<STBVHElement<E>>& elements, size_t min, size_t max) const = 0;

	__host__
	virtual ~STBVHElementBuilder() {
	}
};

template<typename T> class STBVHNode: public Polymorphic {
private:
	static constexpr uint8_t TIME_AXIS  = 3;
	static constexpr uint8_t TIME_DEPTH = 4;
	static constexpr uint8_t TIME_STEPS = 8;
	static constexpr uint8_t MAX_DEPTH = 63;

private:
	RangeF time_;
	Box boundary_;
	uint8_t axis_;
	Pointer<T> element_;
	Pointer<STBVHNode> child0_;
	Pointer<STBVHNode> child1_;
	float area_;

private:
	__host__
	static RangeF timeOf(const Pointer<STBVHNode>& a, const Pointer<STBVHNode>& b) {
		assert(!a.isNull());
		assert(!b.isNull());
		return hull(a->time_, b->time_);
	}

	__host__
	static Box boundaryOf(const Pointer<STBVHNode>& a, const Pointer<STBVHNode>& b) {
		assert(!a.isNull());
		assert(!b.isNull());
		return a->boundary_ + b->boundary_;
	}

	__host__
	static float areaOf(const Pointer<STBVHNode>& a, const Pointer<STBVHNode>& b) {
		assert(!a.isNull());
		assert(!b.isNull());
		return (a->area_ < 0.f or b->area_ < 0.f) ? -1.f : a->area_ + b->area_;
	}

private:
	__host__
	float innerCost() const {
		if (isLeaf()) return element_->cost();
		if (axis_ == TIME_AXIS) {
			return 2.f + (child0_->innerCost() * child0_->time_.length() + child1_->innerCost() * child1_->time_.length()) / time_.length();
		}
		return 2.f + (child0_->innerCost() * child0_->boundary_.area() + child1_->innerCost() * child1_->boundary_.area()) / boundary_.area();
	}

	__host__ __device__
	const STBVHNode* nearChild(const Ray& ray) const {
		assert(!isLeaf());
		return axis_ == TIME_AXIS or ray.d[axis_] >= 0.f ? child0_.get(): child1_.get();
	}

	__host__ __device__
	const STBVHNode*  farChild(const Ray& ray) const {
		assert(!isLeaf());
		return axis_ == TIME_AXIS or ray.d[axis_] >= 0.f ? child1_.get(): child0_.get();
	}

	__host__ __device__
	bool overlaps(const Ray& ray) const {
		return time_.contains(ray.t) and boundary_.intersection(ray);
	}

public:
	__host__
	explicit STBVHNode(MemoryPool& pool, const Pointer<T>& element, const RangeF& time):
	time_(time), boundary_(element->boundary(time)), axis_(0), element_(element), child0_(), child1_(), area_(element->area()) {
	}

	explicit STBVHNode(MemoryPool& pool, const Pointer<STBVHNode>& c0, const Pointer<STBVHNode>& c1, const uint8_t axis):
	time_(timeOf(c0, c1)), boundary_(boundaryOf(c0, c1)), axis_(axis), element_(), child0_(c0), child1_(c1), area_(areaOf(c0, c1)) {
	}

	__host__ __device__
	bool isLeaf() const {
		return !element_.isNull();
	}

	__host__ __device__
	Pointer<T>& element() {
		return element_;
	}

	__host__ __device__
	const Pointer<const T>& element() const {
		return element_;
	}

	__host__
	float cost() const {
		return 1.f + innerCost();
	}

	__host__ __device__
	float area() const {
		return area_;
	}

	__host__ __device__
	float area(const float time) const {
		return area_ >= 0.f ? area_ : (isLeaf() ? element_->area(time) : child0_->area(time) + child1_->area(time));
	}

	__host__
	Box boundary() const {
		return boundary_;
	}

	__host__
	Box boundary(const RangeF& time) const {
		return isLeaf() ? element_->boundary(time) : child0_->boundary(time) + child1_->boundary(time);
	}

	__host__ __device__
	const STBVHNode* child0() const {
		return child0_.get();
	}

	__host__ __device__
	const STBVHNode* child1() const {
		return child1_.get();
	}

	__host__ __device__
	STBVHNode* child0() {
		return child0_;
	}

	__host__ __device__
	STBVHNode* child1() {
		return child1_;
	}

	__host__
	Pointer<STBVHNode> transform(MemoryPool& pool, const Transformation3F& transformation) const {
		if (isLeaf()) {
			return Pointer<STBVHNode>::make(pool, pool, element_->transform(pool, transformation), time_);
		}
		return Pointer<STBVHNode>::make(pool, pool, child0_->transform(pool, transformation), child1_->transform(pool, transformation), axis_);
	}

	__host__
	Pointer<STBVHNode> transform(MemoryPool& pool, const Pointer<const AnimatedTransformation>& transformation) const {
		if (isLeaf()) {
			return Pointer<STBVHNode>::make(pool, pool, element_->transform(pool, transformation), time_);
		}
		return Pointer<STBVHNode>::make(pool, pool, child0_->transform(pool, transformation), child1_->transform(pool, transformation), axis_);
	}

	__host__
	uint16_t depth() const {
		if (isLeaf()) {
			return 0;
		}
		return max(child0_->depth(), child1_->depth()) + 1;
	}

	__host__
	uint32_t leaves() const {
		if (isLeaf()) {
			return 1;
		}
		return child0_->leaves() + child1_->leaves();
	}

	template<typename I> __host__ __device__
	bool intersection(Ray& ray, I* isect) const {
		using NodePtr = const STBVHNode*;
		// test root node
		if (!overlaps(ray)) {
			return false;
		}
		// initialize stack
		NodePtr stack[MAX_DEPTH + 1];
		NodePtr* stackPtr = stack;
		*stackPtr++ = nullptr;
		// while stack not empty
		bool found = false;
		for (NodePtr node = this; node != nullptr;) {
			if (node->isLeaf()) {
				found |= node->element_->intersection(ray, isect);
				node = *--stackPtr;
			}
			else {
				NodePtr childL = node->nearChild(ray);
				NodePtr childR = node-> farChild(ray);
				bool traverseL = childL->overlaps(ray);
				bool traverseR = childR->overlaps(ray);
				if (!traverseL && !traverseR) {
					node = *--stackPtr;
				} else {
					node = (traverseL) ? childL : childR;
					if (traverseL && traverseR) {
						*stackPtr++ = childR;
					}
				}
			}
		}
		return found;
	}

	template<typename E = T>
	static Pointer<STBVHNode> build(MemoryPool& pool, Array<STBVHElement<const E>>& elements, size_t min, size_t max, const RangeF& time, const STBVHElementBuilder<const T, const E>& builder, uint8_t depth, uint8_t tdepth) {
		int64_t n = max - min;
		if (n <= 0) {
			return Pointer<STBVHNode<const T>>();
		}
		if (depth == MAX_DEPTH) {
			return Pointer<STBVHNode>::make(pool, pool, builder.build(pool, elements, min, max), time);
		}
		// compute boundary and centroid extent
		Box centroid;
		Box boundary;
		for (auto e = min; e < max; e++) {
			elements[e].update(time);
			boundary += elements[e].boundary;
			centroid += elements[e].centroid;
		}
		// find leaf cost;
		float leafCost = 0.f;
		for (auto e = min; e < max; e++) leafCost += elements[e].cost;
		uint8_t axis, spatialSplit;
		// find minimum cost spatial axis and split position
		float spatialCost = +INFINITY;
		if (n > 1) {
			for (uint8_t a = 0; a < 3; a++) {
				// initialize buckets
				STBVHBucket buckets[STBVHBucket::S_BUCKETS];
				for (size_t e = min; e < max; e++) {
					elements[e].bucket = STBVHBucket::index(elements[e].centroid[a], centroid.lower()[a], centroid.length(a));
					buckets[elements[e].bucket].cost += elements[e].cost;
					buckets[elements[e].bucket].boundary += elements[e].boundary;
				}
				// find minimum SAH cost split position
				for (uint8_t s = 0; s < (STBVHBucket::S_BUCKETS - 1); s++) {
					Box b0;
					float c0 = 0.f;
					for (uint8_t b = 0; b <= s; b++) {
						b0 += buckets[b].boundary;
						c0 += buckets[b].cost;
					}
					Box b1;
					float c1 = 0.f;
					for (uint8_t b = s + 1; b < STBVHBucket::S_BUCKETS; b++) {
						b1 += buckets[b].boundary;
						c1 += buckets[b].cost;
					}
					float c = 1.f + ((c0 * b0.area() + c1 * b1.area()) / boundary.area());
					if (c < spatialCost) {
						spatialCost = c;
						spatialSplit = s;
						axis = a;
					}
				}
			}
		}
		// find minimum temporal split position
		uint8_t temporalSplit;
		float temporalCost = +INFINITY;
		if (tdepth < TIME_DEPTH) {
			for (int s = 0; s < TIME_STEPS; ++s) {
				float tMid = time.lower + time.length() * (s + 1.f) / (TIME_STEPS + 1.f);
				Box b0, b1;
				for (size_t e = min; e < max; e++) {
					b0 += elements[e].boundaryAtTime(RangeF(time.lower, tMid));
					b1 += elements[e].boundaryAtTime(RangeF(tMid, time.upper));
				}
				float c = 1.f + leafCost * (((tMid - time.lower) * b0.area() + (time.upper - tMid) * b1.area()) / (time.length() * boundary.area()));
				if (c < temporalCost) {
					temporalCost = c;
					temporalSplit = s;
				}
			}
		}
		if (leafCost <= spatialCost and leafCost <= temporalCost) {
			// do not partition, finish.
			return Pointer<STBVHNode>::make(pool, pool, builder.build(pool, elements, min, max), time);
		}
		if (spatialCost <= temporalCost) {
			// partition elements at minimum cost spatial split position
			for (size_t e = min; e < max; e++) {
				elements[e].bucket = STBVHBucket::index(elements[e].centroid[axis], centroid.lower()[axis], centroid.length(axis));
			}
			size_t mid = STBVHElement<const E>::partition(elements, min, max - 1, spatialSplit);
			if ((mid <= min) || (mid >= max)) {
				return Pointer<STBVHNode>::make(pool, pool, builder.build(pool, elements, min, max), time);
			}
			Pointer<STBVHNode> child0 = build(pool, elements, min, mid, time, builder, depth + 1, tdepth);
			Pointer<STBVHNode> child1 = build(pool, elements, mid, max, time, builder, depth + 1, tdepth);
			return Pointer<STBVHNode>::make(pool, pool, child0, child1, axis);
		}
		// partition elements at minimum cost temporal split position
		float tMid = time.lower + time.length() * (temporalSplit + 1.f) / (TIME_STEPS + 1.f);
		Pointer<STBVHNode> child0 = build(pool, elements, min, max, RangeF(time.lower, tMid), builder, depth + 1, tdepth + 1);
		Pointer<STBVHNode> child1 = build(pool, elements, min, max, RangeF(tMid, time.upper), builder, depth + 1, tdepth + 1);
		const auto timeAxis = TIME_AXIS;
		return Pointer<STBVHNode<const T>>::make(pool, pool, child0, child1, timeAxis);
	}

public:
	template<typename index_t, typename E = T> __host__
	static Pointer<STBVHNode<const T>> build(MemoryPool& pool, const Array<Pointer<const E>, index_t>& elements, const STBVHElementBuilder<const T, const E>&& builder) {
		Array<STBVHElement<const E>> bvhElements(pool, elements.size());
		for (size_t i = 0; i < elements.size(); i++) {
			bvhElements[i] = STBVHElement<const E>(elements[i]);
		}
		Pointer<STBVHNode<const T>> result = build(pool, bvhElements, 0, elements.size(),  RangeF(0.f, 1.f), builder, 0, 0);
#ifdef __LUCIFER_DEVELOPMENT__
		printf("bvh leaves: %u\tdepth: %u\tcost: %f\n", result->leaves(), result->depth(), result->cost());
#endif
		return result;
	}
};

}

#endif /* STBVH_CUH_ */

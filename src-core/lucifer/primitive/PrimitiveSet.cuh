#ifndef PRIMITIVESET_CUH_
#define PRIMITIVESET_CUH_

#include <cstdint>
#include "lucifer/math/geometry/AnimatedTransformation.cuh"
#include "lucifer/math/geometry/Box.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/primitive/GeometricPrimitive.cuh"
#include "lucifer/primitive/Primitive.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * A composite primitive.
 *
 * \author
 * Bruno Pera.
 */
class PrimitiveSet: public Primitive {
public:
	using index_t = uint32_t;
	using array_t = Array<Pointer<const GeometricPrimitive>, index_t>;

private:
	array_t primitives_;

	__host__ __device__
	static const PrimitiveSet* cast(const Primitive* this_) {
		return static_cast<const PrimitiveSet*>(this_);
	}

	__host__ __device__
	static bool intersection_(const Primitive* this_, Ray& ray, Intersection* intersection) {
		bool found = false;
		for (auto& p : cast(this_)->primitives_) {
			found |= p->intersection(ray, intersection);
		}
		return found;
	}

	__host__
	PrimitiveSet(const array_t& primitives, const intersection_t& intersection_):
	Primitive(intersection_), primitives_(primitives) {
	}

public:
	friend class TypedPool<PrimitiveSet>;

	/*!
	 * \brief
	 * Creates a new composite primitive.
	 *
	 * \param[in] primitives The set of primitives.
	 */
	__host__
	static Pointer<PrimitiveSet> build(MemoryPool& pool, const array_t& primitives) {
		return Pointer<PrimitiveSet>::make(
			pool, primitives,
			intersection_t(virtualize(intersection_))
		);
	}

	/*!
	 * \brief
	 * Returns the number of elements.
	 *
	 * \return The number of elements.
	 */
	__host__ __device__
	index_t size() const {
		return primitives_.size();
	}

	__host__
	Box boundary(const RangeF& time) const override {
		Box result = primitives_[0]->boundary(time);
		for (int i = 1; i < primitives_.size(); i++) {
			result += primitives_[i]->boundary(time);
		}
		return result;
	}

	__host__
	float cost() const override {
		float c = 0.f;
		for (auto& p : primitives_) c += p->cost();
		return c;
	}

	__host__
	float area() const override {
		float result = 0.f;
		for (auto& p : primitives_) {
			const float temp = p->area();
			if (temp < 0.f) return 1.f;
			result += temp;
		}
		return result;
	}

	__host__
	Pointer<Primitive> transform(MemoryPool& pool, const Transformation3F& transformation) const override {
		array_t array(pool, primitives_.size());
		for (index_t i = 0; i < primitives_.size(); i++) {
			array[i] = primitives_[i]->transform(pool, transformation).dynamicCast<const GeometricPrimitive>();
		}
		Pointer<Primitive> transformed = PrimitiveSet::build(pool, array);
		return transformed;
	}

	__host__
	Pointer<Primitive> transform(MemoryPool& pool, const Pointer<const AnimatedTransformation>& transformation) const override {
		array_t array(pool, primitives_.size());
		for (index_t i = 0; i < primitives_.size(); i++) {
			array[i] = primitives_[i]->transform(pool, transformation).dynamicCast<const GeometricPrimitive>();
		}
		Pointer<Primitive> transformed = PrimitiveSet::build(pool, array);
		return transformed;
	}
};

}

#endif /* PRIMITIVESET_CUH_ */

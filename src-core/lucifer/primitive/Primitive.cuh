#ifndef PRIMITIVE_CUH_
#define PRIMITIVE_CUH_

#include <vector>
#include "lucifer/material/Material.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/shape/Shape.cuh"
#include "lucifer/material/InteriorListUtils.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/memory/Virtual.cuh"

namespace lucifer {

class GeometricPrimitive;

/*!
 * \brief
 * A renderable primitve.
 *
 * \author
 * Bruno Pera
 */
class Primitive: public Polymorphic {
public:
	/*!
	 * \brief
	 * The interaction of a ray with a primitive, can represent an intersection
	 * on the primitive's surface or an interaction within the primitive's
	 * medium.
	 */
	class Intersection: public Shape::Intersection {
	public:
		const GeometricPrimitive* primitive = nullptr;

	private:
		__host__ __device__
		void setIOR(const InteriorList& list, const Vectr3F& wo, ComplexF& ior1, ComplexF& ior2) const;

		__host__ __device__
		void setIOR(const InteriorList& media, ComplexF& ior1, ComplexF& ior2) const;

	public:
		/*!
		 * \brief
		 * Returns `true` if this instance represents an intersection on the
		 * primitive's surface or `false` if represents an interaction within
		 * the primitive's medium.
		 *
		 * \return If the interaction occured on the primitive's surface.
		 */
		__host__ __device__
		bool onSurface() const;

		/*!
		 * \brief
		 * Returns if the interaction occured on a light-emitting surface.
		 *
		 * \return if the interaction occured on a light-emitting surface.
		 */
		__host__ __device__
		bool isEmissive() const;

		/*!
		 * \brief
		 * Returns the directional spectral emission from the interaction.
		 *
		 * \return The directional spectral emission from the interaction.
		 */
		__host__ __device__
		float emission() const;

		/*!
		 * \brief
		 * Returns the material at the interaction point.
		 *
		 * \return The material at the interaction point.
		 */
		__host__ __device__
		const Material* material() const;

		/*!
		 * \brief
		 * Return if the material at the interaction point has a BSDF
		 * that actually interacts with light.
		 *
		 * \return If the material has light-interacting BSDF.
		 */
		__host__ __device__
		bool hasOpticallyInteractingBSDF() const;

		/*!
		 * \brief
		 * Return if the material at the interaction point has a BSDF
		 * that matches the given BxDF type.
		 *
		 * \param[in] type The BxDF type.
		 * \return If the material matches the type.
		 */
		__host__ __device__
		bool matchesBSDF(BxDF::Type type) const;

		/*!
		 * \brief
		 * Evaluates the material's BSDF at the interaction point.
		 *
		 * \param[in] wi The normalized incoming direction.
		 * \param[in] media The interior list of materials.
		 * \param[in] mode The transport mode, RADIANCE or IMPORTANCE.
		 * \param[in] restrictions The restrictions on the BxDF types.
		 *
		 * \return The BSDF value.
		 */
		__host__ __device__
		float evaluateBSDF(const Vectr3F& wi, const InteriorList& media, const BxDF::Mode& mode, RNG* rng, const BxDF::Type restrictions = BxDF::Type(0)) const;

		/*!
		 * \brief
		 * Samples an incoming direction from the material's BSDF.
		 *
		 * \param[in] media The interior list of materials.
		 * \param[in] mode The transport mode.
		 * \param[in] restrictions The restrictions on the BxDF types
		 * \param[in] rnd Two uniformly distributed random numbers in [0, 1].
		 * \param[out] wi The sampled normalized incoming direction.
		 * \param[out] pdf The PDF of sampling the incoming direction.
		 * \param[out] sampled If not null holds the actually sampled BxDF.
		 *
		 * \return The material's BSDF value.
		 */
		__host__ __device__
		float sampleBSDFWi(const InteriorList& media, const BxDF::Mode& mode, const BxDF::Type restrictions, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng, const BxDF** sampled = nullptr) const;

		/*!
		 * \brief
		 * Computes the PDF (with respect to the solid angle measue from the
		 * geometry's normal vector) of sampling the incoming direction `wi`
		 * from the material's BSDF.
		 *
		 * \param[in] wi The normalized incoming direction.
		 * \param[in] media The interior list of materials.
		 * \param[in] mode The transport mode.
		 * \param[in] restrictions The restrictions on the BxDF types.
		 * \param[out] sampled The actually sampled BxDF.
		 *
		 * \return The PDF of sampling the incoming direction from the BSDF.
		 */
		__host__ __device__
		float sampleBSDFWiPDF(const Vectr3F& wi, const InteriorList& media, const BxDF::Mode& mode, RNG* rng, const BxDF::Type restrictions = BxDF::Type(0), const BxDF* sampled = nullptr) const;

		/*!
		 * \brief
		 * Computes the PDF (with respect to the solid angle measue from the
		 * geometry's normal vector) of sampling the incoming direction `wi`
		 * from the material's BSDF.
		 *
		 * \param[in] wo The normalized outgoing direction (overrides the interaction's).
		 * \param[in] wi The normalized incoming direction.
		 * \param[in] media The interior list of materials.
		 * \param[in] mode The transport mode.
		 * \param[in] restrictions The restrictions on the BxDF types.
		 * \param[out] sampled The actually sampled BxDF.
		 *
		 * \return The PDF of sampling the incoming direction from the BSDF.
		 */
		__host__ __device__
		float sampleBSDFWiPDF(const Vectr3F& wo, const Vectr3F& wi, const InteriorList& media, const BxDF::Mode& mode, RNG* rng, const BxDF::Type restrictions = BxDF::Type(0), const BxDF* sampled = nullptr) const;
	};

public:
	// cuda virtual interface
	using intersection_t = Virtual<
		bool, 				// return type
		const Primitive*, 	// this
		Ray&, 				// ray
		Intersection* 		// intersection
	>;

private:
	const intersection_t intersection_;

protected:
	__host__
	Primitive(const intersection_t& intersection_):
	intersection_(intersection_) {
	}

public:
	/*!
	 * \brief
	 * Creates a new transformed primitive.
	 *
	 * \param[in] transformation The transformation.
	 * \return The new transformed primitive.
	 */
	__host__
	virtual Pointer<Primitive> transform(MemoryPool& pool, const Transformation3F& transformation) const = 0;

	/*!
	 * \brief
	 * Creates a new animated primitive.
	 *
	 * \param[in] transformation The animated transformation.
	 * \return The new animated primitive.
	 */
	__host__
	virtual Pointer<Primitive> transform(MemoryPool& pool, const Pointer<const AnimatedTransformation>& transformation) const = 0;

	/*!
	 * \brief
	 * Returns the primitives's bounding box.
	 *
	 * \param[in] time The normalized time range in [0, 1].
	 *
	 * \return A bounding box large enough to enclose the whole primitve over
	 * the entire time range.
	 */
	__host__
	virtual Box boundary(const RangeF& time) const = 0;

	/*!
	 * \brief
	 * Returns the estimated cost of intersecting the primitive with a ray.
	 *
	 * The cost should be estimated as the average computational cost
	 * relatively to the cost of intersecting an axis aligned bounding Box,
	 * whose cost is, by definition 1.
	 * This estimate is used to guide the construction of intersection
	 * acceleration structures as the BVH and the ST-BVH. The more precise
	 * the estimate the better the expected performance of such structures.
	 *
	 * \return The estimated relative cost.
	 */
	__host__
	virtual float cost() const = 0;

	/*!
	 * \brief
	 * returns The primitive's area or `-1.f` if the area is not constant.
	 *
	 * \return The primitive's area or `-1.f` if the area is not constant.
	 */
	__host__
	virtual float area() const = 0;

	/*!
	 * \brief
	 * Computes the first intersection of a ray with this shape.
	 *
	 * \param[in, out] ray The ray to intersect the primitive with. If an
	 * intersection is found, the ray's max range is set to the intersection
	 * distance from the ray's origin.
	 * \param[out] intersection If an intersection is found holds it's details.
	 *
	 * \return true if an intersection was found and false otherwise.
	 */
	__host__ __device__
	bool intersection(Ray& ray, Intersection* intersection) const {
		return intersection_(this, ray, intersection);
	}

	/*!
	 * \brief
	 * Virtual destructor.
	 */
	__host__
	virtual ~Primitive() {
	}
};

}

#endif /* PRIMITIVE_CUH_ */

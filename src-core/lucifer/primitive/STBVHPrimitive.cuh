#ifndef STBVHPRIMITIVE_CUH_
#define STBVHPRIMITIVE_CUH_

#include <cstdint>
#include "GeometricPrimitive.cuh"
#include "PrimitiveSet.cuh"
#include "lucifer/bvh/STBVH.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class STPrimitiveBuilder: public STBVHElementBuilder<const Primitive, const GeometricPrimitive> {
public:
	__host__
	Pointer<const Primitive> build(MemoryPool& pool, const Array<STBVHElement<const GeometricPrimitive>>& elements, size_t min, size_t max) const override {
		size_t n = max - min;
		if (n == 0) {
			return Pointer<const Primitive>();
		}
		if (n == 1) {
			return Pointer<const Primitive>(elements[min].element);
		}
		PrimitiveSet::array_t array(pool, n);
		for (size_t i = 0; i < n; i++) {
			array[i] = elements[min + i].element;
		}
		return Pointer<const Primitive>(PrimitiveSet::build(pool, array));
	}

	__host__
	virtual ~STPrimitiveBuilder() {
	}
};

/*!
 * \brief
 * A composite primitive organized in a hierarchy of spatiotemporal bounding
 * volumes.
 *
 * \author
 * Bruno Pera.
 */
class STBVHPrimitive: public Primitive {
public:
	using index_t = uint32_t;
	using array_t = Array<Pointer<const GeometricPrimitive>, index_t>;

private:
	Pointer<STBVHNode<const Primitive>> bvh;

	__host__ __device__
	static bool intersection_(const Primitive* this_, Ray& ray, Intersection* intersection) {
		return static_cast<const STBVHPrimitive*>(this_)->bvh->intersection(ray, intersection);
	}

	__host__
	STBVHPrimitive(Pointer<STBVHNode<const Primitive>> bvh, const intersection_t& intersection_):
	Primitive(intersection_), bvh(bvh) {
	}

	__host__
	STBVHPrimitive(MemoryPool& pool, const array_t& primitives, const intersection_t& intersection_):
	Primitive(intersection_), bvh(STBVHNode<const Primitive>::build(pool, primitives, STPrimitiveBuilder())) {
	}

public:
	friend class TypedPool<STBVHPrimitive>;

	__host__
	static Pointer<STBVHPrimitive> build(MemoryPool& pool, Pointer<STBVHNode<const Primitive>>&& bvh) {
		return Pointer<STBVHPrimitive>::make(
			pool, bvh,
			intersection_t(virtualize(intersection_))
		);
	}

	/*!
	 * \brief
	 * Creates a new BVH from an array of primitives.
	 *
	 * \param[in] n The number of elements.
	 * \param[in] shapes The array of primitives.
	 */
	__host__
	static Pointer<STBVHPrimitive> build(MemoryPool& pool, const array_t& primitives) {
		return Pointer<STBVHPrimitive>::make(
			pool, pool, primitives,
			intersection_t(virtualize(intersection_))
		);
	}

	__host__
	Pointer<Primitive> transform(MemoryPool& pool, const Transformation3F& transformation) const override {
		return STBVHPrimitive::build(pool, bvh->transform(pool, transformation));
	}

	__host__
	Pointer<Primitive> transform(MemoryPool& pool, const Pointer<const AnimatedTransformation>& transformation) const override {
		return STBVHPrimitive::build(pool, bvh->transform(pool, transformation));
	}

	__host__
	Box boundary(const RangeF& time) const override {
		return bvh->boundary(time);
	}

	__host__
	float cost() const override {
		return bvh->cost();
	}

	__host__
	float area() const override {
		return bvh->area();
	}
};

}

#endif /* STBVHPRIMITIVE_CUH_ */

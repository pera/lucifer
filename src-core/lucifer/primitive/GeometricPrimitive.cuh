#ifndef GEOMETRICPRIMITIVE_CUH_
#define GEOMETRICPRIMITIVE_CUH_

#include "Primitive.cuh"
#include "lucifer/color/XYZColorSpace.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/random/PiecewiseLinearDistribution2D.cuh"
#include "lucifer/random/CorrelatedMultijitteredRNG.cuh"

namespace lucifer {

/*!
 * \brief
 * Geomtric Primitive.
 *
 * A simple primitive that connects a Shape and Material to define it's
 * geometry and appearance respectively.
 *
 * \author
 * Bruno Pera.
 */
class GeometricPrimitive: public Primitive {
private:
	mutable size_t id_;
	const Pointer<const Shape> shape;
	const Pointer<const Material> material;
	const Pointer<const PiecewiseLinearDistribution2D> histogram;

	__host__
	static Pointer<const PiecewiseLinearDistribution2D> makeHistogram(
		MemoryPool& pool,
		const Pointer<const Shape>& shape,
		const Pointer<const Material>& material,
		const short nu, const short nv, const short nl,
		const Pointer<const XYZColorSpace>& cs)
	{
		if (shape->canComputeRND() && material->isEmissive()) {
			Geometry g;
			float du = 1.f / (nu - 1);
			float dv = 1.f / (nv - 1);
			CPUCorrelatedMultiJitteredRNGFactory rngFactory(nl);
			Pointer<RNG> rng1 = rngFactory.build(pool, 1);
			Pointer<RNG> rng2 = rngFactory.build(pool, 2);
			float lRND[1], lPDF, gRND[2], tRND[1];
			PiecewiseLinearDistribution2D::array_t power(pool, nu * nv);
			for (PiecewiseLinearDistribution2D::idx2d_t u = 0; u < nu; u++) {
				for (PiecewiseLinearDistribution2D::idx2d_t v = 0; v < nv; v++) {
					Color3F color;
					power[v * nu + u] = 0.f;
					for (PiecewiseLinearDistribution2D::idx2d_t l = 0; l < nl; l++) {
						// sample geometry
						rng2->next(gRND);
						gRND[0] = clamp(du * (gRND[0] + u - .5f), 0.f, 1.f);
						gRND[1] = clamp(dv * (gRND[1] + v - .5f), 0.f, 1.f);
						rng1->next(tRND);
						shape->sampleGeometry(tRND[0], gRND, &g);
						// sample wavelength
						rng1->next(lRND);
						lRND[0] = cs->xyzHistogram()->sample(lRND[0], &lPDF);
						// emitted XYZ power
						cs->XYZ(*lRND, material->emission0(g, lRND[0], tRND[0]) / lPDF, color);
						power[v * nu + u] += (color[0] + color[1] + color[2]);
					}

				}
			}
			return Pointer<const PiecewiseLinearDistribution2D>(PiecewiseLinearDistribution2D::build(pool, RangeF(0.f, 1.f), RangeF(0.f, 1.f), nu, nv, power));
		}
		return Pointer<const PiecewiseLinearDistribution2D>();
	}

	__host__ __device__
	static const GeometricPrimitive* cast(const Primitive* this_) {
		return static_cast<const GeometricPrimitive*>(this_);
	}

	__host__ __device__
	static bool intersection_(const Primitive* this_, Ray& ray, Intersection* intersection) {
		if (cast(this_)->shape->intersection(ray, intersection)) {
			if (intersection != nullptr) {
				intersection->primitive = cast(this_);
			}
			return true;
		}
		return false;
	}

	__host__
	GeometricPrimitive(
		MemoryPool& pool,
		const Pointer<const Shape>& shape,
		const Pointer<const Material>& material,
		const bool buildHistogram,
		const short nu,
		const short nv,
		const short nl,
		const Pointer<const XYZColorSpace>& cs,
		const intersection_t& intersection_
	):
		Primitive(intersection_),
		id_(0),
		shape(shape),
		material(material),
		histogram(buildHistogram ? makeHistogram(pool, shape, material, nu, nv, nl, cs) : Pointer<const PiecewiseLinearDistribution2D>()) {
	}

public:
	friend class TypedPool<GeometricPrimitive>;

	/*!
	 * \brief
	 * Creates a new geometric primitive.
	 *
	 * \param[in] shape The shape that defines the primitive's geometry.
	 * \param[in] material The material that defines the primitive's appearance.
	 * \param[in] buildHistogram If a emitted power distribution should be
	 * estimated. The distribution is used to better sample the surface of
	 * light emitting primtives with with non constant emitted power.
	 * \param[in] nu The number of bins in the u axis of the estimated
	 * emitted power distribution, is ignored if `buildHistogram` is `false`.
	 * \param[in] nu The number of bins in the v axis of the estimated
	 * emitted power distribution, is ignored if `buildHistogram` is `false`.
	 * \param[in] nl The number of samples taken per uv bins to estimate the
	 * emitted power distribution, is ignored if `buildHistogram` is `false`.
	 * \param[in] cs The color space used to weight the different wavelengths
	 * while estimating the emitted power distribution, is ignored if
	 * `buildHistogram` is `false`.
	 */
	__host__
	static Pointer<GeometricPrimitive> build(
		MemoryPool& pool,
		const Pointer<const Shape>& shape,
		const Pointer<const Material>& material,
		const bool buildHistogram = false,
		const short nu = 0,
		const short nv = 0,
		const short nl = 0,
		const Pointer<const XYZColorSpace>& cs = Pointer<const XYZColorSpace>())
	{
		return Pointer<GeometricPrimitive>::make(
			pool, pool, shape, material, buildHistogram, nu, nv, nl, cs,
			intersection_t(virtualize(intersection_))
		);
	}

	/*!
	 * \brief
	 * Sets the identifier of this geometric primitive defined by the scene's
	 * light distribution.
	 *
	 * \param[id] The identifier defined the light distribution.
	 */
	__host__
	void id(const size_t id) const  {
		id_ = id;
	}

	/*!
	 * \brief
	 * Returns the identifier of this geometric primitive defined by the
	 * scene's light distribution.
	 *
	 * \return The identifier defined the light distribution.
	 */
	__host__ __device__
	size_t id() const {
		return id_;
	}

	/*!
	 * \brief
	 * Returns the Shape that defines the primtive's geometry.
	 *
	 * \return the Shape that defines the primtive's geometry.
	 */
	__host__ __device__
	const Pointer<const Shape>& getShape() const {
		return shape;
	}

	/*!
	 * \brief
	 * Returns the Material that defines the primtive's appearance.
	 *
	 * \return The Material that defines the primtive's appearance.
	 */
	__host__ __device__
	const Pointer<const Material>& getMaterial() const {
		return material;
	}

	__host__ __device__
	float emission0(const Geometry& g, const float wlen, const float time) const {
		return material->emission0(g, wlen, time);
	}

	__host__ __device__
	float emission1(const Geometry& g, const float wlen, const float time, const Vectr3F& w) const {
		return material->emission1(g, wlen, time, w);
	}

	__host__ __device__
	float emission(const Geometry& g, const float wlen, const float time, const Vectr3F& w) const {
		return material->emission(g, wlen, time, w);
	}

	__host__ __device__
	float sampleLe(const Point3F& from, const float wlen, const float time, float rnd[2], Geometry* geometry, float* pdf) const {
		if (histogram.isNull()) {
			shape->sampleGeometry(from, time, rnd, geometry, pdf);
		} else {
			histogram->sample(rnd, rnd, pdf);
			shape->sampleGeometry(time, rnd, geometry);
			*pdf = toSolidAnglePdf(from, geometry->gp, geometry->gn, *pdf / shape->area(time));
		}
		return material->emission(*geometry, wlen, time, (from - geometry->gp).normalize());
	}

	__host__ __device__
	float sampleLePDF(const Point3F& from, const float wlen, const float time, const Geometry& geometry) const {
		if (histogram.isNull()) {
			return shape->sampleGeometryPDF(from, time, geometry);
		} else {
			float rnd[2];
			shape->rnd(time, geometry.gp, rnd);
			return toSolidAnglePdf(from, geometry.gp, geometry.gn, histogram->pdf(rnd) / shape->area(time));
		}
	}

	__host__ __device__
	void sampleLeGeo(const Point3F& from, const float wlen, const float time, float rnd[2], Geometry* geometry, float* pdf) const {
		if (histogram.isNull()) {
			shape->sampleGeometry(from, time, rnd, geometry, pdf);
		} else {
			histogram->sample(rnd, rnd, pdf);
			shape->sampleGeometry(time, rnd, geometry);
			*pdf = toSolidAnglePdf(from, geometry->gp, geometry->gn, *pdf / shape->area(time));
		}
	}

	__host__ __device__
	void sampleLeGeo(const float wlen, const float time, float rnd[2], Geometry* geometry, float* pdf) const {
		if (!histogram.isNull()) histogram->sample(rnd, rnd, pdf);
		else if (pdf) *pdf = 1.f;
		shape->sampleGeometry(time, rnd, geometry);
		if (pdf) *pdf /= shape->area(time);
	}

	__host__ __device__
	float sampleLeGeoPDF(const float wlen, const float time, const Point3F& geometry) const {
		float pdf = 1.f;
		if (!histogram.isNull()) {
			float rnd[2];
			shape->rnd(time, geometry, rnd);
			pdf = histogram->pdf(rnd);
		}
		return pdf / shape->area(time);
	}

	__host__ __device__
	float sampleLeDir(const Geometry& g, const float wlen, const float time, float rnd[2], Vectr3F* w, float* pdf, const UEDC** sampled = nullptr) const {
		return material->sampleWe(g, wlen, time, rnd, w, pdf, sampled);
	}

	__host__ __device__
	float sampleLeDirPDF(const Geometry& g, const float wlen, const float time, const Vectr3F& w) const {
		return material->sampleWePDF(g, wlen, time, w);
	}

	__host__
	Pointer<Primitive> transform(MemoryPool& pool, const Transformation3F& transformation) const  override {
		// TODO: deal with histogram
		return GeometricPrimitive::build(pool, shape->transform(pool, transformation, shape), material->transform(pool, transformation, material));
	}

	__host__
	Pointer<Primitive> transform(MemoryPool& pool, const Pointer<const AnimatedTransformation>& transformation) const override {
		// TODO:: deal with histogram
		return GeometricPrimitive::build(pool, shape->transform(pool, transformation, shape), material->transform(pool, transformation, material));
	}

	__host__
	Box boundary(const RangeF& time) const override {
		return shape->boundary(time);
	}

	__host__
	float cost() const override {
		return shape->cost();
	}

	__host__
	float area() const override {
		return shape->area();
	}

	__host__ __device__
	float area(const float time) const {
		return shape->area(time);
	}
};

}

#endif /* GEOMETRICPRIMITIVE_CUH_ */

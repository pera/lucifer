#include "Primitive.cuh"
#include "GeometricPrimitive.cuh"
#include "lucifer/material/InteriorListUtils.cuh"

namespace lucifer {

__host__ __device__
void Primitive::Intersection::setIOR(const InteriorList& list, const Vectr3F& wo, ComplexF& ior1, ComplexF& ior2) const {
	const Material* ior1Material = nullptr;
	const Material* ior2Material = nullptr;
	if (this->primitive->getMaterial()->hasVolumetricMedium() && dot(geometry.gn, wo) < 0.f) {
		ior1Material = this->primitive->getMaterial().get();
		ior2Material = highestPriority(list, ior1Material->prioriry());
	} else {
		ior1Material = highestPriority(list);
		ior2Material = this->primitive->getMaterial().get();
	}
	if (ior1Material != nullptr) ior1 = ior1Material->refractiveIndex(geometry.gp, geometry.uv, wlen, time);
	if (ior2Material != nullptr) ior2 = ior2Material->refractiveIndex(geometry.gp, geometry.uv, wlen, time);
}

__host__ __device__
void Primitive::Intersection::setIOR(const InteriorList& list, ComplexF& ior1, ComplexF& ior2) const {
	setIOR(list, direction, ior1, ior2);
}

__host__ __device__
bool Primitive::Intersection::onSurface() const {
	return primitive != nullptr;
}

/**************************************************
 * UEDF interface
 **************************************************/
__host__ __device__
bool Primitive::Intersection::isEmissive() const {
	return primitive->getMaterial()->isEmissive();
}

__host__ __device__
float Primitive::Intersection::emission() const {
	return primitive->getMaterial()->emission(geometry, wlen, time, direction);
}

__host__ __device__
const Material* Primitive::Intersection::material() const {
	return primitive->getMaterial().get();
}

/**************************************************
 * BSDF interface
 **************************************************/
__host__ __device__
bool Primitive::Intersection::hasOpticallyInteractingBSDF() const {
	return primitive->getMaterial()->hasOpticallyInteractingBSDF();
}

__host__ __device__
bool Primitive::Intersection::matchesBSDF(BxDF::Type t) const {
	return primitive->getMaterial()->matchesBSDF(t);
}

__host__ __device__
float Primitive::Intersection::evaluateBSDF(const Vectr3F& wi, const InteriorList& media, const BxDF::Mode& mode, RNG* rng, const BxDF::Type restrictions) const {
	ComplexF ior1(1.f, 0.f);
	ComplexF ior2(1.f, 0.f);
	setIOR(media, ior1, ior2);
	return primitive->getMaterial()->evaluateBSDF(geometry, wlen, time, wi, direction, ior1, ior2, mode, rng, restrictions);
}

__host__ __device__
float Primitive::Intersection::sampleBSDFWi(const InteriorList& media, const BxDF::Mode& mode, const BxDF::Type restrictions, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng, const BxDF** sampled) const {
	ComplexF ior1(1.f, 0.f);
	ComplexF ior2(1.f, 0.f);
	setIOR(media, ior1, ior2);
	return primitive->getMaterial()->sampleBSDFWi(geometry, wlen, time, direction, ior1, ior2, mode, restrictions, rnd, wi, pdf, rng, sampled);
}

__host__ __device__
float Primitive::Intersection::sampleBSDFWiPDF(const Vectr3F& wi, const InteriorList& media, const BxDF::Mode& mode, RNG* rng, const BxDF::Type restrictions, const BxDF* sampled) const {
	ComplexF ior1(1.f, 0.f);
	ComplexF ior2(1.f, 0.f);
	setIOR(media, ior1, ior2);
	return primitive->getMaterial()->sampleBSDFWiPDF(geometry, wlen, time, direction, wi, ior1, ior2, mode, rng, restrictions, sampled);
}

__host__ __device__
float Primitive::Intersection::sampleBSDFWiPDF(const Vectr3F& wo, const Vectr3F& wi, const InteriorList& media, const BxDF::Mode& mode, RNG* rng, const BxDF::Type restrictions, const BxDF* sampled) const {
	ComplexF ior1(1.f, 0.f);
	ComplexF ior2(1.f, 0.f);
	setIOR(media, wo, ior1, ior2);
	return primitive->getMaterial()->sampleBSDFWiPDF(geometry, wlen, time, wo, wi, ior1, ior2, mode, rng, restrictions, sampled);
}

}

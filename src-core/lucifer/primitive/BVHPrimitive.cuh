#ifndef BVHPRIMITIVE_CUH_
#define BVHPRIMITIVE_CUH_

#include <cstdint>
#include "Primitive.cuh"
#include "PrimitiveSet.cuh"
#include "lucifer/bvh/BVH.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class PrimitiveBuilder: public BVHElementBuilder<const Primitive, const GeometricPrimitive> {
public:
	__host__
	Pointer<const Primitive> build(MemoryPool& pool, const Array<BVHElement<const GeometricPrimitive>>& elements, size_t min, size_t max) const override {
		size_t n = max - min;
		if (n == 0) {
			return Pointer<const Primitive>();
		}
		if (n == 1) {
			return Pointer<const Primitive>(elements[min].element);
		}
		PrimitiveSet::array_t array(pool, n);
		for (size_t i = 0; i < n; i++) {
			array[i] = elements[min + i].element;
		}
		return Pointer<const Primitive>(PrimitiveSet::build(pool, array));
	}

	__host__
	virtual ~PrimitiveBuilder() {
	}
};

/*!
 * \brief
 * A composite primitive organized in a hierarchy of bounding volumes.
 *
 * \author
 * Bruno Pera.
 */
class BVHPrimitive: public Primitive {
public:
	using index_t = uint32_t;
	using array_t = Array<Pointer<const GeometricPrimitive>, index_t>;

private:
	Pointer<BVHNode<const Primitive>> bvh;

	__host__ __device__
	static bool intersection_(const Primitive* this_, Ray& ray, Intersection* intersection) {
		return static_cast<const BVHPrimitive*>(this_)->bvh->intersection(ray, intersection);
	}

	__host__
	BVHPrimitive(Pointer<BVHNode<const Primitive>> bvh, const intersection_t& intersection_):
	Primitive(intersection_), bvh(bvh) {
	}

	__host__
	BVHPrimitive(MemoryPool& pool,const array_t& primitives, const intersection_t& intersection_):
	Primitive(intersection_), bvh(BVHNode<const Primitive>::build(pool, primitives, PrimitiveBuilder())) {
	}

public:
	friend class TypedPool<BVHPrimitive>;

	__host__
	static Pointer<BVHPrimitive> build(MemoryPool& pool, Pointer<BVHNode<const Primitive>>&& bvh) {
		return Pointer<BVHPrimitive>::make(
			pool, bvh,
			intersection_t(virtualize(intersection_))
		);
	}

	/*!
	 * \brief
	 * Creates a new BVH from an array of primitives.
	 *
	 * \param[in] n The number of elements.
	 * \param[in] shapes The array of primitives.
	 */
	__host__
	static Pointer<BVHPrimitive> build(MemoryPool& pool, const array_t& primitives) {
		return Pointer<BVHPrimitive>::make(
			pool, pool, primitives,
			intersection_t(virtualize(intersection_))
		);
	}

	__host__
	Pointer<Primitive> transform(MemoryPool& pool, const Transformation3F& transformation) const override {
		return BVHPrimitive::build(pool, bvh->transform(pool, transformation));
	}

	__host__
	Pointer<Primitive> transform(MemoryPool& pool, const Pointer<const AnimatedTransformation>& transformation) const override {
		return BVHPrimitive::build(pool, bvh->transform(pool, transformation));
	}

	__host__
	Box boundary(const RangeF& time) const override {
		return bvh->boundary(time);
	}

	__host__
	float cost() const override {
		return bvh->cost();
	}

	__host__
	float area() const override {
		return bvh->area();
	}
};

}

#endif /* BVHPRIMITIVE_CUH_ */

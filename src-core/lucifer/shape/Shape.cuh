#ifndef SHAPE_CUH_
#define SHAPE_CUH_

#include <vector>
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/Box.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/math/geometry/Ray.cuh"
#include "lucifer/math/geometry/Geometry.cuh"
#include "lucifer/math/geometry/AnimatedTransformation.cuh"
#include "lucifer/math/Util.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/memory/Virtual.cuh"

namespace lucifer {

/*!
 * \brief
 * The basic interface of a geometric shape.
 *
 * A Shape represents the geometric facet of a Primitive. To be properly
 * rendered a shape must be able to compute it's intersection with a ray and to
 * be properly used as light source it must also be able to sample points
 * uniformly distributed over it's surface area.
 *
 * \author
 * Bruno Pera.
 */
class Shape: public Polymorphic {
public:
	class Intersection {
	public:
		float wlen;
		float time;
		Vectr3F direction;
		Geometry geometry;

	public:
		__host__ __device__
		inline void setRay(const Ray& ray) {
			wlen = ray.l;
			time = ray.t;
			direction = (-ray.d).normalize();
		}

		__host__ __device__
		inline void transform(const Transform3F& t) {
			geometry.apply(t);
			direction = (t * direction).normalize();
		}

		__host__ __device__
		inline void transform(const Transformation3F& t) {
			transform(t.direct());
		}
	};

public:
	/**************************************************************************
	 * cuda virtual interface
	 *************************************************************************/
	using instant_area_t = Virtual<
		float,				// return type
		const Shape*,		// this
		const float			// time
	>;

	using intersection_t = Virtual<
		bool,				// return type
		const Shape*,		// this
		Ray&, 				// ray
		Intersection*		// intersection
	>;

	using sample_area_t = Virtual<
		void, 				// return type
		const Shape*,		// this
		const float, 		// time
		float[2],			// random
		Geometry*			// geometry
	>;

	using sample_solid_angle_t = Virtual<
		void,				// return type
		const Shape*,		// this
		const Point3F&,		// reference
		const float,		// time
		float[2],			// random
		Geometry*,			// geometry
		float*				// pdf
	>;

	using sample_pdf_t = Virtual<
		float,				// return type
		const Shape*, 		// this
		const Point3F&,		// reference
		const float, 		// time
		const Geometry&		// geometry
	>;

	using can_compute_rnd_t = Virtual<
		bool,				// return type
		const Shape*		// this
	>;

	using compute_rnd_t = Virtual<
		void,				// return type
		const Shape*,		// this
		const float,		// time
		const Point3F&,		// position
		float[2]			// random
	>;

private:
	const instant_area_t instant_area_;
	const intersection_t intersection_;
	const sample_area_t sample_area_;
	const sample_solid_angle_t sample_solid_angle_;
	const sample_pdf_t sample_pdf_;
	const can_compute_rnd_t can_compute_rnd_;
	const compute_rnd_t compute_rnd_;

protected:
	__host__ __device__
	static void default_sample_solid_angle_(const Shape* this_, const Point3F& ref, const float time, float rnd[2], Geometry* geometry, float* pdf) {
		this_->sampleGeometry(time, rnd, geometry);
		*pdf = toSolidAnglePdf(ref, geometry->gp, geometry->gn, 1.f / this_->area(time));
	}

	__host__ __device__
	static float default_sample_pdf_(const Shape* this_, const Point3F& ref, const float time, const Geometry& geometry) {
		return toSolidAnglePdf(ref,  geometry.gp,  geometry.gn, 1.f / this_->area(time));
	}

	__host__
	Shape(
		const instant_area_t& instant_area_,
		const intersection_t& intersection_,
		const sample_area_t& sample_area_,
		const sample_solid_angle_t& sample_solid_angle_,
		const sample_pdf_t& sample_pdf_,
		const can_compute_rnd_t& can_compute_rnd_,
		const compute_rnd_t& compute_rnd_
	):
		instant_area_(instant_area_),
		intersection_(intersection_),
		sample_area_(sample_area_),
		sample_solid_angle_(sample_solid_angle_),
		sample_pdf_(sample_pdf_),
		can_compute_rnd_(can_compute_rnd_),
		compute_rnd_(compute_rnd_) {
	}

public:
	/*!
	 * \brief
	 * Creates a new transformed instance of this shape.
	 *
	 * \param[in] pool The memory pool used to create new objects.
	 * \param[in] transformation The transformation. If the resulting shape is
	 * used as a samplable light source the transformation must have uniform
	 * scaling factors, otherwise the result is undefined.
	 * \param[in] self A pointer to this shape.
	 *
	 * \return A new transformed instance of this shape.
	 */
	__host__
	virtual Pointer<const Shape> transform(MemoryPool& pool, const Transformation3F& transformation, const Pointer<const Shape>& self) const = 0;

	/*!
	 * \brief
	 * Creates a new animated instace of this shape.
	 *
	 * \param[in] pool The memory pool used to create new objects.
	 * \param[in] transformation The animated transformation. If the resulting
	 * shape is used as a samplable light source the transformation must have
	 * uniform scaling factors over the entire normalized time range, otherwise
	 * the result is undefined.
	 * \param[in] self A pointer to this shape.
	 *
	 * \return A new animated instance of this shape.
	 */
	__host__
	virtual Pointer<const Shape> transform(MemoryPool& pool, const Pointer<const AnimatedTransformation>& transformation, const Pointer<const Shape>& self) const = 0;


	/*!
	 * \brief
	 * Decomposes this shape into a set of elemetary shapes.
	 *
	 * \param[in] pool The memory pool used to create new objects.
	 * \param[out] list The list to hold the decomposed set.
	 */
	__host__
	virtual void decompose(MemoryPool& pool, std::vector<Pointer<const Shape>>& list, const Pointer<const Shape>& self) const = 0;

	/*!
	 * \brief
	 * Returns the shape's bounding box.
	 *
	 * \param[in] time The normalized time range in [0, 1].
	 *
	 * \return A bounding box large enough to enclose the shape over the entire
	 * time range.
	 */
	__host__
	virtual Box boundary(const RangeF& time) const = 0;

	/*!
	 * \brief
	 * Returns the estimated cost of intersecting the shape with a ray.
	 *
	 * The cost should be estimated as the average computational cost
	 * relatively to the cost of intersecting an axis aligned bounding Box,
	 * whose cost is, by definition, 1.
	 * This estimate is used to guide the construction of intersection
	 * acceleration structures as the BVH and the ST-BVH. The more precise
	 * the estimate the better the expected performance of such structures.
	 *
	 * \return The estimated relative cost.
	 */
	__host__
	virtual float cost() const = 0;

	/*!
	 * \brief
	 * returns The shape's area or `-1.f` if the area is not constant.
	 *
	 * \return The shape's area or `-1.f` if the area is not constant.
	 */
	__host__
	virtual float area() const = 0;

	/*!
	 * \brief
	 * Virtual destructor.
	 */
	__host__
	virtual ~Shape() {
	}

	/*!
	 * \brief
	 * Returns the shape's area.
	 *
	 * \param[in] time The normalized time in [0, 1].
	 *
	 * \return The shape's area at the given time.
	 */
	__host__ __device__
	float area(const float time) const {
		return instant_area_(this, time);
	}

	/*!
	 * \brief
	 * Computes the first intersection of a ray with this shape.
	 *
	 * \param[in, out] ray The ray to intersect the shape with. If an
	 * intersection is found, the ray's max range is set to the intersection
	 * distance from the ray's origin.
	 * \param[out] intersection If an intersection is found holds it's details.
	 *
	 * \return true if an intersection was found and false otherwise.
	 */
	__host__ __device__
	bool intersection(Ray& ray, Intersection* intersection) const {
		return intersection_(this, ray, intersection);
	}
	/*!
	 * \brief
	 * Uniformly samples a position over the shape's surface.
	 *
	 * \param[in] time The normalized time in [0, 1]
	 * \param[in] rnd A pair of randomly generated numbers in [0, 1].
	 * \param[out] geometry Holds the geometric details at the sampled position.
	 */
	__host__ __device__
	void sampleGeometry(const float time, float rnd[2], Geometry* geometry) const {
		sample_area_(this, time, rnd, geometry);
	}

	/*!
	 * \brief
	 * Samples a position over the shape's surface according to the solid angle
	 * measure from a given reference point.
	 *
	 * \param[in] ref The reference point.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] rnd A pair of randomly generated numbers in [0, 1].
	 * \param[out] geometry Holds the geometric details at the sampled position.
	 * \param[out] pdf Holds the sampling pdf.
	 *
	 */
	__host__ __device__
	void sampleGeometry(const Point3F& ref, const float time, float rnd[2], Geometry* geometry, float* pdf) const {
		sample_solid_angle_(this, ref, time, rnd, geometry, pdf);
	}

	/*!
	 * \brief
	 * Computes the PDF, according to the solid angle measure from a given
	 * reference point, of sampling a position over the shape's surface.
	 *
	 * \param[in] ref The reference point.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] geometry The sampled geomtry.
	 *
	 * \return The sampling pdf.
	 */
	__host__ __device__
	float sampleGeometryPDF(const Point3F& ref, const float time, const Geometry& geometry) const {
		return sample_pdf_(this, ref, time, geometry);
	}

	/*!
	 * \brief
	 * Returns true if this shape can compute the pair of random numbers that
	 * generates a given position on the surface.
	 *
	 * \return true if this shape can compute the pair of random numbers that
	 * generates a given position on the surface.
	 */
	__host__ __device__
	bool canComputeRND() const {
		return can_compute_rnd_(this);
	}

	/*!
	 * \brief
	 * Computes the pair of random numbers in [0, 1] that generates a position
	 * on the shape's surface.
	 *
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] point The position on the shape's surface.
	 * \param[out] rnd The pair of random numbers that generates the point.
	 */
	__host__ __device__
	void rnd(const float time, const Point3F& point, float rnd[2]) const {
		compute_rnd_(this, time, point, rnd);
	}
};

}

#endif /* SHAPE_CUH_ */

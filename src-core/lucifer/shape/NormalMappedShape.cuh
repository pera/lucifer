#ifndef NORMALMAPPEDSHAPE_CUH_
#define NORMALMAPPEDSHAPE_CUH_

#include <cassert>
#include "TransformableShape.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class NormalMappedShape: public TransformableShape {
private:
	const Pointer<const ShadingNode<Norml3F>> normalMap;
	const Pointer<const ShadingNode<float>> factor;
	const Pointer<const Shape> shape;

private:
	__host__ __device__
	void bump(Geometry& g, const float time) const {
		const float f = max(0.f, (*factor)(g.gp, g.su, g.sn, g.uv, 0.f, time));
		Norml3F n = (*normalMap)(g.gp, g.su, g.sn, g.uv, 0.f, time);
		n[0] = f * (2.f * clamp(n[0], 0.f, 1.f) - 1.f);
		n[1] = f * (2.f * clamp(n[1], 0.f, 1.f) - 1.f);
		n[2] = (n[0] == 0.f and n[1] == 0.f) ? 1.f : clamp(n[2], 0.f, 1.f);
		Vectr3F wn, ln = Vectr3F(n).normalize();
		Vectr3F g_sv = cross<Norml3F, Vectr3F, Vectr3F>(g.sn, g.su).normalize();
		toWorldCoordinates(g.su, g_sv, g.sn, ln, wn);
		g.sn = Norml3F(wn);
		g.su = cross<Vectr3F, Norml3F, Vectr3F>(g_sv, g.sn).normalize();
		assert(dot(g.gn, g.sn) >= 0.f);
	}

	__host__ __device__
	static const NormalMappedShape* cast(const Shape* this_) {
		return static_cast<const NormalMappedShape*>(this_);
	}

	__host__ __device__
	static float instant_area_(const Shape* this_, const float time) {
		return cast(this_)->shape->area(time);
	}

	__host__ __device__
	static bool intersection_(const Shape* this_, Ray& ray, Intersection* intersection) {
		if (!cast(this_)->shape->intersection(ray, intersection)) {
			return false;
		}
		if (intersection != nullptr) {
			cast(this_)->bump(intersection->geometry, ray.t);
		}
		return true;
	}

	__host__ __device__
	static void sample_area_(const Shape* this_, const float time, float rnd[2], Geometry* geometry) {
		cast(this_)->shape->sampleGeometry(time, rnd, geometry);
		cast(this_)->bump(*geometry, time);
	}

	__host__ __device__
	static void sample_solid_angle_ (const Shape* this_, const Point3F& ref, const float time, float rnd[2], Geometry* geometry, float* pdf) {
		cast(this_)->shape->sampleGeometry(ref, time, rnd, geometry, pdf);
		cast(this_)->bump(*geometry, time);
	}

	__host__ __device__
	static float sample_pdf_(const Shape* this_, const Point3F& ref, const float time, const Geometry& geometry) {
		return cast(this_)->shape->sampleGeometryPDF(ref, time, geometry);
	}

	__host__ __device__
	static bool can_compute_rnd_(const Shape* this_) {
		return cast(this_)->shape->canComputeRND();
	}

	__host__ __device__
	static void compute_rnd_(const Shape* this_, const float time, const Point3F& geometry, float rnd[2]) {
		cast(this_)->shape->rnd(time, geometry, rnd);
	}

	__host__
	NormalMappedShape(
		const Pointer<const ShadingNode<Norml3F>>& normalMap,
		const Pointer<const ShadingNode<float>>& factor,
		const Pointer<const Shape>& shape,
		const instant_area_t& instant_area_,
		const intersection_t& intersection_,
		const sample_area_t& sample_area_,
		const sample_solid_angle_t& sample_solid_angle_,
		const sample_pdf_t& sample_pdf_,
		const can_compute_rnd_t& can_compute_rnd_,
		const compute_rnd_t& compute_rnd_
	):
		TransformableShape(instant_area_, intersection_, sample_area_, sample_solid_angle_, sample_pdf_, can_compute_rnd_, compute_rnd_),
		normalMap(normalMap), factor(factor), shape(shape) {
	}

public:
	friend class TypedPool<NormalMappedShape>;

	/*!
	 * \brief
	 * Creates a new normal-mapped shape
	 *
	 * \param[in] pool The memory pool used to create new objects.
	 * \param[in] bumpMap The bump map.
	 * \param[in] factor The strength factor.
	 * \param[in] shape The original shape.
	 */
	__host__
	static Pointer<NormalMappedShape> build(
		MemoryPool& pool,
		const Pointer<const ShadingNode<Norml3F>>& normalMap,
		const Pointer<const ShadingNode<float>>& factor,
		const Pointer<const Shape>& shape)
	{
		return Pointer<NormalMappedShape>::make(
			pool, normalMap, factor, shape,
			instant_area_t(virtualize(instant_area_)),
			intersection_t(virtualize(intersection_)),
			sample_area_t(virtualize(sample_area_)),
			sample_solid_angle_t(virtualize(sample_solid_angle_)),
			sample_pdf_t(virtualize(sample_pdf_)),
			can_compute_rnd_t(virtualize(can_compute_rnd_)),
			compute_rnd_t(virtualize(compute_rnd_))
		);
	}

	__host__
	void decompose(MemoryPool& pool, std::vector<Pointer<const Shape>>& list, const Pointer<const Shape>& self) const override {
		std::vector<Pointer<const Shape>> tmp;
		shape->decompose(pool, tmp, shape);
		for (const auto& child : tmp) {
			list.push_back(Pointer<const Shape>(NormalMappedShape::build(pool, normalMap, factor, child)));
		}
	}

	__host__
	Box boundary(const RangeF& time) const override {
		return shape->boundary(time);
	}

	__host__
	float cost() const override {
		return shape->cost();
	}

	__host__
	float area() const override {
		return shape->area();
	}
};

}

#endif /* NORMALMAPPEDSHAPE_CUH_ */

#ifndef BVHSHAPE_CUH_
#define BVHSHAPE_CUH_

#include <cstdint>
#include "ShapeSet.cuh"
#include "TransformableShape.cuh"
#include "lucifer/bvh/BVH.cuh"
#include "lucifer/system/Environment.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class ShapeBuilder: public BVHElementBuilder<const Shape> {
public:
	__host__
	Pointer<const Shape> build(MemoryPool& pool, const Array<BVHElement<const Shape>>& elements, size_t min, size_t max) const override {
		size_t n = max - min;
		if (n == 0) {
			return Pointer<const Shape>();
		}
		if (n == 1) {
			return elements[min].element;
		}
		ShapeSet::array_t array(pool, n);
		for (size_t i = 0; i < n; i++) {
			array[i] = elements[min + i].element;
		}
		return Pointer<const Shape>(ShapeSet::build(pool, array));
	}

	__host__
	virtual ~ShapeBuilder() {
	}
};

/*!
 * \brief
 * A composite primitive organized in a hierarchy of bounding volumes.
 *
 * \author
 * Bruno Pera.
 */
class BVHShape: public TransformableShape {
public:
	using index_t = uint32_t;
	using array_t = Array<Pointer<const Shape>, index_t>;

private:
	const Pointer<BVHNode<const Shape>> bvh;

	__host__
	static void decompose(MemoryPool& pool, const BVHNode<const Shape>* node, std::vector<Pointer<const Shape>>& list) {
		if (node->isLeaf()) {
			node->element()->decompose(pool, list, node->element());
			return;
		}
		decompose(pool, node->child0(), list);
		decompose(pool, node->child1(), list);
	}

	__host__ __device__
	static const BVHShape* cast(const Shape* this_) {
		return static_cast<const BVHShape*>(this_);
	}

	__host__ __device__
	static float instant_area_(const Shape* this_, const float time) {
		const float area_ = cast(this_)->bvh->area();
		return area_ >= 0.f ? area_ : cast(this_)->bvh->area(time);
	}

	__host__ __device__
	static bool intersection_(const Shape* this_, Ray& ray, Intersection* intersection) {
		return cast(this_)->bvh->intersection(ray, intersection);
	}

	__host__ __device__
	static void sample_area_(const Shape* this_, const float time, float rnd[2], Geometry* geometry) {
		require(false);
	}

	__host__ __device__
	static void sample_solid_angle_ (const Shape* this_, const Point3F& ref, const float time, float rnd[2], Geometry* geometry, float* pdf) {
		require(false);
	}

	__host__ __device__
	static float sample_pdf_(const Shape* this_, const Point3F& ref, const float time, const Geometry& geometry) {
		require(false);
		return 0.f;
	}

	__host__ __device__
	static bool can_compute_rnd_(const Shape* this_) {
		return false;
	}

	__host__ __device__
	static void compute_rnd_(const Shape* this_, const float time, const Point3F& geometry, float rnd[2]) {
	}

	__host__
	BVHShape(
		MemoryPool& pool,
		const array_t& shapes,
		const instant_area_t& instant_area_,
		const intersection_t& intersection_,
		const sample_area_t& sample_area_,
		const sample_solid_angle_t& sample_solid_angle_,
		const sample_pdf_t& sample_pdf_,
		const can_compute_rnd_t& can_compute_rnd_,
		const compute_rnd_t& compute_rnd_
	):
		TransformableShape(instant_area_, intersection_, sample_area_, sample_solid_angle_, sample_pdf_, can_compute_rnd_, compute_rnd_),
		bvh(BVHNode<const Shape>::build(pool, shapes, ShapeBuilder())) {
	}

	__host__
	BVHShape(
		MemoryPool& pool,
		const array_t&& shapes,
		const instant_area_t& instant_area_,
		const intersection_t& intersection_,
		const sample_area_t& sample_area_,
		const sample_solid_angle_t& sample_solid_angle_,
		const sample_pdf_t& sample_pdf_,
		const can_compute_rnd_t& can_compute_rnd_,
		const compute_rnd_t& compute_rnd_
	):
	TransformableShape(instant_area_, intersection_, sample_area_, sample_solid_angle_, sample_pdf_, can_compute_rnd_, compute_rnd_),
		bvh(BVHNode<const Shape>::build(pool, shapes, ShapeBuilder())) {
	}

public:
	friend class TypedPool<BVHShape>;

	/*!
	 * \brief
	 * Creates a new BVH from a set of shapes.
	 *
	 * \param[in] shapes The set of shapes.
	 */
	__host__
	static Pointer<BVHShape> build(MemoryPool& pool, const array_t& shapes) {
		return Pointer<BVHShape>::make(
			pool, pool, shapes,
			instant_area_t(virtualize(instant_area_)),
			intersection_t(virtualize(intersection_)),
			sample_area_t(virtualize(sample_area_)),
			sample_solid_angle_t(virtualize(sample_solid_angle_)),
			sample_pdf_t(virtualize(sample_pdf_)),
			can_compute_rnd_t(virtualize(can_compute_rnd_)),
			compute_rnd_t(virtualize(compute_rnd_))
		);
	}

	/*!
	 * \brief
	 * Creates a new BVH from a set of shapes.
	 *
	 * \param[in] shapes The set of shapes.
	 */
	__host__
	static Pointer<BVHShape> build(MemoryPool& pool, const array_t&& shapes) {
		return Pointer<BVHShape>::make(
			pool, pool, shapes,
			instant_area_t(virtualize(instant_area_)),
			intersection_t(virtualize(intersection_)),
			sample_area_t(virtualize(sample_area_)),
			sample_solid_angle_t(virtualize(sample_solid_angle_)),
			sample_pdf_t(virtualize(sample_pdf_)),
			can_compute_rnd_t(virtualize(can_compute_rnd_)),
			compute_rnd_t(virtualize(compute_rnd_))
		);
	}

	__host__
	void decompose(MemoryPool& pool, std::vector<Pointer<const Shape>>& list, const Pointer<const Shape>& self) const override {
		decompose(pool, bvh.get(), list);
	}

	__host__
	Box boundary(const RangeF& time) const override{
		return bvh->boundary(time);
	}

	__host__
	float cost() const override {
		return bvh->cost();
	}

	__host__
	float area() const override {
		return bvh->area();
	}
};

}

#endif /* BVHSHAPE_CUH_ */

#ifndef TRIANGLE_CUH_
#define TRIANGLE_CUH_

#include "TransformableShape.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/random/UniformTriangleDistribution.cuh"
#include "lucifer/memory/MemoryPool.cuh"

namespace lucifer {

/*!
 * \brief
 * A triangular Shape defined by three vertices.
 *
 * \author
 * Bruno Pera.
 */
class Triangle: public TransformableShape {
private:
	const Point3F p;
	const Vectr3F u;
	const Vectr3F v;
	const Norml3F n;
	const float area_;

	__host__ __device__
	void geometry(const Point3F& point, Geometry* geometry) const {
		geometry->gp = point;
		geometry->gn = geometry->sn = n;
		geometry->gu = geometry->su = u;
		geometry->gu = geometry->su.normalize();
	}

	__host__ __device__
	static const Triangle* cast(const Shape* this_) {
		return static_cast<const Triangle*>(this_);
	}

	__host__ __device__
	static float instant_area_(const Shape* this_, const float time) {
		return cast(this_)->area_;
	}

	__host__ __device__
	static bool intersection_(const Shape* this_, Ray& ray, Intersection* intersection) {
		Vectr3F s = cross(ray.d, cast(this_)->v);
		float factor = dot(s, cast(this_)->u);
	    if (factor == 0.f) {
	      return false;
	    }
	    factor = 1.f / factor;
	    // Compute first barycentric coordinate
	    Vectr3F d = ray.o - cast(this_)->p;
	    float b1 = dot(d, s) * factor;
	    if ((b1 < 0.f) || (b1 > 1.f)) {
	      return false;
	    }
	    // Compute second barycentric coordinate
	    s = cross(d, cast(this_)->u);
	    float b2 = dot(ray.d, s) * factor;
	    if ((b2 < 0.f) || ((b1 + b2) > 1.f)) {
	      return false;
	    }
	    // Compute distance to ray's origin
	    float distance = dot(cast(this_)->v, s) * factor;
	    if ((distance < ray.r.lower) || (distance > ray.r.upper)) {
	      return false;
	    }
	    ray.r.upper = distance;
	    if (intersection != nullptr) {
			intersection->setRay(ray);
			cast(this_)->geometry(ray(distance), &intersection->geometry);
			intersection->geometry.uv = Point2F(1.f - (b1 + b2), b1);
	    }
	    return true;
	}

	__host__ __device__
	static void sample_area_(const Shape* this_, const float time, float rnd[2], Geometry* geometry) {
		float b0, b1;
		UniformTriangleDistribution::sample(cast(this_)->p, cast(this_)->u, cast(this_)->v, rnd, &geometry->gp[0], &b0, &b1);
		geometry->gn = geometry->sn = cast(this_)->n;
		geometry->gu = geometry->su = cast(this_)->u;
		geometry->gu = geometry->su.normalize();
		geometry->uv = Point2F(b0, b1);
	}

	__host__ __device__
	static bool can_compute_rnd_(const Shape* this_) {
		return true;
	}

	__host__ __device__
	static void compute_rnd_(const Shape* this_, const float time, const Point3F& point, float rnd[2]) {
		UniformTriangleDistribution::random(cast(this_)->p, cast(this_)->u, cast(this_)->v, cast(this_)->n, point, rnd);
	}

	__host__
	Triangle(
		const Point3F& a,
		const Point3F& b,
		const Point3F& c,
		const instant_area_t& instant_area_,
		const intersection_t& intersection_,
		const sample_area_t& sample_area_,
		const sample_solid_angle_t& sample_solid_angle_,
		const sample_pdf_t& sample_pdf_,
		const can_compute_rnd_t& can_compute_rnd_,
		const compute_rnd_t& compute_rnd_
	):
		TransformableShape(instant_area_, intersection_, sample_area_, sample_solid_angle_, sample_pdf_, can_compute_rnd_, compute_rnd_),
		p(a), u(b - a), v(c - a), n(cross<Vectr3F, Vectr3F, Norml3F>(u, v).normalize()), area_(triangleArea(a, b, c)) {
	}

public:
	friend class TypedPool<Triangle>;

	/*!
	 * \brief
	 * Creates a new triangular shape.
	 *
	 * \param[in] a The 1st vertex.
	 * \param[in] b The 2nd vertex.
	 * \param[in] c The 3rd vertex.
	 */
	__host__
	static Pointer<Triangle> build(MemoryPool& pool, const Point3F& a, const Point3F& b, const Point3F& c) {
		return Pointer<Triangle>::make(
			pool, a, b, c,
			instant_area_t(virtualize(instant_area_)),
			intersection_t(virtualize(intersection_)),
			sample_area_t(virtualize(sample_area_)),
			sample_solid_angle_t(virtualize(default_sample_solid_angle_)),
			sample_pdf_t(virtualize(default_sample_pdf_)),
			can_compute_rnd_t(virtualize(can_compute_rnd_)),
			compute_rnd_t(virtualize(compute_rnd_))
		);
	}

	__host__
	void decompose(MemoryPool& pool, std::vector<Pointer<const Shape>>& list, const Pointer<const Shape>& self) const override {
		list.push_back(self);
	}

	__host__
	Box boundary(const RangeF& time) const override {
		Box result(p);
		result += (p + u);
		result += (p + v);
		return result;
	}

	__host__
	float cost() const override {
		return 2.25f;
	}

	__host__
	float area() const override {
		return area_;
	}
};

}

#endif /* TRIANGLE_CUH_ */

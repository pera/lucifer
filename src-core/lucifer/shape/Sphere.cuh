#ifndef SPHERE_CUH_
#define SPHERE_CUH_

#include "TransformableShape.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/random/UniformSphereDistribution.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * A spherical Shape defined by it's center point and radius.
 *
 * \author
 * Bruno Pera.
 */
class Sphere: public TransformableShape {
private:
	const Point3F center_;
	const float radius_;
	const float area_;

private:
	__host__ __device__
	void uvCoordinates(Geometry* g) const {
		Vectr3F v = g->gp - center_;
		g->uv[1] = 1.f - acos(v[1] / v.length()) / PI<float>();
		g->uv[0] = atan2(v[2], v[0]) / (2 * PI<float>());
		if (g->uv[0]  < 0.f) {
			g->uv[0] += 1.f;
		}
	}

	__host__ __device__
	void geometry(const Point3F& point, Geometry* geometry) const {
		geometry->gp = point;
		geometry->gn = geometry->sn = Norml3F{(point - center_) / radius_};
		geometry->gu = geometry->su = Vectr3F{-(point[1] - center_[1]), +(point[0] - center_[0]), 0.f}.normalize();
		uvCoordinates(geometry);
	}

	__host__ __device__
	bool intersection(const Point3F& o, const Vectr3F& d, Geometry* geo) const {
		Vectr3F l = o - center_;
		float a = dot(d, d);
		float b = dot(d, l) * 2.f;
		float c = dot(l, l) - radius_ * radius_;
		float t0, t1;
		if (!solveQuadratic(a, b, c, t0, t1)) {
			return false;
		}
		if (t0 >= 0.f) {
			geometry(o + d * t0, geo);
			return true;
		}
		if (t1 >= 0.f) {
			geometry(o + d * t1, geo);
			return true;
		}
		return false;
	}

	__host__ __device__
	static const Sphere* cast(const Shape* this_) {
		return static_cast<const Sphere*>(this_);
	}

	__host__ __device__
	static float instant_area_(const Shape* this_, const float time) {
		return cast(this_)->area_;
	}

	__host__ __device__
	static bool intersection_(const Shape* this_, Ray& ray, Intersection* intersection) {
		Vectr3F l = ray.o - cast(this_)->center_;
		float a = dot(ray.d, ray.d);
		float b = dot(ray.d, l) * 2.f;
		float c = dot(l, l) - cast(this_)->radius_ * cast(this_)->radius_;

		float t0, t1;
		if (!solveQuadratic(a, b, c, t0, t1))
			return false;

		if (t0 >= ray.r.lower && t0 <= ray.r.upper) {
			ray.r.upper = t0;
			if (intersection != nullptr) {
				intersection->setRay(ray);
				cast(this_)->geometry(ray(t0), &intersection->geometry);
			}
			return true;
		}
		if (t1 >= ray.r.lower && t1 <= ray.r.upper) {
			ray.r.upper = t1;
			if (intersection != nullptr) {
				intersection->setRay(ray);
				cast(this_)->geometry(ray(t1), &intersection->geometry);
			}
			return true;
		}
		return false;
	}

	__host__ __device__
	static void sample_area_(const Shape* this_, const float time, float rnd[2], Geometry* geometry) {
		UniformSphereDistribution::sample(rnd, &geometry->gp[0], nullptr);
		geometry->gp *= cast(this_)->radius_;
		geometry->gp += cast(this_)->center_;
		geometry->gn = geometry->sn = Norml3F{(geometry->gp - cast(this_)->center_) / cast(this_)->radius_};
		geometry->gu = geometry->su = Vectr3F{-(geometry->gp[1] - cast(this_)->center_[1]), +(geometry->gp[0] - cast(this_)->center_[0]), 0.f}.normalize();
		cast(this_)->uvCoordinates(geometry);
	}

	/**************************************************************************
	 * Based on: Monte Carlo Techniques for Direct Lighting Calculations
	 * By: Peter Shirley, Changyaw Wang and Kurt Zimmerman
	 **************************************************************************/
	__host__ __device__
	static void sample_solid_angle_ (const Shape* this_, const Point3F& ref, const float time, float rnd[2], Geometry* geometry, float* pdf) {
		Vectr3F w  = cast(this_)->center_ - ref;
		const float d = w.length();
		if (d <= cast(this_)->radius_) {
			// reference point inside sphere, sample area uniformly.
			this_->sampleGeometry(time, rnd, geometry);
			*pdf = toSolidAnglePdf(ref, geometry->gp, geometry->gn, 1.f / cast(this_)->area_);
		} else {
			// reference point outside sphere, sample solid angle uniformly.
			const float cosThetaMax = sqrt(max(0.f, 1.f - (cast(this_)->radius_ * cast(this_)->radius_) / (d * d)));
			*pdf = 1.f / (2.f * PI<float>() * (1.f - cosThetaMax));
			const float theta = acos(1.f - rnd[0] + rnd[0] * cosThetaMax);
			const float phi = 2.f * PI<float>() * rnd[1];
			// intersect sampled direction with sphere
			w /= d;
			Vectr3F u, v;
			orthonormalBasis(w, &u, &v);
			const float cosTheta = cos(theta);
			const float sinTheta = sin(theta);
			const float cosPhiSinTheta = cos(phi) * sinTheta;
			const float sinPhiSinTheta = sin(phi) * sinTheta;
			Vectr3F dir(
				u[0] * cosPhiSinTheta + v[0] * sinPhiSinTheta + w[0] * cosTheta,
				u[1] * cosPhiSinTheta + v[1] * sinPhiSinTheta + w[1] * cosTheta,
				u[2] * cosPhiSinTheta + v[2] * sinPhiSinTheta + w[2] * cosTheta
			);
			if (!cast(this_)->intersection(ref, dir, geometry)) {
				*pdf = 0.f;
			}
		}
	}

	/**************************************************************************
	 * Based on: Monte Carlo Techniques for Direct Lighting Calculations
	 * By: Peter Shirley, Changyaw Wang and Kurt Zimmerman
	 **************************************************************************/
	__host__ __device__
	static float sample_pdf_(const Shape* this_, const Point3F& ref, const float time, const Geometry& geometry) {
		const float d = (cast(this_)->center_ - ref).length();
		if (d <= cast(this_)->radius_) {
			// reference point inside sphere, sample area uniformly.
			return toSolidAnglePdf(ref,  geometry.gp,  geometry.gn, 1.f / cast(this_)->area_);
		} else {
			// reference point outside sphere, sample solid angle uniformly.
			const float cosThetaMax = sqrt(max(0.f, 1.f - (cast(this_)->radius_ * cast(this_)->radius_) / (d * d)));
			return 1.f / (2.f * PI<float>() * (1.f - cosThetaMax));
		}
	}

	__host__ __device__
	static bool can_compute_rnd_(const Shape* this_) {
		return true;
	}

	__host__ __device__
	static void compute_rnd_(const Shape* this_, const float time, const Point3F& geometry, float rnd[2]) {
		auto local = (geometry - cast(this_)->center_) / cast(this_)->radius_;
		UniformSphereDistribution::random(&local[0], rnd);
	}

	__host__
	Sphere (
		const Point3F& center,
		const float radius,
		const instant_area_t& instant_area_,
		const intersection_t& intersection_,
		const sample_area_t& sample_area_,
		const sample_solid_angle_t& sample_solid_angle_,
		const sample_pdf_t& sample_pdf_,
		const can_compute_rnd_t& can_compute_rnd_,
		const compute_rnd_t& compute_rnd_
	):
		TransformableShape(instant_area_, intersection_, sample_area_, sample_solid_angle_, sample_pdf_, can_compute_rnd_, compute_rnd_),
		center_(center), radius_(radius), area_(radius * radius * 4.f * PI<float>()) {
	}

public:
	friend class TypedPool<Sphere>;

	/*!
	 * \brief
	 * Creates a new Sphere.
	 *
	 * \param[in] pool The memory pool used to create new objects.
	 * \param[in] center The center point.
	 * \param[in] radius The radius.
	 */
	__host__
	static Pointer<Sphere> build(MemoryPool& pool, const Point3F& center, const float radius) {
		return Pointer<Sphere>::make(
			pool, center, radius,
			instant_area_t(virtualize(instant_area_)),
			intersection_t(virtualize(intersection_)),
			sample_area_t(virtualize(sample_area_)),
			sample_solid_angle_t(virtualize(sample_solid_angle_)),
			sample_pdf_t(virtualize(sample_pdf_)),
			can_compute_rnd_t(virtualize(can_compute_rnd_)),
			compute_rnd_t(virtualize(compute_rnd_))
		);
	}

	__host__
	void decompose(MemoryPool& pool, std::vector<Pointer<const Shape>>& list, const Pointer<const Shape>& self) const override {
		list.push_back(self);
	}

	__host__
	Box boundary(const RangeF& time) const override {
		Vectr3F r = Vectr3F(radius_, radius_, radius_);
		return Box(center_ - r, center_ + r);
	}

	__host__
	float cost() const override {
		return 5.f;
	}

	__host__
	float area() const override {
		return area_;
	}
};

}

#endif /* SPHERE_CUH_ */

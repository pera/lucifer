#ifndef BUMPMAPPEDSHAPE_CUH_
#define BUMPMAPPEDSHAPE_CUH_

#include "TransformableShape.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class BumpMappedShape: public TransformableShape {
private:
	const Pointer<const ShadingNode<float>> bumpMap;
	const Pointer<const Shape> shape;
	const float delta;

	__host__ __device__
	void bump(Geometry& g, const float time) const {
		Vectr3F g_sv = cross<Norml3F, Vectr3F, Vectr3F>(g.sn, g.su).normalize();
		float value =  (*bumpMap)(g.gp, g.su, g.sn, g.uv, 0.f, time);
		float db_du = ((*bumpMap)(g.gp + g.su * delta, g.su, g.sn, g.uv + Point2F(delta, 0), 0.f, time) - value) / delta;
		float db_dv = ((*bumpMap)(g.gp + g_sv * delta, g.su, g.sn, g.uv + Point2F(0, delta), 0.f, time) - value) / delta;
		g.sn = (g.sn + Norml3F(g.su * db_du + g_sv * db_dv)).normalize();
		g.su = cross<Vectr3F, Norml3F, Vectr3F>(g_sv, g.sn).normalize();
		if (dot(g.gn, g.sn) < 0.f) {
			g.gn = -(g.gn);
		}
	}

	__host__ __device__
	static const BumpMappedShape* cast(const Shape* this_) {
		return static_cast<const BumpMappedShape*>(this_);
	}

	__host__ __device__
	static float instant_area_(const Shape* this_, const float time) {
		return cast(this_)->shape->area(time);
	}

	__host__ __device__
	static bool intersection_(const Shape* this_, Ray& ray, Intersection* intersection) {
		if (!cast(this_)->shape->intersection(ray, intersection)) {
			return false;
		}
		if (intersection != nullptr) {
			cast(this_)->bump(intersection->geometry, ray.t);
		}
		return true;
	}

	__host__ __device__
	static void sample_area_(const Shape* this_, const float time, float rnd[2], Geometry* geometry) {
		cast(this_)->shape->sampleGeometry(time, rnd, geometry);
		cast(this_)->bump(*geometry, time);
	}

	__host__ __device__
	static void sample_solid_angle_ (const Shape* this_, const Point3F& ref, const float time, float rnd[2], Geometry* geometry, float* pdf) {
		cast(this_)->shape->sampleGeometry(ref, time, rnd, geometry, pdf);
		cast(this_)->bump(*geometry, time);
	}

	__host__ __device__
	static float sample_pdf_(const Shape* this_, const Point3F& ref, const float time, const Geometry& geometry) {
		return cast(this_)->shape->sampleGeometryPDF(ref, time, geometry);
	}

	__host__ __device__
	static bool can_compute_rnd_(const Shape* this_) {
		return cast(this_)->shape->canComputeRND();
	}

	__host__ __device__
	static void compute_rnd_(const Shape* this_, const float time, const Point3F& geometry, float rnd[2]) {
		cast(this_)->shape->rnd(time, geometry, rnd);
	}

	__host__
	BumpMappedShape(
		const Pointer<const ShadingNode<float>>& bumpMap,
		const Pointer<const Shape>& shape,
		const float delta,
		const instant_area_t& instant_area_,
		const intersection_t& intersection_,
		const sample_area_t& sample_area_,
		const sample_solid_angle_t& sample_solid_angle_,
		const sample_pdf_t& sample_pdf_,
		const can_compute_rnd_t& can_compute_rnd_,
		const compute_rnd_t& compute_rnd_
	):
		TransformableShape(instant_area_, intersection_, sample_area_, sample_solid_angle_, sample_pdf_, can_compute_rnd_, compute_rnd_),
		bumpMap(bumpMap), shape(shape), delta(delta) {
	}

public:
	friend class TypedPool<BumpMappedShape>;

	/*!
	 * \brief
	 * Creates a new bump-mapped shape
	 *
	 * \param[in] pool The memory pool used to create new objects.
	 * \param[in] bumpMap The bump map.
	 * \param[in] shape The original shape.
	 * \param[in] delta The delta value to compute the bump map differential.
	 */
	__host__
	static Pointer<BumpMappedShape> build(
		MemoryPool& pool,
		const Pointer<const ShadingNode<float>>& bumpMap,
		const Pointer<const Shape>& shape,
		const float delta)
	{
		return Pointer<BumpMappedShape>::make(
			pool, bumpMap, shape, delta,
			instant_area_t(virtualize(instant_area_)),
			intersection_t(virtualize(intersection_)),
			sample_area_t(virtualize(sample_area_)),
			sample_solid_angle_t(virtualize(sample_solid_angle_)),
			sample_pdf_t(virtualize(sample_pdf_)),
			can_compute_rnd_t(virtualize(can_compute_rnd_)),
			compute_rnd_t(virtualize(compute_rnd_))
		);
	}

	__host__
	void decompose(MemoryPool& pool, std::vector<Pointer<const Shape>>& list, const Pointer<const Shape>& self) const override {
		std::vector<Pointer<const Shape>> tmp;
		shape->decompose(pool, tmp, shape);
		for (const auto& child : tmp) {
			list.push_back(Pointer<const Shape>(BumpMappedShape::build(pool, bumpMap, child, delta)));
		}
	}

	__host__
	Box boundary(const RangeF& time) const override {
		return shape->boundary(time);
	}

	__host__
	float cost() const override {
		return shape->cost();
	}

	__host__
	float area() const override {
		return shape->area();
	}
};

}

#endif /* BUMPMAPPEDSHAPE_CUH_ */

#ifndef TRANSFORMEDSHAPE_CUH_
#define TRANSFORMEDSHAPE_CUH_

#include "Shape.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * A transformed Shape.
 *
 * \author
 * Bruno Pera.
 */
class TransformedShape: public Shape {
private:
	const Transformation3F shapeToWorld_;
	const Pointer<const Shape> shape_;
	const float scaleE2;

	__host__
	static float computeEscaleE2(const Transformation3F& t) {
		auto f = scale(t);
		auto sE2 = abs(f[0] + f[1] + f[2]) / 3.f;
		return sE2 *= sE2;
	}

	__host__ __device__
	static const TransformedShape* cast(const Shape* this_) {
		return static_cast<const TransformedShape*>(this_);
	}

	__host__ __device__
	static float instant_area_(const Shape* this_, const float time) {
		return cast(this_)->scaleE2 * cast(this_)->shape_->area(time);
	}

	__host__ __device__
	static bool intersection_(const Shape* this_, Ray& ray, Intersection* intersection) {
		Ray local = cast(this_)->shapeToWorld_.inverse() * ray;
		if (cast(this_)->shape_->intersection(local, intersection)) {
			ray.r = local.r;
			if (intersection != nullptr) {
				intersection->transform(cast(this_)->shapeToWorld_);
			}
			return true;
		}
		return false;
	}

	__host__ __device__
	static void sample_area_(const Shape* this_, const float time, float rnd[2], Geometry* geometry) {
		cast(this_)->shape_->sampleGeometry(time, rnd, geometry);
		geometry->apply(cast(this_)->shapeToWorld_);
	}

	__host__ __device__
	static void sample_solid_angle_ (const Shape* this_, const Point3F& ref, const float time, float rnd[2], Geometry* geometry, float* pdf) {
		cast(this_)->shape_->sampleGeometry(cast(this_)->shapeToWorld_.inverse() * ref, time, rnd, geometry, pdf);
		geometry->apply(cast(this_)->shapeToWorld_);
		*pdf /= cast(this_)->scaleE2;
	}

	__host__ __device__
	static float sample_pdf_(const Shape* this_, const Point3F& ref, const float time, const Geometry& geometry) {
		auto o2s = cast(this_)->shapeToWorld_.inverse();
		return cast(this_)->shape_->sampleGeometryPDF(o2s * ref, time, o2s * geometry) / cast(this_)->scaleE2;
	}

	__host__ __device__
	static bool can_compute_rnd_(const Shape* this_) {
		return cast(this_)->shape_->canComputeRND();
	}

	__host__ __device__
	static void compute_rnd_(const Shape* this_, const float time, const Point3F& geometry, float rnd[2]) {
		Point3F local = cast(this_)->shapeToWorld_.inverse() * geometry;
		cast(this_)->shape_->rnd(time, local, rnd);
	}

	__host__
	TransformedShape(
		const Transformation3F& shapeToWorld,
		const Pointer<const Shape>& shape,
		const instant_area_t& instant_area_,
		const intersection_t& intersection_,
		const sample_area_t& sample_area_,
		const sample_solid_angle_t& sample_solid_angle_,
		const sample_pdf_t& sample_pdf_,
		const can_compute_rnd_t& can_compute_rnd_,
		const compute_rnd_t& compute_rnd_
	):
		Shape(instant_area_, intersection_, sample_area_, sample_solid_angle_, sample_pdf_, can_compute_rnd_, compute_rnd_),
		shapeToWorld_(shapeToWorld), shape_(shape), scaleE2(computeEscaleE2(shapeToWorld)) {
	}

public:
	friend class TypedPool<TransformedShape>;

	/*!
	 * \brief
	 * Creates a new transformed instance of a shape.
	 *
	 * \param[in] pool The memory pool used to create new objects.
	 * \param[in] shapeToWorld The shape to world transformation. If the
	 * transformed shape is supposed to be used as a samplable light source
	 * the transformation must have uniform scaling factors, otherwise the
	 * result is undefined.
	 * \param[in] shape The original shape to be transformed.
	 */
	__host__
	static Pointer<TransformedShape> build(MemoryPool& pool, const Transformation3F& shapeToWorld, const Pointer<const Shape>& shape) {
		return Pointer<TransformedShape>::make(
			pool, shapeToWorld, shape,
			instant_area_t(virtualize(instant_area_)),
			intersection_t(virtualize(intersection_)),
			sample_area_t(virtualize(sample_area_)),
			sample_solid_angle_t(virtualize(sample_solid_angle_)),
			sample_pdf_t(virtualize(sample_pdf_)),
			can_compute_rnd_t(virtualize(can_compute_rnd_)),
			compute_rnd_t(virtualize(compute_rnd_))
		);
	}

	__host__
	Pointer<const Shape> transform(MemoryPool& pool, const Transformation3F& transformation, const Pointer<const Shape>& self) const override;

	__host__
	Pointer<const Shape> transform(MemoryPool& pool, const Pointer<const AnimatedTransformation>& transformation, const Pointer<const Shape>& self) const override;

	__host__
	void decompose(MemoryPool& pool, std::vector<Pointer<const Shape>>& list, const Pointer<const Shape>& self) const override {
		std::vector<Pointer<const Shape>> tmp;
		shape_->decompose(pool, tmp, shape_);
		for (const auto& child : tmp) {
			list.push_back(child->transform(pool, shapeToWorld_, child));
		}
	}

	__host__
	Box boundary(const RangeF& time) const override {
		return shapeToWorld_ * shape_->boundary(time);
	}

	__host__
	float cost() const override {
		return shape_->cost();
	}

	__host__
	float area() const override {
		const float area_ = shape_->area();
		return area_ < 0.f ? -1.f : scaleE2 * area_;
	}
};

}

#endif /* TRANSFORMEDSHAPE_CUH_ */

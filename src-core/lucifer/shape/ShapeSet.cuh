#ifndef SHAPESET_CUH_
#define SHAPESET_CUH_

#include <cstdint>
#include <algorithm>
#include "TransformableShape.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/system/Environment.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * A simple composition of Shapes.
 *
 * \author
 * Bruno Pera.
 */
class ShapeSet: public TransformableShape {
public:
	using Shape::sampleGeometry;
	using index_t = uint32_t;
	using array_t = Array<Pointer<const Shape>, index_t>;

private:
	const array_t shapes_;
	const float area_;

private:
	__host__
	static float totalArea(const array_t& shapes) {
		float result = 0.f;
		for (const auto& shape : shapes) {
			const float area_ = shape->area();
			if (area_ < 0.f) return -1.f;
			result += area_;
		}
		return result;
	}

	__host__ __device__
	float totalArea(const float time) const {
		float result = 0.f;
		for (const auto& shape : shapes_) {
			result += shape->area(time);
		}
		return result;
	}

	__host__ __device__
	static const ShapeSet* cast(const Shape* this_) {
		return static_cast<const ShapeSet*>(this_);
	}

	__host__ __device__
	static float instant_area_(const Shape* this_, const float time) {
		return cast(this_)->area_ >= 0.f ? cast(this_)->area_ : cast(this_)->totalArea(time);
	}

	__host__ __device__
	static bool intersection_(const Shape* this_, Ray& ray, Intersection* intersection) {
		bool found = false;
		for (const auto& shape : cast(this_)->shapes_) {
			found |= shape->intersection(ray, intersection);
		}
		return found;
	}

	__host__ __device__
	static void sample_area_(const Shape* this_, const float time, float rnd[2], Geometry* geometry) {
		require(false);
	}

	__host__ __device__
	static void sample_solid_angle_ (const Shape* this_, const Point3F& ref, const float time, float rnd[2], Geometry* geometry, float* pdf) {
		require(false);
	}

	__host__ __device__
	static float sample_pdf_(const Shape* this_, const Point3F& ref, const float time, const Geometry& geometry) {
		require(false);
		return 0.f;
	}

	__host__ __device__
	static bool can_compute_rnd_(const Shape* this_) {
		return false;
	}

	__host__ __device__
	static void compute_rnd_(const Shape* this_, const float time, const Point3F& geometry, float rnd[2]) {
	}

	__host__
	ShapeSet(
		const array_t& shapes,
		const instant_area_t& instant_area_,
		const intersection_t& intersection_,
		const sample_area_t& sample_area_,
		const sample_solid_angle_t& sample_solid_angle_,
		const sample_pdf_t& sample_pdf_,
		const can_compute_rnd_t& can_compute_rnd_,
		const compute_rnd_t& compute_rnd_
	):
		TransformableShape(instant_area_, intersection_, sample_area_, sample_solid_angle_, sample_pdf_, can_compute_rnd_, compute_rnd_),
		shapes_(shapes), area_(totalArea(shapes)) {
	}

public:
	friend class TypedPool<ShapeSet>;

	/*!
	 * \brief
	 * Creates a new simple composition of shapes.
	 *
	 * \param[in] shapes The set of shapes.
	 */
	__host__
	static Pointer<ShapeSet> build(MemoryPool& pool, const array_t& shapes) {
		return Pointer<ShapeSet>::make(
			pool, shapes,
			instant_area_t(virtualize(instant_area_)),
			intersection_t(virtualize(intersection_)),
			sample_area_t(virtualize(sample_area_)),
			sample_solid_angle_t(virtualize(sample_solid_angle_)),
			sample_pdf_t(virtualize(sample_pdf_)),
			can_compute_rnd_t(virtualize(can_compute_rnd_)),
			compute_rnd_t(virtualize(compute_rnd_))
		);
	}

	/*!
	 * \brief
	 * Returns the number of shape in the set.
	 *
	 * \return the number of shape in the set.
	 */
	__host__  __device__
	index_t size() const {
		return shapes_.size();
	}

	/*!
	 * \brief
	 * Returns a read-only reference to the i-th shape in the set.
	 *
	 * \return A read-only reference to the i-th shape in the set.
	 */
	__host__  __device__
	const Pointer<const Shape>& operator[](index_t i) const {
		return shapes_[i];
	}

	__host__
	void decompose(MemoryPool& pool, std::vector<Pointer<const Shape>>& list, const Pointer<const Shape>& self) const override {
		for (const auto& shape : shapes_) {
			shape->decompose(pool, list, shape);
		}
	}

	__host__
	Box boundary(const RangeF& time) const override {
		Box result = shapes_[0]->boundary(time);
		for (index_t i = 1; i < shapes_.size(); ++i) {
			result += shapes_[i]->boundary(time);
		}
		return result;
	}

	__host__
	float cost() const override {
		float c = 0.f;
		for (auto& s : shapes_) c += s->cost();
		return c;
	}

	__host__
	float area() const override {
		return area_;
	}
};

}

#endif /* SHAPESET_CUH_ */

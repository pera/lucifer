#ifndef INVERTEDSHAPE_CUH_
#define INVERTEDSHAPE_CUH_

#include "TransformableShape.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * A shape with inverted normal vector direction.
 *
 * \author
 * Bruno Pera.
 */
class InvertedShape: public TransformableShape {
private:
	const Pointer<const Shape> shape;

	__host__ __device__
	static const InvertedShape* cast(const Shape* this_) {
		return static_cast<const InvertedShape*>(this_);
	}

	__host__ __device__
	static float instant_area_(const Shape* this_, const float time) {
		return cast(this_)->shape->area(time);
	}

	__host__ __device__
	static bool intersection_(const Shape* this_, Ray& ray, Intersection* intersection) {
		bool result = cast(this_)->shape->intersection(ray, intersection);
		if (result && intersection != nullptr) {
			intersection->geometry.gn = -intersection->geometry.gn;
			intersection->geometry.sn = -intersection->geometry.sn;
		}
		return result;
	}

	__host__ __device__
	static void sample_area_(const Shape* this_, const float time, float rnd[2], Geometry* geometry) {
		cast(this_)->shape->sampleGeometry(time, rnd, geometry);
		geometry->gn = -geometry->gn;
		geometry->sn = -geometry->sn;
	}

	__host__ __device__
	static void sample_solid_angle_ (const Shape* this_, const Point3F& ref, const float time, float rnd[2], Geometry* geometry, float* pdf) {
		cast(this_)->shape->sampleGeometry(ref, time, rnd, geometry, pdf);
		geometry->gn = -geometry->gn;
		geometry->sn = -geometry->sn;
	}

	__host__ __device__
	static float sample_pdf_(const Shape* this_, const Point3F& ref, const float time, const Geometry& geometry) {
		return cast(this_)->shape->sampleGeometryPDF(ref, time, geometry);
	}

	__host__ __device__
	static bool can_compute_rnd_(const Shape* this_) {
		return cast(this_)->shape->canComputeRND();
	}

	__host__ __device__
	static void compute_rnd_(const Shape* this_, const float time, const Point3F& geometry, float rnd[2]) {
		cast(this_)->shape->rnd(time, geometry, rnd);
	}

	__host__
	InvertedShape(
		const Pointer<const Shape>& shape,
		const instant_area_t& instant_area_,
		const intersection_t& intersection_,
		const sample_area_t& sample_area_,
		const sample_solid_angle_t& sample_solid_angle_,
		const sample_pdf_t& sample_pdf_,
		const can_compute_rnd_t& can_compute_rnd_,
		const compute_rnd_t& compute_rnd_
	):
		TransformableShape(instant_area_, intersection_, sample_area_, sample_solid_angle_, sample_pdf_, can_compute_rnd_, compute_rnd_), shape(shape) {
	}

public:
	friend class TypedPool<InvertedShape>;

	/*!
	 * \brief
	 * Creates a new inverted instance of a Shape.
	 *
	 * \param[in] pool The memory pool used to create new objects.
	 * \param[in] shape The shape to be inverted.
	 */
	__host__
	static Pointer<InvertedShape> build(MemoryPool& pool, const Pointer<const Shape>& shape) {
		return Pointer<InvertedShape>::make(
			pool, shape,
			instant_area_t(virtualize(instant_area_)),
			intersection_t(virtualize(intersection_)),
			sample_area_t(virtualize(sample_area_)),
			sample_solid_angle_t(virtualize(sample_solid_angle_)),
			sample_pdf_t(virtualize(sample_pdf_)),
			can_compute_rnd_t(virtualize(can_compute_rnd_)),
			compute_rnd_t(virtualize(compute_rnd_))
		);
	}

	__host__
	void decompose(MemoryPool& pool, std::vector<Pointer<const Shape>>& list, const Pointer<const Shape>& self) const override {
		std::vector<Pointer<const Shape>> tmp;
		shape->decompose(pool, tmp, shape);
		for (const auto& child : tmp) {
			list.push_back(Pointer<const Shape>(InvertedShape::build(pool, child)));
		}
	}

	__host__
	Box boundary(const RangeF& time) const override {
		return shape->boundary(time);
	}

	__host__
	float cost() const override {
		return shape->cost();
	}

	__host__
	float area() const override {
		return shape->area();
	}
};

}

#endif /* INVERTEDSHAPE_CUH_ */

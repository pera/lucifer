#ifndef TRANSFORMABLESHAPE_CUH_
#define TRANSFORMABLESHAPE_CUH_

#include "AnimatedShape.cuh"
#include "TransformedShape.cuh"

namespace lucifer {

class TransformableShape: public Shape {
protected:
	using Shape::Shape;

public:
	__host__
	Pointer<const Shape> transform(MemoryPool& pool, const Transformation3F& transformation, const Pointer<const Shape>& self) const override {
		return Pointer<const Shape>(TransformedShape::build(pool, transformation, self));
	}

	__host__
	Pointer<const Shape> transform(MemoryPool& pool, const Pointer<const AnimatedTransformation>& transformation, const Pointer<const Shape>& self) const override {
		return Pointer<const Shape>(AnimatedShape::build(pool, transformation, self));
	}
};

}

#endif /* TRANSFORMABLESHAPE_CUH_ */

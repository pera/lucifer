#ifndef TRIANGLEMESH_CUH_
#define TRIANGLEMESH_CUH_

#include <cassert>
#include <cstdint>
#include <vector>
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/random/UniformTriangleDistribution.cuh"
#include "lucifer/shape/TransformableShape.cuh"
#include "lucifer/system/Environment.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/memory/Polymorphic.cuh"

namespace lucifer {

class Index3 {
public:
	using index_t = uint32_t;
	index_t i[3];

	__host__
	Index3(): i{0, 0, 0} {
	}

	__host__
	Index3(index_t i, index_t j, index_t k): i{i, j, k} {
	}

	__host__ __device__
	index_t operator[](const index_t at) const {
		return i[at];
	}
};

class Mesh {
public:
	std::vector<Index3> vrtx;
	std::vector<Index3> nrml;
	std::vector<Index3> tngt;
	std::vector<Index3> txtr;

	__host__
	void clear() {
		vrtx.clear();
		nrml.clear();
		tngt.clear();
		txtr.clear();
	}
};

class Data {
public:
	std::vector<Point3F> vrtx;
	std::vector<Norml3F> nrml;
	std::vector<Vectr3F> tngt;
	std::vector<Point2F> txtr;

	__host__
	Norml3F faceNrml(const Index3& vIdx) {
		auto u = vrtx[vIdx[1]] - vrtx[vIdx[0]];
		auto v = vrtx[vIdx[2]] - vrtx[vIdx[0]];
		return cross<Vectr3F, Vectr3F, Norml3F>(u, v);
	}

	/*
	 * http://www.terathon.com/code/tangent.html
	 */
	__host__
	Vectr3F faceTngt(const Index3& vIdx, const Index3& tIdx) {
		auto q1 = vrtx[vIdx[1]] - vrtx[vIdx[0]];
		auto q2 = vrtx[vIdx[2]] - vrtx[vIdx[0]];
		auto s1 = txtr[tIdx[1]][0] - txtr[tIdx[0]][0];
		auto t1 = txtr[tIdx[1]][1] - txtr[tIdx[0]][1];
		auto s2 = txtr[tIdx[2]][0] - txtr[tIdx[0]][0];
		auto t2 = txtr[tIdx[2]][1] - txtr[tIdx[0]][1];
		auto x1 = +t2 * q1[0] - t1 * q2[0];
		auto y1 = +t2 * q1[1] - t1 * q2[1];
		auto z1 = +t2 * q1[2] - t1 * q2[2];
		auto x2 = -s2 * q1[0] + s1 * q2[0];
		auto y2 = -s2 * q1[1] + s1 * q2[1];
		auto z2 = -s2 * q1[2] + s1 * q2[2];
		// create tangent vectors
		auto tngt1 = Vectr3F(x1, y1, z1) / (s1 * t2 - s2 * t1);
		auto tngt2 = Vectr3F(x2, y2, z2) / (s1 * t2 - s2 * t1);
		// check handedness
		auto n = faceNrml(vIdx);
		if (dot(cross<Norml3F, Vectr3F, Vectr3F>(n, tngt1), tngt2) < 0.f) {
			tngt1 = -tngt1;
		}
		// ok
		return tngt1;
	}

	__host__
	void clear() {
		vrtx.clear();
		nrml.clear();
		tngt.clear();
		txtr.clear();
	}

	__host__
	void estimateNrmls(std::vector<Mesh>& meshes, std::vector<Norml3F>* nrml = nullptr) {
		// clear normals
		assert(vrtx.size() >= 3);
		if (nrml == nullptr) nrml = &this->nrml;
		nrml->clear();
		nrml->reserve(vrtx.size());
		for (Index3::index_t i = 0; i < vrtx.size(); ++i) {
			nrml->emplace_back(0.f, 0.f, 0.f);
		}
		// accumulate face normals
		for (auto& m: meshes) {
			if (nrml == &this->nrml) m.nrml = m.vrtx;
			for (auto& v: m.vrtx) {
				auto n = faceNrml(v);
				(*nrml)[v[0]] += n * acos(absDot((vrtx[v[1]] - vrtx[v[0]]).normalize(), (vrtx[v[2]] - vrtx[v[0]]).normalize()));
				(*nrml)[v[1]] += n * acos(absDot((vrtx[v[2]] - vrtx[v[1]]).normalize(), (vrtx[v[0]] - vrtx[v[1]]).normalize()));
				(*nrml)[v[2]] += n * acos(absDot((vrtx[v[0]] - vrtx[v[2]]).normalize(), (vrtx[v[1]] - vrtx[v[2]]).normalize()));
			}
		}
		// normalize results
		for (auto& n : *nrml) {
			n.normalize();
		}
	}

	__host__
	void estimateTngts(std::vector<Mesh>& meshes, std::vector<Vectr3F>* tngt = nullptr) {
		// clear tangents
		assert(vrtx.size() >= 3);
		if (tngt == nullptr) tngt = &this->tngt;
		tngt->clear();
		tngt->reserve(vrtx.size());
		for (Index3::index_t i = 0; i < vrtx.size(); ++i) {
			tngt->emplace_back(0.f, 0.f, 0.f);
		}
		// accumulate face tangents
		for (auto& m: meshes) {
			assert(m.vrtx.size() == m.txtr.size());
			if (tngt == &this->tngt) m.tngt = m.vrtx;
			for (Index3::index_t i = 0; i < m.vrtx.size(); ++i) {
				const Index3& v = m.vrtx[i];
				const Index3& t = m.txtr[i];
				Vectr3F t1 = faceTngt(v, t);
				(*tngt)[v[0]] += t1 * acos(absDot((vrtx[v[1]] - vrtx[v[0]]).normalize(), (vrtx[v[2]] - vrtx[v[0]]).normalize()));
				(*tngt)[v[1]] += t1 * acos(absDot((vrtx[v[2]] - vrtx[v[1]]).normalize(), (vrtx[v[0]] - vrtx[v[1]]).normalize()));
				(*tngt)[v[2]] += t1 * acos(absDot((vrtx[v[0]] - vrtx[v[2]]).normalize(), (vrtx[v[1]] - vrtx[v[2]]).normalize()));
			}
		}
		// normalize results
		for (auto& t : *tngt) {
			t.normalize();
		}
	}

	__host__
	void dropNrmls(std::vector<Mesh>& meshes) {
		nrml.clear();
		for(auto& mesh: meshes) mesh.nrml.clear();
	}

	__host__
	void dropTngts(std::vector<Mesh>& meshes) {
		tngt.clear();
		for(auto& mesh: meshes) mesh.tngt.clear();
	}

	__host__
	void displace(const Pointer<const ShadingNode<float>>& map, std::vector<Mesh>& meshes) {
		// estimate nrml per vertex
		std::vector<Norml3F> nrml;
		estimateNrmls(meshes, &nrml);
		// average txtr coordinates
		std::vector<Point2F> txtr;
		std::vector<int32_t> size;
		bool hasTxtrPerVertex = this->txtr.size() > 0;
		if (hasTxtrPerVertex){
			txtr.reserve(vrtx.size());
			size.reserve(vrtx.size());
			for(Index3::index_t i = 0; i < vrtx.size(); ++i) {
				txtr.emplace_back(0.f, 0.f);
				size.emplace_back(0);
			}
			for (auto& mesh: meshes) {
				require(mesh.vrtx.size() == mesh.txtr.size());
				for (Index3::index_t i = 0; i < mesh.vrtx.size(); ++i) {
					Index3& vIdx = mesh.vrtx[i];
					Index3& tIdx = mesh.txtr[i];
					txtr[vIdx[0]] += this->txtr[tIdx[0]];
					txtr[vIdx[1]] += this->txtr[tIdx[1]];
					txtr[vIdx[2]] += this->txtr[tIdx[2]];
					size[vIdx[0]] += 1;
					size[vIdx[1]] += 1;
					size[vIdx[2]] += 1;
				}
			}
			for(Index3::index_t i = 0; i < txtr.size(); ++i) {
				txtr[i] /= size[i];
			}
		}
		// average tanget vectors
		std::vector<Vectr3F> tngt;
		if (this->tngt.size() > 0) {
			tngt.reserve(vrtx.size());
			for(Index3::index_t i = 0; i < vrtx.size(); ++i) {
				tngt.emplace_back(0.f, 0.f, 0.f);
			}
			for (auto& mesh: meshes) {
				require(mesh.vrtx.size() == mesh.tngt.size());
				for (Index3::index_t i = 0; i < mesh.vrtx.size(); ++i) {
					Index3& vIdx = mesh.vrtx[i];
					Index3& tIdx = mesh.tngt[i];
					tngt[vIdx[0]] += this->tngt[tIdx[0]];
					tngt[vIdx[1]] += this->tngt[tIdx[1]];
					tngt[vIdx[2]] += this->tngt[tIdx[2]];
				}
			}
			for(Index3::index_t i = 0; i < txtr.size(); ++i) {
				tngt[i].normalize();
			}
		} else if (hasTxtrPerVertex) {
			estimateTngts(meshes, &tngt);
		}
		// displace
		float wlen = 0.f;
		float time = 0.f;
		bool hasTngtPerVertex = tngt.size() > 0;
		for (Index3::index_t i = 0; i < vrtx.size(); ++i) {
			Point3F p = vrtx[i];
			Norml3F n = nrml[i];
			Vectr3F t = hasTngtPerVertex ? tngt[i] : Vectr3F();
			Point2F u = hasTxtrPerVertex ? txtr[i] : Point2F();
			vrtx[i] += nrml[i] * (*map)(p, t, n, u, wlen, time);
		}
	}
};

class MeshVrtxData: public Polymorphic {
private:
	using index_t = Index3::index_t;
	using vrtx_array_t = Array<Point3F, index_t>;
	using nrml_array_t = Array<Norml3F, index_t>;
	using tngt_array_t = Array<Vectr3F, index_t>;
	using txtr_array_t = Array<Point2F, index_t>;

private:
	vrtx_array_t vrtx;
	nrml_array_t nrml;
	tngt_array_t tngt;
	txtr_array_t txtr;

	__host__
	MeshVrtxData(MemoryPool& pool, const Data& data):
		vrtx(pool, data.vrtx.size(), data.vrtx.data()),
		nrml(pool, data.nrml.size(), data.nrml.data()),
		tngt(pool, data.tngt.size(), data.tngt.data()),
		txtr(pool, data.txtr.size(), data.txtr.data())
	{
		require(vrtx.size() >= 3);
	}

public:
	friend class TypedPool<MeshVrtxData>;

	__host__
	static Pointer<MeshVrtxData> build(MemoryPool& pool, const Data& data) {
		return Pointer<MeshVrtxData>::make(pool, pool, data);
	}

	__host__ __device__
	const Point3F& getVrtx(const index_t i) const {
		return vrtx[i];
	}

	__host__ __device__
	const Norml3F& getNrml(const index_t i) const {
		return nrml[i];
	}

	__host__ __device__
	const Vectr3F& getTngt(const index_t i) const {
		return tngt[i];
	}

	__host__ __device__
	const Point2F& getTxtr(const index_t i) const {
		return txtr[i];
	}
};

class MeshFaceData: public Polymorphic {
private:
	using index_t = Index3::index_t;
	using array_t = Array<Index3, index_t>;

	const Pointer<const MeshVrtxData> data;
	const array_t vrtx;
	const array_t nrml;
	const array_t tngt;
	const array_t txtr;

	__host__
	MeshFaceData(MemoryPool& pool, const Pointer<const MeshVrtxData>& data, const Mesh& mesh):
		data(data),
		vrtx(pool, mesh.vrtx.size(), mesh.vrtx.data()),
		nrml(pool, mesh.nrml.size(), mesh.nrml.data()),
		tngt(pool, mesh.tngt.size(), mesh.tngt.data()),
		txtr(pool, mesh.txtr.size(), mesh.txtr.data())
	{
		require(vrtx.size() >= 1);
		require(nrml.size() == 0 || nrml.size() == vrtx.size());
		require(tngt.size() == 0 || tngt.size() == vrtx.size());
		require(txtr.size() == 0 || txtr.size() == vrtx.size());
	}

public:
	friend class TypedPool<MeshFaceData>;

	__host__
	static Pointer<MeshFaceData> build(MemoryPool& pool, const Pointer<const MeshVrtxData>& data, const Mesh& mesh) {
		return Pointer<MeshFaceData>::make(pool, pool, data, mesh);
	}

	__host__
	index_t size() const {
		return vrtx.size();
	}

	__host__ __device__
	bool hasNrmlPerVertex() const {
		return nrml.size() > 0;
	}

	__host__ __device__
	bool hasTngtPerVertex() const {
		return tngt.size() > 0;
	}

	__host__ __device__
	bool hasTxtrPerVertex() const {
		return txtr.size() > 0;
	}

	__host__ __device__
	const Point3F& faceVertex(const index_t f, const index_t v) const {
		assert(v <= 3);
		return data->getVrtx(vrtx[f][v]);
	}

	__host__ __device__
	const Vectr3F faceEdge_0(const index_t f) const {
		return faceVertex(f, 1) - faceVertex(f, 0);
	}

	__host__ __device__
	const Vectr3F faceEdge_1(const index_t f) const {
		return faceVertex(f, 2) - faceVertex(f, 0);
	}

	__host__ __device__
	const Norml3F faceNormal(const index_t f) const {
		return cross<Vectr3F, Vectr3F, Norml3F>(faceEdge_0(f), faceEdge_1(f)).normalize();
	}

	__host__ __device__
	float faceArea(const index_t f) const {
		return triangleArea(faceVertex(f, 0), faceVertex(f, 1), faceVertex(f, 2));
	}

	__host__ __device__
	const Norml3F computeNrml(const index_t f, const float b0, const float b1, const float b2) const {
		return
			data->getNrml(nrml[f][0]) * b0 +
			data->getNrml(nrml[f][1]) * b1 +
			data->getNrml(nrml[f][2]) * b2;
	}

	__host__ __device__
	const Vectr3F computeTngt(const index_t f, const float b0, const float b1, const float b2) const {
		return
			data->getTngt(tngt[f][0]) * b0 +
			data->getTngt(tngt[f][1]) * b1 +
			data->getTngt(tngt[f][2]) * b2;
	}

	__host__ __device__
	const Point2F computeTxtr(const index_t f, const float b0, const float b1, const float b2) const {
		if (!hasTxtrPerVertex()) {
			return Point2F(b0, b1);
		}
		return
			data->getTxtr(txtr[f][0]) * b0 +
			data->getTxtr(txtr[f][1]) * b1 +
			data->getTxtr(txtr[f][2]) * b2;
	}
};

class MTriangle: public TransformableShape {
private:
	using index_t = Index3::index_t;
	const Pointer<const MeshFaceData> data_;
	const index_t i_;
	const float area_;

	__host__ __device__
	const Point3F& vertex(const index_t v) const {
		return data_->faceVertex(i_, v);
	}

	__host__ __device__
	const Vectr3F faceEdge_0() const {
		return data_->faceEdge_0(i_);
	}

	__host__ __device__
	const Vectr3F faceEdge_1() const {
		return data_->faceEdge_1(i_);
	}

	__host__ __device__
	float faceArea() const {
		return data_->faceArea(i_);
	}

	__host__ __device__
	const Norml3F faceNormal() const {
		return data_->faceNormal(i_);
	}

	__host__ __device__
	const Norml3F computeNrml(const float b0, const float b1, const float b2) const {
		return data_->computeNrml(i_, b0, b1, b2);
	}

	__host__ __device__
	const Vectr3F computeTngt(const float b0, const float b1, const float b2) const {
		return data_->computeTngt(i_, b0, b1, b2);
	}

	__host__ __device__
	const Point2F computeTxtr(const float b0, const float b1, const float b2) const {
		return data_->computeTxtr(i_, b0, b1, b2);
	}

	__host__ __device__
	void computeShadingGeometry(const float b0, const float b1, const float b2, Geometry* geometry) const {
		// shading data
		geometry->uv = computeTxtr(b0, b1, b2);
		if (!data_->hasNrmlPerVertex() && !data_->hasTngtPerVertex()) {
			// no per vertex shading geometry
			geometry->sn = geometry->gn;
			geometry->su = geometry->gu;
			return;
		}
		// per vertex shading geometry
		geometry->sn = data_->hasNrmlPerVertex() ? computeNrml(b0, b1, b2) : geometry->gn;
		geometry->su = data_->hasTngtPerVertex() ? computeTngt(b0, b1, b2) : geometry->gu;
		if (isnan(geometry->sn) || isinf(geometry->sn)) {
			geometry->sn = geometry->gn;
		}
		if (isnan(geometry->su) || isinf(geometry->su)) {
			geometry->su = geometry->gu;
		}
		geometry->su  = geometry->su * dot(geometry->sn, geometry->sn) - geometry->sn * dot(geometry->sn, geometry->su);
		if (dot(geometry->gn, geometry->sn) < 0.f) {
			geometry->gn = -(geometry->gn);
		}
		// normalize shading geometry
		geometry->sn.normalize();
		geometry->su.normalize();
		assert(absDot(geometry->sn, geometry->su) < 1E-3f);
	}

	__host__ __device__
	static const MTriangle* cast(const Shape* shape) {
		return static_cast<const MTriangle*>(shape);
	}

	__host__ __device__
	static float instant_area_(const Shape* shape, const float time) {
		return cast(shape)->area_;
	}

	__host__ __device__
	static bool intersection_(const Shape* shape, Ray& ray, Intersection* intersection) {
		auto this_ = cast(shape);
		Vectr3F u = this_->faceEdge_0();
		Vectr3F v = this_->faceEdge_1();
		Vectr3F s = cross(ray.d, v);
		float factor = dot(s, u);
		if (factor == 0.f) {
		  return false;
		}
		factor = 1.f / factor;
		// Compute first barycentric coordinate
		Vectr3F d = ray.o - this_->vertex(0);
		float b1 = dot(d, s) * factor;
		if ((b1 < 0.f) || (b1 > 1.f)) {
		  return false;
		}
		// Compute second barycentric coordinate
		s = cross(d, u);
		float b2 = dot(ray.d, s) * factor;
		if ((b2 < 0.f) || ((b1 + b2) > 1.f)) {
		  return false;
		}
		// Compute distance to ray's origin
		float distance = dot(v, s) * factor;
		if ((distance < ray.r.lower) || (distance > ray.r.upper)) {
		  return false;
		}
		ray.r.upper = distance;
		// define intersection geometry
		if (intersection != nullptr) {
			intersection->setRay(ray);
			intersection->geometry.gp = ray(distance);
			intersection->geometry.gn = this_->faceNormal();
			intersection->geometry.gu = u / u.length();
			this_->computeShadingGeometry(1.f - (b1 + b2), b1, b2, &(intersection->geometry));
		}
		// ok
		return true;
	}

	__host__ __device__
	static void sample_area_(const Shape* shape, const float time, float rnd[2], Geometry* geometry) {
		auto this_ = cast(shape);
		Vectr3F u = this_->faceEdge_0();
		Vectr3F v = this_->faceEdge_1();
		// geometric data
		float b0, b1;
		UniformTriangleDistribution::sample(this_->vertex(0), u, v, rnd, &geometry->gp[0], &b0, &b1);
		geometry->gn = this_->faceNormal();
		geometry->gu = u / u.length();
		this_->computeShadingGeometry(b0, b1, 1.f - (b0 + b1), geometry);
	}

	__host__ __device__
	static bool can_compute_rnd_(const Shape* shape) {
		return true;
	}

	__host__ __device__
	static void compute_rnd_(const Shape* shape, const float time, const Point3F& geometry, float rnd[2]) {
		auto this_ = cast(shape);
		UniformTriangleDistribution::random(
			this_->vertex(0),
			this_->vertex(1),
			this_->vertex(2),
			this_->faceNormal(),
			geometry,
			rnd
		);
	}

	__host__
	MTriangle(
		const Pointer<const MeshFaceData>& data,
		const index_t i,
		const instant_area_t& instant_area_,
		const intersection_t& intersection_,
		const sample_area_t& sample_area_,
		const sample_solid_angle_t& sample_solid_angle_,
		const sample_pdf_t& sample_pdf_,
		const can_compute_rnd_t& can_compute_rnd_,
		const compute_rnd_t& compute_rnd_
	):
		TransformableShape(instant_area_, intersection_, sample_area_, sample_solid_angle_, sample_pdf_, can_compute_rnd_, compute_rnd_),
		data_(data), i_(i), area_(faceArea()) {
	}

public:
	friend class TypedPool<MTriangle>;

	__host__
	static Pointer<MTriangle> build(MemoryPool& pool, const Pointer<const MeshFaceData>& data, const index_t i) {
		return Pointer<MTriangle>::make(
			pool, data, i,
			instant_area_t(virtualize(instant_area_)),
			intersection_t(virtualize(intersection_)),
			sample_area_t(virtualize(sample_area_)),
			sample_solid_angle_t(virtualize(default_sample_solid_angle_)),
			sample_pdf_t(virtualize(default_sample_pdf_)),
			can_compute_rnd_t(virtualize(can_compute_rnd_)),
			compute_rnd_t(virtualize(compute_rnd_))
		);
	}

	__host__
	void decompose(MemoryPool& pool, std::vector<Pointer<const Shape>>& list, const Pointer<const Shape>& self) const override {
		list.push_back(self);
	}

	__host__
	Box boundary(const RangeF& time) const override {
		Box result;
		result += vertex(0);
		result += vertex(1);
		result += vertex(2);
		return result;
	}

	__host__
	float cost() const override {
		return 2.75f;
	}

	__host__
	float area() const override {
		return area_;
	}
};

__host__
inline void buildTriangleMesh(
	MemoryPool& pool,
	Pointer<const MeshFaceData>& mesh,
	std::vector<Pointer<const Shape>>& triangles)
{
	for (Index3::index_t i = 0; i < mesh->size(); ++i) {
		triangles.push_back(Pointer<const Shape>(MTriangle::build(pool, mesh, i)));
	}
}

__host__
inline void buildTriangleMesh(
	MemoryPool& pool,
	const Array<Pointer<const MeshFaceData>>& data,
	std::vector<Pointer<const Shape>>& triangles)
{
	for (const auto& mesh : data) {
		if (!mesh.isNull()) {
			for (Index3::index_t i = 0; i < mesh->size(); ++i) {
				triangles.push_back(Pointer<const Shape>(MTriangle::build(pool, mesh, i)));
			}
		}
	}
}

}

#endif /* TRIANGLEMESH_CUH_ */

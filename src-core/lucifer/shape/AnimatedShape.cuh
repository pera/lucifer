#ifndef ANIMATEDSHAPE_CUH_
#define ANIMATEDSHAPE_CUH_

#include "Shape.cuh"
#include "lucifer/math/geometry/AnimatedTransformation.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * A Shape transformed by an AnimatedTransformation.
 *
 * \author
 * Bruno Pera.
 */
class AnimatedShape: public Shape {
private:
	const Pointer<const AnimatedTransformation> shapeToWorld_;
	const Pointer<const Shape> shape_;

	__host__ __device__
	static const AnimatedShape* cast(const Shape* this_) {
		return static_cast<const AnimatedShape*>(this_);
	}

	__host__ __device__
	static float instant_area_(const Shape* this_, const float time) {
		auto s = scaleE2(cast(this_)->shapeToWorld_->atTime(time));
		return cast(this_)->shape_->area(time) * (s[0] + s[1] + s[2]) / 3.f;
	}

	__host__ __device__
	static bool intersection_(const Shape* this_, Ray& ray, Intersection* intersection) {
		auto shapeToWorld = cast(this_)->shapeToWorld_->atTime(ray.t);
		auto local = shapeToWorld.inverse() * ray;
		if (cast(this_)->shape_->intersection(local, intersection)) {
			ray.r = local.r;
			if (intersection != nullptr) {
				intersection->transform(shapeToWorld);
			}
			return true;
		}
		return false;
	}

	__host__ __device__
	static void sample_area_(const Shape* this_, const float time, float rnd[2], Geometry* geometry) {
		cast(this_)->shape_->sampleGeometry(time, rnd, geometry);
		geometry->apply(cast(this_)->shapeToWorld_->atTime(time));
	}

	__host__ __device__
	static void sample_solid_angle_ (const Shape* this_, const Point3F& ref, const float time, float rnd[2], Geometry* geometry, float* pdf) {
		const auto t = cast(this_)->shapeToWorld_->atTime(time);
		const auto s = scaleE2(t);
		cast(this_)->shape_->sampleGeometry(t.inverse() * ref, time, rnd, geometry, pdf);
		geometry->apply(t);
		*pdf *= 3.f / (s[0] + s[1] + s[2]);
	}

	__host__ __device__
	static float sample_pdf_(const Shape* this_, const Point3F& ref, const float time, const Geometry& geometry) {
		const auto t = cast(this_)->shapeToWorld_->atTime(time);
		const auto p = cast(this_)->shape_->sampleGeometryPDF(t.inverse() * ref, time, t.inverse() * geometry);
		const auto s = scaleE2(t);
		return p * 3.f / (s[0] + s[1] + s[2]);
	}

	__host__ __device__
	static bool can_compute_rnd_(const Shape* this_) {
		return cast(this_)->shape_->canComputeRND();
	}

	__host__ __device__
	static void compute_rnd_(const Shape* this_, const float time, const Point3F& geometry, float rnd[2]) {
		auto shapeToWorld = cast(this_)->shapeToWorld_->atTime(time);
		Point3F local = shapeToWorld.inverse() * geometry;
		cast(this_)->shape_->rnd(time, local, rnd);
	}

	__host__
	AnimatedShape(
		const Pointer<const AnimatedTransformation>& shapeToWorld,
		const Pointer<const Shape>& shape,
		const instant_area_t& instant_area_,
		const intersection_t& intersection_,
		const sample_area_t& sample_area_,
		const sample_solid_angle_t& sample_solid_angle_,
		const sample_pdf_t& sample_pdf_,
		const can_compute_rnd_t& can_compute_rnd_,
		const compute_rnd_t& compute_rnd_
	):
		Shape(instant_area_, intersection_, sample_area_, sample_solid_angle_, sample_pdf_, can_compute_rnd_, compute_rnd_),
		shapeToWorld_(shapeToWorld), shape_(shape) {
	}

public:
	friend class TypedPool<AnimatedShape>;

	/*!
	 * \brief
	 * Creates a new AnimatedShape.
	 *
	 * \param[in] pool The memory pool used to create new objects.
	 * \param[in] shapeToWorld The shape-to-world AnimatedTransformation.
	 * \param[in] shape The Shape to be animated.
	 */
	__host__
	static Pointer<AnimatedShape> build(
		MemoryPool& pool,
		const Pointer<const AnimatedTransformation>& shapeToWorld,
		const Pointer<const Shape>& shape)
	{
		return Pointer<AnimatedShape>::make(
			pool, shapeToWorld, shape,
			instant_area_t(virtualize(instant_area_)),
			intersection_t(virtualize(intersection_)),
			sample_area_t(virtualize(sample_area_)),
			sample_solid_angle_t(virtualize(sample_solid_angle_)),
			sample_pdf_t(virtualize(sample_pdf_)),
			can_compute_rnd_t(virtualize(can_compute_rnd_)),
			compute_rnd_t(virtualize(compute_rnd_))
		);
	}

	__host__
	Pointer<const Shape> transform(MemoryPool& pool, const Transformation3F& transformation, const Pointer<const Shape>& self) const override;

	__host__
	Pointer<const Shape> transform(MemoryPool& pool, const Pointer<const AnimatedTransformation>& transformation, const Pointer<const Shape>& self) const override;

	__host__
	void decompose(MemoryPool& pool, std::vector<Pointer<const Shape>>& list, const Pointer<const Shape>& self) const override {
		std::vector<Pointer<const Shape>> tmp;
		shape_->decompose(pool, tmp, shape_);
		for (const auto& child : tmp) {
			list.push_back(AnimatedShape::build(pool, shapeToWorld_, child));
		}
	}

	__host__
	Box boundary(const RangeF& time) const override {
		return shapeToWorld_->atTime(time) * shape_->boundary(time);
	}

	__host__
	float cost() const override {
		return shape_->cost();
	}

	__host__
	float area() const override {
		const float area_ = shape_->area();
		if (area_ < 0.f or !shapeToWorld_->hasConstantScale()) return -1.f;
		auto s = scaleE2(shapeToWorld_->atTime(0.f));
		return area_ * (s[0] + s[1] + s[2]) / 3.f;
	}
};

}

#endif /* ANIMATEDSHAPE_CUH_ */

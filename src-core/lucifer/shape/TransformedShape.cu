#include "AnimatedShape.cuh"
#include "TransformedShape.cuh"

namespace lucifer {

__host__
Pointer<const Shape> TransformedShape::transform(MemoryPool& pool, const Transformation3F& transformation, const Pointer<const Shape>& self) const {
	assert(this == self.get());
	return Pointer<const Shape>(TransformedShape::build(pool, transformation * shapeToWorld_, shape_));
}

__host__
Pointer<const Shape> TransformedShape::transform(MemoryPool& pool, const Pointer<const AnimatedTransformation>& transformation, const Pointer<const Shape>& self) const {
	assert(this == self.get());
	return Pointer<const Shape>(AnimatedShape::build(pool, transformation,  self));
}

}

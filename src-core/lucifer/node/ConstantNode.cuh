#ifndef CONSTANTNODE_CUH_
#define CONSTANTNODE_CUH_

#include "Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename O, typename... I> class ConstantNode: public Node<O, I...> {
private:
	const O value;

	__host__ __device__
	static O scalar_(const Node<O, I...>* node, const I&... args) {
		return static_cast<const ConstantNode<O, I...>*>(node)->value;
	}

	__host__ __device__
	static typename ranged<O>::type ranged_(const Node<O, I...>* node, const typename ranged<I>::type&... args) {
		return typename ranged<O>::type(static_cast<const ConstantNode<O, I...>*>(node)->value);
	}

	__host__
	ConstantNode(
		const O& value,
		const typename Node<O, I...>::scalar_eval_t& scalar,
		const typename Node<O, I...>::ranged_eval_t& ranged
	): Node<O, I...>(scalar, ranged), value(value) {
	}

public:
	friend class TypedPool<ConstantNode>;

	__host__
	static Pointer<ConstantNode<O, I...>> build(MemoryPool& pool, const O& value) {
		return Pointer<ConstantNode<O, I...>>::make(
			pool, value,
			typename Node<O, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<O, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* CONSTANTNODE_CUH_ */

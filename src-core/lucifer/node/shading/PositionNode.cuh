#ifndef POSITIONNODE_CUH_
#define POSITIONNODE_CUH_

#include "ShadingNode.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class PositionNode: public ShadingNode<Point3F> {
private:
	friend class TypedPool<PositionNode>;
	__host__ __device__
	static Point3F scalar_(const ShadingNode<Point3F>* node, const Point3F& p, const Vectr3F& t, const Norml3F& n, const Point2F& u, const float& wlen, const float& time) {
		return p;
	}

	__host__ __device__
	static Point3R ranged_(const ShadingNode<Point3F>* node, const Point3R& p, const Vectr3R& t, const Norml3R& n, const Point2R& u, const RangeF& wlen, const RangeF& time) {
		return p;
	}

	__host__
	PositionNode(
		const typename ShadingNode<Point3F>::scalar_eval_t& scalar,
		const typename ShadingNode<Point3F>::ranged_eval_t& ranged
	): ShadingNode<Point3F>(scalar, ranged) {
	}

public:
	__host__
	static Pointer<PositionNode> build(MemoryPool& pool) {
		return Pointer<PositionNode>::make(
			pool,
			typename ShadingNode<Point3F>::scalar_eval_t(virtualize(scalar_)),
			typename ShadingNode<Point3F>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* POSITIONNODE_CUH_ */

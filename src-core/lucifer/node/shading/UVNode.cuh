#ifndef UVNODE_CUH_
#define UVNODE_CUH_

#include "ShadingNode.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class UVNode: public ShadingNode<Point2F> {
private:
	__host__ __device__
	static Point2F scalar_(const ShadingNode<Point2F>* node, const Point3F& p, const Vectr3F& t, const Norml3F& n, const Point2F& u, const float& wlen, const float& time) {
		return u;
	}

	__host__ __device__
	static Point2R ranged_(const ShadingNode<Point2F>* node, const Point3R& p, const Vectr3R& t, const Norml3R& n, const Point2R& u, const RangeF& wlen, const RangeF& time) {
		return u;
	}

	__host__
	UVNode(
		const typename ShadingNode<Point2F>::scalar_eval_t& scalar,
		const typename ShadingNode<Point2F>::ranged_eval_t& ranged
	): ShadingNode<Point2F>(scalar, ranged) {
	}

public:
	friend class TypedPool<UVNode>;

	__host__
	static Pointer<UVNode> build(MemoryPool& pool) {
		return Pointer<UVNode>::make(
			pool,
			typename ShadingNode<Point2F>::scalar_eval_t(virtualize(scalar_)),
			typename ShadingNode<Point2F>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* UVNODE_CUH_ */

#ifndef WAVELENGTHNODE_CUH_
#define WAVELENGTHNODE_CUH_

#include "ShadingNode.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class WavelengthNode: public ShadingNode<float> {
private:
	friend class TypedPool<WavelengthNode>;
	__host__ __device__
	static float scalar_(const ShadingNode<float>* node, const Point3F& p, const Vectr3F& t, const Norml3F& n, const Point2F& u, const float& wlen, const float& time) {
		return wlen;
	}

	__host__ __device__
	static RangeF ranged_(const ShadingNode<float>* node, const Point3R& p, const Vectr3R& t, const Norml3R& n, const Point2R& u, const RangeF& wlen, const RangeF& time) {
		return wlen;
	}

	__host__
	WavelengthNode(
		const typename ShadingNode<float>::scalar_eval_t& scalar,
		const typename ShadingNode<float>::ranged_eval_t& ranged
	): ShadingNode<float>(scalar, ranged) {
	}

public:
	__host__
	static Pointer<WavelengthNode> build(MemoryPool& pool) {
		return Pointer<WavelengthNode>::make(
			pool,
			typename ShadingNode<float>::scalar_eval_t(virtualize(scalar_)),
			typename ShadingNode<float>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* WAVELENGTHNODE_CUH_ */

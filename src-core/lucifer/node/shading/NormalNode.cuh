#ifndef NORMALNODE_CUH_
#define NORMALNODE_CUH_

#include "ShadingNode.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class NormalNode: public ShadingNode<Norml3F> {
private:
	friend class TypedPool<NormalNode>;
	__host__ __device__
	static Norml3F scalar_(const ShadingNode<Norml3F>* node, const Point3F& p, const Vectr3F& t, const Norml3F& n, const Point2F& u, const float& wlen, const float& time) {
		return n;
	}

	__host__ __device__
	static Norml3R ranged_(const ShadingNode<Norml3F>* node, const Point3R& p, const Vectr3R& t, const Norml3R& n, const Point2R& u, const RangeF& wlen, const RangeF& time) {
		return n;
	}

	__host__
	NormalNode(
		const typename ShadingNode<Norml3F>::scalar_eval_t& scalar,
		const typename ShadingNode<Norml3F>::ranged_eval_t& ranged
	): ShadingNode<Norml3F>(scalar, ranged) {
	}

public:
	__host__
	static Pointer<NormalNode> build(MemoryPool& pool) {
		return Pointer<NormalNode>::make(
			pool,
			typename ShadingNode<Norml3F>::scalar_eval_t(virtualize(scalar_)),
			typename ShadingNode<Norml3F>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* NORMALNODE_CUH_ */

#ifndef SHADINGNODE_CUH_
#define SHADINGNODE_CUH_

#include "../Node.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"

// position, tangent, normal, uv, wavelength, time
#define LUCIFER_SHADING_INPUT Point3F, Vectr3F, Norml3F, Point2F, float, float

namespace lucifer {

template<typename O> using ShadingNode = Node<O, LUCIFER_SHADING_INPUT>;

}

#endif /* SHADINGNODE_CUH_ */

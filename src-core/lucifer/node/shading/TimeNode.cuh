#ifndef TIMENODE_CUH_
#define TIMENODE_CUH_

#include "ShadingNode.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class TimeNode: public ShadingNode<float> {
private:
	friend class TypedPool<TimeNode>;
	__host__ __device__
	static float scalar_(const ShadingNode<float>* node, const Point3F& p, const Vectr3F& t, const Norml3F& n, const Point2F& u, const float& wlen, const float& time) {
		return time;
	}

	__host__ __device__
	static RangeF ranged_(const ShadingNode<float>* node, const Point3R& p, const Vectr3R& t, const Norml3R& n, const Point2R& u, const RangeF& wlen, const RangeF& time) {
		return time;
	}

	__host__
	TimeNode(
		const typename ShadingNode<float>::scalar_eval_t& scalar,
		const typename ShadingNode<float>::ranged_eval_t& ranged
	): ShadingNode<float>(scalar, ranged) {
	}

public:
	__host__
	static Pointer<TimeNode> build(MemoryPool& pool) {
		return Pointer<TimeNode>::make(
			pool,
			typename ShadingNode<float>::scalar_eval_t(virtualize(scalar_)),
			typename ShadingNode<float>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* TIMENODE_CUH_ */

#ifndef TANGENTVECTORNODE_CUH_
#define TANGENTVECTORNODE_CUH_

#include "ShadingNode.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

class TangentVectorNode: public ShadingNode<Vectr3F> {
private:
	friend class TypedPool<TangentVectorNode>;
	__host__ __device__
	static Vectr3F scalar_(const ShadingNode<Vectr3F>* node, const Point3F& p, const Vectr3F& t, const Norml3F& n, const Point2F& u, const float& wlen, const float& time) {
		return t;
	}

	__host__ __device__
	static Vectr3R ranged_(const ShadingNode<Vectr3F>* node, const Point3R& p, const Vectr3R& t, const Norml3R& n, const Point2R& u, const RangeF& wlen, const RangeF& time) {
		return t;
	}

	__host__
	TangentVectorNode(
		const typename ShadingNode<Vectr3F>::scalar_eval_t& scalar,
		const typename ShadingNode<Vectr3F>::ranged_eval_t& ranged
	): ShadingNode<Vectr3F>(scalar, ranged) {
	}

public:
	__host__
	static Pointer<TangentVectorNode> build(MemoryPool& pool) {
		return Pointer<TangentVectorNode>::make(
			pool,
			typename ShadingNode<Vectr3F>::scalar_eval_t(virtualize(scalar_)),
			typename ShadingNode<Vectr3F>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* TANGENTVECTORNODE_CUH_ */

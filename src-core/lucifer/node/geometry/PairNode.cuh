#ifndef PAIRNODE_CUH_
#define PAIRNODE_CUH_

#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename O, typename... I> class PairNode: public Node<O, I...> {
private:
	friend class TypedPool<PairNode>;
	const Pointer<const Node<float, I...>> x;
	const Pointer<const Node<float, I...>> y;

	__host__ __device__
	static O scalar_(const Node<O, I...>* node, const I&... input) {
		auto this_ = static_cast<const PairNode<O, I...>*>(node);
		return O((*this_->x)(input...), (*this_->y)(input...));
	}

	__host__ __device__
	static typename ranged<O>::type ranged_(const Node<O, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const PairNode<O, I...>*>(node);
		return typename ranged<O>::type((*this_->x)(input...), (*this_->y)(input...));
	}

	__host__
	PairNode(
		const Pointer<const Node<float, I...>>& x,
		const Pointer<const Node<float, I...>>& y,
		const typename Node<O, I...>::scalar_eval_t& scalar,
		const typename Node<O, I...>::ranged_eval_t& ranged
	): Node<O, I...>(scalar, ranged), x(x), y(y) {
	}

public:
	friend class TypedPool<PairNode>;

	__host__
	static Pointer<PairNode<O, I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<float, I...>>& x,
		const Pointer<const Node<float, I...>>& y)
	{
		return Pointer<PairNode<O, I...>>::make(
			pool, x, y,
			typename Node<O, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<O, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* PAIRNODE_CUH_ */

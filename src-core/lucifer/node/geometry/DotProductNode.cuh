#ifndef DOTPRODUCTNODE_CUH_
#define DOTPRODUCTNODE_CUH_

#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename V, typename... I> class DotProductNode: public Node<float, I...> {
private:
	const Pointer<const Node<V, I...>> lhs;
	const Pointer<const Node<V, I...>> rhs;

	__host__ __device__
	static float scalar_(const Node<float, I...>* node, const I&... input) {
		auto this_ = static_cast<const DotProductNode<V, I...>*>(node);
		return dot((*this_->lhs)(input...), (*this_->rhs)(input...));
	}

	__host__ __device__
	static typename ranged<float>::type ranged_(const Node<float, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const DotProductNode<V, I...>*>(node);
		return dot((*this_->lhs)(input...), (*this_->rhs)(input...));
	}

	__host__
	DotProductNode(
		const Pointer<const Node<V, I...>>& lhs,
		const Pointer<const Node<V, I...>>& rhs,
		const typename Node<float, I...>::scalar_eval_t& scalar,
		const typename Node<float, I...>::ranged_eval_t& ranged
	): Node<float, I...>(scalar, ranged), lhs(lhs), rhs(rhs) {
	}

public:
	friend class TypedPool<DotProductNode>;

	__host__
	static Pointer<DotProductNode<V, I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<V, I...>>& lhs,
		const Pointer<const Node<V, I...>>& rhs)
	{
		return Pointer<DotProductNode<V, I...>>::make(
			pool, lhs, rhs,
			typename Node<float, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<float, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}

};

}

#endif /* DOTPRODUCTNODE_CUH_ */

#ifndef TRANSFORM2DNODE_CUH_
#define TRANSFORM2DNODE_CUH_

#include "../Node.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename O, typename... I> class Transform2DNode: public Node<O, I...> {
private:
	friend class TypedPool<Transform2DNode>;
	const Pointer<const Node<O, I...>> tuple;
	const Affine2F transform;

	__host__ __device__
	static O scalar_(const Node<O, I...>* node, const I&... input) {
		auto this_ = static_cast<const Transform2DNode<O, I...>*>(node);
		return this_->transform * (*this_->tuple)(input...);
	}

	__host__ __device__
	static typename ranged<O>::type ranged_(const Node<O, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const Transform2DNode<O, I...>*>(node);
		return this_->transform * (*this_->tuple)(input...);
	}

	__host__
	Transform2DNode(
		const Pointer<const Node<O, I...>>& tuple,
		const Affine2F& transform,
		const typename Node<O, I...>::scalar_eval_t& scalar,
		const typename Node<O, I...>::ranged_eval_t& ranged
	): Node<O, I...>(scalar, ranged), tuple(tuple), transform(transform) {
	}

public:
	friend class TypedPool<Transform2DNode>;

	__host__
	static Pointer<Transform2DNode<O, I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<O, I...>>& tuple,
		const Affine2F& transform)
	{
		return Pointer<Transform2DNode<O, I...>>::make(
			pool, tuple, transform,
			typename Node<O, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<O, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* TRANSFORM2DNODE_CUH_ */

#ifndef TUPLEELEMENTNODE_CUH_
#define TUPLEELEMENTNODE_CUH_

#include <cstdint>
#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename T, typename... I> class TupleElementNode: public Node<float, I...> {
public:
	friend class TypedPool<TupleElementNode>;
	using index_t = uint8_t;

private:
	const index_t index;
	const Pointer<const Node<T, I...>> tuple;

	__host__ __device__
	static float scalar_(const Node<float, I...>* node, const I&... input) {
		auto this_ = static_cast<const TupleElementNode<T, I...>*>(node);
		return (*this_->tuple)(input...)[this_->index];
	}

	__host__ __device__
	static typename ranged<float>::type ranged_(const Node<float, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const TupleElementNode<T, I...>*>(node);
		return (*this_->tuple)(input...)[this_->index];
	}

	__host__
	TupleElementNode(
		const index_t index,
		const Pointer<const Node<T, I...>>& tuple,
		const typename Node<float, I...>::scalar_eval_t& scalar,
		const typename Node<float, I...>::ranged_eval_t& ranged
	): Node<float, I...>(scalar, ranged), index(index), tuple(tuple) {
	}


public:
	__host__
	static Pointer<TupleElementNode<T, I...>> build(
		MemoryPool& pool,
		const index_t index,
		const Pointer<const Node<T, I...>>& tuple)
	{
		return Pointer<TupleElementNode<T, I...>>::make(
			pool, index, tuple,
			typename Node<float, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<float, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* TUPLEELEMENTNODE_CUH_ */

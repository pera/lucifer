#ifndef UVMAPPINGNODE_CUH_
#define UVMAPPINGNODE_CUH_

#include "../Node.cuh"
#include "lucifer/math/geometry/UVMapping.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename... I> class UVMappingNode: public Node<Point2F, I...> {
private:
	const Pointer<const Node<Point3F, I...>> point;
	const Pointer<const UVMapping> mapping;

	__host__ __device__
	static Point2F scalar_(const Node<Point2F, I...>* node, const I&... input) {
		auto this_ = static_cast<const UVMappingNode<I...>*>(node);
		return (*this_->mapping)((*this_->point)(input...));
	}

	__host__ __device__
	static typename ranged<Point2F>::type ranged_(const Node<Point2F, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const UVMappingNode<I...>*>(node);
		return (*this_->mapping)((*this_->point)(input...));
	}

	__host__
	UVMappingNode(
		const Pointer<const Node<Point3F, I...>>& point,
		const Pointer<const UVMapping>& mapping,
		const typename Node<Point2F, I...>::scalar_eval_t& scalar,
		const typename Node<Point2F, I...>::ranged_eval_t& ranged
	): Node<Point2F, I...>(scalar, ranged), point(point), mapping(mapping) {
	}

public:
	friend class TypedPool<UVMappingNode>;

	__host__
	static Pointer<UVMappingNode<I...>> build(MemoryPool& pool, const Pointer<const Node<Point3F, I...>>& point, const Pointer<const UVMapping>& mapping) {
		return Pointer<UVMappingNode<I...>>::make(
			pool, point, mapping,
			typename Node<Point2F, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<Point2F, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* UVMAPPINGNODE_CUH_ */

#ifndef TRIPLETNODE_CUH_
#define TRIPLETNODE_CUH_

#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename O, typename... I> class TripletNode: public Node<O, I...> {
private:
	friend class TypedPool<TripletNode>;
	const Pointer<const Node<float, I...>> x;
	const Pointer<const Node<float, I...>> y;
	const Pointer<const Node<float, I...>> z;

	__host__ __device__
	static O scalar_(const Node<O, I...>* node, const I&... input) {
		auto this_ = static_cast<const TripletNode<O, I...>*>(node);
		return O((*this_->x)(input...), (*this_->y)(input...), (*this_->z)(input...));
	}

	__host__ __device__
	static typename ranged<O>::type ranged_(const Node<O, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const TripletNode<O, I...>*>(node);
		return typename ranged<O>::type((*this_->x)(input...), (*this_->y)(input...), (*this_->z)(input...));
	}

	__host__
	TripletNode(
		const Pointer<const Node<float, I...>>& x,
		const Pointer<const Node<float, I...>>& y,
		const Pointer<const Node<float, I...>>& z,
		const typename Node<O, I...>::scalar_eval_t& scalar,
		const typename Node<O, I...>::ranged_eval_t& ranged
	): Node<O, I...>(scalar, ranged), x(x), y(y), z(z) {
	}

public:
	friend class TypedPool<TripletNode>;

	__host__
	static Pointer<TripletNode<O, I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<float, I...>>& x,
		const Pointer<const Node<float, I...>>& y,
		const Pointer<const Node<float, I...>>& z)
	{
		return Pointer<TripletNode<O, I...>>::make(
			pool, x, y, z,
			typename Node<O, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<O, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* TRIPLETNODE_CUH_ */

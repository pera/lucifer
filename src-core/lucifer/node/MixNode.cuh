#ifndef MIXNODE_CUH_
#define MIXNODE_CUH_

#include "Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename O, typename... I> class MixNode: public Node<O, I...> {
private:
	friend class TypedPool<MixNode>;
	const Pointer<const Node<O, I...>> lhs;
	const Pointer<const Node<O, I...>> rhs;
	const Pointer<const Node<float, I...>> weight;

	__host__ __device__
	static O scalar_(const Node<O, I...>* node, const I&... input) {
		auto this_ = static_cast<const MixNode<O, I...>*>(node);
		return lerp((*this_->lhs)(input...), (*this_->rhs)(input...), (*this_->weight)(input...));
	}

	__host__ __device__
	static typename ranged<O>::type ranged_(const Node<O, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const MixNode<O, I...>*>(node);
		return lerp((*this_->lhs)(input...), (*this_->rhs)(input...), (*this_->weight)(input...));
	}

	__host__
	MixNode(
		const Pointer<const Node<O, I...>>& lhs,
		const Pointer<const Node<O, I...>>& rhs,
		const Pointer<const Node<float, I...>>& weight,
		const typename Node<O, I...>::scalar_eval_t& scalar,
		const typename Node<O, I...>::ranged_eval_t& ranged
	): Node<O, I...>(scalar, ranged), lhs(lhs), rhs(rhs), weight(weight) {
	}

public:
	__host__
	static Pointer<MixNode<O, I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<O, I...>>& lhs,
		const Pointer<const Node<O, I...>>& rhs,
		const Pointer<const Node<float, I...>>& weight)
	{
		return Pointer<MixNode<O, I...>>::make(
			pool, lhs, rhs, weight,
			typename Node<O, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<O, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* MIXNODE_CUH_ */

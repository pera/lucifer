#ifndef LUMINANCENODE_CUH_
#define LUMINANCENODE_CUH_

#include "../Node.cuh"
#include "lucifer/color/RGBColorSpace.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename... I> class LuminanceNode: public Node<float, I...> {
private:
	friend class TypedPool<LuminanceNode>;
	const Pointer<const Node<Color3F, I...>> color;
	const Pointer<const RGBColorSpace> space;

	__host__ __device__
	static float scalar_(const Node<float, I...>* node, const I&... input) {
		auto this_ = static_cast<const LuminanceNode<I...>*>(node);
		return this_->space.isNull() ? (*this_->color)(input...)[1] : this_->space->luminance((*this_->color)(input...));
	}

	__host__ __device__
	static typename ranged<float>::type ranged_(const Node<float, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const LuminanceNode<I...>*>(node);
		return this_->space.isNull() ? (*this_->color)(input...)[1] : this_->space->luminance((*this_->color)(input...));
	}

	__host__
	LuminanceNode(
		const Pointer<const Node<Color3F, I...>>& color,
		const Pointer<const RGBColorSpace>& space,
		const typename Node<float, I...>::scalar_eval_t& scalar,
		const typename Node<float, I...>::ranged_eval_t& ranged
	): Node<float, I...>(scalar, ranged), color(color), space(space) {
	}

public:
	__host__
	static Pointer<LuminanceNode<I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<Color3F, I...>>& color,
		const Pointer<const RGBColorSpace>& space)
	{
		return Pointer<LuminanceNode<I...>>::make(
			pool, color, space,
			typename Node<float, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<float, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* LUMINANCENODE_CUH_ */

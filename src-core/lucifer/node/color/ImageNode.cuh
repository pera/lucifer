#ifndef IMAGENODE_CUH_
#define IMAGENODE_CUH_

#include "../Node.cuh"
#include "lucifer/filter/Filter2D.cuh"
#include "lucifer/image/HDRImage.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename... I> class ImageNode: public Node<Color3F, I...> {
private:
	friend class TypedPool<ImageNode>;
	const Pointer<const Node<Point2F, I...>> pixel;
	const Pointer<const HDRImage<float, 3>> image;
	const Pointer<const Filter2D> filter;
	const typename ranged<Color3F>::type range;
	const bool wrap;

	__host__ __device__
	Color3F evaluate(float r, float c) const {
		Color3F v{};
		auto h = image->height();
		auto w = image->width();
		if (wrap) {
			r = mod_fi(r, h);
			c = mod_fi(c, w);
			if (filter.isNull()) {
				v = (*image)(r, c);
			} else {
				RangeF rRange = filter->xSupport();
				RangeF cRange = filter->ySupport();
				int64_t rMin = floor(rRange.lower + r);
				int64_t rMax =  ceil(rRange.upper + r);
				int64_t cMin = floor(cRange.lower + c);
				int64_t cMax =  ceil(cRange.upper + c);
				float weight, weightSum = 0.f;
				for (int64_t r_ = rMin; r_ <= rMax; r_++) {
					for (int64_t c_ = cMin; c_ <= cMax; c_++) {
						weight = (*filter)(r_ - r, c_ - c);
						weightSum += weight;
						v += weight * (*image)(mod_fi(r_, h), mod_fi(c_, w));
					}
				}
				if (weightSum != 0.f) v /= weightSum;
			}
		} else if (r >= 0 && r < h && c >= 0 && c < w) {
			if (filter.isNull()) {
				v = (*image)(r, c);
			} else {
				RangeF rRange = filter->xSupport();
				RangeF cRange = filter->ySupport();
				int64_t rMin = floor(rRange.lower + r);
				int64_t rMax =  ceil(rRange.upper + r);
				int64_t cMin = floor(cRange.lower + c);
				int64_t cMax =  ceil(cRange.upper + c);
				float weight, weightSum = 0.f;
				for (int64_t r_ = rMin; r_ <= rMax; r_++) {
					for (int64_t c_ = cMin; c_ <= cMax; c_++) {
						if (r_ >= 0 && r_ < h && c_ >= 0 && c < w) {
							weight = (*filter)(r_ - r, c_ - c);
							weightSum += weight;
							v += weight * (*image)(r_, c_);
						}
					}
				}
				if (weightSum != 0.f) v /= weightSum;
			}
		}
		return v;
	}

	__host__ __device__
	static Color3F scalar_(const Node<Color3F, I...>* node, const I&... input) {
		auto this_ = static_cast<const ImageNode<I...>*>(node);
		const auto pix = (*this_->pixel)(input...);
		return this_->evaluate(this_->image->height() - 1.f - pix[1], pix[0]);
	}

	__host__ __device__
	static typename ranged<Color3F>::type ranged_(const Node<Color3F, I...>* node, const typename ranged<I>::type&... input) {
		return static_cast<const ImageNode<I...>*>(node)->range;
	}

	__host__
	ImageNode(
		const Pointer<const Node<Point2F, I...>>& pixel,
		const Pointer<const HDRImage<float, 3>>& image,
		const Pointer<const Filter2D>& filter,
		const bool wrap,
		const typename Node<Color3F, I...>::scalar_eval_t& scalar,
		const typename Node<Color3F, I...>::ranged_eval_t& ranged
	): Node<Color3F, I...>(scalar, ranged), pixel(pixel), image(image), filter(filter), range(image->range()), wrap(wrap) {
	}

public:
	friend class TypedPool<ImageNode>;

	__host__
	static Pointer<ImageNode<I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<Point2F, I...>>& pixel,
		const Pointer<const HDRImage<float, 3>>& image,
		const Pointer<const Filter2D>& filter,
		const bool wrap)
	{
		return Pointer<ImageNode<I...>>::make(
			pool, pixel, image, filter, wrap,
			typename Node<Color3F, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<Color3F, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* IMAGENODE_CUH_ */

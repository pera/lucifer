#ifndef COLORSPACENODE_CUH_
#define COLORSPACENODE_CUH_

#include "../Node.cuh"
#include "lucifer/color/RGBColorSpace.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename... I> class ColorSpaceNode: public Node<Color3F, I...> {
private:
	friend class TypedPool<ColorSpaceNode>;
	const Pointer<const Node<Color3F, I...>> color;
	const Pointer<const RGBColorSpace> src;
	const Pointer<const RGBColorSpace> dst;

	__host__ __device__
	static Color3F scalar_(const Node<Color3F, I...>* node, const I&... input) {
		auto this_ = static_cast<const ColorSpaceNode<I...>*>(node);
		Color3F tmp = this_->src.isNull() ? (*this_->color)(input...) : this_->src->toXYZ((*this_->color)(input...));
		return this_->dst.isNull() ? tmp : this_->dst->toRGB(tmp);
	}

	__host__ __device__
	static typename ranged<Color3F>::type ranged_(const Node<Color3F, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const ColorSpaceNode<I...>*>(node);
		typename ranged<Color3F>::type tmp = this_->src.isNull() ? (*this_->color)(input...) : this_->src->toXYZ((*this_->color)(input...));
		return this_->dst.isNull() ? tmp : this_->dst->toRGB(tmp);
	}

	__host__
	ColorSpaceNode(
		const Pointer<const Node<Color3F, I...>>& color,
		const Pointer<const RGBColorSpace>& input,
		const Pointer<const RGBColorSpace>& output,
		const typename Node<Color3F, I...>::scalar_eval_t& scalar,
		const typename Node<Color3F, I...>::ranged_eval_t& ranged
	): Node<Color3F, I...>(scalar, ranged), color(color), src(input), dst(output) {
	}

public:
	friend class TypedPool<ColorSpaceNode>;

	__host__
	static Pointer<ColorSpaceNode<I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<Color3F, I...>>& color,
		const Pointer<const RGBColorSpace>& input,
		const Pointer<const RGBColorSpace>& output)
	{
		return Pointer<ColorSpaceNode<I...>>::make(
			pool, color, input, output,
			typename Node<Color3F, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<Color3F, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* COLORSPACENODE_CUH_ */

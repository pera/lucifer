#ifndef NODE_CUH_
#define NODE_CUH_

#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/memory/Virtual.cuh"

namespace lucifer {

template<typename O, typename... I> class Node: public Polymorphic {
public:
	using scalar_eval_t = Virtual<O, const Node<O, I...>*, const I&...>;
	using ranged_eval_t = Virtual<typename ranged<O>::type, const Node<O, I...>*, const typename ranged<I>::type&...>;

private:
	const scalar_eval_t scalar_;
	const ranged_eval_t ranged_;

public:
	__host__
	Node(const scalar_eval_t& scalar_, const ranged_eval_t& ranged_):
	scalar_(scalar_), ranged_(ranged_) {
	}

	__host__ __device__
	O operator()(const I&... input) const {
		return scalar_(this, input...);
	}

	__host__ __device__
	typename ranged<O>::type operator()(const typename ranged<I>::type&... input) const {
		return ranged_(this, input...);
	}
};

}

#endif /* NODE_CUH_ */

#ifndef TRIPLANARNODE_CUH_
#define TRIPLANARNODE_CUH_

#include "lucifer/node/Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename O, typename... I> class TriplanarNode: public Node<O, I...> {
private:
	friend class TypedPool<TriplanarNode>;
	const Pointer<const Node<Norml3F, I...>> n;
	const Pointer<const Node<O, I...>> x;
	const Pointer<const Node<O, I...>> y;
	const Pointer<const Node<O, I...>> z;
	const Pointer<const Node<float, I...>> e;

	__host__ __device__
	static O scalar_(const Node<O, I...>* node, const I&... input) {
		auto this_ = static_cast<const TriplanarNode<O, I...>*>(node);
		auto n_ = (*this_->n)(input...);
		auto e_ = (*this_->e)(input...);
		n_[0] = pow(abs(n_[0]), e_);
		n_[1] = pow(abs(n_[1]), e_);
		n_[2] = pow(abs(n_[2]), e_);
		return ((*this_->x)(input...) * n_[0] + (*this_->y)(input...) * n_[1] + (*this_->z)(input...) * n_[2]) / (n_[0] + n_[1] + n_[2]);
	}

	__host__ __device__
	static typename ranged<O>::type ranged_(const Node<O, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const TriplanarNode<O, I...>*>(node);
		auto n_ = (*this_->n)(input...);
		auto e_ = (*this_->e)(input...);
		n_[0] = pow(abs(n_[0]), e_);
		n_[1] = pow(abs(n_[1]), e_);
		n_[2] = pow(abs(n_[2]), e_);
		return ((*this_->x)(input...) * n_[0] + (*this_->y)(input...) * n_[1] + (*this_->z)(input...) * n_[2]) / (n_[0] + n_[1] + n_[2]);
	}

	__host__
	TriplanarNode(
		const Pointer<const Node<Norml3F, I...>>& n,
		const Pointer<const Node<O, I...>>& x,
		const Pointer<const Node<O, I...>>& y,
		const Pointer<const Node<O, I...>>& z,
		const Pointer<const Node<float, I...>>& e,
		const typename Node<O, I...>::scalar_eval_t& scalar,
		const typename Node<O, I...>::ranged_eval_t& ranged
	): Node<O, I...>(scalar, ranged), n(n), x(x), y(y), z(z), e(e) {
	}

public:
	friend class TypedPool<TriplanarNode>;

	__host__
	static Pointer<TriplanarNode<O, I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<Norml3F, I...>>& n,
		const Pointer<const Node<O, I...>>& x,
		const Pointer<const Node<O, I...>>& y,
		const Pointer<const Node<O, I...>>& z,
		const Pointer<const Node<float, I...>>& e)
	{
		return Pointer<TriplanarNode<O, I...>>::make(
			pool, n, x, y, z, e,
			typename Node<O, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<O, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* TRIPLANARNODE_CUH_ */

#ifndef VORONOINOISE2DNODE_CUH_
#define VORONOINOISE2DNODE_CUH_

#include <cstdint>
#include <random>
#include "../Node.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename... I> class VoronoiNoise2DNode: public Node<float, I...> {
public:
	using idx1d_t = uint16_t;
	using idx2d_t = uint32_t;
	using array_t = Array<Point2F, idx2d_t>;

private:
	const Pointer<const Node<Point2F, I...>> point;
	float factor;
	float min_;
	float max_;
	idx1d_t nx;
	idx1d_t ny;
	array_t value;

private:
	__host__ __device__
	static idx2d_t size(const idx2d_t nx, const idx2d_t ny) {
		return nx * ny;
	}

	__host__ __device__
	idx2d_t index(const int32_t x, const int32_t y) const {
		return static_cast<idx2d_t>(x + y * nx);
	}

	__host__
	float upperBound() const {
		float result = 0.f;
		// for each cell
		for (int32_t y = 0; y < ny; ++y) {
			for (int32_t x = 0; x < nx; ++x) {
				Point2F cell = Point2F(x, y) + value[index(mod_ii(x, nx), mod_ii(y, ny))];
				// for each neighbour
				for (int32_t dy = -1; dy <= 1; ++dy) {
					for (int32_t dx = -1; dx <= 1; ++dx) {
						Point2F neighbour = Point2F(x + dx, y + dy) + value[index(mod_ii(x + dx, nx), mod_ii(y + dy, ny))];
						result = max(result, (cell - neighbour).lengthSquared());
					}
				}
			}
		}
		return sqrt(result) * .5f;
	}

	__host__ __device__
	static float scalar_(const Node<float, I...>* node, const I&... input) {
		auto this_ = static_cast<const VoronoiNoise2DNode<I...>*>(node);
		const auto p = (*this_->point)(input...);
		const auto x = static_cast<int32_t>(floor(p[0]));
		const auto y = static_cast<int32_t>(floor(p[1]));
		float length = +INFINITY;
		for (int32_t dx = -1; dx <= 1; ++dx) {
			for (int32_t dy = -1; dy <= 1; ++dy) {
				const float temp = (p - (this_->value[this_->index(mod_ii(x + dx, this_->nx), mod_ii(y + dy, this_->ny))] + Point2F(x + dx, y + dy))).lengthSquared();
				if (temp < length)
					length = temp;
			}
		}
		return sqrt(length) * this_->factor * (this_->max_ - this_->min_) + this_->min_;
	}

	__host__ __device__
	static RangeF ranged_(const Node<float, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const VoronoiNoise2DNode<I...>*>(node);
		return RangeF(this_->min_, this_->max_);
	}

	__host__
	VoronoiNoise2DNode(
		MemoryPool& pool,
		const Pointer<const Node<Point2F, I...>>& point,
		const float min,
		const float max,
		const idx1d_t nx,
		const idx1d_t ny,
		const float jitter,
		const long seed,
		const typename Node<float, I...>::scalar_eval_t& scalar,
		const typename Node<float, I...>::ranged_eval_t& ranged
	): Node<float, I...>(scalar, ranged), point(point), min_(min), max_(max), nx(nx), ny(ny), value(pool, size(nx, ny))
	{
		float jtr = clamp(jitter, 0.f, 1.f);
		std::default_random_engine engine(seed);
		std::uniform_real_distribution<float> distribution;
		for (idx2d_t y = 0, i = 0; y < ny; ++y) {
			for (idx2d_t x = 0; x < nx; ++x, ++i) {
				value[i][0] = 0.5f + (distribution(engine) - 0.5f) * jtr;
				value[i][1] = 0.5f + (distribution(engine) - 0.5f) * jtr;
			}
		}
		factor = 1.f / upperBound();
	}

public:
	friend class TypedPool<VoronoiNoise2DNode>;

	__host__
	static Pointer<VoronoiNoise2DNode<I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<Point2F, I...>>& point,
		const float min,
		const float max,
		const idx1d_t nx,
		const idx1d_t ny,
		const float jitter,
		const long seed)
	{
		return Pointer<VoronoiNoise2DNode<I...>>::make(
			pool, pool, point, min, max, nx, ny, jitter, seed,
			typename Node<float, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<float, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* VORONOINOISE2DNODE_CUH_ */

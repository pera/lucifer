#ifndef PERLINNOISE2DNODE_CUH_
#define PERLINNOISE2DNODE_CUH_

#include <cstdint>
#include <random>

#include "../Node.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename... I> class PerlinNoise2DNode: public Node<float, I...> {
public:
	using idx1d_t = uint16_t;
	using idx2d_t = uint32_t;
	using array_t = Array<Point2F, idx2d_t>;

private:
	const Pointer<const Node<Point2F, I...>> point;
	const float min;
	const float max;
	const idx1d_t nx;
	const idx1d_t ny;
	array_t value;

private:
	__host__ __device__
	static idx2d_t size(const idx2d_t nx, const idx2d_t ny) {
		return nx * ny;
	}

	__host__ __device__
	idx2d_t index(const idx2d_t x, const idx2d_t y) const {
		return x + y * nx;
	}

	__host__ __device__
	float gradient(const int32_t xi, const int32_t yi, const float dx, const float dy) const {
		const Point2F v = value[index(mod_ii(xi, nx), mod_ii(yi, ny))];
		return dx * v[0] + dy * v[1];
	}

	__host__ __device__
	static float scalar_(const Node<float, I...>* node, const I&... input) {
		auto this_ = static_cast<const PerlinNoise2DNode<I...>*>(node);
		const Point2F uv = (*this_->point)(input...);
		const int32_t xi = static_cast<int32_t>(uv[0]);
		const int32_t yi = static_cast<int32_t>(uv[1]);
		const float dx = uv[0] - xi;
		const float dy = uv[1] - yi;
		const float wx = smooth5(dx);
		const float wy = smooth5(dy);
		const float w  =
			lerp(
				lerp(
					this_->gradient(xi + 0, yi + 0, dx - 0, dy - 0),
					this_->gradient(xi + 1, yi + 0, dx - 1, dy - 0),
					wx
				),
				lerp(
					this_->gradient(xi + 0, yi + 1, dx - 0, dy - 1),
					this_->gradient(xi + 1, yi + 1, dx - 1, dy - 1),
					wx
				),
				wy
			);
		return this_->min + .5f * (this_->max - this_->min) * (1.f + w);
	}

	__host__ __device__
	static RangeF ranged_(const Node<float, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const PerlinNoise2DNode<I...>*>(node);
		return RangeF(this_->min, this_->max);
	}

	__host__
	PerlinNoise2DNode(
		MemoryPool& pool,
		const Pointer<const Node<Point2F, I...>>& point,
		const float min,
		const float max,
		const idx1d_t nx,
		const idx1d_t ny,
		const long seed,
		const typename Node<float, I...>::scalar_eval_t& scalar,
		const typename Node<float, I...>::ranged_eval_t& ranged
	): Node<float, I...>(scalar, ranged), point(point), min(min), max(max), nx(nx), ny(ny), value(pool, size(nx, ny)) {
		float rnd;
		std::default_random_engine engine(seed);
		std::uniform_real_distribution<float> distribution;
		for (idx2d_t i = 0; i < value.size(); i++) {
			rnd = distribution(engine) * 2.f * PI<float>();
			value[i][0] = sin(rnd);
			value[i][1] = cos(rnd);
		}
	}

public:
	friend class TypedPool<PerlinNoise2DNode>;

	__host__
	static Pointer<PerlinNoise2DNode<I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<Point2F, I...>>& point,
		const float min,
		const float max,
		const idx1d_t nx,
		const idx1d_t ny,
		const long seed)
	{
		return Pointer<PerlinNoise2DNode<I...>>::make(
			pool, pool, point, min, max, nx, ny, seed,
			typename Node<float, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<float, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* PERLINNOISE2DNODE_CUH_ */

#ifndef REGULARGRID3DNODE_CUH_
#define REGULARGRID3DNODE_CUH_

#include <cstdint>

#include "../Node.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/Util.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename O, typename... I> class RegularGrid3DNode: public Node<O, I...> {
public:
	friend class TypedPool<RegularGrid3DNode>;
	using idx1d_t = uint16_t;
	using idx3d_t = uint32_t;
	using array_t = Array<O, idx3d_t>;

private:
	const Pointer<const Node<Point3F, I...>> point;
	const idx1d_t nx;
	const idx1d_t ny;
	const idx1d_t nz;
	const array_t values;
	const Smooth smooth;
	const typename ranged<O>::type range;

private:
	__host__
	static typename ranged<O>::type span(const array_t& values) {
		typename ranged<O>::type r{values[0]};
		for (const auto& value: values) {
			r = hull(r, typename ranged<O>::type(value));
		}
		return r;
	}

	__host__  __device__
	idx3d_t index(const idx3d_t x, const idx3d_t y, const idx3d_t z) const {
		return x + (y * nx) + (z * nx * ny);
	}

	__host__ __device__
	static O scalar_(const Node<O, I...>* node, const I&... input) {
		auto this_ = static_cast<const RegularGrid3DNode<O, I...>*>(node);
		const Point3F p = (*this_->point)(input...);
		const idx1d_t x = mod_fi(p[0], this_->nx);
		const idx1d_t y = mod_fi(p[1], this_->ny);
		const idx1d_t z = mod_fi(p[2], this_->nz);
		if (this_->smooth == Smooth::NONE) {
			return this_->values[this_->index(x, y, z)];
		}
		float dx = p[0] - floor(p[0]);
		float dy = p[1] - floor(p[1]);
		float dz = p[2] - floor(p[2]);
		smooth3D(dx, dy, dz, this_->smooth);
	    return
			lerp(
				lerp(
					lerp(
						this_->values[this_->index((x + 0) % this_->nx, (y + 0) % this_->ny, (z + 0) % this_->nz)],
						this_->values[this_->index((x + 1) % this_->nx, (y + 0) % this_->ny, (z + 0) % this_->nz)],
						dx
					),
					lerp(
						this_->values[this_->index((x + 0) % this_->nx, (y + 1) % this_->ny, (z + 0) % this_->nz)],
						this_->values[this_->index((x + 1) % this_->nx, (y + 1) % this_->ny, (z + 0) % this_->nz)],
						dx
					),
					dy
				),
				lerp(
					lerp(
						this_->values[this_->index((x + 0) % this_->nx, (y + 0) % this_->ny, (z + 1) % this_->nz)],
						this_->values[this_->index((x + 1) % this_->nx, (y + 0) % this_->ny, (z + 1) % this_->nz)],
						dx
					),
					lerp(
						this_->values[this_->index((x + 0) % this_->nx, (y + 1) % this_->ny, (z + 1) % this_->nz)],
						this_->values[this_->index((x + 1) % this_->nx, (y + 1) % this_->ny, (z + 1) % this_->nz)],
						dx
					),
					dy
				),
				dz
			);
	}

	__host__ __device__
	static typename ranged<O>::type ranged_(const Node<O, I...>* node, const typename ranged<I>::type&... input) {
		return static_cast<const RegularGrid3DNode<O, I...>*>(node)->range;
	}

	__host__
	RegularGrid3DNode(
		const Pointer<const Node<Point3F, I...>>& point,
		const idx1d_t nx,
		const idx1d_t ny,
		const idx1d_t nz,
		const array_t& values,
		const Smooth smooth,
		const typename Node<O, I...>::scalar_eval_t& scalar,
		const typename Node<O, I...>::ranged_eval_t& ranged
	): Node<O, I...>(scalar, ranged), point(point), nx(nx), ny(ny), nz(nz), values(values), smooth(smooth), range(span(values)) {
	}

public:
	__host__
	static Pointer<RegularGrid3DNode<O, I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<Point3F, I...>>& point,
		const idx1d_t nx,
		const idx1d_t ny,
		const idx1d_t nz,
		const array_t& values,
		const Smooth smooth)
	{
		return Pointer<RegularGrid3DNode<O, I...>>::make(
			pool, point, nx, ny, nz, values, smooth,
			typename Node<O, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<O, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* REGULARGRID3DNODE_CUH_ */

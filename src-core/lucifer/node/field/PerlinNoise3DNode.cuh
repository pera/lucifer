#ifndef PERLINNOISE3DNODE_CUH_
#define PERLINNOISE3DNODE_CUH_

#include <cstdint>
#include <random>

#include "../Node.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/Util.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/random/UniformSphereDistribution.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename... I> class PerlinNoise3DNode: public Node<float, I...> {
public:
	friend class TypedPool<PerlinNoise3DNode>;
	using idx1d_t = uint16_t;
	using idx3d_t = uint32_t;
	using array_t = Array<Vectr3F, idx3d_t>;

private:
	const Pointer<const Node<Point3F, I...>> point;
	const float min;
	const float max;
	const idx1d_t nx;
	const idx1d_t ny;
	const idx1d_t nz;
	array_t value;

private:
	__host__ __device__
	static idx3d_t size(const idx3d_t nx, const idx3d_t ny, const idx3d_t nz) {
		return nx * ny * nz;
	}

	__host__ __device__
	idx3d_t index(const idx3d_t x, const idx3d_t y, const idx3d_t z) const {
		return x + (y * nx) + (z * nx * ny);
	}

	__host__ __device__
	float gradient(const int32_t xi, const int32_t yi, const int32_t zi, const float dx, const float dy, const float dz) const {
		const Vectr3F v = value[index(mod_ii(xi, nx), mod_ii(yi, ny), mod_ii(zi, nz))];
		return dx * v[0] + dy * v[1] + dz * v[2];
	}

	__host__ __device__
	static float scalar_(const Node<float, I...>* node, const I&... input) {
		auto this_ = static_cast<const PerlinNoise3DNode<I...>*>(node);
		const Point3F p = (*this_->point)(input...);
		const int32_t xi = (int32_t) floor(p[0]);
		const int32_t yi = (int32_t) floor(p[1]);
		const int32_t zi = (int32_t) floor(p[2]);
		const float dx = p[0] - xi;
		const float dy = p[1] - yi;
		const float dz = p[2] - zi;
		const float wx = smooth5(dx);
		const float wy = smooth5(dy);
		const float wz = smooth5(dz);
		const float w  =
			lerp(
				lerp(
					lerp(
						this_->gradient(xi + 0, yi + 0, zi + 0, dx - 0, dy - 0, dz - 0),
						this_->gradient(xi + 1, yi + 0, zi + 0, dx - 1, dy - 0, dz - 0),
						wx
					),
					lerp(
						this_->gradient(xi + 0, yi + 1, zi + 0, dx - 0, dy - 1, dz - 0),
						this_->gradient(xi + 1, yi + 1, zi + 0, dx - 1, dy - 1, dz - 0),
						wx
					),
					wy
				),
				lerp(
					lerp(
						this_->gradient(xi + 0, yi + 0, zi + 1, dx - 0, dy - 0, dz - 1),
						this_->gradient(xi + 1, yi + 0, zi + 1, dx - 1, dy - 0, dz - 1),
						wx
					),
					lerp(
						this_->gradient(xi + 0, yi + 1, zi + 1, dx - 0, dy - 1, dz - 1),
						this_->gradient(xi + 1, yi + 1, zi + 1, dx - 1, dy - 1, dz - 1),
						wx
					),
					wy
				),
				wz
			);
		return this_->min + .5f * (this_->max - this_->min) * (1.f + w);
	}

	__host__ __device__
	static RangeF ranged_(const Node<float, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const PerlinNoise3DNode<I...>*>(node);
		return RangeF(this_->min, this_->max);
	}

	__host__
	PerlinNoise3DNode(
		MemoryPool& pool,
		const Pointer<const Node<Point3F, I...>>& point,
		const float min,
		const float max,
		const idx1d_t nx,
		const idx1d_t ny,
		const idx1d_t nz,
		const long seed,
		const typename Node<float, I...>::scalar_eval_t& scalar,
		const typename Node<float, I...>::ranged_eval_t& ranged
	): Node<float, I...>(scalar, ranged), point(point), min(min), max(max), nx(nx), ny(ny), nz(nz), value(pool, size(nx, ny, nz)) {
		float rnd[2];
		std::default_random_engine engine(seed);
		std::uniform_real_distribution<float> distribution;
		for (idx3d_t i = 0; i < value.size(); i++) {
			rnd[0] = distribution(engine);
			rnd[1] = distribution(engine);
			UniformSphereDistribution::sample(rnd, &value[i][0], nullptr);
		}
	}

public:
	__host__
	static Pointer<PerlinNoise3DNode<I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<Point3F, I...>>& point,
		const float min,
		const float max,
		const idx1d_t nx,
		const idx1d_t ny,
		const idx1d_t nz,
		const long seed)
	{
		return Pointer<PerlinNoise3DNode<I...>>::make(
			pool, pool, point, min, max, nx, ny, nz, seed,
			typename Node<float, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<float, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* PERLINNOISE3DNODE_CUH_ */

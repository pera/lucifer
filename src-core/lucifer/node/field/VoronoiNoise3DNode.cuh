#ifndef VORONOINOISE3DNODE_CUH_
#define VORONOINOISE3DNODE_CUH_

#include <cstdint>
#include <random>
#include "../Node.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename... I> class VoronoiNoise3DNode: public Node<float, I...> {
public:
	using idx1d_t = uint16_t;
	using idx3d_t = uint32_t;
	using array_t = Array<Point3F, idx3d_t>;

private:
	const Pointer<const Node<Point3F, I...>> point;
	float factor;
	float min_;
	float max_;
	idx1d_t nx;
	idx1d_t ny;
	idx1d_t nz;
	array_t value;

private:
	__host__ __device__
	static idx3d_t size(const idx3d_t nx, const idx3d_t ny, const idx3d_t nz) {
		return nx * ny * nz;
	}

	__host__ __device__
	int64_t index(const int32_t x, const int32_t y, const int32_t z) const {
		return x + (y * nx) + (z * nx * ny);
	}

	__host__
	float upperBound() const {
		float result = 0.f;
		// for each cell
		for (int32_t z = 0; z < ny; ++z) {
			for (int32_t y = 0; y < ny; ++y) {
				for (int32_t x = 0; x < nx; ++x) {
					Point3F cell = Point3F(x, y, z) + value[index(mod_ii(x, nx), mod_ii(y, ny), mod_ii(z, nz))];
					// for each neighbour
					for (int32_t dz = -1; dz <= 1; ++dz) {
						for (int32_t dy = -1; dy <= 1; ++dy) {
							for (int32_t dx = -1; dx <= 1; ++dx) {
								Point3F neighbour = Point3F(x + dx, y + dy, z + dz) + value[index(mod_ii(x + dx, nx), mod_ii(y + dy, ny), mod_ii(z + dz, nz))];
								result = max(result, (cell - neighbour).lengthSquared());
							}
						}
					}
				}
			}
		}
		return sqrt(result) * .5f;
	}

	__host__ __device__
	static float scalar_(const Node<float, I...>* node, const I&... input) {
		auto this_ = static_cast<const VoronoiNoise3DNode<I...>*>(node);
		const auto p = (*this_->point)(input...);
		const auto x = static_cast<int32_t>(floor(p[0]));
		const auto y = static_cast<int32_t>(floor(p[1]));
		const auto z = static_cast<int32_t>(floor(p[2]));
		float length = +INFINITY;
		for (int32_t dx = -1; dx <= 1; ++dx) {
			for (int32_t dy = -1; dy <= 1; ++dy) {
				for (int32_t dz = -1; dz <= 1; ++dz) {
					const float temp = (p - (this_->value[this_->index(mod_ii(x + dx, this_->nx), mod_ii(y + dy, this_->ny), mod_ii(z + dz, this_->nz))] + Point3F(x + dx, y + dy, z + dz))).lengthSquared();
					if (temp < length)
						length = temp;
				}
			}
		}
		return sqrt(length) * this_->factor * (this_->max_ - this_->min_) + this_->min_;
	}

	__host__ __device__
	static RangeF ranged_(const Node<float, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const VoronoiNoise3DNode<I...>*>(node);
		return RangeF(this_->min_, this_->max_);
	}

	__host__
	VoronoiNoise3DNode(
		MemoryPool& pool,
		const Pointer<const Node<Point3F, I...>>& point,
		const float min,
		const float max,
		const idx1d_t nx,
		const idx1d_t ny,
		const idx1d_t nz,
		const float jitter,
		const long seed,
		const typename Node<float, I...>::scalar_eval_t& scalar,
		const typename Node<float, I...>::ranged_eval_t& ranged
	): Node<float, I...>(scalar, ranged), point(point), min_(min), max_(max), nx(nx), ny(ny), nz(nz), value(pool, size(nx, ny, nz))
	{
		float jtr = clamp(jitter, 0.f, 1.f);
		std::default_random_engine engine(seed);
		std::uniform_real_distribution<float> distribution;
		for (idx3d_t z = 0, i = 0; z < nz; ++z) {
			for (idx3d_t y = 0; y < ny; ++y) {
				for (idx3d_t x = 0; x < nx; ++x, ++i) {
					value[i][0] = 0.5f + (distribution(engine) - 0.5f) * jtr;
					value[i][1] = 0.5f + (distribution(engine) - 0.5f) * jtr;
					value[i][2] = 0.5f + (distribution(engine) - 0.5f) * jtr;
				}
			}
		}
		factor = 1.f / upperBound();
	}

public:
	friend class TypedPool<VoronoiNoise3DNode>;

	__host__
	static Pointer<VoronoiNoise3DNode<I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<Point3F, I...>>& point,
		const float min,
		const float max,
		const idx1d_t nx,
		const idx1d_t ny,
		const idx1d_t nz,
		const float jitter,
		const long seed)
	{
		return Pointer<VoronoiNoise3DNode<I...>>::make(
			pool, pool, point, min, max, nx, ny, nz, jitter, seed,
			typename Node<float, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<float, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* VORONOINOISE3DNODE_CUH_ */

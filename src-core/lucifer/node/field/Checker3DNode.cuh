#ifndef CHECKER3DNODE_CUH_
#define CHECKER3DNODE_CUH_

#include "../Node.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename O, typename... I> class Checker3DNode: public Node<O, I...> {
private:
	friend class TypedPool<Checker3DNode>;
	const Pointer<const Node<O, I...>> lhs;
	const Pointer<const Node<O, I...>> rhs;
	const Pointer<const Node<Point3F, I...>> point;

private:
	__host__ __device__
	static int floor_(float x) {
		return (int) floor(x - 0.5f);
	}

	__host__ __device__
	static int checker(const Point3F& p) {
		return ((int) abs(floor_(p[0]) + floor_(p[1]) + floor_(p[2])));
	}

	__host__ __device__
	static O scalar_(const Node<O, I...>* node, const I&... input) {
		auto this_ = static_cast<const Checker3DNode<O, I...>*>(node);
		return checker((*this_->point)(input...)) % 2 == 0 ? (*this_->lhs)(input...) : (*this_->rhs)(input...);
	}

	__host__ __device__
	static typename ranged<O>::type ranged_(const Node<O, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const Checker3DNode<O, I...>*>(node);
		return hull((*this_->lhs)(input...), (*this_->rhs)(input...));
	}

	__host__
	Checker3DNode(
		const Pointer<const Node<O, I...>>& lhs,
		const Pointer<const Node<O, I...>>& rhs,
		const Pointer<const Node<Point3F, I...>>& point,
		const typename Node<O, I...>::scalar_eval_t& scalar,
		const typename Node<O, I...>::ranged_eval_t& ranged
	): Node<O, I...>(scalar, ranged), lhs(lhs), rhs(rhs), point(point) {
	}

public:
	__host__
	static Pointer<Checker3DNode<O, I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<O, I...>>& lhs,
		const Pointer<const Node<O, I...>>& rhs,
		const Pointer<const Node<Point3F, I...>>& point)
	{
		return Pointer<Checker3DNode<O, I...>>::make(
			pool, lhs, rhs, point,
			typename Node<O, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<O, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* CHECKER3DNODE_CUH_ */

#ifndef IFELSENODE_CUH_
#define IFELSENODE_CUH_

#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename O, typename... I> class IfElseNode: public Node<O, I...> {
private:
	const Pointer<const Node<bool, I...>> condition;
	const Pointer<const Node<O, I...>> lhs;
	const Pointer<const Node<O, I...>> rhs;

	__host__ __device__
	static O scalar_(const Node<O, I...>* node, const I&... input) {
		const IfElseNode<O, I...>* this_ = static_cast<const IfElseNode<O, I...>*>(node);
		return (*this_->condition)(input...) ? (*this_->lhs)(input...) : (*this_->rhs)(input...);
	}

	__host__ __device__
	static typename ranged<O>::type ranged_(const Node<O, I...>* node, const typename ranged<I>::type&... input) {
		const IfElseNode<O, I...>* this_ = static_cast<const IfElseNode<O, I...>*>(node);
		typename ranged<bool>::type eval = (*this_->condition)(input...);
		if (eval) {
			typename ranged<O>::type result = (*this_->lhs)(input...);
			if (!eval) {
				result = hull(result, (*this_->rhs)(input...));
			}
			return result;
		} else {
			return (*this_->rhs)(input...);
		}
	}

	__host__
	IfElseNode(
		const Pointer<const Node<bool, I...>>& condition,
		const Pointer<const Node<O, I...>>& lhs,
		const Pointer<const Node<O, I...>>& rhs,
		const typename Node<O, I...>::scalar_eval_t& scalar,
		const typename Node<O, I...>::ranged_eval_t& ranged
	): Node<O, I...>(scalar, ranged), condition(condition), lhs(lhs), rhs(rhs) {
	}

public:
	friend class TypedPool<IfElseNode>;

	__host__
	static Pointer<IfElseNode<O, I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<bool, I...>>& condition,
		const Pointer<const Node<O, I...>>& lhs,
		const Pointer<const Node<O, I...>>& rhs)
	{
		return Pointer<IfElseNode<O, I...>>::make(
			pool, condition, lhs, rhs,
			typename Node<O, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<O, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* IFELSENODE_CUH_ */

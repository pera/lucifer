#ifndef LESSTHANNODE_CUH_
#define LESSTHANNODE_CUH_

#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename... I> class LessThanNode: public Node<bool, I...> {
private:
	const Pointer<const Node<float, I...>> lhs;
	const Pointer<const Node<float, I...>> rhs;

	__host__ __device__
	static bool scalar_(const Node<bool, I...>* node, const I&... input) {
		const LessThanNode<I...>* this_ = static_cast<const LessThanNode<I...>*>(node);
		return (*this_->lhs)(input...) < (*this_->rhs)(input...);
	}

	__host__ __device__
	static typename ranged<bool>::type ranged_(const Node<bool, I...>* node, const typename ranged<I>::type&... input) {
		const LessThanNode<I...>* this_ = static_cast<const LessThanNode<I...>*>(node);
		return (*this_->lhs)(input...) < (*this_->rhs)(input...);
	}

	__host__
	LessThanNode (
		const Pointer<const Node<float, I...>>& lhs,
		const Pointer<const Node<float, I...>>& rhs,
		const typename Node<bool, I...>::scalar_eval_t& scalar,
		const typename Node<bool, I...>::ranged_eval_t& ranged
	): Node<bool, I...>(scalar, ranged), lhs(lhs), rhs(rhs) {
	}

public:
	friend class TypedPool<LessThanNode>;

	__host__
	static Pointer<LessThanNode<I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<float, I...>>& lhs,
		const Pointer<const Node<float, I...>>& rhs)
	{
		return Pointer<LessThanNode<I...>>::make(
			pool, lhs, rhs,
			typename Node<bool, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<bool, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* LESSTHANNODE_CUH_ */

#ifndef GREATERTHANNODE_CUH_
#define GREATERTHANNODE_CUH_

#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename... I> class GreaterThanNode: public Node<bool, I...> {
private:
	const Pointer<const Node<float, I...>> lhs;
	const Pointer<const Node<float, I...>> rhs;

	__host__ __device__
	static bool scalar_(const Node<bool, I...>* node, const I&... input) {
		const GreaterThanNode<I...>* this_ = static_cast<const GreaterThanNode<I...>*>(node);
		return (*this_->lhs)(input...) > (*this_->rhs)(input...);
	}

	__host__ __device__
	static typename ranged<bool>::type ranged_(const Node<bool, I...>* node, const typename ranged<I>::type&... input) {
		const GreaterThanNode<I...>* this_ = static_cast<const GreaterThanNode<I...>*>(node);
		return (*this_->lhs)(input...) > (*this_->rhs)(input...);
	}

	__host__
	GreaterThanNode (
		const Pointer<const Node<float, I...>>& lhs,
		const Pointer<const Node<float, I...>>& rhs,
		const typename Node<bool, I...>::scalar_eval_t& scalar,
		const typename Node<bool, I...>::ranged_eval_t& ranged
	): Node<bool, I...>(scalar, ranged), lhs(lhs), rhs(rhs) {
	}

public:
	friend class TypedPool<GreaterThanNode>;

	__host__
	static Pointer<GreaterThanNode<I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<float, I...>>& lhs,
		const Pointer<const Node<float, I...>>& rhs)
	{
		return Pointer<GreaterThanNode<I...>>::make(
			pool, lhs, rhs,
			typename Node<bool, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<bool, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* GREATERTHANNODE_CUH_ */

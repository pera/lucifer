#ifndef ANDNODE_CUH_
#define ANDNODE_CUH_

#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename... I> class AndNode: public Node<bool, I...> {
private:
	const Pointer<const Node<bool, I...>> lhs;
	const Pointer<const Node<bool, I...>> rhs;

	__host__ __device__
	static bool scalar_(const Node<bool, I...>* node, const I&... input) {
		const AndNode<I...>* this_ = static_cast<const AndNode<I...>*>(node);
		return (*this_->lhs)(input...) && (*this_->rhs)(input...);
	}

	__host__ __device__
	static typename ranged<bool>::type ranged_(const Node<bool, I...>* node, const typename ranged<I>::type&... input) {
		const AndNode<I...>* this_ = static_cast<const AndNode<I...>*>(node);
		return (*this_->lhs)(input...) && (*this_->rhs)(input...);
	}

	__host__
	AndNode(
		const Pointer<const Node<bool, I...>>& lhs,
		const Pointer<const Node<bool, I...>>& rhs,
		const typename Node<bool, I...>::scalar_eval_t& scalar,
		const typename Node<bool, I...>::ranged_eval_t& ranged
	): Node<bool, I...>(scalar, ranged), lhs(lhs), rhs(rhs) {
	}

public:
	friend class TypedPool<AndNode>;

	__host__
	static Pointer<AndNode<I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<bool, I...>>& lhs,
		const Pointer<const Node<bool, I...>>& rhs)
	{
		return Pointer<AndNode<I...>>::make(
			pool, lhs, rhs,
			typename Node<bool, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<bool, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* ANDNODE_CUH_ */

#ifndef NOTNODE_CUH_
#define NOTNODE_CUH_

#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename... I> class NotNode: public Node<bool, I...> {
private:
	const Pointer<const Node<bool, I...>> arg;

	__host__ __device__
	static bool scalar_(const Node<bool, I...>* node, const I&... input) {
		const Node<bool, I...>* arg = static_cast<const NotNode<I...>*>(node)->arg.get();
		return !(*arg)(input...);
	}

	__host__ __device__
	static typename ranged<bool>::type ranged_(const Node<bool, I...>* node, const typename ranged<I>::type&... input) {
		const Node<bool, I...>* arg = static_cast<const NotNode<I...>*>(node)->arg.get();
		return !(*arg)(input...);
	}

	__host__
	NotNode(
		const Pointer<const Node<bool, I...>>& arg,
		const typename Node<bool, I...>::scalar_eval_t& scalar,
		const typename Node<bool, I...>::ranged_eval_t& ranged
	): Node<bool, I...>(scalar, ranged), arg(arg) {
	}

public:
	friend class TypedPool<NotNode>;

	__host__
	static Pointer<NotNode<I...>> build(MemoryPool& pool, const Pointer<const Node<bool, I...>>& arg) {
		return Pointer<NotNode<I...>>::make(
			pool, arg,
			typename Node<bool, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<bool, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* NOTNODE_CUH_ */

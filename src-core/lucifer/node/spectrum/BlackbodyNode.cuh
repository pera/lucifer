#ifndef BLACKBODYNODE_CUH_
#define BLACKBODYNODE_CUH_

#include "../Node.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename... I> class BlackbodyNode: public Node<float, I...> {
private:
	static constexpr float W   = 2.897772126E-03f;
	static constexpr float C   = 2.997924580E+08f;
	static constexpr float H   = 6.626069570E-34f;
	static constexpr float K   = 1.380648800E-23f;
	static constexpr float HC  = H * C;
	static constexpr float HCC = H * C * C;

private:
	const Pointer<const Node<float, I...>> wavelength;
	const Pointer<const Node<float, I...>> temperature;
	const bool normalize;
	const float minWavelength;
	const float maxWavelength;

	__host__ __device__ constexpr
	static float pow5(const float x) {
		return x * x * x * x * x;
	}

	__host__ __device__ constexpr
	static Range<float> pow5(const Range<float>& x) {
		return Range<float>{pow5(x.lower), pow5(x.upper)};
	}

	template<typename T> __host__ __device__
	static T radiance(const T& w, const T& t) {
		return (2.f * PI<float>() * HCC) / (pow5(w) * expm1(+HC / (w * +K * t)));
	}

	template<typename T> __host__ __device__
	T peakWavelength(const T& t) const {
		return min(T(maxWavelength), max(T(minWavelength), +W / t));
	}

	template<typename T> __host__ __device__
	T evaluate(const T& w, const T& t) const {
		const auto value = radiance(w, t);
		if (normalize) {
			return value / radiance(peakWavelength(t), t);
		}
		return value;
	}

	__host__ __device__
	static float scalar_(const Node<float, I...>* node, const I&... input) {
		auto this_ = static_cast<const BlackbodyNode<I...>*>(node);
		return this_->evaluate((*this_->wavelength)(input...), (*this_->temperature)(input...));
	}

	__host__ __device__
	static typename ranged<float>::type ranged_(const Node<float, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const BlackbodyNode<I...>*>(node);
		return this_->evaluate((*this_->wavelength)(input...), (*this_->temperature)(input...));
	}

	__host__
	BlackbodyNode(
		const Pointer<const Node<float, I...>>& wavelength,
		const Pointer<const Node<float, I...>>& temperature,
		const bool normalize,
		const float minWavelength,
		const float maxWavelength,
		const typename Node<float, I...>::scalar_eval_t& scalar,
		const typename Node<float, I...>::ranged_eval_t& ranged
	):
		Node<float, I...>(scalar, ranged),
		wavelength(wavelength),
		temperature(temperature),
		normalize(normalize),
		minWavelength(minWavelength),
		maxWavelength(maxWavelength) {
	}

public:
	friend class TypedPool<BlackbodyNode>;

	__host__
	static Pointer<BlackbodyNode<I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<float, I...>>& wavelength,
		const Pointer<const Node<float, I...>>& temperature,
		const bool normalize,
		const float minWavelength,
		const float maxWavelength)
	{
		return Pointer<BlackbodyNode<I...>>::make(
			pool,  wavelength, temperature, normalize, minWavelength, maxWavelength,
			typename Node<float, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<float, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* BLACKBODYNODE_CUH_ */

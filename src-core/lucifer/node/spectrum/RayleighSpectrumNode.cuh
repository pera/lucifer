#ifndef RAYLEIGHSPECTRUMNODE_CUH_
#define RAYLEIGHSPECTRUMNODE_CUH_

#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*
 * Display of the Earth taking into account atmospheric scattering
 * - Tomoyuki Nishita
 * - Takao Sirai
 * - Katsumi Tadamura
 * - Eihachiro Nakamae
 */
template<typename... I> class RayleighSpectrumNode: public Node<float, I...> {
private:
	friend class TypedPool<RayleighSpectrumNode>;
	const Pointer<const Node<float, I...>> wavelength;
	const Pointer<const Node<float, I...>> height;
	const Pointer<const Node<float, I...>> density;
	const Pointer<const Node<float, I...>> ior;

private:
	static constexpr float __RAYLEIGH_HEIGHT__ = 7994.f;

	template<typename T> __host__ __device__
	static constexpr T rho(const T h) {
		return exp(-h / +__RAYLEIGH_HEIGHT__);
	}

	template<typename T> __host__ __device__
	static constexpr T raise2(const T x) {
		return x * x;
	}

	template<typename T> __host__ __device__
	static constexpr T raise3(const T x) {
		return x * raise2(x);
	}

	template<typename T> __host__ __device__
	static constexpr T raise4(const T x) {
		return raise2(raise2(x));
	}

	__host__ __device__
	static float scalar_(const Node<float, I...>* node, const I&... input) {
		auto this_ = static_cast<const RayleighSpectrumNode<I...>*>(node);
		return raise2(raise2((*this_->ior)(input...)) - 1.f) * rho((*this_->height)(input...)) * 8.f * raise3(PI<float>()) / ((*this_->density)(input...) * raise4((*this_->wavelength)(input...)) * 3.f);
	}

	__host__ __device__
	static typename ranged<float>::type ranged_(const Node<float, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const RayleighSpectrumNode<I...>*>(node);
		return raise2(raise2((*this_->ior)(input...)) - 1.f) * rho((*this_->height)(input...)) * 8.f * raise3(PI<float>()) / ((*this_->density)(input...) * raise4((*this_->wavelength)(input...)) * 3.f);
	}

	__host__
	RayleighSpectrumNode(
		const Pointer<const Node<float, I...>>& wavelength,
		const Pointer<const Node<float, I...>>& height,
		const Pointer<const Node<float, I...>>& density,
		const Pointer<const Node<float, I...>>& ior,
		const typename Node<float, I...>::scalar_eval_t& scalar,
		const typename Node<float, I...>::ranged_eval_t& ranged
	): Node<float, I...>(scalar, ranged), wavelength(wavelength), height(height), density(density), ior(ior) {
	}

public:
	__host__
	static Pointer<RayleighSpectrumNode<I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<float, I...>>& wavelength,
		const Pointer<const Node<float, I...>>& height,
		const Pointer<const Node<float, I...>>& density,
		const Pointer<const Node<float, I...>>& ior)
	{
		return Pointer<RayleighSpectrumNode<I...>>::make(
			pool, wavelength, height, density, ior,
			typename Node<float, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<float, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* RAYLEIGHSPECTRUMNODE_CUH_ */

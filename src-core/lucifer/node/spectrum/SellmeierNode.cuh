#ifndef SELLMEIERNODE_CUH_
#define SELLMEIERNODE_CUH_

#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename... I> class SellmeierNode: public Node<float, I...> {
private:
	friend class TypedPool<SellmeierNode>;
	const Pointer<const Node<float, I...>> wavelength;
	const float b[3];
	const float c[3];

	__host__ __device__
	static float scalar_(const Node<float, I...>* node, const I&... input) {
		auto this_ = static_cast<const SellmeierNode<I...>*>(node);
		float lE2 = pow2((*this_->wavelength)(input...) * 1E6f);
		float ior = 1.f;
#pragma unroll
		for (uint8_t i = 0; i < 3; ++i) {
			ior += (lE2 * this_->b[i]) / (lE2 - this_->c[i]);
		}
		return sqrt(max(ior, 0.f));
	}

	__host__ __device__
	static typename ranged<float>::type ranged_(const Node<float, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const SellmeierNode<I...>*>(node);
		RangeF lE2 = pow2((*this_->wavelength)(input...) * 1E6f);
		RangeF ior{1.f};
#pragma unroll
		for (uint8_t i = 0; i < 3; ++i) {
			ior += (lE2 * this_->b[i]) / (lE2 - this_->c[i]);
		}
		return sqrt(max(ior, RangeF(0.f)));
	}

	__host__
	SellmeierNode(
		const Pointer<const Node<float, I...>>& wavelength,
		const float b0, const float b1, const float b2,
		const float c0, const float c1, const float c2,
		const typename Node<float, I...>::scalar_eval_t& scalar,
		const typename Node<float, I...>::ranged_eval_t& ranged
	): Node<float, I...>(scalar, ranged), wavelength(wavelength), b{b0, b1, b2}, c{c0, c1, c2} {
	}

public:
	__host__
	static Pointer<SellmeierNode<I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<float, I...>>& wavelength,
		const float b0, const float b1, const float b2,
		const float c0, const float c1, const float c2)
	{
		return Pointer<SellmeierNode<I...>>::make(
			pool, wavelength, b0, b1, b2, c0, c1, c2,
			typename Node<float, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<float, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* SELLMEIERNODE_CUH_ */

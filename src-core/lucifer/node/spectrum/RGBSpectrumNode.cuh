#ifndef RGBSPECTRUMNODE_CUH_
#define RGBSPECTRUMNODE_CUH_

#include "../Node.cuh"
#include "lucifer/color/RGBColorSpace.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename... I> class RGBSpectrumNode: public Node<float, I...> {
private:
	friend class TypedPool<RGBSpectrumNode>;
	const Pointer<const Node<Color3F, I...>> color;
	const Pointer<const Node<float, I...>> wavelength;
	const Pointer<const RGBColorSpace> space;

	__host__ __device__
	static float scalar_(const Node<float, I...>* node, const I&... input) {
		auto this_ = static_cast<const RGBSpectrumNode<I...>*>(node);
		return this_->space->spectrum((*this_->color)(input...), (*this_->wavelength)(input...), 0.f, +INFINITY);
	}

	__host__ __device__
	static typename ranged<float>::type ranged_(const Node<float, I...>* node, const typename ranged<I>::type&... input) {
		// TODO: implement ranged RGBSpectrum
		return typename ranged<float>::type{};
//		auto this_ = static_cast<const RGBSpectrumNode<I...>*>(node);
//		auto rgb = (*this_->color)(input...);
//		this_->space->gamma()->decode(rgb);
//		return rgb.minElement().hull(rgb.maxElement());
	}

	__host__
	RGBSpectrumNode(
		const Pointer<const Node<Color3F, I...>>& color,
		const Pointer<const Node<float, I...>>& wavelength,
		const Pointer<const RGBColorSpace>& space,
		const typename Node<float, I...>::scalar_eval_t& scalar,
		const typename Node<float, I...>::ranged_eval_t& ranged
	): Node<float, I...>(scalar, ranged), color(color), wavelength(wavelength), space(space) {
	}

public:
	__host__
	static Pointer<RGBSpectrumNode<I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<Color3F, I...>>& color,
		const Pointer<const Node<float, I...>>& wavelength,
		const Pointer<const RGBColorSpace>& space)
	{
		return Pointer<RGBSpectrumNode<I...>>::make(
			pool, color, wavelength, space,
			typename Node<float, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<float, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* RGBSPECTRUMNODE_CUH_ */

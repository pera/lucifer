#ifndef CONVERTERNODE_CUH_
#define CONVERTERNODE_CUH_

#include "Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename O, typename M, typename... I> class ConverterNode: public Node<O, I...> {
private:
	friend class TypedPool<ConverterNode>;
	const Pointer<const Node<M, I...>> value;

	__host__ __device__
	static O scalar_(const Node<O, I...>* node, const I&... input) {
		auto this_ = static_cast<const ConverterNode<O, M, I...>*>(node);
		return O((*this_->value)(input...));
	}

	__host__ __device__
	static typename ranged<O>::type ranged_(const Node<O, I...>* node, const typename ranged<I>::type&... input) {
		auto this_ = static_cast<const ConverterNode<O, M, I...>*>(node);
		return typename ranged<O>::type((*this_->value)(input...));
	}

	__host__
	ConverterNode(
		const Pointer<const Node<M, I...>>& value,
		const typename Node<O, I...>::scalar_eval_t& scalar,
		const typename Node<O, I...>::ranged_eval_t& ranged
	): Node<O, I...>(scalar, ranged), value(value) {
	}

public:
	friend class TypedPool<ConverterNode>;

	__host__
	static Pointer<ConverterNode<O, M, I...>> build(MemoryPool& pool, const Pointer<const Node<M, I...>>& value) {
		return Pointer<ConverterNode<O, M, I...>>::make(
			pool, value,
			typename Node<O, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<O, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* CONVERTERNODE_CUH_ */

#ifndef MINIMUMNODE_CUH_
#define MINIMUMNODE_CUH_

#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename O, typename... I> class MinimumNode: public Node<O, I...> {
private:
	friend class TypedPool<MinimumNode>;
	const Pointer<const Node<O, I...>> lhs;
	const Pointer<const Node<O, I...>> rhs;

	__host__ __device__
	static O scalar_(const Node<O, I...>* node, const I&... input) {
		const MinimumNode<O, I...>* this_ = static_cast<const MinimumNode<O, I...>*>(node);
		return min((*this_->lhs)(input...), (*this_->rhs)(input...));
	}

	__host__ __device__
	static typename ranged<O>::type ranged_(const Node<O, I...>* node, const typename ranged<I>::type&... input) {
		const MinimumNode<O, I...>* this_ = static_cast<const MinimumNode<O, I...>*>(node);
		return min((*this_->lhs)(input...), (*this_->rhs)(input...));
	}

	__host__
	MinimumNode(
		const Pointer<const Node<O, I...>>& lhs,
		const Pointer<const Node<O, I...>>& rhs,
		const typename Node<O, I...>::scalar_eval_t& scalar,
		const typename Node<O, I...>::ranged_eval_t& ranged
	): Node<O, I...>(scalar, ranged), lhs(lhs), rhs(rhs) {
	}


public:
	friend class TypedPool<MinimumNode>;

	__host__
	static Pointer<MinimumNode<O, I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<O, I...>>& lhs,
		const Pointer<const Node<O, I...>>& rhs)
	{
		return Pointer<MinimumNode<O, I...>>::make(
			pool, lhs, rhs,
			typename Node<O, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<O, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* MINIMUMNODE_CUH_ */

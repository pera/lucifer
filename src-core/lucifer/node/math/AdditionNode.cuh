#ifndef ADDITIONNODE_CUH_
#define ADDITIONNODE_CUH_

#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename O, typename... I> class AdditionNode: public Node<O, I...> {
private:
	const Pointer<const Node<O, I...>> lhs;
	const Pointer<const Node<O, I...>> rhs;

	__host__ __device__
	static O scalar_(const Node<O, I...>* node, const I&... input) {
		const AdditionNode<O, I...>* this_ = static_cast<const AdditionNode<O, I...>*>(node);
		return (*this_->lhs)(input...) + (*this_->rhs)(input...);
	}

	__host__ __device__
	static typename ranged<O>::type ranged_(const Node<O, I...>* node, const typename ranged<I>::type&... input) {
		const AdditionNode<O, I...>* this_ = static_cast<const AdditionNode<O, I...>*>(node);
		return (*this_->lhs)(input...) + (*this_->rhs)(input...);
	}

	__host__
	AdditionNode(
		const Pointer<const Node<O, I...>>& lhs,
		const Pointer<const Node<O, I...>>& rhs,
		const typename Node<O, I...>::scalar_eval_t& scalar,
		const typename Node<O, I...>::ranged_eval_t& ranged
	): Node<O, I...>(scalar, ranged), lhs(lhs), rhs(rhs) {
	}


public:
	friend class TypedPool<AdditionNode>;

	__host__
	static Pointer<AdditionNode<O, I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<O, I...>>& lhs,
		const Pointer<const Node<O, I...>>& rhs)
	{
		return Pointer<AdditionNode<O, I...>>::make(
			pool, lhs, rhs,
			typename Node<O, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<O, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* ADDITIONNODE_CUH_ */

#ifndef MULTIPLICATIONNODE_CUH_
#define MULTIPLICATIONNODE_CUH_

#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename L, typename R, typename... I> class MultiplicationNode: public Node<L, I...> {
private:
	friend class TypedPool<MultiplicationNode>;
	const Pointer<const Node<L, I...>> lhs;
	const Pointer<const Node<R, I...>> rhs;

	__host__ __device__
	static L scalar_(const Node<L, I...>* node, const I&... input) {
		const MultiplicationNode<L, R, I...>* this_ = static_cast<const MultiplicationNode<L, R, I...>*>(node);
		return (*this_->lhs)(input...) * (*this_->rhs)(input...);
	}

	__host__ __device__
	static typename ranged<L>::type ranged_(const Node<L, I...>* node, const typename ranged<I>::type&... input) {
		const MultiplicationNode<L, R, I...>* this_ = static_cast<const MultiplicationNode<L, R, I...>*>(node);
		return (*this_->lhs)(input...) * (*this_->rhs)(input...);
	}

	__host__
	MultiplicationNode(
		const Pointer<const Node<L, I...>>& lhs,
		const Pointer<const Node<R, I...>>& rhs,
		const typename Node<L, I...>::scalar_eval_t& scalar,
		const typename Node<L, I...>::ranged_eval_t& ranged
	): Node<L, I...>(scalar, ranged), lhs(lhs), rhs(rhs) {
	}


public:
	friend class TypedPool<MultiplicationNode>;

	__host__
	static Pointer<MultiplicationNode<L, R, I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<L, I...>>& lhs,
		const Pointer<const Node<R, I...>>& rhs)
	{
		return Pointer<MultiplicationNode<L, R, I...>>::make(
			pool, lhs, rhs,
			typename Node<L, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<L, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* MULTIPLICATIONNODE_CUH_ */

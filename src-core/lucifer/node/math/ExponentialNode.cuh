#ifndef EXPONENTIALNODE_CUH_
#define EXPONENTIALNODE_CUH_

#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename O, typename... I> class ExponentialNode: public Node<O, I...> {
private:
	friend class TypedPool<ExponentialNode>;
	const Pointer<const Node<O, I...>> arg;

	__host__ __device__
	static O scalar_(const Node<O, I...>* node, const I&... input) {
		const Node<O,I...>* arg = static_cast<const ExponentialNode<O, I...>*>(node)->arg.get();
		return exp((*arg)(input...));
	}

	__host__ __device__
	static typename ranged<O>::type ranged_(const Node<O, I...>* node, const typename ranged<I>::type&... input) {
		const Node<O,I...>* arg = static_cast<const ExponentialNode<O, I...>*>(node)->arg.get();
		return exp((*arg)(input...));
	}

	__host__
	ExponentialNode(
		const Pointer<const Node<O, I...>>& arg,
		const typename Node<O, I...>::scalar_eval_t& scalar,
		const typename Node<O, I...>::ranged_eval_t& ranged
	): Node<O, I...>(scalar, ranged), arg(arg) {
	}

public:
	friend class TypedPool<ExponentialNode>;

	__host__
	static Pointer<ExponentialNode<O, I...>> build(MemoryPool& pool, const Pointer<const Node<O, I...>>& arg) {
		return Pointer<ExponentialNode<O, I...>>::make(
			pool, arg,
			typename Node<O, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<O, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* EXPONENTIALNODE_CUH_ */

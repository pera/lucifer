#ifndef SQUAREROOTNODE_CUH_
#define SQUAREROOTNODE_CUH_

#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename O, typename... I> class SquareRootNode: public Node<O, I...> {
private:
	friend class TypedPool<SquareRootNode>;
	const Pointer<const Node<O, I...>> arg;

	__host__ __device__
	static O scalar_(const Node<O, I...>* node, const I&... input) {
		const Node<O,I...>* arg = static_cast<const SquareRootNode<O, I...>*>(node)->arg.get();
		return sqrt((*arg)(input...));
	}

	__host__ __device__
	static typename ranged<O>::type ranged_(const Node<O, I...>* node, const typename ranged<I>::type&... input) {
		const Node<O,I...>* arg = static_cast<const SquareRootNode<O, I...>*>(node)->arg.get();
		return sqrt((*arg)(input...));
	}

	__host__
	SquareRootNode(
		const Pointer<const Node<O, I...>>& arg,
		const typename Node<O, I...>::scalar_eval_t& scalar,
		const typename Node<O, I...>::ranged_eval_t& ranged
	): Node<O, I...>(scalar, ranged), arg(arg) {
	}

public:
	friend class TypedPool<SquareRootNode>;

	__host__
	static Pointer<SquareRootNode<O, I...>> build(MemoryPool& pool, const Pointer<const Node<O, I...>>& arg) {
		return Pointer<SquareRootNode<O, I...>>::make(
			pool, arg,
			typename Node<O, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<O, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* SQUAREROOTNODE_CUH_ */

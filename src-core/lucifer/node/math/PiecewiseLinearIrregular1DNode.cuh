#ifndef PIECEWISELINEARIRREGULAR1DNODE_CUH_
#define PIECEWISELINEARIRREGULAR1DNODE_CUH_

#include "../Node.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename O, typename... I> class PiecewiseLinearIrregular1DNode: public Node<O, I...> {
public:
	friend class TypedPool<PiecewiseLinearIrregular1DNode>;
	using index_t = uint16_t;
	using x_arr_t = Array<float, index_t>;
	using y_arr_t = Array<O, index_t>;

private:
	const Pointer<const Node<float, I...>> arg;
	const x_arr_t xValues;
	const y_arr_t yValues;

private:
	__host__ __device__
	O lerp(const int k, const float at) const {
		return yValues[k] + (((at - xValues[k]) * (yValues[k + 1] - yValues[k])) / (xValues[k + 1] - xValues[k]));
	}

	__host__ __device__
	int binarySearch(const float at, const int l, const int u) const {
		if ((u - l) < 2) return l;
		const int m = (l + u) / 2;
		return at < xValues[m] ? binarySearch(at, l, m) : binarySearch(at, m, u);
	}

	__host__ __device__
	static O scalar_(const Node<O, I...>* node, const I&... input) {
		auto this_ = static_cast<const PiecewiseLinearIrregular1DNode*>(node);
		const float x = (*this_->arg)(input...);
		return
			(x < this_->xValues[0] || x > this_->xValues[this_->xValues.size() - 1]) ?
				0.f :
				this_->lerp(this_->binarySearch(x, 0, this_->xValues.size() - 1), x);
	}

	__host__ __device__
	static typename ranged<O>::type ranged_(const Node<O, I...>* node, const typename ranged<I>::type&... input) {
		using output_type = typename ranged<O>::type;
		auto this_ = static_cast<const PiecewiseLinearIrregular1DNode*>(node);
		const RangeF x = (*this_->arg)(input...);
		const index_t indexMin =
			x.lower <= this_->xValues[0] ?
				0 :
				this_->binarySearch(x.lower, 0, this_->xValues.size() - 1) + 0;
		const index_t indexMax =
			x.upper >= this_->xValues[this_->xValues.size() - 1] ?
				this_->xValues.size() - 1 :
				this_->binarySearch(x.upper, 0, this_->xValues.size() - 1) + 1;
		output_type result{this_->yValues[indexMin]};
		for (index_t i = indexMin + 1; i <= indexMax; ++i) {
			result = hull(result, output_type{this_->yValues[i]});
		}
		return result;
	}

	__host__
	PiecewiseLinearIrregular1DNode(
		const Pointer<const Node<float, I...>>& arg,
		const x_arr_t& xValues,
		const y_arr_t& yValues,
		const typename Node<O, I...>::scalar_eval_t& scalar,
		const typename Node<O, I...>::ranged_eval_t& ranged
	):Node<O, I...>(scalar, ranged), arg(arg), xValues(xValues), yValues(yValues) {
	}

public:
	__host__
	static Pointer<PiecewiseLinearIrregular1DNode<O, I...>> build(
			MemoryPool& pool,
			const Pointer<const Node<float, I...>>& arg,
			const x_arr_t& xValues,
			const y_arr_t& yValues
		) {
		return Pointer<PiecewiseLinearIrregular1DNode<O, I...>>::make(
			pool, arg, xValues, yValues,
			typename Node<O, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<O, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* PIECEWISELINEARIRREGULAR1DNODE_CUH_ */

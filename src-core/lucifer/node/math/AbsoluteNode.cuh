#ifndef ABSOLUTENODE_CUH_
#define ABSOLUTENODE_CUH_

#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename O, typename... I> class AbsoluteNode: public Node<O, I...> {
private:
	friend class TypedPool<AbsoluteNode>;
	const Pointer<const Node<O, I...>> arg;

	__host__ __device__
	static O scalar_(const Node<O, I...>* node, const I&... input) {
		const Node<O,I...>* arg = static_cast<const AbsoluteNode<O, I...>*>(node)->arg.get();
		return abs((*arg)(input...));
	}

	__host__ __device__
	static typename ranged<O>::type ranged_(const Node<O, I...>* node, const typename ranged<I>::type&... input) {
		const Node<O,I...>* arg = static_cast<const AbsoluteNode<O, I...>*>(node)->arg.get();
		return abs((*arg)(input...));
	}

	__host__
	AbsoluteNode(
		const Pointer<const Node<O, I...>>& arg,
		const typename Node<O, I...>::scalar_eval_t& scalar,
		const typename Node<O, I...>::ranged_eval_t& ranged
	): Node<O, I...>(scalar, ranged), arg(arg) {
	}

public:
	__host__
	static Pointer<AbsoluteNode<O, I...>> build(MemoryPool& pool, const Pointer<const Node<O, I...>>& arg) {
		return Pointer<AbsoluteNode<O, I...>>::make(
			pool, arg,
			typename Node<O, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<O, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* ABSOLUTENODE_CUH_ */

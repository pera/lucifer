#ifndef DIVISIONNODE_CUH_
#define DIVISIONNODE_CUH_

#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename L, typename R, typename... I> class DivisionNode: public Node<L, I...> {
private:
	friend class TypedPool<DivisionNode>;
	const Pointer<const Node<L, I...>> lhs;
	const Pointer<const Node<R, I...>> rhs;

	__host__ __device__
	static L scalar_(const Node<L, I...>* node, const I&... input) {
		const DivisionNode<L, R, I...>* this_ = static_cast<const DivisionNode<L, R, I...>*>(node);
		return (*this_->lhs)(input...) / (*this_->rhs)(input...);
	}

	__host__ __device__
	static typename ranged<L>::type ranged_(const Node<L, I...>* node, const typename ranged<I>::type&... input) {
		const DivisionNode<L, R, I...>* this_ = static_cast<const DivisionNode<L, R, I...>*>(node);
		return (*this_->lhs)(input...) / (*this_->rhs)(input...);
	}

	__host__
	DivisionNode(
		const Pointer<const Node<L, I...>>& lhs,
		const Pointer<const Node<R, I...>>& rhs,
		const typename Node<L, I...>::scalar_eval_t& scalar,
		const typename Node<L, I...>::ranged_eval_t& ranged
	): Node<L, I...>(scalar, ranged), lhs(lhs), rhs(rhs) {
	}


public:
	friend class TypedPool<DivisionNode>;

	__host__
	static Pointer<DivisionNode<L, R, I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<L, I...>>& lhs,
		const Pointer<const Node<R, I...>>& rhs)
	{
		return Pointer<DivisionNode<L, R, I...>>::make(
			pool, lhs, rhs,
			typename Node<L, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<L, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* DIVISIONNODE_CUH_ */

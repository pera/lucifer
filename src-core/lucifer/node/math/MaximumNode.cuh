#ifndef MAXIMUMNODE_CUH_
#define MAXIMUMNODE_CUH_

#include "../Node.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename O, typename... I> class MaximumNode: public Node<O, I...> {
private:
	friend class TypedPool<MaximumNode>;
	const Pointer<const Node<O, I...>> lhs;
	const Pointer<const Node<O, I...>> rhs;

	__host__ __device__
	static O scalar_(const Node<O, I...>* node, const I&... input) {
		const MaximumNode<O, I...>* this_ = static_cast<const MaximumNode<O, I...>*>(node);
		return max((*this_->lhs)(input...), (*this_->rhs)(input...));
	}

	__host__ __device__
	static typename ranged<O>::type ranged_(const Node<O, I...>* node, const typename ranged<I>::type&... input) {
		const MaximumNode<O, I...>* this_ = static_cast<const MaximumNode<O, I...>*>(node);
		return max((*this_->lhs)(input...), (*this_->rhs)(input...));
	}

	__host__
	MaximumNode(
		const Pointer<const Node<O, I...>>& lhs,
		const Pointer<const Node<O, I...>>& rhs,
		const typename Node<O, I...>::scalar_eval_t& scalar,
		const typename Node<O, I...>::ranged_eval_t& ranged
	): Node<O, I...>(scalar, ranged), lhs(lhs), rhs(rhs) {
	}


public:
	friend class TypedPool<MaximumNode>;

	__host__
	static Pointer<MaximumNode<O, I...>> build(
		MemoryPool& pool,
		const Pointer<const Node<O, I...>>& lhs,
		const Pointer<const Node<O, I...>>& rhs)
	{
		return Pointer<MaximumNode<O, I...>>::make(
			pool, lhs, rhs,
			typename Node<O, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<O, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* MAXIMUMNODE_CUH_ */

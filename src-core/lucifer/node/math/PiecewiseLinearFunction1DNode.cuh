#ifndef PIECEWISELINEARFUNCTION1DNODE_CUH_
#define PIECEWISELINEARFUNCTION1DNODE_CUH_

#include "../Node.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

template<typename O, typename... I> class PiecewiseLinearFunction1DNode: public Node<O, I...> {
public:
	friend class TypedPool<PiecewiseLinearFunction1DNode>;
	using index_t = uint16_t;
	using array_t = Array<O, index_t>;

private:
	const Pointer<const Node<float, I...>> arg;
	const float lower;
	const float delta;
	const array_t values;

	__host__ __device__
	static O scalar_(const Node<O, I...>* node, const I&... input) {
		auto this_ = static_cast<const PiecewiseLinearFunction1DNode*>(node);
		const float x = max(this_->lower, (*this_->arg)(input...));
		const  auto i = (index_t) ((x - this_->lower) / this_->delta);
	    return lerp(
			this_->values[clamp<index_t>(i + 0, 0, this_->values.size() - 1)],
			this_->values[clamp<index_t>(i + 1, 0, this_->values.size() - 1)],
			(x - (this_->lower + i * this_->delta)) / this_->delta
		);
	}

	__host__ __device__
	static typename ranged<O>::type ranged_(const Node<O, I...>* node, const typename ranged<I>::type&... input) {
		using output_type = typename ranged<O>::type;
		auto this_ = static_cast<const PiecewiseLinearFunction1DNode*>(node);
		const RangeF x = (*this_->arg)(input...);
		const index_t indexMin = clamp<index_t>(0 + (x.lower - this_->lower) / this_->delta, 0, this_->values.size() - 1);
		const index_t indexMax = clamp<index_t>(1 + (x.upper - this_->lower) / this_->delta, 0, this_->values.size() - 1);
		output_type result = output_type(this_->values[indexMin]);
		for (index_t i = indexMin + 1; i <= indexMax; ++i) {
			result = hull(result, output_type(this_->values[i]));
		}
		return result;
	}

	__host__
	PiecewiseLinearFunction1DNode(
		const Pointer<const Node<float, I...>>& arg,
		const float lower,
		const float upper,
		const array_t& values,
		const typename Node<O, I...>::scalar_eval_t& scalar,
		const typename Node<O, I...>::ranged_eval_t& ranged
	):Node<O, I...>(scalar, ranged), arg(arg), lower(lower), delta((upper - lower) / (values.size() - 1.f)), values(values) {
	}

public:
	friend class TypedPool<PiecewiseLinearFunction1DNode>;

	__host__
	static Pointer<PiecewiseLinearFunction1DNode<O, I...>> build(
			MemoryPool& pool,
			const Pointer<const Node<float, I...>>& arg,
			const float lower,
			const float upper,
			const array_t& values
		) {
		return Pointer<PiecewiseLinearFunction1DNode<O, I...>>::make(
			pool, arg, lower, upper, values,
			typename Node<O, I...>::scalar_eval_t(virtualize(scalar_)),
			typename Node<O, I...>::ranged_eval_t(virtualize(ranged_))
		);
	}
};

}

#endif /* PIECEWISELINEARFUNCTION1DNODE_CUH_ */

#include <cassert>
#include "LuciferAPI.h"
#include "lucifer/color/RGBColorSpace.cuh"
#include "lucifer/image/HDRImage.cuh"
#include "lucifer/image/HDRImageIO.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/memory/ManagedAllocator.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/util/Progress.cuh"
#include "lucifer/xml/XMLInput.cuh"
#include "lucifer/xml/XMLScene.cuh"
#include "lucifer/xml/generated/lucifer.hxx"

namespace lucifer {

Pointer<HDRImage<float, 3>> temp1{};
Pointer<HDRImage<float, 3>> temp2{};

Lucifer::Lucifer(const char* inputfile): exposure(1) {
	try {
		assert(inputfile != nullptr);
		std::unique_ptr<luc::scene> xmlScene(luc::scene_(inputfile));
		input_ = XMLScene::load(xmlScene);
	} catch (const xml_schema::exception& e) {
		std::cerr << e << std::endl;
		input_ = nullptr;
	}
}

void Lucifer::render() {
	XMLInput* input = (XMLInput*)input_;
	input->renderer->render(input->scene.get(), input->progressive);
}

void Lucifer::update(unsigned char* buffer) {
	size_t w = width();
	size_t h = height();
	// retrieve raw image preview
	if (temp2.isNull()) temp2 = Pointer<HDRImage<float, 3>>::make(Context::managedMemoryPool(), Context::managedMemoryPool(), w, h);
	((XMLInput*)input_)->renderer->preview(temp2.get());
	// tone map
	if (temp1.isNull()) temp1 = Pointer<HDRImage<float, 3>>::make(Context::managedMemoryPool(), Context::managedMemoryPool(), w, h);
	auto mapper = ((XMLInput*) input_)->preview.toneMapper.get();
	mapper->map(*temp2, *temp1);
	// write image to screen buffer
	Color3F rgb_;
	for (int i = 0; i < h; i++) {
		for (int j = 0; j < w; j++) {
			((XMLInput*) input_)->preview.colorSpace->toRGB((*temp1)(i, j), rgb_);
			rgb_ *= exposure;
			buffer[3 * (i * width() + j) + 0] = (unsigned char) (254.99f * clamp(rgb_[0], 0.f, 1.f));
			buffer[3 * (i * width() + j) + 1] = (unsigned char) (254.99f * clamp(rgb_[1], 0.f, 1.f));
			buffer[3 * (i * width() + j) + 2] = (unsigned char) (254.99f * clamp(rgb_[2], 0.f, 1.f));
		}
	}
}

bool Lucifer::preview() {
	return !((XMLInput*) input_)->preview.colorSpace.isNull();
}

bool Lucifer::autoUpdate() {
	return ((XMLInput*) input_)->preview.autoUpdate;
}

void Lucifer::increaseExposure() {
	exposure += 0.1;
}

void Lucifer::decreaseExposure() {
	if (exposure > 0.15) {
		exposure -= 0.1;
	}
}

void Lucifer::restoreDefaults() {
	exposure = 1.0;
}

void Lucifer::writePFM() {
	size_t w = width();
	size_t h = height();
	// retrieve raw image
	if (temp1.isNull()) temp1 = Pointer<HDRImage<float, 3>>::make(Context::managedMemoryPool(), Context::managedMemoryPool(), w, h);
	if (temp2.isNull()) temp2 = Pointer<HDRImage<float, 3>>::make(Context::managedMemoryPool(), Context::managedMemoryPool(), w, h);
	((XMLInput*)input_)->renderer->preview(temp2.get());
	for (int i = 0; i < ((XMLInput*) input_)->output.size(); i++) {
		// tone map
		XMLOutput& output = ((XMLInput*) input_)->output[i];
		const ToneMapper* mapper = output.toneMapper.get();
		mapper->map(*temp2, *temp1);
		// write image to disk
		std::ofstream file(output.filename);
		if (!file.is_open()) {
			std::cerr << "could not create output file: " << output.filename << std::endl;
		} else {
			HDRImageIO::writePFM(temp1, file);
			file.close();
//			std::cout << "output file " << output.filename << " written to disk" << std::endl;
		}
	}
}

size_t Lucifer::width() {
	return ((XMLInput*)input_)->scene->camera()->sensor()->width();
}

size_t Lucifer::height() {
	return ((XMLInput*)input_)->scene->camera()->sensor()->height();
}

float Lucifer::progress() {
	return ((XMLInput*)input_)->renderer->tracker()->percentDone();
}

void Lucifer::watchProgress() {
	trackProgress(((XMLInput*) input_)->renderer->tracker(), 50);
}

bool Lucifer::terminated() {
	return ((XMLInput*)input_)->renderer->tracker()->isTerminated();
}

float Lucifer::elapsedTime() {
	return ((XMLInput*)input_)->renderer->tracker()->elapsedTime();
}

unsigned char* Lucifer::bufferCreate() {
	size_t size = 3 * width() * height();
	auto result = new unsigned char[size];
	if (result != nullptr) {
		memset(result, 0, size * sizeof(unsigned char));
	}
	return result;
}

void Lucifer::bufferDestroy(unsigned char*& buffer) {
	if (buffer != nullptr) {
		delete[] buffer;
		buffer = nullptr;
	}
}

Lucifer::~Lucifer() {
	if (input_ != nullptr) {
		delete ((XMLInput*) input_);
		input_ = nullptr;
	}
	temp1 = Pointer<HDRImage<float, 3>>{};
	temp2 = Pointer<HDRImage<float, 3>>{};
}

}

#include <iostream>
#include <thread>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include "PreviewWindow.hpp"

namespace lucifer {

	GtkWidget* darea;
	unsigned char* buffer;
	GdkPixbuf* pixbuf;

	void doRender(Lucifer* luc) {
		((Lucifer*) luc)->render();
	}

	gpointer render(gpointer data) {
		std::thread worker(doRender, (Lucifer*) data);
		((Lucifer*) data)->watchProgress();
		worker.join();
		if (((Lucifer*) data)->preview()) {
			((Lucifer*) data)->update(buffer);
			gtk_widget_queue_draw(GTK_WIDGET(darea));
		}
		((Lucifer*) data)->writePFM();
		std::cout << "render time: " << ((Lucifer*) data)->elapsedTime() << "s\n";
		return NULL;
	}

	void map_callback(GtkWidget *widget, gpointer data) {
		GThread *thread;
		thread = g_thread_new ("render", render, data);
		g_thread_unref (thread);
	}

	gboolean timer_callback(gpointer data) {
		((Lucifer*) data)->update(buffer);
		gtk_widget_queue_draw(GTK_WIDGET(darea));
		if (((Lucifer*) data)->terminated()) {
			return FALSE;
		}
		return TRUE;
	}

	gboolean draw_callback(GtkWidget *widget, cairo_t *cr, gpointer data) {
		gdk_cairo_set_source_pixbuf(cr, pixbuf, 0, 0);
		cairo_paint(cr);
		return FALSE;
	}

	gboolean on_key_press(GtkWidget *widget, GdkEventKey *event, gpointer data) {
		if (event->keyval == '+') {
			((Lucifer*) data)->increaseExposure();
		}
		if (event->keyval == '-') {
			((Lucifer*) data)->decreaseExposure();
		}
		if (event->keyval == 'x' || event->keyval == 'X') {
			((Lucifer*) data)->writePFM();
		}
		if (event->keyval == 'r' || event->keyval == 'R') {
			((Lucifer*) data)->restoreDefaults();
		}
		if (event->keyval == 'u' || event->keyval == 'U') {
			if (((Lucifer*) data)->preview() && !((Lucifer*) data)->autoUpdate()) {
				timer_callback(data);
			}
		}
		if (event->keyval == 'q' || event->keyval == 'Q') {
			gtk_main_quit();
		}
		return FALSE;
	}

	void preview(int argc, char* argv[], Lucifer* luc) {
		if (luc->preview()) {
			gtk_init(&argc, &argv);
			size_t w = ((Lucifer*) luc)->width();
			size_t h = ((Lucifer*) luc)->height();
			buffer = ((Lucifer*) luc)->bufferCreate();
			if (buffer == NULL) {
				std::cerr << "not enough memory for image buffer" << std::endl;
				exit(1);
			} else {
				pixbuf = gdk_pixbuf_new_from_data(buffer, GdkColorspace::GDK_COLORSPACE_RGB, FALSE, 8, w, h, 3 * w, NULL, NULL);
				GtkWidget* window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
				gtk_window_set_title (GTK_WINDOW (window), argv[1]);
				gtk_window_set_default_size (GTK_WINDOW (window), luc->width(), luc->height());
				GdkGeometry geometry;
				geometry.max_width = geometry.min_width = luc->width();
				geometry.max_height = geometry.min_height = luc->height();
				gtk_window_set_geometry_hints(GTK_WINDOW (window), NULL, &geometry, (GdkWindowHints)(GDK_HINT_MIN_SIZE | GDK_HINT_MAX_SIZE));
				g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
				g_signal_connect(window, "map", G_CALLBACK(map_callback), luc);
				g_signal_connect(window, "key-press-event", G_CALLBACK(on_key_press), luc);
				darea = gtk_drawing_area_new();
				if (darea == NULL) {
					std::cerr << "not enough memory for gtk drawing area" << std::endl;
					exit(1);
				} else {
					gtk_container_add(GTK_CONTAINER(window), darea);
					g_signal_connect(darea, "draw", G_CALLBACK(draw_callback), luc);
					gtk_widget_set_app_paintable(darea, TRUE);
					if (luc->autoUpdate()) {
						gdk_threads_add_idle(timer_callback, luc);
					}
					gtk_widget_show_all(window);
					std::cout << "commands:" << std::endl;
					std::cout << "\t+ : increase exposure" << std::endl;
					std::cout << "\t- : decrease exposure" << std::endl;
					std::cout << "\tx : export output image" << std::endl;
					std::cout << "\tr : restore defaults" << std::endl;
					if (!luc->autoUpdate()) {
						std::cout << "\tu : update preview" << std::endl;
					}
					std::cout << "\tq : quit" << std::endl;
					gtk_main();
				}
			}
		} else {
			std::thread worker(doRender, luc);
			luc->watchProgress();
			worker.join();
//			std::cout << "elapsed time: " << ((Lucifer*) luc)->elapsedTime() << "\n";
			std::cout << ((Lucifer*) luc)->elapsedTime() << "\n";
			luc->writePFM();
		}
	}

}

#ifndef PREVIEWWINDOW_HPP_
#define PREVIEWWINDOW_HPP_

#include "LuciferAPI.h"

namespace lucifer {
	void preview(int argc, char* argv[], Lucifer* luc);
}

#endif /* PREVIEWWINDOW_HPP_ */

#ifndef LUCIFERAPI_H_
#define LUCIFERAPI_H_

namespace lucifer {

class Lucifer {
private:
	void* input_;
	float exposure;

public:
	Lucifer(const char* inputfile);
	size_t width();
	size_t height();
	void render();
	void update(unsigned char* buffer);
	bool preview();
	bool autoUpdate();
	void increaseExposure();
	void decreaseExposure();
	void restoreDefaults();
	void writePFM();
	float progress();
	void watchProgress();
	bool terminated();
	float elapsedTime();
	unsigned char* bufferCreate();
	void bufferDestroy(unsigned char*& buffer);
	static void test();
	~Lucifer();
};

}

#endif /* LUCIFERAPI_H_ */

#ifndef LINEARTONEMAPPER_CUH_
#define LINEARTONEMAPPER_CUH_

#include "ToneMapper.cuh"

namespace lucifer {

class LinearToneMapper: public ToneMapper {
public:
	__host__
	virtual void map(const HDRImage<float, 3>& input, HDRImage<float, 3>& output) const {
		(output = input).normalize();
	}

	__host__
	virtual ~LinearToneMapper() {
	}
};

}

#endif /* LINEARTONEMAPPER_CUH_ */

#ifndef REINHARDTONEMAPPER_CUH_
#define REINHARDTONEMAPPER_CUH_

#include "ToneMapper.cuh"

namespace lucifer {

#define __REINHARD_MINIMUM__ 1E-4

class ReinhardToneMapper: public ToneMapper {
private:
	const float key;

public:
	__host__
	ReinhardToneMapper(const float key):
	key(key) {
	}

	__host__
	virtual void map(const HDRImage<float, 3>& input, HDRImage<float, 3>& output) const {
		// compute luminance geometric mean
		size_t w = input.width();
		size_t h = input.height();
		float min_ = +INFINITY;
		float max_ = -INFINITY;
		float avg_ = 0.f;
		size_t n = 0;
		for (size_t y = 0; y < h; y++) {
			for (size_t x = 0; x < w; x++) {
				if (input(y, x)[1] < min_) {
					min_ = input(y, x)[1];
				}
				if (input(y, x)[1] > max_) {
					max_ = input(y, x)[1];
				}
				if (input(y, x)[1] > __REINHARD_MINIMUM__) {
					avg_ += log(input(y, x)[1]);
					n++;
				}
			}
		}
		min_ = max(__REINHARD_MINIMUM__, min_);
		avg_ = exp(avg_ / n);
		// estimate key value
		float k = key;
		if (key <= 0.f) {
			float a = max_ - avg_;
			float b = avg_ - min_;
			k = 0.18f * pow(2.f, 2.f * (b - a) / (a + b));
		}
		// rescales output image
		float maxE2 = -INFINITY;
		for (size_t y = 0; y < h; y++) {
			for (size_t x = 0; x < w; x++) {
				output(y, x) = input(y, x) * (k / avg_);
				if (maxE2 < output(y, x)[1]) {
					maxE2 = output(y, x)[1];
				}
			}
		}
		maxE2 *= maxE2;
		// perform non linear compression
		for (size_t y = 0; y < h; y++) {
			for (size_t x = 0; x < w; x++) {
				output(y, x) *= (1 + output(y, x)[1] / maxE2) / (1 + output(y, x)[1]);
			}
		}
	}

	__host__
	virtual ~ReinhardToneMapper() {
	}
};

}

#endif /* REINHARDTONEMAPPER_CUH_ */

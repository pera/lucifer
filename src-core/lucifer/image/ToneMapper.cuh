#ifndef TONEMAPPER_CUH_
#define TONEMAPPER_CUH_

#include "lucifer/image/HDRImage.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"

namespace lucifer {

class ToneMapper {
public:
	__host__
	virtual void map(const HDRImage<float, 3>& input, HDRImage<float, 3>& output) const = 0;

	__host__
	virtual ~ToneMapper() {
	}
};

}

#endif /* TONEMAPPER_CUH_ */

#ifndef TILEDBUFFER_CUH_
#define TILEDBUFFER_CUH_

#include <cassert>
#include <vector>
#include "lucifer/color/XYZColorSpace.cuh"
#include "lucifer/filter/Filter2D.cuh"
#include "lucifer/image/HDRImage.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"

namespace lucifer {

class TiledBuffer {
public:
	class Tile {
	private:
		int32_t cMin;
		int32_t rMin;
		int32_t cMax;
		int32_t rMax;
		TiledBuffer* parent;
		std::vector<Color3F> buffer;

	public:
		__host__
		Tile():
		cMin(0), cMax(0), rMin(0), rMax(0), parent(nullptr), buffer(0) {
		}

		__host__
		Tile(const int32_t cMin, const int32_t rMin, const int32_t cMax, const int32_t rMax, TiledBuffer* parent):
		cMin(cMin), rMin(rMin), cMax(cMax), rMax(rMax), parent(parent), buffer((cMax - cMin + 2 * parent->cOff) * (rMax - rMin + 2 * parent->rOff)) {
			assert(cMin < cMax);
			assert(rMin < rMax);
			for (auto& pixel : buffer) {
				pixel = Color3F(0.f, 0.f, 0.f);
			}
		}

		__host__
		inline int32_t colMin() const {
			return cMin;
		}

		__host__
		inline int32_t colMax() const {
			return cMax;
		}

		__host__
		inline int32_t rowMin() const {
			return rMin;
		}

		__host__
		inline int32_t rowMax() const {
			return rMax;
		}

		__host__
		inline size_t size() const {
			return static_cast<size_t>(cMax - cMin) * static_cast<size_t>(rMax - rMin);
		}

		__host__
		Color3F& pixel(const int32_t r, const int32_t c) {
			assert(c >= cMin - parent->cOff && c < cMax + parent->cOff);
			assert(r >= rMin - parent->rOff && r < rMax + parent->rOff);
			return buffer[(r - rMin + parent->rOff) * (cMax - cMin + 2 * parent->cOff) + (c - cMin + parent->cOff)];
		}

		__host__
		const Color3F& pixel(const int32_t r, const int32_t c) const {
			assert(c >= cMin - parent->cOff && c < cMax + parent->cOff);
			assert(r >= rMin - parent->rOff && r < rMax + parent->rOff);
			return buffer[(r - rMin + parent->rOff) * (cMax - cMin + 2 * parent->cOff) + (c - cMin + parent->cOff)];
		}

		__host__
		void addSample(const float y, const float x, const float l, const float val, const float pdf) {
			// validate input data
			if (isinf(val) or isinf(pdf) or isnan(val) or isnan(pdf) ) {
				return;
			}
			int32_t xi = min(static_cast<int32_t>(x), cMax - 1);
			int32_t yi = min(static_cast<int32_t>(y), rMax - 1);
			assert(xi >= cMin);
			assert(xi <  cMax);
			assert(yi >= rMin);
			assert(yi <  rMax);
			Color3F xyz = parent->colorspace->XYZ(l, val / pdf);
			assert(!isinf(xyz) and !isnan(xyz));
			// add sample
			if (parent->filter.isNull()) {
				// not filtered
				pixel(yi, xi) += xyz;
			} else {
				// filtered
				for (int32_t dr = -parent->rOff; dr <= +parent->rOff; ++dr) {
					for (int32_t dc = -parent->cOff; dc <= +parent->cOff; ++dc) {
						float w = (*parent->filter)(x - (xi + dc), y - (yi + dr));
						pixel(yi + dr, xi + dc) += w * xyz;
					}
				}
			}
		}

	};

private:
	const int32_t cols;
	const int32_t rows;
	const int32_t cOff;
	const int32_t rOff;
	const Pointer<const Filter2D> filter;
	const Pointer<const XYZColorSpace> colorspace;
	std::vector<Tile> tiles;

	static int32_t cOffset(const Filter2D* filter) {
		return filter == nullptr ? 0 : static_cast<int32_t>(ceil(max(abs(filter->xSupport().lower), abs(filter->xSupport().upper))));
	}

	static int32_t rOffset(const Filter2D* filter) {
		return filter == nullptr ? 0 : static_cast<int32_t>(ceil(max(abs(filter->ySupport().lower), abs(filter->ySupport().upper))));
	}

public:
	__host__
	TiledBuffer(const size_t wImg, const size_t hImg, const size_t wTile, const size_t hTile, const Pointer<const Filter2D>& filter, const Pointer<const XYZColorSpace>& colorspace):
	cols(wImg / wTile + ((wImg % wTile) > 0 ? 1 : 0)),
	rows(hImg / hTile + ((hImg % hTile) > 0 ? 1 : 0)),
	cOff(cOffset(filter.get())),
	rOff(rOffset(filter.get())),
	filter(filter),
	colorspace(colorspace),
	tiles(cols * rows) {
		// build tiles
		auto rw = wImg % wTile;
		auto rh = hImg % hTile;
		for (int32_t r = 0, i = 0; r < rows; ++r) {
			int32_t hMin = r * hTile;
			int32_t hMax = hMin + (r == rows - 1 && rh > 0 ? rh : hTile);
			for (int32_t c = 0; c < cols; ++c, ++i) {
				int32_t wMin = c * wTile;
				int32_t wMax = wMin + (c == cols - 1 && rw > 0 ? rw : wTile);
				tiles[i] = Tile(wMin, hMin, wMax, hMax, this);
			}
		}
	}

	__host__
	size_t size() const {
		return tiles.size();
	}

	__host__
	inline const Tile& operator[](const size_t i) const {
		return tiles[i];
	}

	__host__
	inline Tile& operator[](const size_t i) {
		return tiles[i];
	}

	__host__
	void reconstruct(HDRImage<float, 3>* output) const {
		auto w = tiles[tiles.size()-1].colMax();
		auto h = tiles[tiles.size()-1].rowMax();
		output->clear();
		for (const Tile& tile : tiles) {
			for (int32_t r = tile.rowMin() - rOff; r < tile.rowMax() + rOff; ++r) {
				for (int32_t c = tile.colMin() - cOff; c < tile.colMax() + cOff; ++c) {
					if (r >= 0 and r < h and c >= 0 and c < w) {
						(*output)(r, c) += tile.pixel(r, c);
					}
				}
			}
		}
	}
};

}

#endif /* TILEDBUFFER_CUH_ */

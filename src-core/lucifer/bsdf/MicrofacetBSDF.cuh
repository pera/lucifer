#ifndef MICROFACETBSDF_CUH_
#define MICROFACETBSDF_CUH_

#include <cassert>
#include "BxDF.cuh"
#include "Fresnel.cuh"
#include "microfacet/MicrofacetModel.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * A specular
 * <a href="https://en.wikipedia.org/wiki/Specular_highlight#Microfacets">microfacet</a>
 * distribution based BSDF.
 *
 * The MicrofacetBasedBSDF is a bidirectional scattering distribution function
 * that assumes that surfaces that are not perfectly smooth are composed of
 * many very tiny facets, each of which is a perfect dielectric that reflects
 * and transmits light according to the unpolarized Fresnel factor based on the
 * refractive indexes of the inside and outside media.
 *
 * These microfacets have normals that are distributed about the normal of the
 * approximating smooth surface. The actual distribution of the microfacet's
 * normal vectors is given by an instance of a MicrofacetModel.
 *
 * Note:
 * The current implmentation does not account for multiple scattering between
 * the microfacets resulting in energy loss. The lost energy is responsible for
 * a reduced actual reflectance, even if no fresnel factor is applied.
 *
 * \author
 * Bruno Pera
 */
class MicrofacetBSDF: public BxDF {
private:
	friend class TypedPool<MicrofacetBSDF>;
	const Pointer<const ShadingNode<float>> reflectance;
	const Pointer<const ShadingNode<float>> transmittance;
	const Pointer<const MicrofacetModel> distribution;

	__host__ __device__
	static const MicrofacetBSDF* cast(const BxDF* bxdf) {
		return static_cast<const MicrofacetBSDF*>(bxdf);
	}

	__host__ __device__
	static bool interacting_(const BxDF* bxdf) {
		return true;
	}

	__host__ __device__
	static float albedo_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode) {
		// TODO: deal with energy loss and fresnel effect
		const float m = (mode == Mode::RADIANCE) ? 1.f : ((ior1.re / ior2.re) * (ior1.re / ior2.re));
		return max((*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time) , m * (*cast(bxdf)->transmittance)(g.gp, g.su, g.sn, g.uv, wlen, time));
	}

	__host__ __device__
	static float evaluate_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, RNG* rng) {
		if (sameHemisphere(wi, wo)) {
			// reflection
		    if (wi[2] == 0.f || wo[2] == 0.f) return 0.f;
		    Vectr3F wh = wi + wo;
		    if (wh[0] == 0.f && wh[1] == 0.f && wh[2] == 0.f) return 0.f;
		    wh.normalize();
		    const float fresnelFactor = Fresnel::reflectance(ior1, ior2, absDot(wo, wh));
		    return (*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time) * cast(bxdf)->distribution->DG(g, wlen, time, wo, wi, wh) * fresnelFactor / (4.f * wi[2] * wo[2]);
		} else {
			// refraction
		    if (wi[2] == 0.f || wo[2] == 0.f) return 0.f;
		    const float eta = ior1.re / ior2.re;
		    Vectr3F wh = (wi + wo * eta).normalize();
		    if (!sameHemisphere(wo, wh)) wh = -wh;
			const float f = Fresnel::reflectance(ior1, ior2, absDot(wo, wh));
			const float m = (mode == Mode::RADIANCE) ? 1.f : (eta * eta);
			const float sqrtDenom = dot(wi, wh) + eta * dot(wo, wh);
		    return (1.f - f) * m * (*cast(bxdf)->transmittance)(g.gp, g.su, g.sn, g.uv, wlen, time) * abs(cast(bxdf)->distribution->DG(g, wlen, time, wo, Vectr3F(wi[0], wi[1], -(wi[2])), wh) * absDot(wi, wh) * absDot(wo, wh) / (wi[2] * wo[2] * sqrtDenom * sqrtDenom));
		}
	}

	__host__ __device__
	static float sample_wi_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng) {
		const Vectr3F wh = cast(bxdf)->distribution->sampleWh(g, wlen, time, wo, rnd, pdf);
		const float eta = ior1.re / ior2.re;
		const float f = Fresnel::reflectance(ior1, ior2, absDot(wo, wh));
		const float r = (f + 0.f) * (  *cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time);
		const float t = (1.f - f) * (*cast(bxdf)->transmittance)(g.gp, g.su, g.sn, g.uv, wlen, time) * (mode == Mode::IMPORTANCE ? (eta * eta) : 1.f);
		const float threshold = r / (r + t);
		if (rnd[0] <= threshold) {
			// reflect
			*wi = reflect(wo, wh);
			*pdf *= threshold / (4.f * dot(wo, wh));
			if (wo[2] * (*wi)[2] <= 0.f) return 0.f;
			return r * cast(bxdf)->distribution->DG(g, wlen, time, wo, *wi, wh) / abs(4.f * (*wi)[2] * wo[2]);
		} else {
			// refract
			if (!refract(wo, wh, eta, wi)) {
				assert(false);
				*pdf = 0.f;
				return 0.f;
			}
			const float woDotWh = dot( wo, wh);
			const float wiDotWh = dot(*wi, wh);
			const float sqrtDenom = wiDotWh + eta * woDotWh;
			const float dwh_dwi = abs((eta * eta * wiDotWh) / (sqrtDenom * sqrtDenom));
			*pdf *= (1.f - threshold) * dwh_dwi;
			if (wo[2] * (*wi)[2] >= 0.f) return 0.f;
			return t * abs(cast(bxdf)->distribution->DG(g, wlen, time, wo, Vectr3F((*wi)[0], (*wi)[1], -((*wi)[2])), wh) * wiDotWh * woDotWh / ((*wi)[2] * wo[2] * sqrtDenom * sqrtDenom));
		}
	}

	__host__ __device__
	static float sample_wi_pdf_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, bool sampled, RNG* rng) {
		const float eta = ior1.re / ior2.re;
		if (sameHemisphere(wi, wo)) {
			// reflection
			const Vectr3F wh = (wo + wi).normalize();
			const float f = Fresnel::reflectance(ior1, ior2, absDot(wo, wh));
			const float r = (f + 0.f) * (  *cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time);
			const float t = (1.f - f) * (*cast(bxdf)->transmittance)(g.gp, g.su, g.sn, g.uv, wlen, time) * (mode == Mode::IMPORTANCE ? (eta * eta) : 1.f);
			return (r / (r + t)) * cast(bxdf)->distribution->sampleWhPDF(g, wlen, time, wo, wh) / (4.f * dot(wo, wh));
		} else {
			// refraction
			Vectr3F wh = (wi + wo * eta).normalize();
			if (!sameHemisphere(wo, wh)) wh = -wh;
			const float f = Fresnel::reflectance(ior1, ior2, absDot(wo, wh));
			const float r = (f + 0.f) * (  *cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time);
			const float t = (1.f - f) * (*cast(bxdf)->transmittance)(g.gp, g.su, g.sn, g.uv, wlen, time) * (mode == Mode::IMPORTANCE ? (eta * eta) : 1.f);
			const float sqrtDenom = dot(wi, wh) + eta * dot(wo, wh);
			const float dwh_dwi = abs((eta * eta * dot(wi, wh)) / (sqrtDenom * sqrtDenom));
			return (t / (r + t)) * cast(bxdf)->distribution->sampleWhPDF(g, wlen, time, wo, wh)  * dwh_dwi;
		}
	}

	__host__
	MicrofacetBSDF(
		const Pointer<const ShadingNode<float>>& reflectance,
		const Pointer<const ShadingNode<float>>& transmittance,
		const Pointer<const MicrofacetModel>& distribution,
		const interacting_t& interacting_,
		const albedo_t& albedo_,
		const evaluate_t& evaluate_,
		const sample_wi_t& sample_wi_,
		const sample_wi_pdf_t& sample_wi_pdf_
	):
		BxDF(BxDF::Type(DIFFUSE | REFLECTIVE | REFRACTIVE), interacting_, albedo_, evaluate_, sample_wi_, sample_wi_pdf_),
		reflectance(reflectance),
		transmittance(transmittance),
		distribution(distribution) {
	}

public:
	/*!
	 * \brief
	 * Creates a new MicrofacetBSDF.
	 *
	 * \param[in] reflectance The spectral reflectance at each point on the
	 * surface, must be greater than or equal to 0 and less than or equal to 1
	 * everywhere, otherwise the BxDF's behaviour is undefined.
	 * \param[in] transmittance The spectral transmittance at each point on the
	 * surface, must be greater than or equal to 0 and less than or equal to 1
	 * everywhere, otherwise the BxDF's behaviour is undefined.*
	 * \param[in] distribution The distribution of the microfacet's normal
	 * vectors around the macrosurface geometric normal.
	 */
	__host__
	static Pointer<MicrofacetBSDF> build(
		MemoryPool& pool,
		const Pointer<const ShadingNode<float>>& reflectance,
		const Pointer<const ShadingNode<float>>& transmittance,
		const Pointer<const MicrofacetModel>& distribution) {
		return Pointer<MicrofacetBSDF>::make(
			pool, reflectance, transmittance, distribution,
			interacting_t(virtualize(interacting_)),
			albedo_t(virtualize(albedo_)),
			evaluate_t(virtualize(evaluate_)),
			sample_wi_t(virtualize(sample_wi_)),
			sample_wi_pdf_t(virtualize(sample_wi_pdf_))
		);
	}
};

}



#endif /* MICROFACETBSDF_CUH_ */

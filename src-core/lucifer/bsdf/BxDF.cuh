#ifndef BXDF_CUH_
#define BXDF_CUH_

#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/math/geometry/Geometry.cuh"
#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/memory/Virtual.cuh"
#include "lucifer/random/RNG.cuh"

namespace lucifer {

/*!
 * A simple component of a BSDF.
 *
 * A BxDF describes part of the scattering behaviour of a BSDF, in other words,
 * a BSDF is just a sum of BxDFs. A BxDF must be diffuse OR specular and must
 * be reflective AND/OR refractive.
 *
 * \author
 * Bruno Pera.
 */
class BxDF: public Polymorphic {
public:
	/*!
	 * \brief
	 * The BxDF type.
	 */
	enum Type {
		DIFFUSE    = 1 << 0,
		SPECULAR   = 1 << 1,
		REFLECTIVE = 1 << 2,
		REFRACTIVE = 1 << 3,
	};

	/*!
	 * The transport mode used during evaluation.
	 */
	enum Mode {
		RADIANCE,
		IMPORTANCE,
	};

public:
	/**************************************************************************
	 * cuda virtual interface
	 *************************************************************************/
	using interacting_t = Virtual<
		bool,
		const BxDF*
	>;

	using albedo_t = Virtual<
		float,
		const BxDF*,
		const Geometry&,
		const float,
		const float,
		const Vectr3F&,
		const ComplexF&,
		const ComplexF&,
		const Mode&
	>;

	using evaluate_t = Virtual<
		float,
		const BxDF*,
		const Geometry&,
		const float,
		const float,
		const Vectr3F&,
		const Vectr3F&,
		const ComplexF&,
		const ComplexF&,
		const Mode&,
		RNG*
	>;

	using sample_wi_t = Virtual<
		float, const BxDF*,
		const Geometry&,
		const float,
		const float,
		const Vectr3F&,
		const ComplexF&,
		const ComplexF&,
		const Mode&,
		float[2],
		Vectr3F*,
		float*,
		RNG*
	>;

	using sample_wi_pdf_t = Virtual<
		float,
		const BxDF*,
		const Geometry&,
		const float,
		const float,
		const Vectr3F&,
		const Vectr3F&,
		const ComplexF&,
		const ComplexF&,
		const Mode&,
		bool,
		RNG*
	>;

private:
	const Type type_;
	const interacting_t interacting_;
	const albedo_t albedo_;
	const evaluate_t evaluate_;
	const sample_wi_t sample_wi_;
	const sample_wi_pdf_t sample_wi_pdf_;

protected:
	__host__
	BxDF(
		const Type type,
		const interacting_t& interacting_,
		const albedo_t& albedo_,
		const evaluate_t& evaluate_,
		const sample_wi_t& sample_wi_,
		const sample_wi_pdf_t& sample_wi_pdf_
	):
		type_(type),
		interacting_(interacting_),
		albedo_(albedo_),
		evaluate_(evaluate_),
		sample_wi_(sample_wi_),
		sample_wi_pdf_(sample_wi_pdf_) {
	}

public:
	/*!
	 * \brief
	 * Returns the BxDF type.
	 *
	 * \return The type.
	 */
	__host__ __device__
	inline Type type() const {
		return type_;
	}
	/*!
	 * \brief
	 * Returns if the BxDF matches the given type.
	 *
	 * \param[in] t The type to be checked.
	 *
	 *
	 * \return If the BxDF matches the given type.
	 */
	__host__ __device__
	inline bool matches(Type t) const {
		return (type_ & t) == t;
	}

	/*!
	 * \brief
	 * Returns if the BxDF actually interacts with light.
	 *
	 * \return If the BxDF actually interacts with light.
	 */
	__host__ __device__
	bool isOpticallyInteracting() const {
		return interacting_(this);
	}

	/*!
	 * \brief
	 * Estimates the spectral albedo.
	 *
	 * Computes the fraction of scattered light from a given direction `wo`
	 * into the entire sphere of directions. This information is used to guide
	 * the importance sampling of complex BSDFs, it does not need to be exact
	 * but the more accurate the estimate the better the resulting importance
	 * sampling.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wo The normalized outgoing direction.
	 * \param[in] ior1 The refractive index on the same side of the outgoing
	 * direction.
	 * \param[in] ior2 The refractive index on the opposite side of the
	 * outgoing direction.
	 * \param[in] mode The transport mode, RADIANCE or IMPORTANCE.
	 */
	__host__ __device__
	float albedo(const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode) const {
		return albedo_(this, g, wlen, time, wo, ior1, ior2, mode);
	}

	/*!
	 * \brief
	 * Evaluates the part of bidirectional scattering distribution function
	 * represented by this BxDF. For any fixed outgoing direction a physically
	 * correct BxDF integral over the entire sphere of incoming direction must
	 * be less then or equal to 1 in order no to violate the principle of
	 * conservation of energy. A non refractive, physically correct BxDF must
	 * also obey the reciprocity principle: BxDF(wi, wo) = BxDF(wo, wi).
	 *
	 * \param[in] g The geometry of the position on the surface. The geometry
	 * is supposed to be rotated so that the normal vector is aligned with the
	 * positive z-axis.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wi The normalized incoming direction.
	 * \param[in] wo The normalized outgoing direction.
	 * \param[in] ior1 The refractive index on the same side of the outgoing
	 * direction.
	 * \param[in] ior2 The refractive index on the opposite side of the
	 * outgoing direction.
	 * \param[in] mode The transport mode, RADIANCE or IMPORTANCE. Describes
	 * what is incoming and outgoing.
	 * \param[in] rng1D Uncorrelated unidimensional random number generator.
	 *
	 * \return The BxDF's contribution the BSDF's value.
	 */
	__host__ __device__
	float evaluate(const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, RNG* rng1D) const {
		return evaluate_(this, g, wlen, time, wi, wo, ior1, ior2, mode, rng1D);
	}

	/*!
	 * \brief
	 * Samples an incoming direction given an outgoing direction.
	 *
	 * \param[in] g The geometry of the position on the surface. The geometry
	 * is supposed to be rotated so that the normal vector is aligned with the
	 * positive z-axis.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wo The normalized outgoing direction.
	 * \param[in] ior1 The refractive index on the same side of the outgoing
	 * direction.
	 * \param[in] ior2 The refractive index on the opposite side of the
	 * outgoing direction.
	 * \param[in] mode The transport mode.
	 * \param[in] rnd Two uniformly distributed random numbers in [0, 1].*
	 * \param[out] wi The sampled normalized incoming direction.
	 * \param[out] pdf The PDF of sampling `wi` with respect to the solid angle
	 * measure from the geometry's normal vector. If the sampling operation
	 * fails the computed PDF is garanteed to be zero and the sampled incoming
	 * direction and returned value are both undefined.
 	 * \param[in] rng1D Uncorrelated unidimensional random number generator.
	 *
	 * \return The BxDF's contribution the BSDF's value at the sampled
	 * direction.
	 */
	__host__ __device__
	float sampleWi(const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng1D) const {
		return sample_wi_(this, g, wlen, time, wo, ior1, ior2, mode, rnd, wi, pdf, rng1D);
	}

	/*!
	 * \brief
	 * Computes the PDF of sampling the incoming direction `wi` given the
	 * outgoing direction `wo` with respect to the solid angle measue from the
	 * geometry's normal vector.
	 *
	 * \param[in] g The geometry of the position on the surface. The geometry
	 * is supposed to be rotated so that the normal vector is aligned with the
	 * positive z-axis.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wo The normalized outgoing direction.
	 * \param[in] wi The normalized incoming direction.
	 * \param[in] ior1 The refractive index on the same side of the outgoing
	 * direction.
	 * \param[in] ior2 The refractive index on the opposite side of the
	 * outgoing direction.
	 * \param[in] mode The transport mode.
	 * \param[out] sampled If this BxDF was actually sampled from the BSDF.
	 * \param[in] rng1D Uncorrelated unidimensional random number generator.
	 *
	 * \return The PDF of sampling `wi` with respect to the solid angle
	 * measure from the geometry's normal vector.
	 */
	__host__ __device__
	float sampleWiPDF(const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, bool sampled, RNG* rng1D) const {
		return sample_wi_pdf_(this, g, wlen, time, wo, wi, ior1, ior2, mode, sampled, rng1D);
	}
};

}

#endif /* BXDF_CUH_ */

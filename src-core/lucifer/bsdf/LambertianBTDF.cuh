#ifndef LAMBERTIANBTDF_CUH_
#define LAMBERTIANBTDF_CUH_

#include "BxDF.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"
#include "lucifer/random/CosineHemisphereDistribution.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * An ideal lambertian BTDF.
 *
 * A LambertianBTDF transmits light uniformly along the entire hemisphere
 * opposed to the incident light. The apparent brightness of a lambertian BTDF
 * to an observer is the same regardless of the observer's angle of view.
 *
 * \author
 * Bruno Pera.
 */
class LambertianBTDF: public BxDF {
private:
	friend class TypedPool<LambertianBTDF>;
	const Pointer<const ShadingNode<float>> transmittance;

	__host__ __device__
	static const LambertianBTDF* cast(const BxDF* bxdf) {
		return static_cast<const LambertianBTDF*>(bxdf);
	}

	__host__ __device__
	static bool interacting_(const BxDF* bxdf) {
		return true;
	}

	__host__ __device__
	static float albedo_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode) {
		return (*cast(bxdf)->transmittance)(g.gp, g.su, g.sn, g.uv, wlen, time);
	}

	__host__ __device__
	static float evaluate_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, RNG* rng) {
		return sameHemisphere(wi, wo) ? 0.f : (*cast(bxdf)->transmittance)(g.gp, g.su, g.sn, g.uv, wlen, time) / PI<float>();
	}

	__host__ __device__
	static float sample_wi_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng) {
		CosineHemisphereDistribution::sample(rnd, &((*wi)[0]), pdf);
		if (wo[2] > 0.f) (*wi)[2] = -((*wi)[2]);
		return (*cast(bxdf)->transmittance)(g.gp, g.su, g.sn, g.uv, wlen, time) / PI<float>();
	}

	__host__ __device__
	static float sample_wi_pdf_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, bool sampled, RNG* rng) {
		if (sameHemisphere(wi, wo)) return 0.f;
		Vectr3F w(wi[0], wi[1], abs(wi[2]));
		return CosineHemisphereDistribution::pdf(&(w[0]));
	}

	__host__
	LambertianBTDF(
		const Pointer<const ShadingNode<float>>& transmittance,
		const interacting_t& interacting_,
		const albedo_t& albedo_,
		const evaluate_t& evaluate_,
		const sample_wi_t& sample_wi_,
		const sample_wi_pdf_t& sample_wi_pdf_
	):
		BxDF(Type(DIFFUSE | REFRACTIVE), interacting_, albedo_, evaluate_, sample_wi_, sample_wi_pdf_), transmittance(transmittance) {
	}

public:
	/*!
	 * \brief
	 * Creates a new LambertianBTDF.
	 *
	 * \param[in] transmittance The spectral transmittance at each point on the
	 * surface, must be greater than or equal to 0 and less than or equal to 1
	 *  everywhere, otherwise the BxDF's behaviour is undefined.
	 */
	__host__
	static Pointer<LambertianBTDF> build(MemoryPool& pool, const Pointer<const ShadingNode<float>>& transmittance) {
		return Pointer<LambertianBTDF>::make(
			pool, transmittance,
			interacting_t(virtualize(interacting_)),
			albedo_t(virtualize(albedo_)),
			evaluate_t(virtualize(evaluate_)),
			sample_wi_t(virtualize(sample_wi_)),
			sample_wi_pdf_t(virtualize(sample_wi_pdf_))
		);
	}
};

}

#endif /* LAMBERTIANBTDF_CUH_ */

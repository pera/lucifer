#ifndef SPECULARBSDF_CUH_
#define SPECULARBSDF_CUH_

#include <cassert>
#include "BxDF.cuh"
#include "Fresnel.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * Specular BSDF
 *
 * A SpecularBSDF is a delta distribution in which the incident light is
 * reflected and refracted only into the specular directions. The amount of
 * energy actually reflected and refracted will vary with the outgoing grazing
 * angle according to the unpolarized Fresnel equations for unpolarized light.
 *
 * \author
 * Bruno Pera.
 */
class SpecularBSDF: public BxDF {
private:
	friend class TypedPool<SpecularBSDF>;
	const Pointer<const ShadingNode<float>> reflectance;
	const Pointer<const ShadingNode<float>> transmittance;

	__host__ __device__
	static const SpecularBSDF* cast(const BxDF* bxdf) {
		return static_cast<const SpecularBSDF*>(bxdf);
	}

	__host__ __device__
	static bool interacting_(const BxDF* bxdf) {
		return true;
	}

	__host__ __device__
	static float albedo_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode) {
		const float f = Fresnel::reflectance(ior1, ior2, absCosTheta(wo));
		const float m = mode == Mode::RADIANCE ? 1.f : (ior1.re * ior1.re) / (ior2.re * ior2.re);
		return f * (*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time) + m * (1.f - f) * (*cast(bxdf)->transmittance)(g.gp, g.su, g.sn, g.uv, wlen, time);
	}

	__host__ __device__
	static float evaluate_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, RNG* rng) {
		return 0.f;
	}

	__host__ __device__
	static float sample_wi_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng) {
		const float f = Fresnel::reflectance(ior1, ior2, absCosTheta(wo));
		const float r = (f + 0.f) * (*cast(bxdf)->reflectance  )(g.gp, g.su, g.sn, g.uv, wlen, time);
		float       t = (1.f - f) * (*cast(bxdf)->transmittance)(g.gp, g.su, g.sn, g.uv, wlen, time);
		if (mode == Mode::IMPORTANCE) t *= (ior1.re * ior1.re) / (ior2.re * ior2.re);
		const float threshold = r / (r + t);
		if (rnd[0] <= threshold) {
			// reflect
			*pdf = threshold;
			*wi = Vectr3F(-wo[0], -wo[1], wo[2]);
			return r / absCosTheta(*wi);
		} else {
			// refract
			if (!refract(wo, ior1.re / ior2.re, wi)) {
				assert(false);
				*pdf = 0.f;
				return 0.f;
			}
			*pdf = 1.f - threshold;
			return t / absCosTheta(*wi);
		}
	}

	__host__ __device__
	static float sample_wi_pdf_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, bool sampled, RNG* rng) {
		if (!sampled) return 0.f;
		const float f = Fresnel::reflectance(ior1, ior2, absCosTheta(wo));
		const float r = (f + 0.f) * (*cast(bxdf)->reflectance  )(g.gp, g.su, g.sn, g.uv, wlen, time);
		float       t = (1.f - f) * (*cast(bxdf)->transmittance)(g.gp, g.su, g.sn, g.uv, wlen, time);
		if (mode == Mode::IMPORTANCE) t *= (ior1.re * ior1.re) / (ior2.re * ior2.re);
		return sameHemisphere(wi, wo) ? r / (r + t) : t / (r + t);
	}

	__host__
	SpecularBSDF(
		const Pointer<const ShadingNode<float>>& reflectance,
		const Pointer<const ShadingNode<float>>& transmittance,
		const interacting_t& interacting_,
		const albedo_t& albedo_,
		const evaluate_t& evaluate_,
		const sample_wi_t& sample_wi_,
		const sample_wi_pdf_t& sample_wi_pdf_
	):
		BxDF(Type(SPECULAR | REFLECTIVE | REFRACTIVE), interacting_, albedo_, evaluate_, sample_wi_, sample_wi_pdf_),
		reflectance(reflectance),
		transmittance(transmittance) {
	}

public:
	/*!
	 * \brief
	 * Creates a new SpecularBSDF.
	 *
	 * \param[in] reflectance The spectral reflectance at each point on the
	 * surface, must be greater than or equal to 0 and less than or equal to 1
	 *  everywhere, otherwise the BxDF's behaviour is undefined.
	 * \param[in] transmittance The spectral transmittance at each point on the
	 * surface, must be greater than or equal to 0 and less than or equal to 1
	 *  everywhere, otherwise the BxDF's behaviour is undefined.
	 */
	__host__
	static Pointer<SpecularBSDF> build(
		MemoryPool& pool,
		const Pointer<const ShadingNode<float>>& reflectance,
		const Pointer<const ShadingNode<float>>& transmittance
	) {
		return Pointer<SpecularBSDF>::make(
			pool, reflectance, transmittance,
			interacting_t(virtualize(interacting_)),
			albedo_t(virtualize(albedo_)),
			evaluate_t(virtualize(evaluate_)),
			sample_wi_t(virtualize(sample_wi_)),
			sample_wi_pdf_t(virtualize(sample_wi_pdf_))
		);
	}
};

}

#endif /* SPECULARBSDF_CUH_ */

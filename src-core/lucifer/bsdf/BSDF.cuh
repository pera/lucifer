#ifndef BSDF_CUH_
#define BSDF_CUH_

#include <cassert>
#include "BxDF.cuh"
#include "lucifer/data/Array.cuh"
#include "lucifer/math/Util.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/memory/Polymorphic.cuh"

namespace lucifer {

/*!
 * \brief
 * Bidirectional Scattering Distribution Function
 *
 * A BSDF describes how light is scattered at the surface of a primitive.
 * Structurally a BSDF holds a set of BxDF instances that together fully
 * describes the light scattering. Mathematically the BSDF is defined as
 * differential ratio of outgoing radiance dL in direction wo and the incoming
 * irradiance dE in direction wi, in other words, BSDF(wi, wo) = dL(wo)/dE(wi).
 *
 * A pshysical correct BSDF must be non negative in every point and, in order
 * to respect the concept of conservation of enery, for any fixed incoming
 * direction wi, the integral of the BSDF over the entire sphere of outgoing
 * directions must be at most 1/abs(dot(wi, n)), where n is the unit normal
 * vector at the scattering point over the surface.
 *
 * \author
 * Bruno Pera.
 */
class BSDF: public Polymorphic {
public:
	using index_t = uint8_t;
	using array_t = Array<Pointer<const BxDF>, index_t>;

private:
	array_t bxdfs;

	__host__
	BSDF(MemoryPool& pool, const size_t n):
	bxdfs(pool, n) {
	}

public:
	friend class TypedPool<BSDF>;

	/*!
	 * \brief
	 * Creates a new BSDF to hold `n` BxDF instances.
	 *
	 * \param[in] n The number of BxDF instances.
	 */
	__host__
	static Pointer<BSDF> build(MemoryPool& pool, const size_t n) {
		return Pointer<BSDF>::make(pool, pool, n);
	}

	/*!
	 * \brief
	 * Returns if the BSDF matches the given BxDF type.
	 *
	 * \param[in] type The type to be checked.
	 * \return If the BSDF matches the given BxDF type.
	 */
	__host__ __device__
	bool matches(BxDF::Type type) const {
		for (index_t i = 0; i < bxdfs.size(); i++) {
			if (bxdfs[i]->matches(type)) {
				return true;
			}
		}
		return false;
	}

	/*!
	 * \brief
	 * Returns if the BSDF actually interacts with light.
	 *
	 * \return If the BSDF actually interacts with light.
	 */
	__host__ __device__
	bool isOpticallyInteracting() const {
		for (index_t i = 0; i < bxdfs.size(); i++) {
			if (bxdfs[i]->isOpticallyInteracting()) {
				return true;
			}
		}
		return false;
	}

	/*!
	 * \brief
	 * Returns the number of components.
	 *
	 * \return The number of components.
	 */
	__host__ __device__
	index_t size() const {
		return bxdfs.size();
	}

	/*!
	 * \brief
	 * Returns a constant pointer to the i-st BxDF.
	 *
	 * \param[in] i The BxDF index.
	 * \return a constant pointer to the i-st BxDF.
	 */
	__host__ __device__
	const Pointer<const BxDF>& operator[](index_t i) const {
		return bxdfs[i];
	}

	/*!
	 * \brief
	 * Returns a pointer to the i-st BxDF.
	 *
	 * \param[in] i The BxDF index.
	 * \return a pointer to the i-st BxDF.
	 */
	__host__
	Pointer<const BxDF>& operator[](index_t i) {
		return bxdfs[i];
	}

	/*!
	 * \brief
	 * Evalues the BSDF. For any fixed outgoing direction a physically correct
	 * BSDF integral over the entire sphere of incoming direction must be less
	 * then or equal to 1 in order no to violate the principle of conservation
	 * of energy. A non refractive, physically correct BSDF must also obey the
	 * reciprocity principle: BSDF(wi, wo) = BSDF(wo, wi).
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] l The light wavelength in meters.
	 * \param[in] wi The normalized incoming direction.
	 * \param[in] wo The normalized outgoing direction.
	 * \param[in] ior1 The refractive index on the same side of the outgoing
	 * direction.
	 * \param[in] ior2 The refractive index on the opposite side of the
	 * outgoing direction.
	 * \param[in] mode The transport mode, RADIANCE or IMPORTANCE. Describes
	 * what is incoming and outgoing.
	 * \param[in] restrictions The restrictions on the BxDF types to be
	 * considered.
	 *
	 * \return The value of the BSDF.
	 */
	__host__ __device__
	float evaluate(const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const BxDF::Mode& mode, RNG* rng, const BxDF::Type restrictions = BxDF::Type(0)) const {
		// convert to local coordinates
		Vectr3F lwi, lwo;
		Vectr3F g_sv = cross<Norml3F, Vectr3F, Vectr3F>(g.sn, g.su).normalize();
		toShadingCoordinates(g.su, g_sv, g.sn, wi, lwi);
		toShadingCoordinates(g.su, g_sv, g.sn, wo, lwo);
		BxDF::Type type = BxDF::Type(restrictions | (sameHemisphere(g.gn, wi, wo) ? BxDF::Type::REFLECTIVE : BxDF::Type::REFRACTIVE));
		float value = 0.f;
		for (index_t i = 0; i < bxdfs.size(); i++) {
			if (bxdfs[i]->matches(type)) {
				value += bxdfs[i]->evaluate(g, wlen, time, lwi, lwo, ior1, ior2, mode, rng);
			}
		}
		if (mode == BxDF::Mode::IMPORTANCE) {
			value *= abs(dot(wo, g.sn) / dot(wo, g.gn));
		}
		return value;
	}

	/*!
	 * \brief
	 * Samples an incoming direction given an outgoing direction.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] l The light wavelength in meters.
	 * \param[in] wo The normalized outgoing direction.
	 * \param[in] ior1 The refractive index on the same side of the outgoing
	 * direction.
	 * \param[in] ior2 The refractive index on the opposite side of the
	 * outgoing direction.
	 * \param[in] mode The transport mode.
	 * \param[in] restrictions The restrictions on the BxDF types to be
	 * considered.
	 * \param[in] rnd Two uniformly distributed random numbers in [0, 1].
	 * \param[out] wi The sampled normalized incoming direction.
	 * \param[out] pdf The PDF of sampling `wi` with respect to the solid angle
	 * measure from the geometry's normal vector.
	 * \param[out] sampled If not null holds the actually sampled BxDF.
	 *
	 * \return The BSDF value at the sampled direction.
	 */
	__host__ __device__
	float sampleWi(const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const BxDF::Mode& mode, const BxDF::Type restrictions, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng, const BxDF** sampled = nullptr) const {
		// count matching bxdfs
		index_t n = 0;
		for (auto& bxdf: bxdfs) {
			if (bxdf->matches(restrictions)) {
				n++;
			}
		}
		if (n == 0) {
			// no bxdf matches the restrictions
			*sampled = nullptr;
			*pdf = 0.f;
			return 0.f;
		}
		// convert to local shading coordinates
		Vectr3F lwo, lwi;
		Vectr3F g_sv = cross<Norml3F, Vectr3F, Vectr3F>(g.sn, g.su).normalize();
		toShadingCoordinates(g.su, g_sv, g.sn, wo, lwo);
		// compute total albedo of matching bxdfs
		float rhoSum = 0.f;
		for (auto& bxdf: bxdfs) {
			if (bxdf->matches(restrictions)) {
				rhoSum += (n == 1) ? 1.f : bxdf->albedo(g, wlen, time, lwo, ior1, ior2, mode);
			}
		}
		if (rhoSum == 0.f) {
			// no scattering
			*sampled = nullptr;
			*pdf = 0.f;
			return 0.f;
		}
		// select one bxdf according to the relative albedo
		index_t i = 0;
		index_t j = 0;
		float a, b, rho, rhoAcc = 0.f;
		for (auto& bxdf: bxdfs) {
			if (bxdf->matches(restrictions)) {
				a = rhoAcc;
				rho = (n == 1) ? 1.f : bxdf->albedo(g, wlen, time, lwo, ior1, ior2, mode);
				rhoAcc += rho;
				b = rhoAcc;
				if (i == (n - 1) or rnd[0] <= (rhoAcc / rhoSum)) break;
				i++;
			}
			j++;
		}
		assert(i < bxdfs.size());
		assert(j < bxdfs.size());
		// rescale first random parameter
		a /= rhoSum;
		b /= rhoSum;
		rnd[0] = clamp((rnd[0] - a) / (b - a), 0.f, 1.f);
		// sample selected bxdf
		float val = bxdfs[j]->sampleWi(g, wlen, time, lwo, ior1, ior2, mode, rnd, &lwi, pdf, rng);
		toWorldCoordinates(g.su, g_sv, g.sn, lwi, *wi);
		if (*pdf <= 0.f) {
			// sampling failed
			if (sampled) *sampled = nullptr;
			*pdf = 0.f;
			return 0.f;
		}
		*pdf *= rho;
		if (sampled) *sampled = bxdfs[j].get();
		// account for other matching bxdfs albedo-weighted pdfs
		if (n > 1 and !bxdfs[j]->matches(BxDF::Type::SPECULAR)) {
			for (index_t k = 0; k < bxdfs.size(); k++) {
				if (k != j && bxdfs[k]->matches(restrictions)) {
					*pdf += bxdfs[k]->albedo(g, wlen, time, lwo, ior1, ior2, mode) * bxdfs[k]->sampleWiPDF(g, wlen, time, lwo, lwi, ior1, ior2, mode, false, rng);
					 val += bxdfs[k]->evaluate(g, wlen, time, lwi, lwo, ior1, ior2, mode, rng);
				}
			}
		}
		*pdf /= rhoSum;
		// adjust importance value due to shading normal
		if (mode == BxDF::Mode::IMPORTANCE) {
			val *= abs(dot(wo, g.sn) / dot(wo, g.gn));
		}
		// return bsdf value
		return val;
	}

	/*!
	 * \brief
	 * Computes the PDF of sampling the incoming direction `wi` given the
	 * outgoing direction `wo` with respect to the solid angle measue from the
	 * geometry's normal vector.
	 *
	 * \param[in] g The geometry of the position on the surface.
	 * \param[in] l The light wavelength in meters.
	 * \param[in] wo The normalized outgoing direction.
	 * \param[in] wi The normalized incoming direction.
	 * \param[in] ior1 The refractive index on the same side of the outgoing
	 * direction.
	 * \param[in] ior2 The refractive index on the opposite side of the
	 * outgoing direction.
	 * \param[in] mode The transport mode.
	 * \param[in] restrictions The restrictions on the BxDF types to be
	 * considered.
	 * \param[out] sampled The actually sampled BxDF.
	 *
	 * \return The PDF of sampling `wi` with respect to the solid angle
	 * measure from the geometry's normal vector.
	 */
	__host__ __device__
	float sampleWiPDF(const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const BxDF::Mode& mode, RNG* rng, const BxDF::Type restrictions = BxDF::Type(0), const BxDF* sampled = nullptr) const {
		// count matching bxdfs
		index_t n = 0;
		for (auto& bxdf: bxdfs) {
			if (bxdf->matches(restrictions)) {
				n++;
			}
		}
		if (n == 0) return 0.f;
		// convert to local shading coordinates
		Vectr3F lwi, lwo;
		Vectr3F g_sv = cross<Norml3F, Vectr3F, Vectr3F>(g.sn, g.su).normalize();
		toShadingCoordinates(g.su, g_sv, g.sn, wi, lwi);
		toShadingCoordinates(g.su, g_sv, g.sn, wo, lwo);
		// compute total albedo of matching bxdfs
		float rhoSum = 0.f;
		for (auto& bxdf: bxdfs) {
			if (bxdf->matches(restrictions)) {
				rhoSum += (n == 1) ? 1.f : bxdf->albedo(g, wlen, time, lwo, ior1, ior2, mode);
			}
		}
		if (rhoSum == 0.f) return 0.f;
		// compute albedo weighted pdf of matching bxdfs
		float pdfSum = 0.f;
		for (auto& bxdf: bxdfs) {
			if (bxdf->matches(restrictions)) {
				pdfSum += ((n == 1) ? 1.f : bxdf->albedo(g, wlen, time, lwo, ior1, ior2, mode)) * bxdf->sampleWiPDF(g, wlen, time, lwo, lwi, ior1, ior2, mode, bxdf.get() == sampled, rng);
			}
		}
		return pdfSum / rhoSum;
	}
};

}

#endif /* BSDF_CUH_ */

#ifndef SPECULARBRDF_CUH_
#define SPECULARBRDF_CUH_

#include "BxDF.cuh"
#include "Fresnel.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * Specular BRDF
 *
 * A SpecularBRDF is a delta distribution in which the incident light is
 * reflected only into the specular direction. The amount of energy actually
 * reflected will vary according to the outgoing grazing angle if the `fresnel`
 * option is turned on.
 *
 * \author
 * Bruno Pera.
 */
class SpecularBRDF: public BxDF {
private:
	friend class TypedPool<SpecularBRDF>;
	const Pointer<const ShadingNode<float>> reflectance;
	const bool fresnel;

	__host__ __device__
	static const SpecularBRDF* cast(const BxDF* bxdf) {
		return static_cast<const SpecularBRDF*>(bxdf);
	}

	__host__ __device__
	static bool interacting_(const BxDF* bxdf) {
		return true;
	}

	__host__ __device__
	static float albedo_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode) {
		const float ff = cast(bxdf)->fresnel ? Fresnel::reflectance(ior1, ior2, absCosTheta(wo)) : 1.f;
		return ff * (*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time);
	}

	__host__ __device__
	static float evaluate_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, RNG* rng) {
		return 0.f;
	}

	__host__ __device__
	static float sample_wi_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng) {
		*wi = Vectr3F(-wo[0], -wo[1], wo[2]);
		*pdf = 1.f;
		const float ff = cast(bxdf)->fresnel ? Fresnel::reflectance(ior1, ior2, absCosTheta(wo)) : 1.f;
		return ff * (*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time) / absCosTheta(*wi);
	}

	__host__ __device__
	static float sample_wi_pdf_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, bool sampled, RNG* rng) {
		return sampled ? 1.f : 0.f;
	}

	__host__
	SpecularBRDF(
		const Pointer<const ShadingNode<float>>& reflectance,
		const bool fresnel,
		const interacting_t& interacting_,
		const albedo_t& albedo_,
		const evaluate_t& evaluate_,
		const sample_wi_t& sample_wi_,
		const sample_wi_pdf_t& sample_wi_pdf_
	):
		BxDF(Type(SPECULAR | REFLECTIVE), interacting_, albedo_, evaluate_, sample_wi_, sample_wi_pdf_), reflectance(reflectance), fresnel(fresnel) {
	}

public:
	/*!
	 * \brief
	 * Creates a new SpecularBRDF.
	 *
	 * \param[in] reflectance The spectral reflectance at each point on the
	 * surface, must be greater than or equal to 0 and less than or equal to 1
	 *  everywhere, otherwise the BxDF's behaviour is undefined.
	 * \param[in] fresnel If the unpolarized
	 * <a href="https://en.wikipedia.org/wiki/Fresnel_equations">fresnel</a>
	 * factor should be used to scale down the reflectance according to the
	 * outgoing angle.
	 */
	/*!
	 * \brief
	 * Creates a new LambertianBRDF.
	 *
	 * \param[in] reflectance The spectral reflectance at each point on the
	 * surface, must be greater than or equal to 0 and less than or equal to 1
	 *  everywhere, otherwise the BxDF's behaviour is undefined.
	 */
	__host__
	static Pointer<SpecularBRDF> build(MemoryPool& pool, const Pointer<const ShadingNode<float>>& reflectance, const bool fresnel) {
		return Pointer<SpecularBRDF>::make(
			pool, reflectance, fresnel,
			interacting_t(virtualize(interacting_)),
			albedo_t(virtualize(albedo_)),
			evaluate_t(virtualize(evaluate_)),
			sample_wi_t(virtualize(sample_wi_)),
			sample_wi_pdf_t(virtualize(sample_wi_pdf_))
		);
	}
};

}

#endif /* SPECULARBRDF_CUH_ */

#ifndef MICROFACETBRDF_CUH_
#define MICROFACETBRDF_CUH_

#include "BxDF.cuh"
#include "Fresnel.cuh"
#include "microfacet/MicrofacetModel.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"
#include "lucifer/random/CosineHemisphereDistribution.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * A specular
 * <a href="https://en.wikipedia.org/wiki/Specular_highlight#Microfacets">microfacet</a>
 * distribution based BRDF.
 *
 * The MicrofacetBRDF is a bidirectional reflection distribution function that
 * assumes that surfaces that are not perfectly smooth are composed of many
 * very tiny facets, each of which is a perfect specular reflector.
 *
 * These microfacets have normals that are distributed about the normal of the
 * approximating smooth surface. The actual distribution of the microfacet's
 * normal vector is given by an instance of a MicrofacetModel.
 *
 * Note:
 * The current implmentation does not account for multiple scattering between
 * the microfacets resulting in energy loss. The lost energy is responsible for
 * a reduced actual reflectance, even if no fresnel factor is applied.
 *
 * \author
 * Bruno Pera
 */
class MicrofacetBRDF: public BxDF {
private:
	friend class TypedPool<MicrofacetBRDF>;
	const Pointer<const ShadingNode<float>> reflectance;
	const Pointer<const MicrofacetModel> distribution;
	const bool fresnel;

	__host__ __device__
	static const MicrofacetBRDF* cast(const BxDF* bxdf) {
		return static_cast<const MicrofacetBRDF*>(bxdf);
	}

	__host__ __device__
	static bool interacting_(const BxDF* bxdf) {
		return true;
	}

	__host__ __device__
	static float albedo_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode) {
		// TODO: deal with energy loss and fresnel effect
		return (*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time);
	}

	__host__ __device__
	static float evaluate_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, RNG* rng) {
		if (wo[2] * wi[2] <= 0.f) return 0.f;
		Vectr3F wh = wo + wi;
		const float lengthE2 = wh.lengthSquared();
		if (lengthE2 == 0.f) return 0.f;
		wh /= sqrt(lengthE2);
		const float fresnelFactor = cast(bxdf)->fresnel ? Fresnel::reflectance(ior1, ior2, absDot(wo, wh)) : 1.f;
		return (*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time) * cast(bxdf)->distribution->DG(g, wlen, time, wo, wi, wh) * fresnelFactor / (4.f * abs(wo[2] * wi[2]));
	}

	__host__ __device__
	static float sample_wi_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng) {
		// importance sample wh
		Vectr3F wh = cast(bxdf)->distribution->sampleWh(g, wlen, time, wo, rnd, pdf);
		*wi = reflect(wo, wh);
		*pdf /= (4.f * absDot(wo, wh));
		if (wo[2] * (*wi)[2] <= 0.f) return 0.f;
		const float fresnelFactor = cast(bxdf)->fresnel ? Fresnel::reflectance(ior1, ior2, absDot(wo, wh)) : 1.f;
		return (*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time) * cast(bxdf)->distribution->DG(g, wlen, time, wo, *wi, wh) * fresnelFactor / (4.f * abs(wo[2] * (*wi)[2]));
	}

	__host__ __device__
	static float sample_wi_pdf_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, bool sampled, RNG* rng) {
		// importance sample wh pdf
		Vectr3F wh = wo + wi;
		const float lengthE2 = wh.lengthSquared();
		if (lengthE2 == 0.f) return 0.f;
		wh /= sqrt(lengthE2);
		if (wo[2] * wh[2] < 0.f) wh = -wh;
		return cast(bxdf)->distribution->sampleWhPDF(g, wlen, time, wo, wh) / (4.f * absDot(wo, wh));
	}

	__host__
	MicrofacetBRDF(
		const Pointer<const ShadingNode<float>>& reflectance,
		const Pointer<const MicrofacetModel>& distribution,
		const bool fresnel,
		const interacting_t& interacting_,
		const albedo_t& albedo_,
		const evaluate_t& evaluate_,
		const sample_wi_t& sample_wi_,
		const sample_wi_pdf_t& sample_wi_pdf_
	):
		BxDF(Type(DIFFUSE | REFLECTIVE), interacting_, albedo_, evaluate_, sample_wi_, sample_wi_pdf_),
		reflectance(reflectance),
		distribution(distribution),
		fresnel(fresnel) {
	}

public:
	/*!
	 * \brief
	 * Creates a new MicrofacetBRDF.
	 *
	 * \param[in] reflectance The spectral reflectance at each point on the
	 * surface, must be greater than or equal to 0 and less than or equal to 1
	 * everywhere, otherwise the BxDF's behaviour is undefined.
	 * \param[in] distribution The Microfacet distribution that determines how
	 * much the microfacet normals differ from the macrosurface normal.
	 * \param[in] fresnel If the unpolarized
	 * <a href="https://en.wikipedia.org/wiki/Fresnel_equations">fresnel</a>
	 * factor should be used to modulate the amount of reflected energy.
	 */
	__host__
	static Pointer<MicrofacetBRDF> build(
		MemoryPool& pool,
		const Pointer<const ShadingNode<float>>& reflectance,
		const Pointer<const MicrofacetModel>& distribution,
		const bool fresnel
	) {
		return Pointer<MicrofacetBRDF>::make(
			pool, reflectance, distribution, fresnel,
			interacting_t(virtualize(interacting_)),
			albedo_t(virtualize(albedo_)),
			evaluate_t(virtualize(evaluate_)),
			sample_wi_t(virtualize(sample_wi_)),
			sample_wi_pdf_t(virtualize(sample_wi_pdf_))
		);
	}
};


}

#endif /* MICROFACETBRDF_CUH_ */

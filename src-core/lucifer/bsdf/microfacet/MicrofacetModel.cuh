#ifndef MICROFACETMODEL_CUH_
#define MICROFACETMODEL_CUH_

#include "lucifer/math/geometry/Geometry.cuh"
#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/memory/Virtual.cuh"

namespace lucifer {

/*!
 * \brief
 * Microsurface model interface.
 *
 * A meaningful microsurface model is described by a distribution of normals,
 * which models how the microfacets are statistically oriented, and a
 * microsurface profile, which models how the microfacets are organized on the
 * microsurface.
 *
 * When the microfacet does not model multiple scatering from the microsurface,
 * the microfacet-based BRDF will not be normalized and will not integrate to
 * 1. Even in this case, the weak white furnace test can be used to verify that
 * the microfacet-based BRDF is well designed:
 * Integral[(G1(wo, wh)D(wh)/(4|wg.wo|))dwi] = 1
 *
 * Based on:
 * Understanding  the  Masking-Shadowing  Function in  Microfacet-Based  BRDFs.
 * by Eric  Heitz.
 *
 * In order to simplify the implementation of microfacet based BSDFs:
 * * All geometric data must be given and will be returned in local shading
 * coordinates where the geometric surface normal vector is aligned with the z
 * axis and the geometric surface tangent vector is alined with the x axis.
 * * The microsurface normal vector is supposed to be given and must always be
 * returned in the same hemisphere of the given outgoing direction vector `wo`.
 * * The model must account for both sides of the geometric surface, positive
 * and negative z-axis normal vector.
 *
 * \author
 * Bruno Pera.
 */
class MicrofacetModel: public Polymorphic {
public:
	using nd_t  = Virtual<  float, const MicrofacetModel*, const Geometry&, const float, const float, const Vectr3F&>;
	using g1_t  = Virtual<  float, const MicrofacetModel*, const Geometry&, const float, const float, const Vectr3F&, const Vectr3F&>;
	using g2_t  = Virtual<  float, const MicrofacetModel*, const Geometry&, const float, const float, const Vectr3F&, const Vectr3F&, const Vectr3F&>;
	using dg_t  = Virtual<  float, const MicrofacetModel*, const Geometry&, const float, const float, const Vectr3F&, const Vectr3F&, const Vectr3F&>;
	using pdf_t = Virtual<  float, const MicrofacetModel*, const Geometry&, const float, const float, const Vectr3F&, const Vectr3F&> ;
	using smp_t = Virtual<Vectr3F, const MicrofacetModel*, const Geometry&, const float, const float, const Vectr3F&, const float[2], float*>;

private:
	const  nd_t  nd_;
	const  g1_t  g1_;
	const  g2_t  g2_;
	const  dg_t  dg_;
	const pdf_t pdf_;
	const smp_t smp_;

protected:
	__host__
	MicrofacetModel(
		const  nd_t&  nd_,
		const  g1_t&  g1_,
		const  g2_t&  g2_,
		const  dg_t&  dg_,
		const pdf_t& pdf_,
		const smp_t& smp_
	): nd_(nd_), g1_(g1_), g2_(g2_), dg_(dg_), pdf_(pdf_), smp_(smp_) {
	}

public:
	/*!
	 * \brief
	 * Computes the value of the distribution of microfacet normals.
	 *
	 * The distribution of normals models how the microfacet normals are
	 * oriented. The main property of a physically correct distribution of
	 * normals is that the project area of the microsurface over the
	 * geometric normal must equal the area of the geometric surface. That is:
	 * Integral[(wh . gn) * D(wh)dwh] = 1.
	 *
	 * \param[in] g The local geometry of a point on the geometric surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wh The normalized microsurface normal vector.
	 *
	 * \return The distribution value at `g` for wavelength `wlen`.
	 */
	__host__ __device__
	float D (const Geometry& g, const float wlen, const float time, const Vectr3F& wh) const {
		return nd_(this, g, wlen, time, wh);
	}

	/*!
	 * \brief
	 * Computes the masking function.
	 *
	 * The masking function gives the probability that a microfacet is visible
	 * from the outgoing direction, given the microfacet normal vector `wh`.
	 * The main property of a physycally correct masking function is that the
	 * projected area of the visible microsurface onto the outgoing direction
	 * must be exactly the projected area of the geometric surface onto the
	 * outgoing direction. That is:
	 * Integral[G1(wo, wh)<wo, wh>D(wh)dWh] = cosTheta(wo).
	 *
	 * \param[in] g The local geometry of a point on the geometric surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wo The normalized outgoing direction.
	 * \param[in] wh The normalized microsurface normal vector.
	 *
	 * \return The masking function value.
	 */
	__host__ __device__
	float G1(const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wh) const {
		return g1_(this, g, wlen, time, wo, wh);
	}

	/*!
	 * \brief
	 * Computes the shadowing-masking function.
	 *
	 * The shadowing-masking function gives the probability that a microfacet
	 * is visible from the outgoing direction and from the incoming direction,
	 * given the microfacet normal vector `wh`.
	 *
	 * \param[in] g The local geometry of a point on the geometric surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wo The normalized outgoing direction.
	 * \param[in] wi The normalized incoming direction.
	 * \param[in] wh The normalized microsurface normal vector.
	 *
	 * \return The shadowing-masking function value.
	 */
	__host__ __device__
	float G2(const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const Vectr3F& wh) const {
		return g2_(this, g, wlen, time, wo, wi, wh);
	}

	/*!
	 * \brief
	 * Computes the product of `D` and `G2`.
	 *
	 * \param[in] g The local geometry of a point on the geometric surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wo The normalized outgoing direction.
	 * \param[in] wi The normalized incoming direction.
	 * \param[in] wh The normalized microsurface normal vector.
	 *
	 * \return The product of `D` and `G2`.
	 */
	__host__ __device__
	float DG(const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const Vectr3F& wh) const {
		return dg_(this, g, wlen, time, wo, wi, wh);
	}

	/*!
	 * \brief
	 * Computes the PDF of sampling `wh` given the outgoing direction `wo`.
	 *
	 * \param[in] g The local geometry of a point on the geometric surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wo The normalized outgoing direction.
	 * \param[in] wh The normalized microsurface normal vector.
	 *
	 * \return The PDF with respect to the solid angle measure.
	 */
	__host__ __device__
	float sampleWhPDF(const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wh) const {
		return pdf_(this, g, wlen, time, wo, wh);
	}

	/*!
	 * \brief
	 * Samples a microfacet normal vector given an outgoing direction `wo`.
	 *
	 * \param[in] g The local geometry of a point on the geometric surface.
	 * \param[in] wlen The light wavelength in meters.
	 * \param[in] time The normalized time in [0, 1].
	 * \param[in] wo The normalized outgoing direction.
	 * \param[out] pdf Outputs the PDF of sampling the microfacet normal `wh`.
	 *
	 * \return The sampled microfacet normal vector `wh`.
	 */
	__host__ __device__
	Vectr3F sampleWh(const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const float rnd[2], float* pdf) const {
		return smp_(this, g, wlen, time, wo, rnd, pdf);
	}
};

}

#endif /* MICROFACETMODEL_CUH_ */

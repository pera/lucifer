#ifndef SMITHTROWBRIDGEREITZMODEL_CUH_
#define SMITHTROWBRIDGEREITZMODEL_CUH_

#include <cassert>
#include "MicrofacetModel.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/Util.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * Trowbridge-Reitz (also known as GGX) distribution using the Smith profile.
 *
 * The sampling routine is based on:
 * A Simpler and Exact Sampling Routine for the GGX Distribution of Visible Normals.
 * by Eric  Heitz.
 *
 * \author
 * Bruno Pera.
 *
 * Note:
 * The following is the estimated effective reflectance r(cosTheta(wo)) for
 * unit isotropic roughness that could be used with the stretch invariance
 * principle to implement a simple compensation mechanism for the lack of
 * multiple scattering:
 * 1.0000000, 0.9787390, 0.9610450, 0.9474260, 0.9348810, 0.9226170, 0.9109680,
 * 0.9004990, 0.8905600, 0.8808250, 0.8713990, 0.8625290, 0.8540700, 0.8458110,
 * 0.8377050, 0.8298950, 0.8224640, 0.8152360, 0.8080740, 0.8011030, 0.7943970,
 * 0.7879020, 0.7815220, 0.7752130, 0.7690450, 0.7631300, 0.7573470, 0.7516280,
 * 0.7459860, 0.7404710, 0.7351270, 0.7299090, 0.7247280, 0.7196270, 0.7146260,
 * 0.7097600, 0.7049970, 0.7002780, 0.6956030, 0.6910290, 0.6865590, 0.6821770,
 * 0.6778540, 0.6735450, 0.6693420, 0.6651990, 0.6611330, 0.6571530, 0.6531810,
 * 0.6492670, 0.6454170, 0.6416390, 0.6379290, 0.6342550, 0.6306140, 0.6270210,
 * 0.6234860, 0.6200020, 0.6165800, 0.6131980, 0.6098230, 0.6065130, 0.6032460,
 * 0.6000260, 0.5968530, 0.5937210, 0.5906000, 0.5875230, 0.5844950, 0.5814990,
 * 0.5785300, 0.5756160, 0.5727260, 0.5698490, 0.5670290, 0.5642320, 0.5614630,
 * 0.5587370, 0.5560420, 0.5533580, 0.5507070, 0.5481000, 0.5455130, 0.5429360,
 * 0.5404080, 0.5379090, 0.5354200, 0.5329560, 0.5305320, 0.5281270, 0.5257370,
 * 0.5233790, 0.5210550, 0.5187350, 0.5164350, 0.5141790, 0.5119360, 0.5097050,
 * 0.5074920, 0.5053210, 0.5031670, 0.5010130, 0.4988870, 0.4967940, 0.4947160,
 * 0.4926420, 0.4905990, 0.4885800, 0.4865730, 0.4845730, 0.4825960, 0.4806500,
 * 0.4787190, 0.4767870, 0.4748840, 0.4729990, 0.4711290, 0.4692670, 0.4674210,
 * 0.4655990, 0.4637980, 0.4620040, 0.4602170, 0.4584540, 0.4567080, 0.4549760,
 * 0.4532500, 0.4515340, 0.4498510, 0.4481720, 0.4465010, 0.4448450, 0.4432060,
 * 0.4415800, 0.4399680, 0.4383590, 0.4367660, 0.4351920, 0.4336340, 0.4320820,
 * 0.4305310, 0.4290010, 0.4274870, 0.4259820, 0.4244840, 0.4229930, 0.4215200,
 * 0.4200600, 0.4186090, 0.4171620, 0.4157290, 0.4143080, 0.4129000, 0.4115100,
 * 0.4101130, 0.4087300, 0.4073570, 0.4060010, 0.4046550, 0.4033090, 0.4019700,
 * 0.4006430, 0.3993350, 0.3980380, 0.3967360, 0.3954480, 0.3941620, 0.3928970,
 * 0.3916390, 0.3903870, 0.3891410, 0.3879020, 0.3866690, 0.3854530, 0.3842450,
 * 0.3830400, 0.3818450, 0.3806520, 0.3794720, 0.3783020, 0.3771410, 0.3759790,
 * 0.3748280, 0.3736790, 0.3725510, 0.3714200, 0.3703020, 0.3691840, 0.3680800,
 * 0.3669700, 0.3658850, 0.3647960, 0.3637220, 0.3626440, 0.3615760, 0.3605130,
 * 0.3594570, 0.3584130, 0.3573770, 0.3563380, 0.3553080, 0.3542840, 0.3532670,
 * 0.3522570, 0.3512590, 0.3502590, 0.3492620, 0.3482750, 0.3472910, 0.3463200,
 * 0.3453540, 0.3443890, 0.3434320, 0.3424780, 0.3415300, 0.3405880, 0.3396470,
 * 0.3387200, 0.3377960, 0.3368750, 0.3359550, 0.3350420, 0.3341360, 0.3332350,
 * 0.3323440, 0.3314560, 0.3305690, 0.3296850, 0.3288110, 0.3279400, 0.3270720,
 * 0.3262150, 0.3253610, 0.3245020, 0.3236530, 0.3228100, 0.3219750, 0.3211390,
 * 0.3203120, 0.3194890, 0.3186690, 0.3178480, 0.3170360, 0.3162260, 0.3154210,
 * 0.3146240, 0.3138250, 0.3130380, 0.3122520, 0.3114650, 0.3106860, 0.3099100,
 * 0.3091360, 0.3083730, 0.3076140, 0.3068520
 */
class SmithTrowbridgeReitzModel: public MicrofacetModel {
private:
	const Pointer<const ShadingNode<float>> alphax;
	const Pointer<const ShadingNode<float>> alphay;

private:
	__host__ __device__
	static float lambda(const float axE2, const float ayE2, const Vectr3F& w) {
		const float tan2Theta = tanE2Theta(w);
		if (isinf(tan2Theta)) return +INFINITY;
		const float aE2 = cosE2Phi(w) * axE2 + sinE2Phi(w) * ayE2;
		return .5f * (-1.f + sqrt(1.f + aE2 * tan2Theta));
	}

	__host__ __device__
	static float D (const float ax, const float ay, const float tanE2ThetaWh, const Vectr3F& wh) {
		if (isinf(tanE2ThetaWh)) return 1.f;
		const float cos2Theta = cosE2Theta(wh);
		const float factor = 1.f + tanE2ThetaWh * (cosE2Phi(wh) / (ax * ax) + sinE2Phi(wh) / (ay * ay));
		return 1.f / (PI<float>() * ax * ay * cos2Theta * cos2Theta * factor * factor);
	}

	__host__ __device__
	static const SmithTrowbridgeReitzModel* cast(const MicrofacetModel* model) {
		return static_cast<const SmithTrowbridgeReitzModel*>(model);
	}

	__host__ __device__
	static float nd_(const MicrofacetModel* model, const Geometry& g, const float wlen, const float time, const Vectr3F& wh) {
		const float tanE2ThetaWh = tanE2Theta(wh);
		if (isinf(tanE2ThetaWh)) return 1.f;
		const float ax = (*cast(model)->alphax)(g.gp, g.su, g.sn, g.uv, wlen, time);
		const float ay = (*cast(model)->alphay)(g.gp, g.su, g.sn, g.uv, wlen, time);
		return D(ax, ay, tanE2ThetaWh, wh);
	}

	__host__ __device__
	static float g1_(const MicrofacetModel* model, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wh) {
		assert(sameHemisphere(wo, wh));
		if (dot(wo, wh) <= 0.f) return 0.f;
		const float axE2 = pow2((*cast(model)->alphax)(g.gp, g.su, g.sn, g.uv, wlen, time));
		const float ayE2 = pow2((*cast(model)->alphay)(g.gp, g.su, g.sn, g.uv, wlen, time));
		return 1.f / (1.f + lambda(axE2, ayE2, wo));
	}

	__host__ __device__
	static float g2_(const MicrofacetModel* model, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const Vectr3F& wh) {
		assert(sameHemisphere(wo, wh));
		assert(sameHemisphere(wi, wh));
		if (dot(wo, wh) <= 0.f or dot(wi, wh) <= 0.f) return 0.f;
		const float axE2 = pow2((*cast(model)->alphax)(g.gp, g.su, g.sn, g.uv, wlen, time));
		const float ayE2 = pow2((*cast(model)->alphay)(g.gp, g.su, g.sn, g.uv, wlen, time));
		return  1.f / (1.f + lambda(axE2, ayE2, wo) + lambda(axE2, ayE2, wi));
	}

	__host__ __device__
	static float dg_(const MicrofacetModel* model, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const Vectr3F& wh) {
		assert(sameHemisphere(wo, wh));
		assert(sameHemisphere(wi, wh));
		const float tanE2ThetaWh = tanE2Theta(wh);
		if (dot(wo, wh) <= 0.f or dot(wi, wh) <= 0.f) return 0.f;
		const float ax = (*cast(model)->alphax)(g.gp, g.su, g.sn, g.uv, wlen, time);
		const float ay = (*cast(model)->alphay)(g.gp, g.su, g.sn, g.uv, wlen, time);
		return D(ax, ay, tanE2ThetaWh, wh) / (1.f + lambda(ax * ax, ay * ay, wo) + lambda(ax * ax, ay * ay, wi));
	}

	__host__ __device__
	static float pdf_(const MicrofacetModel* model, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wh) {
		if (wo[2] * wh[2] < 0.f or dot(wo, wh) < 0.f) return 0.f;
		const float tanE2ThetaWh = tanE2Theta(wh);
		const float ax = (*cast(model)->alphax)(g.gp, g.su, g.sn, g.uv, wlen, time);
		const float ay = (*cast(model)->alphay)(g.gp, g.su, g.sn, g.uv, wlen, time);
		return absDot(wo, wh) * D(ax, ay, tanE2ThetaWh, wh) / (abs(wo[2]) * (1.f + lambda(ax * ax, ay * ay, wo)));
	}

	__host__ __device__
	static Vectr3F smp_(const MicrofacetModel* model, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const float rnd[2], float* pdf) {
		// evaluate roughness parameters
		const float ax = (*cast(model)->alphax)(g.gp, g.su, g.sn, g.uv, wlen, time);
		const float ay = (*cast(model)->alphay)(g.gp, g.su, g.sn, g.uv, wlen, time);
		// stretch wo
		const Vectr3F w_ = Vectr3F(ax * wo[0], ay * wo[1], abs(wo[2])).normalize();
		// construct orthonormal basis
		const Vectr3F t1 = w_[2] < 0.9999 ? cross<Vectr3F, Vectr3F, Vectr3F>(w_, Vectr3F(0.f, 0.f, 1.f)).normalize() : Vectr3F(1.f, 0.f, 0.f);
		const Vectr3F t2 = cross<Vectr3F, Vectr3F, Vectr3F>(t1, w_);
		// sample point with polar coordinates (r, phi)
		const float a = 1.f / (1.f + w_[2]);
		const float r = sqrt(rnd[0]);
		const float phi = (rnd[1] < a) ? PI<float>() * rnd[1] / a : PI<float>() + PI<float>() * (rnd[1] - a) / (1.f - a);
		const float p1 = r * cos(phi);
		const float p2 = r * sin(phi) * ((rnd[2] < a) ? 1.f : w_[2]);
		// compute microfacet normal
		Vectr3F wh = t1 * p1 + t2 * p2 + sqrt(max(0.f, 1.f - p1 * p1 - p2 * p2)) * w_;
		// unstretch
		wh = Vectr3F(ax * wh[0], ay * wh[1], max(0.f, wh[2])).normalize();
		if (wo[2] < 0.f) wh[2] = -wh[2];
		// compute pdf
		*pdf = absDot(wo, wh) * D(ax, ay, tanE2Theta(wh), wh) / (abs(wo[2]) * (1.f + lambda(ax * ax, ay * ay, wo)));
		// ok
		return wh;
	}

	__host__
	SmithTrowbridgeReitzModel(
		const Pointer<const ShadingNode<float>>& alphax,
		const Pointer<const ShadingNode<float>>& alphay,
		const  nd_t&  nd_,
		const  g1_t&  g1_,
		const  g2_t&  g2_,
		const  dg_t&  dg_,
		const pdf_t& pdf_,
		const smp_t& smp_
	): MicrofacetModel(nd_, g1_, g2_, dg_, pdf_, smp_), alphax(alphax), alphay(alphay){
	}

public:
	friend class TypedPool<SmithTrowbridgeReitzModel>;

	/*!
	 * \brief
	 * Creates a new TrowbridgeReitz microfacet distribution
	 *
	 * \param[in] alphax The roughness along the u direction.
	 * \param[in] alphay The roughness along the v direction.
	 */
	__host__
	static Pointer<SmithTrowbridgeReitzModel> build(
		MemoryPool& pool,
		const Pointer<const ShadingNode<float>>& alphax,
		const Pointer<const ShadingNode<float>>& alphay)
	{
		return Pointer<SmithTrowbridgeReitzModel>::make(
			pool, alphax, alphay,
			 nd_t(virtualize( nd_)),
			 g1_t(virtualize( g1_)),
			 g2_t(virtualize( g2_)),
			 dg_t(virtualize( dg_)),
			pdf_t(virtualize(pdf_)),
			smp_t(virtualize(smp_))
		);
	}
};

}

#endif /* SMITHTROWBRIDGEREITZMODEL_CUH_ */

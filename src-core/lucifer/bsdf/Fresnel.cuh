#ifndef FRESNEL_CUH_
#define FRESNEL_CUH_

#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/math/Math.cuh"

namespace lucifer {

class Fresnel {
private:
	__host__ __device__
	Fresnel() {
	}

	__host__ __device__
	inline static float pow5(const float x) {
		return x * x * x * x * x;
	}

public:
	__host__ __device__
	static float reflectance(const ComplexF& ior1, const ComplexF& ior2, const float cosTheta1) {
		// find cosTheta2
		const float sinTheta1 = sqrt(max(0.f, 1.f - cosTheta1 * cosTheta1));
		const float sinTheta2 = ior1.re * sinTheta1 / ior2.re;
		if (sinTheta2 >= 1.f) {
			// total internal reflection
			return 1.0f;
		}
		const float cosTheta2 = sqrt(max(0.f, 1.f - sinTheta2 * sinTheta2));
		float r = 0.f;
		// perpendicular polarization
		ComplexF a = ior1 * cosTheta1;
		ComplexF b = ior2 * cosTheta2;
		r += ((a - b) / (a + b)).lengthSquared();
		// parallel polarization
		a = ior1 * cosTheta2;
		b = ior2 * cosTheta1;
		r += ((a - b) / (a + b)).lengthSquared();
		// unpolarized
		return 0.5f * r;
	}
};

}

#endif /* FRESNEL_CUH_ */

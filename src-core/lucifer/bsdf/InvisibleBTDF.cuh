#ifndef INVISIBLEBTDF_CUH_
#define INVISIBLEBTDF_CUH_

#include "BxDF.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * A BxDF that does not interact with light.
 *
 * An InvisibleBTDF is a delta BTDF that does not scatter any light. The
 * sampled incoming direction is always the negative outgoing direction.
 *
 * \author
 * Bruno Pera.
 */
class InvisibleBTDF: public BxDF {
private:
	friend class TypedPool<InvisibleBTDF>;

	__host__ __device__
	static bool interacting_(const BxDF* bxdf) {
		return false;
	}

	__host__ __device__
	static float albedo_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode) {
		return 1.f;
	}

	__host__ __device__
	static float evaluate_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, RNG* rng) {
		return 0.f;
	}

	__host__ __device__
	static float sample_wi_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng) {
		*wi = -wo;
		*pdf = 1.f;
		return 1.f / absCosTheta(*wi);
	}

	__host__ __device__
	static float sample_wi_pdf_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, bool sampled, RNG* rng) {
		return sampled ? 1.f : 0.f;
	}

	__host__
	InvisibleBTDF(
		const interacting_t& interacting_,
		const albedo_t& albedo_,
		const evaluate_t& evaluate_,
		const sample_wi_t& sample_wi_,
		const sample_wi_pdf_t& sample_wi_pdf_
	):
		BxDF(BxDF::Type(SPECULAR | REFRACTIVE), interacting_, albedo_, evaluate_, sample_wi_, sample_wi_pdf_) {
	}

public:
	/*!
	 * \brief
	 * Creates a new InvisibleBTDF.
	 */
	__host__
	static Pointer<InvisibleBTDF> build(MemoryPool& pool) {
		return Pointer<InvisibleBTDF>::make(
			pool,
			interacting_t(virtualize(interacting_)),
			albedo_t(virtualize(albedo_)),
			evaluate_t(virtualize(evaluate_)),
			sample_wi_t(virtualize(sample_wi_)),
			sample_wi_pdf_t(virtualize(sample_wi_pdf_))
		);
	}
};

}

#endif /* INVISIBLEBTDF_CUH_ */

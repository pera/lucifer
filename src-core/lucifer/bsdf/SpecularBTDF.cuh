#ifndef SPECULARBTDF_CUH_
#define SPECULARBTDF_CUH_

#include "BxDF.cuh"
#include "Fresnel.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"

namespace lucifer {

/*!
 * \brief
 * Specular BTDF
 *
 * A SpecularBTDF is a delta distribution in which the incident light is
 * transmitted only into the specular direction. The amount of energy actually
 * transmitted will vary according to the outgoing grazing angle if the
 * `fresnel` option is turned on.
 *
 * \author
 * Bruno Pera.
 */
class SpecularBTDF: public BxDF {
private:
	friend class TypedPool<SpecularBTDF>;
	const Pointer<const ShadingNode<float>> transmittance;
	const bool fresnel;

	__host__ __device__
	static const SpecularBTDF* cast(const BxDF* bxdf) {
		return static_cast<const SpecularBTDF*>(bxdf);
	}

	__host__ __device__
	static bool interacting_(const BxDF* bxdf) {
		return true;
	}

	__host__ __device__
	static float albedo_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode) {
		const float f = cast(bxdf)->fresnel ? Fresnel::reflectance(ior1, ior2, absCosTheta(wo)) : 0.f;
		const float m = (mode == Mode::RADIANCE) ? 1.f : (ior1.re * ior1.re) / (ior2.re * ior2.re);
		return (*cast(bxdf)->transmittance)(g.gp, g.su, g.sn, g.uv, wlen, time) * (1.f - f) * m;
	}

	__host__ __device__
	static float evaluate_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, RNG* rng) {
		return 0.f;
	}

	__host__ __device__
	static float sample_wi_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng) {
		if (!refract(wo, ior1.re / ior2.re, wi)) {
			*pdf = 0.f;
			return 0.f;
		}
		*pdf = 1.f;
		const float f = cast(bxdf)->fresnel ? Fresnel::reflectance(ior1, ior2, absCosTheta(wo)) : 0.f;
		const float m = (mode == Mode::RADIANCE) ? 1.f : (ior1.re * ior1.re) / (ior2.re * ior2.re);
		return (*cast(bxdf)->transmittance)(g.gp, g.su, g.sn, g.uv, wlen, time) * (1.f - f) * m / absCosTheta(*wi);
	}

	__host__ __device__
	static float sample_wi_pdf_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, bool sampled, RNG* rng) {
		return sampled ? 1.f : 0.f;
	}

	__host__
	SpecularBTDF(
		const Pointer<const ShadingNode<float>>& transmittance,
		const bool fresnel,
		const interacting_t& interacting_,
		const albedo_t& albedo_,
		const evaluate_t& evaluate_,
		const sample_wi_t& sample_wi_,
		const sample_wi_pdf_t& sample_wi_pdf_
	):
		BxDF(BxDF::Type(SPECULAR | REFRACTIVE), interacting_, albedo_, evaluate_, sample_wi_, sample_wi_pdf_), transmittance(transmittance), fresnel(fresnel) {
	}

public:
	/*!
	 * \brief
	 * Creates a new SpecularBTDF.
	 *
	 * \param[in] transmittance The spectral transmittance at each point on the
	 * surface, must be greater than or equal to 0 and less than or equal to 1
	 *  everywhere, otherwise the BxDF's behaviour is undefined.
	 * \param[in] fresnel If the unpolarized
	 * <a href="https://en.wikipedia.org/wiki/Fresnel_equations">fresnel</a>
	 * factor should be used to scale down the transmittance according to the
	 * outgoing angle.
	 */
	__host__
	static Pointer<SpecularBTDF> build(MemoryPool& pool, const Pointer<const ShadingNode<float>>& transmittance, const bool fresnel) {
		return Pointer<SpecularBTDF>::make(
			pool, transmittance, fresnel,
			interacting_t(virtualize(interacting_)),
			albedo_t(virtualize(albedo_)),
			evaluate_t(virtualize(evaluate_)),
			sample_wi_t(virtualize(sample_wi_)),
			sample_wi_pdf_t(virtualize(sample_wi_pdf_))
		);
	}
};

}

#endif /* SPECULARBTDF_CUH_ */

#ifndef LAMBERTIANBRDF_CUH_
#define LAMBERTIANBRDF_CUH_

#include "BxDF.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"
#include "lucifer/random/CosineHemisphereDistribution.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * An ideal lambertian BRDF.
 *
 * A LambertianBRDF reflects light uniformly along the entire hemisphere of
 * incident light. The apparent brightness of a lambertian BRDF to an observer
 * is the same regardless of the observer's angle of view.
 *
 * \author
 * Bruno Pera.
 */
class LambertianBRDF: public BxDF {
private:
	friend class TypedPool<LambertianBRDF>;
	const Pointer<const ShadingNode<float>> reflectance;

	__host__ __device__
	static const LambertianBRDF* cast(const BxDF* bxdf) {
		return static_cast<const LambertianBRDF*>(bxdf);
	}

	__host__ __device__
	static bool interacting_(const BxDF* bxdf) {
		return true;
	}

	__host__ __device__
	static float albedo_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode) {
		return (*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time);
	}

	__host__ __device__
	static float evaluate_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, RNG* rng) {
		return sameHemisphere(wi, wo) ? (*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time) / PI<float>() : 0.f;
	}

	__host__ __device__
	static float sample_wi_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng) {
		CosineHemisphereDistribution::sample(rnd, &((*wi)[0]), pdf);
		if (wo[2] < 0.f) (*wi)[2] = -((*wi)[2]);
		return (*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time) / PI<float>();
	}

	__host__ __device__
	static float sample_wi_pdf_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, bool sampled, RNG* rng) {
		if (!sameHemisphere(wi, wo)) return 0.f;
		Vectr3F w(wi[0], wi[1], abs(wi[2]));
		return CosineHemisphereDistribution::pdf(&(w[0]));
	}

	__host__
	LambertianBRDF(
		const Pointer<const ShadingNode<float>>& reflectance,
		const interacting_t& interacting_,
		const albedo_t& albedo_,
		const evaluate_t& evaluate_,
		const sample_wi_t& sample_wi_,
		const sample_wi_pdf_t& sample_wi_pdf_
	):
		BxDF(Type(DIFFUSE | REFLECTIVE), interacting_, albedo_, evaluate_, sample_wi_, sample_wi_pdf_), reflectance(reflectance) {
	}

public:
	/*!
	 * \brief
	 * Creates a new LambertianBRDF.
	 *
	 * \param[in] reflectance The spectral reflectance at each point on the
	 * surface, must be greater than or equal to 0 and less than or equal to 1
	 *  everywhere, otherwise the BxDF's behaviour is undefined.
	 */
	__host__
	static Pointer<LambertianBRDF> build(MemoryPool& pool, const Pointer<const ShadingNode<float>>& reflectance) {
		return Pointer<LambertianBRDF>::make(
			pool, reflectance,
			interacting_t(virtualize(interacting_)),
			albedo_t(virtualize(albedo_)),
			evaluate_t(virtualize(evaluate_)),
			sample_wi_t(virtualize(sample_wi_)),
			sample_wi_pdf_t(virtualize(sample_wi_pdf_))
		);
	}
};

}

#endif /* LAMBERTIANBRDF_CUH_ */

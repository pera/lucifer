#ifndef ORENNAYARBRDF_CUH_
#define ORENNAYARBRDF_CUH_

#include "BxDF.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"
#include "lucifer/random/CosineHemisphereDistribution.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * Oren-Nayar rough reflection BRDF.
 *
 * The OrenNayarBRDF implements the
 * <a href="https://en.wikipedia.org/wiki/Oren%E2%80%93Nayar_reflectance_model">
 * </a>rough reflection model proposed by Michael Oren and Shree K. Nayar. The
 * appearance is quite similar to the ideal LambertianBRDF, but with less
 * darkening at grazing angles.
 *
 * \author
 * Bruno Pera.
 */
class OrenNayarBRDF: public BxDF {
private:
	const Pointer<const ShadingNode<float>> reflectance;
	const Pointer<const ShadingNode<float>> stddev;

	__host__ __device__
	static const OrenNayarBRDF* cast(const BxDF* bxdf) {
		return static_cast<const OrenNayarBRDF*>(bxdf);
	}

	__host__ __device__
	static bool interacting_(const BxDF* bxdf) {
		return true;
	}

	__host__ __device__
	static float albedo_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode) {
		return (*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time);
	}

	__host__ __device__
	static float evaluate_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, RNG* rng) {
		if (!sameHemisphere(wi, wo)) return 0.f;
		const float var = pow2((*cast(bxdf)->stddev)(g.gp, g.su, g.sn, g.uv, wlen, time));
		const float a = 1.00f - (var / (2.f * (var + 0.33f)));
		const float b = 0.45f * (var / (var + 0.09f));
		// cosine term
		float maxCos = 0.f;
		const float sinThetaI = sinTheta(wi);
		const float sinThetaO = sinTheta(wo);
		if (sinThetaI > 1e-4f && sinThetaO > 1e-4f) {
			maxCos = max(0.f, cosPhi(wi) * cosPhi(wo) + sinPhi(wi) * sinPhi(wo));
		}
		// sine and tangent terms
		float sinAlpha;
		float tanBeta;
		if (absCosTheta(wi) > absCosTheta(wo)) {
			sinAlpha = sinThetaO;
			tanBeta = sinThetaI / absCosTheta(wi);
		} else {
			sinAlpha = sinThetaI;
			tanBeta = sinThetaO / absCosTheta(wo);
		}
		// done
		return (*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time) * INV_PI<float>() * (a + b * maxCos * sinAlpha * tanBeta);
	}

	__host__ __device__
	static float sample_wi_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng) {
		CosineHemisphereDistribution::sample(rnd, &((*wi)[2]), pdf);
		if (wo[2] < 0.f) (*wi)[2] = -((*wi)[2]);
		return evaluate_(bxdf, g, wlen, time, *wi, wo, ior1, ior2, mode, rng);
	}

	__host__ __device__
	static float sample_wi_pdf_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, bool sampled, RNG* rng) {
		if (!sameHemisphere(wi, wo)) return 0.f;
		Vectr3F w(wi[0], wi[1], abs(wi[2]));
		return CosineHemisphereDistribution::pdf(&(w[0]));
	}

	__host__
	OrenNayarBRDF(
		const Pointer<const ShadingNode<float>>& reflectance,
		const Pointer<const ShadingNode<float>>& stddev,
		const interacting_t& interacting_,
		const albedo_t& albedo_,
		const evaluate_t& evaluate_,
		const sample_wi_t& sample_wi_,
		const sample_wi_pdf_t& sample_wi_pdf_
	):
		BxDF(Type(DIFFUSE | REFLECTIVE), interacting_, albedo_, evaluate_, sample_wi_, sample_wi_pdf_), reflectance(reflectance), stddev(stddev) {
	}

public:
	friend class TypedPool<OrenNayarBRDF>;

	/*!
	 * \brief
	 * Creates a new OrenNayarBRDF.
	 *
	 * \param[in] reflectance The spectral reflectance at each point on the
	 * surface, must be greater than or equal to 0 and less than or equal to 1
	 * everywhere, otherwise the BxDF's behaviour is undefined.
	 * \param[in] stddev The standard deviation of the microfacets orientation,
	 * is used as measure of roughness, must be greater than or equal to 0,
	 * with 0 corresponding to be the ideal isotropic lambertian reflection.
	 */
	__host__
	static Pointer<OrenNayarBRDF> build(
		MemoryPool& pool,
		const Pointer<const ShadingNode<float>>& reflectance,
		const Pointer<const ShadingNode<float>>& stddev
	) {
		return Pointer<OrenNayarBRDF>::make(
			pool, reflectance, stddev,
			interacting_t(virtualize(interacting_)),
			albedo_t(virtualize(albedo_)),
			evaluate_t(virtualize(evaluate_)),
			sample_wi_t(virtualize(sample_wi_)),
			sample_wi_pdf_t(virtualize(sample_wi_pdf_))
		);
	}
};

}

#endif /* ORENNAYARBRDF_CUH_ */

#ifndef MICROFACETBTDF_CUH_
#define MICROFACETBTDF_CUH_

#include "BxDF.cuh"
#include "Fresnel.cuh"
#include "microfacet/MicrofacetModel.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * A specular
 * <a href="https://en.wikipedia.org/wiki/Specular_highlight#Microfacets">microfacet</a>
 * distribution based BTDF.
 *
 * The MicrofacetBTDF is a bidirectional transmittance distribution function
 * that assumes that surfaces that are not perfectly smooth are composed of
 * many very tiny facets, each of which is a perfect specular transmitter.
 *
 * These microfacets have normals that are distributed about the normal of the
 * approximating smooth surface. The actual distribution of the microfacet's
 * normal vector is given by an instance of a MicrofacetModel.
 *
 * Note:
 * The current implmentation does not account for multiple scattering between
 * the microfacets resulting in energy loss. The lost energy is responsible for
 * a reduced actual reflectance, even if no fresnel factor is applied.
 *
 * \author
 * Bruno Pera
 */
class MicrofacetBTDF: public BxDF {
private:
	friend class TypedPool<MicrofacetBTDF>;
	const Pointer<const ShadingNode<float>> transmittance;
	const Pointer<const MicrofacetModel> distribution;
	const bool fresnel;

	__host__ __device__
	static const MicrofacetBTDF* cast(const BxDF* bxdf) {
		return static_cast<const MicrofacetBTDF*>(bxdf);
	}

	__host__ __device__
	static bool interacting_(const BxDF* bxdf) {
		return true;
	}

	__host__ __device__
	static float albedo_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode) {
		// TODO: deal with energy loss and fresnel effect
		const float m = (mode == Mode::RADIANCE) ? 1.f : ((ior1.re / ior2.re) * (ior1.re / ior2.re));
		return m * (*cast(bxdf)->transmittance)(g.gp, g.su, g.sn, g.uv, wlen, time);
	}

	__host__ __device__
	static float evaluate_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, RNG* rng) {
		if (sameHemisphere(wo, wi)) return 0.f;
	    if (wi[2] == 0.f || wo[2] == 0.f) return 0.f;
	    const float eta = ior1.re / ior2.re;
	    Vectr3F wh = (wi + wo * eta).normalize();
	    if (!sameHemisphere(wo, wh)) wh = -wh;
		const float f = cast(bxdf)->fresnel ? Fresnel::reflectance(ior1, ior2, absDot(wo, wh)) : 0.f;
		const float m = (mode == Mode::RADIANCE) ? 1.f : (eta * eta);
		const float sqrtDenom = dot(wi, wh) + eta * dot(wo, wh);
	    return (1.f - f) * m * (*cast(bxdf)->transmittance)(g.gp, g.su, g.sn, g.uv, wlen, time) * abs(cast(bxdf)->distribution->DG(g, wlen, time, wo, Vectr3F(wi[0], wi[1], -(wi[2])), wh) * absDot(wi, wh) * absDot(wo, wh) / (wi[2] * wo[2] * sqrtDenom * sqrtDenom));
	}

	__host__ __device__
	static float sample_wi_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng) {
	    const Vectr3F wh = cast(bxdf)->distribution->sampleWh(g, wlen, time, wo, rnd, pdf);
	    const float eta = ior1.re / ior2.re;
		if (!refract(wo, wh, eta, wi)) {
			// total internal reflection
			*pdf = 0.f;
			return 0.f;
		}
		const float woDotWh = dot( wo, wh);
		const float wiDotWh = dot(*wi, wh);
		const float sqrtDenom = wiDotWh + eta * woDotWh;
	    const float dwh_dwi = abs((eta * eta * wiDotWh) / (sqrtDenom * sqrtDenom));
		*pdf *= dwh_dwi;
		if (sameHemisphere(wo, *wi)) return 0.f;
		const float f = cast(bxdf)->fresnel ? Fresnel::reflectance(ior1, ior2, abs(woDotWh)) : 0.f;
		const float m = (mode == Mode::RADIANCE) ? 1.f : (eta * eta);
		return (1.f - f) * m * (*cast(bxdf)->transmittance)(g.gp, g.su, g.sn, g.uv, wlen, time) * abs(cast(bxdf)->distribution->DG(g, wlen, time, wo, Vectr3F((*wi)[0], (*wi)[1], -((*wi)[2])), wh) * wiDotWh * woDotWh / ((*wi)[2] * wo[2] * sqrtDenom * sqrtDenom));
	}

	__host__ __device__
	static float sample_wi_pdf_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, bool sampled, RNG* rng) {
	    const float eta = ior1.re / ior2.re;
	    Vectr3F wh = (wi + wo * eta).normalize();
	    if (!sameHemisphere(wo, wh)) wh = -wh;
	    const float sqrtDenom = dot(wi, wh) + eta * dot(wo, wh);
	    const float dwh_dwi = abs((eta * eta * dot(wi, wh)) / (sqrtDenom * sqrtDenom));
	    return cast(bxdf)->distribution->sampleWhPDF(g, wlen, time, wo, wh)  * dwh_dwi;
	}

	__host__
	MicrofacetBTDF(
		const Pointer<const ShadingNode<float>>& transmittance,
		const Pointer<const MicrofacetModel>& distribution,
		const bool fresnel,
		const interacting_t& interacting_,
		const albedo_t& albedo_,
		const evaluate_t& evaluate_,
		const sample_wi_t& sample_wi_,
		const sample_wi_pdf_t& sample_wi_pdf_
	):
		BxDF(Type(DIFFUSE | REFRACTIVE), interacting_, albedo_, evaluate_, sample_wi_, sample_wi_pdf_),
		transmittance(transmittance),
		distribution(distribution),
		fresnel(fresnel) {
	}

public:
	/*!
	 * \brief
	 * Creates a new MicrofacetBTDF.
	 *
	 * \param[in] transmittance The spectral transmittance at each point on the
	 * surface, must be greater than or equal to 0 and less than or equal to 1
	 * everywhere, otherwise the BxDF's behaviour is undefined.
	 * \param[in] distribution The Microfacet distribution that determines how
	 * much the microfacet normals differ from the macrosurface normal.
	 * \param[in] fresnel If the unpolarized
	 * <a href="https://en.wikipedia.org/wiki/Fresnel_equations">fresnel</a>
	 * factor should be used to modulate the amount of transmitted energy.
	 */
	__host__
	static Pointer<MicrofacetBTDF> build(
		MemoryPool& pool,
		const Pointer<const ShadingNode<float>>& transmittance,
		const Pointer<const MicrofacetModel>& distribution,
		const bool fresnel
	) {
		return Pointer<MicrofacetBTDF>::make(
			pool, transmittance, distribution, fresnel,
			interacting_t(virtualize(interacting_)),
			albedo_t(virtualize(albedo_)),
			evaluate_t(virtualize(evaluate_)),
			sample_wi_t(virtualize(sample_wi_)),
			sample_wi_pdf_t(virtualize(sample_wi_pdf_))
		);
	}
};

}

#endif /* MICROFACETBTDF_CUH_ */

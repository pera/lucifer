#ifndef SUBSTRATEBRDF_CUH_
#define SUBSTRATEBRDF_CUH_

#include <cassert>
#include "BxDF.cuh"
#include "Fresnel.cuh"
#include "microfacet/MicrofacetModel.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * Diffuse substrate under a glossy specular layer.
 *
 * A BRDF that smoothly interpolates between a prodominantly diffuse reflection
 * at near normal angles and a prodominantly glossy specular reflection at
 * grazing angles modulated according to the fresnel factor based on the
 * refractive indexes of the inside and outside media.
 *
 * \author
 * Bruno Pera
 */
class SubstrateBRDF: public BxDF {
private:
	friend class TypedPool<SubstrateBRDF>;
	const Pointer<const ShadingNode<float>> diffuse;
	const Pointer<const ShadingNode<float>> specular;
	const Pointer<const MicrofacetModel> distribution;

	__host__ __device__
	float evaluateDiffuse(const Vectr3F& wi, const Vectr3F& wo, const float d) const {
		return sameHemisphere(wi, wo) ? d / PI<float>() : 0.f;
	}

	__host__ __device__
	float sampleWiDiffuse(const float rnd[2], const Vectr3F& wo, const float d, Vectr3F* wi, float* pdf) const {
		CosineHemisphereDistribution::sample(rnd, &((*wi)[0]), pdf);
		if (wo[2] < 0.f) (*wi)[2] = -((*wi)[2]);
		return d / PI<float>();
	}

	__host__ __device__
	float sampleWiPdfDiffuse(const Vectr3F& wo, const Vectr3F& wi) const {
		if (wo[2] * wi[2] <= 0.f) return 0.f;
		const Vectr3F w(wi[0], wi[1], abs(wi[2]));
		return CosineHemisphereDistribution::pdf(&(w[0]));
	}

	__host__ __device__
	float evaluateSpecular(const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const Vectr3F& wh, const float s) const {
	    if (wo[2] * wi[2] <= 0.f) return 0.f;
	    return s * distribution->DG(g, wlen, time, wo, wi, wh) / abs(4.f * wo[2] * wi[2]);
	}

	__host__ __device__
	float sampleWiSpecular(const float rnd[2], const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const float s, Vectr3F* wi, float* pdf) const {
	    const Vectr3F wh = distribution->sampleWh(g, wlen, time, wo, rnd, pdf);
	    *wi = reflect(wo, wh);
	    *pdf /= (4.f * absDot(wo, wh));
	    if (wo[2] * (*wi)[2] <= 0.f) return 0.f;
	    return s * distribution->DG(g, wlen, time, wo, *wi, wh) / abs(4.f * wo[2] * (*wi)[2]);
	}

	__host__ __device__
	float sampleWiPdfSpecular(const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const Vectr3F& wh) const {
		if (wo[2] * wh[2] <= 0.f) return 0.f;
		return distribution->sampleWhPDF(g, wlen, time, wo, wh) / (4.f * absDot(wo, wh));
	}

	__host__ __device__
	static const SubstrateBRDF* cast(const BxDF* bxdf) {
		return static_cast<const SubstrateBRDF*>(bxdf);
	}

	__host__ __device__
	static bool interacting_(const BxDF* bxdf) {
		return true;
	}

	__host__ __device__
	static float albedo_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode) {
		// TODO: deal with energy loss and fresnel effect
		return max((*cast(bxdf)->diffuse)(g.gp, g.su, g.sn, g.uv, wlen, time), (*cast(bxdf)->specular)(g.gp, g.su, g.sn, g.uv, wlen, time));
	}

	__host__ __device__
	static float evaluate_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, RNG* rng) {
		if (!sameHemisphere(wi, wo)) return 0.f;
		Vectr3F wh = wo + wi;
		const float lengthE2 = wh.lengthSquared();
		if (lengthE2 == 0.f) return 0.f;
		wh /= sqrt(lengthE2);
		const float f = Fresnel::reflectance(ior1, ior2, absDot(wo, wh));
		assert(f >= 0.f and f <= 1.f);
		const float s = cast(bxdf)->evaluateSpecular(g, wlen, time, wi, wo, wh, (*cast(bxdf)->specular)(g.gp, g.su, g.sn, g.uv, wlen, time));
		const float d = cast(bxdf)->evaluateDiffuse(wi, wo, (*cast(bxdf)->diffuse)(g.gp, g.su, g.sn, g.uv, wlen, time));
		return f * s + (1.f - f) * d;
	}

	__host__ __device__
	static float sample_wi_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng) {
		float dVal, dPdf, sVal, sPdf;
		const float s = (*cast(bxdf)->specular)(g.gp, g.su, g.sn, g.uv, wlen, time);
		const float d = (*cast(bxdf)->diffuse )(g.gp, g.su, g.sn, g.uv, wlen, time);
		if (s == 0.f and d == 0.f)  {
			*pdf = 0.f;
	    	return 0.f;
	    }
		Vectr3F wh;
		const float t = s / (s + d);
		if (rnd[0] <= t) {
			// sample specular
			rnd[0] /= t;
			sVal = cast(bxdf)->sampleWiSpecular(rnd, g, wlen, time, wo, s, wi, &sPdf);
			wh = wo + *wi;
			const float lengthE2 = wh.lengthSquared();
			if (lengthE2 == 0.f) return 0.f;
			wh /= sqrt(lengthE2);
			if (wo[2] * wh[2] < 0.f) wh = -wh;
			dVal = cast(bxdf)->evaluateDiffuse(*wi, wo, d);
			dPdf = cast(bxdf)->sampleWiPdfDiffuse(wo, *wi);
		} else {
			// sample diffuse
			rnd[0] = (rnd[0] - t) / (1 - t);
			dVal = cast(bxdf)->sampleWiDiffuse(rnd, wo, d, wi, &dPdf);
			wh = wo + *wi;
			const float lengthE2 = wh.lengthSquared();
			if (lengthE2 == 0.f) return 0.f;
			wh /= sqrt(lengthE2);
			if (wo[2] * wh[2] < 0.f) wh = -wh;
			sVal = cast(bxdf)->evaluateSpecular(g, wlen, time, *wi, wo, wh, s);
			sPdf = cast(bxdf)->sampleWiPdfSpecular(g, wlen, time, wo, *wi, wh);
		}
		*pdf = (s * sPdf + d * dPdf) / (s + d);
		const float f = Fresnel::reflectance(ior1, ior2, absDot(wo, wh));
		assert(f >= 0.f and f <= 1.f);
		return f * sVal + (1.f - f) * dVal;
	}

	__host__ __device__
	static float sample_wi_pdf_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, bool sampled, RNG* rng) {
		Vectr3F wh = wo + wi;
		const float lengthE2 = wh.lengthSquared();
		if (lengthE2 == 0.f) return 0.f;
		wh /= sqrt(lengthE2);
		if (wo[2] * wh[2] < 0.f) wh = -wh;
		const float s = (*cast(bxdf)->specular)(g.gp, g.su, g.sn, g.uv, wlen, time);
		const float d = (*cast(bxdf)->diffuse )(g.gp, g.su, g.sn, g.uv, wlen, time);
		if (s == 0.f and d == 0.f) return 0.f;
		const float sPdf = cast(bxdf)->sampleWiPdfSpecular(g, wlen, time, wo, wi, wh);
		const float dPdf = cast(bxdf)->sampleWiPdfDiffuse(wo, wi);
		return (s * sPdf + d * dPdf) / (s + d);
	}

	__host__
	SubstrateBRDF(
		const Pointer<const ShadingNode<float>>& diffuse,
		const Pointer<const ShadingNode<float>>& specular,
		const Pointer<const MicrofacetModel>& distribution,
		const interacting_t& interacting_,
		const albedo_t& albedo_,
		const evaluate_t& evaluate_,
		const sample_wi_t& sample_wi_,
		const sample_wi_pdf_t& sample_wi_pdf_
	):
		BxDF(Type(DIFFUSE | REFLECTIVE), interacting_, albedo_, evaluate_, sample_wi_, sample_wi_pdf_),
		diffuse(diffuse),
		specular(specular),
		distribution(distribution) {
	}

public:
	/*!
	 * \brief
	 * Creates a new SubstrateBRDF.
	 *
	 * \param[in] diffuse The diffuse spectral reflectance at each point on the
	 * surface, must be greater than or equal to 0 and less than or equal to 1
	 * everywhere, otherwise the BxDF's behaviour is undefined.
	 * \param[in] specular The specular spectral reflectance at each point on
	 * the surface, must be greater than or equal to 0 and less than or equal
	 * to 1 everywhere, otherwise the BxDF's behaviour is undefined.
	 * \param[in] distribution The Microfacet distribution used to control the
	 * glossy-specular reflection term.
	 */
	/*!
	 * \brief
	 * Creates a new LambertianBRDF.
	 *
	 * \param[in] reflectance The spectral reflectance at each point on the
	 * surface, must be greater than or equal to 0 and less than or equal to 1
	 *  everywhere, otherwise the BxDF's behaviour is undefined.
	 */
	__host__
	static Pointer<SubstrateBRDF> build(
		MemoryPool& pool,
		const Pointer<const ShadingNode<float>>& diffuse,
		const Pointer<const ShadingNode<float>>& specular,
		const Pointer<const MicrofacetModel>& distribution
	) {
		return Pointer<SubstrateBRDF>::make(
			pool, diffuse, specular, distribution,
			interacting_t(virtualize(interacting_)),
			albedo_t(virtualize(albedo_)),
			evaluate_t(virtualize(evaluate_)),
			sample_wi_t(virtualize(sample_wi_)),
			sample_wi_pdf_t(virtualize(sample_wi_pdf_))
		);
	}
};

}

#endif /* SUBSTRATEBRDF_CUH_ */

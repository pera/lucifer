#ifndef CONDUCTORMICROSURFACEBRDF_CUH_
#define CONDUCTORMICROSURFACEBRDF_CUH_

#include "UniformHeightDistribution.cuh"
#include "GGXSlopeDistribution.cuh"
#include "ConductorMicrosurface.cuh"
#include "lucifer/bsdf/BxDF.cuh"
#include "lucifer/math/Util.cuh"
#include "lucifer/node/shading/ShadingNode.cuh"
#include "lucifer/random/CPUBasicRNG.cuh"
#include "lucifer/random/CosineHemisphereDistribution.cuh"

namespace lucifer {

/*!
 * \brief
 * BRDF that models a microsurface composed of perfect conductor microfacets.
 */
class ConductorMicrosurfaceBRDF: public BxDF {
private:
	const Pointer<const ConductorMicrosurface> surface;
	const Pointer<const ShadingNode<float>> reflectance;
	const Pointer<const ShadingNode<float>> roughnessX;
	const Pointer<const ShadingNode<float>> roughnessY;

	__host__ __device__
	static const ConductorMicrosurfaceBRDF* cast(const BxDF* bxdf) {
		return static_cast<const ConductorMicrosurfaceBRDF*>(bxdf);
	}

	__host__ __device__
	static auto parameterize(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const ComplexF& ior1, const ComplexF& ior2, RNG* rng) {
		return Microsurface::Parameter {
			rng,
			clamp((*cast(bxdf)->roughnessX )(g.gp, g.su, g.sn, g.uv, wlen, time), 1E-4f, 1.f),
			clamp((*cast(bxdf)->roughnessY )(g.gp, g.su, g.sn, g.uv, wlen, time), 1E-4f, 1.f),
			clamp((*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time),   0.f, 1.f),
			0.f, ior1, ior2
		};
	}

	__host__ __device__
	static bool interacting_(const BxDF* bxdf) {
		return true;
	}

	__host__ __device__
	static float albedo_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode) {
		return (*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time);
	}

	__host__ __device__
	static float evaluate_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, RNG* rng1D) {
		float u;
		rng1D->next(&u);
		auto param = parameterize(bxdf, g, wlen, time, ior1, ior2, rng1D);
		if (u < 0.5f) {
			auto value = (wo[2] < 0.f) ?
				cast(bxdf)->surface->evaluateMS(-wo, -wi, param):
				cast(bxdf)->surface->evaluateMS( wo,  wi, param);
			return  value / abs(wi[2]);
		} else {
			auto value = (wi[2] < 0.f) ?
				cast(bxdf)->surface->evaluateMS(-wi, -wo, param) / absCosTheta(wo) * absCosTheta(wi):
				cast(bxdf)->surface->evaluateMS( wi,  wo, param) / absCosTheta(wo) * absCosTheta(wi);
			return  value / abs(wi[2]);
		}
	}

	__host__ __device__
	static float sample_wi_pdf_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, bool sampled, RNG* rng1D) {
		auto param = parameterize(bxdf, g, wlen, time, ior1, ior2, rng1D);
		return (wo[2] < 0.f) ?
			cast(bxdf)->surface->MISPDF(-wo, -wi, param):
			cast(bxdf)->surface->MISPDF( wo,  wi, param);
	}

	__host__ __device__
	static float sample_wi_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng1D) {
		float weight;
		auto param = parameterize(bxdf, g, wlen, time, ior1, ior2, rng1D);
		*wi = (wo[2] < 0.f) ?
			-cast(bxdf)->surface->sample(-wo, weight, param):
			+cast(bxdf)->surface->sample(+wo, weight, param);
		*pdf = 1.f;
		return weight / abs((*wi)[2]);
	}

	__host__
	ConductorMicrosurfaceBRDF(
		const Pointer<const ConductorMicrosurface>& surface,
		const Pointer<const ShadingNode<float>>& reflectance,
		const Pointer<const ShadingNode<float>>& roughnessX,
		const Pointer<const ShadingNode<float>>& roughnessY,
		const interacting_t& interacting_,
		const albedo_t& albedo_,
		const evaluate_t& evaluate_,
		const sample_wi_t& sample_wi_,
		const sample_wi_pdf_t& sample_wi_pdf_
	):
		BxDF(Type(DIFFUSE | REFLECTIVE), interacting_, albedo_, evaluate_, sample_wi_, sample_wi_pdf_),
		surface(surface),
		reflectance(reflectance),
		roughnessX(roughnessX),
		roughnessY(roughnessY) {
	}

public:
	friend class TypedPool<ConductorMicrosurfaceBRDF>;

	__host__
	static Pointer<ConductorMicrosurfaceBRDF> build(
		MemoryPool& pool,
		const Pointer<const ShadingNode<float>>& reflectance,
		const Pointer<const ShadingNode<float>>& roughnessX,
		const Pointer<const ShadingNode<float>>& roughnessY,
		const bool fresnel,
		const float uniformPDF,
		const unsigned int maxScatteringOrder)
	{
		auto height = UniformHeightDistribution::build(pool);
		auto slope = GGXSlopeDistribution::build(pool);
		auto surface = ConductorMicrosurface::build(pool, height, slope, fresnel, uniformPDF, maxScatteringOrder);

		return Pointer<ConductorMicrosurfaceBRDF>::make(
			pool, surface, reflectance,
			roughnessX, roughnessY,
			interacting_t(virtualize(interacting_)),
			albedo_t(virtualize(albedo_)),
			evaluate_t(virtualize(evaluate_)),
			sample_wi_t(virtualize(sample_wi_)),
			sample_wi_pdf_t(virtualize(sample_wi_pdf_))
		);
	}
};

}

#endif /* SPECULARMICROSURFACEBRDF_CUH_ */

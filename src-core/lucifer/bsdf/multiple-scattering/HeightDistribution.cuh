#ifndef HEIGHTDISTRIBUTION_CUH_
#define HEIGHTDISTRIBUTION_CUH_

#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/memory/Virtual.cuh"

namespace lucifer {

/*!
 * \brief
 * Microsurface's distribution of heights.
 *
 * Based on: Multiple-Scattering Microfacet BSDFs with the Smith Model.
 * Eric Heitz, Johannes Hanika, Eugene d'Eon, Carsten Dachbacher
 */
class HeightDistribution: public Polymorphic {
public:
	using pdf_f = Virtual<			// pdf function type
		float, 						// return type
		const HeightDistribution*,	// this object
		const float					// the height parameter
	>;

	using cdf_f = Virtual<			// cdf function type
		float, 						// return type
		const HeightDistribution*,	// this object
		const float					// the height parameter
	>;

	using qnt_f = Virtual<			// quantile function type
		float, 						// return type
		const HeightDistribution*,	// this object
		float						// the cumulative probability parameter
	>;

private:
	const pdf_f pdf_;
	const cdf_f cdf_;
	const qnt_f qnt_;

protected:
	__host__
	HeightDistribution(const pdf_f& pdf_, const cdf_f& cdf_, const qnt_f& qnt_):
	pdf_(pdf_), cdf_(cdf_), qnt_(qnt_) {
	}

public:
	/**
	 * \brief
	 * The probability density of the given height.
	 *
	 * \param h The height.
	 * \return the PDF at h.
	 */
	__host__ __device__
	float pdf(const float h) const {
		return pdf_(this, h);
	}

	/**
	 * \brief
	 * The cumulative probability at the given height.
	 *
	 * \param h The height.
	 * \return the CDF at h.
	 */
	__host__ __device__
	float cdf(const float h) const {
		return cdf_(this, h);
	}

	/*
	 * \brief
	 * The quantile at the given cumulative probability.
	 *
	 * \param p The cumulative probability.
	 * \return The quantile at p.
	 */
	__host__ __device__
	float qnt(const float p) const {
		return qnt_(this, p);
	}
};

}

#endif /* HEIGHTDISTRIBUTION_CUH_ */

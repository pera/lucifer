#ifndef DIFFUSEMICROSURFACE_CUH_
#define DIFFUSEMICROSURFACE_CUH_

#include "Microsurface.cuh"

namespace lucifer {

/**
 * \brief
 * Microsurface composed of perfectly diffuse microfacets.
 *
 * Based on: Multiple-Scattering Microfacet BSDFs with the Smith Model.
 * Eric Heitz, Johannes Hanika, Eugene d'Eon, Carsten Dachbacher
 */
class DiffuseMicrosurface: public Microsurface {
private:
	const unsigned int maxScatteringOrder;

	__host__ __device__
	static void buildOrthonormalBasis(Vectr3F& w1, Vectr3F& w2, const Vectr3F& w3) {
		if (w3[2] < -0.9999999f) {
			w1 = Vectr3F{  0.f, -1.f, 0.f };
			w2 = Vectr3F{ -1.f,  0.f, 0.f };
		} else {
			const float a = 1.f / (1.f + w3[2]);
			const float b = -w3[0] * w3[1] * a;
			w1 = Vectr3F{ 1.f - w3[0] * w3[0] * a , b, -w3[0] };
			w2 = Vectr3F{ b, 1.f - w3[1] * w3[1] * a , -w3[1] };
		}
	}

	__host__ __device__
	float evaluatePhaseFunction(const Vectr3F& wi, const Vectr3F& wo, Parameter& param) const {
		float u1;
		float u2;
		param.rng->next(&u1);
		param.rng->next(&u2);
		Vectr3F wm = this->slope->sampleD_wi(wi, param.alphaX, param.alphaY, u1, u2);
		return param.reflect * INV_PI<float>() * max(0.f, dot(wo, wm));
	}

	__host__ __device__
	float samplePhaseFunction(const Vectr3F& wi, Vectr3F& wo, Parameter& param) const {
		// sample visible normal
		float u1;
		float u2;
		param.rng->next(&u1);
		param.rng->next(&u2);
		wo = this->slope->sampleD_wi(wi, param.alphaX, param.alphaY, u1, u2);

		// sample diffuse reflection
		Vectr3F w1, w2;
		param.rng->next(&u1);
		param.rng->next(&u2);
		buildOrthonormalBasis(w1, w2, wo);
		const float r1 = 2.f * u1 - 1.f;
		const float r2 = 2.f * u2 - 1.f;

		// concentric map code from
		// http://psgraphics.blogspot.ch/2011/01/improved-code-for-concentric-map.html
		float phi, r;
		if (r1 == 0.f && r2 == 0.f) {
			r = phi = 0.f;
		} else if (r1 * r1 > r2 * r2) {
			r = r1;
			phi = (PI<float>() / 4.f) * (r2 / r1);
		} else {
			r = r2;
			phi = (PI<float>() / 2.f) - (r1 / r2) * (PI<float>() / 4.f);
		}
		const float x = r * cosf(phi);
		const float y = r * sinf(phi);
		const float z = sqrt(max(0.f, 1.f - x * x  - y * y));
		wo = x * w1 + y * w2 + z * wo;
		// return
		return param.reflect;
	}

	__host__ __device__
	DiffuseMicrosurface(
		const Pointer<const HeightDistribution>& height,
		const Pointer<const SlopeDistribution>& slope,
		const unsigned int maxScatteringOrder
	):
		Microsurface(height, slope), maxScatteringOrder(maxScatteringOrder) {

	}

public:

public:
	friend class TypedPool<DiffuseMicrosurface>;

	__host__
	static Pointer<DiffuseMicrosurface> build(
		MemoryPool& pool,
		const Pointer<const HeightDistribution>& height,
		const Pointer<const SlopeDistribution>& slope,
		const unsigned int maxScatteringOrder)
	{
		return Pointer<DiffuseMicrosurface>::make(pool,  height, slope, maxScatteringOrder);
	}

	/*
	 * Evaluate single scatter BSDF.
	 */
	__host__ __device__
	float evaluateSS(const Vectr3F& wi, const Vectr3F& wo, Parameter& param) const {
		// sample visible normal
		float u1;
		float u2;
		param.rng->next(&u1);
		param.rng->next(&u2);
		const Vectr3F wm = this->slope->sampleD_wi(wi, param.alphaX, param.alphaY, u1, u2);
		// shadowing given masking
		const float lambdaI = this->slope->lambda(wi, param.alphaX, param.alphaY);
		const float lambdaO = this->slope->lambda(wo, param.alphaX, param.alphaY);
		const float g2GivenG1 = (1.f + lambdaI) / (1.f + lambdaI + lambdaO);
		// evaluate diffuse and shadowing given masking
		return INV_PI<float>() * param.reflect * max(0.f, dot(wm, wo)) * g2GivenG1;
	}

	/*
	 * Evaluate multiple scatter BSDF.
	 */
	__host__ __device__
	float evaluateMS(const Vectr3F& wi, const Vectr3F& wo, Parameter& param) const {
		// init
		float l = 0.f;
		float e = 1.f;
		Vectr3F wr = -wi;
		float hr = 1.f + this->height->qnt(0.999f);
		// random walk
		int currentScatteringOrder = 0;
		while(true) {
			// next height
			float u;
			param.rng->next(&u);
			hr = this->sampleHeight( wr,  hr, u, param);
			// leave the microsurface?
			if (isinf(hr)) break;
			currentScatteringOrder++;
			// next event estimation
			float phasefunction = evaluatePhaseFunction(-wr, wo, param);
			float shadowing = this->g1(wo, hr, param);
			float I = phasefunction * shadowing;
			if (!isinf(I)) l += e * I;
			if (maxScatteringOrder > 0 and currentScatteringOrder >= maxScatteringOrder) break;
			// next direction
			e *= samplePhaseFunction(-wr, wr, param);
			// if NaN (should not happen, just in case)
			if (isnan(hr) || isnan(wr[2])) return 0.f;
		}
		return l;
	}

	/*
	 * Sample BSDF.
	 */
	__host__ __device__
	float sample(const Vectr3F& wi, Vectr3F& wo, Parameter& param) const {
		// init
		wo = -wi;
		float hr = 1.f + this->height->qnt(0.999f);
		// random walk
		float e = 1.f;
		int currentScatteringOrder = 0;
		while(true) {
			// next height
			float u;
			param.rng->next(&u);
			hr = this->sampleHeight( wo,  hr, u, param);
			// leave the microsurface?
			if (isinf(hr)) break;
			if (maxScatteringOrder > 0 and currentScatteringOrder >= maxScatteringOrder) {
				e = 0.f;
				break;
			}
			currentScatteringOrder++;
			// next direction
			e *= samplePhaseFunction(-wo, wo, param);
			if (isnan(hr) || isnan(wo[2])) {
				wo = Vectr3F{ 0.f, 0.f ,1.f };
				break;
			}
		}
		return e;
	}

	/**
	 * Multiple importance sampling PDF.
	 */
	__host__ __device__
	float MISPDF(const Vectr3F& wi, const Vectr3F& wo, Parameter& param) const {
		if (!sameHemisphere(wi, wo)) return 0.f;
		Vectr3F w(wo[0], wo[1], abs(wo[2]));
		return CosineHemisphereDistribution::pdf(&(w[0]));
	}

};

}
#endif /* DIFFUSEMICROSURFACE_CUH_ */

#ifndef UNIFORMHEIGHTDISTRIBUTION_CUH_
#define UNIFORMHEIGHTDISTRIBUTION_CUH_

#include "HeightDistribution.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer  {

/*!
 * \brief
 * Uniform distribution of heights in [-1, +1].
 *
 * Based on: Multiple-Scattering Microfacet BSDFs with the Smith Model.
 * Eric Heitz, Johannes Hanika, Eugene d'Eon, Carsten Dachbacher
 */
class UniformHeightDistribution: public HeightDistribution {
private:
	__host__ __device__ static
	float pdf_(const HeightDistribution*, const float h) {
		return (h < -1.f or h > +1.f) ? .0f : .5f;
	}

	__host__ __device__ static
	float cdf_(const HeightDistribution* self, const float h) {
		return min(1.f, max(0.f, .5f * (h + 1.f)));
	}

	__host__ __device__ static
	float qnt_(const HeightDistribution*, const float p) {
		return max(-1.f, min(+1.f, 2.f * p - 1.f));
	}

	__host__
	UniformHeightDistribution(const pdf_f& pdf, const cdf_f& cdf, const qnt_f& qnt):
	HeightDistribution(pdf, cdf, qnt){
	}

public:
	friend class TypedPool<UniformHeightDistribution>;

	__host__ static
	Pointer<UniformHeightDistribution> build(MemoryPool& pool) {
		return Pointer<UniformHeightDistribution>::make(
			pool,
			pdf_f(virtualize(pdf_)),
			cdf_f(virtualize(cdf_)),
			qnt_f(virtualize(qnt_))
		);
	}
};

}

#endif /* UNIFORMHEIGHTDISTRIBUTION_CUH_ */

#ifndef SLOPEDISTRIBUTION_CUH_
#define SLOPEDISTRIBUTION_CUH_

#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/memory/Polymorphic.cuh"
#include "lucifer/memory/Virtual.cuh"

namespace lucifer {

/*!
 * \brief
 * Microfacet's distribution of slopes.
 *
 * Based on: Multiple-Scattering Microfacet BSDFs with the Smith Model.
 * Eric Heitz, Johannes Hanika, Eugene d'Eon, Carsten Dachbacher
 */
class SlopeDistribution: public Polymorphic {
public:
	using p22_f = Virtual<			// slope pdf function type
		float, 						// return type
		const SlopeDistribution*,	// this object
		const float,				// slope x parameter
		const float,				// slope y parameter
		const float,				// roughness x parameter
		const float					// roughness x parameter
	>;

	using lambda_f = Virtual <		// smith's lambda function type
		float,						// return type
		const SlopeDistribution*,	// this object
		const Vectr3F&,				// direction parameter
		const float,				// roughness x parameter
		const float					// roughness x parameter
	>;

	using proj_area_f = Virtual<	// projected area function type
		float,						// return type
		const SlopeDistribution*,	// this object
		const Vectr3F&,				// incident direction parameter
		const float,				// roughness x parameter
		const float					// roughness x parameter
	>;

	using smp_p22_f = Virtual<		// sample p22 function type
		Vectr2F,					// return type
		const SlopeDistribution*,	// this object
		const float, 				// incident theta
		const float,				// random number in [0, 1]
		const float					// random number in [0, 1]
	>;

protected:
	const p22_f p22_;
	const lambda_f lambda_;
	const proj_area_f proj_area_;
	const smp_p22_f smp_p22_;

	__host__
	SlopeDistribution(
		const p22_f& p22_,
		const lambda_f& lambda_,
		const proj_area_f& proj_area_,
		const smp_p22_f& smp_p22_
	):
		p22_(p22_),
		lambda_(lambda_),
		proj_area_(proj_area_),
		smp_p22_(smp_p22_) {
	}

public:
	/**
	 * \brief
	 * The distribution of slopes.
	 *
	 * \param slopeX The slope along the x direction.
	 * \param slopeY The slope along the y direction.
	 * \return The PDF at (x, y).
	 */
	__host__ __device__
	float p22(const float slopeX, const float slopeY, const float alphaX, const float alphaY) const {
		return p22_(this, slopeX, slopeY, alphaX, alphaY);
	};

	/**
	 * \brief
	 * The Smith's lambda function.
	 *
	 * \param wi The incident direction.
	 * \param alphaX The roughness in the x direction.
	 * \param alphaY The roughness in the y direction.
	 *
	 * \return The Smith's lambda function at wi;
	 */
	__host__ __device__
	float lambda(const Vectr3F& wi, const float alphaX, const float alphaY) const {
		return lambda_(this, wi, alphaX, alphaY);
	}

	/**
	 * \brief
	 * The projected area towards the given incident direction.
	 *
	 * \param wi The incident direction.
	 * \param alphaX The roughness in the x direction.
	 * \param alphaY The roughness in the y direction.
	 *
	 * \return The projected area towards wi.
	 */
	__host__ __device__
	float projectedArea(const Vectr3F& wi, const float alphaX, const float alphaY) const {
		return proj_area_(this, wi, alphaX, alphaY);
	}

	/**
	 * \brief
	 * Sample the distribution of visible slopes with alpha = 1.
	 *
	 * \param thetaWi Incident theta.
	 * \param u1 Random number in [0, 1].
	 * \param u2 Random number in [0, 1].
	 * \return The sampled slopes in x and y direction.
	 */
	__host__ __device__
	Vectr2F sampleP22(const float thetaWi, const float u1, const float u2) const {
		return smp_p22_(this, thetaWi, u1, u2);
	}

	/**
	 * \brief
	 * Roughness projected towards the given incident direction.
	 *
	 * \param wi The incident direction.
	 * \param alphaX The roughness in the x direction.
	 * \param alphaY The roughness in the y direction.
	 *
	 * \return The roughness projected towards wi.
	 */
	__host__ __device__
	float alpha(const Vectr3F& wi, const float alphaX, const float alphaY) const {
		const float invSinTheta2 = 1.f / (1.f - wi[2] * wi[2]);
		const float cosPhi2 = wi[0]*wi[0]*invSinTheta2;
		const float sinPhi2 = wi[1]*wi[1]*invSinTheta2;
		const float alpha_i = sqrt(cosPhi2 * alphaX * alphaX + sinPhi2 * alphaY * alphaY);
		return alpha_i;
	}

	/**
	 * \brief
	 * The distribution of normals (NDF).
	 *
	 * \param wm The normal vector.
	 * \param alphaX The roughness in the x direction.
	 * \param alphaY The roughness in the y direction.
	 *
	 * \return The PDF at wm.
	 */
	__host__ __device__
	float D(const Vectr3F& wm, const float alphaX, const float alphaY) const {
		if( wm[2] <= 0.f) return 0.f;
		// slope of wm
		const float slope_x = -wm[0] / wm[2];
		const float slope_y = -wm[1] / wm[2];
		// value
		const float value = p22(slope_x, slope_y, alphaX, alphaY) / (wm[2] * wm[2] * wm[2] * wm[2]);
		return value;
	}

	/**
	 * \brief
	 * The distribution of visible normals (VNDF).
	 *
	 * \param wi The incident direction.
	 * \param wm The normal vector.
	 * \param alphaX The roughness in the x direction.
	 * \param alphaY The roughness in the y direction.
	 *
	 * \return The PDF at wm.
	 */
	__host__ __device__
	float D_wi(const Vectr3F& wi, const Vectr3F& wm, const float alphaX, const float alphaY) const {
		if( wm[2] <= 0.f) return 0.f;
		// normalization coefficient
		const float projectedarea = projectedArea(wi, alphaX, alphaY);
		if (projectedarea == 0.f) return 0.f;
		const float c = 1.f / projectedarea;
		// value
		const float value = c * max(0.f, dot(wi, wm)) * D(wm, alphaX, alphaY);
		return value;
	}

	/**
	 * \brief
	 * Samples the distribution of visible normals.
	 *
	 * \param wi The incident direction.
	 * \param alphaX The roughness in the x direction.
	 * \param alphaY The roughness in the y direction.*
	 * \param u1 Random number in [0, 1].
	 * \param u2 Random number in [0, 1].
	 *
	 * \return The sampled normal vector.
	 */
	__host__ __device__
	Vectr3F sampleD_wi(const Vectr3F& wi, const float alphaX, const float alphaY, const float U1, const float U2) const {
		// stretch to match configuration with alpha=1.0
		const Vectr3F wi_11 = Vectr3F{alphaX * wi[0], alphaY * wi[1], wi[2]}.normalize();
		// sample visible slope with alpha=1.0
		Vectr2F slope_11 = sampleP22(acosf(wi_11[2]), U1, U2);
		// align with view direction
		const float phi = atan2(wi_11[1], wi_11[0]);
		Vectr2F slope{cos(phi) * slope_11[0] - sin(phi) * slope_11[1], sin(phi) * slope_11[0] + cos(phi) * slope_11[1]};
		// stretch back
		slope[0] *= alphaX;
		slope[1] *= alphaY;
		// if numerical instability
		if (isnan(slope) || isinf(slope)) {
			if (wi[2] > 0) return Vectr3F{0.f, 0.f, 1.f};
			else return Vectr3F{wi[0], wi[0], 0.f}.normalize();
		}
		// compute normal
		const Vectr3F wm = Vectr3F{-slope[0], -slope[1], 1.0f}.normalize();
		return wm;
	}
};

}

#endif /* SLOPEDISTRIBUTION_CUH_ */

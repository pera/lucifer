#ifndef DIELECTRICMICROSURFACEBSDF_CUH_
#define DIELECTRICMICROSURFACEBSDF_CUH_

#include "DielectricMicrosurface.cuh"
#include "UniformHeightDistribution.cuh"
#include "GGXSlopeDistribution.cuh"
#include "lucifer/bsdf/BxDF.cuh"
#include "lucifer/math/Util.cuh"
#include "lucifer/random/CPUBasicRNG.cuh"
#include "lucifer/random/CosineHemisphereDistribution.cuh"

namespace lucifer {

/*!
 * \brief
 * BSDF that models a microsurface composed of perfect dielectric microfacets.
 */
class DielectricMicrosurfaceBSDF: public BxDF {
private:
	const Pointer<const DielectricMicrosurface> surface;
	const Pointer<const ShadingNode<float>> reflectance;
	const Pointer<const ShadingNode<float>> transmittance;
	const Pointer<const ShadingNode<float>> roughnessX;
	const Pointer<const ShadingNode<float>> roughnessY;

	__host__ __device__
	static const DielectricMicrosurfaceBSDF* cast(const BxDF* bxdf) {
		return static_cast<const DielectricMicrosurfaceBSDF*>(bxdf);
	}

	__host__ __device__
	static auto parameterize(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const ComplexF& ior1, const ComplexF& ior2, RNG* rng) {
		return Microsurface::Parameter {
			rng,
			(*cast(bxdf)->roughnessX)(g.gp, g.su, g.sn, g.uv, wlen, time),
			(*cast(bxdf)->roughnessY)(g.gp, g.su, g.sn, g.uv, wlen, time),
			(*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time),
			(*cast(bxdf)->transmittance)(g.gp, g.su, g.sn, g.uv, wlen, time),
			ior1, ior2
		};
	}

	__host__ __device__
	static bool interacting_(const BxDF* bxdf) {
		return true;
	}

	__host__ __device__
	static float albedo_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode) {
		// TODO: fresnel
		return max((*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time), (*cast(bxdf)->transmittance)(g.gp, g.su, g.sn, g.uv, wlen, time));
	}

	__host__ __device__
	static float evaluate_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, RNG* rng) {
		float u;
		rng->next(&u);

		if (u < 0.5f) {
			auto param = parameterize(bxdf, g, wlen, time, ior1, ior2, rng);
			auto value = (wo[2] < 0.f) ?
				cast(bxdf)->surface->evaluateMS(-wo, -wi, param):
				cast(bxdf)->surface->evaluateMS( wo,  wi, param);
			return  value / abs(wi[2]);
		} else {
			ComplexF eta1, eta2;
			if (wo[2] * wi[2] >= 0.f) {
				eta1 = ior1;
				eta2 = ior2;
			} else {
				eta1 = ior2;
				eta2 = ior1;
			}
			auto param = parameterize(bxdf, g, wlen, time, eta1, eta2, rng);
			auto value = (wi[2] < 0.f) ?
				cast(bxdf)->surface->evaluateMS(-wi, -wo, param) / absCosTheta(wo) * absCosTheta(wi):
				cast(bxdf)->surface->evaluateMS( wi,  wo, param) / absCosTheta(wo) * absCosTheta(wi);
			return  value / abs(wi[2]);
		}
	}

	__host__ __device__
	static float sample_wi_pdf_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, bool sampled, RNG* rng) {
		auto param = parameterize(bxdf, g, wlen, time, ior1, ior2, rng);
		return (wo[2] < 0.f) ?
			cast(bxdf)->surface->MISPDF(-wo, -wi, param):
			cast(bxdf)->surface->MISPDF( wo,  wi, param);
	}

	__host__ __device__
	static float sample_wi_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng) {
		float weight;
		auto param = parameterize(bxdf, g, wlen, time, ior1, ior2, rng);
		*wi = (wo[2] < 0.f) ?
			-cast(bxdf)->surface->sample(-wo, weight, param):
			+cast(bxdf)->surface->sample(+wo, weight, param);
		*pdf = 1.f;
		return weight / abs((*wi)[2]);
	}

	__host__
	DielectricMicrosurfaceBSDF(
		const Pointer<const DielectricMicrosurface>& surface,
		const Pointer<const ShadingNode<float>>& reflectance,
		const Pointer<const ShadingNode<float>>& transmittance,
		const Pointer<const ShadingNode<float>>& roughnessX,
		const Pointer<const ShadingNode<float>>& roughnessY,
		const Pointer<RNG>& rng,
		const interacting_t& interacting_,
		const albedo_t& albedo_,
		const evaluate_t& evaluate_,
		const sample_wi_t& sample_wi_,
		const sample_wi_pdf_t& sample_wi_pdf_
	):
		BxDF(Type(DIFFUSE | REFLECTIVE | REFRACTIVE), interacting_, albedo_, evaluate_, sample_wi_, sample_wi_pdf_),
		surface(surface),
		reflectance(reflectance),
		transmittance(transmittance),
		roughnessX(roughnessX),
		roughnessY(roughnessY) {
	}

public:
	friend class TypedPool<DielectricMicrosurfaceBSDF>;

	__host__
	static Pointer<DielectricMicrosurfaceBSDF> build(
		MemoryPool& pool,
		const Pointer<const ShadingNode<float>>& reflectance,
		const Pointer<const ShadingNode<float>>& transmittance,
		const Pointer<const ShadingNode<float>>& roughnessX,
		const Pointer<const ShadingNode<float>>& roughnessY,
		const float uniformPDF,
		const unsigned int maxScatteringOrder)
	{
		auto height = UniformHeightDistribution::build(pool);
		auto slope = GGXSlopeDistribution::build(pool);
		auto surface = DielectricMicrosurface::build(pool, height, slope, uniformPDF, maxScatteringOrder);
		auto rng = CPUBasicRNG<>::build(pool, 1, 0);

		return Pointer<DielectricMicrosurfaceBSDF>::make(
			pool, surface, reflectance, transmittance,
			roughnessX, roughnessY, rng,
			interacting_t(virtualize(interacting_)),
			albedo_t(virtualize(albedo_)),
			evaluate_t(virtualize(evaluate_)),
			sample_wi_t(virtualize(sample_wi_)),
			sample_wi_pdf_t(virtualize(sample_wi_pdf_))
		);
	}
};

}

#endif /* DIELECTRICMICROSURFACEBSDF_CUH_ */

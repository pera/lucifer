#ifndef GGXSLOPEDISTRIBUTION_CUH_
#define GGXSLOPEDISTRIBUTION_CUH_

#include "SlopeDistribution.cuh"
#include "lucifer/math/Math.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer {

/*!
 * \brief
 * The GGX distribution of slopes.
 *
 * Based on: Multiple-Scattering Microfacet BSDFs with the Smith Model.
 * Eric Heitz, Johannes Hanika, Eugene d'Eon, Carsten Dachbacher
 */
class GGXSlopeDistribution: public SlopeDistribution {

private:
	__host__ __device__ static
	float p22_(const SlopeDistribution* this_, const float slopeX, const float slopeY, const float alphaX, const float alphaY) {
		const float tmp = 1.f + slopeX * slopeX / (alphaX * alphaX) + slopeY * slopeY / (alphaY * alphaY);
		return 1.f / (PI<float>() * alphaX * alphaY) / (tmp * tmp);
	}

	__host__ __device__ static
	float lambda_(const SlopeDistribution* self, const Vectr3F& wi, const float alphaX, const float alphaY) {
		if(wi[2] > +0.9999f) return +0.f;
		if(wi[2] < -0.9999f) return -1.f;
		// a
		const float theta_i = acos(wi[2]);
		const float a = 1.f / tan(theta_i) / self->alpha(wi, alphaX, alphaY);
		// value
		return 0.5f * (-1.f + sign(a) * sqrt(1.f + 1.f / (a * a)));
	}

	__host__ __device__ static
	float proj_area_(const SlopeDistribution* self, const Vectr3F& wi, const float alphaX, const float alphaY) {
		if (wi[2] > +0.9999f) return 1.f;
		if (wi[2] < -0.9999f) return 0.f;
		// a
		const float theta_i = acos(wi[2]);
		const float sin_theta_i = sin(theta_i);
		const float alphai = self->alpha(wi, alphaX, alphaY);
		// value
		return 0.5f * (wi[2] + sqrt(wi[2] * wi[2] + sin_theta_i * sin_theta_i * alphai * alphai));
	}

	__host__ __device__ static
	Vectr2F smp_p22_(const SlopeDistribution* self, const float theta_i, const float U, const float U_2) {
		Vectr2F slope;
		if(theta_i < 0.0001f) {
			const float r = sqrt(U / (1.f - U));
			const float phi = 6.28318530718f * U_2;
			slope[0] = r * cos(phi);
			slope[1] = r * sin(phi);
			return slope;
		}
		// constant
		const float sin_theta_i = sin(theta_i);
		const float cos_theta_i = cos(theta_i);
		const float tan_theta_i = sin_theta_i / cos_theta_i;
		// projected area
		const float projectedarea = 0.5f * (cos_theta_i + 1.f);
		if(projectedarea < 0.0001f || isnan(projectedarea)) return Vectr2F{0.f, 0.f};
		// normalization coefficient
		const float c = 1.f / projectedarea;
		const float A = 2.f * U / cos_theta_i / c - 1.f;
		const float B = tan_theta_i;
		const float tmp = 1.f / (A * A - 1.f);
		const float D = sqrt(max(0.f, B * B * tmp * tmp - (A * A - B * B) * tmp));
		const float slope_x_1 = B * tmp - D;
		const float slope_x_2 = B * tmp + D;
		slope[0] = (A < 0.f || slope_x_2 > 1.f / tan_theta_i) ? slope_x_1 : slope_x_2;
		float U2;
		float S;
		if(U_2 > 0.5f) {
			S = +1.f;
			U2 = 2.f * (U_2 - 0.5f);
		} else {
			S = -1.f;
			U2 = 2.f * (0.5f - U_2);
		}
		const float z = (U2 * (U2 * (U2 * 0.27385f - 0.73369f) + 0.46341f)) / (U2 * (U2 * (U2 * 0.093073f + 0.309420f) - 1.000000f) + 0.597999f);
		slope[1] = S * z * sqrt(1.f + slope[0] * slope[0]);
		return slope;
	}

	__host__
	GGXSlopeDistribution(const p22_f& p22, const lambda_f& lambda, const proj_area_f& proj_area, const smp_p22_f& smp_p22):
	SlopeDistribution(p22, lambda, proj_area, smp_p22) {
	}

public:
	friend class TypedPool<GGXSlopeDistribution>;

	__host__ static
	Pointer<GGXSlopeDistribution> build(MemoryPool& pool) {
		return Pointer<GGXSlopeDistribution>::make(
			pool,
			p22_f(virtualize(p22_)),
			lambda_f(virtualize(lambda_)),
			proj_area_f(virtualize(proj_area_)),
			smp_p22_f(virtualize(smp_p22_))
		);
	}

};

}

#endif /* GGXSLOPEDISTRIBUTION_CUH_ */

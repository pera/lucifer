#ifndef DIFFUSEMICROSURFACEBRDF_CUH_
#define DIFFUSEMICROSURFACEBRDF_CUH_

#include "UniformHeightDistribution.cuh"
#include "GGXSlopeDistribution.cuh"
#include "DiffuseMicrosurface.cuh"
#include "lucifer/bsdf/BxDF.cuh"
#include "lucifer/math/Util.cuh"
#include "lucifer/random/CPUBasicRNG.cuh"
#include "lucifer/random/CosineHemisphereDistribution.cuh"

namespace lucifer {

/*!
 * \brief
 * BRDF that models a microsurface composed of perfect diffuse microfacets.
 */
class DiffuseMicrosurfaceBRDF: public BxDF {
private:
	const Pointer<const DiffuseMicrosurface> surface;
	const Pointer<const ShadingNode<float>> reflectance;
	const Pointer<const ShadingNode<float>> roughnessX;
	const Pointer<const ShadingNode<float>> roughnessY;

	__host__ __device__
	static const DiffuseMicrosurfaceBRDF* cast(const BxDF* bxdf) {
		return static_cast<const DiffuseMicrosurfaceBRDF*>(bxdf);
	}

	__host__ __device__
	static auto parameterize(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const ComplexF& ior1, const ComplexF& ior2, RNG* rng) {
		return Microsurface::Parameter {
			rng,
			(*cast(bxdf)->roughnessX)(g.gp, g.su, g.sn, g.uv, wlen, time),
			(*cast(bxdf)->roughnessY)(g.gp, g.su, g.sn, g.uv, wlen, time),
			(*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time),
			0.f, ior1, ior2
		};
	}

	__host__ __device__
	static bool interacting_(const BxDF* bxdf) {
		return true;
	}

	__host__ __device__
	static float albedo_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode) {
		return (*cast(bxdf)->reflectance)(g.gp, g.su, g.sn, g.uv, wlen, time);
	}

	__host__ __device__
	static float evaluate_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, RNG* rng) {
		auto param = parameterize(bxdf, g, wlen, time, ior1, ior2, rng);
		if (abs(wo[2]) < abs(wi[2])) {
			return (wo[2] < 0.f) ?
				cast(bxdf)->surface->evaluateMS(-wo, -wi, param) / abs(wi[2]):
				cast(bxdf)->surface->evaluateMS( wo,  wi, param) / abs(wi[2]);
		} else {
			return (wi[2] < 0.f) ?
				cast(bxdf)->surface->evaluateMS(-wi, -wo, param) / abs(wi[2]) / absCosTheta(wo) * absCosTheta(wi):
				cast(bxdf)->surface->evaluateMS( wi,  wo, param) / abs(wi[2]) / absCosTheta(wo) * absCosTheta(wi);
		}
	}

	__host__ __device__
	static float sample_wi_pdf_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, bool sampled, RNG* rng) {
		auto param = parameterize(bxdf, g, wlen, time, ior1, ior2, rng);
		return (wo[2] < 0.f) ?
			cast(bxdf)->surface->MISPDF(-wo, -wi, param):
			cast(bxdf)->surface->MISPDF( wo,  wi, param);
	}

	__host__ __device__
	static float sample_wi_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng) {
		auto param = parameterize(bxdf, g, wlen, time, ior1, ior2, rng);
		auto weight = cast(bxdf)->surface->sample((wo[2] < 0.f) ? -wo : wo, *wi, param);
		if (wo[2] < 0.f) *wi = -*wi;
		*pdf = 1.f;
		return weight / abs((*wi)[2]);
	}

	__host__
	DiffuseMicrosurfaceBRDF(
		const Pointer<const DiffuseMicrosurface>& surface,
		const Pointer<const ShadingNode<float>>& reflectance,
		const Pointer<const ShadingNode<float>>& roughnessX,
		const Pointer<const ShadingNode<float>>& roughnessY,
		const interacting_t& interacting_,
		const albedo_t& albedo_,
		const evaluate_t& evaluate_,
		const sample_wi_t& sample_wi_,
		const sample_wi_pdf_t& sample_wi_pdf_
	):
		BxDF(Type(DIFFUSE | REFLECTIVE), interacting_, albedo_, evaluate_, sample_wi_, sample_wi_pdf_),
		surface(surface),
		reflectance(reflectance),
		roughnessX(roughnessX),
		roughnessY(roughnessY) {
	}

public:
	friend class TypedPool<DiffuseMicrosurfaceBRDF>;

	__host__
	static Pointer<DiffuseMicrosurfaceBRDF> build(
		MemoryPool& pool,
		const Pointer<const ShadingNode<float>>& reflectance,
		const Pointer<const ShadingNode<float>>& roughnessX,
		const Pointer<const ShadingNode<float>>& roughnessY,
		const unsigned int maxScatteringOrder)
	{
		auto height = UniformHeightDistribution::build(pool);
		auto slope = GGXSlopeDistribution::build(pool);
		auto surface = DiffuseMicrosurface::build(pool, height, slope, maxScatteringOrder);

		return Pointer<DiffuseMicrosurfaceBRDF>::make(
			pool, surface, reflectance, roughnessX, roughnessY,
			interacting_t(virtualize(interacting_)),
			albedo_t(virtualize(albedo_)),
			evaluate_t(virtualize(evaluate_)),
			sample_wi_t(virtualize(sample_wi_)),
			sample_wi_pdf_t(virtualize(sample_wi_pdf_))
		);
	}
};

}

#endif /* DIFFUSEMICROSURFACEBRDF_CUH_ */

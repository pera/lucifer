#ifndef MICROSURFACE_CUH_
#define MICROSURFACE_CUH_

#include "HeightDistribution.cuh"
#include "SlopeDistribution.cuh"
#include "lucifer/math/geometry/CommonTypes.cuh"
#include "lucifer/random/RNG.cuh"

namespace lucifer {

/*!
 * \brief
 * Microsurface base class. Defines the slopes and heights distributions.
 *
 * Based on: Multiple-Scattering Microfacet BSDFs with the Smith Model.
 * Eric Heitz, Johannes Hanika, Eugene d'Eon, Carsten Dachbacher
 */
class Microsurface {
public:
	class Parameter {
	public:
		RNG* rng;
		const float alphaX;
		const float alphaY;
		const float reflect;
		const float refract;
		const ComplexF ior1;
		const ComplexF ior2;

		__host__ __device__
		Parameter(
			RNG* rng,
			const float alphaX,
			const float alphaY,
			const float reflect,
			const float refract,
			const ComplexF ior1,
			const ComplexF ior2
		):
			rng(rng),
			alphaX(alphaX),
			alphaY(alphaY),
			reflect(reflect),
			refract(refract),
			ior1(ior1),
			ior2(ior2) {
		}
	};

protected:
	Pointer<const HeightDistribution> height;
	Pointer<const SlopeDistribution> slope;

	__host__
	Microsurface(const Pointer<const HeightDistribution>& height, const Pointer<const SlopeDistribution>& slope):
		height(height), slope(slope) {
	}

public:
	/*
	 * Masking function.
	 */
	__host__ __device__
	float g1(const Vectr3F& wi, Parameter& param) const {
		if (wi[2] >  0.9999f) return 1.f;
		if (wi[2] <= 0.0000f) return 0.f;
		return 1.f / (1.f + this->slope->lambda(wi, param.alphaX, param.alphaY));
	}

	/*
	 * Masking function at height h0.
	 */
	__host__ __device__
	float g1(const Vectr3F& wi, const float h0, Parameter& param) const {
		if (wi[2] >  0.9999f) return 1.f;
		if (wi[2] <= 0.0000f) return 0.f;
		return pow(this->height->cdf(h0), this->slope->lambda(wi, param.alphaX, param.alphaY));
	}

	/*
	 * Sample height in outgoing direction.
	 */
	__host__ __device__
	float sampleHeight(const Vectr3F& wr, const float hr, const float U, Parameter& param) const {
		if (wr[2] >  0.9999f) return POSITIVE_INFINITY<float>();
		if (wr[2] < -0.9999f) return this->height->qnt(U * this->height->cdf(hr));
		if (abs(wr[2]) < 0.0001f) return hr;
		if (U > 1.f - g1(wr, hr, param)) return POSITIVE_INFINITY<float>();
		return this->height->qnt(this->height->cdf(hr) / pow((1.f - U), 1.f / this->slope->lambda(wr, param.alphaX, param.alphaY)));
	}
};

}

#endif /* MICROSURFACE_CUH_ */

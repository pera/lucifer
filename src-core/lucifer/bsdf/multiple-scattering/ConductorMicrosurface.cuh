#ifndef CONDUCTORMICROSURFACE_CUH_
#define CONDUCTORMICROSURFACE_CUH_

#include "lucifer/bsdf/Fresnel.cuh"
#include "lucifer/random/CosineHemisphereDistribution.cuh"
#include "Microsurface.cuh"

namespace lucifer {

/*
 * \brief
 * Microsurface composed of perfectly specular conductor microfacets.
 *
 * Based on: Multiple-Scattering Microfacet BSDFs with the Smith Model.
 * Eric Heitz, Johannes Hanika, Eugene d'Eon, Carsten Dachbacher
 */
class ConductorMicrosurface: public Microsurface {
private:
	const bool fresnel;
	const float uniformPDF;
	const unsigned int maxScatteringOrder;

	__host__ __device__
	float fresnelFactor(const ComplexF& ior1, const ComplexF& ior2, const float cosTheta) const {
		return fresnel ? Fresnel::reflectance(ior1, ior2, cosTheta) : 1.f;
	}

	__host__ __device__
	float MISWeight(const Vectr3F& wi, const Vectr3F& wo, Parameter& p) const {
		if(wi[0] == -wo[0] and wi[1] == -wo[1] and wi[2] == -wo[2]) return 1.0f;
		const Vectr3F wh = (wi + wo).normalize();
		return this->slope->D(wh[2] > 0.f ? wh : -wh , p.alphaX, p.alphaY);
	}

	__host__ __device__
	float evaluatePhaseFunction(const Vectr3F& wi, const Vectr3F& wo, Parameter& p) const {
		const Vectr3F wh = (wi + wo).normalize();
		if (wh[2] < 0.f) return 0.f;
		const float f = p.reflect * fresnelFactor(p.ior1, p.ior2, abs(dot(wi,  wh)));
		return 0.25f * f * this->slope->D_wi(wi, wh, p.alphaX, p.alphaY) / dot(wi, wh);
	}

	__host__ __device__
	Vectr3F samplePhaseFunction(const Vectr3F& wi, float& weight, Parameter& p) const {
		float U1;
		float U2;
		p.rng->next(&U1);
		p.rng->next(&U2);
		Vectr3F wm = this->slope->sampleD_wi(wi, p.alphaX, p.alphaY, U1, U2);
		weight = p.reflect * fresnelFactor(p.ior1, p.ior2, abs(dot(wi,  wm)));
		return -wi + 2.f * wm * dot(wi, wm);
	}
	__host__ __device__
	ConductorMicrosurface(
		const Pointer<const HeightDistribution>& height,
		const Pointer<const SlopeDistribution>& slope,
		const bool fresnel,
		const float uniformPDF,
		const unsigned int maxScatteringOrder):
		Microsurface(height, slope), fresnel(fresnel), uniformPDF(uniformPDF), maxScatteringOrder(maxScatteringOrder) {
	}

public:
	friend class TypedPool<ConductorMicrosurface>;

	__host__
	static Pointer<ConductorMicrosurface> build(
		MemoryPool& pool,
		const Pointer<const HeightDistribution>& height,
		const Pointer<const SlopeDistribution>& slope,
		const bool fresnel,
		const float uniformPDF,
		const unsigned int maxScatteringOrder) {
		return Pointer<ConductorMicrosurface>::make(pool, height, slope, fresnel, uniformPDF, maxScatteringOrder);
	}

	__host__ __device__
	float evaluateSS(const Vectr3F& wi, const Vectr3F& wo, Parameter& p) const {
		if (wi[2] * wo[2] < 0.f) return 0.f;
		const Vectr3F wh = (wi + wo).normalize();
		if (wh[2] < 0.f) return 0.f;
		const float D = this->slope->D(wh, p.alphaX, p.alphaY);
		const float lambdaI = this->slope->lambda(wi, p.alphaX, p.alphaY);
		const float lambdaO = this->slope->lambda(wo, p.alphaX, p.alphaY);
		const float G = 1.f / (1.f +  lambdaI + lambdaO);
		const float F = p.reflect * fresnelFactor(p.ior1, p.ior2, abs(dot(wi,  wh)));
		return D * G * F / (4.f * wi[2]);
	}

	/*
	 * Evaluate multiple scatter BSDF.
	 */
	__host__ __device__
	float evaluateMS(const Vectr3F& wi, const Vectr3F& wo, Parameter& p) const {
		if (wo[2] < 0.f) return 0.f;
		// init
		Vectr3F wr = -wi;
		float hr = 1.f + this->height->qnt(0.999f);
		float sum = 0.f;
		float energy = 1.f;
		float phaseWeight, wiMISWeight;
		// random walk
		int currentScatteringOrder = 0;
		while(true) {
			// next height
			float U;
			p.rng->next(&U);
			hr = this->sampleHeight(wr, hr, U, p);
			// leave the microsurface?
			if (isinf(hr)) break;
			currentScatteringOrder++;
			// next event estimation
			float phasefunction = this->evaluatePhaseFunction(-wr, wo, p);
			float shadowing = this->g1(wo, hr, p);
			float I = phasefunction * shadowing;
			// MIS
			float MIS = currentScatteringOrder == 1 ? 1.f : 2.f * wiMISWeight / (wiMISWeight + MISWeight(-wr, wo, p));
			if (!isinf(I)) sum += energy * I * MIS;
			if (maxScatteringOrder > 0 and currentScatteringOrder >= maxScatteringOrder) break;
			// next direction
			wr = this->samplePhaseFunction(-wr, phaseWeight, p);
			energy *= phaseWeight;
			if (currentScatteringOrder == 1) wiMISWeight = MISWeight(wi, wr, p);
			// if NaN (should not happen, just in case)
			if (isnan(hr) || isnan(wr[2])) return 0.f;
		}
		return sum;
	}

	/*
	 * Sample BSDF.
	 */
	__host__ __device__
	Vectr3F sample(const Vectr3F& wi, float& energy, Parameter& p) const {
		// init
		energy = 1.f;
		Vectr3F wr = -wi;
		float hr = 1.f + this->height->qnt(0.999f);
		// random walk
		float phaseWeight;
		int currentScatteringOrder = 0;
		while(true) {
			// next height
			float U;
			p.rng->next(&U);
			hr = this->sampleHeight(wr, hr, U, p);
			// leave the microsurface?
			if (isinf(hr)) break;
			if (maxScatteringOrder > 0 and currentScatteringOrder >= maxScatteringOrder) {
				energy = 0.f;
				break;
			}
			// next direction
			wr = this->samplePhaseFunction(-wr, phaseWeight, p);
			energy *= phaseWeight;
			// if NaN (should not happen, just in case)
			if (isnan(hr) || isnan(wr)) {
				energy = 0.f;
				return Vectr3F{ 0.f, 0.f ,1.f };
			}
			currentScatteringOrder++;
		}
		return wr;
	}

	__host__ __device__
	float MISPDF(const Vectr3F& wi, const Vectr3F& wo, Parameter& p) const {
		// cosine hemisphere pdf
		if (!sameHemisphere(wi, wo)) return 0.f;
		Vectr3F w(wo[0], wo[1], abs(wo[2]));
		auto pdf1 = CosineHemisphereDistribution::pdf(&(w[0]));
		// microfacet single scatter pdf
		Vectr3F wh = wi + wo;
		const float lengthE2 = wh.lengthSquared();
		if (lengthE2 == 0.f) return 0.f;
		wh /= sqrt(lengthE2);
		if (wi[2] * wh[2] < 0.f) wh = -wh;
		float pdf2 = this->slope->D_wi(wi, wh, p.alphaX, p.alphaY) / (4.f * absDot(wi, wh));
		// weighted pdf;
		float u = maxScatteringOrder == 1 ? 0.f : uniformPDF;
		return u * pdf1 + (1 - u) * pdf2;
	}
};

}

#endif /* CONDUCTORMICROSURFACE_CUH_ */

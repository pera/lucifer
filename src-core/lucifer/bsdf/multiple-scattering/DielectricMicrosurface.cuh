#ifndef DIELECTRICMICROSURFACE_CUH_
#define DIELECTRICMICROSURFACE_CUH_

#include "Microsurface.cuh"
#include "lucifer/bsdf/Fresnel.cuh"

namespace lucifer {

/*!
 * \brief
 * Microsurface composed of perfectly specular dielectric microfacets.
 *
 * Based on: Multiple-Scattering Microfacet BSDFs with the Smith Model.
 * Eric Heitz, Johannes Hanika, Eugene d'Eon, Carsten Dachbacher
 */
class DielectricMicrosurface: public Microsurface {
private:
	const float uniformPDF;
	const unsigned int maxScatteringOrder;

	__host__ __device__
	float MISWeight(const Vectr3F& wi, const Vectr3F& wo, const bool outside, Parameter& p) const {
		if (wi[2] * wo[2] >= 0.f) {
			// reflection
			if (wi[0] == -wo[0] && wi[1] == -wo[1] && wi[2] == -wo[2]) return 1.0f;
			const Vectr3F wh = (wi + wo).normalize();
			return this->slope->D((wh[2] > 0.f) ? wh : -wh , p.alphaX, p.alphaY);
		}
		else {
			// transmission
			const float invEta = outside ?  p.ior2.re / p.ior1.re : p.ior1.re / p.ior2.re;
			Vectr3F wh = -(wi + wo * invEta).normalize();
			return this->slope->D((wh[2] > 0.f) ? wh : -wh , p.alphaX, p.alphaY);
		}
	}

	__host__ __device__
	static Vectr3F refract(const Vectr3F& wi, const Vectr3F& wm, const float invEta) {
		const float cosThetaI = dot(wi, wm);
		const float cosThetaT2 = 1.f - (1.f - cosThetaI * cosThetaI) / (invEta * invEta);
		const float cosThetaT = -sqrt(max(0.f, cosThetaT2));
		return wm * (dot(wi, wm) / invEta + cosThetaT) - wi / invEta;
	}

	__host__ __device__
	float evaluatePhaseFunction(const Vectr3F& wi, const Vectr3F& wo, const bool wi_outside, const bool wo_outside, Parameter& p) const {
		if (wi_outside == wo_outside) {
			// reflection
			const Vectr3F wh = (wi + wo).normalize();
			const float value = (wi_outside) ?
					(0.25f * this->slope->D_wi( wi,  wh, p.alphaX, p.alphaY) / dot( wi,  wh) * Fresnel::reflectance(p.ior1, p.ior2, abs(dot( wi,  wh)))):
					(0.25f * this->slope->D_wi(-wi, -wh, p.alphaX, p.alphaY) / dot(-wi, -wh) * Fresnel::reflectance(p.ior1, p.ior2, abs(dot(-wi, -wh))));
			return p.reflect * value;
		} else {
			// transmission
			const float invEta = wi_outside ?  p.ior2.re / p.ior1.re : p.ior1.re / p.ior2.re;
			Vectr3F wh = -(wi + wo * invEta).normalize();
			wh *= (wi_outside) ? (sign(wh[2])) : (-sign(wh[2]));
			if (dot(wh, wi) < 0.f) return 0.f;
			if(wi_outside) {
				return invEta * invEta * (1.f - Fresnel::reflectance(p.ior1, p.ior2, abs(dot(-wi, -wh)))) *
				this->slope->D_wi( wi,  wh, p.alphaX, p.alphaY) * max(0.f, -dot( wo,  wh)) *
				p.refract / pow(dot( wi,  wh) + invEta * dot( wo,  wh), 2.f);
			} else {
				return invEta * invEta * (1.f - Fresnel::reflectance(p.ior1, p.ior2, abs(dot(-wi, -wh)))) *
				this->slope->D_wi(-wi, -wh, p.alphaX, p.alphaY) * max(0.f, -dot(-wo, -wh)) *
				p.refract / pow(dot(-wi, -wh) + invEta * dot(-wo, -wh), 2.f);
			}
		}
	}

	__host__ __device__
	Vectr3F samplePhaseFunction(const Vectr3F& wi, const bool wi_outside, bool& wo_outside, float& weight, Parameter& p) const {
		float u1;
		float u2;
		// sample wh
		p.rng->next(&u1);
		p.rng->next(&u2);
		Vectr3F wm = wi_outside ?
			 this->slope->sampleD_wi( wi, p.alphaX, p.alphaY, u1, u2):
			-this->slope->sampleD_wi(-wi, p.alphaX, p.alphaY, u1, u2);
		// compute reflection prabability
		const float f = Fresnel::reflectance(p.ior1, p.ior2, abs(dot(wi, wm)));
		const float r = (f + 0.f) * p.reflect;
		const float t = (1.f - f) * p.refract;
		const float threshold = r / (r + t);
		p.rng->next(&u1);
		if (u1 < threshold) {
			// reflect
			wo_outside =  wi_outside;
			weight = p.reflect * (f + 0.f) / (threshold + 0.f);
			return -wi + 2.f * wm * dot(wi, wm);
		}
		else {
			// refract
			const float invEta = wi_outside ?  p.ior2.re / p.ior1.re : p.ior1.re / p.ior2.re;
			wo_outside = !wi_outside;
			weight = p.refract * (1.f - f) / (1.f - threshold);
			return refract(wi, wm, invEta).normalize();
		}
	}

	__host__ __device__
	DielectricMicrosurface(
		const Pointer<const HeightDistribution>& height,
		const Pointer<const SlopeDistribution>& slope,
		const float uniformPDF,
		const unsigned int maxScatteringOrder):
		Microsurface(height, slope), uniformPDF(uniformPDF), maxScatteringOrder(maxScatteringOrder) {
	}

public:
	friend class TypedPool<DielectricMicrosurface>;

	__host__
	static Pointer<DielectricMicrosurface> build(
		MemoryPool& pool,
		const Pointer<const HeightDistribution>& height,
		const Pointer<const SlopeDistribution>& slope,
		const float uniformPDF,
		const unsigned int maxScatteringOrder) {
		return Pointer<DielectricMicrosurface>::make(pool,  height, slope, uniformPDF, maxScatteringOrder);
	}

	/*
	 * Evaluate single scatter BSDF.
	 */
	__host__ __device__
	float evaluateSS(const Vectr3F& wi, const Vectr3F& wo, Parameter& p) const {
		return 0.f;
	}

	/*
	 * Evaluate multiple scatter BSDF.
	 */
	__host__ __device__
	float evaluateMS(const Vectr3F& wi, const Vectr3F& wo, Parameter& p) const {
		// init
		Vectr3F wr = -wi;
		float hr = 1.f + this->height->qnt(0.999f);
		bool outside = true;
		float l = 0.f;
		float e = 1.f;
		float phaseWeight, wiMISWeight;
		// random walk
		int currentScatteringOrder = 0;
		while(true) {
			// next height
			float u;
			p.rng->next(&u);
			hr = (outside) ?
				 this->sampleHeight( wr,  hr, u, p):
				-this->sampleHeight(-wr, -hr, u, p);
			// leave the microsurface?
			if (isinf(hr)) break;
			currentScatteringOrder++;
			// next event estimation
			float phasefunction = evaluatePhaseFunction(-wr, wo, outside, (wo[2] > 0.f), p);
			float shadowing = (wo[2] > 0) ? this->g1(wo, hr, p) : this->g1(-wo, -hr, p);
			float I = phasefunction * shadowing;
			// MIS
			float MIS = currentScatteringOrder == 1 ? 1.f : 2.f * wiMISWeight / (wiMISWeight + MISWeight(-wr, wo, outside, p));
			if (!isinf(I)) l += e * I * MIS;
			if (maxScatteringOrder > 0 and currentScatteringOrder >= maxScatteringOrder) break;
			// next direction
			wr = samplePhaseFunction(-wr, outside, outside, phaseWeight, p);
			e *= phaseWeight;
			if (currentScatteringOrder == 1) wiMISWeight = MISWeight(wi, wr, outside, p);
			// if NaN (should not happen, just in case)
			if (isnan(hr) || isnan(wr[2])) return 0.f;
		}
		return l;
	}

	/*
	 * Sample BSDF.
	 */
	__host__ __device__
	Vectr3F sample(const Vectr3F& wi, float& energy, Parameter& p) const {
		// init
		energy = 1.f;
		Vectr3F wr = -wi;
		bool outside = true;
		float hr = 1.f + this->height->qnt(0.999f);
		// random walk
		float phaseWeight;
		int currentScatteringOrder = 0;
		while(true) {
			// next height
			float u;
			p.rng->next(&u);
			hr = (outside) ?
				 this->sampleHeight( wr,  hr, u, p):
				-this->sampleHeight(-wr, -hr, u, p);
			// leave the microsurface?
			if (isinf(hr)) break;
			if (maxScatteringOrder > 0 and currentScatteringOrder >= maxScatteringOrder) {
				energy = 0.f;
				break;
			}
			// next direction
			wr = samplePhaseFunction(-wr, outside, outside, phaseWeight, p);
			energy *= phaseWeight;
			// if NaN (should not happen, just in case)
			if (isnan(hr) || isnan(wr)) {
				energy = 0.f;
				return Vectr3F{ 0.f, 0.f ,1.f };
			}
			currentScatteringOrder++;
		}
		return wr;
	}

	/**
	 * Multiple importance sampling PDF.
	 */
	__host__ __device__
	float MISPDF(const Vectr3F& wi, const Vectr3F& wo, Parameter& p) const {
		// uniform pdf
		Vectr3F w(wo[0], wo[1], abs(wo[2]));
		auto pdf1 = CosineHemisphereDistribution::pdf(&(w[0]));
		if (sameHemisphere(wi, wo)) {
			// single scatter pdf
			const Vectr3F wh = (wi + wo).normalize();
			const float f = Fresnel::reflectance(p.ior1, p.ior2, absDot(wi, wh));
			const float r = (f + 0.f) * p.reflect;
			const float t = (1.f - f) * p.refract;
			auto pdf2 = this->slope->D_wi(wi, wh, p.alphaX, p.alphaY) / (4.f * dot(wi, wh));
			// weighted pdf;
			float u = maxScatteringOrder == 1 ? 0.f : uniformPDF;
			return (r / (r + t)) * u * pdf1 + (1 - u) * pdf2;
		} else {
			// single scatter pdf
			const float eta = p.ior1.re / p.ior2.re;
			Vectr3F wh = (wo + wi * eta).normalize();
			if (!sameHemisphere(wi, wh)) wh = -wh;
			const float f = Fresnel::reflectance(p.ior1, p.ior2, absDot(wi, wh));
			const float r = (f + 0.f) * p.reflect;
			const float t = (1.f - f) * p.refract;
			const float sqrtDenom = dot(wo, wh) + eta * dot(wi, wh);
			const float dwh_dwo = abs((eta * eta * dot(wo, wh)) / (sqrtDenom * sqrtDenom));
			auto pdf2 = this->slope->D_wi(wi, wh, p.alphaX, p.alphaY) * dwh_dwo;
			// weighted pdf;
			float u = maxScatteringOrder == 1 ? 0.f : uniformPDF;
			return (t / (r + t)) * u * pdf1 + (1 - u) * pdf2;
		}
	}
};

}

#endif /* DIELECTRICMICROSURFACE_CUH_ */

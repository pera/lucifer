#ifndef DOUBLESCATTERNORMALMAP_CUH_
#define DOUBLESCATTERNORMALMAP_CUH_

#include "lucifer/bsdf/BxDF.cuh"
#include "lucifer/math/Util.cuh"
#include "lucifer/math/geometry/Frame.cuh"

namespace lucifer {

/*!
 * \brief
 * Double-Scatter Normal Map Microsurface.
 *
 * Based On:
 * Microfacet-based Normal Mapping for Robust Monte Carlo Path Tracing.
 * By: Vincent Schüssler, Eric Heitz, Johannes Hanika and Carsten Dachbacher.
 */
class DoubleScatterNormalMap {
private:
	const Frame p;
	const Frame t;

	__host__ __device__
	static Norml3F tangent(const Norml3F& wp) {
		return Norml3F{-wp[0], -wp[1], 0.f}.normalize();
	}

	__host__ __device__
	float projAreaP(const Vectr3F& wi) const {
		return max(0.f, dot(wi, p.w)) / p.w[2];
	}

	__host__ __device__
	float projAreaT(const Vectr3F& wi) const {
		return max(0.f, dot(wi, t.w)) * sqrt(1.f - (p.w[2] * p.w[2])) / p.w[2];
	}

	__host__ __device__
	float probP(const Vectr3F& wi) const {
		const float projP = projAreaP(wi);
		return projP / (projP + projAreaT(wi));
	}

	__host__ __device__
	float g1(const Vectr3F& wi, const Vectr3F& wm) const {
		if (dot(wi, wm) <= 0.f) return 0.f;
		return min(1.f, max(0.f, wi[2]) / (projAreaP(wi) + projAreaT(wi)));
	}

public:
	__host__ __device__
	DoubleScatterNormalMap(const Norml3F& wp): p{wp}, t{tangent(wp)} {
	}

	__host__ __device__
	float evaluate(const BxDF* bxdf, const Vectr3F& wo, const Vectr3F& wi, const Geometry& g, const float wlen, const float time, const ComplexF& ior1, const ComplexF& ior2, RNG* rng1D) const {
		const auto lWo = p.toLocal(wo);
		const auto lWi = p.toLocal(wi);
		float value = 0.f;
		if (wi[2] > 0.f) {
			// reflection
			const float lambdaP = probP(wo);
			if (lambdaP > 0.f) {
				Vectr3F rWi = -reflect(wi, t.w);
				value += lambdaP * bxdf->evaluate(g, wlen, time, lWi, lWo, ior1, ior2, BxDF::Mode::RADIANCE, rng1D) * abs(lWi[2]) * g1(wi, p.w);
				value += lambdaP * bxdf->evaluate(g, wlen, time, p.toLocal(rWi), lWo, ior1, ior2, BxDF::Mode::RADIANCE, rng1D) * abs(dot(rWi, p.w)) * (1.f - g1(rWi, p.w)) * g1(wi, t.w);
			}
			if (lambdaP < 1.f) {
				Vectr3F rWo = -reflect(wo, t.w);
				value += (1.f - lambdaP) * bxdf->evaluate(g, wlen, time, lWi, p.toLocal(rWo), ior1, ior2, BxDF::Mode::RADIANCE, rng1D) * abs(lWi[2]) * g1(wi, p.w);
			}
		} else {
			// refraction
			const float lambdaP = probP(wo);
			if (lambdaP > 0.f) {
				value += lambdaP * bxdf->evaluate(g, wlen, time, lWi, lWo, ior1, ior2, BxDF::Mode::RADIANCE, rng1D) * abs(lWi[2]) * g1(-wi, p.w);
			}
			if (lambdaP < 1.f) {
				Vectr3F rWo = -reflect(wo, t.w);
				value += (1.f - lambdaP) * bxdf->evaluate(g, wlen, time, lWi, p.toLocal(rWo), ior1, ior2, BxDF::Mode::RADIANCE, rng1D) * abs(lWi[2]) * g1(-wi, p.w);
			}
		}
		return value;
	}

	__host__ __device__
	Vectr3F sample(const BxDF* bxdf, const Vectr3F& wo, const Geometry& g, const float wlen, const float time, const ComplexF& ior1, const ComplexF& ior2, float& energy, RNG* rng1D) const {
		// initialize
		energy = 1.f;
		Vectr3F wr = -wo;
		float pdf, rnd[2];

		// choose first facet
		bool outside = true;
		rng1D->next(rnd);
		if (rnd[0] > probP(wo)) {
			// reflect
			wr = reflect(-wr, t.w);
			// sample next direction
			rng1D->next(&rnd[0]);
			rng1D->next(&rnd[1]);
			float wgt = bxdf->sampleWi(g, wlen, time, -p.toLocal(wr), ior1, ior2, BxDF::Mode::RADIANCE, rnd, &wr, &pdf, rng1D);
			wgt *= abs(wr[2]) / pdf;
			// deal with unexpected conditions
			if (isinf(wgt) or isnan(wgt) or wgt <= 0.f) {
				energy = 0.f;
				return wr;
			}
			// if refracted, flip microsurface
			if (wr[2] < 0.f) {
				wr = -wr;
				outside = !outside;
			}
			// update
			wr = p.toWorld(wr);
			energy *= wgt * g1(wr, p.w);
			return outside ? wr : -wr;
		}

		// sample next direction
		rng1D->next(&rnd[0]);
		rng1D->next(&rnd[1]);
		float wgt = bxdf->sampleWi(g, wlen, time, -p.toLocal(wr), ior1, ior2, BxDF::Mode::RADIANCE, rnd, &wr, &pdf, rng1D);
		wgt *= abs(wr[2]) / pdf;
		// deal with unexpected conditions
		if (isinf(wgt) or isnan(wgt) or wgt <= 0.f) {
			energy = 0.f;
			return wr;
		}
		// flip microsurface
		if (wr[2] < 0.f) {
			wr = -wr;
			outside = !outside;
		}
		// update
		energy *= wgt;
		wr = p.toWorld(wr);
		// exit microsurface?
		rng1D->next(rnd);
		if (rnd[0] < g1(wr, p.w)) return outside ? wr : -wr;
		// reflect on wt;
		wr = reflect(-wr, t.w);
		rng1D->next(rnd);
		if (rnd[0] < g1(wr, t.w)) return outside ? wr : -wr;
		// reached max scattering
		energy = 0.f;
		return outside ? wr : -wr;
	}

	__host__ __device__
	float MISPDF(const BxDF* bxdf, const Vectr3F& wo, const Vectr3F& wi, const Geometry& g, const float wlen, const float time, const ComplexF& ior1, const ComplexF& ior2, bool sampled, RNG* rng1D) const {
		const auto lWo = p.toLocal(wo);
		const auto lWi = p.toLocal(wi);
		const float probWp = probP(wo);
		float pdf = 0.f;
		if (wi[2] > 0.f) {
			// reflection
			if (probWp > 0.f) {
				pdf += (probWp + 0.f) * bxdf->sampleWiPDF(g, wlen, time, lWo, lWi, ior1, ior2, BxDF::Mode::RADIANCE, sampled, rng1D) * g1(wi, p.w);
				if (dot(wi, t.w) > 0.f)  {
					const Vectr3F rWi = -reflect(wi, t.w);
					pdf += (probWp + 0.f) * bxdf->sampleWiPDF(g, wlen, time, lWo, p.toLocal(rWi), ior1, ior2, BxDF::Mode::RADIANCE, sampled, rng1D) * (1.f - g1(rWi, p.w)) * g1(wi, t.w);
				}
			}
			if (probWp < 1.f) {
				const Vectr3F rWo = -reflect(wo, t.w);
				pdf += (1.f - probWp) * bxdf->sampleWiPDF(g, wlen, time, p.toLocal(rWo), lWi, ior1, ior2, BxDF::Mode::RADIANCE, sampled, rng1D) * g1(wi, p.w);
			}
		} else {
			// refraction
			if (probWp > 0.f) {
				pdf += (probWp + 0.f) * bxdf->sampleWiPDF(g, wlen, time, lWo, lWi, ior1, ior2, BxDF::Mode::RADIANCE, sampled, rng1D) * g1(-wi, p.w);
			}
			if (probWp < 1.f) {
				const Vectr3F rWo = -reflect(wo, t.w);
				pdf += (1.f - probWp) * bxdf->sampleWiPDF(g, wlen, time, p.toLocal(rWo), lWi, ior1, ior2, BxDF::Mode::RADIANCE, sampled, rng1D) * g1(-wi, p.w);
			}
		}
		return pdf;
	}
};

}

#endif /* DOUBLESCATTERNORMALMAP_CUH_ */

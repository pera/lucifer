#ifndef DOUBLESCATTERRMALMAPBRDF_CUH_
#define DOUBLESCATTERRMALMAPBRDF_CUH_

#include "lucifer/bsdf/BxDF.cuh"
#include "lucifer/bsdf/LambertianBRDF.cuh"
#include "lucifer/math/geometry/Frame.cuh"
#include "lucifer/node/ConstantNode.cuh"
#include "DoubleScatterNormalMap.cuh"

namespace lucifer {

/*!
 * \brief
 *
 * Microfacet Based Double Scatter Normal Mapping BSDF.
 */
class DoubleScatterNormalMapBRDF: public BxDF {
	const Pointer<const BxDF> bxdf;
	const Pointer<const ShadingNode<Norml3F>> map;

private:

	__host__ __device__
	static const DoubleScatterNormalMapBRDF* cast(const BxDF* bxdf) {
		return static_cast<const DoubleScatterNormalMapBRDF*>(bxdf);
	}

	__host__ __device__
	static Norml3F mappedNormal(const BxDF* bxdf, const Geometry& g, const float wlen, const float time) {
		auto wp = (*cast(bxdf)->map)(g.gp, g.su, g.sn, g.uv, wlen, time);
		wp[0] = 2.f * clamp(wp[0], 0.f, 1.f) - 1.f;
		wp[1] = 2.f * clamp(wp[1], 0.f, 1.f) - 1.f;
		wp[2] = 2.f * clamp(wp[2], 0.f, 1.f) - 1.f;
		if (wp[2] < 0.f) wp = -wp;
		return wp.normalize();
	}

	__host__ __device__
	static bool interacting_(const BxDF* bxdf) {
		return true;
	}

	__host__ __device__
	static float albedo_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode) {
		return clamp(cast(bxdf)->albedo(g, wlen, time, wo, ior1, ior2, mode), 0.f, 1.f);
	}

	__host__ __device__
	static float evaluate_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, RNG* rng1D) {
		// retrieve perturbed normal
		const auto self = cast(bxdf);
		const auto wp = mappedNormal(bxdf, g, wlen, time);
		// if not perturbed, sample base bxdf
		if (isnan(wp) or isinf(wp) or wp[2] > 0.999f) {
			return self->bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, mode, rng1D);
		}
		// microsurface double scatter
		const DoubleScatterNormalMap m{wp};
		const auto value = (wo[2] < 0.f) ?
			m.evaluate(self->bxdf.get(), -wo, -wi, g, wlen, time, ior1, ior2, rng1D):
			m.evaluate(self->bxdf.get(),  wo,  wi, g, wlen, time, ior1, ior2, rng1D);
		return value / abs(wi[2]);
	}

	__host__ __device__
	static float sample_wi_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng1D) {
		// retrieve perturbed normal
		const auto self = cast(bxdf);
		const auto wp = mappedNormal(bxdf, g, wlen, time);
		// if not perturbed, sample base bxdf
		if (isnan(wp) or isinf(wp) or wp[2] > 0.999f) {
			return self->bxdf->sampleWi(g, wlen, time, wo, ior1, ior2, mode, rnd, wi, pdf, rng1D);
		}
		// sample microsurface
		float value;
		const DoubleScatterNormalMap m{wp};
		*wi = (wo[2] < 0.f) ?
			-m.sample(self->bxdf.get(), -wo, g, wlen, time, ior1, ior2, value, rng1D):
			+m.sample(self->bxdf.get(), +wo, g, wlen, time, ior1, ior2, value, rng1D);
		*pdf = 1.f;
		return value / abs((*wi)[2]);
	}

	__host__ __device__
	static float sample_wi_pdf_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, bool sampled, RNG* rng1D) {
		// retrieve perturbed normal
		const auto self = cast(bxdf);
		auto wp = mappedNormal(bxdf, g, wlen, time);
		// if not perturbed, return base bxdf pdf
		if (isnan(wp) or isinf(wp) or wp[2] > 0.999f) {
			return self->bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, mode, sampled, rng1D);
		}
		// microsurface MISPDF
		const DoubleScatterNormalMap m{wp};
		return (wo[2] < 0.f) ?
			m.MISPDF(self->bxdf.get(), -wo, -wi, g, wlen, time, ior1, ior2, sampled, rng1D):
			m.MISPDF(self->bxdf.get(),  wo,  wi, g, wlen, time, ior1, ior2, sampled, rng1D);
	}

	__host__
	DoubleScatterNormalMapBRDF(
			const Pointer<const BxDF> bxdf,
			const Pointer<const ShadingNode<Norml3F>> map,
			const interacting_t& interacting_, const albedo_t& albedo_,
			const evaluate_t& evaluate_, const sample_wi_t& sample_wi_,
			const sample_wi_pdf_t& sample_wi_pdf_) :
			BxDF(Type(bxdf->type()), interacting_, albedo_, evaluate_, sample_wi_, sample_wi_pdf_), bxdf(bxdf), map(map) {
	}

public:
	friend class TypedPool<DoubleScatterNormalMapBRDF> ;

	__host__
	static Pointer<DoubleScatterNormalMapBRDF> build(
			MemoryPool& pool,
			const Pointer<const BxDF> bxdf,
			const Pointer<const ShadingNode<Norml3F>> map) {
	return Pointer<DoubleScatterNormalMapBRDF>::make(
			pool, bxdf, map,
			interacting_t(virtualize(interacting_)),
			albedo_t(virtualize(albedo_)),
			evaluate_t(virtualize(evaluate_)),
			sample_wi_t(virtualize(sample_wi_)),
			sample_wi_pdf_t(virtualize(sample_wi_pdf_))
	);
}
};

}

#endif /* DOUBLESCATTERRMALMAPBRDF_CUH_ */

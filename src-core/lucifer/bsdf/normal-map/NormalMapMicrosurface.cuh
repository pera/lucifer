/*
 * NormalMapMicrosurface.cuh
 *
 *  Created on: 21/09/2020
 *      Author: pera
 */

#ifndef NORMALMAPMICROSURFACE_CUH_
#define NORMALMAPMICROSURFACE_CUH_

#include "lucifer/bsdf/BxDF.cuh"
#include "lucifer/math/Util.cuh"
#include "lucifer/math/geometry/Frame.cuh"
#include "lucifer/memory/Pointer.cuh"
#include "lucifer/random/CosineHemisphereDistribution.cuh"

namespace lucifer {

/*!
 * \brief
 * Multiple-Scatter Normal Map Microsurface.
 *
 * Based On:
 * Microfacet-based Normal Mapping for Robust Monte Carlo Path Tracing.
 * By: Vincent Schüssler, Eric Heitz, Johannes Hanika and Carsten Dachbacher.
 */
class NormalMapMicrosurface {
private:
	const Frame p;
	const Frame t;
	const float uniformPDF;
	const unsigned int maxScatteringOrder;

	__host__ __device__
	static Norml3F tangent(const Norml3F& wp) {
		return Norml3F{-wp[0], -wp[1], 0.f}.normalize();
	}

	__host__ __device__
	float projAreaP(const Vectr3F& wi) const {
		return max(0.f, dot(wi, p.w)) / p.w[2];
	}

	__host__ __device__
	float projAreaT(const Vectr3F& wi) const {
		return max(0.f, dot(wi, t.w)) * sqrt(1.f - (p.w[2] * p.w[2])) / p.w[2];
	}

	__host__ __device__
	float probP(const Vectr3F& wi) const {
		const float projP = projAreaP(wi);
		return projP / (projP + projAreaT(wi));
	}

	__host__ __device__
	float probT(const Vectr3F& wi) const {
		const float projT = projAreaT(wi);
		return projT / (projAreaP(wi) + projT);
	}

	__host__ __device__
	float g1(const Vectr3F& wi, const Vectr3F& wm) const {
		if (dot(wi, wm) <= 0.f) return 0.f;
		return min(1.f, max(0.f, wi[2]) / (projAreaP(wi) + projAreaT(wi)));
	}

public:
	__host__ __device__
	NormalMapMicrosurface(const Norml3F& wp, const float uniformPDF, const unsigned int maxScatteringOrder):
		p{wp}, t{tangent(wp)}, uniformPDF(uniformPDF), maxScatteringOrder(maxScatteringOrder) {
	}

	__host__ __device__
	float evaluateSpecularSS(const BxDF* bxdf, const Vectr3F& wo, const Vectr3F& wi, const Geometry& g, const float wlen, const float time, const ComplexF& ior1, const ComplexF& ior2, RNG* rng1D) const {
		float value = 0.f;
		const float lambdaP = probP(wo);
		if (lambdaP > 0.f) {
			// single scatter
			value += lambdaP * bxdf->evaluate(g, wlen, time, p.toLocal(wi), p.toLocal(wo), ior1, ior2, BxDF::Mode::RADIANCE, rng1D) * abs(dot(wi, p.w)) * g1(wi, p.w);
			if (maxScatteringOrder != 1) {
				// double scatter
				Vectr3F rWi = -reflect(wi, t.w);
				value += lambdaP * bxdf->evaluate(g, wlen, time, p.toLocal(wo), p.toLocal(rWi), ior1, ior2, BxDF::Mode::RADIANCE, rng1D) * abs(dot(rWi, p.w)) * (1.f - g1(rWi, p.w)) * g1(wi, t.w);
				if (lambdaP < 1.f) {
					Vectr3F rWo = -reflect(wo, t.w);
					value += (1.f - lambdaP) * bxdf->evaluate(g, wlen, time, p.toLocal(rWo), p.toLocal(wi), ior1, ior2, BxDF::Mode::RADIANCE, rng1D) * abs(dot(wi, p.w)) * g1(wi, p.w);
				}
			}
		}
		return value;
	}

	__host__ __device__
	float evaluateSS(const BxDF* bxdfp, const BxDF* bxdft, const Vectr3F& wo, const Vectr3F& wi, const Geometry& g, const float wlen, const float time, const ComplexF& ior1, const ComplexF& ior2, RNG* rng1D) const {
		float value = 0.f;
		const float lambdaP = probP(wo);
		if (lambdaP > 0.f) value += (lambdaP + 0.f) * bxdfp->evaluate(g, wlen, time, p.toLocal(wi), p.toLocal(wo), ior1, ior2, BxDF::Mode::RADIANCE, rng1D) * abs(p.toLocal(wi)[2]) * g1(wi, p.w);
		if (lambdaP < 1.f) value += (1.f - lambdaP) * bxdft->evaluate(g, wlen, time, t.toLocal(wi), t.toLocal(wo), ior1, ior2, BxDF::Mode::RADIANCE, rng1D) * abs(t.toLocal(wi)[2]) * g1(wi, t.w);
		return value;
	}

	__host__ __device__
	float evaluateSpecularMS(const BxDF* bxdf, const Vectr3F& wo, const Vectr3F& wi, const Geometry& g, const float wlen, const float time, const ComplexF& ior1, const ComplexF& ior2, RNG* rng1D) const {
		// initialize
		float l = 0.f;
		float e = 1.f;
		float pdf, rnd[2];

		// first facet
		Vectr3F wr = -wo;
		int scatteringOrder = 0;
		rng1D->next(rnd);
		if (rnd[0] > probP(wo)) {
			wr = reflect(-wr, t.w);
			scatteringOrder++;
			if (maxScatteringOrder == 1)
				return 0.f;
		}

		//random walk
		float pdfWo;
		bool singleScatter = true;
		const Vectr3F rWi = -reflect(wi, t.w);
		while(true) {
			// evaluate wi
			float l0 = bxdf->evaluate(g, wlen, time, p.toLocal(wi), p.toLocal(-wr), ior1, ior2, BxDF::Mode::RADIANCE, rng1D) * abs(dot(wi, p.w)) * g1(wi, p.w);
			if (!singleScatter and l0 > 0.f) {
				// backward mis pdf
				const float pdfWi = bxdf->sampleWiPDF(g, wlen, time, p.toLocal(wi), p.toLocal(-wr), ior1, ior2, BxDF::Mode::RADIANCE, false, rng1D) / abs(p.toLocal(wi)[2]);
//				l0 *= 2.f * pdfWo / (pdfWo + pdfWi);
				l0 *= pdfWo < pdfWi ? 0.f : 2.f;
			}
			if (!isinf(l0) and !isnan(l0)) l += e * l0;
			scatteringOrder++;
			if (maxScatteringOrder > 0 and scatteringOrder >= maxScatteringOrder) break;

			// evaluate reflected wi
			float l1 = bxdf->evaluate(g, wlen, time, p.toLocal(rWi), p.toLocal(-wr), ior1, ior2, BxDF::Mode::RADIANCE, rng1D) * abs(dot(rWi, p.w)) * (1.f - g1(rWi, p.w)) * g1(wi, t.w);
			if (!singleScatter and l1 > 0.f) {
				// backward mis pdf
				const float pdfWi = bxdf->sampleWiPDF(g, wlen, time, p.toLocal(rWi), p.toLocal(-wr), ior1, ior2, BxDF::Mode::RADIANCE, false, rng1D) / abs(p.toLocal(rWi)[2]);
//				l1 *= 2.f * pdfWo / (pdfWo + pdfWi);
				l1 *= pdfWo < pdfWi ? 0.f : 2.f;
			}
			if (!isinf(l1) and !isnan(l1)) l += e * l1;
			scatteringOrder++;
			if (maxScatteringOrder > 0 and scatteringOrder >= maxScatteringOrder) break;

			// sample next direction
			rng1D->next(&rnd[0]);
			rng1D->next(&rnd[1]);
			float wgt = bxdf->sampleWi(g, wlen, time, p.toLocal(-wr), ior1, ior2, BxDF::Mode::RADIANCE, rnd, &wr, &pdf, rng1D);
			wgt *= abs(wr[2]) / pdf;

			// update throuput
			if (isinf(wgt) or isnan(wgt)) return 0.f;
			if (wgt == 0.f) break;
			e *= wgt;

			// update next wr
			wr = p.toWorld(wr);
			rng1D->next(&rnd[0]);
			if (rnd[0] < g1(wr, p.w)) {
				break;
			}

			// forward mis pdf
			if (singleScatter) {
				pdfWo = bxdf->sampleWiPDF(g, wlen, time, p.toLocal(wo), p.toLocal(wr), ior1, ior2, BxDF::Mode::RADIANCE, false, rng1D) / abs(p.toLocal(wr)[2]);
				singleScatter = false;
			}

			// reflect
			wr = reflect(-wr, t.w);
			rng1D->next(&rnd[0]);
			if (rnd[0] < g1(wr, t.w)) {
				break;
			}
		}

		return l;
	}

	__host__ __device__
	float evaluateMS(const BxDF* bxdfp, const BxDF* bxdft, const Vectr3F& wo, const Vectr3F& wi, const Geometry& g, const float wlen, const float time, const ComplexF& ior1, const ComplexF& ior2, RNG* rng1D) const {
		// initialize
		float lp = 0.f;
		float e0 = 1.f;
		Vectr3F wr = -wo;

		// choose first facet
		float pdf, rnd[2];
		rng1D->next(&rnd[0]);
		const Frame* m;
		const BxDF* bxdf;
		if (rnd[0] < probP(wo)) {
			m = &p;
			bxdf = bxdfp;
		} else {
			m = &t;
			bxdf = bxdft;
		}

		// random walk
		float pdfWi;
		int scatteringOrder = 0;
		while(true) {
			// next event estimation
			float l0 = bxdf->evaluate(g, wlen, time, m->toLocal(wi), m->toLocal(-wr), ior1, ior2, BxDF::Mode::RADIANCE, rng1D) * max(0.f, m->toLocal(wi)[2]) * g1(wi, m->w);
			// MIS pdf
			if (scatteringOrder > 0 and l0 > 0.f) {
				const float pdfWo = bxdf->sampleWiPDF(g, wlen, time, m->toLocal(wi), m->toLocal(-wr), ior1, ior2, BxDF::Mode::RADIANCE, false, rng1D) / abs(m->toLocal(wi)[2]);
				l0 *= 2.f * pdfWi / (pdfWi + pdfWo);
			}
			if (!isinf(l0) and !isnan(l0)) lp += e0 * l0;

			// possibly terminate random walk
			scatteringOrder++;
			if (maxScatteringOrder > 0 and scatteringOrder >= maxScatteringOrder) break;

			// sample current facet
			rng1D->next(&rnd[0]);
			rng1D->next(&rnd[1]);
			float wgt = bxdf->sampleWi(g, wlen, time, m->toLocal(-wr), ior1, ior2, BxDF::Mode::RADIANCE, rnd, &wr, &pdf, rng1D);
			wgt *= abs(wr[2]) / pdf;

			// update throuput
			if (isinf(wgt) or isnan(wgt)) return 0.f;
			if (wgt == 0.f) break;
			e0 *= wgt;

			// leaves the microsurface?
			wr = m->toWorld(wr);
			rng1D->next(&rnd[0]);
			if (rnd[0] < g1(wr, m->w)) {
				break;
			}

			// update MIS pdf
			if (scatteringOrder == 1) {
				pdfWi = bxdf->sampleWiPDF(g, wlen, time, m->toLocal(wo), m->toLocal(wr), ior1, ior2, BxDF::Mode::RADIANCE, false, rng1D) / abs(m->toLocal(wr)[2]);
			}

			// next facet
			if (m == &p) {
				m = &t;
				bxdf = bxdft;
			} else {
				m = &p;
				bxdf = bxdfp;
			}
		}
		return lp;
	}

	__host__ __device__
	Vectr3F sampleSpecular(const BxDF* bxdf, const Vectr3F& wo, const Geometry& g, const float wlen, const float time, const ComplexF& ior1, const ComplexF& ior2, float& energy, RNG* rng1D) const {
		// initialize
		energy = 1.f;
		Vectr3F wr = -wo;
		float pdf, rnd[2];

		// random walk
		rng1D->next(rnd);
		int scatteringOrder = 0;
		if (rnd[0] > probP(wo)) {
			wr = reflect(-wr, t.w);
			scatteringOrder++;
		}
		while(true) {
			// reached max scattering order?
			if (maxScatteringOrder > 0 and scatteringOrder >= maxScatteringOrder) {
				energy = 0.f;
				break;
			}
			// sample next direction
			rng1D->next(&rnd[0]);
			rng1D->next(&rnd[1]);
			float wgt = bxdf->sampleWi(g, wlen, time, -p.toLocal(wr), ior1, ior2, BxDF::Mode::RADIANCE, rnd, &wr, &pdf, rng1D);
			wgt *= abs(wr[2]) / pdf;
			// deal with unexpected conditions
			if (isinf(wgt) or isnan(wgt) or wgt <= 0.f) {
				energy = 0.f;
				break;
			}
			// update
			energy *= wgt;
			wr = p.toWorld(wr);
			// leave microsurface?
			rng1D->next(rnd);
			if (rnd[0] < g1(wr, p.w)) break;
			scatteringOrder++;
			// reached max scattering?
			if (maxScatteringOrder > 0 and scatteringOrder >= maxScatteringOrder) {
				energy = 0.f;
				break;
			}
			// reflect on wt;
			wr = reflect(-wr, t.w);
			rng1D->next(rnd);
			if (rnd[0] < g1(wr, t.w)) break;
			scatteringOrder++;
		}
		// done
		return wr;
	}

	__host__ __device__
	Vectr3F sample(const BxDF* bxdfp, const BxDF* bxdft, const Vectr3F& wo, const Geometry& g, const float wlen, const float time, const ComplexF& ior1, const ComplexF& ior2, float& energy, RNG* rng1D) const {
		// initialize
		energy = 1.f;
		Vectr3F wi = -wo;
		// sample first facet
		float pdf, rnd[2];
		rng1D->next(&rnd[0]);
		const Frame* m;
		const BxDF* bxdf;
		if (rnd[0] < probP(wo)) {
			m = &p;
			bxdf = bxdfp;
		} else {
			m = &t;
			bxdf = bxdft;
		}
		// random walk
		int scatteringOrder = 0;
		while(true) {
			// sample current facet
			rng1D->next(&rnd[0]);
			rng1D->next(&rnd[1]);
			float val = bxdf->sampleWi(g, wlen, time, -m->toLocal(wi), ior1, ior2, BxDF::Mode::RADIANCE, rnd, &wi, &pdf, rng1D);
			// update throuput
			val = val * abs(wi[2]) / pdf;
			if (isinf(val) or isnan(val) or val <= 0.f) {
				energy = 0.f;
				break;
			}
			energy *= val;
			// leaves the microsurface?
			wi = m->toWorld(wi);
			rng1D->next(&rnd[0]);
			if (rnd[0] < g1(wi, m->w)) {
				break;
			}
			// reached max scattering order?
			scatteringOrder++;
			if (maxScatteringOrder > 0 and scatteringOrder >= maxScatteringOrder) {
				energy = 0.f;
				break;
			}
			// next facet
			if (m == &p) {
				m = &t;
				bxdf = bxdft;
			} else {
				m = &p;
				bxdf = bxdfp;
			}
		}
		return wi;
	}

	__host__ __device__
	float MISPDF(const BxDF* bxdfp, const BxDF* bxdft, const Vectr3F& wo, const Vectr3F& wi, const Geometry& g, const float wlen, const float time, const ComplexF& ior1, const ComplexF& ior2, bool sampled, RNG* rng1D) const {
		// single scatter pdf
		float pdf1 = 0.f;
		const float probWp = probP(wo);
		if (probWp > 0.f) pdf1 += (probWp + 0.f) * bxdfp->sampleWiPDF(g, wlen, time, p.toLocal(wo), p.toLocal(wi), ior1, ior2, BxDF::Mode::RADIANCE, sampled, rng1D);
		if (probWp < 1.f) pdf1 += (1.f - probWp) * bxdft->sampleWiPDF(g, wlen, time, t.toLocal(wo), t.toLocal(wi), ior1, ior2, BxDF::Mode::RADIANCE, sampled, rng1D);
		// cosine hemisphere pdf
		Vectr3F w(wi[0], wi[1], abs(wi[2]));
		const float pdf2 = CosineHemisphereDistribution::pdf(&w[0]);
		// weighted pdf
		const float u = (maxScatteringOrder == 1) ? 0.f : uniformPDF;
		return u * pdf2 + (1 - u) * pdf1;
	}

	__host__ __device__
	float MISPDFSpecular(const BxDF* bxdf, const Vectr3F& wo, const Vectr3F& wi, const Geometry& g, const float wlen, const float time, const ComplexF& ior1, const ComplexF& ior2, bool sampled, RNG* rng1D) const {
		// single / double scatter pdf
		float pdf1 = 0.f;
		const float probWp = probP(wo);
		if (probWp > 0.f) pdf1 += (probWp + 0.f) * bxdf->sampleWiPDF(g, wlen, time, p.toLocal(wo), p.toLocal(wi), ior1, ior2, BxDF::Mode::RADIANCE, sampled, rng1D) * g1(wi, p.w);
		if (maxScatteringOrder != 1) {
			if (dot(wi, t.w) > 0.f)  {
				const Vectr3F rWi = -reflect(wi, t.w);
				pdf1 += (probWp + 0.f) * bxdf->sampleWiPDF(g, wlen, time, p.toLocal(wo), p.toLocal(rWi), ior1, ior2, BxDF::Mode::RADIANCE, sampled, rng1D) * (1.f - g1(rWi, p.w)) * g1(wi, t.w);
			}
			if (probWp < 1.f) {
				const Vectr3F rWo = -reflect(wo, t.w);
				pdf1 += (1.f - probWp) * bxdf->sampleWiPDF(g, wlen, time, p.toLocal(rWo), p.toLocal(wi), ior1, ior2, BxDF::Mode::RADIANCE, sampled, rng1D) * g1(wi, p.w);
			}
		}
		// cosine hemisphere pdf
		Vectr3F w(wi[0], wi[1], abs(wi[2]));
		const float pdf2 = CosineHemisphereDistribution::pdf(&w[0]);
		// weighted pdf
		const float u = (maxScatteringOrder == 1 or maxScatteringOrder == 2) ? 0.f : uniformPDF;
		return u * pdf2 + (1 - u) * pdf1;
	}
};

}

#endif /* NORMALMAPMICROSURFACE_CUH_ */

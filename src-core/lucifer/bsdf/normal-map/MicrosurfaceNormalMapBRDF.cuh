#ifndef MICROSURFACENORMALMAPBRDF_CUH_
#define MICROSURFACENORMALMAPBRDF_CUH_

#include "lucifer/bsdf/BxDF.cuh"
#include "lucifer/bsdf/LambertianBRDF.cuh"
#include "lucifer/math/geometry/Frame.cuh"
#include "lucifer/node/ConstantNode.cuh"
#include "NormalMapMicrosurface.cuh"

namespace lucifer {

/*!
 * \brief
 *
 * Microfacet Based Multiple Scatter Normal Mapping BSDF.
 */
class MicrosurfaceNormalMapBRDF: public BxDF {
	const Pointer<const BxDF> bxdfp;
	const Pointer<const BxDF> bxdft;
	const Pointer<const ShadingNode<Norml3F>> map;
	const float uniformPDF;
	const unsigned int maxScatteringOrder;

private:

	__host__ __device__
	static const MicrosurfaceNormalMapBRDF* cast(const BxDF* bxdf) {
		return static_cast<const MicrosurfaceNormalMapBRDF*>(bxdf);
	}

	__host__ __device__
	static Norml3F mappedNormal(const BxDF* bxdf, const Geometry& g, const float wlen, const float time) {
		auto wp = (*cast(bxdf)->map)(g.gp, g.su, g.sn, g.uv, wlen, time);
		wp[0] = 2.f * clamp(wp[0], 0.f, 1.f) - 1.f;
		wp[1] = 2.f * clamp(wp[1], 0.f, 1.f) - 1.f;
		wp[2] = 2.f * clamp(wp[2], 0.f, 1.f) - 1.f;
		if (wp[2] < 0.f) wp = -wp;
		return wp.normalize();
	}

	__host__ __device__
	static bool interacting_(const BxDF* bxdf) {
		return true;
	}

	__host__ __device__
	static float albedo_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode) {
		return clamp(cast(bxdf)->albedo(g, wlen, time, wo, ior1, ior2, mode), 0.f, 1.f);
	}

	__host__ __device__
	static float evaluate_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wi, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, RNG* rng1D) {
		// retrieve perturbed normal
		const auto self = cast(bxdf);
		const auto wp = mappedNormal(bxdf, g, wlen, time);
		// if not perturbed, sample base bxdf
		if (isnan(wp) or isinf(wp) or wp[2] > 0.999f) {
			return self->bxdfp->evaluate(g, wlen, time, wi, wo, ior1, ior2, mode, rng1D);
		}
		// bidirectional random walk on microsurface
		float rnd;
		rng1D->next(&rnd);
		const NormalMapMicrosurface m{wp, cast(bxdf)->uniformPDF, cast(bxdf)->maxScatteringOrder};
		if (rnd < 0.5f) {
			const auto value = (wo[2] < 0.f) ?
				m.evaluateSpecularMS(self->bxdfp.get(), -wo, -wi, g, wlen, time, ior1, ior2, rng1D):
				m.evaluateSpecularMS(self->bxdfp.get(),  wo,  wi, g, wlen, time, ior1, ior2, rng1D);
			return value / abs(wi[2]);
		} else {
			const auto value = (wi[2] < 0.f) ?
				m.evaluateSpecularMS(self->bxdfp.get(), -wi, -wo, g, wlen, time, ior1, ior2, rng1D) / absCosTheta(wo) * absCosTheta(wi):
				m.evaluateSpecularMS(self->bxdfp.get(),  wi,  wo, g, wlen, time, ior1, ior2, rng1D) / absCosTheta(wo) * absCosTheta(wi);
			return value / abs(wi[2]);
		}
	}

	__host__ __device__
	static float sample_wi_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, float rnd[2], Vectr3F* wi, float* pdf, RNG* rng1D) {
		// retrieve perturbed normal
		const auto self = cast(bxdf);
		const auto wp = mappedNormal(bxdf, g, wlen, time);
		// if not perturbed, sample base bxdf
		if (isnan(wp) or isinf(wp) or wp[2] > 0.999f) {
			return self->bxdfp->sampleWi(g, wlen, time, wo, ior1, ior2, mode, rnd, wi, pdf, rng1D);
		}
		// random walk on microsurface
		float value;
		const NormalMapMicrosurface m{wp, cast(bxdf)->uniformPDF, cast(bxdf)->maxScatteringOrder};
		*wi = (wo[2] < 0.f) ?
			-m.sampleSpecular(self->bxdfp.get(), -wo, g, wlen, time, ior1, ior2, value, rng1D):
			+m.sampleSpecular(self->bxdfp.get(), +wo, g, wlen, time, ior1, ior2, value, rng1D);
		*pdf = 1.f;
		return value / abs((*wi)[2]);
	}

	__host__ __device__
	static float sample_wi_pdf_(const BxDF* bxdf, const Geometry& g, const float wlen, const float time, const Vectr3F& wo, const Vectr3F& wi, const ComplexF& ior1, const ComplexF& ior2, const Mode& mode, bool sampled, RNG* rng1D) {
		// retrieve perturbed normal
		const auto self = cast(bxdf);
		auto wp = mappedNormal(bxdf, g, wlen, time);
		// if not perturbed, return base bxdf pdf
		if (isnan(wp) or isinf(wp) or wp[2] > 0.999f) {
			return self->bxdfp->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, mode, sampled, rng1D);
		}
		// microsurface MISPDF
		const NormalMapMicrosurface m{wp, cast(bxdf)->uniformPDF, cast(bxdf)->maxScatteringOrder};
		return (wo[2] < 0.f) ?
			m.MISPDFSpecular(self->bxdfp.get(), -wo, -wi, g, wlen, time, ior1, ior2, sampled, rng1D):
			m.MISPDFSpecular(self->bxdfp.get(),  wo,  wi, g, wlen, time, ior1, ior2, sampled, rng1D);
	}

	__host__
	MicrosurfaceNormalMapBRDF(
			const Pointer<const BxDF> bxdfp,
			const Pointer<const BxDF> bxdft,
			const Pointer<const ShadingNode<Norml3F>> map,
			const float uniformPDF, const unsigned int maxScatteringOrder,
			const interacting_t& interacting_, const albedo_t& albedo_,
			const evaluate_t& evaluate_, const sample_wi_t& sample_wi_,
			const sample_wi_pdf_t& sample_wi_pdf_) :
			BxDF(Type(DIFFUSE | REFLECTIVE), interacting_, albedo_, evaluate_, sample_wi_, sample_wi_pdf_),
			bxdfp(bxdfp), bxdft(bxdft), map(map), uniformPDF(uniformPDF), maxScatteringOrder(maxScatteringOrder) {
	}

public:
	friend class TypedPool<MicrosurfaceNormalMapBRDF> ;

	__host__
	static Pointer<MicrosurfaceNormalMapBRDF> build(MemoryPool& pool,
			const Pointer<const BxDF> bxdf,
			const Pointer<const ShadingNode<Norml3F>> map,
			const float uniformPDF, const unsigned int maxScatteringOrder) {

	return Pointer<MicrosurfaceNormalMapBRDF>::make(
			pool, bxdf, bxdf, map, uniformPDF, maxScatteringOrder,
			interacting_t(virtualize(interacting_)),
			albedo_t(virtualize(albedo_)),
			evaluate_t(virtualize(evaluate_)),
			sample_wi_t(virtualize(sample_wi_)),
			sample_wi_pdf_t(virtualize(sample_wi_pdf_))
	);
}
};

}

#endif /* MICROSURFACENORMALMAPBRDF_CUH_ */

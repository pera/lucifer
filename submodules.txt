# diff with submodules
git diff --cached --submodule

# initialize all submodules
git submodule update --init --recursive

# update all submodules
git submodule update --remote
git submodule update --remote --merge

#include <cstdio>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/resource.h>
#include "lucifer/gui/PreviewWindow.hpp"
#include "lucifer/system/CudaEnvironment.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/math/Util.cuh"
#include "lucifer/util/Context.cuh"
#include "lucifer/util/Timer.cuh"

#include "lucifer/bsdf/normal-map/NormalMapMicrosurface.cuh"
#include "lucifer/bsdf/multiple-scattering/ConductorMicrosurfaceBRDF.cuh"
#include "lucifer/bsdf/MicrofacetBRDF.cuh"
#include "lucifer/bsdf/microfacet/SmithTrowbridgeReitzModel.cuh"
#include "lucifer/node/ConstantNode.cuh"
#include "lucifer/random/CPUBasicRNG.cuh"
#include "lucifer/random/CorrelatedMultijitteredRNG.cuh"
#include "lucifer/random/UniformHemisphereDistribution.cuh"
#include "lucifer/statistics/Statistics.cuh"
#include "lucifer/util/Context.cuh"

using namespace lucifer;

void handler(int sig) {
  void* array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}

int main(int argc, char* argv[]) {
//	Context::initialize();
//	{
//		NormalMapMicrosurface m{Norml3F{1.f, 1.f, 1.f}.normalize(), 0.5, 0};
//		Pointer<RNG> rng1 = CPUBasicRNG<>::build(Context::cpuOnlyMemoryPool(), 1, 0);
//
//		auto bxdf = MicrofacetBRDF::build(
//			Context::cpuOnlyMemoryPool(),
//			ConstantNode<float,LUCIFER_SHADING_INPUT>::build(Context::cpuOnlyMemoryPool(), 1.f),
//			SmithTrowbridgeReitzModel::build(
//				Context::cpuOnlyMemoryPool(),
//				ConstantNode<float,LUCIFER_SHADING_INPUT>::build(Context::cpuOnlyMemoryPool(), 1E-1f),
//				ConstantNode<float,LUCIFER_SHADING_INPUT>::build(Context::cpuOnlyMemoryPool(), 1E-1f)
//			),
//			false
//		);
//
//		auto bxdf = ConductorMicrosurfaceBRDF::build(
//			Context::cpuOnlyMemoryPool(),
//			ConstantNode<float,LUCIFER_SHADING_INPUT>::build(Context::cpuOnlyMemoryPool(), 1.f),
//			ConstantNode<float,LUCIFER_SHADING_INPUT>::build(Context::cpuOnlyMemoryPool(), 1E-1f),
//			ConstantNode<float,LUCIFER_SHADING_INPUT>::build(Context::cpuOnlyMemoryPool(), 1E-1f),
//			false, 0.5f, 1
//		);
//
//		Geometry g;
//		Vectr3F wo, wi;
//
//		float rnd[2];
//		int nCases = 1024;
//		int nSamples = 256;
//		Pointer<RNG> rng2 = CorrelatedMultijitteredRNG::build(Context::cpuOnlyMemoryPool(), 2, nCases, 0);
//
//		for (int j = 0; j < nCases; ++j) {
//			rng2->next(rnd);
//			UniformHemisphereDistribution::sample(rnd, &wo[0], nullptr);
//			rng2->next(rnd);
//			UniformHemisphereDistribution::sample(rnd, &wi[0], nullptr);
//
//			Statistics<float> s1;
//			Statistics<float> s2;
//			for (int i = 0; i < nSamples; ++i) {
//				s1.add(m.evaluateMS(bxdf.get(), bxdf.get(), wo, wi, g, 0, 0, ComplexF{}, ComplexF{}, rng1.get()) / wi[2]) ;
//				s2.add(m.evaluateMS(bxdf.get(), bxdf.get(), wi, wo, g, 0, 0, ComplexF{}, ComplexF{}, rng1.get()) / wo[2]) ;
//			}
//			std::cout << wo << "\t" << wi << "\t" << s1.avg() << "\t" << s1.var() << "\t" << s2.avg() << "\t" << s2.var() << std::endl;
//		}
//	}
//	Context::shutdown();

	// set gpu stack size limit
	int count;
	cudaError_t error = cudaGetDeviceCount(&count);
	if (error == cudaSuccess) {
		size_t limit;
		CUDA_CHECK(cudaDeviceSetLimit(cudaLimitStackSize, 2048));
		CUDA_CHECK(cudaDeviceGetLimit(&limit, cudaLimitStackSize));
	}

	// stack tracer
	signal(SIGSEGV, handler);
	// lucifer
	if (argc != 2) {
		printf("usage: lucifer <input xml file name>\n");
		return 0;
	}
	// load input file
	Context::initialize();
	Timer timer;
//	printf("loading input file...\n");
	timer.reset();
	Lucifer* input = new Lucifer(argv[1]);
	auto elapsed = timer.elapsed();

	printf("%s\t%.2f\t", argv[1], elapsed);
	// render scene
	preview(argc, argv, input);
	delete input;
	Context::shutdown();
	// done
//	printf("Done!\n");
	return 0;
}

//#include <gtest/gtest.h>
//#include "../TestUtils.cuh"
//#include "lucifer/bsdf/microfacet/SmithTrowbridgeReitzModel.cuh"
//#include "lucifer/math/Math.cuh"
//#include "lucifer/math/function/Constant.cuh"
//#include "lucifer/node/ConstantNode.cuh"
//#include "lucifer/random/UniformHemisphereDistribution.cuh"
//#include "lucifer/random/UniformSphereDistribution.cuh"
//#include "lucifer/random/generator/PlainRNG.cuh"
//#include "lucifer/memory/CPUAllocator.cuh"
//#include "lucifer/memory/MemoryPool.cuh"
//#include "lucifer/memory/Pointer.cuh"
//
//namespace lucifer { namespace test {
//
//constexpr int MICROFACET_MIN_COUNT = 500;
//constexpr float MICROFACET_CONVERGENCE_ERROR = 1E-3f;
//
///******************************************************************************
// * TROWBRIDGE-REITZ DISTRIBUTION TESTS
// *****************************************************************************/
///*
// * Asserts that the distribution o microfacet normals is properly normalized.
// */
//TEST(Microfacet_TrowbridgeReitz, distribution) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto m = Pointer<MicrofacetModel>{SmithTrowbridgeReitzModel::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//	)};
//	{
//		// positive geometric normal vector
//		Vectr3F wh;
//		float val, pdf, rnd[2], wlen = 540E-9, time = 0.f, integral = 0.f;
//		Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//		auto rng = PlainRNGFactory<>().build(pool, 2);
//		for (int i = 0; i < MICROFACET_MIN_COUNT; ++i) {
//			for (int j = 0; j < MICROFACET_MIN_COUNT; ++j) {
//				rng->next(rnd);
//				rnd[0] = (i + rnd[0]) / MICROFACET_MIN_COUNT;
//				rnd[1] = (j + rnd[1]) / MICROFACET_MIN_COUNT;
//				UniformHemisphereDistribution::sample(rnd, &wh[0], &pdf);
//				val = m->D(g, wlen, time, wh);
//				ASSERT_LE(0.f, val);
//				integral += abs(wh[2]) * val / pdf;
//			}
//		}
//		integral /= (MICROFACET_MIN_COUNT * MICROFACET_MIN_COUNT);
//		ASSERT_NEAR(1.f, integral, MICROFACET_CONVERGENCE_ERROR);
//	}
//	{
//		// negative geometric normal vector
//		Vectr3F wh;
//		float val, pdf, rnd[2], wlen = 540E-9, time = 0.f, integral = 0.f;
//		Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//		auto rng = PlainRNGFactory<>().build(pool, 2);
//		for (int i = 0; i < MICROFACET_MIN_COUNT; ++i) {
//			for (int j = 0; j < MICROFACET_MIN_COUNT; ++j) {
//				rng->next(rnd);
//				rnd[0] = (i + rnd[0]) / MICROFACET_MIN_COUNT;
//				rnd[1] = (j + rnd[1]) / MICROFACET_MIN_COUNT;
//				UniformHemisphereDistribution::sample(rnd, &wh[0], &pdf);
//				wh[2] = -wh[2];
//				val = m->D(g, wlen, time, wh);
//				ASSERT_LE(0.f, val);
//				integral += abs(wh[2]) * val / pdf;
//			}
//		}
//		integral /= (MICROFACET_MIN_COUNT * MICROFACET_MIN_COUNT);
//		ASSERT_NEAR(1.f, integral, MICROFACET_CONVERGENCE_ERROR);
//	}
//}
//
///*
// * Asserts that the masking function is non negative and that the projected
// * area of the visible microsurface is exactly the projected area of the
// * geometric surface.
// */
//TEST(Microfacet_TrowbridgeReitz, masking) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto m = Pointer<MicrofacetModel>{SmithTrowbridgeReitzModel::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//	)};
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//	auto rng = PlainRNGFactory<>().build(pool, 2);
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//			// compute projected area of visible microsurface
//			Vectr3F wh;
//			float val, pdf, rnd[2], wlen = 540E-9, time = 0.f, integral = 0.f;
//			for (int i = 0; i < MICROFACET_MIN_COUNT; ++i) {
//				for (int j = 0; j < MICROFACET_MIN_COUNT; ++j) {
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / MICROFACET_MIN_COUNT;
//					rnd[1] = (j + rnd[1]) / MICROFACET_MIN_COUNT;
//					UniformHemisphereDistribution::sample(rnd, &wh[0], &pdf);
//					if (wo[2] <= 0.f) wh[2] = -wh[2];
//					val = m->G1(g, wlen, time, wo, wh);
//					ASSERT_LE(0.f, val);
//					integral += val * max(0.f, dot(wo, wh)) * m->D(g, wlen, time, wh) / pdf;
//				}
//			}
//			integral /= (MICROFACET_MIN_COUNT * MICROFACET_MIN_COUNT);
//			ASSERT_NEAR(abs(wo[2]), integral, MICROFACET_CONVERGENCE_ERROR);
//		}
//	}
//}
//
///*
// * Weak white furnace test.
// */
//TEST(Microfacet_TrowbridgeReitz, weakFurnace) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto m = Pointer<MicrofacetModel>{SmithTrowbridgeReitzModel::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//	)};
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//	auto rng = PlainRNGFactory<>().build(pool, 2);
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//			// perform weak white furnace test for outgoing direction wo:
//			Vectr3F wi;
//			float pdf, rnd[2], wlen = 540E-9, time = 0.f, integral = 0.f;
//			for (int i = 0; i < MICROFACET_MIN_COUNT; ++i) {
//				for (int j = 0; j < MICROFACET_MIN_COUNT; ++j) {
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / MICROFACET_MIN_COUNT;
//					rnd[1] = (j + rnd[1]) / MICROFACET_MIN_COUNT;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//					Vectr3F wh = (wi + wo).normalize();
//					if (wh[2] * wo[2] > 0.f) integral += m->G1(g, wlen, time, wo, wh) * m->D(g, wlen, time, wh) / (4.f * abs(wo[2]) * pdf);
//				}
//			}
//			integral /= (MICROFACET_MIN_COUNT * MICROFACET_MIN_COUNT);
//			ASSERT_NEAR(1.f, integral, MICROFACET_CONVERGENCE_ERROR);
//		}
//	}
//}
//
///*
// * Asserts that the shadowing masking function is non negative
// */
//TEST(Microfacet_TrowbridgeReitz, maskingShadowing) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto m = Pointer<MicrofacetModel>{SmithTrowbridgeReitzModel::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//	)};
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//	auto rng = PlainRNGFactory<>().build(pool, 2);
//	Vectr3F wo, wi, wh;
//	float pdf, rnd[2], wlen = 540E-9, time = 0.f;
//	for (int i = 0; i < MICROFACET_MIN_COUNT * MICROFACET_MIN_COUNT; ++i) {
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], &pdf);
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wh[0], &pdf);
//		if (wh[2] * wo[2] < 0) wh[2] = -wh[2];
//		ASSERT_LE(0.f, m->G2(g, wlen, time, wo, wi, wh));
//	}
//}
//
///*
// * Asserts that the DG method is consistent with D and G2 methods.
// */
//TEST(Microfacet_TrowbridgeReitz, DGConsistency) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto m = Pointer<MicrofacetModel>{SmithTrowbridgeReitzModel::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//	)};
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//	auto rng = PlainRNGFactory<>().build(pool, 2);
//	Vectr3F wo, wi, wh;
//	float pdf, rnd[2], wlen = 540E-9, time = 0.f;
//	for (int i = 0; i < MICROFACET_MIN_COUNT * MICROFACET_MIN_COUNT; ++i) {
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], &pdf);
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wh[0], &pdf);
//		if (wh[2] * wo[2] < 0) wh[2] = -wh[2];
//		float d_ = m->D (g, wlen, time, wh);
//		float g2 = m->G2(g, wlen, time, wo, wi, wh);
//		ASSERT_NEAR(d_ * g2, m->DG(g, wlen, time, wo, wi, wh), 1E-4f);
//	}
//}
//
///*
// * Asserts that the PDF is non negative and integrates to 1.
// */
//TEST(Microfacet_TrowbridgeReitz, pdf) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto m = Pointer<MicrofacetModel>{SmithTrowbridgeReitzModel::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//	)};
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//	auto rng = PlainRNGFactory<>().build(pool, 2);
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//			// compute projected area of visible microsurface
//			Vectr3F wh;
//			float val, pdf, rnd[2], wlen = 540E-9, time = 0.f, integral = 0.f;
//			for (int i = 0; i < MICROFACET_MIN_COUNT; ++i) {
//				for (int j = 0; j < MICROFACET_MIN_COUNT; ++j) {
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / MICROFACET_MIN_COUNT;
//					rnd[1] = (j + rnd[1]) / MICROFACET_MIN_COUNT;
//					UniformSphereDistribution::sample(rnd, &wh[0], &pdf);
//					val = m->sampleWhPDF(g, wlen, time, wo, wh);
//					ASSERT_LE(0.f, val);
//					if (wh[2] * wh[2] < 0.f) ASSERT_EQ(0.f, val);
//					integral += val / pdf;
//				}
//			}
//			integral /= (MICROFACET_MIN_COUNT * MICROFACET_MIN_COUNT);
//			ASSERT_NEAR(1.f, integral, MICROFACET_CONVERGENCE_ERROR);
//		}
//	}
//}
//
///*
// * Asserts that sampling has consistent and pdf, that the sampled vector is
// * properly normalized and lies on the correct hemisphere.
// */
//TEST(Microfacet_TrowbridgeReitz, sample) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto m = Pointer<MicrofacetModel>{SmithTrowbridgeReitzModel::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//	)};
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//	auto rng = PlainRNGFactory<>().build(pool, 2);
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//			// compute projected area of visible microsurface
//			Vectr3F wh;
//			float pdf, rnd[2], wlen = 540E-9, time = 0.f;
//			for (int i = 0; i < MICROFACET_MIN_COUNT; ++i) {
//				for (int j = 0; j < MICROFACET_MIN_COUNT; ++j) {
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / MICROFACET_MIN_COUNT;
//					rnd[1] = (j + rnd[1]) / MICROFACET_MIN_COUNT;
//					wh = m->sampleWh(g, wlen, time, wo, rnd, &pdf);
//					ASSERT_NEAR(1.f, wh.length(), 1E-4);
//					ASSERT_LE(0.f, wo[2] * wh[2]);
//					ASSERT_NEAR(pdf, m->sampleWhPDF(g, wlen, time, wo, wh), 1E-4f);
//				}
//			}
//		}
//	}
//}
//
//}}

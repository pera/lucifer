//#include <gtest/gtest.h>
//#include "lucifer/math/function/Constant.cuh"
//#include "lucifer/math/Math.cuh"
//#include "lucifer/node/ConstantNode.cuh"
//#include "lucifer/random/UniformSphereDistribution.cuh"
//#include "lucifer/random/generator/PlainRNG.cuh"
//#include "lucifer/uedf/DeltaDirectionalUEDC.cuh"
//#include "lucifer/uedf/FalloffHemisphericalUEDC.cuh"
//#include "lucifer/uedf/IESProfileUEDC.cuh"
//#include "lucifer/uedf/IsotropicHemisphericalUEDC.cuh"
//#include "lucifer/uedf/UEDF.cuh"
//#include "lucifer/memory/CPUAllocator.cuh"
//#include "lucifer/memory/MemoryPool.cuh"
//#include "lucifer/memory/Pointer.cuh"
//#include "../TestUtils.cuh"
//
//namespace lucifer { namespace test {
//
//constexpr int UEDF_MIN_COUNT = 10000;
//constexpr float UEDF_CONVERGENCE_ERROR = 1E-3f;
//
///******************************************************************************
// * ISOTROPIC HEMISPHERICAL UEDC TESTS
// *****************************************************************************/
///*
// * Asserts the delta distribution discriminator.
// */
//TEST(UEDC_Isotropic, delta) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto uedc = IsotropicHemisphericalUEDC::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f));
//	ASSERT_FALSE(uedc->isDelta());
//}
//
///*
// * Asserts the total emitted spectral power
// */
//TEST(UEDC_Isotropic, emission0) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto uedc = IsotropicHemisphericalUEDC::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f));
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//	ASSERT_NEAR(2.f, uedc->emission0(g, 540E-9, 0.f), 1E-4f);
//}
//
///*
// * Asserts the directional differential of the spectral emitted power is zero
// * on the hemisphere opposite to the surface normal and that it integrates to 1
// * on the entire sphere of directions.
// */
//TEST(UEDC_Isotropic, emission1) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], pdf, val;
//	auto uedc = IsotropicHemisphericalUEDC::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f));
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	// samples a random geometry
//	Point3F p;
//	rng->next(&p[0]);
//	Vectr3F n;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &n[0], nullptr);
//	Vectr3F u, v;
//	orthonormalBasis(n, &u, &v);
//	Geometry g(p, u, Norml3F(n));
//	// monte-carlo integration until convergence
//	float valSum = 0.f;
//	for (int i = 0; true; ++i) {
//		// updates valSum
//		Vectr3F w;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &w[0], &pdf);
//		val = uedc->emission1(g, 540E-9, 0.f, w);
//		if (dot(w, g.gn) > 0.f) {
//			ASSERT_TRUE(val >  0.f);
//		} else {
//			ASSERT_TRUE(val == 0.f);
//		}
//		valSum += val / pdf;
//		// check if reached convergence
//		if (i >= UEDF_MIN_COUNT) {
//			if (abs(1.f - valSum / (i + 1)) < UEDF_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the sampled direction is on the right hemisphere and that the
// * sampled directional differential of emitted power integrates to 1 over the
// * entire sphere of directions and that sampleWe and sampleWePDF methods are
// * consistent with respect to each other.
// */
//TEST(UEDC_Isotropic, sampleWe) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], pdf, val;
//	auto uedc = IsotropicHemisphericalUEDC::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f));
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	// samples a random geometry
//	Point3F p;
//	rng->next(&p[0]);
//	Vectr3F n;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &n[0], nullptr);
//	Vectr3F u, v;
//	orthonormalBasis(n, &u, &v);
//	Geometry g(p, u, Norml3F(n));
//	float valSum = 0.f;
//	for (int i = 0; true; ++i) {
//		// updates valSum
//		Vectr3F w;
//		rng->next(rnd);
//		val = uedc->sampleWe(g, 540E-9, 0.f, rnd, &w, &pdf);
//		ASSERT_TRUE(val > 0.f);
//		ASSERT_TRUE(pdf > 0.f);
//		ASSERT_TRUE(dot(w, g.gn) > 0.f);
//		ASSERT_NEAR(1.f, w.lengthSquared(), 1E-4);
//		ASSERT_NEAR(uedc->emission1(g, 540E-9, 0.f, w), val, 1E-4);
//		ASSERT_NEAR(uedc->sampleWePDF(g, 540E-9, 0.f, w), pdf, 1E-4);
//		valSum += val / pdf;
//		// check if reached convergence
//		if (i >= UEDF_MIN_COUNT) {
//			if (abs(1.f - valSum / (i + 1)) < UEDF_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the PDF of sampling a direction integrates to 1 over the entire
// * sphere of directions.
// */
//TEST(UEDC_Isotropic, sampleWePDF) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], pdf, wPDF;
//	auto uedc = IsotropicHemisphericalUEDC::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f));
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	// samples a random geometry
//	Point3F p;
//	rng->next(&p[0]);
//	Vectr3F n;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &n[0], nullptr);
//	Vectr3F u, v;
//	orthonormalBasis(n, &u, &v);
//	Geometry g(p, u, Norml3F(n));
//	float pdfSum = 0.f;
//	for (int i = 0; true; ++i) {
//		// updates pdfSum
//		Vectr3F w;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &w[0], &pdf);
//		wPDF = uedc->sampleWePDF(g, 540E-9, 0.f, w);
//		if (dot(w, g.gn) > 0.f) {
//			ASSERT_TRUE(wPDF >  0.f);
//		} else {
//			ASSERT_TRUE(wPDF == 0.f);
//		}
//		pdfSum += wPDF / pdf;
//		// check if reached convergence
//		if (i >= UEDF_MIN_COUNT) {
//			if (abs(1.f - pdfSum / (i + 1)) < UEDF_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///******************************************************************************
// * FALLOFF HEMISPHERICAL UEDC TESTS
// *****************************************************************************/
///*
// * Asserts the delta distribution discriminator.
// */
//TEST(UEDC_Falloff, delta) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float min = PI<float>() / 8.f;
//	float max = PI<float>() / 4.f;
//	auto uedc = FalloffHemisphericalUEDC::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, min),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, max)
//	);
//	ASSERT_FALSE(uedc->isDelta());
//}
//
///*
// * Asserts the total emitted spectral power
// */
//TEST(UEDC_Falloff, emission0) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float min = PI<float>() / 8.f;
//	float max = PI<float>() / 4.f;
//	auto uedc = FalloffHemisphericalUEDC::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, min),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, max)
//	);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//	ASSERT_NEAR(2.f, uedc->emission0(g, 540E-9, 0.f), 1E-4f);
//}
//
///*
// * Asserts the directional differential of the spectral emitted power is zero
// * on the hemisphere opposite to the surface normal and that it integrates to 1
// * on the entire sphere of directions.
// */
//TEST(UEDC_Falloff, emission1) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], pdf, val;
//	float min = PI<float>() / 8.f;
//	float max = PI<float>() / 4.f;
//	auto uedc = FalloffHemisphericalUEDC::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, min),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, max)
//	);
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	// samples a random geometry
//	Point3F p;
//	rng->next(&p[0]);
//	Vectr3F n;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &n[0], nullptr);
//	Vectr3F u, v;
//	orthonormalBasis(n, &u, &v);
//	Geometry g(p, u, Norml3F(n));
//	// monte-carlo integration until convergence
//	float valSum = 0.f;
//	for (int i = 0; true; ++i) {
//		// updates valSum
//		Vectr3F w;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &w[0], &pdf);
//		val = uedc->emission1(g, 540E-9, 0.f, w);
//		ASSERT_LE(0.f, val);
//		valSum += val / pdf;
//		// check if reached convergence
//		if (i >= UEDF_MIN_COUNT) {
//			if (abs(1.f - valSum / (i + 1)) < UEDF_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the sampled direction is on the right hemisphere and that the
// * sampled directional differential of emitted power integrates to 1 over the
// * entire sphere of directions and that sampleWe and sampleWePDF methods are
// * consistent with respect to each other.
// */
//TEST(UEDC_Falloff, sampleWe) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], pdf, val;
//	float min = PI<float>() / 8.f;
//	float max = PI<float>() / 4.f;
//	auto uedc = FalloffHemisphericalUEDC::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, min),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, max)
//	);
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	// samples a random geometry
//	Point3F p;
//	rng->next(&p[0]);
//	Vectr3F n;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &n[0], nullptr);
//	Vectr3F u, v;
//	orthonormalBasis(n, &u, &v);
//	Geometry g(p, u, Norml3F(n));
//	float valSum = 0.f;
//	for (int i = 0; true; ++i) {
//		// updates valSum
//		Vectr3F w;
//		rng->next(rnd);
//		val = uedc->sampleWe(g, 540E-9, 0.f, rnd, &w, &pdf);
//		ASSERT_LE(0.f, val);
//		ASSERT_LT(0.f, pdf);
//		ASSERT_LE(acos(dot(w, g.gn)), 1E-4f + max / 2.f);
//		ASSERT_NEAR(1.f, w.lengthSquared(), 1E-4);
//		ASSERT_NEAR(uedc->emission1(g, 540E-9, 0.f, w), val, 1E-4);
//		ASSERT_NEAR(uedc->sampleWePDF(g, 540E-9, 0.f, w), pdf, 1E-4);
//		valSum += val / pdf;
//		// check if reached convergence
//		if (i >= UEDF_MIN_COUNT) {
//			if (abs(1.f - valSum / (i + 1)) < UEDF_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the PDF of sampling a direction integrates to 1 over the entire
// * sphere of directions.
// */
//TEST(UEDC_Falloff, sampleWePDF) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], pdf, wPDF;
//	float min = PI<float>() / 8.f;
//	float max = PI<float>() / 4.f;
//	auto uedc = FalloffHemisphericalUEDC::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, min),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, max)
//	);
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	// samples a random geometry
//	Point3F p;
//	rng->next(&p[0]);
//	Vectr3F n;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &n[0], nullptr);
//	Vectr3F u, v;
//	orthonormalBasis(n, &u, &v);
//	Geometry g(p, u, Norml3F(n));
//	float pdfSum = 0.f;
//	for (int i = 0; true; ++i) {
//		// updates pdfSum
//		Vectr3F w;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &w[0], &pdf);
//		wPDF = uedc->sampleWePDF(g, 540E-9, 0.f, w);
//		ASSERT_LE(0.f, wPDF);
//		pdfSum += wPDF / pdf;
//		// check if reached convergence
//		if (i >= UEDF_MIN_COUNT) {
//			if (abs(1.f - pdfSum / (i + 1)) < UEDF_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///******************************************************************************
// * IES PROFILE UEDC TESTS
// *****************************************************************************/
//auto buildTestIESProfile(MemoryPool& pool) {
//	IESProfileData data;
//	data.type = IESNA_TYPEC;
//	data.factor = 1.f;
//	// vertical angles
//	data.vAngles.push_back( 0.f);
//	data.vAngles.push_back(30.f);
//	data.vAngles.push_back(60.f);
//	data.vAngles.push_back(90.f);
//	// horizontal angles
//	data.hAngles.push_back( 0.f);
//	// candela values
//	data.cValues.push_back(30.f);
//	data.cValues.push_back(20.f);
//	data.cValues.push_back(10.f);
//	data.cValues.push_back( 0.f);
//	return Pointer<UEDC>{IESProfileUEDC::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f), data)};
//}
//
///*
// * Asserts the delta distribution discriminator.
// */
//TEST(UEDC_IESProfile, delta) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto uedc = buildTestIESProfile(pool);
//	ASSERT_FALSE(uedc->isDelta());
//}
//
///*
// * Asserts the total emitted spectral power
// */
//TEST(UEDC_IESProfile, emission0) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto uedc = buildTestIESProfile(pool);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//	ASSERT_NEAR(2.f, uedc->emission0(g, 540E-9, 0.f), 1E-4f);
//}
//
///*
// * Asserts the directional differential of the spectral emitted power is zero
// * on the hemisphere opposite to the surface normal and that it integrates to 1
// * on the entire sphere of directions.
// */
//TEST(UEDC_IESProfile, emission1) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], pdf, val;
//	auto uedc = buildTestIESProfile(pool);
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	// samples a random geometry
//	Point3F p;
//	rng->next(&p[0]);
//	Vectr3F n;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &n[0], nullptr);
//	Vectr3F u, v;
//	orthonormalBasis(n, &u, &v);
//	Geometry g(p, u, Norml3F(n));
//	// monte-carlo integration until convergence
//	float valSum = 0.f;
//	for (int i = 0; true; ++i) {
//		// updates valSum
//		Vectr3F w;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &w[0], &pdf);
//		val = uedc->emission1(g, 540E-9, 0.f, w);
//		if (isnan(val) || isinf(val) || val < 0.f) {
//			uedc->emission1(g, 540E-9, 0.f, w);
//			break;
//		}
//		ASSERT_LE(0.f, val);
//		valSum += val / pdf;
//		// check if reached convergence
//		if (i >= UEDF_MIN_COUNT) {
//			if (abs(1.f - valSum / (i + 1)) < UEDF_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the sampled direction is on the right hemisphere and that the
// * sampled directional differential of emitted power integrates to 1 over the
// * entire sphere of directions and that sampleWe and sampleWePDF methods are
// * consistent with respect to each other.
// */
//TEST(UEDC_IESProfile, sampleWe) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], pdf, val;
//	auto uedc = buildTestIESProfile(pool);
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	// samples a random geometry
//	Point3F p;
//	rng->next(&p[0]);
//	Vectr3F n;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &n[0], nullptr);
//	Vectr3F u, v;
//	orthonormalBasis(n, &u, &v);
//	Geometry g(p, u, Norml3F(n));
//	float valSum = 0.f;
//	for (int i = 0; true; ++i) {
//		// updates valSum
//		Vectr3F w;
//		rng->next(rnd);
//		val = uedc->sampleWe(g, 540E-9, 0.f, rnd, &w, &pdf);
//		ASSERT_LE(0.f, val);
//		ASSERT_LT(0.f, pdf);
//		ASSERT_NEAR(1.f, w.lengthSquared(), 1E-4);
//		ASSERT_NEAR(uedc->emission1(g, 540E-9, 0.f, w), val, 1E-4);
//		ASSERT_NEAR(uedc->sampleWePDF(g, 540E-9, 0.f, w), pdf, 1E-4);
//		if (dot(w, g.gn) < 0.f) ASSERT_EQ(0.f, val);
//		valSum += val / pdf;
//		// check if reached convergence
//		if (i >= UEDF_MIN_COUNT) {
//			if (abs(1.f - valSum / (i + 1)) < UEDF_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the PDF of sampling a direction integrates to 1 over the entire
// * sphere of directions.
// */
//TEST(UEDC_IESProfile, sampleWePDF) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], pdf, wPDF;
//	auto uedc = buildTestIESProfile(pool);
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	// samples a random geometry
//	Point3F p;
//	rng->next(&p[0]);
//	Vectr3F n;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &n[0], nullptr);
//	Vectr3F u, v;
//	orthonormalBasis(n, &u, &v);
//	Geometry g(p, u, Norml3F(n));
//	float pdfSum = 0.f;
//	for (int i = 0; true; ++i) {
//		// updates pdfSum
//		Vectr3F w;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &w[0], &pdf);
//		wPDF = uedc->sampleWePDF(g, 540E-9, 0.f, w);
//		ASSERT_LE(0.f, wPDF);
//		pdfSum += wPDF / pdf;
//		// check if reached convergence
//		if (i >= UEDF_MIN_COUNT) {
//			if (abs(1.f - pdfSum / (i + 1)) < UEDF_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///******************************************************************************
// * DELTA DIRECTIONAL UEDC TESTS
// *****************************************************************************/
///*
// * Asserts the delta distribution discriminator.
// */
//TEST(UEDC_Delta, delta) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto uedc = DeltaDirectionalUEDC::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f));
//	ASSERT_TRUE(uedc->isDelta());
//}
//
///*
// * Asserts the total emitted spectral power
// */
//TEST(UEDC_Delta, emission0) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto uedc = DeltaDirectionalUEDC::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f));
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//	ASSERT_NEAR(2.f, uedc->emission0(g, 540E-9, 0.f), 1E-4f);
//}
//
///*
// * Asserts that, as a delta distribution, the directional differential is zero
// * on all sampled directions.
// */
//TEST(UEDC_Delta, emission1) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], pdf, val;
//	auto uedc = DeltaDirectionalUEDC::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f));
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	// samples a random geometry
//	Point3F p;
//	rng->next(&p[0]);
//	Vectr3F n;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &n[0], nullptr);
//	Vectr3F u, v;
//	orthonormalBasis(n, &u, &v);
//	Geometry g(p, u, Norml3F(n));
//	// monte-carlo integration until convergence
//	for (int i = 0; i < UEDF_MIN_COUNT; ++i) {
//		// updates valSum
//		Vectr3F w;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &w[0], &pdf);
//		val = uedc->emission1(g, 540E-9, 0.f, w);
//		ASSERT_EQ(0.f, val);
//	}
//}
//
///*
// * Asserts that the sampled direction always equals the surface normal and that
// * the sampled directional differential and PDF equal one (times an dirac
// * implicit delta).
// */
//TEST(UEDC_Delta, sampleWe) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], pdf, val;
//	auto uedc = DeltaDirectionalUEDC::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f));
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	for (int i = 0; i < UEDF_MIN_COUNT; ++i) {
//		// samples a random geometry
//		Point3F p;
//		rng->next(&p[0]);
//		Vectr3F n;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &n[0], nullptr);
//		Vectr3F u, v;
//		orthonormalBasis(n, &u, &v);
//		Geometry g(p, u, Norml3F(n));
//		// updates valSum
//		Vectr3F w;
//		rng->next(rnd);
//		val = uedc->sampleWe(g, 540E-9, 0.f, rnd, &w, &pdf);
//		ASSERT_EQ(1.f, val);
//		ASSERT_EQ(1.f, pdf);
//		ASSERT_TRIPLET_NEAR(g.gn, w, 1E-4f);
//	}
//}
//
///*
// * Asserts that, as a delta distribution, the PDF of sampling a random
// * direction is always zero.
// */
//TEST(UEDC_Delta, sampleWePDF) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], pdf, wPDF;
//	auto uedc = DeltaDirectionalUEDC::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f));
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	// samples a random geometry
//	Point3F p;
//	rng->next(&p[0]);
//	Vectr3F n;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &n[0], nullptr);
//	Vectr3F u, v;
//	orthonormalBasis(n, &u, &v);
//	Geometry g(p, u, Norml3F(n));
//	for (int i = 0; i < UEDF_MIN_COUNT; ++i) {
//		// updates pdfSum
//		Vectr3F w;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &w[0], &pdf);
//		wPDF = uedc->sampleWePDF(g, 540E-9, 0.f, w);
//		ASSERT_EQ(0.f, wPDF);
//	}
//}
//
///******************************************************************************
// * UEDF TESTS
// *****************************************************************************/
///*
// * Asserts the total emitted spectral power of the UEDF equals the sum of the
// * total emitted spectral power of the components.
// */
//TEST(UEDF, emission0) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto uedc0 = FalloffHemisphericalUEDC::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 8.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 4.f)
//	);
//	auto uedc1 = FalloffHemisphericalUEDC::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 6.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 4.f)
//	);
//	auto uedf_ = UEDF::build(pool, 2);
//	(*uedf_)[0] = uedc0;
//	(*uedf_)[1] = uedc1;
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//	ASSERT_NEAR(
//		uedc0->emission0(g, 540E-9, 0.f) +
//		uedc1->emission0(g, 540E-9, 0.f),
//		uedf_->emission0(g, 540E-9, 0.f),
//		1E-4f
//	);
//}
//
///*
// * Asserts the total emitted directional spectral power of the UEDF equals the
// *  sum of the total emitted directional spectral power of the components.
// */
//TEST(UEDF, emission1Composition) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], pdf;
//	auto uedc0 = FalloffHemisphericalUEDC::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 8.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 4.f)
//	);
//	auto uedc1 = FalloffHemisphericalUEDC::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 6.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 4.f)
//	);
//	auto uedf_ = UEDF::build(pool, 2);
//	(*uedf_)[0] = uedc0;
//	(*uedf_)[1] = uedc1;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	// samples a random geometry
//	Point3F p;
//	rng->next(&p[0]);
//	Vectr3F n;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &n[0], nullptr);
//	Vectr3F u, v;
//	orthonormalBasis(n, &u, &v);
//	Geometry g(p, u, Norml3F(n));
//	// monte-carlo integration until convergence
//	for (int i = 0; i < UEDF_MIN_COUNT; ++i) {
//		// updates valSum
//		Vectr3F w;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &w[0], &pdf);
//		ASSERT_NEAR(
//			uedc0->emission0(g, 540E-9, 0.f) * uedc0->emission1(g, 540E-9, 0.f, w) +
//			uedc1->emission0(g, 540E-9, 0.f) * uedc1->emission1(g, 540E-9, 0.f, w),
//		 	uedf_->emission0(g, 540E-9, 0.f) * uedf_->emission1(g, 540E-9, 0.f, w),
//		 	1E-4f
//		);
//	}
//}
//
///*
// * Asserts the directional differential of the spectral emitted power is zero
// * on the hemisphere opposite to the surface normal and that it integrates to 1
// * on the entire sphere of directions.
// */
//TEST(UEDF, emission1Integration) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], pdf, val;
//	auto uedc0 = FalloffHemisphericalUEDC::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 8.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 4.f)
//	);
//	auto uedc1 = FalloffHemisphericalUEDC::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 6.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 4.f)
//	);
//	auto uedf_ = UEDF::build(pool, 2);
//	(*uedf_)[0] = uedc0;
//	(*uedf_)[1] = uedc1;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	// samples a random geometry
//	Point3F p;
//	rng->next(&p[0]);
//	Vectr3F n;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &n[0], nullptr);
//	Vectr3F u, v;
//	orthonormalBasis(n, &u, &v);
//	Geometry g(p, u, Norml3F(n));
//	// monte-carlo integration until convergence
//	float valSum = 0.f;
//	for (int i = 0; true; ++i) {
//		// updates valSum
//		Vectr3F w;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &w[0], &pdf);
//		val = uedf_->emission1(g, 540E-9, 0.f, w);
//		ASSERT_LE(0.f, val);
//		valSum += val / pdf;
//		// check if reached convergence
//		if (i >= UEDF_MIN_COUNT) {
//			if (abs(1.f - valSum / (i + 1)) < UEDF_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the sampled direction is on the right hemisphere and that the
// * sampled directional differential of emitted power integrates to 1 over the
// * entire sphere of directions and that sampleWe and sampleWePDF methods are
// * consistent with respect to each other.
// */
//TEST(UEDF, sampleWe) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], pdf, val;
//	auto uedc0 = FalloffHemisphericalUEDC::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 8.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 4.f)
//	);
//	auto uedc1 = FalloffHemisphericalUEDC::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 6.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 4.f)
//	);
//	auto uedf_ = UEDF::build(pool, 2);
//	(*uedf_)[0] = uedc0;
//	(*uedf_)[1] = uedc1;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	// samples a random geometry
//	Point3F p;
//	rng->next(&p[0]);
//	Vectr3F n;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &n[0], nullptr);
//	Vectr3F u, v;
//	orthonormalBasis(n, &u, &v);
//	Geometry g(p, u, Norml3F(n));
//	float valSum = 0.f;
//	for (int i = 0; true; ++i) {
//		// updates valSum
//		Vectr3F w;
//		rng->next(rnd);
//		val = uedf_->sampleWe(g, 540E-9, 0.f, rnd, &w, &pdf, nullptr);
//		ASSERT_LE(0.f, val);
//		ASSERT_LT(0.f, pdf);
//		ASSERT_NEAR(1.f, w.lengthSquared(), 1E-4);
//		ASSERT_NEAR(uedf_->emission1(g, 540E-9, 0.f, w), val, 1E-4);
//		ASSERT_NEAR(uedf_->sampleWePDF(g, 540E-9, 0.f, w), pdf, 1E-4);
//		valSum += val / pdf;
//		// check if reached convergence
//		if (i >= UEDF_MIN_COUNT) {
//			if (abs(1.f - valSum / (i + 1)) < UEDF_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the PDF of sampling a direction integrates to 1 over the entire
// * sphere of directions.
// */
//TEST(UEDF, sampleWePDF) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], pdf, wPDF;
//	auto uedc0 = FalloffHemisphericalUEDC::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 8.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 4.f)
//	);
//	auto uedc1 = FalloffHemisphericalUEDC::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 2.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 6.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, PI<float>() / 4.f)
//	);
//	auto uedf_ = UEDF::build(pool, 2);
//	(*uedf_)[0] = uedc0;
//	(*uedf_)[1] = uedc1;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	// samples a random geometry
//	Point3F p;
//	rng->next(&p[0]);
//	Vectr3F n;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &n[0], nullptr);
//	Vectr3F u, v;
//	orthonormalBasis(n, &u, &v);
//	Geometry g(p, u, Norml3F(n));
//	float pdfSum = 0.f;
//	for (int i = 0; true; ++i) {
//		// updates pdfSum
//		Vectr3F w;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &w[0], &pdf);
//		wPDF = uedf_->sampleWePDF(g, 540E-9, 0.f, w);
//		ASSERT_LE(0.f, wPDF);
//		pdfSum += wPDF / pdf;
//		// check if reached convergence
//		if (i >= UEDF_MIN_COUNT) {
//			if (abs(1.f - pdfSum / (i + 1)) < UEDF_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
//}}
//

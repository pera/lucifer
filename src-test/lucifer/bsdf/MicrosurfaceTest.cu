#include <gtest/gtest.h>
#include "../TestUtils.cuh"

#include "lucifer/memory/CPUAllocator.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/random/CPUBasicRNG.cuh"
#include "lucifer/random/UniformHemisphereDistribution.cuh"

#include "lucifer/bsdf/multiple-scattering/ConductorMicrosurfaceBRDF.cuh"

namespace lucifer { namespace test {

constexpr int BxDF_SAMPLES = 500;

TEST(BxDF_ConductorMicrosurface, type) {
	MemoryPool pool{4, new CPUAllocator{}};
	auto bxdf = ConductorMicrosurfaceBRDF::build(pool, 1.f, 1.f);
	ASSERT_TRUE(bxdf->isOpticallyInteracting());
	ASSERT_TRUE(bxdf->matches(BxDF::Type::DIFFUSE));
	ASSERT_TRUE(bxdf->matches(BxDF::Type::REFLECTIVE));
	ASSERT_FALSE(bxdf->matches(BxDF::Type::SPECULAR));
	ASSERT_FALSE(bxdf->matches(BxDF::Type::REFRACTIVE));
}

TEST(BxDF_ConductorMicrosurface, evaluate) {
	// creates the bxdf
	MemoryPool pool{4, new CPUAllocator{}};
	auto bxdf = ConductorMicrosurfaceBRDF::build(pool, 1.f, 1.f);

	// geometric setting
	ComplexF ior1(1.0f, 0.f);
	ComplexF ior2(1.5f, 0.f);
	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
	auto rng = CPUBasicRNG<>::build(pool, 3, 0);
	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));

	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
			Vectr3F wo = Vectr3F{1.f, 1.f, 1.f}.normalize();//sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));

			// integrates radiance value over all `wi`
			Vectr3F wi;
			float integral = 0.f;
			for (int i = 0; i < BxDF_SAMPLES; ++i) {
				for (int j = 0; j < BxDF_SAMPLES; ++j) {
					// samples random `wi`
					rng->next(rnd);
					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
					UniformHemisphereDistribution::sample(rnd, &wi[0], &pdf);

					// asserts evaluated value
					val = bxdf->evaluate(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE);
					ASSERT_FALSE(isnan(val));
					ASSERT_FALSE(isinf(val));
					ASSERT_LE(0.f, val);
					if (!sameHemisphere(wo, wi)) ASSERT_EQ(0.f, val);

					// updates integral
					integral += val /** abs(wi[2])*/ / pdf;
				}
			}
			// asseerts integral
			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
			ASSERT_NEAR(1.f, integral, 0.f);
//		}
//	}
}

}}

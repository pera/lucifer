//#include <iostream>
//#include <gtest/gtest.h>
//#include "../TestUtils.cuh"
//#include "lucifer/bsdf/microfacet/TrowbridgeReitzDistribution.cuh"
//#include "lucifer/field/spectral/ConstantSpectralField.cuh"
//#include "lucifer/math/Math.cuh"
//#include "lucifer/math/function/Constant.cuh"
//#include "lucifer/random/UniformSphereDistribution.cuh"
//#include "lucifer/random/generator/PlainRNG.cuh"
//#include "lucifer/memory/Pointer.cuh"
//
//namespace lucifer { namespace test {
//
//constexpr int MICROFACET_MIN_COUNT = 10000;
//constexpr float MICROFACET_CONVERGENCE_ERROR = 1E-3f;
//
///******************************************************************************
// * TROWBRIDGE-REITZ DISTRIBUTION TESTS
// *****************************************************************************/
//
///*
// * Asserts that the microfacet distribution is non negative and properly
// * normalized.
// */
//TEST(Microfacet_TrowbridgeReitz, distribution) {
//	float rnd[3], val, pdf;
//	Pointer<RNG> rng = PlainRNGFactory<>().build(3);
//	Pointer<MicrofacetDistribution> d = new TrowbridgeReitzDistribution(
//		new ConstantSpectralField(new Constant<const float>(0.25f)),
//		new ConstantSpectralField(new Constant<const float>(0.75f))
//	);
//	float wlen = 540E-9;
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//	float valSum = 0.f;
//	for (int i = 0; i < true; i++) {
//		Vectr3F wh;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wh.x, &pdf);
//		val = d->D(g, wh, wlen);
//		ASSERT_LE(0.f, val);
//		valSum += val * absDot(g.gn, wh) / pdf;
//		if (i >= MICROFACET_MIN_COUNT) {
//			if (abs(1.f - valSum / (i + 1)) < MICROFACET_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the PDF of sampling a random half-vector wh over the entire
// * sphere of directions is properly normalized.
// */
//TEST(Microfacet_TrowbridgeReitz, pdf) {
//	float rnd[3], val, pdf;
//	Pointer<RNG> rng = PlainRNGFactory<>().build(3);
//	Pointer<MicrofacetDistribution> d = new TrowbridgeReitzDistribution(
//		new ConstantSpectralField(new Constant<const float>(0.25f)),
//		new ConstantSpectralField(new Constant<const float>(0.75f))
//	);
//	float wlen = 540E-9;
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//	float valSum = 0.f;
//	Vectr3F wo;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &wo.x, &pdf);
//	for (int i = 0; i < true; i++) {
//		Vectr3F wh;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wh.x, &pdf);
//		val = d->sampleWhPDF(g, wlen, wo, wh);
//		ASSERT_LE(0.f, val);
//		if (!sameHemisphere(wo, wh)) ASSERT_EQ(0.f, val);
//		valSum += val / pdf;
//		if (i >= MICROFACET_MIN_COUNT) {
//			if (abs(1.f - valSum / (i + 1)) < MICROFACET_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the sampling method is consistent.
// */
//TEST(Microfacet_TrowbridgeReitz, sample) {
//	float rnd[3], pdf;
//	Pointer<RNG> rng = PlainRNGFactory<>().build(3);
//	Pointer<MicrofacetDistribution> d = new TrowbridgeReitzDistribution(
//		new ConstantSpectralField(new Constant<const float>(0.25f)),
//		new ConstantSpectralField(new Constant<const float>(0.75f))
//	);
//	float wlen = 540E-9;
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//	for (int i = 0; i < MICROFACET_MIN_COUNT; i++) {
//		Vectr3F wo;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo.x, &pdf);
//		Vectr3F wh;
//		rng->next(rnd);
//		d->sampleWh(rnd, g, wlen, wo, &wh, &pdf);
//		ASSERT_LE(0.f, pdf);
//		ASSERT_NEAR(d->sampleWhPDF(g, wlen, wo, wh), pdf, 1E-4);
//		ASSERT_TRUE(sameHemisphere(wo, wh));
//	}
//}
//
//}}

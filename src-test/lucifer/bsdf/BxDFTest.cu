//#include <gtest/gtest.h>
//#include "../TestUtils.cuh"
//#include "lucifer/bsdf/microfacet/SmithTrowbridgeReitzModel.cuh"
//#include "lucifer/bsdf/InvisibleBTDF.cuh"
//#include "lucifer/bsdf/LambertianBRDF.cuh"
//#include "lucifer/bsdf/LambertianBTDF.cuh"
//#include "lucifer/bsdf/MicrofacetBRDF.cuh"
//#include "lucifer/bsdf/MicrofacetBTDF.cuh"
//#include "lucifer/bsdf/MicrofacetBSDF.cuh"
//#include "lucifer/bsdf/OrenNayarBRDF.cuh"
//#include "lucifer/bsdf/SpecularBRDF.cuh"
//#include "lucifer/bsdf/SpecularBTDF.cuh"
//#include "lucifer/bsdf/SpecularBSDF.cuh"
//#include "lucifer/bsdf/SubstrateBRDF.cuh"
//#include "lucifer/math/function/Constant.cuh"
//#include "lucifer/node/ConstantNode.cuh"
//#include "lucifer/random/generator/PlainRNG.cuh"
//#include "lucifer/random/UniformSphereDistribution.cuh"
//#include "lucifer/memory/CPUAllocator.cuh"
//#include "lucifer/memory/MemoryPool.cuh"
//#include "lucifer/memory/Pointer.cuh"
//
//namespace lucifer { namespace test {
//
//constexpr int BxDF_SAMPLES = 500;
//constexpr float BxDF_ERROR = 1E-2f;
//
///******************************************************************************
// * INVISIBLE BTDF TESTS
// *****************************************************************************/
///*
// * Asserts that the BxDF is correctly tagged as non interacting.
// */
//TEST(BxDF_Invisible, interacting) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = InvisibleBTDF::build(pool);
//	ASSERT_FALSE(bxdf->isOpticallyInteracting());
//}
//
///*
// * Asserts that the BxDF is tagged with the correc types.
// */
//TEST(BxDF_Invisible, type) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = InvisibleBTDF::build(pool);
//	ASSERT_TRUE (bxdf->matches(BxDF::Type::SPECULAR  ));
//	ASSERT_TRUE (bxdf->matches(BxDF::Type::REFRACTIVE));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::DIFFUSE   ));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::REFLECTIVE));
//}
//
///*
// * Asserts that, as a delta distribution, the BxDF is zero at any random pair
// * of outgoing and incoming directions.
// */
//TEST(BxDF_Invisible, evaluate) {
//	float rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = InvisibleBTDF::build(pool);
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	// asserts evaluated values
//	for (int i = 0; i < BxDF_SAMPLES; ++i) {
//		// samples a random position
//		Point3F p;
//		rng->next(&p[0]);
//		Geometry g(p, Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//		// samples random wi and wo directions
//		Vectr3F wi, wo;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wi[0], nullptr);
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], nullptr);
//		// asserts evaluated values
//		ASSERT_EQ(0.f, bxdf->evaluate(g, 540E-9, 0.f, wi, wo, ComplexF(1.f, 0.f), ComplexF(1.f, 0.f), BxDF::Mode::RADIANCE  ));
//		ASSERT_EQ(0.f, bxdf->evaluate(g, 540E-9, 0.f, wi, wo, ComplexF(1.f, 0.f), ComplexF(1.f, 0.f), BxDF::Mode::IMPORTANCE));
//	}
//}
//
///*
// * Asserts that the sampled direction and the BTDF value is correct.
// */
//TEST(BxDF_Invisible, sample) {
//	float rnd[3], val, pdf;
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = InvisibleBTDF::build(pool);
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	for (int i = 0; i < BxDF_SAMPLES; ++i) {
//		// samples a random position
//		Point3F p;
//		rng->next(&p[0]);
//		Geometry g(p, Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//		// samples a outgoing direction
//		Vectr3F wi, wo;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], nullptr);
//		// asserts the sampled radiance direction and BTDF value
//		rng->next(rnd);
//		val = bxdf->sampleWi(g, 540E-9, 0.f, wo, ComplexF(1.f, 0.f), ComplexF(1.f, 0.f), BxDF::Mode::RADIANCE,  rnd, &wi, &pdf);
//		ASSERT_EQ(1.f, pdf);
//		ASSERT_NEAR(1.f, wi.length(), 1E-4);
//		ASSERT_NEAR(1.f, val * absDot(g.gn, wi), 1E-4);
//		ASSERT_TRIPLET_NEAR(-wo, wi, 1E-4);
//		// asserts the sampled importance direction and BTDF value
//		rng->next(rnd);
//		val = bxdf->sampleWi(g, 540E-9, 0.f, wo, ComplexF(1.f, 0.f), ComplexF(1.f, 0.f), BxDF::Mode::IMPORTANCE, rnd, &wi, &pdf);
//		ASSERT_EQ(1.f, pdf);
//		ASSERT_NEAR(1.f, wi.length(), 1E-4);
//		ASSERT_NEAR(1.f, val * absDot(g.gn, wi), 1E-4);
//		ASSERT_TRIPLET_NEAR(-wo, wi, 1E-4);
//	}
//}
//
///*
// * Asserts that the sampling PDF is correct.
// */
//TEST(BxDF_Invisible, pdf) {
//	float rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = InvisibleBTDF::build(pool);
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	for (int i = 0; i < BxDF_SAMPLES; ++i) {
//			// samples a random position
//			Point3F p;
//			rng->next(&p[0]);
//			Geometry g(p, Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//			// samples a outgoing direction
//			Vectr3F wo;
//			rng->next(rnd);
//			UniformSphereDistribution::sample(rnd, &wo[0], nullptr);
//			// asserts the sampling PDF
//			ASSERT_EQ(0.f, bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, -wo, ComplexF(1.f, 0.f), ComplexF(1.f, 0.f), BxDF::Mode::RADIANCE,   false));
//			ASSERT_EQ(1.f, bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, -wo, ComplexF(1.f, 0.f), ComplexF(1.f, 0.f), BxDF::Mode::RADIANCE,   true ));
//			ASSERT_EQ(0.f, bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, -wo, ComplexF(1.f, 0.f), ComplexF(1.f, 0.f), BxDF::Mode::IMPORTANCE, false));
//			ASSERT_EQ(1.f, bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, -wo, ComplexF(1.f, 0.f), ComplexF(1.f, 0.f), BxDF::Mode::IMPORTANCE, true ));
//	}
//}
//
///******************************************************************************
// * SPECULAR BRDF TESTS
// *****************************************************************************/
///*
// * Asserts that the BxDF is correctly tagged as interacting.
// */
//TEST(BxDF_SpecularBRDF, interacting) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = SpecularBRDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f), true);
//	ASSERT_TRUE(bxdf->isOpticallyInteracting());
//}
//
///*
// * Asserts that the BxDF is tagged with the correc types.
// */
//TEST(BxDF_SpecularBRDF, type) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = SpecularBRDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f), true);
//	ASSERT_TRUE (bxdf->matches(BxDF::Type::SPECULAR  ));
//	ASSERT_TRUE (bxdf->matches(BxDF::Type::REFLECTIVE));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::DIFFUSE   ));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::REFRACTIVE));
//}
//
///*
// * Asserts that, as a delta distribution, the BxDF is zero at any random pair
// * of outgoing and incoming directions.
// */
//TEST(BxDF_SpecularBRDF, evaluate) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	auto bxdf = SpecularBRDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f), true);
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	ComplexF ior1(1.0, 0.f);
//	ComplexF ior2(1.5, 0.f);
//	// asserts evaluated values
//	for (int i = 0; i < BxDF_SAMPLES; ++i) {
//		// samples a random position
//		Point3F p;
//		rng->next(&p[0]);
//		Geometry g(p, Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//		// samples random wi and wo directions
//		Vectr3F wi, wo;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wi[0], nullptr);
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], nullptr);
//		// asserts evaluated values
//		ASSERT_EQ(0.f, bxdf->evaluate(g, 540E-9, 0.f, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE  ));
//		ASSERT_EQ(0.f, bxdf->evaluate(g, 540E-9, 0.f, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE));
//	}
//}
//
///*
// * Asserts that the sampled direction and the BTDF value is correct.
// */
//TEST(BxDF_SpecularBRDF, sample) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], val, pdf;
//	ComplexF ior1(1.0, 0.f);
//	ComplexF ior2(1.5, 0.f);
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	// no fresnel
//	{
//		auto bxdf = SpecularBRDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f), false);
//		for (int i = 0; i < BxDF_SAMPLES; ++i) {
//			// samples a random position
//			Point3F p;
//			rng->next(&p[0]);
//			Geometry g(p, Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//			// samples a outgoing direction
//			Vectr3F wi, wo;
//			rng->next(rnd);
//			UniformSphereDistribution::sample(rnd, &wo[0], nullptr);
//			// asserts the sampled radiance direction and BTDF value
//			rng->next(rnd);
//			val = bxdf->sampleWi(g, 540E-9, 0.f, wo, ior1, ior2,  BxDF::Mode::RADIANCE, rnd, &wi, &pdf);
//			ASSERT_EQ(1.f, pdf);
//			ASSERT_NEAR(1.f, wi.length(), 1E-4);
//			ASSERT_NEAR(1.f, val * absDot(g.gn, wi), 1E-4);
//			ASSERT_TRIPLET_NEAR(reflect(wo, Vectr3F(g.gn)), wi, 1E-4);
//			// asserts the sampled importance direction and BTDF value
//			rng->next(rnd);
//			val = bxdf->sampleWi(g, 540E-9, 0.f, wo, ior1, ior2, BxDF::Mode::IMPORTANCE, rnd, &wi, &pdf);
//			ASSERT_EQ(1.f, pdf);
//			ASSERT_NEAR(1.f, wi.length(), 1E-4);
//			ASSERT_NEAR(1.f, val * absDot(g.gn, wi), 1E-4);
//			ASSERT_TRIPLET_NEAR(reflect(wo, Vectr3F(g.gn)), wi, 1E-4);
//		}
//	}
//	// fresnel
//	{
//		auto bxdf = SpecularBRDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f), true);
//		for (int i = 0; i < BxDF_SAMPLES; ++i) {
//			// samples a random position
//			Point3F p;
//			rng->next(&p[0]);
//			Geometry g(p, Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//			// samples a outgoing direction
//			Vectr3F wi, wo;
//			rng->next(rnd);
//			UniformSphereDistribution::sample(rnd, &wo[0], nullptr);
//			// asserts the sampled radiance direction and BTDF value
//			rng->next(rnd);
//			val = bxdf->sampleWi(g, 540E-9, 0.f, wo, ior1, ior2,  BxDF::Mode::RADIANCE, rnd, &wi, &pdf);
//			ASSERT_EQ(1.f, pdf);
//			ASSERT_GE(1.f, val * absDot(g.gn, wi));
//			ASSERT_TRIPLET_NEAR(reflect(wo, Vectr3F(g.gn)), wi, 1E-4);
//			// asserts the sampled importance direction and BTDF value
//			rng->next(rnd);
//			val = bxdf->sampleWi(g, 540E-9, 0.f, wo, ior1, ior2, BxDF::Mode::IMPORTANCE, rnd, &wi, &pdf);
//			ASSERT_EQ(1.f, pdf);
//			ASSERT_GE(1.f, val * absDot(g.gn, wi));
//			ASSERT_TRIPLET_NEAR(reflect(wo, Vectr3F(g.gn)), wi, 1E-4);
//		}
//	}
//}
//
///*
// * Asserts that the sampling PDF is correct.
// */
//TEST(BxDF_SpecularBRDF, pdf) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	auto bxdf = SpecularBRDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f), true);
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	ComplexF ior1(1.0, 0.f);
//	ComplexF ior2(1.5, 0.f);
//	for (int i = 0; i < BxDF_SAMPLES; ++i) {
//		// samples a random position
//		Point3F p;
//		rng->next(&p[0]);
//		Geometry g(p, Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//		// samples a outgoing direction
//		Vectr3F wo;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], nullptr);
//		// asserts the sampling PDF
//		Vectr3F wi = reflect(wo, Vectr3F(g.gn));
//		ASSERT_EQ(0.f, bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE,   false));
//		ASSERT_EQ(1.f, bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE,   true ));
//		ASSERT_EQ(0.f, bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, false));
//		ASSERT_EQ(1.f, bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, true ));
//	}
//}
//
///******************************************************************************
// * SPECULAR BTDF TESTS
// *****************************************************************************/
///*
// * Asserts that the BxDF is correctly tagged as interacting.
// */
//TEST(BxDF_SpecularBTDF, interacting) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = SpecularBTDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f), true);
//	ASSERT_TRUE(bxdf->isOpticallyInteracting());
//}
//
///*
// * Asserts that the BxDF is tagged with the correc types.
// */
//TEST(BxDF_SpecularBTDF, type) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = SpecularBTDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f), true);
//	ASSERT_TRUE (bxdf->matches(BxDF::Type::SPECULAR  ));
//	ASSERT_TRUE (bxdf->matches(BxDF::Type::REFRACTIVE));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::DIFFUSE   ));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::REFLECTIVE));
//}
//
///*
// * Asserts that, as a delta distribution, the BxDF is zero at any random pair
// * of outgoing and incoming directions.
// */
//TEST(BxDF_SpecularBTDF, evaluate) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	auto bxdf = SpecularBTDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f), true);
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	ComplexF ior1(1.0, 0.f);
//	ComplexF ior2(1.5, 0.f);
//	// asserts evaluated values
//	for (int i = 0; i < BxDF_SAMPLES; ++i) {
//		// samples a random position
//		Point3F p;
//		rng->next(&p[0]);
//		Geometry g(p, Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//		// samples random wi and wo directions
//		Vectr3F wi, wo;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wi[0], nullptr);
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], nullptr);
//		// asserts evaluated values
//		ASSERT_EQ(0.f, bxdf->evaluate(g, 540E-9, 0.f, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE  ));
//		ASSERT_EQ(0.f, bxdf->evaluate(g, 540E-9, 0.f, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE));
//	}
//}
//
///*
// * Asserts that the sampled direction and the BTDF value is correct.
// */
//TEST(BxDF_SpecularBTDF, sample) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], val, pdf;
//	ComplexF ior1(1.0, 0.f);
//	ComplexF ior2(1.5, 0.f);
//	float eta = ior1.re / ior2.re;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	// no fresnel
//	{
//		auto bxdf = SpecularBTDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f), false);
//		for (int i = 0; i < BxDF_SAMPLES; ++i) {
//			// samples a random position
//			Point3F p;
//			rng->next(&p[0]);
//			Geometry g(p, Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//			// samples a outgoing direction
//			Vectr3F wi, wo;
//			rng->next(rnd);
//			UniformSphereDistribution::sample(rnd, &wo[0], nullptr);
//			// asserts the sampled radiance direction and BTDF value
//			Vectr3F r;
//			rng->next(rnd);
//			val = bxdf->sampleWi(g, 540E-9, 0.f, wo, ior1, ior2, BxDF::Mode::RADIANCE, rnd, &wi, &pdf);
//			ASSERT_NEAR(1.f, wi.length(), 1E-4);
//			if (val > 0.f) {
//				// actual refraction
//				ASSERT_EQ(1.f, pdf);
//				ASSERT_TRUE(refract(wo, eta, &r));
//				ASSERT_FALSE(sameHemisphere(g.gn, wo, wi));
//				ASSERT_NEAR(1.f, val * absDot(g.gn, wi), 1E-4);
//				ASSERT_TRIPLET_NEAR(r, wi, 1E-4);
//			} else {
//				// total internal reflection
//				ASSERT_EQ(1.f, pdf);
//				ASSERT_FALSE(refract(wo, Vectr3F(g.gn), eta, &r));
//			}
//			rng->next(rnd);
//			val = bxdf->sampleWi(g, 540E-9, 0.f, wo, ior1, ior2, BxDF::Mode::IMPORTANCE, rnd, &wi, &pdf);
//			ASSERT_NEAR(1.f, wi.length(), 1E-4);
//			// asserts the sampled importance direction and BTDF value
//			if (val > 0.f) {
//				// actual refraction
//				ASSERT_EQ(1.f, pdf);
//				ASSERT_TRUE(refract(wo, eta, &r));
//				ASSERT_FALSE(sameHemisphere(g.gn, wo, wi));
//				ASSERT_NEAR(1.f * eta * eta, val * absDot(g.gn, wi), 1E-4);
//				ASSERT_TRIPLET_NEAR(r, wi, 1E-4);
//			} else {
//				// total internal reflection
//				ASSERT_EQ(1.f, pdf);
//				ASSERT_FALSE(refract(wo, Vectr3F(g.gn), eta, &r));
//			}
//		}
//	}
//	// fresnel
//	{
//		auto bxdf = SpecularBTDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f), true);
//		for (int i = 0; i < BxDF_SAMPLES; ++i) {
//			// samples a random position
//			Point3F p;
//			rng->next(&p[0]);
//			Geometry g(p, Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//			// samples a outgoing direction
//			Vectr3F wi, wo;
//			rng->next(rnd);
//			UniformSphereDistribution::sample(rnd, &wo[0], nullptr);
//			// asserts the sampled radiance direction and BTDF value
//			Vectr3F r;
//			rng->next(rnd);
//			val = bxdf->sampleWi(g, 540E-9, 0.f, wo, ior1, ior2, BxDF::Mode::RADIANCE, rnd, &wi, &pdf);
//			ASSERT_NEAR(1.f, wi.length(), 1E-4);
//			// asserts the sampled importance direction and BTDF value
//			if (val > 0.f) {
//				// actual refraction
//				ASSERT_EQ(1.f, pdf);
//				ASSERT_TRUE(refract(wo, eta, &r));
//				ASSERT_GE(1.f, val * absDot(g.gn, wi));
//				ASSERT_TRIPLET_NEAR(r, wi, 1E-4);
//			} else {
//				// total internal reflection
//				ASSERT_EQ(1.f, pdf);
//				ASSERT_FALSE(refract(wo, Vectr3F(g.gn), eta, &r));
//			}
//			rng->next(rnd);
//			val = bxdf->sampleWi(g, 540E-9, 0.f, wo, ior1, ior2, BxDF::Mode::IMPORTANCE, rnd, &wi, &pdf);
//			ASSERT_NEAR(1.f, wi.length(), 1E-4);
//			// asserts the sampled importance direction and BTDF value
//			if (val > 0.f) {
//				// actual refraction
//				ASSERT_EQ(1.f, pdf);
//				ASSERT_TRUE(refract(wo, eta, &r));
//				ASSERT_GE(eta * eta, val * absDot(g.gn, wi));
//				ASSERT_TRIPLET_NEAR(r, wi, 1E-4);
//			} else {
//				// total internal reflection
//				ASSERT_EQ(1.f, pdf);
//				ASSERT_FALSE(refract(wo, Vectr3F(g.gn), eta, &r));
//			}
//		}
//	}
//}
//
///*
// * Asserts that the sampling PDF is correct.
// */
//TEST(BxDF_SpecularBTDF, pdf) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	auto bxdf = SpecularBTDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f), true);
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	ComplexF ior1(1.0, 0.f);
//	ComplexF ior2(1.5, 0.f);
//	float eta = ior1.re / ior2.re;
//	for (int i = 0; i < BxDF_SAMPLES; ++i) {
//		// samples a random position
//		Point3F p;
//		rng->next(&p[0]);
//		Geometry g(p, Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//		// samples a outgoing direction
//		Vectr3F wo;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], nullptr);
//		// asserts the sampling PDF
//		Vectr3F wi;
//		if (refract(wo, eta, &wi)) {
//			ASSERT_EQ(0.f, bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE,   false));
//			ASSERT_EQ(1.f, bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE,   true) );
//			ASSERT_EQ(0.f, bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, false));
//			ASSERT_EQ(1.f, bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, true) );
//		}
//	}
//}
//
///******************************************************************************
// * SPECULAR BSDF TESTS
// *****************************************************************************/
///*
// * Asserts that the BxDF is correctly tagged as interacting.
// */
//TEST(BxDF_SpecularBSDF, interacting) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = SpecularBSDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f)
//	);
//	ASSERT_TRUE(bxdf->isOpticallyInteracting());
//}
//
///*
// * Asserts that the BxDF is tagged with the correc types.
// */
//TEST(BxDF_SpecularBSDF, type) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = SpecularBSDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f)
//	);
//	ASSERT_TRUE (bxdf->matches(BxDF::Type::SPECULAR  ));
//	ASSERT_TRUE (bxdf->matches(BxDF::Type::REFRACTIVE));
//	ASSERT_TRUE (bxdf->matches(BxDF::Type::REFLECTIVE));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::DIFFUSE   ));
//}
//
///*
// * Asserts that, as a delta distribution, the BxDF is zero at any random pair
// * of outgoing and incoming directions.
// */
//TEST(BxDF_SpecularBSDF, evaluate) {
//	float rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = SpecularBSDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f)
//	);
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	ComplexF ior1(1.0, 0.f);
//	ComplexF ior2(1.5, 0.f);
//	// asserts evaluated values
//	for (int i = 0; i < BxDF_SAMPLES; ++i) {
//		// samples a random position
//		Point3F p;
//		rng->next(&p[0]);
//		Geometry g(p, Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//		// samples random wi and wo directions
//		Vectr3F wi, wo;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wi[0], nullptr);
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], nullptr);
//		// asserts evaluated values
//		ASSERT_EQ(0.f, bxdf->evaluate(g, 540E-9, 0.f, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE  ));
//		ASSERT_EQ(0.f, bxdf->evaluate(g, 540E-9, 0.f, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE));
//	}
//}
//
///*
// * Asserts the BxDF integral over the entire sphere of incoming directions.
// */
//TEST(BxDF_SpecularBSDF, sample) {
//	float rnd[3], val, pdf, sPDF;
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = SpecularBSDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f)
//	);
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	ComplexF ior1(1.0, 0.f);
//	ComplexF ior2(1.5, 0.f);
//	float eta = ior1.re / ior2.re;
//	// samples a random position
//	Point3F p;
//	rng->next(&p[0]);
//	Geometry g(p, Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//	// samples a random outgoing direction, without total internal reflection
//	Vectr3F wi, wo;
//	do {
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], &sPDF);
//	} while (!refract(wo, eta, &wi));
//	// monte carlo integrates the bxdf radiance over all incoming directions
//	float valSum = 0.f;
//	for (int i = 0; true; ++i) {
//		rng->next(rnd);
//		val = bxdf->sampleWi(g, 540E-9, 0.f, wo, ior1, ior2, BxDF::Mode::RADIANCE, rnd, &wi, &pdf);
//		ASSERT_LE(0.f, val);
//		ASSERT_LE(0.f, pdf);
//		ASSERT_NEAR(1.f, wi.length(), 1E-4);
//		ASSERT_NEAR(bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, true), pdf, 1E-4f);
//		valSum += val * absDot(g.gn, wi) / pdf;
//		if (i >= BxDF_SAMPLES) {
//			if (abs(1.f - valSum / (i + 1)) < BxDF_ERROR) {
//				break;
//			}
//		}
//	}
//	// monte carlo integrates the bxdf importance over all incoming directions
//	valSum = 0.f;
//	for (int i = 0; true; ++i) {
//		rng->next(rnd);
//		val = bxdf->sampleWi(g, 540E-9, 0.f, wo, ior1, ior2, BxDF::Mode::IMPORTANCE, rnd, &wi, &pdf);
//		ASSERT_LE(0.f, val);
//		ASSERT_LE(0.f, pdf);
//		ASSERT_NEAR(1.f, wi.length(), 1E-4);
//		ASSERT_NEAR(bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, true), pdf, 1E-4f);
//		valSum += val * absDot(g.gn, wi) / (pdf * (sameHemisphere(wi, wo) ? 1 : eta * eta));
//		if (i >= BxDF_SAMPLES) {
//			if (abs(1.f - valSum / (i + 1)) < BxDF_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the sampling PDF is correct.
// */
//TEST(BxDF_SpecularBSDF, pdf) {
//	float rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = SpecularBSDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f)
//	);
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	ComplexF ior1(1.0, 0.f);
//	ComplexF ior2(1.5, 0.f);
//	float eta = ior1.re / ior2.re;
//	for (int i = 0; i < BxDF_SAMPLES; ++i) {
//		// samples a random position
//		Point3F p;
//		rng->next(&p[0]);
//		Geometry g(p, Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//		// samples a outgoing direction
//		Vectr3F wo;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], nullptr);
//		// asserts the sampling PDF
//		Vectr3F wr = reflect(wo, Vectr3F(g.gn));
//		float radPDF0 = bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, wr, ior1, ior2, BxDF::Mode::RADIANCE,   false);
//		float radPDF1 = bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, wr, ior1, ior2, BxDF::Mode::RADIANCE,   true );
//		float impPDF0 = bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, wr, ior1, ior2, BxDF::Mode::IMPORTANCE, false);
//		float impPDF1 = bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, wr, ior1, ior2, BxDF::Mode::IMPORTANCE, true );
//		Vectr3F wt;
//		if (refract(wo, eta, &wt)) {
//			radPDF0 += bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, wt, ior1, ior2, BxDF::Mode::RADIANCE,   false);
//			radPDF1 += bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, wt, ior1, ior2, BxDF::Mode::RADIANCE,   true );
//			impPDF0 += bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, wt, ior1, ior2, BxDF::Mode::IMPORTANCE, false);
//			impPDF1 += bxdf->sampleWiPDF(g, 540E-9, 0.f, wo, wt, ior1, ior2, BxDF::Mode::IMPORTANCE, true );
//		}
//		ASSERT_NEAR(0.f, radPDF0, 1E-4f);
//		ASSERT_NEAR(1.f, radPDF1, 1E-4f);
//		ASSERT_NEAR(0.f, impPDF0, 1E-4f);
//		ASSERT_NEAR(1.f, impPDF1, 1E-4f);
//	}
//}
//
///******************************************************************************
// * LAMBERTIAN BRDF TESTS
// *****************************************************************************/
///*
// * Asserts that the bxdf is correctly typed.
// */
//TEST(BxDF_LambertianBRDF, type) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = LambertianBRDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f));
//	ASSERT_TRUE(bxdf->isOpticallyInteracting());
//	ASSERT_TRUE(bxdf->matches(BxDF::Type::DIFFUSE));
//	ASSERT_TRUE(bxdf->matches(BxDF::Type::REFLECTIVE));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::SPECULAR));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::REFRACTIVE));
//}
//
///*
// * Asserts that the bxdf value is non negative, is zero when `wi` and `wo` are
// * on opposite hemispheres and that for every `wo` the integral over all `wi`
// * is not greater than the especified reflectance.
// */
//TEST(BxDF_LambertianBRDF, evaluate) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = LambertianBRDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f));
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// integrates radiance value over all `wi`
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts evaluated value
//					val = bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//					if (!sameHemisphere(wo, wi)) ASSERT_EQ(0.f, val);
//
//					// updates integral
//					integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + BxDF_ERROR, integral);
//
//			// integrates importance value over all `wi`
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts evaluated value
//					val = bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//					if (!sameHemisphere(wo, wi)) ASSERT_EQ(0.f, val);
//
//					// updates integral
//					integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + BxDF_ERROR, integral);
//		}
//	}
//}
//
///*
// * Asserts that the pdf of sampling an incominngo direction `wi` given an
// * outgoing direction `wo` is non negative and integrates to 1 over the entire
// * hemisphere of incoming directions.
// */
//TEST(BxDF_LambertianBRDF, pdf) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = LambertianBRDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f));
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// integrates radiance pdf over all `wi`
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts the pdf
//					val = bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, false);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					if (pdf > 0.f) integral += val / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_NEAR(1.f, integral, BxDF_ERROR);
//
//			// integrates importance pdf over all `wi`
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts the pdf
//					val = bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, false);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					if (pdf > 0.f) integral += val / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_NEAR(1.f, integral, BxDF_ERROR);
//		}
//	}
//}
//
///*
// * Asserts that sampling has consistent value and pdf, that the sampled vector
// * is properly normalized and that the sampled radiance and importance
// * integrate to the correct values.
// */
//TEST(BxDF_LambertianBRDF, sample) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = LambertianBRDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f));
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// asserts radiance sampling along the entire domain
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					val = bxdf->sampleWi(g, wlen, time, wo, ior1, ior2, BxDF::Mode::RADIANCE, rnd, &wi, &pdf);
//
//					// asserts consistency
//					ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//					ASSERT_RELATIVE(bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE), val, 1E-2f);
//					ASSERT_RELATIVE(bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, true), pdf, 1E-2f);
//
//					// updates integral
//					if (pdf > 0.f) integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + 1E-2, integral);
//
//			// asserts importance sampling along the entire domain
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					val = bxdf->sampleWi(g, wlen, time, wo, ior1, ior2, BxDF::Mode::IMPORTANCE, rnd, &wi, &pdf);
//
//					// asserts consistency
//					ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//					ASSERT_RELATIVE(bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE), val, 1E-2f);
//					ASSERT_RELATIVE(bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, true), pdf, 1E-2f);
//
//					// updates integral
//					if (pdf > 0.f) integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + 1E-2, integral);
//		}
//	}
//}
//
///******************************************************************************
// * LAMBERTIAN BTDF TESTS
// *****************************************************************************/
///*
// * Asserts that the bxdf is correctly typed.
// */
//TEST(BxDF_LambertianBTDF, type) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = LambertianBTDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f));
//	ASSERT_TRUE(bxdf->isOpticallyInteracting());
//	ASSERT_TRUE(bxdf->matches(BxDF::Type::DIFFUSE));
//	ASSERT_TRUE(bxdf->matches(BxDF::Type::REFRACTIVE));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::SPECULAR));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::REFLECTIVE));
//}
//
///*
// * Asserts that the bxdf value is non negative, is zero when `wi` and `wo` are
// * on opposite hemispheres and that for every `wo` the integral over all `wi`
// * is not greater than the especified reflectance.
// */
//TEST(BxDF_LambertianBTDF, evaluate) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = LambertianBTDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f));
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// integrates radiance value over all `wi`
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts evaluated value
//					val = bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//					if (sameHemisphere(wo, wi)) ASSERT_EQ(0.f, val);
//
//					// updates integral
//					integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + BxDF_ERROR, integral);
//
//			// integrates importance value over all `wi`
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts evaluated value
//					val = bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//					if (sameHemisphere(wo, wi)) ASSERT_EQ(0.f, val);
//
//					// updates integral
//					integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + BxDF_ERROR, integral);
//		}
//	}
//}
//
///*
// * Asserts that the pdf of sampling an incominngo direction `wi` given an
// * outgoing direction `wo` is non negative and integrates to 1 over the entire
// * hemisphere of incoming directions.
// */
//TEST(BxDF_LambertianBTDF, pdf) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = LambertianBTDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f));
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// integrates radiance pdf over all `wi`
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts the pdf
//					val = bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, false);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					if (pdf > 0.f) integral += val / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_NEAR(1.f, integral, BxDF_ERROR);
//
//			// integrates importance pdf over all `wi`
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts the pdf
//					val = bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, false);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					if (pdf > 0.f) integral += val / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_NEAR(1.f, integral, BxDF_ERROR);
//		}
//	}
//}
//
///*
// * Asserts that sampling has consistent value and pdf, that the sampled vector
// * is properly normalized and that the sampled radiance and importance
// * integrate to the correct values.
// */
//TEST(BxDF_LambertianBTDF, sample) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = LambertianBTDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f));
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// asserts radiance sampling along the entire domain
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					val = bxdf->sampleWi(g, wlen, time, wo, ior1, ior2, BxDF::Mode::RADIANCE, rnd, &wi, &pdf);
//
//					// asserts consistency
//					ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//					ASSERT_RELATIVE(bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE), val, 1E-2f);
//					ASSERT_RELATIVE(bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, true), pdf, 1E-2f);
//
//					// updates integral
//					if (pdf > 0.f) integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + 1E-2, integral);
//
//			// asserts importance sampling along the entire domain
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					val = bxdf->sampleWi(g, wlen, time, wo, ior1, ior2, BxDF::Mode::IMPORTANCE, rnd, &wi, &pdf);
//
//					// asserts consistency
//					ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//					ASSERT_RELATIVE(bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE), val, 1E-2f);
//					ASSERT_RELATIVE(bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, true), pdf, 1E-2f);
//
//					// updates integral
//					if (pdf > 0.f) integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + 1E-2, integral);
//		}
//	}
//}
//
///******************************************************************************
// * OREN-NAYAR BRDF TESTS
// *****************************************************************************/
///*
// * Asserts that the bxdf is correctly typed.
// */
//TEST(BxDF_OrenNayarBRDF, type) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = OrenNayarBRDF::build(pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.25f)
//	);
//	ASSERT_TRUE(bxdf->isOpticallyInteracting());
//	ASSERT_TRUE(bxdf->matches(BxDF::Type::DIFFUSE));
//	ASSERT_TRUE(bxdf->matches(BxDF::Type::REFLECTIVE));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::SPECULAR));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::REFRACTIVE));
//}
//
///*
// * Asserts that the bxdf value is non negative, is zero when `wi` and `wo` are
// * on opposite hemispheres and that for every `wo` the integral over all `wi`
// * is not greater than the especified reflectance.
// */
//TEST(BxDF_OrenNayarBRDF, evaluate) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = OrenNayarBRDF::build(pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.25f)
//	);
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// integrates radiance value over all `wi`
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts evaluated value
//					val = bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//					if (!sameHemisphere(wo, wi)) ASSERT_EQ(0.f, val);
//
//					// updates integral
//					integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + BxDF_ERROR, integral);
//
//			// integrates importance value over all `wi`
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts evaluated value
//					val = bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//					if (!sameHemisphere(wo, wi)) ASSERT_EQ(0.f, val);
//
//					// updates integral
//					integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + BxDF_ERROR, integral);
//		}
//	}
//}
//
///*
// * Asserts that the pdf of sampling an incominngo direction `wi` given an
// * outgoing direction `wo` is non negative and integrates to 1 over the entire
// * hemisphere of incoming directions.
// */
//TEST(BxDF_OrenNayarBRDF, pdf) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = OrenNayarBRDF::build(pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.25f)
//	);
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// integrates radiance pdf over all `wi`
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts the pdf
//					val = bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, false);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					if (pdf > 0.f) integral += val / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_NEAR(1.f, integral, BxDF_ERROR);
//
//			// integrates importance pdf over all `wi`
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts the pdf
//					val = bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, false);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					if (pdf > 0.f) integral += val / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_NEAR(1.f, integral, BxDF_ERROR);
//		}
//	}
//}
//
///*
// * Asserts that sampling has consistent value and pdf, that the sampled vector
// * is properly normalized and that the sampled radiance and importance
// * integrate to the correct values.
// */
//TEST(BxDF_OrenNayarBRDF, sample) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = OrenNayarBRDF::build(pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.25f)
//	);
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// asserts radiance sampling along the entire domain
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					val = bxdf->sampleWi(g, wlen, time, wo, ior1, ior2, BxDF::Mode::RADIANCE, rnd, &wi, &pdf);
//
//					// asserts consistency
//					ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//					ASSERT_RELATIVE(bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE), val, 1E-2f);
//					ASSERT_RELATIVE(bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, true), pdf, 1E-2f);
//
//					// updates integral
//					if (pdf > 0.f) integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + 1E-2, integral);
//
//			// asserts importance sampling along the entire domain
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					val = bxdf->sampleWi(g, wlen, time, wo, ior1, ior2, BxDF::Mode::IMPORTANCE, rnd, &wi, &pdf);
//
//					// asserts consistency
//					ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//					ASSERT_RELATIVE(bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE), val, 1E-2f);
//					ASSERT_RELATIVE(bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, true), pdf, 1E-2f);
//
//					// updates integral
//					if (pdf > 0.f) integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + 1E-2, integral);
//		}
//	}
//}
//
///******************************************************************************
// * MICROFACET BRDF TESTS
// *****************************************************************************/
///*
// * Asserts that the bxdf is correctly typed.
// */
//TEST(BxDF_MicrofacetBRDF, type) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = MicrofacetBRDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		),
//		false
//	);
//	ASSERT_TRUE(bxdf->isOpticallyInteracting());
//	ASSERT_TRUE(bxdf->matches(BxDF::Type::DIFFUSE));
//	ASSERT_TRUE(bxdf->matches(BxDF::Type::REFLECTIVE));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::SPECULAR));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::REFRACTIVE));
//}
//
///*
// * Asserts that the bxdf value is non negative, is zero when `wi` and `wo` are
// * on opposite hemispheres and that for every `wo` the integral over all `wi`
// * is not greater than the especified reflectance.
// * Note: The current microfacet model implmententation does no account for
// * multiple scattering and it causes some energy to be lost making the integral
// * less than the specified reflectance.
// */
//TEST(BxDF_MicrofacetBRDF, evaluate) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = MicrofacetBRDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		),
//		false
//	);
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// integrates radiance value over all `wi`
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts evaluated value
//					val = bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//					if (!sameHemisphere(wo, wi)) ASSERT_EQ(0.f, val);
//
//					// updates integral
//					integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + BxDF_ERROR, integral);
//
//			// integrates importance value over all `wi`
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts evaluated value
//					val = bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//					if (!sameHemisphere(wo, wi)) ASSERT_EQ(0.f, val);
//
//					// updates integral
//					integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + BxDF_ERROR, integral);
//		}
//	}
//}
//
///*
// * Asserts that the pdf of sampling an incominngo direction `wi` given an
// * outgoing direction `wo` is non negative and integrates to 1 over the entire
// * hemisphere of incoming directions.
// */
//TEST(BxDF_MicrofacetBRDF, pdf) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = MicrofacetBRDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		),
//		false
//	);
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// integrates radiance pdf over all `wi`
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts the pdf
//					val = bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, false);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					if (pdf > 0.f) integral += val / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_NEAR(1.f, integral, BxDF_ERROR);
//
//			// integrates importance pdf over all `wi`
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts the pdf
//					val = bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, false);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					if (pdf > 0.f) integral += val / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_NEAR(1.f, integral, BxDF_ERROR);
//		}
//	}
//}
//
///*
// * Asserts that sampling has consistent value and pdf, that the sampled vector
// * is properly normalized and that the sampled radiance and importance
// * integrate to the correct values.
// */
//TEST(BxDF_MicrofacetBRDF, sample) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = MicrofacetBRDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		),
//		false
//	);
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// asserts radiance sampling along the entire domain
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					val = bxdf->sampleWi(g, wlen, time, wo, ior1, ior2, BxDF::Mode::RADIANCE, rnd, &wi, &pdf);
//
//					// asserts consistency
//					ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//					ASSERT_RELATIVE(bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE), val, 1E-2f);
//					ASSERT_RELATIVE(bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, true), pdf, 1E-2f);
//
//					// updates integral
//					if (pdf > 0.f) integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + 1E-2, integral);
//
//			// asserts importance sampling along the entire domain
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					val = bxdf->sampleWi(g, wlen, time, wo, ior1, ior2, BxDF::Mode::IMPORTANCE, rnd, &wi, &pdf);
//
//					// asserts consistency
//					ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//					ASSERT_RELATIVE(bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE), val, 1E-2f);
//					ASSERT_RELATIVE(bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, true), pdf, 1E-2f);
//
//					// updates integral
//					if (pdf > 0.f) integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + 1E-2, integral);
//		}
//	}
//}
//
///******************************************************************************
// * MICROFACET BTDF TESTS
// *****************************************************************************/
///*
// * Asserts that the bxdf is correctly typed.
// */
//TEST(BxDF_MicrofacetBTDF, type) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = MicrofacetBTDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		),
//		false
//	);
//	ASSERT_TRUE(bxdf->isOpticallyInteracting());
//	ASSERT_TRUE(bxdf->matches(BxDF::Type::DIFFUSE));
//	ASSERT_TRUE(bxdf->matches(BxDF::Type::REFRACTIVE));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::SPECULAR));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::REFLECTIVE));
//}
//
///*
// * Asserts that the bxdf value is non negative, is zero when `wi` and `wo` are
// * on opposite hemispheres and that for every `wo` the integral over all `wi`
// * is not greater than the especified reflectance.
// * Note: The current microfacet model implmententation does no account for
// * multiple scattering and it causes some energy to be lost making the integral
// * less than the specified reflectance.
// */
//TEST(BxDF_MicrofacetBTDF, evaluate) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = MicrofacetBTDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		),
//		false
//	);
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// integrates radiance value over all `wi`
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts evaluated value
//					val = bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//					if (sameHemisphere(wo, wi)) ASSERT_EQ(0.f, val);
//
//					// updates integral
//					integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + BxDF_ERROR, integral);
//
//			// integrates importance value over all `wi`
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts evaluated value
//					val = bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//					if (sameHemisphere(wo, wi)) ASSERT_EQ(0.f, val);
//
//					// updates integral
//					integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + BxDF_ERROR, integral);
//		}
//	}
//}
//
///*
// * Asserts that the pdf of sampling an incominngo direction `wi` given an
// * outgoing direction `wo` is non negative and integrates to 1 over the entire
// * hemisphere of incoming directions.
// */
//TEST(BxDF_MicrofacetBTDF, pdf) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = MicrofacetBTDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		),
//		false
//	);
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// integrates radiance pdf over all `wi`
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts the pdf
//					val = bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, false);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					if (pdf > 0.f) integral += val / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_NEAR(1.f, integral, BxDF_ERROR);
//
//			// integrates importance pdf over all `wi`
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts the pdf
//					val = bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, false);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					if (pdf > 0.f) integral += val / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_NEAR(1.f, integral, BxDF_ERROR);
//		}
//	}
//}
//
///*
// * Asserts that sampling has consistent value and pdf, that the sampled vector
// * is properly normalized and that the sampled radiance and importance
// * integrate to the correct values.
// */
//TEST(BxDF_MicrofacetBTDF, sample) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = MicrofacetBTDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		),
//		false
//	);
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// asserts radiance sampling along the entire domain
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					val = bxdf->sampleWi(g, wlen, time, wo, ior1, ior2, BxDF::Mode::RADIANCE, rnd, &wi, &pdf);
//
//					// asserts consistency
//					ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//					ASSERT_RELATIVE(bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE), val, 1E-2f);
//					ASSERT_RELATIVE(bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, true), pdf, 1E-2f);
//
//					// updates integral
//					if (pdf > 0.f) integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + 1E-2, integral);
//
//			// asserts importance sampling along the entire domain
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					val = bxdf->sampleWi(g, wlen, time, wo, ior1, ior2, BxDF::Mode::IMPORTANCE, rnd, &wi, &pdf);
//
//					// asserts consistency
//					ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//					ASSERT_RELATIVE(bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE), val, 1E-2f);
//					ASSERT_RELATIVE(bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, true), pdf, 1E-2f);
//
//					// updates integral
//					if (pdf > 0.f) integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + 1E-2, integral);
//		}
//	}
//}
//
///******************************************************************************
// * MICROFACET BSDF TESTS
// *****************************************************************************/
///*
// * Asserts that the bxdf is correctly typed.
// */
//TEST(BxDF_MicrofacetBSDF, type) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = MicrofacetBSDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		)
//	);
//	ASSERT_TRUE(bxdf->isOpticallyInteracting());
//	ASSERT_TRUE(bxdf->matches(BxDF::Type::DIFFUSE));
//	ASSERT_TRUE(bxdf->matches(BxDF::Type::REFRACTIVE));
//	ASSERT_TRUE(bxdf->matches(BxDF::Type::REFLECTIVE));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::SPECULAR));
//
//}
//
///*
// * Asserts that the bxdf value is non negative, is zero when `wi` and `wo` are
// * on opposite hemispheres and that for every `wo` the integral over all `wi`
// * is not greater than the especified reflectance.
// * Note: The current microfacet model implmententation does no account for
// * multiple scattering and it causes some energy to be lost making the integral
// * less than the specified reflectance.
// */
//TEST(BxDF_MicrofacetBSDF, evaluate) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = MicrofacetBSDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		)
//	);
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// integrates radiance value over all `wi`
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts evaluated value
//					val = bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + BxDF_ERROR, integral);
//
//			// integrates importance value over all `wi`
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts evaluated value
//					val = bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + BxDF_ERROR, integral);
//		}
//	}
//}
//
///*
// * Asserts that the pdf of sampling an incominngo direction `wi` given an
// * outgoing direction `wo` is non negative and integrates to 1 over the entire
// * hemisphere of incoming directions.
// */
//TEST(BxDF_MicrofacetBSDF, pdf) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = MicrofacetBSDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		)
//	);
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// integrates radiance pdf over all `wi`
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts the pdf
//					val = bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, false);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					if (pdf > 0.f) integral += val / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_NEAR(1.f, integral, BxDF_ERROR);
//
//			// integrates importance pdf over all `wi`
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts the pdf
//					val = bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, false);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					if (pdf > 0.f) integral += val / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_NEAR(1.f, integral, BxDF_ERROR);
//		}
//	}
//}
//
///*
// * Asserts that sampling has consistent value and pdf, that the sampled vector
// * is properly normalized and that the sampled radiance and importance
// * integrate to the correct values.
// */
//TEST(BxDF_MicrofacetBSDF, sample) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = MicrofacetBSDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		)
//	);
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// asserts radiance sampling along the entire domain
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					val = bxdf->sampleWi(g, wlen, time, wo, ior1, ior2, BxDF::Mode::RADIANCE, rnd, &wi, &pdf);
//
//					// asserts consistency
//					ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//					ASSERT_RELATIVE(bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE), val, 1E-2f);
//					ASSERT_RELATIVE(bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, true), pdf, 1E-2f);
//
//					// updates integral
//					if (pdf > 0.f) integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + 1E-2, integral);
//
//			// asserts importance sampling along the entire domain
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					val = bxdf->sampleWi(g, wlen, time, wo, ior1, ior2, BxDF::Mode::IMPORTANCE, rnd, &wi, &pdf);
//
//					// asserts consistency
//					ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//					ASSERT_RELATIVE(bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE), val, 1E-2f);
//					ASSERT_RELATIVE(bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, true), pdf, 1E-2f);
//
//					// updates integral
//					if (pdf > 0.f) integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + 1E-2, integral);
//		}
//	}
//}
//
///******************************************************************************
// * SUBSTRATE BRDF TESTS
// *****************************************************************************/
///*
// * Asserts that the bxdf is correctly typed.
// */
//TEST(BxDF_SubstrateBRDF, type) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = SubstrateBRDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		)
//	);
//	ASSERT_TRUE(bxdf->isOpticallyInteracting());
//	ASSERT_TRUE(bxdf->matches(BxDF::Type::DIFFUSE));
//	ASSERT_TRUE(bxdf->matches(BxDF::Type::REFLECTIVE));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::SPECULAR));
//	ASSERT_FALSE(bxdf->matches(BxDF::Type::REFRACTIVE));
//}
//
///*
// * Asserts that the bxdf value is non negative, is zero when `wi` and `wo` are
// * on opposite hemispheres and that for every `wo` the integral over all `wi`
// * is not greater than the especified reflectance.
// * Note: The current microfacet model implmententation does no account for
// * multiple scattering and it causes some energy to be lost making the integral
// * less than the specified reflectance.
// */
//TEST(BxDF_SubstrateBRDF, evaluate) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = SubstrateBRDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		)
//	);
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// integrates radiance value over all `wi`
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts evaluated value
//					val = bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//					if (!sameHemisphere(wo, wi)) ASSERT_EQ(0.f, val);
//
//					// updates integral
//					integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + BxDF_ERROR, integral);
//
//			// integrates importance value over all `wi`
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts evaluated value
//					val = bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//					if (!sameHemisphere(wo, wi)) ASSERT_EQ(0.f, val);
//
//					// updates integral
//					integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + BxDF_ERROR, integral);
//		}
//	}
//}
//
///*
// * Asserts that the pdf of sampling an incominngo direction `wi` given an
// * outgoing direction `wo` is non negative and integrates to 1 over the entire
// * hemisphere of incoming directions.
// */
//TEST(BxDF_SubstrateBRDF, pdf) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = SubstrateBRDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		)
//	);
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// integrates radiance pdf over all `wi`
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts the pdf
//					val = bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, false);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					if (pdf > 0.f) integral += val / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_NEAR(1.f, integral, BxDF_ERROR);
//
//			// integrates importance pdf over all `wi`
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts the pdf
//					val = bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, false);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					if (pdf > 0.f) integral += val / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_NEAR(1.f, integral, BxDF_ERROR);
//		}
//	}
//}
//
///*
// * Asserts that sampling has consistent value and pdf, that the sampled vector
// * is properly normalized and that the sampled radiance and importance
// * integrate to the correct values.
// */
//TEST(BxDF_SubstrateBRDF, sample) {
//	// creates the bxdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bxdf = SubstrateBRDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		)
//	);
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = 0; theta <= 180; theta += 30) {
//		for (int phi = 0; phi <= 360; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// asserts radiance sampling along the entire domain
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					val = bxdf->sampleWi(g, wlen, time, wo, ior1, ior2, BxDF::Mode::RADIANCE, rnd, &wi, &pdf);
//
//					// asserts consistency
//					ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//					ASSERT_RELATIVE(bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE), val, 1E-2f);
//					ASSERT_RELATIVE(bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, true), pdf, 1E-2f);
//
//					// updates integral
//					if (pdf > 0.f) integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + 1E-2, integral);
//
//			// asserts importance sampling along the entire domain
//			integral = 0.f;
//			for (int i = 0; i < BxDF_SAMPLES; ++i) {
//				for (int j = 0; j < BxDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BxDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BxDF_SAMPLES;
//					val = bxdf->sampleWi(g, wlen, time, wo, ior1, ior2, BxDF::Mode::IMPORTANCE, rnd, &wi, &pdf);
//
//					// asserts consistency
//					ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//					ASSERT_RELATIVE(bxdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE), val, 1E-2f);
//					ASSERT_RELATIVE(bxdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, true), pdf, 1E-2f);
//
//					// updates integral
//					if (pdf > 0.f) integral += val * abs(wi[2]) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BxDF_SAMPLES * BxDF_SAMPLES);
//			ASSERT_GE(0.75f + 1E-2, integral);
//		}
//	}
//}
//
//}}

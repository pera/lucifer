//#include <gtest/gtest.h>
//#include "../TestUtils.cuh"
//#include "lucifer/bsdf/BSDF.cuh"
//#include "lucifer/bsdf/LambertianBRDF.cuh"
//#include "lucifer/bsdf/LambertianBTDF.cuh"
//#include "lucifer/bsdf/MicrofacetBRDF.cuh"
//#include "lucifer/bsdf/microfacet/SmithTrowbridgeReitzModel.cuh"
//#include "lucifer/math/Math.cuh"
//#include "lucifer/math/function/Constant.cuh"
//#include "lucifer/node/ConstantNode.cuh"
//#include "lucifer/random/UniformSphereDistribution.cuh"
//#include "lucifer/random/generator/PlainRNG.cuh"
//#include "lucifer/memory/CPUAllocator.cuh"
//#include "lucifer/memory/MemoryPool.cuh"
//#include "lucifer/memory/Pointer.cuh"
//
//namespace lucifer { namespace test {
//
//constexpr int BSDF_SAMPLES = 500;
//constexpr float BSDF_ERROR = 1E-3f;
//
///*
// * Asserts that the bsdf is correctly typed.
// */
//TEST(BSDF, type) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bsdf = BSDF::build(pool, 2);
//	(*bsdf)[0] = LambertianBRDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f));
//	(*bsdf)[1] = MicrofacetBRDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		),
//		false
//	);
//	ASSERT_TRUE(bsdf->isOpticallyInteracting());
//	ASSERT_TRUE(bsdf->matches(BxDF::Type(0)));
//	ASSERT_TRUE(bsdf->matches(BxDF::Type::DIFFUSE));
//	ASSERT_TRUE(bsdf->matches(BxDF::Type::REFLECTIVE));
//	ASSERT_FALSE(bsdf->matches(BxDF::Type::SPECULAR));
//	ASSERT_FALSE(bsdf->matches(BxDF::Type::REFRACTIVE));
//}
//
///*
// * Asserts that the bsdf value is non negative, is zero when `wi` and `wo` are
// * on opposite hemispheres and that for every `wo` the integral over all `wi`
// * is not greater than the especified reflectance.
// * Note: The current microfacet model implmententation does no account for
// * multiple scattering and it causes some energy to be lost making the integral
// * less than the specified reflectance.
// */
//TEST(BSDF, evaluate) {
//	// creates the bsdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bsdf = BSDF::build(pool, 2);
//	(*bsdf)[0] = LambertianBRDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f));
//	(*bsdf)[1] = MicrofacetBRDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		),
//		false
//	);
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = -90; theta <= 90; theta += 30) {
//		for (int phi = -180; phi <= 180; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// integrates radiance value over all `wi`
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BSDF_SAMPLES; ++i) {
//				for (int j = 0; j < BSDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BSDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BSDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts evaluated value
//					val = bsdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE, BxDF::Type(0));
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//					if (!sameHemisphere(g.gn, wo, wi)) ASSERT_EQ(0.f, val);
//
//					// updates integral
//					integral += val * absDot(g.gn, wi) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BSDF_SAMPLES * BSDF_SAMPLES);
//			ASSERT_GE(0.8f + BSDF_ERROR, integral);
//
//			// integrates importance value over all `wi`
//			integral = 0.f;
//			for (int i = 0; i < BSDF_SAMPLES; ++i) {
//				for (int j = 0; j < BSDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BSDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BSDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts evaluated value
//					val = bsdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE, BxDF::Type(0));
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//					if (!sameHemisphere(g.gn, wo, wi)) ASSERT_EQ(0.f, val);
//
//					// updates integral
//					integral += val * absDot(g.gn, wi) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BSDF_SAMPLES * BSDF_SAMPLES);
//			ASSERT_GE(0.8f + BSDF_ERROR, integral);
//		}
//	}
//}
//
///*
// * Asserts that the pdf of sampling an incoming direction `wi` given an
// * outgoing direction `wo` is non negative and integrates to 1 over the entire
// * sphere of incoming directions.
// */
//TEST(BSDF, pdf) {
//	// creates the bsdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bsdf = BSDF::build(pool, 2);
//	(*bsdf)[0] = LambertianBRDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f));
//	(*bsdf)[1] = MicrofacetBRDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//		SmithTrowbridgeReitzModel::build(
//			pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		),
//		false
//	);
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = -90; theta <= 90; theta += 30) {
//		for (int phi = -180; phi <= 180; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// integrates radiance pdf over all `wi`
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BSDF_SAMPLES; ++i) {
//				for (int j = 0; j < BSDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BSDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BSDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts the pdf
//					val = bsdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, BxDF::Type(0), nullptr);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					integral += val / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BSDF_SAMPLES * BSDF_SAMPLES);
//			ASSERT_NEAR(1.f, integral, BSDF_ERROR);
//
//			// integrates importance pdf over all `wi`
//			integral = 0.f;
//			for (int i = 0; i < BSDF_SAMPLES; ++i) {
//				for (int j = 0; j < BSDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BSDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BSDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts the pdf
//					val = bsdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, BxDF::Type(0), nullptr);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					integral += val / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BSDF_SAMPLES * BSDF_SAMPLES);
//			ASSERT_NEAR(1.f, integral, BSDF_ERROR);
//		}
//	}
//}
//
///*
// * Asserts that sampling has consistent value and pdf, that the sampled vector
// * is properly normalized and that the sampled radiance and importance
// * integrate to the correct values.
// */
//TEST(BSDF, sample) {
//	// creates the bsdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bsdf = BSDF::build(pool, 2);
//	(*bsdf)[0] = LambertianBRDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f));
//	(*bsdf)[1] = MicrofacetBRDF::build(
//		pool,
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//		SmithTrowbridgeReitzModel::build(pool,
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.4f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.6f)
//		),
//		false
//	);
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = -90; theta <= 90; theta += 30) {
//		for (int phi = -180; phi <= 180; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// asserts radiance sampling along the entire domain
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BSDF_SAMPLES; ++i) {
//				for (int j = 0; j < BSDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BSDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BSDF_SAMPLES;
//					val = bsdf->sampleWi(g, wlen, time, wo, ior1, ior2, BxDF::Mode::RADIANCE, BxDF::Type(0), rnd, &wi, &pdf, nullptr);
//
//					// asserts consistency
//					ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//					ASSERT_NEAR(bsdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE, BxDF::Type(0)), val, 1E-4f);
//					ASSERT_NEAR(bsdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, BxDF::Type(0), nullptr), pdf, 1E-4f);
//
//					// updates integral
//					if (pdf > 0.f) integral += val * absDot(g.gn, wi) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BSDF_SAMPLES * BSDF_SAMPLES);
//			ASSERT_GE(0.8f + BSDF_ERROR, integral);
//
//			// asserts importance sampling along the entire domain
//			integral = 0.f;
//			for (int i = 0; i < BSDF_SAMPLES; ++i) {
//				for (int j = 0; j < BSDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BSDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BSDF_SAMPLES;
//					val = bsdf->sampleWi(g, wlen, time, wo, ior1, ior2, BxDF::Mode::IMPORTANCE, BxDF::Type(0), rnd, &wi, &pdf, nullptr);
//
//					// asserts consistency
//					ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//					ASSERT_NEAR(bsdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE, BxDF::Type(0)), val, 1E-4f);
//					ASSERT_NEAR(bsdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, BxDF::Type(0), nullptr), pdf, 1E-4f);
//
//					// updates integral
//					if (pdf > 0.f) integral += val * absDot(g.gn, wi) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BSDF_SAMPLES * BSDF_SAMPLES);
//			ASSERT_GE(0.8f + BSDF_ERROR, integral);
//		}
//	}
//}
//
///*
// * Asserts that the relative albedo-based importance sampling PDF of incoming
// * directions is non negative and integrates to 1.
// */
//TEST(BSDF, albedo_pdf) {
//	// creates the bsdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bsdf = BSDF::build(pool, 2);
//	(*bsdf)[0] = LambertianBRDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.1f));
//	(*bsdf)[1] = LambertianBTDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.7f));
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = -90; theta <= 90; theta += 30) {
//		for (int phi = -180; phi <= 180; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// integrates radiance pdf over all `wi`
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BSDF_SAMPLES; ++i) {
//				for (int j = 0; j < BSDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BSDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BSDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts the pdf
//					val = bsdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, BxDF::Type(0), nullptr);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					integral += val / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BSDF_SAMPLES * BSDF_SAMPLES);
//			ASSERT_NEAR(1.f, integral, BSDF_ERROR);
//
//			// integrates importance pdf over all `wi`
//			integral = 0.f;
//			for (int i = 0; i < BSDF_SAMPLES; ++i) {
//				for (int j = 0; j < BSDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BSDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BSDF_SAMPLES;
//					UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//
//					// asserts the pdf
//					val = bsdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, BxDF::Type(0), nullptr);
//					ASSERT_FALSE(isnan(val));
//					ASSERT_FALSE(isinf(val));
//					ASSERT_LE(0.f, val);
//
//					// updates integral
//					integral += val / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BSDF_SAMPLES * BSDF_SAMPLES);
//			ASSERT_NEAR(1.f, integral, BSDF_ERROR);
//		}
//	}
//}
//
///*
// * Asserts that the relative albedo-based importance sampling has consistent
// * value and pdf, that the sampled vector is properly normalized and that the
// * sampled radiance and importance integrate to the correct values.
// */
//TEST(BSDF, albedo_sample) {
//	// creates the bsdf
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto bsdf = BSDF::build(pool, 2);
//	(*bsdf)[0] = LambertianBRDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.1f));
//	(*bsdf)[1] = LambertianBTDF::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.7f));
//
//	// geometric setting
//	ComplexF ior1(1.0f, 0.f);
//	ComplexF ior2(1.5f, 0.f);
//	float val, pdf, rnd[3], wlen = 540E-9, time = 0.f;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Geometry g(Point3F(0.f, 0.f, 0.f), Vectr3F(1.f, 0.f, 0.f), Norml3F(0.f, 0.f, 1.f));
//
//	// for each `wo`
//	for (int theta = -90; theta <= 90; theta += 30) {
//		for (int phi = -180; phi <= 180; phi += 45) {
//			Vectr3F wo = sphericalDirection<Vectr3F>(sin(toRadians((float)theta)), cos(toRadians((float)theta)), toRadians((float)phi));
//
//			// asserts radiance sampling along the entire domain
//			Vectr3F wi;
//			float integral = 0.f;
//			for (int i = 0; i < BSDF_SAMPLES; ++i) {
//				for (int j = 0; j < BSDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BSDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BSDF_SAMPLES;
//					val = bsdf->sampleWi(g, wlen, time, wo, ior1, ior2, BxDF::Mode::RADIANCE, BxDF::Type(0), rnd, &wi, &pdf, nullptr);
//
//					// asserts consistency
//					ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//					ASSERT_NEAR(bsdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::RADIANCE, BxDF::Type(0)), val, 1E-4f);
//					ASSERT_NEAR(bsdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::RADIANCE, BxDF::Type(0), nullptr), pdf, 1E-4f);
//
//					// updates integral
//					if (pdf > 0.f) integral += val * absDot(g.gn, wi) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BSDF_SAMPLES * BSDF_SAMPLES);
//			ASSERT_NEAR(0.8f, integral, BSDF_ERROR);
//
//			// asserts importance sampling along the entire domain
//			integral = 0.f;
//			for (int i = 0; i < BSDF_SAMPLES; ++i) {
//				for (int j = 0; j < BSDF_SAMPLES; ++j) {
//					// samples random `wi`
//					rng->next(rnd);
//					rnd[0] = (i + rnd[0]) / BSDF_SAMPLES;
//					rnd[1] = (j + rnd[1]) / BSDF_SAMPLES;
//					val = bsdf->sampleWi(g, wlen, time, wo, ior1, ior2, BxDF::Mode::IMPORTANCE, BxDF::Type(0), rnd, &wi, &pdf, nullptr);
//
//					// asserts consistency
//					ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//					ASSERT_NEAR(bsdf->evaluate(g, wlen, time, wi, wo, ior1, ior2, BxDF::Mode::IMPORTANCE, BxDF::Type(0)), val, 1E-4f);
//					ASSERT_NEAR(bsdf->sampleWiPDF(g, wlen, time, wo, wi, ior1, ior2, BxDF::Mode::IMPORTANCE, BxDF::Type(0), nullptr), pdf, 1E-4f);
//
//					// updates integral
//					if (pdf > 0.f) integral += val * absDot(g.gn, wi) / pdf;
//				}
//			}
//			// asseerts integral
//			integral /= (BSDF_SAMPLES * BSDF_SAMPLES);
//			ASSERT_NEAR(0.8f, integral, BSDF_ERROR);
//		}
//	}
//}
//
//}}

#ifndef TESTUTILS_CUH_
#define TESTUTILS_CUH_

namespace lucifer { namespace test {

#define ASSERT_PAIR_NEAR(a, b, e) \
ASSERT_NEAR((a)[0], (b)[0], (e)); \
ASSERT_NEAR((a)[1], (b)[1], (e));

#define ASSERT_TRIPLET_NEAR(a, b, e) \
ASSERT_NEAR((a)[0], (b)[0], (e)); \
ASSERT_NEAR((a)[1], (b)[1], (e)); \
ASSERT_NEAR((a)[2], (b)[2], (e));

#define ASSERT_QUAD_NEAR(a, b, e) \
ASSERT_NEAR((a)[0], (b)[0], (e)); \
ASSERT_NEAR((a)[1], (b)[1], (e)); \
ASSERT_NEAR((a)[2], (b)[2], (e)); \
ASSERT_NEAR((a)[3], (b)[3], (e));

#define ASSERT_RELATIVE(a, b, e) \
		ASSERT_NEAR((a), (b), e * max(abs(a), abs(b)));

template<typename T=float> __host__
inline void assertRelative(const T val1, const T val2, const T relError = 1E-4) {
	ASSERT_NEAR(val1, val2, abs(val1) * relError) << "relative Error: " << (abs(val1 - val2) / val1);
}

}}



#endif /* TESTUTILS_CUH_ */

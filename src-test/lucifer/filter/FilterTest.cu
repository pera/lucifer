#include <gtest/gtest.h>
#include "lucifer/filter/BoxFilter1D.cuh"
#include "lucifer/filter/CartesianFilter2D.cuh"
#include "lucifer/filter/GaussianFilter1D.cuh"
#include "lucifer/filter/LanczosFilter1D.cuh"
#include "lucifer/filter/MitchellNetravaliFilter1D.cuh"
#include "lucifer/filter/SampledFilter2D.cuh"
#include "lucifer/filter/TriangleFilter1D.cuh"
#include "lucifer/memory/CPUAllocator.cuh"
#include "lucifer/memory/MemoryPool.cuh"

namespace lucifer { namespace test {

TEST(Filter1D, box) {
	MemoryPool pool{4, new CPUAllocator{}};
	auto filter = BoxFilter1D::build(pool, 1.f);
	ASSERT_FLOAT_EQ(0.00f, (*filter)(-1.25f));
	ASSERT_FLOAT_EQ(1.00f, (*filter)(-0.25f));
	ASSERT_FLOAT_EQ(1.00f, (*filter)(+0.25f));
	ASSERT_FLOAT_EQ(0.00f, (*filter)(+1.25f));
}

TEST(Filter1D, triangle) {
	MemoryPool pool{4, new CPUAllocator{}};
	auto filter = TriangleFilter1D::build(pool, 1.f);
	ASSERT_FLOAT_EQ(0.00f, (*filter)(-1.25f));
	ASSERT_FLOAT_EQ(0.75f, (*filter)(-0.25f));
	ASSERT_FLOAT_EQ(0.75f, (*filter)(+0.25f));
	ASSERT_FLOAT_EQ(0.00f, (*filter)(+1.25f));
}

}}

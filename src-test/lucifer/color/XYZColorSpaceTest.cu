#include <gtest/gtest.h>
#include "lucifer/color/XYZColorSpace.cuh"
#include "lucifer/math/function/Constant.cuh"
#include "lucifer/memory/CPUAllocator.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer { namespace test {

TEST(XYZColorSpace, constructor) {
	MemoryPool pool{4, new CPUAllocator{}};
	auto cs = XYZColorSpace::build(pool);
	ASSERT_FLOAT_EQ(360E-9, cs->support().lower);
	ASSERT_FLOAT_EQ(830E-9, cs->support().upper);
}

TEST(XYZColorSpace, xyzFromSpectrum) {
	MemoryPool pool{4, new CPUAllocator{}};
	auto cs = XYZColorSpace::build(pool);
	auto E = Constant<float>::build(pool, 1.f);
	Color3F xyz;
	cs->XYZ(*E, xyz);
	EXPECT_GE(1E-4, abs(1.f - xyz[0]));
	EXPECT_GE(1E-4, abs(1.f - xyz[1]));
	EXPECT_GE(1E-4, abs(1.f - xyz[2]));
}

}}

#include <gtest/gtest.h>
#include "lucifer/color/Gamma.cuh"
#include "lucifer/memory/CPUAllocator.cuh"
#include "lucifer/memory/MemoryPool.cuh"

namespace lucifer { namespace test {

TEST(NoGama, encode) {
	MemoryPool pool{4, new CPUAllocator{}};
	auto g = NoGamma::build(pool);
	Color3F c(0.1f, 0.2f, 0.3f);
	g->encode(c);
	ASSERT_FLOAT_EQ(0.1f, c[0]);
	ASSERT_FLOAT_EQ(0.2f, c[1]);
	ASSERT_FLOAT_EQ(0.3f, c[2]);
}

TEST(NoGama, decode) {
	MemoryPool pool{4, new CPUAllocator{}};
	auto g = NoGamma::build(pool);
	Color3F c(0.1f, 0.2f, 0.3f);
	g->decode(c);
	ASSERT_FLOAT_EQ(0.1f, c[0]);
	ASSERT_FLOAT_EQ(0.2f, c[1]);
	ASSERT_FLOAT_EQ(0.3f, c[2]);
}

TEST(SimplifiedGama, encode) {
	MemoryPool pool{4, new CPUAllocator{}};
	auto g = SimplifiedGamma::build(pool, 2.2f);
	Color3F c(0.1f, 0.2f, 0.3f);
	g->encode(c);
	ASSERT_FLOAT_EQ(pow(0.1f, 1.f / 2.2f), c[0]);
	ASSERT_FLOAT_EQ(pow(0.2f, 1.f / 2.2f), c[1]);
	ASSERT_FLOAT_EQ(pow(0.3f, 1.f / 2.2f), c[2]);
}

TEST(SimplifiedGama, decode) {
	MemoryPool pool{4, new CPUAllocator{}};
	auto g = SimplifiedGamma::build(pool, 2.2f);
	Color3F c(0.1f, 0.2f, 0.3f);
	g->decode(c);
	ASSERT_FLOAT_EQ(pow(0.1f, 2.2f), c[0]);
	ASSERT_FLOAT_EQ(pow(0.2f, 2.2f), c[1]);
	ASSERT_FLOAT_EQ(pow(0.3f, 2.2f), c[2]);
}

}}

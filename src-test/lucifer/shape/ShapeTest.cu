//#include <gtest/gtest.h>
//#include "../TestUtils.cuh"
//#include "lucifer/math/geometry/KeyFrameTransformation.cuh"
//#include "lucifer/math/Transformation.cuh"
//#include "lucifer/memory/CPUAllocator.cuh"
//#include "lucifer/memory/MemoryPool.cuh"
//#include "lucifer/memory/Pointer.cuh"
//#include "lucifer/node/field/PerlinNoise3DNode.cuh"
//#include "lucifer/node/shading/PositionNode.cuh"
//#include "lucifer/shape/AnimatedShape.cuh"
//#include "lucifer/shape/BumpMappedShape.cuh"
//#include "lucifer/shape/BVHShape.cuh"
//#include "lucifer/shape/InvertedShape.cuh"
//#include "lucifer/shape/ShapeSet.cuh"
//#include "lucifer/shape/Sphere.cuh"
//#include "lucifer/shape/TransformedShape.cuh"
//#include "lucifer/shape/Triangle.cuh"
//#include "lucifer/shape/TriangleMesh.cuh"
//#include "lucifer/random/generator/PlainRNG.cuh"
//
//namespace lucifer { namespace test {
//
//constexpr int SHAPE_SAMPLE_COUNT = 100;
//
///******************************************************************************
// * SPHERE SHAPE TESTS
// *****************************************************************************/
///*
// * Asserts that the surface area is correct.
// */
//TEST(Shape_Sphere, area) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto s = Pointer<Shape>{Sphere::build(pool, center, radius)};
//	ASSERT_NEAR(50.2654824574f, s->area(), 1E-4);
//	ASSERT_NEAR(50.2654824574f, s->area(0.25f), 1E-4);
//	ASSERT_NEAR(50.2654824574f, s->area(0.75f), 1E-4);
//}
//
///*
// * Asserts that sampled points are actually on the shape's surface and that
// * geometric and shading normals and tangent vectors are consistent.
// */
//TEST(Shape_Sphere, sample) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto s = Sphere::build(pool, center, radius);
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		// asserts sampled point
//		ASSERT_NEAR(radius, (geometry.gp - center).length(), 1E-4);
//		// asserts geometric and shading normals
//		Vectr3F n = (geometry.gp - center).normalize();
//		ASSERT_TRIPLET_NEAR(n, geometry.gn, 1E-4);
//		ASSERT_TRIPLET_NEAR(n, geometry.sn, 1E-4);
//		// asserts geometric and shading tangents
//		ASSERT_NEAR(1.f, geometry.gu.length(), 1E-4);
//		ASSERT_NEAR(1.f, geometry.su.length(), 1E-4);
//		ASSERT_NEAR(0.f, dot(geometry.gn, geometry.gu), 1E-4);
//		ASSERT_NEAR(0.f, dot(geometry.sn, geometry.su), 1E-4);
//	}
//}
//
///*
// * Tests that the shape's boundary contains the sampled points.
// */
//TEST(Shape_Sphere, boundary) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto s = Sphere::build(pool, center, radius);
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	Box boundary = s->boundary(RangeF(0.f, 1.f));
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		ASSERT_TRUE(boundary.contains(geometry.gp));
//	}
//}
//
///*
// * Tests that rays actually intersect the shape's surface, that the
// * intersecting rays' max range are correctly updated and that the geometry at
// * the intersection points are consistent.
// */
//TEST(Shape_Sphere, intersection) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto s = Pointer<Shape>{Sphere::build(pool, center, radius)};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	Box boundary = s->boundary(RangeF(0.f, 1.f));
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		// samples a random ray origin within the bounding box.
//		rng->next(rnd);
//		Point3F origin = boundary.lower() + Vectr3F(rnd[0] * boundary.length(0), rnd[1] * boundary.length(1), rnd[2] * boundary.length(2));
//		ASSERT_TRUE(boundary.contains(origin));
//		// samples a point on the shape's surface to define the ray's direction.
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		Vectr3F direct = (geometry.gp - origin).normalize();
//		// asserts ray intersects shape
//		Shape::Intersection isect;
//		Ray ray(origin, direct, RangeF(0.f, +INFINITY), 0.f, rnd[2]);
//		ASSERT_TRUE(s->intersection(ray, &isect));
//		// asserts ray's range is correctly updated
//		ASSERT_NEAR(0.f, ray.r.lower, 1E-4);
//		ASSERT_NEAR((ray.o - isect.geometry.gp).length(), ray.r.upper, 1E-4);
//		// asserts intersection point is on the surface
//		ASSERT_NEAR(radius, (isect.geometry.gp - center).length(), 1E-4);
//		ASSERT_NEAR(1.f, isect.direction.length(), 1E-4);
//		// asserts geometric and shading normals
//		Vectr3F n = (isect.geometry.gp - center).normalize();
//		ASSERT_TRIPLET_NEAR(n, isect.geometry.gn, 1E-4);
//		ASSERT_TRIPLET_NEAR(n, isect.geometry.sn, 1E-4);
//		// asserts geometric and shading tangents
//		ASSERT_NEAR(1.f, isect.geometry.gu.length(), 1E-4);
//		ASSERT_NEAR(1.f, isect.geometry.su.length(), 1E-4);
//		ASSERT_NEAR(0.f, dot(isect.geometry.gn, isect.geometry.gu), 1E-4);
//		ASSERT_NEAR(0.f, dot(isect.geometry.sn, isect.geometry.su), 1E-4);
//	}
//}
//
///*
// * Asserts that the shape can correctly compute the random numbers that
// * generates points over it's surface.
// */
//TEST(Shape_Sphere, rnd) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], rec[2];
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto s = Pointer<Shape>{Sphere::build(pool, center, radius)};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		float u = rnd[0];
//		float v = rnd[1];
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		ASSERT_TRUE(s->canComputeRND());
//		s->rnd(rnd[2], geometry.gp, rec);
//		ASSERT_NEAR(u, rec[0], 1E-4);
//		ASSERT_NEAR(v, rec[1], 1E-4);
//	}
//}
//
///******************************************************************************
// * TRIANGLE SHAPE TESTS
// *****************************************************************************/
///*
// * Asserts that the surface area is correct.
// */
//TEST(Shape_Triangle, area) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	Point3F a(1.f, 0.f, 0.f);
//	Point3F b(0.f, 1.f, 0.f);
//	Point3F c(0.f, 0.f, 1.f);
//	auto s = Pointer<Shape>{Triangle::build(pool, a, b, c)};
//	//https://en.wikipedia.org/wiki/Triangle#Using_vectors
//	float area = 0.5 * cross((b - a), c - a).length();
//	ASSERT_NEAR(area, s->area(), 1E-4);
//	ASSERT_NEAR(area, s->area(0.25f), 1E-4);
//	ASSERT_NEAR(area, s->area(0.75f), 1E-4);
//}
//
///*
// * Asserts that sampled points are actually on the shape's surface and that
// * geometric and shading normals and tangent vectors are consistent.
// */
//TEST(Shape_Triangle, sample) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	Point3F a(1.f, 0.f, 0.f);
//	Point3F b(0.f, 1.f, 0.f);
//	Point3F c(0.f, 0.f, 1.f);
//	auto s = Triangle::build(pool, a, b, c);
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	Vectr3F n = cross((b - a), c - a).normalize();
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		// asserts sampled point is on the triangle's plane
//		ASSERT_NEAR(0.f, dot(n, geometry.gp - a), 1E-4);
//		// asserts sampled point's angles are smaller than vertexes's angles
//		ASSERT_TRUE(dot((b - a).normalize(), (c - a).normalize()) <= dot((b - a).normalize(), (geometry.gp - a).normalize()));
//		ASSERT_TRUE(dot((a - b).normalize(), (c - b).normalize()) <= dot((a - b).normalize(), (geometry.gp - b).normalize()));
//		ASSERT_TRUE(dot((b - c).normalize(), (a - c).normalize()) <= dot((b - c).normalize(), (geometry.gp - c).normalize()));
//		// asserts geometric and shading normals
//		ASSERT_TRIPLET_NEAR(n, geometry.gn, 1E-4);
//		ASSERT_TRIPLET_NEAR(n, geometry.sn, 1E-4);
//		// asserts geometric and shading tangents
//		ASSERT_NEAR(1.f, geometry.gu.length(), 1E-4);
//		ASSERT_NEAR(1.f, geometry.su.length(), 1E-4);
//		ASSERT_NEAR(0.f, dot(geometry.gn, geometry.gu), 1E-4);
//		ASSERT_NEAR(0.f, dot(geometry.sn, geometry.su), 1E-4);
//	}
//}
//
///*
// * Tests that the shape's boundary contains the sampled points.
// */
//TEST(Shape_Triangle, boundary) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	Point3F a(1.f, 0.f, 0.f);
//	Point3F b(0.f, 1.f, 0.f);
//	Point3F c(0.f, 0.f, 1.f);
//	auto s = Triangle::build(pool, a, b, c);
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	Box boundary = s->boundary(RangeF(0.f, 1.f));
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		ASSERT_TRUE(boundary.contains(geometry.gp));
//	}
//}
//
///*
// * Tests that rays actually intersect the shape's surface, that the
// * intersecting rays' max range are correctly updated and that the geometry at
// * the intersection points are consistent.
// */
//TEST(Shape_Triangle, intersection) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	Point3F a(1.f, 0.f, 0.f);
//	Point3F b(0.f, 1.f, 0.f);
//	Point3F c(0.f, 0.f, 1.f);
//	auto s = Triangle::build(pool, a, b, c);
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Box boundary = s->boundary(RangeF(0.f, 1.f));
//	Vectr3F n = cross((b - a), c - a).normalize();
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		// samples a random ray origin within the bounding box.
//		rng->next(rnd);
//		Point3F origin = boundary.lower() + Vectr3F(rnd[0] * boundary.length(0), rnd[1] * boundary.length(1), rnd[2] * boundary.length(2));
//		ASSERT_TRUE(boundary.contains(origin));
//		// samples a point on the shape's surface to define the ray's direction.
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		Vectr3F direct = (geometry.gp - origin).normalize();
//		// asserts ray intersects shape
//		Shape::Intersection isect;
//		Ray ray(origin, direct, RangeF(0.f, +INFINITY), 0.f, rnd[2]);
//		ASSERT_TRUE(s->intersection(ray, &isect));
//		// asserts ray's range is correctly updated
//		ASSERT_NEAR(0.f, ray.r.lower, 1E-4);
//		ASSERT_NEAR((ray.o - isect.geometry.gp).length(), ray.r.upper, 1E-4);
//		// asserts intersection point is on the surface
//		ASSERT_NEAR(0.f, dot(n, isect.geometry.gp - a), 1E-4);
//		ASSERT_TRUE(dot((b - a).normalize(), (c - a).normalize()) <= dot((b - a).normalize(), (isect.geometry.gp - a).normalize()));
//		ASSERT_TRUE(dot((a - b).normalize(), (c - b).normalize()) <= dot((a - b).normalize(), (isect.geometry.gp - b).normalize()));
//		ASSERT_TRUE(dot((b - c).normalize(), (a - c).normalize()) <= dot((b - c).normalize(), (isect.geometry.gp - c).normalize()));
//		ASSERT_NEAR(1.f, isect.direction.length(), 1E-4);
//		// asserts geometric and shading normals
//		ASSERT_TRIPLET_NEAR(n, isect.geometry.gn, 1E-4);
//		ASSERT_TRIPLET_NEAR(n, isect.geometry.sn, 1E-4);
//		// asserts geometric and shading tangents
//		ASSERT_NEAR(1.f, isect.geometry.gu.length(), 1E-4);
//		ASSERT_NEAR(1.f, isect.geometry.su.length(), 1E-4);
//		ASSERT_NEAR(0.f, dot(isect.geometry.gn, isect.geometry.gu), 1E-4);
//		ASSERT_NEAR(0.f, dot(isect.geometry.sn, isect.geometry.su), 1E-4);
//	}
//}
//
///*
// * Asserts that the shape can correctly compute the random numbers that
// * generates points over it's surface.
// */
//TEST(Shape_Triangle, rnd) {
//	{
//		// xy plane
//		MemoryPool pool{4, new CPUAllocator{}};
//		float rnd[3], rec[2];
//		Point3F a(0.f, 0.f, 0.f);
//		Point3F b(1.f, 0.f, 0.f);
//		Point3F c(0.f, 1.f, 0.f);
//		auto s = Triangle::build(pool, a, b, c);
//		auto rng = PlainRNGFactory<>().build(pool, 3);
//		for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//			rng->next(rnd);
//			float u = rnd[0];
//			float v = rnd[1];
//			Geometry geometry;
//			s->sampleGeometry(rnd[2], rnd, &geometry);
//			ASSERT_TRUE(s->canComputeRND());
//			s->rnd(rnd[2], geometry.gp, rec);
//			ASSERT_NEAR(u, rec[0], 1E-4);
//			ASSERT_NEAR(v, rec[1], 1E-4);
//		}
//	}
//	{
//		// yz plane
//		MemoryPool pool{4, new CPUAllocator{}};
//		float rnd[3], rec[2];
//		Point3F a(0.f, 0.f, 0.f);
//		Point3F b(0.f, 1.f, 0.f);
//		Point3F c(0.f, 0.f, 1.f);
//		auto s = Triangle::build(pool, a, b, c);
//		Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//		for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//			rng->next(rnd);
//			float u = rnd[0];
//			float v = rnd[1];
//			Geometry geometry;
//			s->sampleGeometry(rnd[2], rnd, &geometry);
//			ASSERT_TRUE(s->canComputeRND());
//			s->rnd(rnd[2], geometry.gp, rec);
//			ASSERT_NEAR(u, rec[0], 1E-4);
//			ASSERT_NEAR(v, rec[1], 1E-4);
//		}
//	}
//	{
//		// zx plane
//		MemoryPool pool{4, new CPUAllocator{}};
//		float rnd[3], rec[2];
//		Point3F a(0.f, 0.f, 0.f);
//		Point3F b(0.f, 0.f, 1.f);
//		Point3F c(1.f, 0.f, 0.f);
//		auto s = Triangle::build(pool, a, b, c);
//		auto rng = PlainRNGFactory<>().build(pool, 3);
//		for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//			rng->next(rnd);
//			float u = rnd[0];
//			float v = rnd[1];
//			Geometry geometry;
//			s->sampleGeometry(rnd[2], rnd, &geometry);
//			ASSERT_TRUE(s->canComputeRND());
//			s->rnd(rnd[2], geometry.gp, rec);
//			ASSERT_NEAR(u, rec[0], 1E-4);
//			ASSERT_NEAR(v, rec[1], 1E-4);
//		}
//	}
//	{
//		MemoryPool pool{4, new CPUAllocator{}};
//		float rnd[3], rec[2];
//		Point3F a(1.f, 0.f, 0.f);
//		Point3F b(0.f, 1.f, 0.f);
//		Point3F c(0.f, 0.f, 1.f);
//		auto s = Triangle::build(pool, a, b, c);
//		auto rng = PlainRNGFactory<>().build(pool, 3);
//		for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//			rng->next(rnd);
//			float u = rnd[0];
//			float v = rnd[1];
//			Geometry geometry;
//			s->sampleGeometry(rnd[2], rnd, &geometry);
//			ASSERT_TRUE(s->canComputeRND());
//			s->rnd(rnd[2], geometry.gp, rec);
//			ASSERT_NEAR(u, rec[0], 1E-4);
//			ASSERT_NEAR(v, rec[1], 1E-4);
//		}
//	}
//}
//
///******************************************************************************
// * INVERTED SHAPE TESTS
// *****************************************************************************/
///*
// * Asserts that the surface area is correct.
// */
//TEST(Shape_Inverted, area) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto s = Pointer<Shape>{InvertedShape::build(pool, Sphere::build(pool, center, radius))};
//	ASSERT_NEAR(50.2654824574f, s->area(), 1E-4);
//	ASSERT_NEAR(50.2654824574f, s->area(0.25f), 1E-4);
//	ASSERT_NEAR(50.2654824574f, s->area(0.75f), 1E-4);
//}
//
///*
// * Asserts that sampled points are actually on the shape's surface and that
// * geometric and shading normals and tangent vectors are consistent.
// */
//TEST(Shape_Inverted, sample) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto s = Pointer<Shape>{InvertedShape::build(pool, Sphere::build(pool, center, radius))};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		// asserts sampled point
//		ASSERT_NEAR(radius, (geometry.gp - center).length(), 1E-4);
//		// asserts geometric and shading normals
//		Vectr3F n = -(geometry.gp - center).normalize();
//		ASSERT_TRIPLET_NEAR(n, geometry.gn, 1E-4);
//		ASSERT_TRIPLET_NEAR(n, geometry.sn, 1E-4);
//		// asserts geometric and shading tangents
//		ASSERT_NEAR(1.f, geometry.gu.length(), 1E-4);
//		ASSERT_NEAR(1.f, geometry.su.length(), 1E-4);
//		ASSERT_NEAR(0.f, dot(geometry.gn, geometry.gu), 1E-4);
//		ASSERT_NEAR(0.f, dot(geometry.sn, geometry.su), 1E-4);
//	}
//}
//
///*
// * Tests that the shape's boundary contains the sampled points.
// */
//TEST(Shape_Inverted, boundary) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto s = Pointer<Shape>{InvertedShape::build(pool, Sphere::build(pool, center, radius))};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Box boundary = s->boundary(RangeF(0.f, 1.f));
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		ASSERT_TRUE(boundary.contains(geometry.gp));
//	}
//}
//
///*
// * Tests that rays actually intersect the shape's surface, that the
// * intersecting rays' max range are correctly updated and that the geometry at
// * the intersection points are consistent.
// */
//TEST(Shape_Inverted, intersection) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto s = Pointer<Shape>{InvertedShape::build(pool, Sphere::build(pool, center, radius))};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Box boundary = s->boundary(RangeF(0.f, 1.f));
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		// samples a random ray origin within the bounding box.
//		rng->next(rnd);
//		Point3F origin = boundary.lower() + Vectr3F(rnd[0] * boundary.length(0), rnd[1] * boundary.length(1), rnd[2] * boundary.length(2));
//		ASSERT_TRUE(boundary.contains(origin));
//		// samples a point on the shape's surface to define the ray's direction.
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		Vectr3F direct = (geometry.gp - origin).normalize();
//		// asserts ray intersects shape
//		Shape::Intersection isect;
//		Ray ray(origin, direct, RangeF(0.f, +INFINITY), 0.f, rnd[2]);
//		ASSERT_TRUE(s->intersection(ray, &isect));
//		// asserts ray's range is correctly updated
//		ASSERT_NEAR(0.f, ray.r.lower, 1E-4);
//		ASSERT_NEAR((ray.o - isect.geometry.gp).length(), ray.r.upper, 1E-4);
//		// asserts intersection point is on the surface
//		ASSERT_NEAR(radius, (isect.geometry.gp - center).length(), 1E-4);
//		ASSERT_NEAR(1.f, isect.direction.length(), 1E-4);
//		// asserts geometric and shading normals
//		Vectr3F n = -(isect.geometry.gp - center).normalize();
//		ASSERT_TRIPLET_NEAR(n, isect.geometry.gn, 1E-4);
//		ASSERT_TRIPLET_NEAR(n, isect.geometry.sn, 1E-4);
//		// asserts geometric and shading tangents
//		ASSERT_NEAR(1.f, isect.geometry.gu.length(), 1E-4);
//		ASSERT_NEAR(1.f, isect.geometry.su.length(), 1E-4);
//		ASSERT_NEAR(0.f, dot(isect.geometry.gn, isect.geometry.gu), 1E-4);
//		ASSERT_NEAR(0.f, dot(isect.geometry.sn, isect.geometry.su), 1E-4);
//	}
//}
//
///*
// * Asserts that the shape can correctly compute the random numbers that
// * generates points over it's surface.
// */
//TEST(Shape_Inverted, rnd) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], rec[2];
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto s = Pointer<Shape>{InvertedShape::build(pool, Sphere::build(pool, center, radius))};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		float u = rnd[0];
//		float v = rnd[1];
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		ASSERT_TRUE(s->canComputeRND());
//		s->rnd(rnd[2], geometry.gp, rec);
//		ASSERT_NEAR(u, rec[0], 1E-4);
//		ASSERT_NEAR(v, rec[1], 1E-4);
//	}
//}
//
///******************************************************************************
// * TRANSFORMED SHAPE TESTS
// *****************************************************************************/
///*
// * Asserts that the surface area is correct.
// */
//TEST(Shape_Transformed, area) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto s = Pointer<Shape>{TransformedShape::build(
//			pool,
//			Transformation<float, 3>::translation(Vectr3F{center}) *
//			Transformation<float, 3>::scaling(Vectr3F(radius, radius, radius)),
//			Sphere::build(pool, Point3F(0.f, 0.f, 0.f), 1.f)
//	)};
//	ASSERT_NEAR(50.2654824574f, s->area(), 1E-4);
//	ASSERT_NEAR(50.2654824574f, s->area(0.25f), 1E-4);
//	ASSERT_NEAR(50.2654824574f, s->area(0.75f), 1E-4);
//}
//
///*
// * Asserts that sampled points are actually on the shape's surface and that
// * geometric and shading normals and tangent vectors are consistent.
// */
//TEST(Shape_Transformed, sample) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto s = Pointer<Shape>{TransformedShape::build(
//			pool,
//			Transformation<float, 3>::translation(Vectr3F{center}) *
//			Transformation<float, 3>::scaling(Vectr3F(radius, radius, radius)),
//			Sphere::build(pool, Point3F(0.f, 0.f, 0.f), 1.f)
//	)};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		// asserts sampled point
//		ASSERT_NEAR(radius, (geometry.gp - center).length(), 1E-4);
//		// asserts geometric and shading normals
//		Vectr3F n = (geometry.gp - center).normalize();
//		ASSERT_TRIPLET_NEAR(n, geometry.gn, 1E-4);
//		ASSERT_TRIPLET_NEAR(n, geometry.sn, 1E-4);
//		// asserts geometric and shading tangents
//		ASSERT_NEAR(1.f, geometry.gu.length(), 1E-4);
//		ASSERT_NEAR(1.f, geometry.su.length(), 1E-4);
//		ASSERT_NEAR(0.f, dot(geometry.gn, geometry.gu), 1E-4);
//		ASSERT_NEAR(0.f, dot(geometry.sn, geometry.su), 1E-4);
//	}
//}
//
///*
// * Tests that the shape's boundary contains the sampled points.
// */
//TEST(Shape_Transformed, boundary) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto s = Pointer<Shape>{TransformedShape::build(
//			pool,
//			Transformation<float, 3>::translation(Vectr3F{center}) *
//			Transformation<float, 3>::scaling(Vectr3F(radius, radius, radius)),
//			Sphere::build(pool, Point3F(0.f, 0.f, 0.f), 1.f)
//	)};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Box boundary = s->boundary(RangeF(0.f, 1.f));
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		ASSERT_TRUE(boundary.contains(geometry.gp));
//	}
//}
//
///*
// * Tests that rays actually intersect the shape's surface, that the
// * intersecting rays' max range are correctly updated and that the geometry at
// * the intersection points are consistent.
// */
//TEST(Shape_Transformed, intersection) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto s = Pointer<Shape>{TransformedShape::build(
//			pool,
//			Transformation<float, 3>::translation(Vectr3F{center}) *
//			Transformation<float, 3>::scaling(Vectr3F(radius, radius, radius)),
//			Sphere::build(pool, Point3F(0.f, 0.f, 0.f), 1.f)
//	)};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Box boundary = s->boundary(RangeF(0.f, 1.f));
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		// samples a random ray origin within the bounding box.
//		rng->next(rnd);
//		Point3F origin = boundary.lower() + Vectr3F(rnd[0] * boundary.length(0), rnd[1] * boundary.length(1), rnd[2] * boundary.length(2));
//		ASSERT_TRUE(boundary.contains(origin));
//		// samples a point on the shape's surface to define the ray's direction.
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		Vectr3F direct = (geometry.gp - origin).normalize();
//		// asserts ray intersects shape
//		Shape::Intersection isect;
//		Ray ray(origin, direct, RangeF(0.f, +INFINITY), 0.f, rnd[2]);
//		ASSERT_TRUE(s->intersection(ray, &isect));
//		// asserts ray's range is correctly updated
//		ASSERT_NEAR(0.f, ray.r.lower, 1E-4);
//		ASSERT_NEAR((ray.o - isect.geometry.gp).length(), ray.r.upper, 1E-4);
//		// asserts intersection point is on the surface
//		ASSERT_NEAR(radius, (isect.geometry.gp - center).length(), 1E-4);
//		ASSERT_NEAR(1.f, isect.direction.length(), 1E-4);
//		// asserts geometric and shading normals
//		Vectr3F n = (isect.geometry.gp - center).normalize();
//		ASSERT_TRIPLET_NEAR(n, isect.geometry.gn, 1E-4);
//		ASSERT_TRIPLET_NEAR(n, isect.geometry.sn, 1E-4);
//		// asserts geometric and shading tangents
//		ASSERT_NEAR(1.f, isect.geometry.gu.length(), 1E-4);
//		ASSERT_NEAR(1.f, isect.geometry.su.length(), 1E-4);
//		ASSERT_NEAR(0.f, dot(isect.geometry.gn, isect.geometry.gu), 1E-4);
//		ASSERT_NEAR(0.f, dot(isect.geometry.sn, isect.geometry.su), 1E-4);
//	}
//}
//
///*
// * Asserts that the shape can correctly compute the random numbers that
// * generates points over it's surface.
// */
//TEST(Shape_Transformed, rnd) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], rec[2];
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto s = Pointer<Shape>{TransformedShape::build(
//			pool,
//			Transformation<float, 3>::translation(Vectr3F{center}) *
//			Transformation<float, 3>::scaling(Vectr3F(radius, radius, radius)),
//			Sphere::build(pool, Point3F(0.f, 0.f, 0.f), 1.f)
//	)};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		float u = rnd[0];
//		float v = rnd[1];
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		ASSERT_TRUE(s->canComputeRND());
//		s->rnd(rnd[2], geometry.gp, rec);
//		ASSERT_NEAR(u, rec[0], 1E-4);
//		ASSERT_NEAR(v, rec[1], 1E-4);
//	}
//}
//
///******************************************************************************
// * ANIMATED SHAPE TESTS
// *****************************************************************************/
///*
// * Asserts that the surface area is correct.
// */
//TEST(Shape_Animated, area) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd;
//	float r0 = 1.f;
//	float r1 = 2.f;
//	Point3F c0{0.f, 0.f, 0.f};
//	Point3F c1{1.f, 2.f, 3.f};
//	using frame_t = KeyFrameTransformation::KeyFrame;
//	KeyFrameTransformation::array_t array(pool, 2);
//	array[0] = frame_t(0.f, Transformation<float, 3>::identity());
//	array[1] = frame_t(1.f, Transformation<float, 3>::translation(Vectr3F{c1}) * Transformation<float, 3>::scaling(Vectr3F{r1, r1, r1}));
//	auto s = Pointer<Shape>{AnimatedShape::build(pool, KeyFrameTransformation::build(pool, array), Sphere::build(pool, c0, r0))};
//	auto rng = PlainRNGFactory<>().build(pool, 1);
//	ASSERT_EQ(-1.f, s->area());
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(&rnd);
//		float r = lerp(r0, r1, rnd);
//		ASSERT_NEAR(4.f * PI<float>() * r * r, s->area(rnd), 1E-4);
//	}
//}
//
///*
// * Asserts that sampled points are actually on the shape's surface and that
// * geometric and shading normals and tangent vectors are consistent.
// */
//TEST(Shape_Animated, sample) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	float r0 = 1.f;
//	float r1 = 2.f;
//	Point3F c0(0.f, 0.f, 0.f);
//	Point3F c1(1.f, 2.f, 3.f);
//	using frame_t = KeyFrameTransformation::KeyFrame;
//	KeyFrameTransformation::array_t array(pool, 2);
//	array[0] = frame_t(0.f, Transformation<float, 3>::identity());
//	array[1] = frame_t(1.f, Transformation<float, 3>::translation(Vectr3F{c1}) * Transformation<float, 3>::scaling(Vectr3F{r1, r1, r1}));
//	auto s = Pointer<Shape>{AnimatedShape::build(pool, KeyFrameTransformation::build(pool, array), Sphere::build(pool, c0, r0))};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		// asserts sampled point
//		auto center = lerp(c0, c1, rnd[2]);
//		auto radius = lerp(r0, r1, rnd[2]);
//		ASSERT_NEAR(radius, (geometry.gp - center).length(), 1E-4);
//		// asserts geometric and shading normals
//		Vectr3F n = (geometry.gp - center).normalize();
//		ASSERT_TRIPLET_NEAR(n, geometry.gn, 1E-4);
//		ASSERT_TRIPLET_NEAR(n, geometry.sn, 1E-4);
//		// asserts geometric and shading tangents
//		ASSERT_NEAR(1.f, geometry.gu.length(), 1E-4);
//		ASSERT_NEAR(1.f, geometry.su.length(), 1E-4);
//		ASSERT_NEAR(0.f, dot(geometry.gn, geometry.gu), 1E-4);
//		ASSERT_NEAR(0.f, dot(geometry.sn, geometry.su), 1E-4);
//	}
//}
//
///*
// * Tests that the shape's boundary contains the sampled points.
// */
//TEST(Shape_Animated, boundary) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	float r0 = 1.f;
//	float r1 = 2.f;
//	Point3F c0(0.f, 0.f, 0.f);
//	Point3F c1(1.f, 2.f, 3.f);
//	using frame_t = KeyFrameTransformation::KeyFrame;
//	KeyFrameTransformation::array_t array(pool, 2);
//	array[0] = frame_t(0.f, Transformation<float, 3>::identity());
//	array[1] = frame_t(1.f, Transformation<float, 3>::translation(Vectr3F{c1}) * Transformation<float, 3>::scaling(Vectr3F{r1, r1, r1}));
//	auto s = Pointer<Shape>{AnimatedShape::build(pool, KeyFrameTransformation::build(pool, array), Sphere::build(pool, c0, r0))};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Box boundary = s->boundary(RangeF(0.f, 1.f));
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		ASSERT_TRUE(boundary.contains(geometry.gp));
//	}
//}
//
///*
// * Tests that rays actually intersect the shape's surface, that the
// * intersecting rays' max range are correctly updated and that the geometry at
// * the intersection points are consistent.
// */
//TEST(Shape_Animated, intersection) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	float r0 = 1.f;
//	float r1 = 2.f;
//	Point3F c0(0.f, 0.f, 0.f);
//	Point3F c1(1.f, 2.f, 3.f);
//	using frame_t = KeyFrameTransformation::KeyFrame;
//	KeyFrameTransformation::array_t array(pool, 2);
//	array[0] = frame_t(0.f, Transformation<float, 3>::identity());
//	array[1] = frame_t(1.f, Transformation<float, 3>::translation(Vectr3F{c1}) * Transformation<float, 3>::scaling(Vectr3F{r1, r1, r1}));
//	auto s = Pointer<Shape>{AnimatedShape::build(pool, KeyFrameTransformation::build(pool, array), Sphere::build(pool, c0, r0))};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	auto boundary = s->boundary(RangeF{0.f, 1.f});
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		// samples a radom normalized time
//		rng->next(rnd);
//		auto time = rnd[0];
//		// samples a random ray origin within the bounding box.
//		rng->next(rnd);
//		Point3F origin = boundary.lower() + Vectr3F(rnd[0] * boundary.length(0), rnd[1] * boundary.length(1), rnd[2] * boundary.length(2));
//		ASSERT_TRUE(boundary.contains(origin));
//		// samples a point on the shape's surface to define the ray's direction.
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(time, rnd, &geometry);
//		Vectr3F direct = (geometry.gp - origin).normalize();
//		// asserts ray intersects shape
//		Shape::Intersection isect;
//		Ray ray(origin, direct, RangeF(0.f, +INFINITY), 0.f, time);
//		ASSERT_TRUE(s->intersection(ray, &isect));
//		// asserts ray's range is correctly updated
//		ASSERT_NEAR(0.f, ray.r.lower, 1E-4);
//		ASSERT_NEAR((ray.o - isect.geometry.gp).length(), ray.r.upper, 1E-4);
//		// asserts intersection point is on the surface
//		auto center = lerp(c0, c1, time);
//		auto radius = lerp(r0, r1, time);
//		ASSERT_NEAR(radius, (isect.geometry.gp - center).length(), 1E-4);
//		ASSERT_NEAR(1.f, isect.direction.length(), 1E-4);
//		// asserts geometric and shading normals
//		Vectr3F n = (isect.geometry.gp - center).normalize();
//		ASSERT_TRIPLET_NEAR(n, isect.geometry.gn, 1E-4);
//		ASSERT_TRIPLET_NEAR(n, isect.geometry.sn, 1E-4);
//		// asserts geometric and shading tangents
//		ASSERT_NEAR(1.f, isect.geometry.gu.length(), 1E-4);
//		ASSERT_NEAR(1.f, isect.geometry.su.length(), 1E-4);
//		ASSERT_NEAR(0.f, dot(isect.geometry.gn, isect.geometry.gu), 1E-4);
//		ASSERT_NEAR(0.f, dot(isect.geometry.sn, isect.geometry.su), 1E-4);
//	}
//}
//
///*
// * Asserts that the shape can correctly compute the random numbers that
// * generates points over it's surface.
// */
//TEST(Shape_Animated, rnd) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], rec[2];
//	float r0 = 1.f;
//	float r1 = 2.f;
//	Point3F c0(0.f, 0.f, 0.f);
//	Point3F c1(1.f, 2.f, 3.f);
//	using frame_t = KeyFrameTransformation::KeyFrame;
//	KeyFrameTransformation::array_t array(pool, 2);
//	array[0] = frame_t(0.f, Transformation<float, 3>::identity());
//	array[1] = frame_t(1.f, Transformation<float, 3>::translation(Vectr3F{c1}) * Transformation<float, 3>::scaling(Vectr3F{r1, r1, r1}));
//	auto s = Pointer<Shape>{AnimatedShape::build(pool, KeyFrameTransformation::build(pool, array), Sphere::build(pool, c0, r0))};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		float u = rnd[0];
//		float v = rnd[1];
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		ASSERT_TRUE(s->canComputeRND());
//		s->rnd(rnd[2], geometry.gp, rec);
//		ASSERT_NEAR(u, rec[0], 1E-4);
//		ASSERT_NEAR(v, rec[1], 1E-4);
//	}
//}
//
///******************************************************************************
// * BUMP-MAPPED SHAPE TESTS
// *****************************************************************************/
///*
// * Asserts that the surface area is correct.
// */
//TEST(Shape_BumpMapped, area) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto bumpMap = PerlinNoise3DNode<LUCIFER_SHADING_INPUT>::build(pool, PositionNode::build(pool), -1E-1f, +1E-2f, 8, 8, 8, 0);
//	auto s = Pointer<Shape>{BumpMappedShape::build(pool, bumpMap, Sphere::build(pool, center, radius), 1E-4)};
//	ASSERT_NEAR(50.2654824574f, s->area(), 1E-4);
//	ASSERT_NEAR(50.2654824574f, s->area(0.25f), 1E-4);
//	ASSERT_NEAR(50.2654824574f, s->area(0.75f), 1E-4);
//}
//
///*
// * Asserts that sampled points are actually on the shape's surface and that
// * geometric and shading normals and tangent vectors are consistent.
// */
//TEST(Shape_BumpMapped, sample) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto bumpMap = PerlinNoise3DNode<LUCIFER_SHADING_INPUT>::build(pool, PositionNode::build(pool), -1E-1f, +1E-2f, 8, 8, 8, 0);
//	auto s = Pointer<Shape>{BumpMappedShape::build(pool, bumpMap, Sphere::build(pool, center, radius), 1E-4)};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		// asserts sampled point
//		ASSERT_NEAR(radius, (geometry.gp - center).length(), 1E-4);
//		// asserts geometric and shading normals
//		Vectr3F n = (geometry.gp - center).normalize();
//		ASSERT_TRIPLET_NEAR(n, geometry.gn, 1E-4);
//		ASSERT_NEAR(1.f, geometry.sn.length(), 1E-4);
//		// asserts geometric and shading tangents
//		ASSERT_NEAR(1.f, geometry.gu.length(), 1E-4);
//		ASSERT_NEAR(1.f, geometry.su.length(), 1E-4);
//		ASSERT_NEAR(0.f, dot(geometry.gn, geometry.gu), 1E-4);
//		ASSERT_NEAR(0.f, dot(geometry.sn, geometry.su), 1E-4);
//	}
//}
//
///*
// * Tests that the shape's boundary contains the sampled points.
// */
//TEST(Shape_BumpMapped, boundary) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto bumpMap = PerlinNoise3DNode<LUCIFER_SHADING_INPUT>::build(pool, PositionNode::build(pool), -1E-1f, +1E-2f, 8, 8, 8, 0);
//	auto s = Pointer<Shape>{BumpMappedShape::build(pool, bumpMap, Sphere::build(pool, center, radius), 1E-4)};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Box boundary = s->boundary(RangeF(0.f, 1.f));
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		ASSERT_TRUE(boundary.contains(geometry.gp));
//	}
//}
//
///*
// * Tests that rays actually intersect the shape's surface, that the
// * intersecting rays' max range are correctly updated and that the geometry at
// * the intersection points are consistent.
// */
//TEST(Shape_BumpMapped, intersection) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto bumpMap = PerlinNoise3DNode<LUCIFER_SHADING_INPUT>::build(pool, PositionNode::build(pool), -1E-1f, +1E-2f, 8, 8, 8, 0);
//	auto s = Pointer<Shape>{BumpMappedShape::build(pool, bumpMap, Sphere::build(pool, center, radius), 1E-4)};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Box boundary = s->boundary(RangeF(0.f, 1.f));
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		// samples a random ray origin within the bounding box.
//		rng->next(rnd);
//		Point3F origin = boundary.lower() + Vectr3F(rnd[0] * boundary.length(0), rnd[1] * boundary.length(1), rnd[2] * boundary.length(2));
//		ASSERT_TRUE(boundary.contains(origin));
//		// samples a point on the shape's surface to define the ray's direction.
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		Vectr3F direct = (geometry.gp - origin).normalize();
//		// asserts ray intersects shape
//		Shape::Intersection isect;
//		Ray ray(origin, direct, RangeF(0.f, +INFINITY), 0.f, rnd[2]);
//		ASSERT_TRUE(s->intersection(ray, &isect));
//		// asserts ray's range is correctly updated
//		ASSERT_NEAR(0.f, ray.r.lower, 1E-4);
//		ASSERT_NEAR((ray.o - isect.geometry.gp).length(), ray.r.upper, 1E-4);
//		// asserts intersection point is on the surface
//		ASSERT_NEAR(radius, (isect.geometry.gp - center).length(), 1E-4);
//		ASSERT_NEAR(1.f, isect.direction.length(), 1E-4);
//		// asserts geometric and shading normals
//		Vectr3F n = (isect.geometry.gp - center).normalize();
//		ASSERT_TRIPLET_NEAR(n, isect.geometry.gn, 1E-4);
//		ASSERT_NEAR(1.f, isect.geometry.sn.length(), 1E-4);
//		// asserts geometric and shading tangents
//		ASSERT_NEAR(1.f, isect.geometry.gu.length(), 1E-4);
//		ASSERT_NEAR(1.f, isect.geometry.su.length(), 1E-4);
//		ASSERT_NEAR(0.f, dot(isect.geometry.gn, isect.geometry.gu), 1E-4);
//		ASSERT_NEAR(0.f, dot(isect.geometry.sn, isect.geometry.su), 1E-4);
//	}
//}
//
///*
// * Asserts that the shape can correctly compute the random numbers that
// * generates points over it's surface.
// */
//TEST(Shape_BumpMapped, rnd) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3], rec[2];
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	auto bumpMap = PerlinNoise3DNode<LUCIFER_SHADING_INPUT>::build(pool, PositionNode::build(pool), -1E-1f, +1E-2f, 8, 8, 8, 0);
//	auto s = Pointer<Shape>{BumpMappedShape::build(pool, bumpMap, Sphere::build(pool, center, radius), 1E-4)};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		float u = rnd[0];
//		float v = rnd[1];
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		ASSERT_TRUE(s->canComputeRND());
//		s->rnd(rnd[2], geometry.gp, rec);
//		ASSERT_NEAR(u, rec[0], 1E-4);
//		ASSERT_NEAR(v, rec[1], 1E-4);
//	}
//}
//
///******************************************************************************
// * SHAPE SET SHAPE TESTS
// *****************************************************************************/
///*
// * Asserts that the surface area is correct.
// */
//TEST(Shape_ShapeSet, area) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	ShapeSet::array_t array(pool, 4);
//	array[0] = Sphere::build(pool, Point3F(-1.f, -1.f, 0.f), 1.f);
//	array[1] = Sphere::build(pool, Point3F(-1.f, +1.f, 0.f), 1.f);
//	array[2] = Sphere::build(pool, Point3F(+1.f, -1.f, 0.f), 1.f);
//	array[3] = Sphere::build(pool, Point3F(+1.f, +1.f, 0.f), 1.f);
//	auto s = Pointer<Shape>{ShapeSet::build(pool, array)};
//	ASSERT_NEAR(50.2654824574f, s->area(), 1E-4);
//	ASSERT_NEAR(50.2654824574f, s->area(0.25f), 1E-4);
//	ASSERT_NEAR(50.2654824574f, s->area(0.75f), 1E-4);
//}
//
/////*
//// * Asserts that sampled geometric and shading normals and tangent vectors are
//// * consistent.
//// */
////TEST(Shape_ShapeSet, sample) {
////	MemoryPool pool{4, new CPUAllocator{}};
////	float rnd[3];
////	ShapeSet::array_t array(pool, 4);
////	array[0] = Sphere::build(pool, Point3F(-1.f, -1.f, 0.f), 1.f);
////	array[1] = Sphere::build(pool, Point3F(-1.f, +1.f, 0.f), 1.f);
////	array[2] = Sphere::build(pool, Point3F(+1.f, -1.f, 0.f), 1.f);
////	array[3] = Sphere::build(pool, Point3F(+1.f, +1.f, 0.f), 1.f);
////	auto s = Pointer<Shape>{ShapeSet::build(pool, array)};
////	auto rng = PlainRNGFactory<>().build(pool, 3);
////	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
////		rng->next(rnd);
////		Geometry geometry;
////		s->sampleGeometry(rnd[2], rnd, &geometry);
////		// asserts geometric and shading normals
////		ASSERT_NEAR(1.f, geometry.gn.length(), 1E-4);
////		ASSERT_NEAR(1.f, geometry.sn.length(), 1E-4);
////		ASSERT_TRIPLET_NEAR(geometry.gn, geometry.sn, 1E-4);
////		// asserts geometric and shading tangents
////		ASSERT_NEAR(1.f, geometry.gu.length(), 1E-4);
////		ASSERT_NEAR(1.f, geometry.su.length(), 1E-4);
////		ASSERT_NEAR(0.f, dot(geometry.gn, geometry.gu), 1E-4);
////		ASSERT_NEAR(0.f, dot(geometry.sn, geometry.su), 1E-4);
////	}
////}
////
/////*
//// * Tests that the shape's boundary contains the sampled points.
//// */
////TEST(Shape_ShapeSet, boundary) {
////	MemoryPool pool{4, new CPUAllocator{}};
////	float rnd[3];
////	ShapeSet::array_t array(pool, 4);
////	array[0] = Sphere::build(pool, Point3F(-1.f, -1.f, 0.f), 1.f);
////	array[1] = Sphere::build(pool, Point3F(-1.f, +1.f, 0.f), 1.f);
////	array[2] = Sphere::build(pool, Point3F(+1.f, -1.f, 0.f), 1.f);
////	array[3] = Sphere::build(pool, Point3F(+1.f, +1.f, 0.f), 1.f);
////	auto s = Pointer<Shape>{ShapeSet::build(pool, array)};
////	auto rng = PlainRNGFactory<>().build(pool, 3);
////	Box boundary = s->boundary(RangeF(0.f, 1.f));
////	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
////		rng->next(rnd);
////		Geometry geometry;
////		s->sampleGeometry(rnd[2], rnd, &geometry);
////		ASSERT_TRUE(boundary.contains(geometry.gp));
////	}
////}
//
///*
// * Tests that rays actually intersect the shape's surface, that the
// * intersecting rays' max range are correctly updated and that the geometry at
// * the intersection points are consistent.
// */
//TEST(Shape_ShapeSet, intersection) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[3];
//	ShapeSet::array_t array(pool, 4);
//	array[0] = Sphere::build(pool, Point3F(-1.f, -1.f, 0.f), 1.f);
//	array[1] = Sphere::build(pool, Point3F(-1.f, +1.f, 0.f), 1.f);
//	array[2] = Sphere::build(pool, Point3F(+1.f, -1.f, 0.f), 1.f);
//	array[3] = Sphere::build(pool, Point3F(+1.f, +1.f, 0.f), 1.f);
//	auto s = Pointer<Shape>{ShapeSet::build(pool, array)};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Box boundary = s->boundary(RangeF(0.f, 1.f));
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		// samples a random ray origin within the bounding box.
//		rng->next(rnd);
//		Point3F origin = boundary.lower() + Vectr3F(rnd[0] * boundary.length(0), rnd[1] * boundary.length(1), rnd[2] * boundary.length(2));
//		ASSERT_TRUE(boundary.contains(origin));
//		// samples a point on the shape's surface to define the ray's direction.
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		Vectr3F direct = (geometry.gp - origin).normalize();
//		// asserts ray intersects shape
//		Shape::Intersection isect;
//		Ray ray(origin, direct, RangeF(0.f, +INFINITY), 0.f, rnd[2]);
//		ASSERT_TRUE(s->intersection(ray, &isect));
//		// asserts ray's range is correctly updated
//		ASSERT_NEAR(0.f, ray.r.lower, 1E-4);
//		ASSERT_NEAR((ray.o - isect.geometry.gp).length(), ray.r.upper, 1E-4);
//		ASSERT_NEAR(1.f, isect.direction.length(), 1E-4);
//		// asserts geometric and shading normals
//		ASSERT_NEAR(1.f, isect.geometry.gn.length(), 1E-4);
//		ASSERT_NEAR(1.f, isect.geometry.sn.length(), 1E-4);
//		ASSERT_TRIPLET_NEAR(isect.geometry.gn, isect.geometry.sn, 1E-4);
//		// asserts geometric and shading tangents
//		ASSERT_NEAR(1.f, isect.geometry.gu.length(), 1E-4);
//		ASSERT_NEAR(1.f, isect.geometry.su.length(), 1E-4);
//		ASSERT_NEAR(0.f, dot(isect.geometry.gn, isect.geometry.gu), 1E-4);
//		ASSERT_NEAR(0.f, dot(isect.geometry.sn, isect.geometry.su), 1E-4);
//	}
//}
//
///*
// * Asserts that the shape can not compute the random numbers that generates
// * points over it's surface.
// */
//TEST(Shape_ShapeSet, rnd) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	ShapeSet::array_t array(pool, 4);
//	array[0] = Sphere::build(pool, Point3F(-1.f, -1.f, 0.f), 1.f);
//	array[1] = Sphere::build(pool, Point3F(-1.f, +1.f, 0.f), 1.f);
//	array[2] = Sphere::build(pool, Point3F(+1.f, -1.f, 0.f), 1.f);
//	array[3] = Sphere::build(pool, Point3F(+1.f, +1.f, 0.f), 1.f);
//	auto s = Pointer<Shape>{ShapeSet::build(pool, array)};
//	ASSERT_FALSE(s->canComputeRND());
//}
//
///******************************************************************************
// * BVH SHAPE TESTS
// *****************************************************************************/
///*
// * Asserts that the surface area is correct.
// */
//TEST(Shape_BVHShape, area) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	BVHShape::array_t array(pool, 4);
//	array[0] = Sphere::build(pool, Point3F(-1.f, -1.f, 0.f), 1.f);
//	array[1] = Sphere::build(pool, Point3F(-1.f, +1.f, 0.f), 1.f);
//	array[2] = Sphere::build(pool, Point3F(+1.f, -1.f, 0.f), 1.f);
//	array[3] = Sphere::build(pool, Point3F(+1.f, +1.f, 0.f), 1.f);
//	auto s = Pointer<Shape>{BVHShape::build(pool, array)};
//	ASSERT_NEAR(50.2654824574f, s->area(), 1E-4);
//	ASSERT_NEAR(50.2654824574f, s->area(0.25f), 1E-4);
//	ASSERT_NEAR(50.2654824574f, s->area(0.75f), 1E-4);
//}
//
///*
// * Asserts that sampled geometric and shading normals and tangent vectors are
// * consistent.
// */
//TEST(Shape_BVHShape, sample) {
//	float rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	BVHShape::array_t array(pool, 4);
//	array[0] = Sphere::build(pool, Point3F(-1.f, -1.f, 0.f), 1.f);
//	array[1] = Sphere::build(pool, Point3F(-1.f, +1.f, 0.f), 1.f);
//	array[2] = Sphere::build(pool, Point3F(+1.f, -1.f, 0.f), 1.f);
//	array[3] = Sphere::build(pool, Point3F(+1.f, +1.f, 0.f), 1.f);
//	auto s = Pointer<Shape>{BVHShape::build(pool, array)};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		// asserts geometric and shading normals
//		ASSERT_NEAR(1.f, geometry.gn.length(), 1E-4);
//		ASSERT_NEAR(1.f, geometry.sn.length(), 1E-4);
//		ASSERT_TRIPLET_NEAR(geometry.gn, geometry.sn, 1E-4);
//		// asserts geometric and shading tangents
//		ASSERT_NEAR(1.f, geometry.gu.length(), 1E-4);
//		ASSERT_NEAR(1.f, geometry.su.length(), 1E-4);
//		ASSERT_NEAR(0.f, dot(geometry.gn, geometry.gu), 1E-4);
//		ASSERT_NEAR(0.f, dot(geometry.sn, geometry.su), 1E-4);
//	}
//}
//
///*
// * Tests that the shape's boundary contains the sampled points.
// */
//TEST(Shape_BVHShape, boundary) {
//	float rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	BVHShape::array_t array(pool, 4);
//	array[0] = Sphere::build(pool, Point3F(-1.f, -1.f, 0.f), 1.f);
//	array[1] = Sphere::build(pool, Point3F(-1.f, +1.f, 0.f), 1.f);
//	array[2] = Sphere::build(pool, Point3F(+1.f, -1.f, 0.f), 1.f);
//	array[3] = Sphere::build(pool, Point3F(+1.f, +1.f, 0.f), 1.f);
//	auto s = Pointer<Shape>{BVHShape::build(pool, array)};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Box boundary = s->boundary(RangeF(0.f, 1.f));
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		ASSERT_TRUE(boundary.contains(geometry.gp));
//	}
//}
//
///*
// * Tests that rays actually intersect the shape's surface, that the
// * intersecting rays' max range are correctly updated and that the geometry at
// * the intersection points are consistent.
// */
//TEST(Shape_BVHShape, intersection) {
//	float rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	BVHShape::array_t array(pool, 4);
//	array[0] = Sphere::build(pool, Point3F(-1.f, -1.f, 0.f), 1.f);
//	array[1] = Sphere::build(pool, Point3F(-1.f, +1.f, 0.f), 1.f);
//	array[2] = Sphere::build(pool, Point3F(+1.f, -1.f, 0.f), 1.f);
//	array[3] = Sphere::build(pool, Point3F(+1.f, +1.f, 0.f), 1.f);
//	auto s = Pointer<Shape>{BVHShape::build(pool, array)};
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Box boundary = s->boundary(RangeF(0.f, 1.f));
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		// samples a random ray origin within the bounding box.
//		rng->next(rnd);
//		Point3F origin = boundary.lower() + Vectr3F(rnd[0] * boundary.length(0), rnd[1] * boundary.length(1), rnd[2] * boundary.length(2));
//		ASSERT_TRUE(boundary.contains(origin));
//		// samples a point on the shape's surface to define the ray's direction.
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		Vectr3F direct = (geometry.gp - origin).normalize();
//		// asserts ray intersects shape
//		Shape::Intersection isect;
//		Ray ray(origin, direct, RangeF(0.f, +INFINITY), 0.f, rnd[2]);
//		ASSERT_TRUE(s->intersection(ray, &isect));
//		// asserts ray's range is correctly updated
//		ASSERT_NEAR(0.f, ray.r.lower, 1E-4);
//		ASSERT_NEAR((ray.o - isect.geometry.gp).length(), ray.r.upper, 1E-4);
//		ASSERT_NEAR(1.f, isect.direction.length(), 1E-4);
//		// asserts geometric and shading normals
//		ASSERT_NEAR(1.f, isect.geometry.gn.length(), 1E-4);
//		ASSERT_NEAR(1.f, isect.geometry.sn.length(), 1E-4);
//		ASSERT_TRIPLET_NEAR(isect.geometry.gn, isect.geometry.sn, 1E-4);
//		// asserts geometric and shading tangents
//		ASSERT_NEAR(1.f, isect.geometry.gu.length(), 1E-4);
//		ASSERT_NEAR(1.f, isect.geometry.su.length(), 1E-4);
//		ASSERT_NEAR(0.f, dot(isect.geometry.gn, isect.geometry.gu), 1E-4);
//		ASSERT_NEAR(0.f, dot(isect.geometry.sn, isect.geometry.su), 1E-4);
//	}
//}
//
///*
// * Asserts that the shape can not compute the random numbers that generates
// * points over it's surface.
// */
//TEST(Shape_BVHShape, rnd) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	BVHShape::array_t array(pool, 4);
//	array[0] = Sphere::build(pool, Point3F(-1.f, -1.f, 0.f), 1.f);
//	array[1] = Sphere::build(pool, Point3F(-1.f, +1.f, 0.f), 1.f);
//	array[2] = Sphere::build(pool, Point3F(+1.f, -1.f, 0.f), 1.f);
//	array[3] = Sphere::build(pool, Point3F(+1.f, +1.f, 0.f), 1.f);
//	auto s = Pointer<Shape>{BVHShape::build(pool, array)};
//	ASSERT_FALSE(s->canComputeRND());
//}
//
///******************************************************************************
// * MESH TRIANGLE SHAPE TESTS
// *****************************************************************************/
///*
// * Asserts that the surface area is correct.
// */
//TEST(Shape_MeshTriangle, area) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	Point3F a(1.f, 0.f, 0.f);
//	Point3F b(0.f, 1.f, 0.f);
//	Point3F c(0.f, 0.f, 1.f);
//	Data data;
//	data.vrtx.push_back(a);
//	data.vrtx.push_back(b);
//	data.vrtx.push_back(c);
//	auto meshVrtxData = Pointer<const MeshVrtxData>(MeshVrtxData::build(pool, data));
//	auto mesh = Mesh();
//	mesh.vrtx.emplace_back(0, 1, 2);
//	auto meshFaceData = Pointer<const MeshFaceData>{MeshFaceData::build(pool, meshVrtxData, mesh)};
//	std::vector<Pointer<const Shape>> triangles;
//	buildTriangleMesh(pool, meshFaceData, triangles);
//	auto s = triangles[0];
//	//https://en.wikipedia.org/wiki/Triangle#Using_vectors
//	float area = 0.5 * cross(b - a, c - a).length();
//	ASSERT_NEAR(area, s->area(), 1E-4);
//	ASSERT_NEAR(area, s->area(0.25f), 1E-4);
//	ASSERT_NEAR(area, s->area(0.75f), 1E-4);
//}
//
///*
// * Asserts that sampled points are actually on the shape's surface and that
// * geometric and shading normals and tangent vectors are consistent.
// */
//TEST(Shape_MeshTriangle, sample) {
//	float rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Point3F a(1.f, 0.f, 0.f);
//	Point3F b(0.f, 1.f, 0.f);
//	Point3F c(0.f, 0.f, 1.f);
//	Data data;
//	data.vrtx.push_back(a);
//	data.vrtx.push_back(b);
//	data.vrtx.push_back(c);
//	auto meshVrtxData = Pointer<const MeshVrtxData>(MeshVrtxData::build(pool, data));
//	auto mesh = Mesh();
//	mesh.vrtx.emplace_back(0, 1, 2);
//	auto meshFaceData = Pointer<const MeshFaceData>{MeshFaceData::build(pool, meshVrtxData, mesh)};
//	std::vector<Pointer<const Shape>> triangles;
//	buildTriangleMesh(pool, meshFaceData, triangles);
//	auto s = triangles[0];
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Vectr3F gn = cross(b - a, c - a).normalize();
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		// asserts sampled point is on the triangle's plane
//		ASSERT_NEAR(0.f, dot(gn, geometry.gp - a), 1E-4);
//		// asserts sampled point's angles are smaller than vertexes's angles
//		ASSERT_TRUE(dot((b - a).normalize(), (c - a).normalize()) <= dot((b - a).normalize(), (geometry.gp - a).normalize()));
//		ASSERT_TRUE(dot((a - b).normalize(), (c - b).normalize()) <= dot((a - b).normalize(), (geometry.gp - b).normalize()));
//		ASSERT_TRUE(dot((b - c).normalize(), (a - c).normalize()) <= dot((b - c).normalize(), (geometry.gp - c).normalize()));
//		// asserts geometric and shading normals
//		ASSERT_TRIPLET_NEAR(gn, geometry.gn, 1E-4);
//		ASSERT_TRIPLET_NEAR(gn, geometry.sn, 1E-4);
//		// asserts geometric and shading tangents
//		ASSERT_NEAR(1.f, geometry.gu.length(), 1E-4);
//		ASSERT_NEAR(1.f, geometry.su.length(), 1E-4);
//		ASSERT_NEAR(0.f, dot(geometry.gn, geometry.gu), 1E-4);
//		ASSERT_NEAR(0.f, dot(geometry.sn, geometry.su), 1E-4);
//	}
//}
//
///*
// * Tests that the shape's boundary contains the sampled points.
// */
//TEST(Shape_MeshTriangle, boundary) {
//	float rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Point3F a(1.f, 0.f, 0.f);
//	Point3F b(0.f, 1.f, 0.f);
//	Point3F c(0.f, 0.f, 1.f);
//	Data data;
//	data.vrtx.push_back(a);
//	data.vrtx.push_back(b);
//	data.vrtx.push_back(c);
//	auto meshVrtxData = Pointer<const MeshVrtxData>(MeshVrtxData::build(pool, data));
//	auto mesh = Mesh();
//	mesh.vrtx.emplace_back(0, 1, 2);
//	auto meshFaceData = Pointer<const MeshFaceData>{MeshFaceData::build(pool, meshVrtxData, mesh)};
//	std::vector<Pointer<const Shape>> triangles;
//	buildTriangleMesh(pool, meshFaceData, triangles);
//	auto s = triangles[0];
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Box boundary = s->boundary(RangeF(0.f, 1.f));
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		ASSERT_TRUE(boundary.contains(geometry.gp));
//	}
//}
//
///*
// * Tests that rays actually intersect the shape's surface, that the
// * intersecting rays' max range are correctly updated and that the geometry at
// * the intersection points are consistent.
// */
//TEST(Shape_MeshTriangle, intersection) {
//	float rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Point3F a(1.f, 0.f, 0.f);
//	Point3F b(0.f, 1.f, 0.f);
//	Point3F c(0.f, 0.f, 1.f);
//	Data data;
//	data.vrtx.push_back(a);
//	data.vrtx.push_back(b);
//	data.vrtx.push_back(c);
//	auto meshVrtxData = Pointer<const MeshVrtxData>(MeshVrtxData::build(pool, data));
//	auto mesh = Mesh();
//	mesh.vrtx.emplace_back(0, 1, 2);
//	auto meshFaceData = Pointer<const MeshFaceData>{MeshFaceData::build(pool, meshVrtxData, mesh)};
//	std::vector<Pointer<const Shape>> triangles;
//	buildTriangleMesh(pool, meshFaceData, triangles);
//	auto s = triangles[0];
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	Box boundary = s->boundary(RangeF(0.f, 1.f));
//	Vectr3F gn = cross(b - a, c - a).normalize();
//	for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//		// samples a random ray origin within the bounding box.
//		rng->next(rnd);
//		Point3F origin = boundary.lower() + Vectr3F(rnd[0] * boundary.length(0), rnd[1] * boundary.length(1), rnd[2] * boundary.length(2));
//		ASSERT_TRUE(boundary.contains(origin));
//		// samples a point on the shape's surface to define the ray's direction.
//		rng->next(rnd);
//		Geometry geometry;
//		s->sampleGeometry(rnd[2], rnd, &geometry);
//		Vectr3F direct = (geometry.gp - origin).normalize();
//		// asserts ray intersects shape
//		Shape::Intersection isect;
//		Ray ray(origin, direct, RangeF(0.f, +INFINITY), 0.f, rnd[2]);
//		ASSERT_TRUE(s->intersection(ray, &isect));
//		// asserts ray's range is correctly updated
//		ASSERT_NEAR(0.f, ray.r.lower, 1E-4);
//		ASSERT_NEAR((ray.o - isect.geometry.gp).length(), ray.r.upper, 1E-4);
//		ASSERT_NEAR(1.f, isect.direction.length(), 1E-4);
//		// asserts intersection point is on the surface
//		ASSERT_NEAR(0.f, dot(gn, isect.geometry.gp - a), 1E-4);
//		ASSERT_TRUE(dot((b - a).normalize(), (c - a).normalize()) <= dot((b - a).normalize(), (isect.geometry.gp - a).normalize()));
//		ASSERT_TRUE(dot((a - b).normalize(), (c - b).normalize()) <= dot((a - b).normalize(), (isect.geometry.gp - b).normalize()));
//		ASSERT_TRUE(dot((b - c).normalize(), (a - c).normalize()) <= dot((b - c).normalize(), (isect.geometry.gp - c).normalize()));
//		// asserts geometric and shading normals
//		ASSERT_TRIPLET_NEAR(gn, isect.geometry.gn, 1E-4);
//		ASSERT_TRIPLET_NEAR(gn, isect.geometry.sn, 1E-4);
//		// asserts geometric and shading tangents
//		ASSERT_NEAR(1.f, isect.geometry.gu.length(), 1E-4);
//		ASSERT_NEAR(1.f, isect.geometry.su.length(), 1E-4);
//		ASSERT_NEAR(0.f, dot(isect.geometry.gn, isect.geometry.gu), 1E-4);
//		ASSERT_NEAR(0.f, dot(isect.geometry.sn, isect.geometry.su), 1E-4);
//	}
//}
//
///*
// * Asserts that the shape can correctly compute the random numbers that
// * generates points over it's surface.
// */
//TEST(Shape_MeshTriangle, rnd) {
//	{
//		// xy plane
//		float rnd[3], rec[2];
//		MemoryPool pool{4, new CPUAllocator{}};
//		Point3F a(0.f, 0.f, 0.f);
//		Point3F b(1.f, 0.f, 0.f);
//		Point3F c(0.f, 1.f, 0.f);
//		Data data;
//		data.vrtx.push_back(a);
//		data.vrtx.push_back(b);
//		data.vrtx.push_back(c);
//		auto meshVrtxData = Pointer<const MeshVrtxData>(MeshVrtxData::build(pool, data));
//		auto mesh = Mesh();
//		mesh.vrtx.emplace_back(0, 1, 2);
//		auto meshFaceData = Pointer<const MeshFaceData>{MeshFaceData::build(pool, meshVrtxData, mesh)};
//		std::vector<Pointer<const Shape>> triangles;
//		buildTriangleMesh(pool, meshFaceData, triangles);
//		auto s = triangles[0];
//		Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//		for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//			rng->next(rnd);
//			float u = rnd[0];
//			float v = rnd[1];
//			Geometry geometry;
//			s->sampleGeometry(rnd[2], rnd, &geometry);
//			ASSERT_TRUE(s->canComputeRND());
//			s->rnd(rnd[2], geometry.gp, rec);
//			ASSERT_NEAR(u, rec[0], 1E-4);
//			ASSERT_NEAR(v, rec[1], 1E-4);
//		}
//	}
//	{
//		// yz plane
//		float rnd[3], rec[2];
//		MemoryPool pool{4, new CPUAllocator{}};
//		Point3F a(0.f, 0.f, 0.f);
//		Point3F b(0.f, 1.f, 0.f);
//		Point3F c(0.f, 0.f, 1.f);
//		Data data;
//		data.vrtx.push_back(a);
//		data.vrtx.push_back(b);
//		data.vrtx.push_back(c);
//		auto meshVrtxData = Pointer<const MeshVrtxData>(MeshVrtxData::build(pool, data));
//		auto mesh = Mesh();
//		mesh.vrtx.emplace_back(0, 1, 2);
//		auto meshFaceData = Pointer<const MeshFaceData>{MeshFaceData::build(pool, meshVrtxData, mesh)};
//		std::vector<Pointer<const Shape>> triangles;
//		buildTriangleMesh(pool, meshFaceData, triangles);
//		auto s = triangles[0];
//		Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//		for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//			rng->next(rnd);
//			float u = rnd[0];
//			float v = rnd[1];
//			Geometry geometry;
//			s->sampleGeometry(rnd[2], rnd, &geometry);
//			ASSERT_TRUE(s->canComputeRND());
//			s->rnd(rnd[2], geometry.gp, rec);
//			ASSERT_NEAR(u, rec[0], 1E-4);
//			ASSERT_NEAR(v, rec[1], 1E-4);
//		}
//	}
//	{
//		// zx plane
//		float rnd[3], rec[2];
//		MemoryPool pool{4, new CPUAllocator{}};
//		Point3F a(0.f, 0.f, 0.f);
//		Point3F b(0.f, 0.f, 1.f);
//		Point3F c(1.f, 0.f, 0.f);
//		Data data;
//		data.vrtx.push_back(a);
//		data.vrtx.push_back(b);
//		data.vrtx.push_back(c);
//		auto meshVrtxData = Pointer<const MeshVrtxData>(MeshVrtxData::build(pool, data));
//		auto mesh = Mesh();
//		mesh.vrtx.emplace_back(0, 1, 2);
//		auto meshFaceData = Pointer<const MeshFaceData>{MeshFaceData::build(pool, meshVrtxData, mesh)};
//		std::vector<Pointer<const Shape>> triangles;
//		buildTriangleMesh(pool, meshFaceData, triangles);
//		auto s = triangles[0];
//		Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//		for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//			rng->next(rnd);
//			float u = rnd[0];
//			float v = rnd[1];
//			Geometry geometry;
//			s->sampleGeometry(rnd[2], rnd, &geometry);
//			ASSERT_TRUE(s->canComputeRND());
//			s->rnd(rnd[2], geometry.gp, rec);
//			ASSERT_NEAR(u, rec[0], 1E-4);
//			ASSERT_NEAR(v, rec[1], 1E-4);
//		}
//	}
//	{
//		float rnd[3], rec[2];
//		MemoryPool pool{4, new CPUAllocator{}};
//		Point3F a(1.f, 0.f, 0.f);
//		Point3F b(0.f, 1.f, 0.f);
//		Point3F c(0.f, 0.f, 1.f);
//		Data data;
//		data.vrtx.push_back(a);
//		data.vrtx.push_back(b);
//		data.vrtx.push_back(c);
//		auto meshVrtxData = Pointer<const MeshVrtxData>(MeshVrtxData::build(pool, data));
//		auto mesh = Mesh();
//		mesh.vrtx.emplace_back(0, 1, 2);
//		auto meshFaceData = Pointer<const MeshFaceData>{MeshFaceData::build(pool, meshVrtxData, mesh)};
//		std::vector<Pointer<const Shape>> triangles;
//		buildTriangleMesh(pool, meshFaceData, triangles);
//		auto s = triangles[0];
//		Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//		for (int i = 0; i < SHAPE_SAMPLE_COUNT; ++i) {
//			rng->next(rnd);
//			float u = rnd[0];
//			float v = rnd[1];
//			Geometry geometry;
//			s->sampleGeometry(rnd[2], rnd, &geometry);
//			ASSERT_TRUE(s->canComputeRND());
//			s->rnd(rnd[2], geometry.gp, rec);
//			ASSERT_NEAR(u, rec[0], 1E-4);
//			ASSERT_NEAR(v, rec[1], 1E-4);
//		}
//	}
//}
//
//}}

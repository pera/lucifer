#include <gtest/gtest.h>
#include "lucifer/math/function/RegularGrid1D.cuh"
#include "lucifer/memory/CPUAllocator.cuh"
#include "lucifer/memory/MemoryPool.cuh"
#include "lucifer/memory/Pointer.cuh"

namespace lucifer { namespace test {

TEST(RegularGrid1D, evaluateOnCPU) {
	MemoryPool pool{4, new CPUAllocator{}};
	RegularGrid1D::dim_t n = 2;
	RegularGrid1D::array samples(pool, n);
	samples[0] = 1.f;
	samples[1] = 2.f;
	auto f = RegularGrid1D::build(pool, 0.f, 10.f, samples);
	auto val = f->evaluate(0.f, 10.f);
	ASSERT_FLOAT_EQ(1.0f, val.lower);
	ASSERT_FLOAT_EQ(2.0f, val.upper);
	ASSERT_FLOAT_EQ(1.5f, (*f)(5.f));
}

}}

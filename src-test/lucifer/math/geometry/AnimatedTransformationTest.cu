//#include <gtest/gtest.h>
//#include "lucifer/math/geometry/StaticTransformation.cuh"
//#include "lucifer/math/geometry/KeyFrameTransformation.cuh"
//#include "lucifer/memory/CPUAllocator.cuh"
//#include "lucifer/memory/MemoryPool.cuh"
//#include "lucifer/memory/Pointer.cuh"
//#include "lucifer/random/generator/PlainRNG.cuh"
//#include "../../TestUtils.cuh"
//
//namespace lucifer { namespace test {
//
//constexpr int ANIMATED_SAMPLE_COUNT = 100;
//
///******************************************************************************
// * STATIC TRANSFORMATION TESTS
// *****************************************************************************/
//
///*
// * Asserts the transformation has unit scaling factors.
// */
//TEST(AnimatedTransformation_Static, unitScale) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto t = StaticTransformation::build(pool, Transformation<float, 3>::rotation<2, 0>(PI<float>() / 4.f));
//	ASSERT_TRUE(t->hasUnitScale());
//	ASSERT_TRUE(t->hasUniformScale());
//	float rnd;
//	auto rng = PlainRNGFactory<>().build(pool, 1);
//	for (int i = 0; i < ANIMATED_SAMPLE_COUNT; ++i) {
//		rng->next(&rnd);
//		ASSERT_TRIPLET_NEAR(Vectr3F(1.f, 1.f, 1.f), scale(t->atTime(rnd)), 1E-4f);
//	}
//}
//
///*
// * Asserts the transformation has uniform scaling factors.
// */
//TEST(AnimatedTransformation_Static, uniformScale) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto t = StaticTransformation::build(pool, Transformation<float, 3>::scaling(Vectr3F{2.f, 2.f, 2.f}));
//	ASSERT_FALSE(t->hasUnitScale());
//	ASSERT_TRUE(t->hasUniformScale());
//	float rnd;
//	auto rng = PlainRNGFactory<>().build(pool, 1);
//	for (int i = 0; i < ANIMATED_SAMPLE_COUNT; ++i) {
//		rng->next(&rnd);
//		auto factor = scale(t->atTime(rnd));
//		ASSERT_NEAR(factor[0], factor[1], 1E-4f);
//		ASSERT_NEAR(factor[1], factor[2], 1E-4f);
//	}
//}
//
///*
// * Asserts the transformation has non uniform scaling factors.
// */
//TEST(AnimatedTransformation_Static, nonUniformScale) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto t = StaticTransformation::build(pool, Transformation<float, 3>::scaling(Vectr3F{1.f, 2.f, 3.f}));
//	ASSERT_FALSE(t->hasUnitScale());
//	ASSERT_FALSE(t->hasUniformScale());
//}
//
///******************************************************************************
// * KEY FRAME TRANSFORMATION TESTS
// *****************************************************************************/
///*
// * Asserts the key frame polar matrix decomposition
// */
//TEST(AnimatedTransformation_KeyFrame, decomposition) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	auto rng = PlainRNGFactory<>().build(pool, 1);
//	for (int i = 0; i < ANIMATED_SAMPLE_COUNT; ++i) {
//		// generates random transformation
//		Vectr3F tvec;
//		rng->next(&tvec[0]); rng->next(&tvec[1]); rng->next(&tvec[2]);
//		tvec *= 3.f;
//		auto t = Affine3F::translation(tvec);
//		Vectr3F rvec;
//		rng->next(&rvec[0]); rng->next(&rvec[1]); rng->next(&rvec[2]);
//		rvec *= PI<float>();
//		auto r = Affine3F::rotation(0, 1, rvec[2]) * Affine3F::rotation(2, 0, rvec[1]) * Affine3F::rotation(1, 2, rvec[0]);
//		Vectr3F svec;
//		rng->next(&svec[0]); rng->next(&svec[1]); rng->next(&svec[2]);
//		svec += Vectr3F(1.1f, 1.1f, 1.1f);
//		auto s = Affine3F::scaling(svec);
//		// asserts the decomposition
//		Decomposition<float> d(t * r * s);
//		ASSERT_TRIPLET_NEAR(tvec, d.t, 1E-4f);
//		ASSERT_TRIPLET_NEAR(svec, Vectr3F(d.s[0][0], d.s[1][1], d.s[2][2]), 1E-4f);
//	}
//}
//
///*
// * Asserts the transformation has unit scaling factors.
// */
//TEST(AnimatedTransformation_KeyFrame, unitScale) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	using frame_t = KeyFrameTransformation::KeyFrame;
//	KeyFrameTransformation::array_t array(pool, 2);
//	array[0] = frame_t(0.f, Transformation<float, 3>::identity());
//	array[1] = frame_t(1.f, Transformation<float, 3>::rotation<2, 0>(PI<float>() / 4.f));
//	auto t = KeyFrameTransformation::build(pool, array);
//	ASSERT_TRUE(t->hasUnitScale());
//	ASSERT_TRUE(t->hasUniformScale());
//	float rnd;
//	auto rng = PlainRNGFactory<>().build(pool, 1);
//	for (int i = 0; i < ANIMATED_SAMPLE_COUNT; ++i) {
//		rng->next(&rnd);
//		ASSERT_TRIPLET_NEAR(Vectr3F(1.f, 1.f, 1.f), scale(t->atTime(rnd)), 1E-4f);
//	}
//}
//
///*
// * Asserts the transformation has uniform scaling factors.
// */
//TEST(AnimatedTransformation_KeyFrame, uniformScale) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	using frame_t = KeyFrameTransformation::KeyFrame;
//	KeyFrameTransformation::array_t array(pool, 2);
//	array[0] = frame_t(0.f, Transformation<float, 3>::identity());
//	array[1] = frame_t(1.f, Transformation<float, 3>::scaling(Vectr3F(2.f, 2.f, 2.f)));
//	auto t = KeyFrameTransformation::build(pool, array);
//	ASSERT_FALSE(t->hasUnitScale());
//	ASSERT_TRUE(t->hasUniformScale());
//	float rnd;
//	auto rng = PlainRNGFactory<>().build(pool, 1);
//	for (int i = 0; i < ANIMATED_SAMPLE_COUNT; ++i) {
//		rng->next(&rnd);
//		auto factor = scale(t->atTime(rnd));
//		ASSERT_NEAR(factor[0], factor[1], 1E-4f);
//		ASSERT_NEAR(factor[1], factor[2], 1E-4f);
//	}
//}
//
///*
// * Asserts the transformation has non uniform scaling factors.
// */
//TEST(AnimatedTransformation_KeyFrame, nonUniformScale) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	using frame_t = KeyFrameTransformation::KeyFrame;
//	KeyFrameTransformation::array_t array(pool, 2);
//	array[0] = frame_t(0.f, Transformation<float, 3>::identity());
//	array[1] = frame_t(1.f, Transformation<float, 3>::scaling(Vectr3F(1.f, 2.f, 3.f)));
//	auto t = KeyFrameTransformation::build(pool, array);
//	ASSERT_FALSE(t->hasUnitScale());
//	ASSERT_FALSE(t->hasUniformScale());
//}
//
//}}

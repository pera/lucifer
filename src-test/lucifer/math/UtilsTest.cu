//#include <gtest/gtest.h>
//#include "../TestUtils.cuh"
//#include "lucifer/math/Math.cuh"
//#include "lucifer/math/Util.cuh"
//#include "lucifer/math/Transformation.cuh"
//#include "lucifer/math/geometry/CommonTypes.cuh"
//#include "lucifer/memory/CPUAllocator.cuh"
//#include "lucifer/memory/MemoryPool.cuh"
//#include "lucifer/memory/Pointer.cuh"
//#include "lucifer/random/generator/PlainRNG.cuh"
//#include "lucifer/random/UniformSphereDistribution.cuh"
//
//namespace lucifer { namespace test {
//
//TEST(Reflect, specular) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float pdf, rnd[2];
//	Vectr3F wo, wi, wh, gn;
//	auto rng = PlainRNGFactory<>().build(pool, 2);
//	constexpr int n = 1000;
//	for (int i = 0; i < n; i++) {
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], &pdf);
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &gn[0], &pdf);
//		if ((wo - gn).lengthSquared() > 0.f) {
//			if (wo[2] * gn[2] < 0.f) gn = -gn;
//			wi = reflect(wo, gn);
//			ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//			ASSERT_NEAR(dot(wo, gn), dot(wi, gn), 1E-4f);
//			wh = (wi + wo).normalize();
//			if (wo[2] * wh[2] < 0.f) wh = -wh;
//			ASSERT_TRIPLET_NEAR(gn, wh, 1E-4);
//		}
//	}
//}
//
//TEST(Refract, specular) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	int n = 100;
//	float rnd[3];
//	Vectr3F wo, wi;
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	float ior1, ior2;
//	// ior: 1.0 -> 1.5
//	ior1 = 1.f; ior2 = 1.5;
//	for (int i = 0; i < n; i++) {
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], nullptr);
//		if (refract(wo, ior1 / ior2, &wi)) {
//			ASSERT_FALSE(sameHemisphere(wo, wi));
//			ASSERT_NEAR(sinTheta(wo) * ior1, sinTheta(wi) * ior2, 1E-4);
//		} else {
//			// total internal reflection
//			ASSERT_GE(asin(sinTheta(wo)), asin(ior2 / ior1));
//		}
//	}
//	// ior: 1.5 -> 1.0;
//	ior1 = 1.5f; ior2 = 1.f;
//	for (int i = 0; i < n; i++) {
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], nullptr);
//		if (refract(wo, ior1 / ior2, &wi)) {
//			ASSERT_FALSE(sameHemisphere(wo, wi));
//			ASSERT_NEAR(sinTheta(wo) * ior1, sinTheta(wi) * ior2, 1E-4);
//		} else {
//			// total internal reflection
//			ASSERT_GE(asin(sinTheta(wo)), asin(ior2 / ior1));
//		}
//	}
//}
//
//TEST(Refract, glossy) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	int n = 100;
//	float rnd[3];
//	Vectr3F wo, wi, wh = Vectr3F(0, 0, 1);
//	auto rng = PlainRNGFactory<>().build(pool, 3);
//	float ior1, ior2;
//	// ior: 1.0 -> 1.5
//	ior1 = 1.f; ior2 = 1.5;
//	for (int i = 0; i < n; i++) {
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], nullptr);
//		if (!sameHemisphere(wo, wh)) {
//			wh = -wh;
//		}
//		if (refract(wo, wh, ior1 / ior2, &wi)) {
//			ASSERT_FALSE(sameHemisphere(wo, wi));
//			ASSERT_NEAR(sinTheta(wo) * ior1, sinTheta(wi) * ior2, 1E-4);
//			Vectr3F wh_ = (wi + wo * (ior1 / ior2)).normalize();
//			if (!sameHemisphere(wo, wh_)) {
//				wh_ = -wh_;
//			}
//			ASSERT_EQ(wh, wh_);
//		} else {
//			// total internal reflection
//			ASSERT_GE(asin(sinTheta(wo)), asin(ior2 / ior1));
//		}
//	}
//	// ior: 1.5 -> 1.0;
//	ior1 = 1.5f; ior2 = 1.f;
//	for (int i = 0; i < n; i++) {
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], nullptr);
//		if (!sameHemisphere(wo, wh)) {
//			wh = -wh;
//		}
//		if (refract(wo, wh, ior1 / ior2, &wi)) {
//			ASSERT_FALSE(sameHemisphere(wo, wi));
//			ASSERT_NEAR(sinTheta(wo) * ior1, sinTheta(wi) * ior2, 1E-4);
//			Vectr3F wh_ = (wi + wo * (ior1 / ior2)).normalize();
//			if (!sameHemisphere(wo, wh_)) {
//				wh_ = -wh_;
//			}
//			ASSERT_EQ(wh, wh_);
//		} else {
//			// total internal reflection
//			ASSERT_GE(asin(sinTheta(wo)), asin(ior2 / ior1));
//		}
//	}
//}
//
//TEST(Shading, coordinates) {
//	for (int theta = 0; theta <= 180; theta += 9) {
//		for (int phi = -180; phi < 180; phi += 18) {
//			auto rt = Transformation<float, 3>::rotation<1, 2>(toRadians((float)theta));
//			auto rp = Transformation<float, 3>::rotation<0, 1>(toRadians((float)phi));
//			auto r = rp * rt;
//			Norml3F gn = r * Norml3F(0, 0, 1);
//			Vectr3F gu = r * Vectr3F(1, 0, 0);
//			Vectr3F gv = cross<Norml3F, Vectr3F, Vectr3F>(gn, gu);
//			ASSERT_NEAR(0.f, dot(gu, gv), 1E-4f);
//			ASSERT_NEAR(0.f, dot(gv, gn), 1E-4f);
//			ASSERT_NEAR(0.f, dot(gn, gu), 1E-4f);
//
//			Vectr3F lu, lv, ln;
//			toShadingCoordinates(gu, gv, gn, gu, lu);
//			toShadingCoordinates(gu, gv, gn, gv, lv);
//			toShadingCoordinates(gu, gv, gn, Vectr3F{gn}, ln);
//			ASSERT_TRIPLET_NEAR(Vectr3F(1, 0, 0), lu, 1E-4f);
//			ASSERT_TRIPLET_NEAR(Vectr3F(0, 1, 0), lv, 1E-4f);
//			ASSERT_TRIPLET_NEAR(Vectr3F(0, 0, 1), ln, 1E-4f);
//
//			Vectr3F wu, wv, wn;
//			toWorldCoordinates(gu, gv, gn, Vectr3F(1, 0, 0), wu);
//			toWorldCoordinates(gu, gv, gn, Vectr3F(0, 1, 0), wv);
//			toWorldCoordinates(gu, gv, gn, Vectr3F(0, 0, 1), wn);
//			ASSERT_TRIPLET_NEAR(gu, wu, 1E-4f);
//			ASSERT_TRIPLET_NEAR(gv, wv, 1E-4f);
//			ASSERT_TRIPLET_NEAR(gn, wn, 1E-4f);
//		}
//	}
//}
//
//}}

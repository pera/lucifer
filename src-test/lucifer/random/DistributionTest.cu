//#include <iostream>
//#include <gtest/gtest.h>
//#include "../TestUtils.cuh"
//#include "lucifer/math/Math.cuh"
//#include "lucifer/math/geometry/CommonTypes.cuh"
//#include "lucifer/memory/CPUAllocator.cuh"
//#include "lucifer/memory/MemoryPool.cuh"
//#include "lucifer/random/ConcentricDiskDistribution.cuh"
//#include "lucifer/random/CosineHemisphereDistribution.cuh"
//#include "lucifer/random/ExponentialDistribution.cuh"
//#include "lucifer/random/PiecewiseLinearDistribution1D.cuh"
//#include "lucifer/random/PiecewiseLinearDistribution2D.cuh"
//#include "lucifer/random/UniformConeDistribution.cuh"
//#include "lucifer/random/UniformHemisphereDistribution.cuh"
//#include "lucifer/random/UniformSphereDistribution.cuh"
//#include "lucifer/random/UniformTriangleDistribution.cuh"
//#include "lucifer/random/generator/PlainRNG.cuh"
//#include "lucifer/memory/Pointer.cuh"
//
//namespace lucifer { namespace test {
//
//constexpr int DISTRIBUTION_MIN_COUNT = 10000;
//constexpr float DISTRIBUTION_CONVERGENCE_ERROR = 1E-3f;
//
///******************************************************************************
// * UNIFORM SPHERE DISTRIBUTION TESTS
// *****************************************************************************/
///*
// * Asserts that the sampled values actually lies on the sphere's surface and
// * that the returned PDF is correct. This test does not garantee that the
// * resulting points are actually uniformly distributed over the entire sphere.
// */
//TEST(Distribution_UniformSphere, sample) {
//	float rnd[2], pdf;
//	constexpr float PDF = 1.f / (4.f * PI<float>());
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 2);
//	for(int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		Vectr3F smp;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &smp[0], &pdf);
//		ASSERT_NEAR(1.f, smp.length(), 1E-4);
//		ASSERT_NEAR(PDF, pdf, 1E-4);
//	}
//}
//
///*
// * Asserts that the `random` method actually inverts the `sample` method.
// */
//TEST(Distribution_UniformSphere, random) {
//	float rnd[2], rec[2];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 2);
//	for(int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		Vectr3F smp;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &smp[0], nullptr);
//		UniformSphereDistribution::random(&smp[0], rec);
//		ASSERT_NEAR(rnd[0], rec[0], 1E-4);
//		ASSERT_NEAR(rnd[1], rec[1], 1E-4);
//	}
//}
//
///*
// * Asserts that the `pdf` method returns the correct value.
// */
//TEST(Distribution_UniformSphere, pdf) {
//	float rnd[2];
//	constexpr float PDF = 1.f / (4.f * PI<float>());
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 2);
//	for(int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		Vectr3F smp;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &smp[0], nullptr);
//		ASSERT_NEAR(PDF, UniformSphereDistribution::pdf(&smp[0]), 1E-4f);
//	}
//}
//
///*
// * Asserts that the sampling method actually distributes points according to
// * the distributions PDF.
// */
//TEST(Distribution_UniformSphere, histogram) {
//	// histograms configuration
//	constexpr int N_THETA = 10;
//	constexpr int N_PHI = 10;
//	constexpr int SAMPLES = 1000;
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 2);
//	// builds the expected histogram from pdf.
//	float pdfHistogram[N_THETA][N_PHI];
//	for (int i = 0; i < N_THETA; i++)
//		for (int j = 0; j < N_PHI; j++)
//			pdfHistogram[i][j] = 0.f;
//	float rnd[2], pdfSum = 0.f;
//	for (int t = 0; t < SAMPLES; t++) {
//		for (int p = 0; p < SAMPLES; p++) {
//			rng->next(rnd);
//			float theta = (180.f * (t + rnd[0])) / SAMPLES;
//			float   phi = (360.f * (p + rnd[1])) / SAMPLES;
//			Vectr3F w = sphericalDirection<Vectr3F>(sin(toRadians(theta)), cos(toRadians(theta)), toRadians(phi));
//			float pdf = sin(toRadians(theta)) * UniformSphereDistribution::pdf(&w[0]);
//			pdfSum += pdf;
//			int thetaIndex = min(N_THETA - 1, static_cast<int>(theta * N_THETA / 180.f));
//			int   phiIndex = min(  N_PHI - 1, static_cast<int>(  phi *   N_PHI / 360.f));
//			pdfHistogram[thetaIndex][phiIndex] += pdf;
//		}
//	}
//	for (int i = 0; i < N_THETA; i++) {
//		for (int j = 0; j < N_PHI; j++) {
//			pdfHistogram[i][j] /= pdfSum;
//		}
//	}
//	// builds the sampled histogram.
//	float smpHistogram[N_THETA][N_PHI];
//	for (int i = 0; i < N_THETA; i++)
//		for (int j = 0; j < N_PHI; j++)
//			smpHistogram[i][j] = 0.f;
//	for (int i = 0; i < SAMPLES; ++i) {
//		for (int j = 0; j < SAMPLES; ++j) {
//			rng->next(rnd);
//			rnd[0] = (i + rnd[0]) / SAMPLES;
//			rnd[1] = (j + rnd[1]) / SAMPLES;
//			float pdf;
//			Vectr3F w;
//			UniformSphereDistribution::sample(rnd, &w[0], &pdf);
//			float theta = acos(w[2]);
//			int thetaIndex = min(N_THETA - 1, static_cast<int>(N_THETA * theta / M_PI));
//			float phi = atan2(w[1], w[0]);
//			if (phi < 0.f) phi += 2.f * M_PI;
//			int   phiIndex = min(  N_PHI - 1, static_cast<int>(N_PHI * phi / (2.f * M_PI)));
//			smpHistogram[thetaIndex][phiIndex]++;
//		}
//	}
//	for (int i = 0; i < N_THETA; i++) {
//		for (int j = 0; j < N_PHI; j++) {
//			smpHistogram[i][j] /= (SAMPLES * SAMPLES);
//		}
//	}
//	// asserts that the reconstructed histogram matches the expected one.
//	for (int i = 0; i < N_THETA; i++) {
//		for (int j = 0; j < N_PHI; j++) {
//			ASSERT_NEAR(pdfHistogram[i][j], smpHistogram[i][j], DISTRIBUTION_CONVERGENCE_ERROR);
//		}
//	}
//}
//
///******************************************************************************
// * UNIFORM HEMISPHERE DISTRIBUTION TESTS
// *****************************************************************************/
///*
// * Asserts that the sampled values actually lies on the hemisphere's surface
// * and that the returned PDF is correct. This test does not garantee that the
// * resulting points are actually uniformly distributed over the entire
// * hemisphere.
// */
//TEST(Distribution_UniformHemisphere, sample) {
//	float rnd[2], pdf;
//	constexpr float PDF = 1.f / (2.f * PI<float>());
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 2);
//	for(int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		Vectr3F smp;
//		rng->next(rnd);
//		UniformHemisphereDistribution::sample(rnd, &smp[0], &pdf);
//		ASSERT_LE(0.f, smp[2]);
//		ASSERT_NEAR(1.f, smp.length(), 1E-4);
//		ASSERT_NEAR(PDF, pdf, 1E-4);
//	}
//}
//
///*
// * Asserts that the `pdf` method returns the correct value.
// */
//TEST(Distribution_UniformHemisphere, pdf) {
//	float rnd[2];
//	constexpr float PDF = 1.f / (2.f * PI<float>());
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 2);
//	for(int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		Vectr3F smp;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &smp[0], nullptr);
//		ASSERT_NEAR(
//			smp[2] < 0.f ? 0.f : PDF,
//			UniformHemisphereDistribution::pdf(&smp[0]),
//			1E-4f
//		);
//	}
//}
//
///*
// * Asserts that the sampling method actually distributes points according to
// * the distributions PDF.
// */
//TEST(Distribution_UniformHemisphere, histogram) {
//	// histograms configuration
//	constexpr int N_THETA = 10;
//	constexpr int N_PHI = 10;
//	constexpr int SAMPLES = 1000;
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 2);
//	// builds the expected histogram from pdf.
//	float pdfHistogram[N_THETA][N_PHI];
//	for (int i = 0; i < N_THETA; i++)
//		for (int j = 0; j < N_PHI; j++)
//			pdfHistogram[i][j] = 0.f;
//	float rnd[2], pdfSum = 0.f;
//	for (int t = 0; t < SAMPLES; t++) {
//		for (int p = 0; p < SAMPLES; p++) {
//			rng->next(rnd);
//			float theta = (180.f * (t + rnd[0])) / SAMPLES;
//			float   phi = (360.f * (p + rnd[1])) / SAMPLES;
//			Vectr3F w = sphericalDirection<Vectr3F>(sin(toRadians(theta)), cos(toRadians(theta)), toRadians(phi));
//			float pdf = sin(toRadians(theta)) * UniformHemisphereDistribution::pdf(&w[0]);
//			pdfSum += pdf;
//			int thetaIndex = min(N_THETA - 1, static_cast<int>(theta * N_THETA / 180.f));
//			int   phiIndex = min(  N_PHI - 1, static_cast<int>(  phi *   N_PHI / 360.f));
//			pdfHistogram[thetaIndex][phiIndex] += pdf;
//		}
//	}
//	for (int i = 0; i < N_THETA; i++) {
//		for (int j = 0; j < N_PHI; j++) {
//			pdfHistogram[i][j] /= pdfSum;
//		}
//	}
//	// builds the sampled histogram.
//	float smpHistogram[N_THETA][N_PHI];
//	for (int i = 0; i < N_THETA; i++)
//		for (int j = 0; j < N_PHI; j++)
//			smpHistogram[i][j] = 0.f;
//	for (int i = 0; i < SAMPLES; ++i) {
//		for (int j = 0; j < SAMPLES; ++j) {
//			rng->next(rnd);
//			rnd[0] = (i + rnd[0]) / SAMPLES;
//			rnd[1] = (j + rnd[1]) / SAMPLES;
//			float pdf;
//			Vectr3F w;
//			UniformHemisphereDistribution::sample(rnd, &w[0], &pdf);
//			float theta = acos(w[2]);
//			int thetaIndex = min(N_THETA - 1, static_cast<int>(N_THETA * theta / M_PI));
//			float phi = atan2(w[1], w[0]);
//			if (phi < 0.f) phi += 2.f * M_PI;
//			int   phiIndex = min(  N_PHI - 1, static_cast<int>(N_PHI * phi / (2.f * M_PI)));
//			smpHistogram[thetaIndex][phiIndex]++;
//		}
//	}
//	for (int i = 0; i < N_THETA; i++) {
//		for (int j = 0; j < N_PHI; j++) {
//			smpHistogram[i][j] /= (SAMPLES * SAMPLES);
//		}
//	}
//	// asserts that the reconstructed histogram matches the expected one.
//	for (int i = 0; i < N_THETA; i++) {
//		for (int j = 0; j < N_PHI; j++) {
//			ASSERT_NEAR(pdfHistogram[i][j], smpHistogram[i][j], DISTRIBUTION_CONVERGENCE_ERROR);
//		}
//	}
//}
//
///******************************************************************************
// * 	CONCENTRIC DISK DISTRIBUTION TESTS
// *****************************************************************************/
///*
// * Asserts that the sampled values actually lies on the disk's surface and that
// * the returned PDF is correct. This test does not garantee that the resulting
// * points are actually uniformly distributed over the entire disk.
// */
//TEST(Distribution_ConcentricDisk, sample) {
//	float rnd[2], pdf;
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 2);
//	for(int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		Vectr3F smp(0.f, 0.f, 0.f);
//		rng->next(rnd);
//		ConcentricDiskDistribution::sample(rnd, &smp[0], &pdf);
//		ASSERT_GE(1.f, smp.length());
//		ASSERT_NEAR(INV_PI<float>(), pdf, 1E-4);
//	}
//}
//
///*
// * Asserts that the `pdf` method returns the correct value.
// */
//TEST(Distribution_ConcentricDisk, pdf) {
//	float rnd[2];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 2);
//	for(int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		Vectr3F smp(0.f, 0.f, 0.f);
//		rng->next(rnd);
//		ASSERT_NEAR(
//			smp.length() > 1.f ? 0.f : INV_PI<float>(),
//			ConcentricDiskDistribution::pdf(&smp[0]),
//			1E-4f
//		);
//	}
//}
//
///******************************************************************************
// * 	UNIFORM TRIANGLE DISTRIBUTION TESTS
// *****************************************************************************/
///*
// * Asserts that the sampled values actually lies on the triangle's surface and
// * that the returned barycentric coordinates ares correct. This test does not
// * garantee that the resulting points are actually uniformly distributed over
// * the entire triangle.
// */
//TEST(Distribution_UniformTriangle, sample) {
//	float rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	for(int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		// samples a valid scaled random triangle
//		Point3F a, b, c;
//		do {
//			rng->next(&a[0]);
//			rng->next(&b[0]);
//			rng->next(&c[0]);
//			a = Point3F(-2.f, -2.f, -2.f) + 4.f * a;
//			b = Point3F(-2.f, -2.f, -2.f) + 4.f * b;
//			c = Point3F(-2.f, -2.f, -2.f) + 4.f * c;
//		} while (triangleArea(a, b, c) <= 1E-4f);
//		// samples a position on the triangle
//		Vectr3F smp;
//		rng->next(rnd);
//		float b1, b2;
//		UniformTriangleDistribution::sample(a, b, c, rnd, &smp[0], &b1, &b2);
//		// asserts barycentric coordinates
//		ASSERT_LE(0.f, b1);
//		ASSERT_LE(0.f, b2);
//		ASSERT_GE(1.f, b1);
//		ASSERT_GE(1.f, b2);
//		// asserts point is on the triangle
//		Point3F expected = b1 * a + b2 * b + (1.f -(b1 + b2)) * c;
//		ASSERT_TRIPLET_NEAR(expected, smp, 1E-4);
//	}
//}
//
///*
// * Asserts that the `random` method actually inverts the `sample` method.
// */
//TEST(Distribution_UniformTriangle, random) {
//	float rnd[3], rec[2];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	for(int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		// samples a valid scaled random triangle
//		Point3F a, b, c;
//		do {
//			rng->next(&a[0]);
//			rng->next(&b[0]);
//			rng->next(&c[0]);
//			a = Point3F(-2.f, -2.f, -2.f) + 4.f * a;
//			b = Point3F(-2.f, -2.f, -2.f) + 4.f * b;
//			c = Point3F(-2.f, -2.f, -2.f) + 4.f * c;
//		} while (triangleArea(a, b, c) <= 1E-4f);
//		// samples a position on the triangle
//		Vectr3F smp;
//		rng->next(rnd);
//		float b1, b2;
//		Norml3F n = cross<Vectr3F, Vectr3F, Norml3F>(b - a, c - a).normalize();
//		UniformTriangleDistribution::sample(a, b, c, rnd, &smp[0], &b1, &b2);
//		// asserts rnd parameters from sampled position
//		UniformTriangleDistribution::random(a, b, c, n, Point3F{smp}, rec);
//		ASSERT_NEAR(rnd[0], rec[0], 1E-4);
//		ASSERT_NEAR(rnd[1], rec[1], 1E-4);
//	}
//}
//
///******************************************************************************
// * 	COSINE HEMISPHERE DISTRIBUTION TESTS
// *****************************************************************************/
///*
// * Asserts that the sampled values actually lies on the hemisphere's surface
// * and that the returned PDF is according to the `pdf` method. This test does
// * not garantee that the resulting points are correcly distributed over the
// * entire hemisphere.
// */
//TEST(Distribution_CosineHemisphere, sample) {
//	float rnd[2], pdf;
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 2);
//	for (int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		Vectr3F smp;
//		rng->next(rnd);
//		CosineHemisphereDistribution::sample(rnd, &smp[0], &pdf);
//		ASSERT_LE(0.f, smp[2]);
//		ASSERT_NEAR(1.f, smp.length(), 1E-4);
//		ASSERT_NEAR(CosineHemisphereDistribution::pdf(&smp[0]), pdf, 1E-4);
//	}
//}
//
///*
// * Asserts that the PDF is greater than zero an integrates to 1 over the entire
// * sphere of possivel directions.
// */
//TEST(Distribution_CosineHemisphere, pdf) {
//	float rnd[2], pdf, dPDF;
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 2);
//	float pdfSum = 0.f;
//	for (int i = 0; true; ++i) {
//		// updates pdfSum
//		Vectr3F w;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &w[0], &pdf);
//		dPDF = CosineHemisphereDistribution::pdf(&w[0]);
//		ASSERT_LE(0.f, dPDF);
//		pdfSum += dPDF / pdf;
//		// check if reached convergence
//		if (i >= DISTRIBUTION_MIN_COUNT) {
//			if (abs(1.f - pdfSum / (i + 1)) < DISTRIBUTION_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the sampling method actually distributes points according to
// * the distributions PDF.
// */
//TEST(Distribution_CosineHemisphere, histogram) {
//	// histograms configuration
//	constexpr int N_THETA = 10;
//	constexpr int N_PHI = 10;
//	constexpr int SAMPLES = 1000;
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 2);
//	// builds the expected histogram from pdf.
//	float pdfHistogram[N_THETA][N_PHI];
//	for (int i = 0; i < N_THETA; i++)
//		for (int j = 0; j < N_PHI; j++)
//			pdfHistogram[i][j] = 0.f;
//	float rnd[2], pdfSum = 0.f;
//	for (int t = 0; t < SAMPLES; t++) {
//		for (int p = 0; p < SAMPLES; p++) {
//			rng->next(rnd);
//			float theta = (180.f * (t + rnd[0])) / SAMPLES;
//			float   phi = (360.f * (p + rnd[1])) / SAMPLES;
//			Vectr3F w = sphericalDirection<Vectr3F>(sin(toRadians(theta)), cos(toRadians(theta)), toRadians(phi));
//			float pdf = sin(toRadians(theta)) * CosineHemisphereDistribution::pdf(&w[0]);
//			pdfSum += pdf;
//			int thetaIndex = min(N_THETA - 1, static_cast<int>(theta * N_THETA / 180.f));
//			int   phiIndex = min(  N_PHI - 1, static_cast<int>(  phi *   N_PHI / 360.f));
//			pdfHistogram[thetaIndex][phiIndex] += pdf;
//		}
//	}
//	for (int i = 0; i < N_THETA; i++) {
//		for (int j = 0; j < N_PHI; j++) {
//			pdfHistogram[i][j] /= pdfSum;
//		}
//	}
//	// builds the sampled histogram.
//	float smpHistogram[N_THETA][N_PHI];
//	for (int i = 0; i < N_THETA; i++)
//		for (int j = 0; j < N_PHI; j++)
//			smpHistogram[i][j] = 0.f;
//	for (int i = 0; i < SAMPLES; ++i) {
//		for (int j = 0; j < SAMPLES; ++j) {
//			rng->next(rnd);
//			rnd[0] = (i + rnd[0]) / SAMPLES;
//			rnd[1] = (j + rnd[1]) / SAMPLES;
//			float pdf;
//			Vectr3F w;
//			CosineHemisphereDistribution::sample(rnd, &w[0], &pdf);
//			float theta = acos(w[2]);
//			int thetaIndex = min(N_THETA - 1, static_cast<int>(N_THETA * theta / M_PI));
//			float phi = atan2(w[1], w[0]);
//			if (phi < 0.f) phi += 2.f * M_PI;
//			int   phiIndex = min(  N_PHI - 1, static_cast<int>(N_PHI * phi / (2.f * M_PI)));
//			smpHistogram[thetaIndex][phiIndex]++;
//		}
//	}
//	for (int i = 0; i < N_THETA; i++) {
//		for (int j = 0; j < N_PHI; j++) {
//			smpHistogram[i][j] /= (SAMPLES * SAMPLES);
//		}
//	}
//	// asserts that the reconstructed histogram matches the expected one.
//	for (int i = 0; i < N_THETA; i++) {
//		for (int j = 0; j < N_PHI; j++) {
//			ASSERT_NEAR(pdfHistogram[i][j], smpHistogram[i][j], DISTRIBUTION_CONVERGENCE_ERROR);
//		}
//	}
//}
//
///******************************************************************************
// * 	UNIFORM CONE DISTRIBUTION TESTS
// *****************************************************************************/
///*
// * Asserts that the sampled values actually lies on the spherical cap's surface
// * and that the returned PDF is correct. This test does not garantee that the
// * resulting points are uniformly distributed over the entire cap.
// * https://en.wikipedia.org/wiki/Spherical_cap#Volume_and_surface_area
// */
//TEST(Distribution_UniformCone, sample) {
//	float rnd[3], pdf;
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	for (int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		Vectr3F smp;
//		rng->next(rnd);
//		UniformConeDistribution::sample(rnd, rnd[2], &smp[0], &pdf);
//		ASSERT_LE(rnd[2], smp[2]);
//		ASSERT_NEAR(1.f, smp.length(), 1E-4);
//		ASSERT_NEAR(1.f / (2.f * PI<float>() * (1.f - rnd[2])), pdf, 1E-4);
//	}
//}
//
///*
// * Asserts that the `pdf` method returns the correct value.
// */
//TEST(Distribution_UniformCone, pdf) {
//	float rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	for(int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		Vectr3F smp;
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &smp[0], nullptr);
//		ASSERT_NEAR(
//			smp[2] < rnd[2] ? 0.f : 1.f / (2.f * PI<float>() * (1.f - rnd[2])),
//			UniformConeDistribution::pdf(&smp[0], rnd[2]),
//			1E-4f
//		);
//	}
//}
//
///******************************************************************************
// * EXPONENTIAL DISTRIBUTION TESTS
// *****************************************************************************/
///*
// * Asserts that the sampled values are positive and that the returned pdf is
// * correct. This test does not garantee that the sampled values are actually
// * exponentially distributed over the entire positive real numbers.
// */
//TEST(Distribution_Exponential, sample) {
//	float lambda, rnd, pdf;
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 1);
//	for(int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		rng->next(&rnd);
//		rng->next(&lambda);
//		float smp = ExponentialDistribution::sample(lambda, rnd, &pdf);
//		ASSERT_LE(0.f, smp);
//		ASSERT_NEAR(lambda * exp(-lambda * smp), pdf, 1E-4);
//	}
//}
//
///*
// * Asserts that the `pdf` method returns the correct value.
// */
//TEST(Distribution_Exponential, pdf) {
//	float lambda, rnd, pdf;
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 1);
//	for(int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		rng->next(&rnd);
//		rng->next(&lambda);
//		float smp = ExponentialDistribution::sample(lambda, rnd, nullptr);
//		pdf = ExponentialDistribution::pdf(lambda, smp);
//		ASSERT_NEAR(lambda * exp(-lambda * smp), pdf, 1E-4);
//	}
//}
//
///*
// * Asserts that the `cdf` method returns the correct value.
// */
//TEST(Distribution_Exponential, cdf) {
//	float lambda, rnd, cdf;
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 1);
//	for(int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		rng->next(&rnd);
//		rng->next(&lambda);
//		float smp = ExponentialDistribution::sample(lambda, rnd, nullptr);
//		cdf = ExponentialDistribution::cdf(lambda, smp);
//		ASSERT_NEAR(1.f - exp(-lambda * smp), cdf, 1E-4);
//	}
//}
//
///******************************************************************************
// * 1D PIECEWISE LINEAR DISTRIBUTION TESTS
// *****************************************************************************/
///*
// * Asserts that the sampled values are within the support and that the returned
// * pdf is non negative. This test does not garantee that the sampled values are
// * properly distributed over the entire support.
// */
//TEST(Distribution_PiecewiseLinear, sample) {
//	RangeF support;
//	float rnd, pdf;
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 1);
//	// samples a random support
//	do {
//		rng->next(&support.lower);
//		rng->next(&support.upper);
//		support.upper += support.lower;
//	} while (support.length() > 1E-4f);
//	rng->next(&rnd);
//	support *= 10.f * (rnd + .1f);
//	// samples random positive values;
//	PiecewiseLinearDistribution1D::array_t values(pool, 10);
//	for (PiecewiseLinearDistribution1D::index_t i = 0; i < values.size(); i++) {
//		rng->next(&rnd);
//		values[i] = rnd * 10;
//	}
//	Pointer<PiecewiseLinearDistribution1D> d = PiecewiseLinearDistribution1D::build(pool, support, values);
//	// asserts sampled values and pdf.
//	for(int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		rng->next(&rnd);
//		float smp = d->sample(rnd, &pdf);
//		ASSERT_LE(support.lower, smp);
//		ASSERT_GE(support.upper, smp);
//		ASSERT_LE(0.f, pdf);
//		ASSERT_NEAR(d->pdf(smp), pdf, 1E-4);
//	}
//}
//
///*
// * Asserts that the PDF is greater than zero an integrates to 1 over the entire
// * distribution's support.
// */
//TEST(Distribution_PiecewiseLinear, pdf) {
//	RangeF support;
//	float rnd, pdf, dPDF;
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 1);
//	// samples a random support
//	do {
//		rng->next(&support.lower);
//		rng->next(&support.upper);
//		support.upper += support.lower;
//	} while (support.length() > 1E-4f);
//	rng->next(&rnd);
//	support *= 10.f * (rnd + .1f);
//	// samples random positive values;
//	PiecewiseLinearDistribution1D::array_t values(pool, 10);
//	for (PiecewiseLinearDistribution1D::index_t i = 0; i < values.size(); i++) {
//		rng->next(&rnd);
//		values[i] = rnd * 10;
//	}
//	Pointer<PiecewiseLinearDistribution1D> d = PiecewiseLinearDistribution1D::build(pool, support, values);
//	// integrates pdf
//	float pdfSum = 0.f;
//	pdf = 1.f / support.length();
//	for (int i = 0; true; ++i) {
//		// updates pdfSum
//		rng->next(&rnd);
//		rnd = support.lower + rnd * support.length();
//		dPDF = d->pdf(rnd);
//		ASSERT_LE(0.f, dPDF);
//		pdfSum += dPDF / pdf;
//		// check if reached convergence
//		if (i >= DISTRIBUTION_MIN_COUNT) {
//			if (abs(1.f - pdfSum / (i + 1)) < DISTRIBUTION_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*!
// * Asserts that the CDF is non negative and non decreasing.
// */
//TEST(Distribution_PiecewiseLinear, cdf) {
//	float rnd;
//	RangeF support;
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 1);
//	// samples a random support
//	do {
//		rng->next(&support.lower);
//		rng->next(&support.upper);
//		support.upper += support.lower;
//	} while (support.length() > 1E-4f);
//	rng->next(&rnd);
//	support *= 10.f * (rnd + .1f);
//	// samples random positive values;
//	PiecewiseLinearDistribution1D::array_t values(pool, 10);
//	for (PiecewiseLinearDistribution1D::index_t i = 0; i < values.size(); i++) {
//		rng->next(&rnd);
//		values[i] = rnd * 10;
//	}
//	Pointer<PiecewiseLinearDistribution1D> d = PiecewiseLinearDistribution1D::build(pool, support, values);
//	float x0, x1, cdf0, cdf1;
//	for(int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		rng->next(&x0);
//		rng->next(&x1);
//		x0 = support.lower + x0 * support.length();
//		x1 = support.lower + x1 * support.length();
//		cdf0 = d->cdf(x0);
//		cdf1 = d->cdf(x1);
//		ASSERT_LE(0.f, cdf0);
//		ASSERT_LE(0.f, cdf1);
//		ASSERT_EQ(x0 <= x1, cdf0 <= cdf1);
//	}
//}
//
///*
// * Asserts that integrating the PDF between in the interval [a, b] quals
// * CDF(b) - CDF(a), the probability of sampling in [a, b].
// */
//TEST(Distribution_PiecewiseLinear, probabilty) {
//	float rnd, pdf, dPDF;
//	RangeF support;
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 1);
//	// samples a random support
//	do {
//		rng->next(&support.lower);
//		rng->next(&support.upper);
//		support.upper += support.lower;
//	} while (support.length() > 1E-4f);
//	rng->next(&rnd);
//	support *= 10.f * (rnd + .1f);
//	// samples random positive values;
//	PiecewiseLinearDistribution1D::array_t values(pool, 10);
//	for (PiecewiseLinearDistribution1D::index_t i = 0; i < values.size(); i++) {
//		rng->next(&rnd);
//		values[i] = rnd * 10;
//	}
//	Pointer<PiecewiseLinearDistribution1D> d = PiecewiseLinearDistribution1D::build(pool, support, values);
//	// sampled a random interval within the support;
//	rng->next(&rnd);
//	float a = support.lower + rnd * support.length();
//	rng->next(&rnd);
//	float b = support.lower + rnd * support.length();
//	if (b < a) interchange(a, b);
//	// compute the probability
//	float p = d->cdf(b) - d->cdf(a);
//	// integrates the PDF in [a, b] until convergence
//	float pdfSum = 0.f;
//	pdf = 1.f / (b - a);
//	for (int i = 0; true; ++i) {
//		// updates pdfSum
//		rng->next(&rnd);
//		rnd = a + rnd * (b - a);
//		dPDF = d->pdf(rnd);
//		ASSERT_LE(0.f, dPDF);
//		pdfSum += dPDF / pdf;
//		// check if reached convergence
//		if (i >= DISTRIBUTION_MIN_COUNT) {
//			if (abs(p - pdfSum / (i + 1)) < DISTRIBUTION_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the quantile actually invertes the CDF.
// */
//TEST(Distribution_PiecewiseLinear, quantile) {
//	float rnd;
//	RangeF support;
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 1);
//	// samples a random support
//	do {
//		rng->next(&support.lower);
//		rng->next(&support.upper);
//		support.upper += support.lower;
//	} while (support.length() > 1E-4f);
//	rng->next(&rnd);
//	support *= 10.f * (rnd + .1f);
//	// samples random positive values;
//	PiecewiseLinearDistribution1D::array_t values(pool, 10);
//	for (PiecewiseLinearDistribution1D::index_t i = 0; i < values.size(); i++) {
//		rng->next(&rnd);
//		values[i] = rnd * 10;
//	}
//	Pointer<PiecewiseLinearDistribution1D> d = PiecewiseLinearDistribution1D::build(pool, support, values);
//	// asserts the quantile
//	float x;
//	for(int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		rng->next(&x);
//		x = support.lower + x * support.length();
//		ASSERT_NEAR(x, d->quantile(d->cdf(x)), 1E-4f);
//	}
//}
//
///******************************************************************************
// * 2D PIECEWISE LINEAR DISTRIBUTION TESTS
// *****************************************************************************/
///*
// * Asserts that the sampled values are within the support and that the returned
// * pdf is non negative. This test does not garantee that the sampled values are
// * properly distributed over the entire support.
// */
//TEST(Distribution_PiecewiseLinear2D, sample) {
//	// creates a test distribution
//	MemoryPool pool{4, new CPUAllocator{}};
//	RangeF u(1.f, 2.f);
//	RangeF v(2.f, 4.f);
//	PiecewiseLinearDistribution2D::idx1d_t nu = 4;
//	PiecewiseLinearDistribution2D::idx1d_t nv = 3;
//	PiecewiseLinearDistribution2D::array_t values(pool, nu * nv);
//	values[0] = 1.f; values[1] = 2.f; values[ 2] = 3.f; values[ 3] = 4.f;
//	values[4] = 1.f; values[5] = 3.f; values[ 6] = 6.f; values[ 7] = 9.f;
//	values[8] = 1.f; values[9] = 4.f; values[10] = 8.f; values[11] =12.f;
//	Pointer<PiecewiseLinearDistribution2D> d = PiecewiseLinearDistribution2D::build(pool, u, v, nu, nv, values);
//	// samples the distribution
//	float rnd[2], smp[2], pdf;
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 2);
//	for (int i = 0; i < DISTRIBUTION_MIN_COUNT; ++i) {
//		rng->next(rnd);
//		d->sample(rnd, smp, &pdf);
//		ASSERT_LE(u.lower, smp[0]);
//		ASSERT_GE(u.upper, smp[0]);
//		ASSERT_LE(v.lower, smp[1]);
//		ASSERT_GE(v.upper, smp[1]);
//		ASSERT_LE(0.f, pdf);
//		ASSERT_NEAR(d->pdf(smp), pdf, 1E-4);
//	}
//}
//
///*
// * Asserts that the PDF is greater than zero an integrates to 1 over the entire
// * distribution's 2D support.
// */
//TEST(Distribution_PiecewiseLinear2D, pdf) {
//	// creates a test distribution
//	MemoryPool pool{4, new CPUAllocator{}};
//	RangeF u(1.f, 2.f);
//	RangeF v(2.f, 4.f);
//	PiecewiseLinearDistribution2D::idx1d_t nu = 4;
//	PiecewiseLinearDistribution2D::idx1d_t nv = 3;
//	PiecewiseLinearDistribution2D::array_t values(pool, nu * nv);
//	values[0] = 1.f; values[1] = 2.f; values[ 2] = 3.f; values[ 3] = 4.f;
//	values[4] = 1.f; values[5] = 3.f; values[ 6] = 6.f; values[ 7] = 9.f;
//	values[8] = 1.f; values[9] = 4.f; values[10] = 8.f; values[11] =12.f;
//	Pointer<PiecewiseLinearDistribution2D> d = PiecewiseLinearDistribution2D::build(pool, u, v, nu, nv, values);
//	// integrates the pdf
//	float rnd[2], pdf, dPDF, pdfSum = 0.f;
//	pdf = 1.f / (u.length() * v.length());
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 2);
//	for (int i = 0; true; ++i) {
//		// updates pdfSum
//		rng->next(rnd);
//		rnd[0] = u.lower + rnd[0] * u.length();
//		rnd[1] = v.lower + rnd[1] * v.length();
//		dPDF = d->pdf(rnd);
//		ASSERT_LE(0.f, dPDF);
//		pdfSum += dPDF / pdf;
//		// check if reached convergence
//		if (i >= DISTRIBUTION_MIN_COUNT) {
//			if (abs(1.f - pdfSum / (i + 1)) < DISTRIBUTION_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
//}}

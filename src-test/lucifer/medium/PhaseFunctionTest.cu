//#include <gtest/gtest.h>
//#include "lucifer/medium/HenyeyGreensteinPhaseFunction.cuh"
//#include "lucifer/medium/IsotropicPhaseFunction.cuh"
//#include "lucifer/medium/MixedPhaseFunction.cuh"
//#include "lucifer/medium/RayleighPhaseFunction.cuh"
//#include "lucifer/memory/CPUAllocator.cuh"
//#include "lucifer/memory/MemoryPool.cuh"
//#include "lucifer/node/ConstantNode.cuh"
//#include "lucifer/random/UniformSphereDistribution.cuh"
//#include "lucifer/random/generator/PlainRNG.cuh"
//#include "lucifer/memory/Pointer.cuh"
//
//namespace lucifer { namespace test {
//
//constexpr int PHASE_MIN_COUNT = 10000;
//constexpr float PHASE_CONVERGENCE_ERROR = 1E-3f;
//
///******************************************************************************
// * ISOTROPIC PHASE FUNCTION TESTS
// *****************************************************************************/
///*
// * Asserts that the phase function value is non negative, reciprocal, and
// * integrates to one.
// */
//TEST(PhaseFunction_Isotropic, evaluate) {
//	// creates the phase function
//	float val, pdf, rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	Pointer<PhaseFunction> phase = IsotropicPhaseFunction::build(pool);
//	// chooses a random position and direction
//	Point3F p;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &p[0], &pdf);
//	Vectr3F wo, wi;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &wo[0], &pdf);
//	// monte carlo integration
//	float wlen = 540E-9f;
//	float time = 0.f;
//	float vSum = 0.f;
//	for (int i = 0; true; ++i) {
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//		val = (*phase)(p, wlen, time, wi, wo);
//		ASSERT_LE(0.f, val);
//		ASSERT_NEAR((*phase)(p, wlen, time, wo, wi), val, 1E-4f);
//		vSum += val / pdf;
//		if (i >= PHASE_MIN_COUNT) {
//			if (abs(1.f - vSum / (i + 1)) < PHASE_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the phase function pdf is non negative and integrates to one.
// */
//TEST(PhaseFunction_Isotropic, pdf) {
//	// creates the phase function
//	float val, pdf, rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	Pointer<PhaseFunction> phase = IsotropicPhaseFunction::build(pool);
//	// chooses a random position and direction
//	Point3F p;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &p[0], &pdf);
//	Vectr3F wo, wi;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &wo[0], &pdf);
//	// monte carlo integration
//	float wlen = 540E-9f;
//	float time = 0.f;
//	float vSum = 0.f;
//	for (int i = 0; true; ++i) {
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//		val = phase->sampleWiPdf(p, wlen, time, wo, wi);
//		ASSERT_LE(0.f, val);
//		vSum += val / pdf;
//		if (i >= PHASE_MIN_COUNT) {
//			if (abs(1.f - vSum / (i + 1)) < PHASE_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the phase function sampling is consistent.
// */
//TEST(PhaseFunction_Isotropic, sample) {
//	// creates the phase function
//	float val, pdf, rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	Pointer<PhaseFunction> phase = IsotropicPhaseFunction::build(pool);
//	// sampling data
//	Point3F p;
//	Vectr3F wi, wo;
//	float wlen = 540E-9f;
//	float time = 0.f;
//	for (int i = 0; i < PHASE_MIN_COUNT; ++i) {
//		// chooses radom position and direction
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd,  &p[0], &pdf);
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], &pdf);
//		// asserts consistency
//		rng->next(rnd);
//		val = phase->sampleWi(p, wlen, time, wo, rnd, &wi, &pdf);
//		ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//		ASSERT_NEAR((*phase)(p, wlen, time, wi, wo), val, 1E-4f);
//		ASSERT_NEAR(phase->sampleWiPdf(p, wlen, time, wo, wi), pdf, 1E-4f);
//	}
//}
//
///******************************************************************************
// * RAYLEIGH PHASE FUNCTION TESTS
// *****************************************************************************/
///*
// * Asserts that the phase function value is non negative, reciprocal, and
// * integrates to one.
// */
//TEST(PhaseFunction_Rayleigh, evaluate) {
//	// creates the phase function
//	float val, pdf, rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	Pointer<PhaseFunction> phase = RayleighPhaseFunction::build(pool);
//	// chooses a random position and direction
//	Point3F p;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &p[0], &pdf);
//	Vectr3F wo, wi;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &wo[0], &pdf);
//	// monte carlo integration
//	float wlen = 540E-9f;
//	float time = 0.f;
//	float vSum = 0.f;
//	for (int i = 0; true; ++i) {
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//		val = (*phase)(p, wlen, time, wi, wo);
//		ASSERT_LE(0.f, val);
//		ASSERT_NEAR((*phase)(p, wlen, time, wo, wi), val, 1E-4f);
//		vSum += val / pdf;
//		if (i >= PHASE_MIN_COUNT) {
//			if (abs(1.f - vSum / (i + 1)) < PHASE_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the phase function pdf is non negative and integrates to one.
// */
//TEST(PhaseFunction_Rayleigh, pdf) {
//	// creates the phase function
//	float val, pdf, rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	Pointer<PhaseFunction> phase = RayleighPhaseFunction::build(pool);
//	// chooses a random position and direction
//	Point3F p;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &p[0], &pdf);
//	Vectr3F wo, wi;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &wo[0], &pdf);
//	// monte carlo integration
//	float wlen = 540E-9f;
//	float time = 0.f;
//	float vSum = 0.f;
//	for (int i = 0; true; ++i) {
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//		val = phase->sampleWiPdf(p, wlen, time, wo, wi);
//		ASSERT_LE(0.f, val);
//		vSum += val / pdf;
//		if (i >= PHASE_MIN_COUNT) {
//			if (abs(1.f - vSum / (i + 1)) < PHASE_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the phase function sampling is consistent.
// */
//TEST(PhaseFunction_Rayleigh, sample) {
//	// creates the phase function
//	float val, pdf, rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	Pointer<PhaseFunction> phase = RayleighPhaseFunction::build(pool);
//	// sampling data
//	Point3F p;
//	Vectr3F wi, wo;
//	float wlen = 540E-9f;
//	float time = 0.f;
//	for (int i = 0; i < PHASE_MIN_COUNT; ++i) {
//		// chooses radom position and direction
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd,  &p[0], &pdf);
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], &pdf);
//		// asserts consistency
//		rng->next(rnd);
//		val = phase->sampleWi(p, wlen, time, wo, rnd, &wi, &pdf);
//		ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//		ASSERT_NEAR((*phase)(p, wlen, time, wi, wo), val, 1E-4f);
//		ASSERT_NEAR(phase->sampleWiPdf(p, wlen, time, wo, wi), pdf, 1E-4f);
//	}
//}
//
///******************************************************************************
// * HENYEY-GREENSTEIN PHASE FUNCTION TESTS
// *****************************************************************************/
///*
// * Asserts that the phase function value is non negative, reciprocal, and
// * integrates to one.
// */
//TEST(PhaseFunction_HenyeyForward, evaluate) {
//	// creates the phase function
//	float val, pdf, rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	Pointer<PhaseFunction> phase = HenyeyGreensteinPhaseFunction::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, +0.5f));
//	// chooses a random position and direction
//	Point3F p;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &p[0], &pdf);
//	Vectr3F wo, wi;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &wo[0], &pdf);
//	// monte carlo integration
//	float wlen = 540E-9f;
//	float time = 0.f;
//	float vSum = 0.f;
//	for (int i = 0; true; ++i) {
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//		val = (*phase)(p, wlen, time, wi, wo);
//		ASSERT_LE(0.f, val);
//		ASSERT_NEAR((*phase)(p, wlen, time, wo, wi), val, 1E-4f);
//		vSum += val / pdf;
//		if (i >= PHASE_MIN_COUNT) {
//			if (abs(1.f - vSum / (i + 1)) < PHASE_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the phase function pdf is non negative and integrates to one.
// */
//TEST(PhaseFunction_HenyeyForward, pdf) {
//	// creates the phase function
//	float val, pdf, rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	Pointer<PhaseFunction> phase = HenyeyGreensteinPhaseFunction::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, +0.5f));
//	// chooses a random position and direction
//	Point3F p;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &p[0], &pdf);
//	Vectr3F wo, wi;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &wo[0], &pdf);
//	// monte carlo integration
//	float wlen = 540E-9f;
//	float time = 0.f;
//	float vSum = 0.f;
//	for (int i = 0; true; ++i) {
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//		val = phase->sampleWiPdf(p, wlen, time, wo, wi);
//		ASSERT_LE(0.f, val);
//		vSum += val / pdf;
//		if (i >= PHASE_MIN_COUNT) {
//			if (abs(1.f - vSum / (i + 1)) < PHASE_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the phase function sampling is consistent.
// */
//TEST(PhaseFunction_HenyeyForward, sample) {
//	// creates the phase function
//	float val, pdf, rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	Pointer<PhaseFunction> phase = HenyeyGreensteinPhaseFunction::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, +0.5f));
//	// sampling data
//	Point3F p;
//	Vectr3F wi, wo;
//	float wlen = 540E-9f;
//	float time = 0.f;
//	for (int i = 0; i < PHASE_MIN_COUNT; ++i) {
//		// chooses radom position and direction
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd,  &p[0], &pdf);
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], &pdf);
//		// asserts consistency
//		rng->next(rnd);
//		val = phase->sampleWi(p, wlen, time, wo, rnd, &wi, &pdf);
//		ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//		ASSERT_NEAR((*phase)(p, wlen, time, wi, wo), val, 1E-4f);
//		ASSERT_NEAR(phase->sampleWiPdf(p, wlen, time, wo, wi), pdf, 1E-4f);
//	}
//}
//
///*
// * Asserts that the phase function value is non negative, reciprocal, and
// * integrates to one.
// */
//TEST(PhaseFunction_HenyeyBackward, evaluate) {
//	// creates the phase function
//	float val, pdf, rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	Pointer<PhaseFunction> phase = HenyeyGreensteinPhaseFunction::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, -0.5f));
//	// chooses a random position and direction
//	Point3F p;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &p[0], &pdf);
//	Vectr3F wo, wi;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &wo[0], &pdf);
//	// monte carlo integration
//	float wlen = 540E-9f;
//	float time = 0.f;
//	float vSum = 0.f;
//	for (int i = 0; true; ++i) {
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//		val = (*phase)(p, wlen, time, wi, wo);
//		ASSERT_LE(0.f, val);
//		ASSERT_NEAR((*phase)(p, wlen, time, wo, wi), val, 1E-4f);
//		vSum += val / pdf;
//		if (i >= PHASE_MIN_COUNT) {
//			if (abs(1.f - vSum / (i + 1)) < PHASE_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the phase function pdf is non negative and integrates to one.
// */
//TEST(PhaseFunction_HenyeyBackward, pdf) {
//	// creates the phase function
//	float val, pdf, rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	Pointer<PhaseFunction> phase = HenyeyGreensteinPhaseFunction::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, -0.5f));
//	// chooses a random position and direction
//	Point3F p;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &p[0], &pdf);
//	Vectr3F wo, wi;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &wo[0], &pdf);
//	// monte carlo integration
//	float wlen = 540E-9f;
//	float time = 0.f;
//	float vSum = 0.f;
//	for (int i = 0; true; ++i) {
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//		val = phase->sampleWiPdf(p, wlen, time, wo, wi);
//		ASSERT_LE(0.f, val);
//		vSum += val / pdf;
//		if (i >= PHASE_MIN_COUNT) {
//			if (abs(1.f - vSum / (i + 1)) < PHASE_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the phase function sampling is consistent.
// */
//TEST(PhaseFunction_HenyeyBackward, sample) {
//	// creates the phase function
//	float val, pdf, rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	Pointer<PhaseFunction> phase = HenyeyGreensteinPhaseFunction::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, -0.5f));
//	// sampling data
//	Point3F p;
//	Vectr3F wi, wo;
//	float wlen = 540E-9f;
//	float time = 0.f;
//	for (int i = 0; i < PHASE_MIN_COUNT; ++i) {
//		// chooses radom position and direction
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd,  &p[0], &pdf);
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], &pdf);
//		// asserts consistency
//		rng->next(rnd);
//		val = phase->sampleWi(p, wlen, time, wo, rnd, &wi, &pdf);
//		ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//		ASSERT_NEAR((*phase)(p, wlen, time, wi, wo), val, 1E-4f);
//		ASSERT_NEAR(phase->sampleWiPdf(p, wlen, time, wo, wi), pdf, 1E-4f);
//	}
//}
//
///******************************************************************************
// * MIXED PHASE FUNCTION TESTS
// *****************************************************************************/
///*
// * Asserts that the phase function value is non negative, reciprocal, and
// * integrates to one.
// */
//TEST(PhaseFunction_mixed, evaluate) {
//	// creates the phase function
//	float val, pdf, rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	MixedPhaseFunction::array_f wgt(pool, 2);
//	wgt[0] = 2.f;
//	wgt[1] = 3.f;
//	MixedPhaseFunction::array_p phf(pool, 2);
//	phf[0] = HenyeyGreensteinPhaseFunction::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, -0.5f));
//	phf[1] = HenyeyGreensteinPhaseFunction::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, +0.5f));
//	Pointer<PhaseFunction> phase = MixedPhaseFunction::build(pool, wgt, phf);
//	// chooses a random position and direction
//	Point3F p;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &p[0], &pdf);
//	Vectr3F wo, wi;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &wo[0], &pdf);
//	// monte carlo integration
//	float wlen = 540E-9f;
//	float time = 0.f;
//	float vSum = 0.f;
//	for (int i = 0; true; ++i) {
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//		val = (*phase)(p, wlen, time, wi, wo);
//		ASSERT_LE(0.f, val);
//		ASSERT_NEAR((*phase)(p, wlen, time, wo, wi), val, 1E-4f);
//		vSum += val / pdf;
//		if (i >= PHASE_MIN_COUNT) {
//			if (abs(1.f - vSum / (i + 1)) < PHASE_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the phase function pdf is non negative and integrates to one.
// */
//TEST(PhaseFunction_mixed, pdf) {
//	// creates the phase function
//	float val, pdf, rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	MixedPhaseFunction::array_f wgt(pool, 2);
//	wgt[0] = 2.f;
//	wgt[1] = 3.f;
//	MixedPhaseFunction::array_p phf(pool, 2);
//	phf[0] = HenyeyGreensteinPhaseFunction::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, -0.5f));
//	phf[1] = HenyeyGreensteinPhaseFunction::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, +0.5f));
//	Pointer<PhaseFunction> phase = MixedPhaseFunction::build(pool, wgt, phf);
//	// chooses a random position and direction
//	Point3F p;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &p[0], &pdf);
//	Vectr3F wo, wi;
//	rng->next(rnd);
//	UniformSphereDistribution::sample(rnd, &wo[0], &pdf);
//	// monte carlo integration
//	float wlen = 540E-9f;
//	float time = 0.f;
//	float vSum = 0.f;
//	for (int i = 0; true; ++i) {
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wi[0], &pdf);
//		val = phase->sampleWiPdf(p, wlen, time, wo, wi);
//		ASSERT_LE(0.f, val);
//		vSum += val / pdf;
//		if (i >= PHASE_MIN_COUNT) {
//			if (abs(1.f - vSum / (i + 1)) < PHASE_CONVERGENCE_ERROR) {
//				break;
//			}
//		}
//	}
//}
//
///*
// * Asserts that the phase function sampling is consistent.
// */
//TEST(PhaseFunction_mixed, sample) {
//	// creates the phase function
//	float val, pdf, rnd[3];
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 3);
//	MixedPhaseFunction::array_f wgt(pool, 2);
//	wgt[0] = 2.f;
//	wgt[1] = 3.f;
//	MixedPhaseFunction::array_p phf(pool, 2);
//	phf[0] = HenyeyGreensteinPhaseFunction::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, -0.5f));
//	phf[1] = HenyeyGreensteinPhaseFunction::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, +0.5f));
//	Pointer<PhaseFunction> phase = MixedPhaseFunction::build(pool, wgt, phf);
//	// sampling data
//	Point3F p;
//	Vectr3F wi, wo;
//	float wlen = 540E-9f;
//	float time = 0.f;
//	for (int i = 0; i < PHASE_MIN_COUNT; ++i) {
//		// chooses radom position and direction
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd,  &p[0], &pdf);
//		rng->next(rnd);
//		UniformSphereDistribution::sample(rnd, &wo[0], &pdf);
//		// asserts consistency
//		rng->next(rnd);
//		val = phase->sampleWi(p, wlen, time, wo, rnd, &wi, &pdf);
//		ASSERT_NEAR(1.f, wi.length(), 1E-4f);
//		ASSERT_NEAR((*phase)(p, wlen, time, wi, wo), val, 1E-4f);
//		ASSERT_NEAR(phase->sampleWiPdf(p, wlen, time, wo, wi), pdf, 1E-4f);
//	}
//}
//
//}}

//#include <gtest/gtest.h>
//#include "lucifer/medium/HeterogeneousMedium.cuh"
//#include "lucifer/medium/HomogeneousMedium.cuh"
//#include "lucifer/medium/IsotropicPhaseFunction.cuh"
//#include "lucifer/medium/OpacityNode.cuh"
//#include "lucifer/memory/CPUAllocator.cuh"
//#include "lucifer/memory/MemoryPool.cuh"
//#include "lucifer/node/ConstantNode.cuh"
//#include "lucifer/random/generator/PlainRNG.cuh"
//
//namespace lucifer { namespace test {
//
//constexpr int MEDIUM_MIN_COUNT = 10000;
//
///******************************************************************************
// * HOMOGENEOUS MEDIUM TESTS
// *****************************************************************************/
// /*
//  * Asserts that the medium priority is correct
//  */
// TEST(Medium_Homogeneous, priority) {
//	MemoryPool pool{4, new CPUAllocator{}};
// 	Pointer<Medium> medium = HomogeneousMedium::build(
//		pool,
// 		AttenuationNode::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f)),
// 		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.0f),
// 		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.5f),
//		IsotropicPhaseFunction::build(pool),
//		true, 2
//	);
//	ASSERT_EQ(2, medium->priority());
// }
//
///*
// * Asserts that the medium is correctly tagged as volumetric.
// */
//TEST(Medium_Homogeneous, volumetric) {
//	{
//		// volumetric
//		MemoryPool pool{4, new CPUAllocator{}};
//	 	Pointer<Medium> medium = HomogeneousMedium::build(
//			pool,
//	 		AttenuationNode::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f)),
//	 		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.0f),
//	 		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.5f),
//			IsotropicPhaseFunction::build(pool),
//			true, 2
//		);
//		ASSERT_TRUE(medium->isVolumetric());
//	}
//	{
//		// non volumetric
//		MemoryPool pool{4, new CPUAllocator{}};
//	 	Pointer<Medium> medium = HomogeneousMedium::build(
//			pool,
//	 		AttenuationNode::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f)),
//	 		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.0f),
//	 		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.5f),
//			IsotropicPhaseFunction::build(pool),
//			true, 2
//		);
//		ASSERT_FALSE(medium->isVolumetric());
//	}
//}
//
///*
// * Asserts that the transmittance is in the range [0, 1].
// */
// TEST(Medium_Homogeneous, transmittance) {
//	 MemoryPool pool{4, new CPUAllocator{}};
//	 Pointer<Medium> medium = HomogeneousMedium::build(
//		pool,
//		AttenuationNode::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f)),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.0f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.5f),
//		IsotropicPhaseFunction::build(pool),
//		true, 2
//	);
//	// assert transmittance along inifity ray is 0.
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 1);
//	Ray ray(Point3F(0.f, 0.f, 0.f), Vectr3F(0.f, 0.f, 1.f).normalize(), RangeF(0.f, +INFINITY), 540E-9, 0.f);
//	ASSERT_EQ(0.f, medium->transmittance(ray, rng.get()));
//	// assert finity ray transmittance is between 0 and 1.
//	for (int i = 0; i < MEDIUM_MIN_COUNT; ++i) {
//		rng->next(&ray.r.lower);
//		rng->next(&ray.r.upper);
//		ray.r.upper += ray.r.lower + 1E-3f;
//		float tr = medium->transmittance(ray, rng.get());
//		ASSERT_LE(0.f, tr);
//		ASSERT_GE(1.f, tr);
//	}
// }
//
// /*
//  * Asserts that the sampled position is in the ray's extent and that the
//  * returned `beta` value is non negative.
//  */
// TEST(Medium_Homogeneous, sample) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<Medium> medium = HomogeneousMedium::build(
//		pool,
//		AttenuationNode::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f)),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.0f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.5f),
//		IsotropicPhaseFunction::build(pool),
//		true, 2
//	);
//	// assert sampled position and beta value
//	float beta;
//	Point3F pt;
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 1);
//	Ray ray(Point3F(0.f, 0.f, 0.f), Vectr3F(0.f, 0.f, 1.f).normalize(), RangeF(0.f, +INFINITY), 540E-9, 0.f);
//	for (int i = 0; i < MEDIUM_MIN_COUNT; ++i) {
//		rng->next(&ray.r.lower);
//		rng->next(&ray.r.upper);
//		ray.r.upper += ray.r.lower + 1E-3f;
//		if(medium->sample(ray, rng.get(), &pt, &beta)) {
//			ASSERT_TRUE(ray.r.contains((pt - ray.o).length()));
//		}
//		ASSERT_LE(0.f, beta);
//	}
// }
//
// /******************************************************************************
//  * HETEROGENEOUS MEDIUM TESTS
//  *****************************************************************************/
//  /*
//   * Asserts that the medium priority is correct
//   */
//  TEST(Medium_Heterogeneous, priority) {
//	MemoryPool pool{4, new CPUAllocator{}};
//  	Pointer<Medium> medium = HeterogeneousMedium::build(
//  		pool,
//		AttenuationNode::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f)),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.0f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.5f),
//		IsotropicPhaseFunction::build(pool),
//		true, 2
// 	);
// 	ASSERT_EQ(2, medium->priority());
//  }
//
// /*
//  * Asserts that the medium is correctly tagged as volumetric.
//  */
// TEST(Medium_Heterogeneous, volumetric) {
// 	{
// 		// volumetric
// 		MemoryPool pool{4, new CPUAllocator{}};
// 	  	Pointer<Medium> medium = HeterogeneousMedium::build(
// 	  		pool,
//			AttenuationNode::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f)),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.0f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.5f),
//			IsotropicPhaseFunction::build(pool),
//			true, 0
// 	 	);
// 		ASSERT_TRUE(medium->isVolumetric());
// 	}
// 	{
// 		// non volumetric
// 		MemoryPool pool{4, new CPUAllocator{}};
// 	  	Pointer<Medium> medium = HeterogeneousMedium::build(
// 	  		pool,
//			AttenuationNode::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f)),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.0f),
//			ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.5f),
//			IsotropicPhaseFunction::build(pool),
//			false, 2
// 	 	);
// 		ASSERT_FALSE(medium->isVolumetric());
// 	}
// }
//
// /*
//  * Asserts that the transmittance is in the range [0, 1].
//  */
//  TEST(Medium_Heterogeneous, transmittance) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<Medium> medium = HeterogeneousMedium::build(
//		pool,
//		AttenuationNode::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f)),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.0f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.5f),
//		IsotropicPhaseFunction::build(pool),
//		true, 0
//	);
// 	// assert transmittance along inifity ray is 0.
// 	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 1);
// 	Ray ray(Point3F(0.f, 0.f, 0.f), Vectr3F(0.f, 0.f, 1.f).normalize(), RangeF(0.f, +INFINITY), 540E-9, 0.f);
// 	ASSERT_EQ(0.f, medium->transmittance(ray, rng.get()));
// 	// assert finity ray transmittance is between 0 and 1.
// 	for (int i = 0; i < MEDIUM_MIN_COUNT; ++i) {
// 		rng->next(&ray.r.lower);
// 		rng->next(&ray.r.upper);
// 		ray.r.upper += ray.r.lower + 1E-3f;
// 		float tr = medium->transmittance(ray, rng.get());
// 		ASSERT_LE(0.f, tr);
// 		ASSERT_GE(1.f, tr);
// 	}
//  }
//
//  /*
//   * Asserts that the sampled position is in the ray's extent and that the
//   * returned `beta` value is non negative.
//   */
//  TEST(Medium_Heterogeneous, sample) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<Medium> medium = HeterogeneousMedium::build(
//		pool,
//		AttenuationNode::build(pool, ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f)),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.0f),
//		ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.5f),
//		IsotropicPhaseFunction::build(pool),
//		true, 0
//	);
// 	// assert sampled position and beta value
// 	float beta;
// 	Point3F pt;
// 	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 1);
// 	Ray ray(Point3F(0.f, 0.f, 0.f), Vectr3F(0.f, 0.f, 1.f).normalize(), RangeF(0.f, +INFINITY), 540E-9, 0.f);
// 	for (int i = 0; i < MEDIUM_MIN_COUNT; ++i) {
// 		rng->next(&ray.r.lower);
// 		rng->next(&ray.r.upper);
// 		ray.r.upper += ray.r.lower + 1E-3f;
// 		if(medium->sample(ray, rng.get(), &pt, &beta)) {
// 			ASSERT_TRUE(ray.r.contains((pt - ray.o).length()));
// 		}
// 		ASSERT_LE(0.f, beta);
// 	}
//  }
//
//}}

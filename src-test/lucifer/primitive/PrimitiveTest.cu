//#include <gtest/gtest.h>
//#include "../TestUtils.cuh"
//#include "lucifer/bsdf/LambertianBRDF.cuh"
//#include "lucifer/material/BaseMaterial.cuh"
//#include "lucifer/memory/CPUAllocator.cuh"
//#include "lucifer/memory/MemoryPool.cuh"
//#include "lucifer/memory/Pointer.cuh"
//#include "lucifer/node/ConstantNode.cuh"
//#include "lucifer/primitive/GeometricPrimitive.cuh"
//#include "lucifer/random/generator/PlainRNG.cuh"
//#include "lucifer/shape/Sphere.cuh"
//#include "lucifer/uedf/IsotropicHemisphericalUEDC.cuh"
//#include "lucifer/uedf/UEDF.cuh"
//
//namespace lucifer { namespace test {
//
//constexpr int PRIMITIVE_SAMPLE_COUNT = 100;
//
///******************************************************************************
// * GEOMETRIC PRIMITIVE TESTS
// *****************************************************************************/
//__host__
//Pointer<GeometricPrimitive> buildGeometric(MemoryPool& pool, const Point3F& center, const float radius) {
//
//	auto shape = Pointer<const Shape>{Sphere::build(pool, center, radius)};
//	auto uedf = UEDF::build(pool, 1);
//	(*uedf)[0] = Pointer<const UEDC>{IsotropicHemisphericalUEDC::build(pool, Pointer<const ShadingNode<float>>{ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 1.f)})};
//	auto bsdf = BSDF::build(pool, 1);
//	(*bsdf)[0] = Pointer<const BxDF>{LambertianBRDF::build(pool, Pointer<const ShadingNode<float>>{ConstantNode<float, LUCIFER_SHADING_INPUT>::build(pool, 0.75f)})};
//	auto material = BaseMaterial::build(pool, Pointer<const UEDF>{uedf}, Pointer<const BSDF>{bsdf}, Pointer<const Medium>{});
//	return GeometricPrimitive::build(pool, shape, material, false, 0, 0, 0, Pointer<const XYZColorSpace>{});
//}
//
///*
// * Asserts that the surface area is correct.
// */
//TEST(Primitive_Geometric, area) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	Pointer<GeometricPrimitive> p = buildGeometric(pool, Point3F(1.f, 2.f, 3.f), 2.f);
//	ASSERT_NEAR(50.2654824574f, p->area(0.25f), 1E-4);
//	ASSERT_NEAR(50.2654824574f, p->area(0.75f), 1E-4);
//}
//
///*
// * Asserts that sampled points are actually on the shape's surface and that
// * geometric and shading normals and tangent vectors are consistent.
// */
//TEST(Primitive_Geometric, sample) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float pdf, rnd[4];
//	float radius = 2.f;
//	float wlen = 540E-9f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	Pointer<GeometricPrimitive> p = buildGeometric(pool, center, radius);
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 4);
//	for (int i = 0; i < PRIMITIVE_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		Geometry geometry;
//		p->sampleLeGeo(wlen, rnd[3], rnd, &geometry, &pdf);
//		// asserts sampled point
//		ASSERT_NEAR(radius, (geometry.gp - center).length(), 1E-4);
//		// asserts geometric and shading normals
//		Vectr3F n = (geometry.gp - center).normalize();
//		ASSERT_TRIPLET_NEAR(n, geometry.gn, 1E-4);
//		ASSERT_TRIPLET_NEAR(n, geometry.sn, 1E-4);
//		// asserts geometric and shading tangents
//		ASSERT_NEAR(1.f, geometry.gu.length(), 1E-4);
//		ASSERT_NEAR(1.f, geometry.su.length(), 1E-4);
//		ASSERT_NEAR(0.f, dot(geometry.gn, geometry.gu), 1E-4);
//		ASSERT_NEAR(0.f, dot(geometry.sn, geometry.su), 1E-4);
//	}
//}
//
///*
// * Asserts that the primitives's boundary contains the sampled points.
// */
//TEST(Primitive_Geometric, boundary) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	float rnd[4], pdf;
//	float wlen = 540E-9;
//	float radius = 2.f;
//	Point3F center = Point3F(1.f, 2.f, 3.f);
//	Pointer<GeometricPrimitive> p = buildGeometric(pool, center, radius);
//	Pointer<RNG> rng = PlainRNGFactory<>().build(pool, 4);
//	Box boundary = p->boundary(RangeF(0.f, 1.f));
//	for (int i = 0; i < PRIMITIVE_SAMPLE_COUNT; ++i) {
//		Geometry geometry;
//		rng->next(rnd);
//		p->sampleLe(Point3F(0.f, 0.f, 0.f), wlen, rnd[3], rnd, &geometry, &pdf);
//		ASSERT_TRUE(boundary.contains(geometry.gp));
//		rng->next(rnd);
//		p->sampleLeGeo(wlen, rnd[3], rnd, &geometry, &pdf);
//		ASSERT_TRUE(boundary.contains(geometry.gp));
//	}
//}
//
//}}

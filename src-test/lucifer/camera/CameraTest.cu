//#include <gtest/gtest.h>
//#include "../TestUtils.cuh"
//#include "lucifer/camera/AnimatedCamera.cuh"
//#include "lucifer/camera/PerspectiveCamera.cuh"
//#include "lucifer/camera/TransformedCamera.cuh"
//#include "lucifer/image/HDRImage.cuh"
//#include "lucifer/math/geometry/KeyFrameTransformation.cuh"
//#include "lucifer/memory/CPUAllocator.cuh"
//#include "lucifer/memory/MemoryPool.cuh"
//#include "lucifer/memory/Pointer.cuh"
//#include "lucifer/random/generator/PlainRNG.cuh"
//
//namespace lucifer { namespace test {
//
//constexpr int CAMERA_SAMPLE_COUNT = 100;
//
///******************************************************************************
// * NO LENS PERSPECTIVE CAMERA TESTS
// *****************************************************************************/
//
///*
// * Asserts that the generated rays are correct.
// */
//TEST(Camera_Perspective_NoLens, ray) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	int w = 200;
//	int h = 100;
//	Norml3F norml;
//	auto raster = Pointer<HDRImage<float, 3>>::make(pool, pool, w, h);
//	auto film = Sensor::build(
//		pool,
//		raster,
//		Pointer<const XYZColorSpace>{XYZColorSpace::build(pool)},
//		Pointer<const Filter2D>{}
//	);
//	auto cam = PerspectiveCamera::build(pool, 45 * M_PI / 180, 0.f, 0.f, film);
//	auto rng = PlainRNGFactory<>().build(pool, 6);
//	float rnd[6];
//	for (int i = 0; i < CAMERA_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		rnd[0] *= w;
//		rnd[1] *= h;
//		Ray ray = cam->ray(rnd[0], rnd[1], rnd[2], rnd[3], rnd[4], rnd[5], &norml);
//		ASSERT_NEAR(rnd[4], ray.l, 1E-4f);
//		ASSERT_NEAR(rnd[5], ray.t, 1E-4f);
//		ASSERT_NEAR(1.f, ray.d.length(), 1E-4f);
//		ASSERT_TRIPLET_NEAR(Point3F(0.f, 0.f, 0.f), ray(ray.r.lower), 1E-4f);
//		ASSERT_EQ(+INFINITY, ray.r.upper);
//		ASSERT_TRIPLET_NEAR(Norml3F(0.f, 0.f, 1.f), norml, 1E-4f);
//	}
//}
//
///*
// * Asserts that the computed raster coordinates are consistent with the
// * generated rays.
// */
//TEST(Camera_Perspective_NoLens, raster) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	int w = 200;
//	int h = 100;
//	auto raster = Pointer<HDRImage<float, 3>>::make(pool, pool, w, h);
//		auto film = Sensor::build(
//			pool,
//			raster,
//			Pointer<const XYZColorSpace>{XYZColorSpace::build(pool)},
//			Pointer<const Filter2D>{}
//		);
//		auto cam = PerspectiveCamera::build(pool, 45 * M_PI / 180, 0.f, 0.f, film);
//	auto rng = PlainRNGFactory<>().build(pool, 6);
//	float rnd[6], x, y;
//	for (int i = 0; i < CAMERA_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		rnd[0] *= w;
//		rnd[1] *= h;
//		Ray ray = cam->ray(rnd[0], rnd[1], rnd[2], rnd[3], rnd[4], rnd[5], nullptr);
//		ASSERT_TRUE(cam->raster(ray, &x, &y));
//		ASSERT_NEAR(rnd[0], x, 1E-4f);
//		ASSERT_NEAR(rnd[1], y, 1E-4f);
//	}
//}
//
///*
// * Asserts that the total emitted importance and PDF of the generated rays are
// * positive.
// */
//TEST(Camera_Perspective_NoLens, importance0) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	int w = 200;
//	int h = 100;
//	auto raster = Pointer<HDRImage<float, 3>>::make(pool, pool, w, h);
//		auto film = Sensor::build(
//			pool,
//			raster,
//			Pointer<const XYZColorSpace>{XYZColorSpace::build(pool)},
//			Pointer<const Filter2D>{}
//		);
//		auto cam = PerspectiveCamera::build(pool, 45 * M_PI / 180, 0.f, 0.f, film);
//	auto rng = PlainRNGFactory<>().build(pool, 6);
//	float rnd[6];
//	for (int i = 0; i < CAMERA_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		rnd[0] *= w;
//		rnd[1] *= h;
//		Ray ray = cam->ray(rnd[0], rnd[1], rnd[2], rnd[3], rnd[4], rnd[5], nullptr);
//		ASSERT_TRUE(cam-> importance0(ray(ray.r.lower), ray.t) > 0.f);
//		ASSERT_TRUE(cam->sampleGeoPDF(ray(ray.r.lower), ray.t) > 0.f);
//	}
//}
//
///*
// * Asserts that the directional differential of importance and PDF of the
// * generated rays are positive.
// */
//TEST(Camera_Perspective_NoLens, importance1) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	int w = 200;
//	int h = 100;
//	auto raster = Pointer<HDRImage<float, 3>>::make(pool, pool, w, h);
//	auto film = Sensor::build(
//		pool,
//		raster,
//		Pointer<const XYZColorSpace>{XYZColorSpace::build(pool)},
//		Pointer<const Filter2D>{}
//	);
//	auto cam = PerspectiveCamera::build(pool, 45 * M_PI / 180, 0.f, 0.f, film);
//	auto rng = PlainRNGFactory<>().build(pool, 6);
//	float rnd[6];
//	for (int i = 0; i < CAMERA_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		rnd[0] *= w;
//		rnd[1] *= h;
//		Ray ray = cam->ray(rnd[0], rnd[1], rnd[2], rnd[3], rnd[4], rnd[5], nullptr);
//		ASSERT_TRUE(cam-> importance1(ray) > 0.f);
//		ASSERT_TRUE(cam->sampleDirPDF(ray) > 0.f);
//	}
//}
//
///******************************************************************************
// * PERSPECTIVE CAMERA TESTS
// *****************************************************************************/
//
///*
// * Asserts that the generated rays are correct.
// */
//TEST(Camera_Perspective, ray) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	int w = 200;
//	int h = 100;
//	Norml3F norml;
//	float r = 0.1f;
//	auto raster = Pointer<HDRImage<float, 3>>::make(pool, pool, w, h);
//	auto film = Sensor::build(
//		pool,
//		raster,
//		Pointer<const XYZColorSpace>{XYZColorSpace::build(pool)},
//		Pointer<const Filter2D>{}
//	);
//	auto cam = PerspectiveCamera::build(pool, 45 * M_PI / 180, r, 1.5f, film);
//	auto rng = PlainRNGFactory<>().build(pool, 6);
//	float rnd[6];
//	for (int i = 0; i < CAMERA_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		rnd[0] *= w;
//		rnd[1] *= h;
//		Ray ray = cam->ray(rnd[0], rnd[1], rnd[2], rnd[3], rnd[4], rnd[5], &norml);
//		ASSERT_NEAR(rnd[4], ray.l, 1E-4f);
//		ASSERT_NEAR(rnd[5], ray.t, 1E-4f);
//		ASSERT_NEAR(1.f, ray.d.length(), 1E-4f);
//		Point3F origin = ray(ray.r.lower);
//		ASSERT_NEAR(0.f, origin[2], 1E-4f);
//		ASSERT_TRUE(origin[0] * origin[0] + origin[1] * origin[1] <= r * r);
//		ASSERT_EQ(+INFINITY, ray.r.upper);
//		ASSERT_TRIPLET_NEAR(Norml3F(0.f, 0.f, 1.f), norml, 1E-4f);
//	}
//}
//
///*
// * Asserts that the computed raster coordinates are consistent with the
// * generated rays.
// */
//TEST(Camera_Perspective, raster) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	int w = 200;
//	int h = 100;
//	float r = 0.1f;
//	auto raster = Pointer<HDRImage<float, 3>>::make(pool, pool, w, h);
//	auto film = Sensor::build(
//		pool,
//		raster,
//		Pointer<const XYZColorSpace>{XYZColorSpace::build(pool)},
//		Pointer<const Filter2D>{}
//	);
//	auto cam = PerspectiveCamera::build(pool, 45 * M_PI / 180, r, 1.5f, film);
//	auto rng = PlainRNGFactory<>().build(pool, 6);
//	float rnd[6], x, y;
//	for (int i = 0; i < CAMERA_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		rnd[0] *= w;
//		rnd[1] *= h;
//		Ray ray = cam->ray(rnd[0], rnd[1], rnd[2], rnd[3], rnd[4], rnd[5], nullptr);
//		ASSERT_TRUE(cam->raster(ray, &x, &y));
//		ASSERT_NEAR(rnd[0], x, 1E-4f);
//		ASSERT_NEAR(rnd[1], y, 1E-4f);
//	}
//}
//
///*
// * Asserts that the total emitted importance and PDF of the generated rays are
// * positive.
// */
//TEST(Camera_Perspective, importance0) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	int w = 200;
//	int h = 100;
//	float r = 0.1f;
//	auto raster = Pointer<HDRImage<float, 3>>::make(pool, pool, w, h);
//	auto film = Sensor::build(
//		pool,
//		raster,
//		Pointer<const XYZColorSpace>{XYZColorSpace::build(pool)},
//		Pointer<const Filter2D>{}
//	);
//	auto cam = PerspectiveCamera::build(pool, 45 * M_PI / 180, r, 1.5f, film);
//	auto rng = PlainRNGFactory<>().build(pool, 6);
//	float rnd[6];
//	for (int i = 0; i < CAMERA_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		rnd[0] *= w;
//		rnd[1] *= h;
//		Ray ray = cam->ray(rnd[0], rnd[1], rnd[2], rnd[3], rnd[4], rnd[5], nullptr);
//		ASSERT_TRUE(cam-> importance0(ray(ray.r.lower), ray.t) > 0.f);
//		ASSERT_TRUE(cam->sampleGeoPDF(ray(ray.r.lower), ray.t) > 0.f);
//	}
//}
//
///*
// * Asserts that the directional differential of importance and PDF of the
// * generated rays are positive.
// */
//TEST(Camera_Perspective, importance1) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	int w = 200;
//	int h = 100;
//	float r = 0.1f;
//	auto raster = Pointer<HDRImage<float, 3>>::make(pool, pool, w, h);
//	auto film = Sensor::build(
//		pool,
//		raster,
//		Pointer<const XYZColorSpace>{XYZColorSpace::build(pool)},
//		Pointer<const Filter2D>{}
//	);
//	auto cam = PerspectiveCamera::build(pool, 45 * M_PI / 180, r, 1.5f, film);
//	auto rng = PlainRNGFactory<>().build(pool, 6);
//	float rnd[6];
//	for (int i = 0; i < CAMERA_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		rnd[0] *= w;
//		rnd[1] *= h;
//		Ray ray = cam->ray(rnd[0], rnd[1], rnd[2], rnd[3], rnd[4], rnd[5], nullptr);
//		ASSERT_TRUE(cam-> importance1(ray) > 0.f);
//		ASSERT_TRUE(cam->sampleDirPDF(ray) > 0.f);
//	}
//}
//
///******************************************************************************
// * TRANSFORMED CAMERA TESTS
// *****************************************************************************/
//
///*
// * Asserts that the generated rays are correct.
// */
//TEST(Camera_Transformed, ray) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	int w = 200;
//	int h = 100;
//	Norml3F norml;
//	float r = 0.1f;
//
//	auto raster = Pointer<HDRImage<float, 3>>::make(pool, pool, w, h);
//	auto film = Sensor::build(
//		pool,
//		raster,
//		Pointer<const XYZColorSpace>{XYZColorSpace::build(pool)},
//		Pointer<const Filter2D>{}
//	);
//	auto trs = Transformation<float, 3>::rotation<2, 0>(PI<float>() / 4.f) * Transformation<float, 3>::translation(Vectr3F(1.f, 2.f, 3.f));
//	auto cam = TransformedCamera::build(pool, trs, PerspectiveCamera::build(pool, 45 * M_PI / 180, r, 1.5f, film));
//	auto rng = PlainRNGFactory<>().build(pool, 6);
//
//	float rnd[6];
//	for (int i = 0; i < CAMERA_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		rnd[0] *= w;
//		rnd[1] *= h;
//		Ray ray = cam->ray(rnd[0], rnd[1], rnd[2], rnd[3], rnd[4], rnd[5], &norml);
//		ASSERT_NEAR(rnd[4], ray.l, 1E-4f);
//		ASSERT_NEAR(rnd[5], ray.t, 1E-4f);
//		ASSERT_NEAR(1.f, ray.d.length(), 1E-4f);
//		ASSERT_EQ(+INFINITY, ray.r.upper);
//		Point3F origin = trs.inverse() * ray(ray.r.lower);
//		ASSERT_NEAR(0.f, origin[2], 1E-4f);
//		ASSERT_TRUE(origin[0] * origin[0] + origin[1] * origin[1] <= r * r);
//		ASSERT_TRIPLET_NEAR(Norml3F(0.f, 0.f, 1.f), trs.inverse() * norml, 1E-4f);
//	}
//}
//
///*
// * Asserts that the computed raster coordinates are consistent with the
// * generated rays.
// */
//TEST(Camera_Transformed, raster) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	int w = 200;
//	int h = 100;
//	float r = 0.1f;
//
//	auto raster = Pointer<HDRImage<float, 3>>::make(pool, pool, w, h);
//	auto film = Sensor::build(
//		pool,
//		raster,
//		Pointer<const XYZColorSpace>{XYZColorSpace::build(pool)},
//		Pointer<const Filter2D>{}
//	);
//	auto trs = Transformation<float, 3>::rotation<2, 0>(PI<float>() / 4.f) * Transformation<float, 3>::translation(Vectr3F(1.f, 2.f, 3.f));
//	auto cam = TransformedCamera::build(pool, trs, PerspectiveCamera::build(pool, 45 * M_PI / 180, r, 1.5f, film));
//	auto rng = PlainRNGFactory<>().build(pool, 6);
//
//	float rnd[6], x, y;
//	for (int i = 0; i < CAMERA_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		rnd[0] *= w;
//		rnd[1] *= h;
//		Ray ray = cam->ray(rnd[0], rnd[1], rnd[2], rnd[3], rnd[4], rnd[5], nullptr);
//		ASSERT_TRUE(cam->raster(ray, &x, &y));
//		ASSERT_NEAR(rnd[0], x, 1E-4f);
//		ASSERT_NEAR(rnd[1], y, 1E-4f);
//	}
//}
//
///*
// * Asserts that the total emitted importance and PDF of the generated rays are
// * positive.
// */
//TEST(Camera_Transformed, importance0) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	int w = 200;
//	int h = 100;
//	float r = 0.1f;
//
//	auto raster = Pointer<HDRImage<float, 3>>::make(pool, pool, w, h);
//	auto film = Sensor::build(
//		pool,
//		raster,
//		Pointer<const XYZColorSpace>{XYZColorSpace::build(pool)},
//		Pointer<const Filter2D>{}
//	);
//	auto trs = Transformation<float, 3>::rotation<2, 0>(PI<float>() / 4.f) * Transformation<float, 3>::translation(Vectr3F(1.f, 2.f, 3.f));
//	auto cam = TransformedCamera::build(pool, trs, PerspectiveCamera::build(pool, 45 * M_PI / 180, r, 1.5f, film));
//	auto rng = PlainRNGFactory<>().build(pool, 6);
//
//	float rnd[6];
//	for (int i = 0; i < CAMERA_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		rnd[0] *= w;
//		rnd[1] *= h;
//		Ray ray = cam->ray(rnd[0], rnd[1], rnd[2], rnd[3], rnd[4], rnd[5], nullptr);
//		ASSERT_TRUE(cam-> importance0(ray(ray.r.lower), ray.t) > 0.f);
//		ASSERT_TRUE(cam->sampleGeoPDF(ray(ray.r.lower), ray.t) > 0.f);
//	}
//}
//
///*
// * Asserts that the directional differential of importance and PDF of the
// * generated rays are positive.
// */
//TEST(Camera_Transformed, importance1) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	int w = 200;
//	int h = 100;
//	float r = 0.1f;
//
//	auto raster = Pointer<HDRImage<float, 3>>::make(pool, pool, w, h);
//	auto film = Sensor::build(
//		pool,
//		raster,
//		Pointer<const XYZColorSpace>{XYZColorSpace::build(pool)},
//		Pointer<const Filter2D>{}
//	);
//	auto trs = Transformation<float, 3>::rotation<2, 0>(PI<float>() / 4.f) * Transformation<float, 3>::translation(Vectr3F(1.f, 2.f, 3.f));
//	auto cam = TransformedCamera::build(pool, trs, PerspectiveCamera::build(pool, 45 * M_PI / 180, r, 1.5f, film));
//	auto rng = PlainRNGFactory<>().build(pool, 6);
//
//	float rnd[6];
//	for (int i = 0; i < CAMERA_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		rnd[0] *= w;
//		rnd[1] *= h;
//		Ray ray = cam->ray(rnd[0], rnd[1], rnd[2], rnd[3], rnd[4], rnd[5], nullptr);
//		ASSERT_TRUE(cam-> importance1(ray) > 0.f);
//		ASSERT_TRUE(cam->sampleDirPDF(ray) > 0.f);
//	}
//}
//
///******************************************************************************
// * ANIMATED CAMERA TESTS
// *****************************************************************************/
//
///*
// * Asserts that the generated rays are correct.
// */
//TEST(Camera_Animated, ray) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	int w = 200;
//	int h = 100;
//	Norml3F norml;
//	float r = 0.1f;
//	auto raster = Pointer<HDRImage<float, 3>>::make(pool, pool, w, h);
//	auto film = Sensor::build(
//		pool,
//		raster,
//		Pointer<const XYZColorSpace>{XYZColorSpace::build(pool)},
//		Pointer<const Filter2D>{}
//	);
//	using frame_t = KeyFrameTransformation::KeyFrame;
//	KeyFrameTransformation::array_t array(pool, 2);
//	array[0] = frame_t(0.f, Affine<float, 3>::identity());
//	array[1] = frame_t(1.f, Affine<float, 3>::rotation(2, 0, PI<float>() / 4.f) * Affine<float, 3>::translation(Vectr3F{1.f, 2.f, 3.f}));
//	auto transformation = KeyFrameTransformation::build(pool, array);
//	auto cam = AnimatedCamera::build(pool, transformation, PerspectiveCamera::build(pool, 45 * M_PI / 180, r, 1.5f, film));
//	auto rng = PlainRNGFactory<>().build(pool, 6);
//	float rnd[6];
//	for (int i = 0; i < CAMERA_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		rnd[0] *= w;
//		rnd[1] *= h;
//		Ray ray = cam->ray(rnd[0], rnd[1], rnd[2], rnd[3], rnd[4], rnd[5], &norml);
//		ASSERT_NEAR(rnd[4], ray.l, 1E-4f);
//		ASSERT_NEAR(rnd[5], ray.t, 1E-4f);
//		ASSERT_NEAR(1.f, ray.d.length(), 1E-4f);
//		ASSERT_EQ(+INFINITY, ray.r.upper);
//		Point3F origin = transformation->atTime(ray.t).inverse() * ray(ray.r.lower);
//		ASSERT_NEAR(0.f, origin[2], 1E-4f);
//		ASSERT_TRUE(origin[0] * origin[0] + origin[1] * origin[1] <= r * r);
//		ASSERT_TRIPLET_NEAR(Norml3F(0.f, 0.f, 1.f), transformation->atTime(ray.t).inverse() * norml, 1E-4f);
//	}
//}
//
///*
// * Asserts that the computed raster coordinates are consistent with the
// * generated rays.
// */
//TEST(Camera_Animated, raster) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	int w = 200;
//	int h = 100;
//	float r = 0.1f;
//	auto raster = Pointer<HDRImage<float, 3>>::make(pool, pool, w, h);
//	auto film = Sensor::build(
//		pool,
//		raster,
//		Pointer<const XYZColorSpace>{XYZColorSpace::build(pool)},
//		Pointer<const Filter2D>{}
//	);
//	using frame_t = KeyFrameTransformation::KeyFrame;
//	KeyFrameTransformation::array_t array(pool, 2);
//	array[0] = frame_t(0.f, Affine<float, 3>::identity());
//	array[1] = frame_t(1.f, Affine<float, 3>::rotation(2, 0, PI<float>() / 4.f) * Affine<float, 3>::translation(Vectr3F{1.f, 2.f, 3.f}));
//	auto transformation = KeyFrameTransformation::build(pool, array);
//	auto cam = AnimatedCamera::build(pool, transformation, PerspectiveCamera::build(pool, 45 * M_PI / 180, r, 1.5f, film));
//	auto rng = PlainRNGFactory<>().build(pool, 6);
//	float rnd[6], x, y;
//	for (int i = 0; i < CAMERA_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		rnd[0] *= w;
//		rnd[1] *= h;
//		Ray ray = cam->ray(rnd[0], rnd[1], rnd[2], rnd[3], rnd[4], rnd[5], nullptr);
//		ASSERT_TRUE(cam->raster(ray, &x, &y));
//		ASSERT_NEAR(rnd[0], x, 1E-4f);
//		ASSERT_NEAR(rnd[1], y, 1E-4f);
//	}
//}
//
///*
// * Asserts that the total emitted importance and PDF of the generated rays are
// * positive.
// */
//TEST(Camera_Animated, importance0) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	int w = 200;
//	int h = 100;
//	float r = 0.1f;
//	auto raster = Pointer<HDRImage<float, 3>>::make(pool, pool, w, h);
//	auto film = Sensor::build(
//		pool,
//		raster,
//		Pointer<const XYZColorSpace>{XYZColorSpace::build(pool)},
//		Pointer<const Filter2D>{}
//	);
//	using frame_t = KeyFrameTransformation::KeyFrame;
//	KeyFrameTransformation::array_t array(pool, 2);
//	array[0] = frame_t(0.f, Affine<float, 3>::identity());
//	array[1] = frame_t(1.f, Affine<float, 3>::rotation(2, 0, PI<float>() / 4.f) * Affine<float, 3>::translation(Vectr3F{1.f, 2.f, 3.f}));
//	auto transformation = KeyFrameTransformation::build(pool, array);
//	auto cam = AnimatedCamera::build(pool, transformation, PerspectiveCamera::build(pool, 45 * M_PI / 180, r, 1.5f, film));
//	auto rng = PlainRNGFactory<>().build(pool, 6);
//	float rnd[6];
//	for (int i = 0; i < CAMERA_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		rnd[0] *= w;
//		rnd[1] *= h;
//		Ray ray = cam->ray(rnd[0], rnd[1], rnd[2], rnd[3], rnd[4], rnd[5], nullptr);
//		ASSERT_TRUE(cam-> importance0(ray(ray.r.lower), ray.t) > 0.f);
//		ASSERT_TRUE(cam->sampleGeoPDF(ray(ray.r.lower), ray.t) > 0.f);
//	}
//}
//
///*
// * Asserts that the directional differential of importance and PDF of the
// * generated rays are positive.
// */
//TEST(Camera_Animated, importance1) {
//	MemoryPool pool{4, new CPUAllocator{}};
//	int w = 200;
//	int h = 100;
//	float r = 0.1f;
//	auto raster = Pointer<HDRImage<float, 3>>::make(pool, pool, w, h);
//	auto film = Sensor::build(
//		pool,
//		raster,
//		Pointer<const XYZColorSpace>{XYZColorSpace::build(pool)},
//		Pointer<const Filter2D>{}
//	);
//	using frame_t = KeyFrameTransformation::KeyFrame;
//	KeyFrameTransformation::array_t array(pool, 2);
//	array[0] = frame_t(0.f, Affine<float, 3>::identity());
//	array[1] = frame_t(1.f, Affine<float, 3>::rotation(2, 0, PI<float>() / 4.f) * Affine<float, 3>::translation(Vectr3F{1.f, 2.f, 3.f}));
//	auto transformation = KeyFrameTransformation::build(pool, array);
//	auto cam = AnimatedCamera::build(pool, transformation, PerspectiveCamera::build(pool, 45 * M_PI / 180, r, 1.5f, film));
//	auto rng = PlainRNGFactory<>().build(pool, 6);
//	float rnd[6];
//	for (int i = 0; i < CAMERA_SAMPLE_COUNT; ++i) {
//		rng->next(rnd);
//		rnd[0] *= w;
//		rnd[1] *= h;
//		Ray ray = cam->ray(rnd[0], rnd[1], rnd[2], rnd[3], rnd[4], rnd[5], nullptr);
//		ASSERT_TRUE(cam-> importance1(ray) > 0.f);
//		ASSERT_TRUE(cam->sampleDirPDF(ray) > 0.f);
//	}
//}
//
//}}

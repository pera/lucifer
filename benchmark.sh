#!/bin/bash
COMMIT_DATE="$(git log --pretty=format:"%cd" -1)"
SYSTEM_INFO="Threads: $(nproc --all) - $(cat /proc/meminfo | grep MemTotal: | tr -s [:space:])"
FILES=benchmark/*.xml
for f in $FILES
do
	echo -n -e "${COMMIT_DATE}\t${SYSTEM_INFO}\t"
	Release/lucifer $f
done
